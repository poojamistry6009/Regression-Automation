/**
 * Created by INDUSA
 */
require('mocha');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var error = require('../utility/error.js');
var dependency = require('../utility/dependency.js');

var login = function (username, password, usernameDisplays) {
    describe("Login to the system",function () {
        it("should login to the system !",function (browser) {
            browser.url(variables.fullUrl,function() {
                    browser.pause(3000);
                    browser.expect.element(elements.loginPageElements.body).to.be.visible;
                    browser.expect.element(elements.loginPageElements.usernameInput).to.be.visible;
                    commands.checkAndPerform('clearValue',browser,elements.loginPageElements.usernameInput);
                    commands.checkAndPerform('clearValue',browser,elements.loginPageElements.passwordInput);
                    commands.checkAndSetValue(browser,elements.loginPageElements.usernameInput,username);
                    commands.checkAndSetValue(browser,elements.loginPageElements.passwordInput,password);
                    commands.checkAndPerform('click',browser,elements.loginPageElements.submitBtn);
                    browser.pause(6000);
                    // browser.expect.element(elements.loginPageElements.container).to.be.visible;
                    // browser.assert.containsText(elements.loginPageElements.container, usernameDisplays);
                    dependency.changeStateOf('userLogin',true);
            });
        });
    });
};
module.exports.login = login;

var logout = function () {
    describe("Logout from the system",function () {
        it("should logout from the system !",function (browser) {
                browser.pause(4000);
                browser.moveToElement(elements.globalElements.headerMenu, 3000, 3000, function () {
                    browser.pause(2000);
                    browser.click(elements.globalElements.headerMenu);
                    browser.pause(5000);
                    commands.checkAndPerform('click', browser, elements.globalElements.logoutOption);
                    dependency.changeStateOf('userLogout',true);
                });
                browser.pause(2000);
        });
    });
};
module.exports.logout = logout;