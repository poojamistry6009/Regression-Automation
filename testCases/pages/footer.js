/**
 * Created by Indusa on 6/5/2017.
 */


require('mocha');
var expect = require('chai').expect;
var me = require('./organization.js');
var adminOperations = require('./admin.js');
var messageOperations = require('./message.js');
var contactOperations = require('./contacts.js');
var navigateOperations = require('../utility/navigate.js');
var commonOperations = require('./commons.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var error = require('../utility/error.js');
var dependency = require('../utility/dependency.js');
var me = require('./footer.js');
var currentUrl = '';


var check_Terms_Of_Use_Link = function () {
    describe("FOOTERS LINKS ", function () {
        it("Should check - Footer links ", function (browser) {
            console.log("---------- in terms of use link-------------> " + variables.tempUrl);
            browser.pause(2000);
            browser.url(variables.tempUrl, function (result) {
                browser.pause(2000);
                browser.expect.element('/html/body').to.have.attribute('class').which.contain('terms-of-use');
            });

        });
    });
}
exports.check_Terms_Of_Use_Link = check_Terms_Of_Use_Link;


var check_Footer_Link_Availability = function () {
    describe("FOOTERS LINKS ", function () {
        it("Should check - Footer links ", function (browser) {
            browser.getAttribute(elements.otherSheet.privacyPolicyLink, 'href', function (result) {
                console.log("PRIVACY URL : " + result.value);
                variables.tempUrl = result.value;
                browser.url(variables.tempUrl, function () {
                    browser.pause(2000);
                        browser.expect.element(elements.otherSheet.privacyPolicyPageBanner).to.have.attribute('class').which.contain('banner-inside');
                    browser.back();
                });
            });
            browser.getAttribute(elements.otherSheet.termsOfUseLink, 'href', function (result) {
                console.log("TERMS OF USE URL : "+result.value);
                variables.tempUrl = result.value;
                browser.url(variables.tempUrl, function () {
                    browser.pause(2000);
                    browser.expect.element(elements.otherSheet.termsOfUsePageDiv).to.have.attribute('class').which.contain('banner-inside');
                });

            });
        });
    });
}
exports.check_Footer_Link_Availability = check_Footer_Link_Availability;


var check_footerLink_Availability = function () {
    describe(" SEARCH FOR CAMPAIGN OBJECT ", function () {
        // me.check_Privacy_Policy_Link();
        // me.check_Terms_Of_Use_Link();
    });
}
exports.check_footerLink_Availability = check_footerLink_Availability;
