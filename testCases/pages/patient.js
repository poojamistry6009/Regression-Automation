/**
 * Created by INDUSA
 */
require('mocha');
var expect = require('chai').expect;
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var navigate = require('../utility/navigate.js');
var elements = require('../utility/elements.js');
var me = require('./patient.js');
var commonOperations = require('./commons.js');
var variables = require('../utility/variables.js');
var organizationOperations = require('./organization.js');


//TEST CASE DECLARATION - START

var navigate_On_Order_And_Patient_Tab = function () {
    describe("Navigate to order and patients page", function () {
        it("should navigate - Navigate to order and patients page", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_On_Order_And_Patient_Tab = navigate_On_Order_And_Patient_Tab;

var create_Patient = function () {
    describe("Create a patient", function () {
        it("should create a patient", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.createPatientLink);
            variables.createdPatient = variables.newPatientFName;
            variables.createdPatientFName = variables.newPatientFName;
            variables.createdPatientLName = variables.newPatientLName;
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientFirstNameInput,variables.newPatientFName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientLastNameInput,variables.newPatientLName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientOrgInput,variables.createdORG);
            browser.pause(5000);
            browser.keys(browser.Keys.RIGHT_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientCreateBtn);
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientsSaveAndBackBtn);
            browser.pause(3000);
            commands.checkAndPerform('clearValue', browser, elements.ordersandPatientsPageElements.patientSearchFNameInput);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientSearchFNameInput, variables.createdPatientFName);
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientSearchBtn);
            browser.pause(3000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.ordersandPatientsPageElements.patientsSearchTableList).to.be.present;
            browser.elements("xpath", elements.ordersandPatientsPageElements.patientsSearchTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdPatient)).to.be.true;
                        variables.dependencyFulfilled.push('createdPatient');
                        console.log('=====> Patient created successfully : '+variables.createdPatient);

                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_Patient = create_Patient;

var to_check_object_present_in_Order_And_Patients = function () {
    describe("fields availability (Main fields and Additional information fields element)", function () {
        it("should check  -  fields availability (Main fields and Additional information fields element)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.orderSearchBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.ordersAndPatientsTabLink).to.have.attribute('class').which.contain('selected-tab');
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchBtn).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput).to.be.present;

            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListLastNameInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListSSNInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListMRNInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListDOBInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListOrderNumberInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListSpecimenIdInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListOrderingProviderInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListOrderingLocationInput).to.be.present;

            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListPanelNameInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListOrderDateInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListResultDateInput).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchTableListAccessionedDateInput).to.be.present;

        });
    });
};
exports.to_check_object_present_in_Order_And_Patients = to_check_object_present_in_Order_And_Patients;

var order_And_Patient_Tab_Is_Date_Selector = function () {
    describe(" DOB brings up date selector", function () {
        it("Should check - DOB brings up date selector", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.orderSearchBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchDobDiv);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchDobDiv).to.be.visible;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchOrderDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchOrderDateDiv).to.be.visible;
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchResultDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchResultDateDiv).to.be.visible;
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchAccessionedDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchAccessionedDateDiv).to.be.visible;
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsOrderingProviderSearchIcon).to.have.attribute('name').which.contain('search_field');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsOrderingLocationSearchIcon).to.have.attribute('name').which.contain('search_field');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsPanelNameSearchIcon).to.have.attribute('name').which.contain('search_field');
        });
    });
}
exports.order_And_Patient_Tab_Is_Date_Selector = order_And_Patient_Tab_Is_Date_Selector;

var check_Orders_Search = function () {
    describe(" Clicking search brings up a table of all related items that fit description ", function () {
        it("Should check -Clicking search brings up a table of all related items that fit description ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableSearchBtn);
            browser.expect.element(elements.ordersandPatientsPageElements.orderErrorMsgOnFirstName).to.be.visible;  //if trying to search with all empty fields error message to fill out one field pops up
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput, variables.orderAndPatientFirstNameSuffix);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableSearchBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Order Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Accessioned Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Result Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('MRN');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Date Of Birth');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Status');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Attachment');
        });
    });
}
exports.check_Orders_Search = check_Orders_Search;

var navigate_activityScreen_From_orderAndPatients_Page = function () {
    describe(" Clicking create case takes you to case creation screen", function () {
        it("Should check - Clicking create case takes you to case creation screen", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderCaseIconOnOrderTable);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderTaskIconOnOrderTable);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderMemoIconOnOrderTable);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.caseSaveAndBackBtn);
            browser.pause(3000);
        });
    });
}
exports.navigate_activityScreen_From_orderAndPatients_Page = navigate_activityScreen_From_orderAndPatients_Page;

var navigate_To_PatientSearch_Tab = function () {
    describe(" Clicking on patient search navigates to this screen", function () {
        it("Should check -Clicking on patient search navigates to this screen ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientSearchTab);
            browser.expect.element(elements.ordersandPatientsPageElements.patientSearchTab).to.have.attribute('class').to.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.navigate_To_PatientSearch_Tab = navigate_To_PatientSearch_Tab;

var check_fieldName_is_Enter_Or_Not = function () {
    describe(" Can enter first Name,Last Name,SSN,MRN into text field ", function () {
        it("Should check - Can enter first Name,Last Name,SSN,MRN into text field ", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchFNameInput, variables.newPatientFName);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchLNameInput, variables.newPatientLName);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchSSNInput, variables.newSSNNumber);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchMRNInput, variables.newMRNNumber);
            browser.pause(2000);

            // commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderPageResetBtn);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientSearchDOBInput);
            browser.expect.element(elements.ordersandPatientsPageElements.patientSearchDOBInput).to.be.visible;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsPopWindowCloseBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.orderPageResetBtn);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsSearchBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientErrorMsgOnFirstName).to.be.visible;
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchFNameInput, variables.patientFirstNameSuffix);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsSearchBtn);
            browser.pause(2000);
            commands.readValuesAndAssert(browser, elements.ordersandPatientsPageElements.patientsSearchTableList, variables.patientFirstNameSuffix, true);
            browser.pause(2000);
        });
    });
}
exports.check_fieldName_is_Enter_Or_Not = check_fieldName_is_Enter_Or_Not;

var check_Error_Message_With_Invalid_Input = function () {
    describe(" if not all required fields are filled out proper error messages display", function () {
        it("Should check - if not all required fields are filled out proper error messages display ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.newPatientCreateBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientCreateBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientFNameErrorMsg).to.be.visible;
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientLNameErrorMsg).to.be.visible;
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgErrorMsg).to.be.visible;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientsPopWindowCloseBtn);
            browser.pause(2000);
        });
    });
}
exports.check_Error_Message_With_Invalid_Input = check_Error_Message_With_Invalid_Input;

var check_Salutation_DropDown_Menu_List = function () {
    describe(" Salutation has functional drop down menu including: (Mr,Mrs,Ms,Dr)", function () {
        it("Should check - Salutation has functional drop down menu including:(Mr,Mrs,Ms,Dr)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.newPatientCreateBtn);
            browser.pause(2000);
            browser.moveToElement(elements.ordersandPatientsPageElements.patientSalutationDrpDownMenu, 1000, 1000, function () {
                commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientSalutationDrpDownMenu);
            });
            browser.expect.element(elements.ordersandPatientsPageElements.patientsSalutationMrOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsSalutationMrsOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsSalutationMsOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsSalutationDrOption).to.be.present;
        });
    });
}
exports.check_Salutation_DropDown_Menu_List = check_Salutation_DropDown_Menu_List;

var check_Value_Entered_Is_Accepted_Or_Not = function () {
    describe(" Can enter in a: fName,lName,Mobile_Number,Org,Suffix ", function () {
        it("Should check -Can enter in a: fName,lName,Mobile_Number,Org,Suffix", function (browser) {

            // commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.createPatientLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientFirstNameInput, variables.newPatientFName);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientLastNameInput, variables.newPatientLName);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSuffixInput, variables.newSuffixAdd);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientEmailInput, variables.newEmailAdd);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientMobileInput, variables.newMobileNumberAdd);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientOrgInput, variables.newOrgForPatients);
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsCreateNewOrgBtn);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsCreateOrgBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientCreateBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsSaveAndBackBtn);
            browser.pause(2000);

        });
    });
}
exports.check_Value_Entered_Is_Accepted_Or_Not = check_Value_Entered_Is_Accepted_Or_Not;

var navigate_To_Patient_Address_Tab = function () {
    describe("Navigate to patient's Address subtab", function () {
        it("should navigate to patient's address subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientAddressSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Patient_Address_Tab = navigate_To_Patient_Address_Tab;

var navigate_To_Patient_Activity_Tab = function () {
    describe("Navigate to patient's Activity subtab", function () {
        it("should navigate to patient's activity subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.orderAndPatientsActivitySubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Patient_Activity_Tab = navigate_To_Patient_Activity_Tab;

var navigate_To_New_Patient = function () {
    describe("Navigate to newly created patient", function () {
        it("should navigate to newly created patient", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientSearchTab);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.ordersandPatientsPageElements.patientSearchFNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientSearchFNameInput, variables.createdPatient);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientSearchBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdPatient+"')]");
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Patient = navigate_To_New_Patient;

var check_AddressTab_Highlighted = function () {
    describe("Patient address tab is highlighted", function () {
        it("should check whether patient address tab is highlighted or not", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_AddressTab_Highlighted = check_AddressTab_Highlighted;

var check_ReadOnly_Fields = function () {
    describe("Read only fields", function () {
        it("should check for read only fields", function (browser) {
            browser.pause(2000);
            // Verify that mailing address fields are read only
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStreet1Input).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStreet2Input).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingCityInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStateInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingPostalInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingCountryInput).to.not.present;

            // Verify  that  billing address fields are read only
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingStreetInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingStreet2Input).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingCityInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingStateInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingPostalInput).to.not.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressBillingCountryInput).to.not.present;

            browser.pause(2000);
        });
    });
};
exports.check_ReadOnly_Fields = check_ReadOnly_Fields;

var check_Fields = function () {
    describe("Fields presence", function () {
        it("should check fields are present", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientEditBtn);
            browser.pause(2000);

            // 2 fields for street
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStreet1Input).to.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStreet2Input).to.present;

            // field for city
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingCityInput).to.present;

            // field for state
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingStateInput).to.present;

            // field for postal code
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingPostalInput).to.present;

            // code for the country
            browser.expect.element(elements.ordersandPatientsPageElements.patientAddressMailingCountryInput).to.present;

            browser.pause(2000);
        });
    });
};
exports.check_Fields = check_Fields;

var check_OrganizationTab_Highlighted = function () {
    describe("Patient organization tab", function () {
        it("should check - organization tab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTab);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_OrganizationTab_Highlighted = check_OrganizationTab_Highlighted;

var check_Org_Table = function () {
    describe("Patient organization custom list", function () {
        it("should check - organization custom list has linked organization links", function (browser) {
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdORG+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Org_Table = check_Org_Table;

var check_DeleteOrg_Link = function () {
    describe("Delete organization links", function () {
        it("should check - organizations cannot be deleted", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.ordersandPatientsPageElements.patientOrgTabTable+"/tbody/tr/td", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        counter = counter + 1;
                        expect(text.value).to.not.equal('delete');
                    });
                    // expect(counter).to.equal(7);    //excluding delete column for each row
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_DeleteOrg_Link = check_DeleteOrg_Link;

var check_CustomList_Sort = function () {
    describe("Sort added organizations", function () {
        it("should check - can sort added organizations with custom lists", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientOrgTabNewCustomListInput, variables.newCustomListTestName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgCustomListAddSortConditionBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgCustomListSortByField);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabNewCustomListSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTableOrgColumnHeader).to.have.attribute('class').which.contain('sortable');
            browser.pause(2000);
            browser.keys(browser.Keys.ESCAPE);
            browser.pause(2000);
        });
    });
};
exports.check_CustomList_Sort = check_CustomList_Sort;

var order_CustomLists = function () {
    describe("Order the custom lists", function () {
        it("should check - can order the custom lists", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListLink);
            browser.pause(2000);
            var temp = '';
            browser.getText(elements.ordersandPatientsPageElements.patientOrgTabOrderCustomLists+"/li[1]",function (result) {
                temp = result.value;
            });
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabOrderCustomLists+"/li[1]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgCustomListOrderBtnDown);
            browser.pause(2000);
            browser.getText(elements.ordersandPatientsPageElements.patientOrgTabOrderCustomLists+"/li[2]",function (result) {
                expect(result.value).to.equal(temp);
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.order_CustomLists = order_CustomLists;

var check_Filters = function () {
    describe("Filters working", function () {
        it("should check - filters are properly working or not", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabQuickFilterLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabFilterByDropdown);
            browser.pause(2000);
            browser.expect.element("//option[@value='name']").to.be.present;
            browser.expect.element("//option[@value='mailingAddressState']").to.be.present;
            browser.expect.element("//option[@value='phone']").to.be.present;
            browser.expect.element("//option[@value='salesRep']").to.be.present;
            browser.expect.element("//option[@value='specialties']").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabFilterOperatorDropdown);
            browser.pause(2000);
            browser.expect.element("//option[@value='EQ']").to.be.present;
            browser.expect.element("//option[@value='NEQ']").to.be.present;
            browser.expect.element("//option[@value='STARTWITH']").to.be.present;
            browser.expect.element("//option[@value='NOTSTARTWITH']").to.be.present;
            browser.expect.element("//option[@value='ISNULL']").to.be.present;
            browser.expect.element("//option[@value='ISNOTNULL']").to.be.present;
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabFilterValueInput).to.be.an('input');
            browser.pause(2000);
        });
    });
};
exports.check_Filters = check_Filters;

var check_Sortable_Columns = function () {
    describe("Sortable columns", function () {
        it("should check - sortable columns can be sorted or not", function (browser) {
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Organization')]").to.have.attribute('class').which.contain('sortable');
            browser.expect.element("//th[contains(.,'Mailing Address State')]").to.have.attribute('class').which.contain('sortable');
            browser.expect.element("//th[contains(.,'Phone')]").to.have.attribute('class').which.contain('sortable');

            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//th[contains(.,'Organization')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Organization')]").to.have.attribute('class').which.contain('sortable sort-ascending');
            commands.checkAndPerform('click', browser, "//th[contains(.,'Organization')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Organization')]").to.have.attribute('class').which.contain('sortable sort-descending');

            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//th[contains(.,'Mailing Address State')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Mailing Address State')]").to.have.attribute('class').which.contain('sortable sort-ascending');
            commands.checkAndPerform('click', browser, "//th[contains(.,'Mailing Address State')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Mailing Address State')]").to.have.attribute('class').which.contain('sortable sort-descending');

            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//th[contains(.,'Phone')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Phone')]").to.have.attribute('class').which.contain('sortable sort-ascending');
            commands.checkAndPerform('click', browser, "//th[contains(.,'Phone')]");
            browser.pause(2000);
            browser.expect.element("//th[contains(.,'Phone')]").to.have.attribute('class').which.contain('sortable sort-descending');

            browser.pause(2000);
        });
    });
};
exports.check_Sortable_Columns = check_Sortable_Columns;

var check_Org_Search = function () {
    describe("Organization search", function () {
        it("should check - organization can be searched and added in the list or not", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientOrgTabSearchOrgInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdORG+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Org_Search = check_Org_Search;

var check_CreateOrg_Link = function () {
    describe("Organization create link", function () {
        it("should check - organization create link opens a pop up for creating organization", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientOrgTabCreateOrgLink);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgCreatePopupOrgNameInput).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientsPopWindowCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_CreateOrg_Link = check_CreateOrg_Link;

var check_Patients_Pagination = function () {
    describe("Pagination functionality in organization tab patient detail page",function () {
        it("should check pagination functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.ordersandPatientsPageElements.patientOrgTabNumberOfRecordsDropdown+"/option[4]");
            browser.getText(elements.ordersandPatientsPageElements.patientOrgTabTotalRecords,function (result) {
                browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabPagingRecordNumbers).text.to.equal('1 - '+result.value);
            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.ordersandPatientsPageElements.patientOrgTabNumberOfRecordsDropdown+"/option[1]");
            browser.getText(elements.ordersandPatientsPageElements.patientOrgTabTotalRecords,function (result) {
                if(result.value < 10){
                    browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabPagingRecordNumbers).text.to.equal('1 - '+result.value);
                }
                if(result.value > 10){
                    browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabPagingRecordNumbers).text.to.equal('1 - 10');
                    commands.checkAndPerform('click',browser,elements.ordersandPatientsPageElements.patientOrgPagingNextBtn);
                    browser.pause(3000);
                    var totalNumberOfPages = Math.ceil(parseInt(result.value) / 10);
                    browser.getValue(elements.ordersandPatientsPageElements.patientOrgPagingCurrentPageInput,function (result2) {
                        if(parseInt(result2.value) === totalNumberOfPages){
                            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabPagingRecordNumbers).text.to.equal('11 - '+result.value);
                        }
                        else{
                            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTabPagingRecordNumbers).text.to.equal('11 - '+(10 * parseInt(result2.value)));
                        }
                    });
                }
            });
        });
    });
};
exports.check_Patients_Pagination = check_Patients_Pagination;

var order_And_Patient_Tab_is_Date_Selector = function () {
    describe(" DOB brings up date selector", function () {
        it("Should check - DOB brings up date selector", function (browser) {

            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchDobDiv);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchDobDiv).to.be.visible;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchOrderDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchOrderDateDiv).to.be.visible;
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchResultDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchResultDateDiv).to.be.visible;
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchAccessionedDateDiv);
            browser.expect.element(elements.ordersandPatientsPageElements.orderSearchAccessionedDateDiv).to.be.visible;
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsOrderingProviderSearchIcon).to.have.attribute('name').which.contain('search_field');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsOrderingLocationSearchIcon).to.have.attribute('name').which.contain('search_field');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsPanelNameSearchIcon).to.have.attribute('name').which.contain('search_field');
        });
    });
}
exports.order_And_Patient_Tab_is_Date_Selector = order_And_Patient_Tab_is_Date_Selector;

var check_search_with_all_related_item_having_description = function () {
    describe(" Clicking search brings up a table of all related items that fit description ", function () {
        it("Should check -Clicking search brings up a table of all related items that fit description ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableSearchBtn);
            browser.expect.element(elements.ordersandPatientsPageElements.orderErrorMsgOnFirstName).to.be.visible;  //if trying to search with all empty fields error message to fill out one field pops up
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.orderSearchTableListFirstNameInput, variables.orderAndPatientFirstNameSuffix);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSearchTableSearchBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Order Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Accessioned Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Result Date');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('MRN');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Date Of Birth');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Status');
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientTableListOfColumn).text.to.contain('Attachment');
        });
    });
}
exports.check_search_with_all_related_item_having_description = check_search_with_all_related_item_having_description;

var navigate_to_OrderList_tab = function () {
    describe("Accession/order list tab is highlighted", function () {
        it("Should check - Accession/order list tab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderListTab);
            browser.expect.element(elements.ordersandPatientsPageElements.orderListTab).to.have.attribute('class').to.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.navigate_to_OrderList_tab = navigate_to_OrderList_tab;

var check_List_DropDown_Menu = function () {
    describe(" List date Range drop menu has: Order date", function () {
        it("List date Range drop menu has: Order date ", function (browser) {
            browser.moveToElement(elements.ordersandPatientsPageElements.patientsListDateRangeMenu, 1000, 1000, function () {
                commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientsListDateRangeMenu);
            });
            browser.expect.element(elements.ordersandPatientsPageElements.patientsLDROrderDateOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsLDRAccessionedDateOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsLDRResultDateOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsLDRCollectionDateOption).to.be.present;

        });
    });
}
exports.check_List_DropDown_Menu = check_List_DropDown_Menu;

var startDate_Functional_Date_Selector = function () {
    describe("Start date has functional date selector", function () {
        it("Should check - Start date has functional date selector", function (browser) {

            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderStartDateDiv);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderStartDateDiv).to.be.visible;
            browser.pause(2000);

        });
    });
}
exports.startDate_Functional_Date_Selector = startDate_Functional_Date_Selector;

var range_slideRule = function () {
    describe("Range slide rule can go from 1 to 30 days", function () {
        it("Should check - Range slide rule can go from 1 to 30 days", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderRangeSlideInput).to.have.attribute('min').which.contain('1');
            browser.expect.element(elements.ordersandPatientsPageElements.orderRangeSlideInput).to.have.attribute('max').which.contain('30');
        });
    });
}
exports.range_slideRule = range_slideRule;

var check_CustomList_Option_Availability = function () {
    describe(" Can clone custom lists here", function () {
        it("Should check - Can clone custom lists here ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderSettingsIconBtn);

            browser.expect.element(elements.ordersandPatientsPageElements.cloneCustomListBtn).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.newCustomListBtn).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.orderCustomListBtn).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_CustomList_Option_Availability = check_CustomList_Option_Availability;

var check_OperatorField_Options = function () {
    describe(" can change the operator field to:", function () {
        it("Should check - can change the operator field to:", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderQuickFilterLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.operatorColumnList);

            browser.moveToElement(elements.ordersandPatientsPageElements.operatorColumnList, 1000, 1000, function () {
                commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.operatorColumnList);
            });
            browser.expect.element(elements.ordersandPatientsPageElements.patientsEqualsOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsNEQOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsGTOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsGTEOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsLTOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsISNULLOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsISNOTNULLOperatorOption).to.be.present;
        });
    });
}
exports.check_OperatorField_Options = check_OperatorField_Options;

var check_ScheduleFilter_Options = function () {
    describe("If schedule filter the value can be a static or dynamic date", function () {
        it("Should check - If schedule filter the value can be a static or dynamic date", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderFiltersByColumnList);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.scheduleCollectionFilterByElementOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.valueColumnListOption);
            browser.expect.element(elements.ordersandPatientsPageElements.staticValueDropDownOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.relativeValueDropDownOption).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.dateSelectorOnValueField);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.dateSelectorOnValueField).to.be.visible;
            browser.pause(2000);

            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.applyBtnOrderQuickFilterPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.resetBtnOnQuickFilterPage);
            browser.pause(2000);
        });
    });
}
exports.check_ScheduleFilter_Options = check_ScheduleFilter_Options;

var check_filterData_availability = function () {
    describe(" filters just build upon the custom lists", function () {
        it("Should check - filters just build upon the custom lists", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderDateOptionInLDR);
            browser.pause(2000);

            commands.checkAndPerform('clearValue', browser, elements.ordersandPatientsPageElements.startDateInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.startDateInputField, variables.startDateOrderLsitPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderRangeSlideInput);
            browser.pause(2000);


            browser.expect.element(elements.ordersandPatientsPageElements.orderRecordTable).to.have.attribute('class').which.contain('record-table');
            browser.pause(2000);

        });
    });
}
exports.check_filterData_availability = check_filterData_availability;

var check_General_info_Tab_Highlighted = function () {
    describe("General information tab is highlighted ", function () {
        it("Should check - General information tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.generalInfoSubTab);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.generalInfoSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.check_General_info_Tab_Highlighted = check_General_info_Tab_Highlighted;

var removed_required_info = function () {
    describe("page will not save if required information is removed ", function () {
        it("Should check - page will not save if required information is removed ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.editBtnOrderAndPatientsPage);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.ordersandPatientsPageElements.firstNameInput);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.saveBtnOrderAndPatients);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientFNameErrorLabel).to.be.present;
            browser.keys(browser.Keys.ESCAPE);
            browser.pause(2000);
        });
    });
}
exports.removed_required_info = removed_required_info;

var edit_OrderAndPatients_Page = function () {
    describe("By clicking edit you can change all information on this tab ", function () {
        it("should check - By clicking edit you can change all information on this tab ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.ordersandPatientsPageElements.orderAndPatientsSalutationDrpDownMenu, 1000, 1000, function () {
                commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderAndPatientsSalutationDrpDownMenu);
            });
            browser.expect.element(elements.ordersandPatientsPageElements.editedSalutationMrOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.editedSalutationMrsOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.editedSalutationMsOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.editedSalutationDrOption).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.edit_OrderAndPatients_Page = edit_OrderAndPatients_Page;

var check_CheckBox_Toggle_CheckMark = function () {
    describe("Email checkbox for preferred Communication can be toggled on and off ", function () {
        it("Should check - Email checkbox for preferred Communication can be toggled on and off", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.communicationPreferencesBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.emailOptOutToggle);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.textOptOutToggle);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignEmailOptOutToggle);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignTextOptOutToggle);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.saveBtnOrderAndPatients);
            browser.pause(2000);
        });
    });
}
exports.check_CheckBox_Toggle_CheckMark = check_CheckBox_Toggle_CheckMark;

var check_Enter_HostCode = function () {
    describe("page will not save if required information is removed ", function () {
        it("Should check - page will not save if required information is removed ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.editBtnOrderAndPatientsPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.settingsIconTopOfPage);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.hostCodeInput);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.hostCodeInput, variables.hostCodeNumber);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.saveBtnOnSettingsIconPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.closeIconBtn);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.hostCodeSpan).text.to.contain(variables.hostCodeNumber);
            browser.pause(2000);
        });
    });
}
exports.check_Enter_HostCode = check_Enter_HostCode;

var check_Clicking_On_OrgName_InPatientsName = function () {
    describe("Can enter the organization info clicking the name under the Patient name ", function () {
        it("Should check - Can enter the organization info clicking the name under the Patient name", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orgLinkOnPatientsPage);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.organizationTabLink).to.have.attribute('class').which.contains('organizations');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageSaveAndBackBtn);
        });
    });
}
exports.check_Clicking_On_OrgName_InPatientsName = check_Clicking_On_OrgName_InPatientsName;

var check_Navigate_GeneralInfo_SubTab = function () {
    describe("Navigate to General info subtab ", function () {
        it("Should check - Navigate to General info subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.generalInfoSubTabChrome);
            browser.pause(2000);
        });
    });
}
exports.check_Navigate_GeneralInfo_SubTab = check_Navigate_GeneralInfo_SubTab;

var activities_In_GeneralInfo_OrderAnPatients_Page = function () {
    describe(" Activities, accessions, messages and campaign tabs at the bottom", function () {
        it("Should check - Activities, accessions, messages and campaign tabs at the bottom", function (browser) {
            browser.pause(2000);
            browser.assert.containsText(elements.ordersandPatientsPageElements.customlistTabTableOnGeneralInfoPage, "Activities", "Orders", "Messages", "Campaigns");
            browser.pause(2000);
        });
    });
}
exports.activities_In_GeneralInfo_OrderAnPatients_Page = activities_In_GeneralInfo_OrderAnPatients_Page;

var activities_Tab_Is_Highlighted = function () {
    describe(" activities tab is highlighted", function () {
        it("Should check - activities tab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderAndPatientsActivitySubTab);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsActivitySubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.activities_Tab_Is_Highlighted = activities_Tab_Is_Highlighted;

var check_OperatorField_Options_In_GeneralInfo = function () {
    describe(" can change the operator field to:", function () {
        it("Should check - can change the operator field to:", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnGeneralInfoSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientActivityFiltersByDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.activityNumberFilterByElementOption);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.operatorDropDownList);
            browser.expect.element(elements.ordersandPatientsPageElements.patientsEqualsOperatorOption).to.be.present;
            browser.expect.element(elements.ordersandPatientsPageElements.patientsNEQOperatorOption).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnGeneralInfoSubTab);
            browser.pause(2000);

        });
    });
}
exports.check_OperatorField_Options_In_GeneralInfo = check_OperatorField_Options_In_GeneralInfo;

var check_Given_Criteria_Case_Activity_Object = function () {
    describe("Can create a Case from here ", function () {
        it("Should check - can create a Case from here", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.caseLinkOrderPage);
            browser.pause(3000);
            var caseName = variables.newCaseName;
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.caseSubjectInputField, variables.newCaseName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(5000);
            browser.expect.element('//a[contains(@title,"' + caseName+ '")]').to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_Given_Criteria_Case_Activity_Object = check_Given_Criteria_Case_Activity_Object;

var check_Given_Criteria_Task_Activity_Object = function () {
    describe(" can create a Task from here", function () {
        it("should check -  can create a Task from here", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.taskLinkOrderPage);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.taskSubjectInputField, variables.newTaskName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"' + variables.newTaskName + '")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Given_Criteria_Task_Activity_Object = check_Given_Criteria_Task_Activity_Object;

var check_Given_Criteria_Memo_Activity_Object = function () {
    describe(" can create a Memo from here", function () {
        it("should check - can create a Memo from here", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.taskLinkOrderPage);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.memoSubjectInputField, variables.newMemoName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.memoSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"' + variables.newMemoName + '")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Given_Criteria_Memo_Activity_Object = check_Given_Criteria_Memo_Activity_Object;


var activities_In_GeneralInfo_OrderAnPatients_Page = function () {
    describe(" Activities, accessions, messages and campaign tabs at the bottom", function () {
        it("Should check - Activities, accessions, messages and campaign tabs at the bottom", function (browser) {
            browser.pause(2000);
            browser.assert.containsText(elements.ordersandPatientsPageElements.customlistTabTableOnGeneralInfoPage, "Activities", "Orders", "Messages", "Campaigns");
            browser.pause(2000);
        });
    });
}
exports.activities_In_GeneralInfo_OrderAnPatients_Page = activities_In_GeneralInfo_OrderAnPatients_Page;


var activities_Tab_Is_Highlighted = function () {
    describe(" activities tab is highlighted", function () {
        it("Should check - activities tab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderAndPatientsActivitySubTab);
            browser.pause(2000);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsActivitySubTab).to.have.attribute('class').which.contains('selected');
            browser.pause(2000);
        });
    });
}
exports.activities_Tab_Is_Highlighted = activities_Tab_Is_Highlighted;

var navigate_To_OrderAndPatients_GeneralInfo_Tab = function () {
    describe("Navigate to Order And (Order list)  page ", function () {
        it("Should  be - Navigate to Order And (Order list)  page ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientTabLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchFNameInput, 'test');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientSearchBtn);
            browser.pause(2000);
            // commands.checkAndPerform('click', browser, "//a[contains(@title,'test patient   test patient ')]");
            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[1]/table/tbody/tr[1]/td[1]/a');
            browser.pause(2000);

        });
    });
}
exports.navigate_To_OrderAndPatients_GeneralInfo_Tab = navigate_To_OrderAndPatients_GeneralInfo_Tab;

var navigate_To_OrderSubTab = function () {
    describe(" Order tab is highlighted", function () {
        it("Should check - Order tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderAndPatientsOrderSubTab);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.orderAndPatientsOrderSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.navigate_To_OrderSubTab = navigate_To_OrderSubTab;

var navigate_To_OrgSubTab = function () {
    describe(" Organization tab is highlighted", function () {
        it("Should check - Organization tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientOrgTab);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.patientOrgTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.navigate_To_OrgSubTab = navigate_To_OrgSubTab;


var quickFilter_On_Order_SubTab_DownLink = function () {
    describe("Quick filter brings down quick filter menu ", function () {
        it("Should check - Quick filter brings down quick filter menu  ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab);
            browser.pause(2000);
        });
    });
}
exports.quickFilter_On_Order_SubTab_DownLink = quickFilter_On_Order_SubTab_DownLink;


var check_Table_Page_List = function () {
    describe(" can change how many orders are shown per page of the table", function () {
        it(" Should check -can change how many orders are shown per page of the table", function (browser) {


            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientTabLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.ordersandPatientsPageElements.patientSearchFNameInput, 'test');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientSearchBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'test patient   test patient ')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.orderAndPatientsOrderSubTab);
            browser.pause(2000);
            browser.pause(2000);

            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.rowsPerPageCount);
            // browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

        });
    });
}
exports.check_Table_Page_List = check_Table_Page_List;

var navigate_To_MessageSubTab = function () {
    describe(" Message tab is highlighted", function () {
        it("Should check - Message tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.messageSubTabOnOrderPage);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.messageSubTabOnOrderPage).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.navigate_To_MessageSubTab = navigate_To_MessageSubTab;

var quickFilter_On_Message_SubTab_DownLink = function () {
    describe("Quick filter brings down quick filter menu ", function () {
        it("Should check - Quick filter brings down quick filter menu  ", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnMessageOrderPage);
            browser.pause(2000);
        });
    });
}
exports.quickFilter_On_Message_SubTab_DownLink = quickFilter_On_Message_SubTab_DownLink;

var navigate_To_Campaign_SubTab = function () {
    describe(" Campaign tab is highlighted", function () {
        it("Should check - Campaign tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage).to.have.attribute('class').which.contains('selected');
            browser.pause(2000);

        });
    });
}
exports.navigate_To_Campaign_SubTab = navigate_To_Campaign_SubTab;


var check_Campaign_General_info_Tab_Highlighted = function () {
    describe("Campaign tab is highlighted ", function () {
        it("Should check - Campaign tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage);
            browser.expect.element(elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage).to.have.attribute('class').to.contain('selected');
            browser.pause(2000);
        });
    });
}
exports.check_Campaign_General_info_Tab_Highlighted = check_Campaign_General_info_Tab_Highlighted;


var reorder_CustomList_Order = function () {
    describe("Can reorder the custom lists ", function () {
        it("Should check - Can reorder the custom lists ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientDetailPageCampaignSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignOrgCustomListOrderLink);
            browser.pause(2000);
            browser.elements("xpath", elements.ordersandPatientsPageElements.patientDetailPageCampaignOrderListDiv+"/li", function (re) {
                var els = re.value;
               if(els.length > 1){
                   var firstElementValue = '';
                   browser.getText(elements.ordersandPatientsPageElements.patientDetailPageCampaignOrderListDiv+"/li[1]",function (result) {
                       firstElementValue = result.value;
                   });
                   commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.patientDetailPageCampaignOrderListDiv+"/li[1]");
                   browser.pause(2000);
                   commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.descendingArrowOnOrderList);
                   browser.pause(2000);
                   browser.getText(elements.ordersandPatientsPageElements.patientDetailPageCampaignOrderListDiv+"/li[2]",function (result) {
                       expect(result.value).to.equal(firstElementValue);
                   });
                   browser.pause(2000);
                   commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderCloseBtn);
                   browser.pause(2000);
               }
            });
        });
    });
}
exports.reorder_CustomList_Order = reorder_CustomList_Order;

var check_Campaign_Sortable = function () {
    describe("Can sort in ascending and descending order for Campaign name and number", function () {
        it("Should check - Can sort in ascending and descending order for Campaign name and number", function (browser) {
            browser.pause(2000);
            var counter = 0;
            browser.elements("xpath", elements.ordersandPatientsPageElements.campaignSubTabCustomListTable + "/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        //find 'Name' column and its index
                        if (text.value === 'Name') {
                            browser.pause(2000);
                            //check ascending order
                            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignSubTabCustomListTable + "/thead/tr/th[" + counter + "]");
                            browser.pause(1000);
                            browser.expect.element(elements.ordersandPatientsPageElements.campaignSubTabCustomListTable + "/thead/tr/th[" + counter + "]").to.have.attribute('class').which.contains('sortable');
                            browser.pause(2000);
                            //check descending order
                            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignSubTabCustomListTable + "/thead/tr/th[" + counter + "]");
                            browser.pause(1000);
                            browser.expect.element(elements.ordersandPatientsPageElements.campaignSubTabCustomListTable + "/thead/tr/th[" + counter + "]").to.have.attribute('class').which.contains('sortable');
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Sortable = check_Campaign_Sortable;

var navigate_To_Campaign_SubTab = function () {
    describe(" Campaign tab is highlighted", function () {
        it("Should check - Campaign tab is highlighted ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage);
            browser.pause(2000);
            browser.expect.element(elements.ordersandPatientsPageElements.campaignSubTabOnOrderPage).to.have.attribute('class').which.contains('selected');
            browser.pause(2000);

        });
    });
}
exports.navigate_To_Campaign_SubTab = navigate_To_Campaign_SubTab;

var quickFilter_On_Order_SubTab_DownLink = function () {
    describe("Quick filter brings down quick filter menu ", function () {
        it("Should check - Quick filter brings down quick filter menu  ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab);
            browser.pause(2000);
        });
    });
}
exports.quickFilter_On_Order_SubTab_DownLink = quickFilter_On_Order_SubTab_DownLink;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END








//FUNCTIONS DECLARATION - START

var new_Patients = function () {
    describe(" ORDER AND PATIENTS - NEW PATIENTS", function () {

        // organizationOperations.create_New_Org();            //For creating new organization
        me.pauseBrowserSomeTime();
        organizationOperations.navigate_To_New_Org_Page();
        me.navigate_On_Order_And_Patient_Tab();             //Navigation on order and patients page
        me.navigate_To_PatientSearch_Tab();                 //Navigation on order and patients page to patients search page
        me.check_Salutation_DropDown_Menu_List();           //Salutation has functional drop down menu including: Mr,Mrs,Ms,Dr
        me.check_Value_Entered_Is_Accepted_Or_Not();        //Can enter in a: first name,lastName,suffix,Email,Mobile Number,Org name
                                                            //from here you can create a new organization
                                                            //Typing in the organization field brings up search bar
        me.check_Error_Message_With_Invalid_Input();        //if not all required fields are filled out proper error messages display
                                                            //First name last name and organization all have asterisks indicating required
        // me.create_Patient();                                //Creating new patients
    });
}
exports.new_Patients = new_Patients;

var orders_And_Patients_Tab = function () {
    describe("ORDER AND PATIENTS - MAIN FIELDS AND ADDITIONAL INFORMATION", function () {
        me.pauseBrowserSomeTime();
        me.navigate_On_Order_And_Patient_Tab();                             //order and patients page navigation
        me.to_check_object_present_in_Order_And_Patients();                 //order and patients fields(First name,last name,SSN,MRN,DOB,Order number,Specimen ID,Provider,Ordering Location,Panel Name,Order date,result date,Accession date) visibility check
                                                                            //Orders and Patients Tab is highlighted
        me.order_And_Patient_Tab_Is_Date_Selector();                        //order and patients object date selector
        me.check_Orders_Search();                                           //order and patients object is a functional search bar
                                                                            //if trying to search with all empty fields error message to fill out one field pops up
                                                                            //clicking reset clears all fields
                                                                            //if trying to search with all empty fields error message to fill out one field pops up
        me.navigate_activityScreen_From_orderAndPatients_Page();            //order and patients object page navigation on activity (Case,Task,Memo,Mail)
                                                                            //A tab to create case, task memos and mail
                                                                            //Clicking create case takes you to case creation screen
                                                                            //clicking create task takes you to creating task screen
                                                                            //clicking create memo takes you to create memo screen
        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.ordersPagePagingDiv);         //if there are multiple pages to the table you can navigate through them with the arrows
    });
};
exports.orders_And_Patients_Tab = orders_And_Patients_Tab;

var patient_Search = function () {
    describe("ORDER AND PATIENTS - PATIENTS SEARCH", function () {
        me.pauseBrowserSomeTime();
        me.navigate_On_Order_And_Patient_Tab();
        me.navigate_To_PatientSearch_Tab();
        me.check_fieldName_is_Enter_Or_Not();
    });
}
exports.patient_Search = patient_Search;

var patient_AddressTab = function () {
    describe("ORDERS AND PATIENTS - PATIENT ADDRESS SUB TAB", function () {

        // organizationOperations.create_New_Org();
        // me.create_Patient();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_Patient_Address_Tab();
        me.check_AddressTab_Highlighted();      // address tab is highlighted
        me.check_ReadOnly_Fields();             // Verify that mailing address fields are read only
                                                // Verify  that  billing address fields are read only
        me.check_Fields();                      // 2 fields for street
                                                // field for city
                                                // field for state
                                                // field for postal code
                                                // code for the country
        // Verify that the user can add the mailing address only through ws
        // Verify that the user can add the billing address only through ws

    });
};
exports.patient_AddressTab = patient_AddressTab;

var patient_OrganizationTab = function () {
    describe("ORDERS AND PATIENTS - PATIENT ORGANIZATION SUB TAB", function () {
        // organizationOperations.create_New_Org();
        // me.create_Patient();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.check_OrganizationTab_Highlighted();     // organization tab is highlighted
        me.check_Org_Table();                       // list below has all previously linked organizations
        me.check_DeleteOrg_Link();                  // cannot remove previously linked organizations
        me.check_Sortable_Columns();                // Organization name  mailing address state and phone are underlined
                                                    // can sort in ascending and descending order the underlined table columns
        me.check_CustomList_Sort();                 // can sort added organizations with custom lists
        me.order_CustomLists();                     // can order the custom lists

        me.check_Filters();                         // can filter by : mailing address
                                                    // organization name
                                                    // phone
                                                    // sales rep
                                                    // specialty

                                                    // can change the operator field to:
                                                    //     equals
                                                    // does not equal
                                                    // begins with
                                                    //     does not begin with
                                                    //     is empty
                                                    // is not empty

                                                    // can add to the value field
        me.check_Org_Search();                      // can search for organizations to add with search bar
                                                    // search bar is functional and when selected it adds the org to the table
        me.check_Patients_Pagination();             // can change how many rows are shown in the table per page
        me.check_CreateOrg_Link();                  // create organization allows you to be taken to create a new org to add


    });
};
exports.patient_OrganizationTab = patient_OrganizationTab;

var patient_OrganizationTab_CustomList = function () {
    describe("PATIENT ORGANIZATION SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_OrgSubTab();
        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.patientOrgTabQuickFilterLink,
            elements.ordersandPatientsPageElements.patientOrgTabTable,
            elements.ordersandPatientsPageElements.patientOrgTabFilterAvailColumns,
            elements.ordersandPatientsPageElements.patientOrgTabFilterSelectedColumns,
            elements.ordersandPatientsPageElements.patientOrgTabFilterAddColBtn,
            elements.ordersandPatientsPageElements.patientOrgTabFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.patientOrgTabFilterApplyBtn);                    // clicking quick filter drops down a sort menu
        // can add and remove display columns
        // columns in the selected section are displayed in the table

        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.patientOrgTabSettingsIcon,
            elements.ordersandPatientsPageElements.patientOrgTabNewCustomListLink,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListLink,
            elements.ordersandPatientsPageElements.patientOrgTabNewCustomListInput,
            elements.ordersandPatientsPageElements.patientOrgTabNewCustomListSaveBtn,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomLists,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListCloseBtn,
            elements.ordersandPatientsPageElements.patientOrgTabCustomListAddColBtn,
            elements.ordersandPatientsPageElements.patientOrgTabCustomListRemoveColBtn,
            elements.ordersandPatientsPageElements.patientOrgTabCustomListAvailColumns,
            elements.ordersandPatientsPageElements.patientOrgTabCustomListSelectedColumns,
            elements.ordersandPatientsPageElements.patientOrgTabTable);                                      // can add a new custom list

        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.patientOrgTabSettingsIcon,
            elements.ordersandPatientsPageElements.patientOrgTabCloneCustomListInput,
            elements.ordersandPatientsPageElements.patientOrgTabCloneCustomListLink,
            elements.ordersandPatientsPageElements.patientOrgTabCloneCreateBtn,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListLink,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomLists,
            elements.ordersandPatientsPageElements.patientOrgTabOrderCustomListCloseBtn);                     // clone custom list// can clone custom list

    });
};
exports.patient_OrganizationTab_CustomList = patient_OrganizationTab_CustomList;

var order_And_Patient_Tab = function () {
    describe("ORDER AND PATIENTS - MAIN FIELDS AND ADDITIONAL INFORMATION", function () {

        // orgOperations.create_New_Org();                                  //TO create a new organization
        me.navigate_On_Order_And_Patient_Tab();                             //order and patients page navigation
        me.to_check_object_present_in_Order_And_Patients();                 //order and patients object(First name,last name,SSN,MRN,DOB,Order number,Specimen ID,Provider,Ordering Location,Panel Name,Order date,result date,Acession date) visibility check
                                                                            //Orders and Patients Tab is highlighted
        me.order_And_Patient_Tab_is_Date_Selector();                       //order and patients object date selector
        me.check_search_with_all_related_item_having_description();         //order and patients object is a functional search bar
                                                                            //if trying to search with all empty fields error message to fill out one field pops up
                                                                            //clicking reset clears all fields
                                                                            //if trying to search with all empty fields error message to fill out one field pops up
        me.navigate_activityScreen_From_orderAndPatients_Page();            //order and patients object page navigation on activity (Case,Task,Memo,Mail)
                                                                            //A tab to create case, task memos and mail
                                                                            //Clicking create case takes you to case creation screen
                                                                            //clicking create task takes you to creating task screen
                                                                            //clicking create memo takes you to create memo screen
        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.ordersPagePagingDiv);         //if there are multiple pages to the table you can navigate through them with the arrows
        // me.create_Patient();                                                 //order and patients -> Patients creation
    });
};
exports.order_And_Patient_Tab = order_And_Patient_Tab;

var order_list = function () {
    describe(" ORDER AND PATIENTS - ORDER LIST ", function () {
        me.pauseBrowserSomeTime();
        me.navigate_On_Order_And_Patient_Tab();
        me.navigate_to_OrderList_tab();                  //Accession/order list tab is highlighted
        me.check_List_DropDown_Menu();                   //List date Range drop menu has:
        me.startDate_Functional_Date_Selector();          //Start date has functional date selector
        me.range_slideRule();                           //Range slide rule can go from 1 to 30 days
        me.check_CustomList_Option_Availability();        //Can clone custom lists here
        // me.check_quickFilter_Options();                 //Click on quick filter link

        me.check_OperatorField_Options();               //can change the operator field to:
        me.check_ScheduleFilter_Options();             //if schedule filter the value can be a static or dynamic date
                                                       //if schedule filter can also use date selector to choose the date
                                                       //If not a schedule filter can add values to the value field
                                                       //reset for quick filter clears fields and restores default settings
                                                       //apply button applies the filter to the table

        me.check_filterData_availability();                //filters just build upon the custom lists


    });
}
exports.order_list = order_list;

var patient_OrdersSubtab_CustomList = function () {
    describe("PATIENT ORDERS SUBTAB", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_OrderSubTab();
        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab,
            elements.ordersandPatientsPageElements.orderSubTabCustomListTable,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterAvailableColumn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterSelectedColumn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterAddColBtn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterApplyBtn);                                       //Can filter by: Accession Date


        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.orderSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.orderSubTabCustomListNewLink,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.orderSubTabNewListInput,
            elements.ordersandPatientsPageElements.orderSubTabCustomListSaveColBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderCloseBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderAddBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderRemoveBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListAvailableColumns,
            elements.ordersandPatientsPageElements.orderSubTabCustomListSelectedColumns,
            elements.ordersandPatientsPageElements.orderSubTabCustomListTable);                                               //Create new custom list


        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.orderSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.orderSubTabCloneInput,
            elements.ordersandPatientsPageElements.orderSubTabCloneLink,
            elements.ordersandPatientsPageElements.orderSubTabCloneCreateBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderCloseBtn);// clone custom list
    });
};
exports.patient_OrdersSubtab_CustomList = patient_OrdersSubtab_CustomList;

var general_Information_Basic = function () {
    describe("GENERAL INFO - BASIC", function () {

        // organizationOperations.create_New_Org();
        // me.navigate_On_Order_And_Patient_Tab();                             //For creating new organization
        // me.create_Patient();                                                //For creating new patients
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.check_General_info_Tab_Highlighted();                              //general information tab is highlighted


        me.removed_required_info();                                         //page will not save if required information is removed
                                                                            // page can be edited and saves correctly
                                                                            //If you try to leave after entering info and not saving a prompt says it is not saved
        me.edit_OrderAndPatients_Page();                                     //by clicking edit you can change all information on this tab
                                                                             // by clicking edit you can change all information on this tab
                                                                             // fields are populated by information already filled out
                                                                             // salutation has drop down menu with; (Mr,Mrs,Dr,Ms)
        me.check_CheckBox_Toggle_CheckMark();                               //Email checkbox for preferred Communication can be toggled on and off
                                                                            //text checkbox for preffer communication can be toggled on and off
                                                                            //email opt out check box can be toggled on and off
                                                                            //text opt out can be toggled on and off
                                                                            //do not call can be toggled off and on
                                                                            //information saves correctly on this page after creation
                                                                            //page can be edited and saves correctly
        me.check_Enter_HostCode();                                          //Can enter host codes when clicking the cog next to the patient name


        me.check_Clicking_On_OrgName_InPatientsName();                      //Can enter the organization info clicking the name under the Patient name
        me.check_Navigate_GeneralInfo_SubTab();                             //Navigate to general information tab


    });
}
exports.general_Information_Basic = general_Information_Basic;

var general_Information_Activity = function () {
    describe("GENERAL INFO - ACTIVITY", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.check_General_info_Tab_Highlighted();                            //general information tab is highlighted
        me.activities_In_GeneralInfo_OrderAnPatients_Page();

        me.activities_Tab_Is_Highlighted();                                 //To check Activity tab is highlighted

        me.check_OperatorField_Options_In_GeneralInfo();                    //can change the operator field
                                                                            //reset for quick filter clears fields and restores default settings
                                                                            //table brings up orders that correspond with given criteria
        me.activities_Tab_Is_Highlighted();                                 //To check Activity tab is highlighted
        me.check_Given_Criteria_Case_Activity_Object();                     //can create a case from here
        me.check_Given_Criteria_Task_Activity_Object();                     //can create a memo from here
        me.check_Given_Criteria_Memo_Activity_Object();                     //can create a meo from here
        me.navigate_To_New_Patient();                                       //To navigate order adn patients page (Order list)
        me.activities_Tab_Is_Highlighted();                                 //To check navigate Activity  tab
        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.orderActivityPagingDiv); // can navigate through the pages if there are multiple pages
    });
}
exports.general_Information_Activity = general_Information_Activity;

var patient_ActivitiesTab_CustomList = function () {
    describe("PATIENT ACTIVITIES TAB CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_Patient_Activity_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.quickFilterLinkOnGeneralInfoSubTab,
            elements.ordersandPatientsPageElements.orderActivityTable,
            elements.ordersandPatientsPageElements.orderActivityFilterAvailColumns,
            elements.ordersandPatientsPageElements.orderActivityFilterSelectedColumns,
            elements.ordersandPatientsPageElements.orderActivityFilterAddColBtn,
            elements.ordersandPatientsPageElements.orderActivityFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.orderActivityFilterApplyBtn);    //apply button applies the filter to the table
                                                                                    //filters just build upon the custom lists
                                                                                    //can add and remove columns from the display

        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.orderSettingIcon,
            elements.ordersandPatientsPageElements.orderOrgCustomListNewLink,
            elements.ordersandPatientsPageElements.orderOrderCustomListLink,
            elements.ordersandPatientsPageElements.orderPageNewListInput,
            elements.ordersandPatientsPageElements.orderPageSaveColBtn,
            elements.ordersandPatientsPageElements.orderOrderCustomListDiv,
            elements.ordersandPatientsPageElements.orderOrderCustomListCloseBtn,
            elements.ordersandPatientsPageElements.orderPageOrderAddColumnBtn,
            elements.ordersandPatientsPageElements.orderPageOrderRemoveColBtn,
            elements.ordersandPatientsPageElements.orderPageOrderAvailableColumns,
            elements.ordersandPatientsPageElements.orderPageOrderSelectedColumns,
            elements.ordersandPatientsPageElements.orderActivityTable);            //Create new custom list

        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.orderSettingIcon,
            elements.ordersandPatientsPageElements.orderCloneCustomListInput,
            elements.ordersandPatientsPageElements.orderCloneCustomListLink,
            elements.ordersandPatientsPageElements.orderCloneCustomListCreateBtn,
            elements.ordersandPatientsPageElements.orderOrderCustomListLink,
            elements.ordersandPatientsPageElements.orderOrderCustomListDiv,
            elements.ordersandPatientsPageElements.orderOrderCustomListCloseBtn);  // clone custom list
    });
};
exports.patient_ActivitiesTab_CustomList = patient_ActivitiesTab_CustomList;

var general_Information_Order = function () {
    describe("GENERAL INFO - ORDERS", function () {
        // organizationOperations.create_New_Org();
        // me.create_Patient();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_OrderSubTab();                   //To navigate Order Sub Tab and check activity tab is highlighted
        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.quickFilterLinkOnOrderSubTab,
            elements.ordersandPatientsPageElements.orderSubTabCustomListTable,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterAvailableColumn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterSelectedColumn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterAddColBtn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.orderSubTabQuickFilterApplyBtn);  //quick filter brings down quick filter menu

        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.orderSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.orderSubTabCustomListNewLink,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.orderSubTabNewListInput,
            elements.ordersandPatientsPageElements.orderSubTabCustomListSaveColBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderCloseBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderAddBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListOrderRemoveBtn,
            elements.ordersandPatientsPageElements.orderSubTabCustomListAvailableColumns,
            elements.ordersandPatientsPageElements.orderSubTabCustomListSelectedColumns,
            elements.ordersandPatientsPageElements.orderSubTabCustomListTable);   //Create new custom list


        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.orderSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.orderOrgCustomListCloneInput,
            elements.ordersandPatientsPageElements.orderOrgCustomListCloneLink,
            elements.ordersandPatientsPageElements.orderOrgCustomListCloneCreateBtn,
            elements.ordersandPatientsPageElements.orderOrgCustomListOrderLink,
            elements.ordersandPatientsPageElements.orderOrgCustomListOrderDiv,
            elements.ordersandPatientsPageElements.orderOrgCustomListOrderCloseBtn);// clone custom list


        me.quickFilter_On_Order_SubTab_DownLink();   //quick filter brings down quick filter menu
                                                     // Quick filter has same stuff as order search quick filter section

        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.orderSubTabActivityPagingDiv); // can navigate through the pages if there are multiple pages

    });
}
exports.general_Information_Order = general_Information_Order;

var general_Information_Message = function () {
    describe("GENERAL INFORMATION - MESSAGE SUBTAB", function () {
         // organizationOperations.create_New_Org();
         // me.create_Patient();                     //For creating new patients
        me.pauseBrowserSomeTime();
         me.navigate_To_New_Patient();
         me.check_General_info_Tab_Highlighted(); //general information tab is highlighted
         me.navigate_To_MessageSubTab();          //Navigate to message sub tab on order page

        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.quickFilterLinkOnMessageOrderPage,
            elements.ordersandPatientsPageElements.messageSubTabCustomListTable,
            elements.ordersandPatientsPageElements.messageSubTabQuickFilterAvailableColumn,
            elements.ordersandPatientsPageElements.messageSubTabQuickFilterSelectedColumn,
            elements.ordersandPatientsPageElements.messageSubTabQuickFilterAddColBtn,
            elements.ordersandPatientsPageElements.messageSubTabQuickFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.messageSubTabQuickFilterApplyBtn);


        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.messageSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.messageSubTabCustomListNewLink,
            elements.ordersandPatientsPageElements.messageSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.messageSubTabNewListInput,
            elements.ordersandPatientsPageElements.messageSubTabCustomListSaveColBtn,
            elements.ordersandPatientsPageElements.messageSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.messageSubTabCustomListOrderCloseBtn,
            elements.ordersandPatientsPageElements.messageSubTabCustomListAddColBtn,
            elements.ordersandPatientsPageElements.messageSubTabCustomListRemoveColBtn,
            elements.ordersandPatientsPageElements.messageSubTabCustomListAvailableColumns,
            elements.ordersandPatientsPageElements.messageSubTabCustomListSelectedColumns,
            elements.ordersandPatientsPageElements.messageSubTabCustomListTable);  //Create new custom list


        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.messageSubTabSettingsIcon,
            elements.ordersandPatientsPageElements.messageOrgCustomListCloneInput,
            elements.ordersandPatientsPageElements.messageOrgCustomListCloneLink,
            elements.ordersandPatientsPageElements.messageOrgCustomListCloneCreateBtn,
            elements.ordersandPatientsPageElements.messageOrgCustomListOrderLink,
            elements.ordersandPatientsPageElements.messageOrgCustomListOrderDiv,
            elements.ordersandPatientsPageElements.messageOrgCustomListOrderCloseBtn);  // Can clone old custom lists

        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.messageSubTabActivityPagingDiv);


        me.quickFilter_On_Message_SubTab_DownLink();
    });
}
exports.general_Information_Message = general_Information_Message;

var general_Information_Campaign = function () {
    describe("GENERAL INFORMATION - CAMPAIGN SUBTAB", function () {
        // organizationOperations.create_New_Org();
        // me.create_Patient();                              //For creating new patients
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.check_General_info_Tab_Highlighted();          //general information tab is highlighted
        me.check_Campaign_General_info_Tab_Highlighted();
        me.reorder_CustomList_Order();                    //Can use custom lists to sort table
                                                          //Can reorder the custom lists
        me.check_Campaign_Sortable();

    });
}
exports.general_Information_Campaign = general_Information_Campaign;

var patient_CampaignSubtab_Customlist = function () {
    describe("PATIENT CAMPAIGN SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Patient();
        me.navigate_To_Campaign_SubTab();
        commonOperations.check_Filters_Behave_Properly(elements.ordersandPatientsPageElements.quickFilterLinkOnCampaignOrderPage,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListTable,
            elements.ordersandPatientsPageElements.campaignSubTabQuickFilterAvailableColumn,
            elements.ordersandPatientsPageElements.campaignSubTabQuickFilterSelectedColumn,
            elements.ordersandPatientsPageElements.campaignSubTabQuickFilterAddColBtn,
            elements.ordersandPatientsPageElements.campaignSubTabQuickFilterRemoveColBtn,
            elements.ordersandPatientsPageElements.campaignSubTabQuickFilterApplyBtn); //can add and remove columns from the display
        //quick filter brings down quick filter menu

        commonOperations.create_New_Custom_List(elements.ordersandPatientsPageElements.patientDetailPageCampaignSettingsIcon,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListNewLink,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.campaignSubTabNewListInput,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListSaveColBtn,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderCloseBtn,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderAddBtn,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderRemoveBtn,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListAvailableColumns,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListSelectedColumns,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListTable); //Create new custom list

        commonOperations.clone_Custom_List(elements.ordersandPatientsPageElements.patientDetailPageCampaignSettingsIcon,
            elements.ordersandPatientsPageElements.campaignOrgCustomListCloneInput,
            elements.ordersandPatientsPageElements.campaignOrgCustomListCloneLink,
            elements.ordersandPatientsPageElements.campaignOrgCustomListCloneCreateBtn,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderLink,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderListDiv,
            elements.ordersandPatientsPageElements.campaignSubTabCustomListOrderCloseBtn);// clone custom list

        commonOperations.check_paging_display(elements.ordersandPatientsPageElements.campaignSubTabActivityPagingDiv);
    });
};
exports.patient_CampaignSubtab_Customlist = patient_CampaignSubtab_Customlist;

var patients_Sheet = function () {
    describe("RESOLVE PATIENT SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'createdORG',
            'createdPatient',
        ]);
        organizationOperations.create_New_Org();
        me.create_Patient();
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS


        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        me.new_Patients();
        me.orders_And_Patients_Tab();
        me.patient_Search();
        me.patient_AddressTab();
        me.patient_OrganizationTab();
        me.order_list();
        me.general_Information_Basic();
        me.general_Information_Activity();
        me.general_Information_Campaign();

        me.patient_OrganizationTab_CustomList();
        me.patient_OrdersSubtab_CustomList();
        me.patient_ActivitiesTab_CustomList();
        me.general_Information_Order();
        me.general_Information_Message();
        me.patient_CampaignSubtab_Customlist();
        //SHEET EXECUTION - ENDS

    });
};
exports.patients_Sheet = patients_Sheet;

//FUNCTIONS DECLARATION - END



