/**
 * Created by INDUSA
 */
require('mocha');
var moment = require('moment');
var expect = require('chai').expect;
var assert = require('assert');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var variables = require('../utility/variables.js');
var commonOperations = require('../pages/commons.js');
var userAuthentication = require('../pages/userAuthentication.js');
var orgOperations = require('../pages/organization.js');
var activityOperations = require('../pages/activities.js');
var contactsOperations = require('../pages/contacts.js');
var navigate = require('../utility/navigate.js');
var me = require('./admin.js');
var diagnosisDelColIndex = 0;
var diagnosisCodeColIndex = 0;


//Test cases - start

var navigate_To_Admin_Page = function () {
    describe("Navigate to Admin page",function () {
        it("should navigate to admin page", function (browser) {
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(3000);
        });
    });
};
exports.navigate_To_Admin_Page = navigate_To_Admin_Page;

var navigate_RelationShip_Management_Page = function () {
    describe("Navigate RelationShip Management ", function () {
        it("Should check - Navigate RelationShip Management ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.relationshipManagementTAb);
            browser.pause(2000);
        });
    });
}
exports.navigate_RelationShip_Management_Page = navigate_RelationShip_Management_Page;

var navigate_To_HRMAccessControl_Page = function () {
    describe("Navigate to Admin page",function () {
        it("should navigate to admin page", function (browser) {
            me.navigate_To_Admin_Page();
            commands.checkAndPerform('click',browser,elements.adminPageElements.hrmAccessControlLink);
            browser.pause(3000);
        });
    });
};
exports.navigate_To_HRMAccessControl_Page = navigate_To_HRMAccessControl_Page;

var create_New_Control_Tree = function () {
    describe("Create a new Access Control Tree",function () {
        it("should create a new Access Control Tree", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerMenu);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.hrmAccessControlLink);
            browser.pause(2000);

            // Admin Node appears in New Environment
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.adminControlTreeLink).to.be.present;

            // Admin Node Users can Create New Node
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.createControlTreeLink).to.be.present;

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.createControlTreeLink);
            browser.pause(3000);
            variables.controlTreeNameCreated = variables.newControlTreeName;
            variables.newRootNodeName = variables.newControlTreeName+"_rootNode";
            commands.checkAndSetValue(browser,elements.adminHRMAccessControlPageElements.controlTreeNameInput,variables.newControlTreeName);
            commands.checkAndPerform('click',browser,elements.adminHRMAccessControlPageElements.controlTreeSalesTerritoryCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminHRMAccessControlPageElements.controlTreeSaveBtn);
            commands.checkAndPerform('clearValue',browser,elements.adminHRMAccessControlPageElements.controlNodeRootNodeNameInput);
            commands.checkAndSetValue(browser,elements.adminHRMAccessControlPageElements.controlNodeRootNodeNameInput,variables.newRootNodeName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminHRMAccessControlPageElements.controlNodeAddUserInput,variables.firstname);
            browser.pause(4000);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.keys([browser.Keys.ENTER]);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminHRMAccessControlPageElements.controlNodeSaveBtn);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.adminHRMAccessControlPageElements.controlNodeAddChildLink);
            variables.newChildNodeName = variables.newControlTreeName+"_childNode";
            commands.checkAndSetValue(browser,elements.adminHRMAccessControlPageElements.controlNodeChildNameInput,variables.newChildNodeName);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.adminHRMAccessControlPageElements.controlNodeChildSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.notificationLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.hrmAccessControlLink);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.controlTreeTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.controlTreeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.controlTreeNameCreated)).to.be.true;
                        variables.dependencyFulfilled.push('controlTreeNameCreated');
                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_New_Control_Tree = create_New_Control_Tree;

var check_User_Admin_Role = function () {
    describe("User Admin Role",function () {
        it("should check - Admin User Role",function (browser) {
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(3000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.userAdministrationLink);
            browser.pause(5000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.usernameLink);
            browser.pause(5000);
            browser.expect.element(elements.adminPageElements.adminRightsCheckBox).to.have.attribute('checked').which.equals('true');
            browser.pause(3000);
        });
    });
};
exports.check_User_Admin_Role = check_User_Admin_Role;

var create_Category = function () {
    describe("Create a new category", function () {
        it("should create a new category", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.actCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if(!text.value.includes(variables.newCategoryName))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath",elements.adminPageElements.actCategoryTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdActivityCategoryName = variables.newCategoryName;
                                commands.checkAndSetValue(browser,elements.adminPageElements.actCategoryTable+'/tbody/tr['+categoriesCount+']/td[1]/div/input',variables.newCategoryName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategorySaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.actCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdActivityCategoryName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.actCategoryTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdActivityCategoryName)){
                            console.log('=====> Activity category created successfully : '+variables.createdActivityCategoryName);
                        }
                    });
                });
            });


            //check if it already exists
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.actCategoryTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.actCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdActivityCategoryName)).to.be.true;
                        variables.dependencyFulfilled.push('createdActivityCategoryName');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_Category = create_Category;

var create_SubCategory = function () {
    describe("Create a new sub category", function () {
        it("should create a new sub category", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategoryTab);
            browser.pause(1000);

            //check whether subcategory already exists - START
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.activitySubCategoryFieldInput))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategoryAddLink);
                            var subcategoriesCount = 0;
                            browser.elements("xpath",elements.adminPageElements.actSubCategoryTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    subcategoriesCount = subcategoriesCount + 1;
                                });
                                variables.createdSubActivityCategoryName = variables.activitySubCategoryFieldInput;
                                variables.createdSubActivityCategoryDesc = variables.activitySubCategoryDescInput;
                                commands.checkAndSetValue(browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+(subcategoriesCount)+']/td[1]/div/input',variables.activitySubCategoryFieldInput);
                                browser.keys(browser.Keys.TAB);
                                commands.checkAndSetValue(browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+(subcategoriesCount)+']/td[2]/textarea', variables.activitySubCategoryDescInput);
                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategorySaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.activitySubCategoryFieldInput+'")]').to.be.present;
                        }
                    });
                });
            });
            //check whether subcategory already exists - END
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.actSubCategoryTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdSubActivityCategoryName)).to.be.true;
                        variables.dependencyFulfilled.push('createdSubActivityCategoryName');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_SubCategory = create_SubCategory;

var create_Stage = function () {
    describe("Create a new stage", function () {
        it("should create a new stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.opportunityStageLink);
            browser.pause(2000);

            //check whether stage already exists - START
            browser.elements("xpath", elements.adminPageElements.opporStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.opporStageFieldInput))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.opporStageCreateLink);
                            var opporStageCount = 0;
                            browser.elements("xpath",elements.adminPageElements.opporStageTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    opporStageCount = opporStageCount + 1;
                                });
                                browser.pause(1000);
                                variables.createdOpporStage = variables.opporStageFieldInput;
                                commands.checkAndSetValue(browser,elements.adminPageElements.opporStageTable+'/tbody/tr['+opporStageCount+']/td[1]/div/input', variables.opporStageFieldInput);
                                commands.checkAndSetValue(browser,elements.adminPageElements.opporStageTable+'/tbody/tr['+opporStageCount+']/td[1]/div/input', variables.opporStageFieldInput);
                                browser.pause(1000);
                                commands.checkAndSetValue(browser, elements.adminPageElements.opporStageTable+'/tbody/tr['+opporStageCount+']/td[3]/input', variables.opporStageProbablity);
                                browser.pause(1000);
                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.opporStageSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.opporStageFieldInput+'")]').to.be.present;
                            browser.pause(2000);

                        }
                    });
                });
            });
            //check whether stage already exists - END


            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.opporStageTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.opporStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdOpporStage)).to.be.true;
                        variables.dependencyFulfilled.push('createdOpporStage');
                    });
                });
            });
            //assert and push value to dependency fulfilled list


            browser.pause(2000);
        });
    });
};
exports.create_Stage = create_Stage;

var create_Specialty = function () {
    describe("Create a new Specialty", function () {
        it("should create a new specialty", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.specialityLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.specialtyCreateLink);
            browser.pause(2000);

            //check whether specialty already exists - START
            browser.elements("xpath", elements.adminPageElements.specialtyTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.specialtyFieldInput))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtyCreateLink);
                            var specialtyCount = 0;
                            browser.elements("xpath",elements.adminPageElements.specialtyTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    specialtyCount = specialtyCount + 1;
                                });
                                browser.pause(1000);
                                variables.createdSpecialty = variables.specialtyFieldInput;
                                var revenuePerOrder = variables.createdSpecialtyRevenuePerOrder = variables.specialtyRevenuePerOrder;
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr['+specialtyCount+']/td[1]/div/input', variables.specialtyFieldInput);
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr['+specialtyCount+']/td[2]/input', revenuePerOrder);
                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtySaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.specialtyFieldInput+'")]').to.be.present;
                        }
                    });
                });
            });
            //check whether specialty already exists - END
            browser.pause(2000);
        });
    });
};
exports.create_Specialty = create_Specialty;

var create_EMR = function () {
    describe("Create a new EMR", function () {
        it("should create a new EMR", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.emrLink);
            browser.pause(2000);

            //check whether specialty already exists - START
            browser.elements("xpath", elements.adminPageElements.emrTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.emrNameInput))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.emrAddLink);
                            var emrCount = 0;
                            browser.elements("xpath",elements.adminPageElements.emrTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    emrCount = emrCount + 1;
                                });
                                browser.pause(1000);
                                variables.createdEMR = variables.emrNameInput;
                                commands.checkAndSetValue(browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr['+emrCount+']/td[1]/div/input', variables.emrNameInput);
                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.emrSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.emrTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdEMR)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.emrTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdEMR)){
                            console.log('=====> EMR created successfully : '+variables.createdEMR);
                        }
                    });
                });
            });
            //check whether specialty already exists - END
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.emrTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.emrTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdEMR)).to.be.true;
                        variables.dependencyFulfilled.push('createdEMR');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_EMR = create_EMR;

var create_FeeSchedule = function () {
    describe("Create a Fee schedule", function () {
        it("should create a fee schedule", function (browser) {
            if(variables.createdPanel === ''){
                throw new Error("Panel needed for creating fee schedule");
            }
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.panelsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.feeScheduleAdmin);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.feeScheduleLinkSchedule);
            browser.pause(2000);
            variables.createdFeeSchedule = variables.nameFeesSchedule;
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.nameInputFeeSchedule, variables.nameFeesSchedule);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.descriptionInputFeeSchedule, variables.descriptionFeesSchedule)
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createBtnFeeSchedule);
            browser.pause(5000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdFeeSchedule+"']");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeScheduleOpportunitySearchableCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.addPanel);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.panelsAdminSectionElements.panelNameInput, variables.createdPanel);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.panelsAdminSectionElements.panelPriceInput, variables.panelPriceValue);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.createBtnPanel);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.saveBtnPanelsCreatedPage);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.panelsAdminSectionElements.feeScheduleTable).to.be.present;
            browser.elements("xpath", elements.panelsAdminSectionElements.feeScheduleTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdFeeSchedule)).to.be.true;
                        variables.dependencyFulfilled.push('createdFeeSchedule');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
module.exports.create_FeeSchedule = create_FeeSchedule;

var create_CampaignType = function () {
    describe("Create a new campaign type", function () {
        it("should create a new campaign type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);

            //check whether specialty already exists - START
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.campaignTypeNameInput))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeAddLink);
                            var campaignTypeCount = 0;
                            browser.elements("xpath",elements.adminPageElements.campaignTypeTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    campaignTypeCount = campaignTypeCount + 1;
                                });
                                browser.pause(1000);
                                variables.createdCampaignType = variables.campaignTypeNameInput;
                                commands.checkAndSetValue(browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignTypeCount+']/td[1]/div/input', variables.campaignTypeNameInput);
                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignType+'")]').to.be.present;
                        }
                    });
                });
            });
            //check whether specialty already exists - END

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.campaignTypeTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdCampaignType)).to.be.true;
                        variables.dependencyFulfilled.push('createdCampaignType');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_CampaignType = create_CampaignType;

var create_CampaignStage = function () {
    describe("Create a new campaign stage", function () {
        it("should create a new campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignStageTab);
            browser.pause(2000);

            //check whether specialty already exists - START
            browser.elements("xpath", elements.adminPageElements.campaignStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //create one if it doesnt already exists in the list
                        if(!text.value.includes(variables.campaignStageName))
                        {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageCreateLink);
                            var campaignStageCount = 0;
                            browser.elements("xpath",elements.adminPageElements.campaignStageTable+"/tbody/tr",  function  (result) {
                                var  els = result.value;
                                els.forEach(function  () {
                                    campaignStageCount = campaignStageCount + 1;
                                });
                                browser.pause(1000);
                                variables.createdCampaignStage = variables.campaignStageName;
                                if(campaignStageCount > 1){
                                    commands.checkAndSetValue(browser,elements.adminPageElements.campaignStageTable+'/tbody/tr['+campaignStageCount+']/td[1]/div/input', variables.campaignStageName);
                                }else{
                                    commands.checkAndSetValue(browser,elements.adminPageElements.campaignStageTable+'/tbody/tr/td[1]/div/input', variables.campaignStageName);
                                }

                            });
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignStage+'")]').to.be.present;
                        }
                    });
                });
            });
            //check whether specialty already exists - END

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.campaignStageTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.campaignStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdCampaignStage)).to.be.true;
                        variables.dependencyFulfilled.push('createdCampaignStage');
                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_CampaignStage = create_CampaignStage;

var performCampaignDependencyMapping = function () {
    describe("Dependency mapping", function () {
        it("Perform dependency mapping of campaign type and campaign stage", function (browser) {
            if(variables.createdCampaignType === ''){
                throw new Error('Campaign type required to perform dependency mapping');
            }
            if(variables.createdCampaignStage === ''){
                throw new Error('Campaign Stage required to perform dependency mapping');
            }
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignDependencyMappingSubtab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//label[contains(.,'"+variables.createdCampaignType+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//label[contains(.,'"+variables.createdCampaignStage+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignDependencyMappingSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.performCampaignDependencyMapping = performCampaignDependencyMapping;

var performCampaign_MultipleDependencyMapping = function () {
    describe("Dependency mapping", function () {
        it("Perform dependency mapping of campaign type and campaign stage", function (browser) {
            if(variables.createdCampaignType === ''){
                throw new Error('Campaign type required to perform dependency mapping');
            }
            if(variables.createdCampaignStage === ''){
                throw new Error('Campaign Stage required to perform dependency mapping');
            }
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignDependencyMappingSubtab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//label[contains(.,'"+variables.createdCampaignType+"')]");
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignDependencyStageTable+"/li", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[3]/div[2]/div[2]/ol/li['+counter+']');
                    if(counter === 1){
                        browser.elementIdText(el.ELEMENT, function (text) {
                            variables.campaignDependencyStage_First = text.value;
                        });
                    }
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignDependencyMappingSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.performCampaign_MultipleDependencyMapping = performCampaign_MultipleDependencyMapping;

var check_Create_User = function () {
    describe("Create a New User link", function () {
        it("should check create a new user link", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminCreateUserLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Create_User = check_Create_User;

var check_Email_All_Users_Never_Logged_On = function () {
    describe("Email all Users never logged on link", function () {
        it("should check email all Users never logged on link", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEmailAllUsersLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Email_All_Users_Never_Logged_On = check_Email_All_Users_Never_Logged_On;

var check_Edit_User = function () {
    describe("Edit user profile", function () {
        it("should edit user profile (for now pooja.mistry@indusa.com)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='"+variables.firstname1+" "+variables.lastname1+"']");
            browser.pause(2000);

            // User Edit > Roles
            browser.expect.element(elements.adminPageElements.userAdminEditUserRoles).to.be.present;
            browser.pause(2000);

            // User Edit > Insight Roles
            browser.expect.element(elements.adminPageElements.userAdminEditUserInsightRole).to.be.present;
            browser.pause(2000);

            // User Edit > Email
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserEmailInput).to.be.present;
            browser.pause(2000);

            // User Edit > First and Last Names respectively
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserAliasNameInput).to.be.present;
            browser.expect.element(elements.adminPageElements.userAdminEditUserFullNameInput).to.be.present;
            browser.pause(2000);

            // User Edit > SMS Number
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserSMSNumberInput).to.be.present;
            browser.pause(2000);

            // User Edit > LMS
            // User Edit > User Type
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserUserTypeDropdown).to.be.present;
            browser.pause(2000);

            // User Edit > User Setting Profile
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserSettingProfileDropdown).to.be.present;
            browser.pause(2000);

            // User Edit > Lens Profile
            // User Edit > Disable, Locked, Sales Rep respectively
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserDisabledCheckbox).to.be.present;
            browser.expect.element(elements.adminPageElements.userAdminEditUserLockedCheckbox).to.be.present;
            browser.pause(2000);

            // User Edit > Reassign Sales Rep's Account
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserReassignSalesRepBtn).to.be.present;
            browser.pause(2000);

            // User Edit > Access Control Nodes
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserAccessControlNodeInput).to.be.present;
            browser.pause(2000);

            // User Edit > Audit/Access Log that User Accessed/Modified
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserAccessLogLink).to.be.present;
            browser.pause(2000);

            // User Edit > Audit Log of User Modifications
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserAuditLogLink).to.be.present;
            browser.pause(2000);

        });
    });
};
exports.check_Edit_User = check_Edit_User;

var check_Authentication = function () {
    describe("2 factor authentication for user", function () {
        it("should check 2 factor authentication for the user", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserTwoFactorAuthDropdown).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserTwoFactorAuthDropdown);
            browser.pause(2000);
            browser.expect.element("//option[contains(@value,'Email')]").to.be.present;
            browser.pause(2000);
            browser.expect.element("//option[contains(@value,'SMS')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Authentication = check_Authentication;

var check_Lock_User_Acc = function () {
    describe("Lock user account", function () {
        it("should check lock user account", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserLockedCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Lock_User_Acc = check_Lock_User_Acc;

var check_Lock_User_Acc_Working = function () {
    describe("User account lock working", function () {
        it("should check user account lock working", function (browser) {
            browser.pause(2000);
            browser.url(variables.fullUrl,function() {
                browser.pause(3000);
                browser.expect.element(elements.loginPageElements.body).to.be.visible;
                browser.expect.element(elements.loginPageElements.usernameInput).to.be.visible;
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.usernameInput);
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.passwordInput);
                commands.checkAndSetValue(browser,elements.loginPageElements.usernameInput,variables.username1);
                commands.checkAndSetValue(browser,elements.loginPageElements.passwordInput,variables.password1);
                commands.checkAndPerform('click',browser,elements.loginPageElements.submitBtn);
                browser.pause(6000);
                browser.expect.element(elements.globalElements.resetPasswordDiv).to.be.present;
            });
            browser.pause(2000);
        });
    });
};
exports.check_Lock_User_Acc_Working = check_Lock_User_Acc_Working;

var check_Disable_User_Acc = function () {
    describe("Disable user account", function () {
        it("should check disable user account", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='"+variables.firstname1+" "+variables.lastname1+"']");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserDisabledCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.firstname1+", "+variables.lastname1+"')]").to.not.be.present;
            browser.pause(2000);

        });
    });
};
exports.check_Disable_User_Acc = check_Disable_User_Acc;

var check_Disable_User_Acc_Working = function () {
    describe("Disabled user account", function () {
        it("should check disabled user account working", function (browser) {
            browser.pause(2000);
            browser.url(variables.fullUrl,function() {
                browser.pause(3000);
                browser.expect.element(elements.loginPageElements.body).to.be.visible;
                browser.expect.element(elements.loginPageElements.usernameInput).to.be.visible;
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.usernameInput);
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.passwordInput);
                commands.checkAndSetValue(browser,elements.loginPageElements.usernameInput,variables.username1);
                commands.checkAndSetValue(browser,elements.loginPageElements.passwordInput,variables.password1);
                commands.checkAndPerform('click',browser,elements.loginPageElements.submitBtn);
                browser.pause(6000);
                browser.expect.element(elements.globalElements.disabledAccountAlertDiv).to.be.present;
            });
            browser.pause(2000);
        });
    });
};
exports.check_Disable_User_Acc_Working = check_Disable_User_Acc_Working;

var check_Headers_Sorting = function () {
    describe("Sort by table headers", function () {
        it("should check sorting byt table headers", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminNameHeader);
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminNameHeader).to.have.attribute('class').which.contain('sort-ascending');
            browser.pause(2000);
        });
    });
};
exports.check_Headers_Sorting = check_Headers_Sorting;

var reactivate_Account = function () {
    describe("Reactivate disabled account for further use", function () {
        it("should reactivate disabled account for further use", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminSearchUser, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserDisabledCheckbox);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userAdminEditUserLockedCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.reactivate_Account = reactivate_Account;

var check_User_ProfileTable = function () {
    describe("User profiles", function () {
        it("should check user profiles", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_User_ProfileTable = check_User_ProfileTable;

var create_New_User_Profile = function () {
    describe("New User Profile", function () {
        it("should create new user profile", function (browser) {
            browser.pause(2000);
            // New User Profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileNewProfileLink);

            // Add user profile name
            browser.pause(2000);
            variables.createdUserProfile = variables.newUserProfileName;
            commands.checkAndSetValue(browser,elements.adminPageElements.userProfileNameInput, variables.newUserProfileName);

            // Name and Description
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.userProfileDescInput, variables.newUserProfileDesc);

            // User type
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileUserTypeDropdown).to.be.present;

            // Default Activity Category
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileCategoryDropdown).to.be.present;

            // Lens Profile
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileCustomizationProfileDropdown).to.be.present;

            // 2-Factor Authentication Required checkbox
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileAuthenticationCheckbox).to.be.present;


            // Users Tab
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileUsersSubTab).to.be.present;
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileUsersSubTab);

            // Ensure that the users can Add user to the user profile
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileUsersSubTabAddUserInput).to.be.present;

            // Distribution Group Tab
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileDistributionGroupSubTab).to.be.present;
            commands.checkAndPerform('click', browser, elements.adminPageElements.userProfileDistributionGroupSubTab);

            // Ensure that the user can add distribution groups
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileAddDistributionGroupInput).to.be.present;

            // Visible Distribution Groups
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileVisibleDistributionGroupSubTab).to.be.present;
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileVisibleDistributionGroupSubTab);

            // Ensure that user can add visible distribution groups
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileAddVisibleDistributionGrpInput).to.be.present;

            // Notification Settings Tab
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileNotificationSettingsSubTab).to.be.present;
            commands.checkAndPerform('click', browser, elements.adminPageElements.userProfileNotificationSettingsSubTab);

            // Ensure that the user can add notification settings
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileAddNotificationSetting).to.be.present;

            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileRolesDiv+"/li[1]/label/input");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileSaveBtn);
            browser.pause(5000);

            browser.expect.element("//a[contains(@title,'"+variables.createdUserProfile+"')]").to.be.present;

            browser.pause(2000);
        });
    });
};
exports.create_New_User_Profile = create_New_User_Profile;

var disable_User_Profile = function () {
    describe("Disable user profile", function () {
        it("should disable newly created user profile", function (browser) {
            browser.pause(2000);
            var deleteColumnNumber = '';
            browser.elements("xpath", elements.adminPageElements.userProfileTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT, 'class',function (text) {
                        counter = counter + 1;
                        if(text.value.includes('delete')){
                            deleteColumnNumber = counter;
                        }
                    });
                });
            });
            browser.elements("xpath", elements.adminPageElements.userProfileTable+"/tbody/tr/td[2]/span/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdUserProfile)){
                            browser.pause(2000);
                            if(count > 1){
                                commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileTable+"/tbody/tr["+count+"]/td["+deleteColumnNumber+"]/i");
                            }else{
                                commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileTable+"/tbody/tr/td["+deleteColumnNumber+"]/i");
                            }
                            browser.pause(4000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdUserProfile+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.disable_User_Profile = disable_User_Profile;

var check_User_Profile_Headers_Sorting = function () {
    describe("User profile page table headers sorting", function () {
        it("should check user profile page table sorting",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userProfileNameHeader);
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userProfileNameHeader).to.have.attribute('class').which.contain('sort-ascending');
            browser.pause(2000);
        });
    });
};
exports.check_User_Profile_Headers_Sorting = check_User_Profile_Headers_Sorting;

var create_User_Role = function () {
    describe("Creating User Role", function () {
        it("should create user role", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesCreateNewRoleLink);
            browser.pause(2000);
            variables.createUserRole = variables.newRoleName;
            commands.checkAndSetValue(browser,elements.adminPageElements.userRolesRoleNameInput, variables.newRoleName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.userRolesRoleDescInput, variables.newRoleDesc);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesRoleSaveBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.createUserRole+"']").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_User_Role = create_User_Role;

var edit_User_Role = function () {
    describe("Edit User Role", function () {
        it("should edit user role", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createUserRole+"')]");
            browser.pause(2000);

            // Edit Role > Admin Permissions
            var adminPermissionName = '';
            browser.getText(elements.adminPageElements.userRolesAdminPermissionsDiv+"/li[1]/label",function (result) {
                adminPermissionName = result.value;
            });
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesAdminPermissionsDiv+"/li[1]/div/div/label/input");

            // Edit Role > Health care Insight Permissions
            browser.pause(2000);
            var healthcarePermissionName = '';
            browser.getText(elements.adminPageElements.userRolesHealthCareInsightPermissionsDiv+"/li[1]/label",function (result) {
                healthcarePermissionName = result.value;
            });
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesHealthCareInsightPermissionsDiv+"/li[1]/div/div/label/input");

            // Edit Role > HRM Permissions
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userRolesHRMPermissionsDiv).to.be.present;

            // Edit Role > User Access Control Permissions
            browser.pause(2000);
            var accessControlPermissionName = '';
            browser.getText(elements.adminPageElements.userRolesUserAccessControlPermissionsDiv+"/li[1]/label",function (result) {
                accessControlPermissionName = result.value;
            });
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesUserAccessControlPermissionsDiv+"/li[1]/div/div/label/input");

            // Edit Role > Integration Permissions
            browser.pause(2000);
            var integrationPermissionName = '';
            browser.getText(elements.adminPageElements.userRolesIntegrationPermissonsDiv+"/li[1]/label",function (result) {
                integrationPermissionName = result.value;
            });
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesIntegrationPermissonsDiv+"/li[1]/div/div/label/input");
            browser.pause(5000);
            browser.execute("scrollTo(20, 40)");
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRolesRoleSaveBtn);
            browser.pause(5000);

            browser.pause(2000);
        });
    });
};
exports.edit_User_Role = edit_User_Role;

var create_New_Distribution_Group = function () {
    describe("Create new distribution group", function () {
        it("should create a new dsitribution group", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.distributionGrpCreateLink);
            browser.pause(2000);
            variables.createdGroupName = variables.newGroupName;
            commands.checkAndSetValue(browser,elements.adminPageElements.distributionGrpNameInput, variables.newGroupName);
            commands.checkAndSetValue(browser,elements.adminPageElements.distributionGrpEmailInput, variables.newGroupEmail);
            commands.checkAndSetValue(browser,elements.adminPageElements.distributionGrpDescInput, variables.newGroupDesc);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.distributionGrpSaveBtn);
            browser.pause(4000);
            browser.expect.element("//a[@title='"+variables.createdGroupName+"']").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_New_Distribution_Group = create_New_Distribution_Group;

var add_Group_to_User_Profile = function () {
    describe("Add users to distribution group", function () {
        it("should add users to distribution group", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdGroupName+"']");
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.distributionGroupAddUserInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element("//a[contains(.,'"+variables.firstname1+" "+variables.lastname1+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.distributionGrpSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.add_Group_to_User_Profile = add_Group_to_User_Profile;

var check_Users_List = function () {
    describe("Distribution group list table", function () {
        it("should check distribution group table", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.distributionGrpTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Users_List = check_Users_List;

var assign_Activity_To_Group = function () {
    describe("Assign activity to this group", function () {
        it("should assign an activity to this distribution group", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.caseAssignedToInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseAssignedToInput, variables.createdGroupName);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.assign_Activity_To_Group = assign_Activity_To_Group;

var check_Notifications_To_Users = function () {
    describe("Activity notification", function () {
        it("should check activity notification", function (browser) {
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdCase+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Notifications_To_Users = check_Notifications_To_Users;

var create_New_NotificationSetting = function () {
    describe("New notification setting", function () {
        it("should create new notification setting", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingCreateLink);
            browser.pause(2000);
            variables.createdNotificationSetting = variables.newNotificationSettingName;
            commands.checkAndSetValue(browser,elements.adminPageElements.notifySettingNameInput, variables.newNotificationSettingName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingSaveBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdNotificationSetting+"')]").to.be.present;
        });
    });
};
exports.create_New_NotificationSetting = create_New_NotificationSetting;

var edit_NotificationSetting = function () {
    describe("Edit notification setting", function () {
        it("should update notification setting", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdNotificationSetting+"')]");
            browser.pause(2000);
            //select Record types
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingRecordTypes);
            browser.elements("xpath",elements.adminPageElements.notifySettingRecordTypes+"/option", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function () {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingRecordTypes+"/option["+counter+"]");
                    browser.pause(1000);
                });

            });
            browser.elements("xpath", elements.adminPageElements.notifySettingsRecordTypeTable+"/tbody/tr/td[1]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.satisfy(function (val) {
                            if(val === '' || val === 'Case' || val === 'Memo' || val === 'Opportunity' || val === 'Task')
                                return true;
                            else
                                return false;
                        });
                    });
                });
            });
            browser.pause(2000);

            //select condition update types
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingUpdateType);
            browser.elements("xpath",elements.adminPageElements.notifySettingUpdateType+"/option", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function () {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingUpdateType+"/option["+counter+"]");
                    browser.pause(1000);
                });

            });
            browser.elements("xpath", elements.adminPageElements.notifySettingsUpdateTypeTable+"/tbody/tr/td[1]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.satisfy(function (val) {
                            if(val === '' || val === 'Modified' || val === 'Reopened' || val === 'Subscribed' || val === 'Assigned' || val === 'Completed' || val === 'Claimed')
                                return true;
                            else
                                return false;
                        });
                    });
                });
            });
            browser.pause(2000);

            //select condition relationship types
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingRelationshipType);
            browser.elements("xpath",elements.adminPageElements.notifySettingRelationshipType+"/option", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function () {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingRelationshipType+"/option["+counter+"]");
                    browser.pause(1000);
                });

            });
            browser.elements("xpath", elements.adminPageElements.notifySettingsRelationshipTypeTable+"/tbody/tr/td[1]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.satisfy(function (val) {
                            if(val === '' || val === 'Creator' || val === 'Assignee' || val === 'Subscriber' || val === 'Group')
                                return true;
                            else
                                return false;
                        });
                    });
                });
            });
            browser.pause(2000);

            //select condition priority types
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingPriorityType);
            browser.elements("xpath",elements.adminPageElements.notifySettingPriorityType+"/option", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function () {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingPriorityType+"/option["+counter+"]");
                    browser.pause(1000);
                });

            });
            browser.elements("xpath", elements.adminPageElements.notifySettingsPriorityTypeTable+"/tbody/tr/td[1]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.satisfy(function (val) {
                            if(val === '' || val === 'Medium' || val === 'Low' || val === 'High')
                                return true;
                            else
                                return false;
                        });
                    });
                });
            });
            browser.pause(2000);

            //select condition delivery types
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingDeliveryType);
            browser.elements("xpath",elements.adminPageElements.notifySettingDeliveryType+"/option", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function () {
                    counter = counter + 1;
                    commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingDeliveryType+"/option["+counter+"]");
                    browser.pause(1000);
                });

            });
            browser.elements("xpath", elements.adminPageElements.notifySettingsDeliveryTypeTable+"/tbody/tr/td[1]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.satisfy(function (val) {
                            if(val === '' || val === 'HC1 Notification' || val === 'Email' || val === 'SMS')
                                return true;
                            else
                                return false;
                        });
                    });
                });
            });
            browser.pause(2000);
            browser.execute("scrollTo(20, 40)");
            commands.checkAndPerform('click', browser,elements.adminPageElements.notifySettingSaveBtn);
            browser.pause(4000);

        });
    });
};
exports.edit_NotificationSetting = edit_NotificationSetting;

var delete_NotificationSetting = function () {
    describe("Delete notification setting", function () {
        it("should delete notification setting", function (browser) {
            browser.pause(2000);
            var deleteColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.notifySettingsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath",elements.adminPageElements.notifySettingsTable+"/tbody/tr/td[2]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdNotificationSetting)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+count+']/td['+deleteColIndex+']/i');
                        }
                    });
                });
            });
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdNotificationSetting+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.delete_NotificationSetting = delete_NotificationSetting;

var create_ServerCredential = function () {
    describe("Create server credential", function () {
        it("should create server credential", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.serverCredentialsCreateLink);
            browser.pause(2000);
            variables.createdServer = variables.newServerName;
            commands.checkAndSetValue(browser,elements.adminPageElements.serverCredentialsNameInput, variables.newServerName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.serverCredentialsUsernameInput, variables.newServerUsername);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.serverCredentialsPwdInput, variables.newServerPwd);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.serverCredentialsRepeatPwdInput, variables.newServerPwd);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.serverCredentialsDescInput, variables.newServerDesc);
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.serverCredentialsCreateBtn);
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdServer+"')]").to.be.present;
        });
    });
};
exports.create_ServerCredential = create_ServerCredential;

var delete_ServerCredential = function () {
    describe("Delete server credential", function () {
        it("should delete server credential", function (browser) {
            browser.pause(2000);
            var deleteColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.serverCredentialsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.serverCredentialsTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count =0 ;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdServer)){
                            commands.checkAndPerform('click', browser, elements.adminPageElements.serverCredentialsTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdServer+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_ServerCredential = delete_ServerCredential;

var create_Second_Org = function () {
    describe("Create temporary second organization", function () {
        it("should create organization", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationCreateLink);
            browser.pause(2000);
            variables.createdORG2 = variables.newOrgName2;
            commands.checkAndSetValue(browser,elements.organizationPageElements.orgNameInput,variables.newOrgName2);
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.orgDetailPageSpecialtyDiv);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.orgRecordPageHeading).text.to.equal(variables.newOrgName2);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(5000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.organizationPageElements.orgListTable).to.be.present;
            browser.elements("xpath", elements.organizationPageElements.orgListTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdORG2)).to.be.true;
                        variables.dependencyFulfilled.push('createdORG2');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_Second_Org = create_Second_Org;

var merge_Both_Org = function () {
    describe("Merge two organizations", function () {
        it("should merge two organizations", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordOrgSearchNameInput, variables.createdORG);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsOrgSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordsOrgSearchResultTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdORG)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsOrgSearchResultTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);

            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.mergeRecordOrgSearchNameInput);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordOrgSearchNameInput, variables.createdORG2);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsOrgSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordsOrgSearchResultTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdORG2)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsOrgSearchResultTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsOrgSearchCompareBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsMergeBtn);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

        });
    });
};
exports.merge_Both_Org = merge_Both_Org;

var check_Merged_Org = function () {
    describe("Merged organization", function () {
        it("should check merged organization", function (browser) {
            browser.pause(2000);

            browser.pause(2000);
        });
    });
};
exports.check_Merged_Org = check_Merged_Org;

var search_Temporary_Org = function () {
    describe("Search for temporary organization", function () {
        it("should search for temporary organization", function (browser) {
            browser.pause(2000);
            var currentORG = variables.createdORG2;
            browser.pause(10000);
            commands.checkAndPerform('clearValue',browser,elements.homePageElements.cloudSearchInputField);
            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,currentORG);
            browser.pause(2000);
            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
            browser.elements("xpath", elements.homePageElements.cloudSearchAllTabLink, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.equal('0');
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.search_Temporary_Org = search_Temporary_Org;

var create_Temporary_Contact = function (type) {
    describe("Create temporary contact", function () {
        it("should create temporary contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(8000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.createContactLink);
            browser.pause(2000);
            if(type.toLowerCase() === 'provider') {
                commands.checkAndPerform('click', browser, elements.contactsPageElements.providerTypeInput);
                browser.pause(2000);
                variables.createdProvider2 = variables.providerFName2;
                commands.checkAndSetValue(browser, elements.contactsPageElements.firstNameInput, variables.providerFName2);
                commands.checkAndSetValue(browser, elements.contactsPageElements.lastNameInput, variables.providerLName2);
                commands.checkAndSetValue(browser, elements.contactsPageElements.organizationInput, variables.createdORG);
            }
            else{
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactTypeInput);
                browser.pause(2000);
                variables.createdContact2 = variables.contactFName2;
                commands.checkAndSetValue(browser, elements.contactsPageElements.firstNameInput, variables.contactFName2);
                commands.checkAndSetValue(browser, elements.contactsPageElements.lastNameInput, variables.contactLName2);
                commands.checkAndSetValue(browser, elements.contactsPageElements.organizationInput, variables.createdORG);
            }
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.orgSearchPopUp);
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.createLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsPageSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.contain(variables.createdProvider2);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.contactsPageElements.contactsTable).to.be.present;
            browser.elements("xpath", elements.contactsPageElements.contactsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(type.toLowerCase() === 'provider'){
                            expect(text.value.includes(variables.createdProvider2)).to.be.true;
                            variables.dependencyFulfilled.push('createdProvider2');
                        }
                        else{
                            expect(text.value.includes(variables.createdContact2)).to.be.true;
                            variables.dependencyFulfilled.push('createdContact2');
                        }
                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_Temporary_Contact = create_Temporary_Contact;

var search_Temporary_Contact = function () {
    describe("Search for temporary contact", function () {
        it("should Search for temporary contact", function (browser) {
            browser.pause(2000);
            var currentCon = variables.createdContact2;
            browser.pause(10000);
            commands.checkAndPerform('clearValue',browser,elements.homePageElements.cloudSearchInputField);
            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,currentCon);
            browser.pause(2000);
            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
            browser.elements("xpath", elements.homePageElements.cloudSearchAllTabLink, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.equal('0');
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.search_Temporary_Contact = search_Temporary_Contact;

var merge_Both_Contact = function () {
    describe("Merge two contacts", function () {
        it("should merge two contacts", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordContactSearchFNameInput, variables.createdContact);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordContactSearchTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdContact)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactSearchTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);

            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.mergeRecordContactSearchFNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordContactSearchFNameInput, variables.createdContact2);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordContactSearchTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdContact2)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactSearchTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactCompareBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordContactMergeBtn);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

        });
    });
};
exports.merge_Both_Contact = merge_Both_Contact;

var merge_Both_Provider = function () {
    describe("Merge two Providers", function () {
        it("should merge two providers", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordProviderFNameInput, variables.createdProvider);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordProviderSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordsProviderSearchTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProvider)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsProviderSearchTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);

            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.mergeRecordProviderFNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.mergeRecordProviderFNameInput, variables.createdContact2);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordProviderSearchBtn);
            browser.pause(4000);
            browser.elements("xpath", elements.adminPageElements.mergeRecordsProviderSearchTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProvider2)){
                            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordsProviderSearchTable+"/tbody/tr["+count+"]/td[1]/input");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordProviderCompareBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.mergeRecordProviderMergeBtn);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

        });
    });
};
exports.merge_Both_Provider = merge_Both_Provider;

var inactive_FeeSchedule = function () {
    describe("Inactivate fee schedule", function () {
        it("should inactivate fee schedule", function (browser) {

            browser.pause(3000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(2000);
            var deleteColIndex = 0;
            browser.elements("xpath",elements.panelsAdminSectionElements.feeScheduleTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.panelsAdminSectionElements.feeScheduleTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdFeeSchedule)){
                            if(count > 1){
                                commands.checkAndPerform('click', browser, "/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            }else{
                                commands.checkAndPerform('click', browser, "/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td["+deleteColIndex+"]/i");
                            }
                            browser.pause(4000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdFeeSchedule+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactive_FeeSchedule = inactive_FeeSchedule;

var reactivate_FeeSchedule = function () {
    describe("reactivate fee schedule", function () {
        it("should reactivate fee schedule", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeScheduleSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeScheduleNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.panelsAdminSectionElements.feeScheduleNewCustomListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.panelsAdminSectionElements.feeScheduleCustomListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeScheduleListFilterByFalseOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeScheduleListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.feeSchedulePagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdFeeSchedule+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.panelsAdminSectionElements.feeScheduleTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.panelsAdminSectionElements.feeScheduleTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdFeeSchedule)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,"/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdFeeSchedule+"')]").to.be.present;

        });
    });
};
exports.reactivate_FeeSchedule = reactivate_FeeSchedule;

var check_FeeSchedule_With_Org = function () {
    describe("Adding fee schedule in organization", function () {
        it("should add fee schedule in organization", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.feeSchedulesSubTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.organizationPageElements.searchFieldFeeSchedule, variables.createdFeeSchedule);
            browser.pause(2000);
            browser.elements("xpath", elements.organizationPageElements.orgFeeScheduleSearchResults, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdFeeSchedule);
                    });
                });
            });
        });
    });
};
exports.check_FeeSchedule_With_Org = check_FeeSchedule_With_Org;

var create_Diagnosis = function () {
    describe("Create diagnosis", function () {
        it("should create diagnosis", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisCreateLink);
            browser.pause(2000);
            variables.createdDiagnosis = variables.newDiagnosisCode;
            commands.checkAndSetValue(browser,elements.adminPageElements.diagnosisCodeInput, variables.newDiagnosisCode);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisCreateBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(3000);
            browser.expect.element("//a[contains(@title,'"+variables.createdDiagnosis+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Diagnosis = create_Diagnosis;

var inactivate_Diagnosis = function () {
    describe("Inactivate diagnosis", function () {
        it("Inactivate diagnosis", function (browser) {
            browser.pause(2000);
            var delColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.diagnosisTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var countdel = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        countdel = countdel + 1;
                        if(text.value){
                            if(text.value.includes('delete')){
                                delColIndex = countdel;
                                console.log('dele index : '+delColIndex);
                            }
                        }
                    });
                });
            });

            browser.elements("xpath",elements.adminPageElements.diagnosisTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdDiagnosis)){
                            console.log('text value returned : '+text.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.diagnosisTable+"/tbody/tr["+count+"]/td["+delColIndex+"]/i");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdDiagnosis+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Diagnosis = inactivate_Diagnosis;

var reactivate_Diagnosis = function () {
    describe("Reactivate diagnosis", function () {
        it("Reactivate diagnosis", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.diagnosisNewListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.adminPageElements.diagnosisListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisListFilterByFalseOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.diagnosisPagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdDiagnosis+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.adminPageElements.diagnosisTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.diagnosisTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdDiagnosis)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.diagnosisTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdDiagnosis+"')]").to.be.present;

        });

    });
};
exports.reactivate_Diagnosis = reactivate_Diagnosis;

var inactivate_Panel = function () {
    describe("Inactivate panel", function () {
        it("should inactivate created panel", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelsTabLink);
            browser.pause(2000);
            var delColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.panelTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var countdel = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        countdel = countdel + 1;
                        if(text.value){
                            if(text.value.includes('delete')){
                                delColIndex = countdel;
                                console.log('dele index : '+delColIndex);
                            }
                        }
                    });
                });
            });

            browser.elements("xpath",elements.adminPageElements.panelTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPanel)){
                            console.log('text value returned : '+text.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.panelTable+"/tbody/tr["+count+"]/td["+delColIndex+"]/i");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdPanel+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Panel = inactivate_Panel;

var reactivate_Panel = function () {
    describe("Reactivate panel", function () {
        it("should reactivate inactivated panel", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.panelNewListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.adminPageElements.panelListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelListFilterByFalseOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelPagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdPanel+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.adminPageElements.panelTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.panelTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPanel)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.panelTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdPanel+"')]").to.be.present;
        });
    });
};
exports.reactivate_Panel = reactivate_Panel;

var create_PanelType = function () {
    describe("Create panel type", function () {
        it("should create panel type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeCreateLink);
            browser.pause(2000);
            variables.createdPanelType = variables.newPanelTypeName;
            commands.checkAndSetValue(browser,elements.adminPageElements.panelTypeNameInput, variables.newPanelTypeName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeCreateBtn);
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdPanelType+"')]").to.be.present;
        });
    });
};
exports.create_PanelType = create_PanelType;

var inactivate_PanelType = function () {
    describe("Inactivate panel type", function () {
        it("should inactivate panel type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeTab);
            browser.pause(2000);
            var delColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.panelTypeTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var countdel = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        countdel = countdel + 1;
                        if(text.value){
                            if(text.value.includes('delete')){
                                delColIndex = countdel;
                                console.log('dele index : '+delColIndex);
                            }
                        }
                    });
                });
            });

            browser.elements("xpath",elements.adminPageElements.panelTypeTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPanelType)){
                            console.log('text value returned : '+text.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.panelTypeTable+"/tbody/tr["+count+"]/td["+delColIndex+"]/i");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdPanelType+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_PanelType = inactivate_PanelType;

var reactivate_PanelType = function () {
    describe("Reactivate panel type", function () {
        it("should reactivate panel type", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.panelTypeNewListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.adminPageElements.panelTypeListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeListFilterByFalseOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypeListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelTypePagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdPanelType+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.adminPageElements.panelTypeTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.panelTypeTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPanelType)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.panelTypeTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdPanelType+"')]").to.be.present;

        });
    });
};
exports.reactivate_PanelType = reactivate_PanelType;

var create_Procedures = function () {
    describe("create a procedure", function () {
        it("should create a procedure", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.procedureCreateLink);
            browser.pause(2000);
            variables.createdProcedure = variables.newProcedureCode;
            commands.checkAndSetValue(browser,elements.adminPageElements.procedureCodeInput, variables.newProcedureCode);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.procedureCreateBtn);
            browser.pause(3000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdProcedure+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Procedures = create_Procedures;

var inactivate_Procedures = function () {
    describe("Inactivate procedure", function () {
        it("should inactivate procedure", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresTab);
            browser.pause(2000);
            var delColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.procedureTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var countdel = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        countdel = countdel + 1;
                        if(text.value){
                            if(text.value.includes('delete')){
                                delColIndex = countdel;
                                console.log('dele index : '+delColIndex);
                            }
                        }
                    });
                });
            });

            browser.elements("xpath",elements.adminPageElements.procedureTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProcedure)){
                            console.log('text value returned : '+text.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.procedureTable+"/tbody/tr["+count+"]/td["+delColIndex+"]/i");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdProcedure+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Procedures = inactivate_Procedures;

var reactivate_Procedures = function () {
    describe("Reactivate procedure", function () {
        it("should reactivate procedure", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresNewListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.proceduresNewListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.adminPageElements.proceduresListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresListFilterByFalseOpt);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.proceduresPagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdProcedure+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.adminPageElements.procedureTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.procedureTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProcedure)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.procedureTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdProcedure+"')]").to.be.present;

        });
    });
};
exports.reactivate_Procedures = reactivate_Procedures;

var inactivate_Tests = function () {
    describe("Inactivate procedure", function () {
        it("should inactivate procedure", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsTabLink);
            browser.pause(2000);
            var delColIndex = 0;
            browser.elements("xpath", elements.adminPageElements.testsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var countdel = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        countdel = countdel + 1;
                        if(text.value){
                            if(text.value.includes('delete')){
                                delColIndex = countdel;
                                console.log('dele index : '+delColIndex);
                            }
                        }
                    });
                });
            });

            browser.elements("xpath",elements.adminPageElements.testsTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdTest)){
                            console.log('text value returned : '+text.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.testsTable+"/tbody/tr["+count+"]/td["+delColIndex+"]/i");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdTest+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Tests = inactivate_Tests;

var reactivate_Tests = function () {
    describe("Reactivate procedure", function () {
        it("should reactivate procedure", function (browser) {

            //Creating custom list with inactive list
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.testsNewListInput, variables.newCustomListName);
            browser.pause(2000);
            var equalToOptionIndex = 0;
            browser.elements("xpath",elements.adminPageElements.testsCustomListOperatorDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes('Equals')){
                            equalToOptionIndex = count;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsListFilterByFalseOpt);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsPagingLastBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTest+"')]").to.be.present;
            browser.pause(2000);


            //Reactivating created fee schedule
            var deleteColIndex = 0;
            browser.elements("xpath",elements.adminPageElements.testsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value.includes('delete')){
                            deleteColIndex = count;
                        }
                    });
                });
            });
            browser.elements("xpath",elements.adminPageElements.testsTable+"/tbody/tr/td[3]/span[2]/a", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdTest)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.testsTable+"/tbody/tr["+count+"]/td["+deleteColIndex+"]/i");
                            browser.pause(4000);
                        }
                    });
                });
            });
            browser.pause(4000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(4000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTest+"')]").to.be.present;

        });
    });
};
exports.reactivate_Tests = reactivate_Tests;

var create_Tests = function () {
    describe("Create a test", function () {
        it("should create a test", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsCreateLink);
            browser.pause(2000);
            variables.createdTest = variables.newTestName;
            commands.checkAndSetValue(browser,elements.adminPageElements.testsNameInput, variables.newTestName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.testsCreateBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTest+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Tests = create_Tests;

var create_newCustomList = function (object) {
    describe("Create new custom list", function () {
        it("should create a new custom list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customListLink);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListTypeDropdown);
            browser.pause(5000);
            browser.elements("xpath", elements.adminPageElements.customizationsListTypeDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value === (object)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsListTypeDropdown+"/option["+count+"]");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsNewCustomListBtn);
            browser.pause(2000);
            variables.createdList = variables.newListName;
            commands.checkAndSetValue(browser,elements.adminPageElements.customizationsListNameInput, variables.newListName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListViewDropdown);
            browser.expect.element("//option[contains(.,'"+variables.createdList+"')]").to.be.present;
            browser.getText("//option[contains(.,'"+variables.createdList+"')]",function (re) {
               console.log('=====> Created list name : '+re.value);
            });
            browser.pause(2000);
        });
    });
};
exports.create_newCustomList = create_newCustomList;

var check_newCustomListOption = function () {
    describe("Verify new custom list in UI", function () {
        it("should verify new custom list in UI", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseActSubTabCustomListDropdown);
            browser.pause(5000);
            browser.elements("xpath", elements.activityPageElements.caseActSubTabCustomListDropdown, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        console.log(text.value.trim());
                        expect(text.value.trim().includes(variables.createdList));
                    });
                });
            });
            // browser.expect.element("//option[contains(.,'"+variables.createdList+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_newCustomListOption = check_newCustomListOption;

var edit_newCustomList = function (object) {
    describe("Edit new created custom list", function () {
        it("should edit new created custom list", function (browser) {
            browser.refresh();
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListTypeDropdown);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.customizationsListTypeDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value === (object)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsListTypeDropdown+"/option["+count+"]");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListViewDropdown);
            browser.pause(2000);
            browser.getText("//option[contains(.,'"+variables.createdList+"')]",function (re) {
               console.log('=====> custom list to be edited : '+re.value);
            });
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdList+"')]");
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.adminPageElements.customizationsListNameInput);
            browser.pause(2000);
            variables.createdList = variables.newListNameUpdated;
            commands.checkAndSetValue(browser,elements.adminPageElements.customizationsListNameInput, variables.newListNameUpdated);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListViewDropdown);
            browser.expect.element("//option[contains(.,'"+variables.createdList+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.edit_newCustomList = edit_newCustomList;

var delete_newCustomList = function (object) {
    describe("delete a custom list", function () {
        it("should delete created custom list", function (browser) {
            browser.refresh();
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListTypeDropdown);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.customizationsListTypeDropdown+"/option", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value === (object)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsListTypeDropdown+"/option["+count+"]");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);

            commands.checkAndPerform('click', browser,elements.adminPageElements.customizationsListViewDropdown);
            browser.pause(2000);
            browser.getText("//option[contains(.,'"+variables.createdList+"')]",function (re) {
                console.log('=====> custom list to be deleted : '+re.value);
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdList+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsDeleteBtn);
            browser.pause(2000);
            browser.elements("xpath",elements.adminPageElements.customizationsListViewDropdown+"/option", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdList)).to.be.false;
                    });
                });
            });
        });
    });
};
exports.delete_newCustomList = delete_newCustomList;

var create_Panel = function () {
    describe("To Add Panel From grid Fees Schedule", function () {
        it("Should be check- To Add Panele grid From Fees Schedule", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerMenu);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.panelsTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createPanels);
            browser.pause(3000);
            variables.createdPanel = variables.panelsNameValue;
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.panelsNameInput, variables.panelsNameValue);
            browser.pause(3000);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.panelsDescriptionInput, variables.panelsDescriptionValue);
            browser.pause(2000);
            browser.moveToElement(elements.panelsAdminSectionElements.newPanelForm,2000,2000,function () {
                commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createBtnPanels);
            });
            browser.pause(3000);
            // commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdPanel+"']");
            // browser.pause(3000);
            // commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.panelPageEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.panelsAdminSectionElements.panelOpportunitySearchableCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.saveAndBackPanelsPage);
            browser.pause(3000);
            browser.expect.element(elements.panelsAdminSectionElements.toCheckNumberOfPanelList).to.have.value.not.equal('20');
            browser.pause(3000);


            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminPageElements.panelTable).to.be.present;
            browser.elements("xpath", elements.adminPageElements.panelTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdPanel)).to.be.true;
                        variables.dependencyFulfilled.push('createdPanel');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
module.exports.create_Panel = create_Panel;

var create_competitive_Lab = function () {
    describe("Add Competitive lab   ", function () {
        it("Should check - Add Competitive lab ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabBtn);
            browser.pause(2000);
            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.competitiveLabName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdCompetitiveLab = variables.competitiveLabName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.competitiveLabTableList+'/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.comapetitiveLabName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"' + variables.competitiveLabName + '")]').to.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.adminRelationshipManagementPageElements.competitiveLabTableList).to.be.present;
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdCompetitiveLab)).to.be.true;
                        variables.dependencyFulfilled.push('createdCompetitiveLab');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
}
exports.create_competitive_Lab = create_competitive_Lab;

var create_Category = function () {
    describe("Create a new category", function () {
        it("should create a new category", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.actCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.newCategoryName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.actCategoryTable + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdActivityCategoryName = variables.newCategoryName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.actCategoryTable + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.newCategoryName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategorySaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"' + variables.newCategoryName + '")]').to.be.present;
                            browser.perform(function () {
                                console.log('=====> Activity Category created successfully : '+variables.createdActivityCategoryName);
                            });
                        }
                    });
                });
            });

            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Category = create_Category;

var inactivate_category = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Activity Category', '', elements.adminPageElements.actCategoryTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.actCategoryTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdActivityCategoryName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryTable + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.actCategoryTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdActivityCategoryName)).to.be.false;
                        if(count >= els.length){
                            console.log('=====> Inactivated Activity category : '+variables.createdActivityCategoryName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_category = inactivate_category;

var reactivate_category = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdActivityCategoryName = variables.newCategoryName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityCategoryInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.elements.adminPageElements.actCategoryInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.elements.adminPageElements.actCategoryInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdActivityCategoryName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.elements.adminPageElements.actCategoryInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_category = reactivate_category;



var create_Activity_Sub_Category = function () {
    describe("Create a new category", function () {
        it("should create a new category", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategoryTab);
            browser.pause(2000);
            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.activitySubCategoryName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategoryAddLink);
                            var count = 0;
                            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable + "/tbody/tr", function (result) {
                                var count = 0;
                                var els = result.value;
                                els.forEach(function () {
                                    count = count + 1;
                                });
                                variables.createdActSubCategoryName = variables.activitySubCategoryName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.actSubCategoryTable + "/tbody/tr[" + count + "]/td[1]/div/input", variables.activitySubCategoryName);
                            });
                            console.log("name:" + variables.activitySubCategoryName);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.actSubCategorySaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdActSubCategoryName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdActSubCategoryName)){
                            console.log('=====> Activity sub category created successfully : '+variables.createdActSubCategoryName);
                        }
                    });
                });
            });
            //check if it already exists
            browser.pause(2000);
        });
    });
};
exports.create_Activity_Sub_Category = create_Activity_Sub_Category;

var inactivate_Activity_Sub_Category = function () {
    describe("", function () {
        it("inactivate category status", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Name', '', elements.adminPageElements.actSubCategoryTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdActSubCategoryName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.actSubCategoryTable + "/tbody/tr[" + count + "]/td[3]/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.actSubCategoryTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdActSubCategoryName)).to.be.false;
                        if(count >= els.length){
                            console.log('=====> Inactivated Activity sub category : '+variables.createdActSubCategoryName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Activity_Sub_Category = inactivate_Activity_Sub_Category;

var reactivate_Activity_Sub_Category = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdActivityCategoryName = 'testCategory_2017Apr19184658';
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.activitySubCategoryInactiveTableList + "/tbody/tr", function (result) {
                var els = result.value;                                                                                                // ['+counter+']
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.createdActSubCategoryName + "/tbody/tr[" + count + "]/td[1]/input", function (result) {
                        if (result.value.includes(variables.createdActivityCategoryStatusName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activitySubCategoryInactiveTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Activity_Sub_Category = reactivate_Activity_Sub_Category;

// -------------------------------------------------------------------------


var create_CategoryStatus = function () {
    describe("Create a new status", function () {
        it("should create a new status", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityStatusLink);
            browser.pause(2000);
            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.activityStatusTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.newCategoryStatusName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityStatusAddLink);
                            var Count = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.activityStatusTableList + "/tbody/tr", function (result) {
                                var Count = 0;
                                var els = result.value;
                                els.forEach(function () {
                                    Count = Count + 1;
                                });
                                variables.createdActivityCategoryStatusName = variables.newCategoryStatusName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.activityStatusTableList + "/tbody/tr[" + Count + "]/td[1]/div/input", variables.newCategoryStatusName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityStatusSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.activityStatusTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdActivityCategoryStatusName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.activityStatusTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdActivityCategoryStatusName)){
                            console.log('=====> Activity status created successfully : '+variables.createdActivityCategoryStatusName);
                        }
                    });
                });
            });
            //check if it already exists
            browser.pause(2000);
        });
    });
};
exports.create_CategoryStatus = create_CategoryStatus;

var inactivate_categoryStatus = function () {
    describe("", function () {
        it("inactivate category status", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Name', '', elements.adminRelationshipManagementPageElements.activityStatusTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.activityStatusTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdActivityCategoryStatusName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityStatusTableList + "/tbody/tr[" + count + "]/td[3]/i");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.activityStatusTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdActivityCategoryStatusName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Activity status : ' + variables.createdActivityCategoryStatusName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_categoryStatus = inactivate_categoryStatus;

var reactivate_categoryStatus = function () {
    describe("Reactivate category", function () {
        it("Should check - Reactivate category", function (browser) {
            variables.createdActivityCategoryName = variables.newCategoryStatusName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityStatusInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.activityStatusInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.activityStatusInactiveTable + "/tbody/tr[" + count + "]/td[1]/input", function (result) {
                        if (result.value.includes(variables.createdActivityCategoryStatusName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            console.log(" >>>>>>>>>>>>>>    if con status activity " + variables.createdActivityCategoryStatusName);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.activityStatusInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_categoryStatus = reactivate_categoryStatus;


var create_Contact_Type = function () {
    describe("Create a new category", function () {
        it("should create a new category", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.contactTypeLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.contactTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.contactTypeName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.contactTypebAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.contactTypeTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdContactTypeName = variables.contactTypeName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.contactTypeTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.contactTypeName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.contactTypeSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.contactTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdContactTypeName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.contactTypeTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdContactTypeName)){
                            console.log('=====> Contact type created successfully : '+variables.createdContactTypeName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Contact_Type = create_Contact_Type;

var inactivate_Contact_Type = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Contact Type', '', elements.adminRelationshipManagementPageElements.contactTypeTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.contactTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdContactTypeName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.contactTypeTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.contactTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdContactTypeName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Contact type : ' + variables.createdContactTypeName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Contact_Type = inactivate_Contact_Type;

var reactivate_Contact_Type = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdContactTypeName = variables.newCategoryName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.contactTypeInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.elements.adminRelationshipManagementPageElements.contactTypeInactiveTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.elements.adminRelationshipManagementPageElements.contactTypeInactiveTableList + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdContactTypeName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.elements.adminRelationshipManagementPageElements.contactTypeInactiveTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Contact_Type = reactivate_Contact_Type;

// -------------------------------------------------------------------------


var create_Opportunity_Type = function () {
    describe("Create a Opportunity type", function () {
        it("should create a Opportunity type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.opportunityTypeLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.opportunityTypeName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityTypeAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdOpprtunityTypeName = variables.opportunityTypeName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.opportunityTypeTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.opportunityTypeName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityTypeSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.opportunityTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdOpprtunityTypeName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.opportunityTypeTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdOpprtunityTypeName)){
                            console.log('=====> Opportunity type created successfully : '+variables.createdOpprtunityTypeName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Opportunity_Type = create_Opportunity_Type;

var inactivate_Opportunity_Type = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Opportunity Type', '', elements.adminRelationshipManagementPageElements.opportunityTypeTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdOpprtunityTypeName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.opportunityTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdOpprtunityTypeName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Opportunity type : ' + variables.createdOpprtunityTypeName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Opportunity_Type = inactivate_Opportunity_Type;

var reactivate_Opportunity_Type = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdOpprtunityTypeName = variables.opportunityTypeName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityTypeInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdOpprtunityTypeName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.elements.adminRelationshipManagementPageElements.opportunityTypeTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Opportunity_Type = reactivate_Opportunity_Type;

// -------------------------------------------------------------------------

var create_Corrective_Action = function () {
    describe("Create a Corrective Action", function () {
        it("should create a Corrective Action", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.correctiveActionLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.correctionActionTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.correctionActionName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.correctionActionAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.correctionActionTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdeCorrectionActionName = variables.correctionActionName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.correctionActionTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.correctionActionName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.correctionActionSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.correctionActionTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdeCorrectionActionName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.correctionActionTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdeCorrectionActionName)){
                            console.log('=====> Corrective action created successfully : '+variables.createdeCorrectionActionName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Corrective_Action = create_Corrective_Action;

var inactivate_Corrective_Action = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Corrective Action', '', elements.adminRelationshipManagementPageElements.correctionActionTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.correctionActionTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdeCorrectionActionName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.correctionActionTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.correctionActionTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdeCorrectionActionName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Corrective action : ' + variables.createdeCorrectionActionName);
                        }
                    });
                });
            });

            browser.pause(2000);
        });
    });
};
exports.inactivate_Corrective_Action = inactivate_Corrective_Action;

var reactivate_Corrective_Action = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdeCorrectionActionName = variables.correctionActionName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.correctionActionInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.elements.adminRelationshipManagementPageElements.correctionActionInactiveTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.elements.adminRelationshipManagementPageElements.correctionActionInactiveTableList + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdeCorrectionActionName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.elements.adminRelationshipManagementPageElements.correctionActionInactiveTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Corrective_Action = reactivate_Corrective_Action;

// -------------------------------------------------------------------------


var create_Root_Cause = function () {
    describe("Create a Corrective Action", function () {
        it("should create a Corrective Action", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.rootCauseLink);
            browser.pause(4000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.rootCauseTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.rootCauseName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.rootCauseAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.rootCauseTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdrootCauseName = variables.rootCauseName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.rootCauseTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.rootCauseName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.rootCauseSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.rootCauseTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdrootCauseName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.rootCauseTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdrootCauseName)){
                            console.log('=====> Root cause created successfully : '+variables.createdrootCauseName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Root_Cause = create_Root_Cause;

var inactivate_Root_Cause = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Root Cause', '', elements.adminPageElements.rootCauseTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.rootCauseTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdrootCauseName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.rootCauseTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.rootCauseTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdrootCauseName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Root cause : ' + variables.createdrootCauseName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Root_Cause = inactivate_Root_Cause;

var reactivate_Root_Cause = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdrootCauseName = variables.rootCauseName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.rootCauseInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.rootCauseInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.rootCauseInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdrootCauseName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.rootCauseInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Root_Cause = reactivate_Root_Cause;


// -------------------------------------------------------------------------


var create_Organization_Type = function () {
    describe("Create a Organization Type", function () {
        it("should create a Organization Type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.organizationTypeLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.organizationTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.organizationTypeName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.organizationTypeAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.organizationTypeTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdOrgTypeName = variables.organizationTypeName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.organizationTypeTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.organizationTypeName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.organizationTypeSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.organizationTypeTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdOrgTypeName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.organizationTypeTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdOrgTypeName)){
                            console.log('=====> Organization type created successfully : '+variables.createdOrgTypeName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Organization_Type = create_Organization_Type;

var inactivate_Organization_Type = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Organization Type', '', elements.adminRelationshipManagementPageElements.organizationTypeTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.organizationTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdOrgTypeName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.organizationTypeTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.organizationTypeTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdOrgTypeName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Organization type : ' + variables.createdOrgTypeName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Organization_Type = inactivate_Organization_Type;

var reactivate_Organization_Type = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdOrgTypeName = variables.organizationTypeName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.organizationTypeInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.organizationTypeInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.organizationTypeInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdOrgTypeName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.organizationTypeInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Organization_Type = reactivate_Organization_Type;

var create_EMR = function () {
    describe("Create a Organization Type", function () {
        it("should create a Organization Type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.emrLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.emrTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.emrName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.emrAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.emrTable + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdEMRName = variables.emrName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.emrTable + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.emrName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.emrSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"' + variables.emrName + '")]').to.be.present;
                        }
                    });
                });
            });

            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_EMR = create_EMR;

var inactivate_EMR = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'EMR', '', elements.adminPageElements.emrTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.emrTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdEMRName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.emrTable + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.emrTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdEMRName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated EMR : ' + variables.createdEMRName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_EMR = inactivate_EMR;

var reactivate_EMR = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdEMRName = variables.emrName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.emrInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdEMRName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_EMR = reactivate_EMR;


var create_Competitive_Lab = function () {
    describe("Create a Competitive Lab", function () {
        it("should create a Competitive Lab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.competitiveLabLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.competitiveLabName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdCompetitiveLabName = variables.competitiveLabName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.competitiveLabTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.emrName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.competitiveLabTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdCompetitiveLabName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.competitiveLabTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdCompetitiveLabName)){
                            console.log('=====> Competitive lab created successfully : '+variables.createdCompetitiveLabName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Competitive_Lab = create_Competitive_Lab;

var inactivate_Competitive_Lab = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Competitive Lab', '', elements.adminRelationshipManagementPageElements.competitiveLabTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.competitiveLabTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdCompetitiveLabName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.competitiveLabTableList + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.competitiveLabTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdCompetitiveLabName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Competitive lab : ' + variables.createdCompetitiveLabName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Competitive_Lab = inactivate_Competitive_Lab;

var reactivate_Competitive_Lab = function () {
    describe("", function () {
        it("reactivate Competitive Lab", function (browser) {
            variables.createdCompetitiveLabName = variables.competitiveLabName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.emrInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdCompetitiveLabName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.emrInactiveTableList + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Competitive_Lab = reactivate_Competitive_Lab;

var create_Campaign_type = function () {
    describe("Create a Campaign type", function () {
        it("should create a Campaign type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.campaignTypeName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.campaignTypeTable + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdcampaignTypeName = variables.campaignTypeName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.campaignTypeTable + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.emrName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdcampaignTypeName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdcampaignTypeName)){
                            console.log('=====> Campaign type created successfully : '+variables.createdcampaignTypeName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Campaign_type = create_Campaign_type;

var inactivate_Campaign_type = function () {
    describe("", function () {
        it("inactivate Campaign type", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Campaign type', '', elements.adminPageElements.campaignTypeTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdcampaignTypeName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeTable + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.campaignTypeTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdcampaignTypeName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Campaign type : ' + variables.createdcampaignTypeName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Campaign_type = inactivate_Campaign_type;

var reactivate_Campaign_type = function () {
    describe("", function () {
        it("reactivate Campaign type", function (browser) {
            variables.createdcampaignTypeName = variables.campaignTypeName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeInactiveListLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignTypeInactiveListTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminPageElements.campaignTypeInactiveListTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdcampaignTypeName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeInactiveListTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }                                                                                                                       //tbody/tr[1]/td[1]/input
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Campaign_type = reactivate_Campaign_type;

// -------------------------------------------------------------------------


var create_Campaign_stage = function () {
    describe("Create a Campaign stage", function () {
        it("should create a Campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.campaignStageButton);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.campaignStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.campaignStageName)) {
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageCreateLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.campaignStageTable + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdcampaignstageName = variables.campaignStageName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.campaignStageTable + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.emrName);

                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignStageTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdcampaignstageName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignStageTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdcampaignstageName)){
                            console.log('=====> Campaign stage created successfully : '+variables.createdcampaignstageName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Campaign_stage = create_Campaign_stage;

var inactivate_Campaign_stage = function () {
    describe("", function () {
        it("inactivate Campaign type", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Campaign stage', '', elements.adminPageElements.campaignStageTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.campaignStageTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdcampaignstageName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageTable + "/tbody/tr[" + count + "]/td[2]/div/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.campaignStageTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdcampaignstageName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated campaign stage : ' + variables.createdcampaignstageName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Campaign_stage = inactivate_Campaign_stage;

var reactivate_Campaign_stage = function () {
    describe("", function () {
        it("reactivate Campaign type", function (browser) {
            variables.createdcampaignstageName = variables.campaignStageName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.campaignStageInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminPageElements.campaignStageInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdcampaignstageName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }                                                                                                                       //tbody/tr[1]/td[1]/input
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Campaign_stage = reactivate_Campaign_stage;

// -------------------------------------------------------------------------



var create_Opportunity_Stage = function () {
    describe("Create a Opportunity type", function () {
        it("should create a Opportunity type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageBtn);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityStageTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.opportunityStageName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageAddLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityStageTableList + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdOpprtunityStageName = variables.opportunityStageName;
                                commands.checkAndSetValue(browser, elements.adminRelationshipManagementPageElements.opportunityStageTableList + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.opportunityTypeName);
                            });
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageSaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.opportunityStageTableList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdOpprtunityStageName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.opportunityStageTableList+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdOpprtunityStageName)){
                            console.log('=====> Opportunity stage created successfully : '+variables.createdOpprtunityStageName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Opportunity_Stage = create_Opportunity_Stage;

var inactivate_Opportunity_Stage = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Name', 'State', elements.adminRelationshipManagementPageElements.opportunityStageTableList, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityStageTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdOpprtunityStageName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageTableList + "/tbody/tr[" + count + "]/td[4]/i");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.opportunityStageTableList + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdOpprtunityStageName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Opportunity stage : ' + variables.createdOpprtunityStageName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Opportunity_Stage = inactivate_Opportunity_Stage;

var reactivate_Opportunity_Stage = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdOpprtunityStageName = variables.opportunityStageName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminRelationshipManagementPageElements.opportunityStageInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminRelationshipManagementPageElements.opportunityStageInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {

                        if (result.value.includes(variables.createdOpprtunityStageName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.opportunityStageInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Opportunity_Stage = reactivate_Opportunity_Stage;

// -------------------------------------------------------------------------


var create_Speciality = function () {
    describe("Create a Speciality ", function () {
        it("should create a Speciality ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.specialtyBtn);
            browser.pause(2000);

            //check if it already exists
            browser.elements("xpath", elements.adminPageElements.specialtyTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        //if it exists,then do nothing
                        if (!text.value.includes(variables.specialtyFieldName)) {
                            commands.checkAndPerform('click', browser, elements.adminRelationshipManagementPageElements.addSpecialityLink);
                            var categoriesCount = 0;
                            browser.elements("xpath", elements.adminPageElements.specialtyTable + "/tbody/tr", function (result) {
                                var els = result.value;
                                els.forEach(function () {
                                    categoriesCount = categoriesCount + 1;
                                });
                                variables.createdSpecialtyName = variables.specialtyFieldName;
                                commands.checkAndSetValue(browser, elements.adminPageElements.specialtyTable + '/tbody/tr[' + categoriesCount + ']/td[1]/div/input', variables.specialtyFieldName);

                            });
                            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtySaveBtn);
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(4000);
            //check whether it is there in table
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.specialtyTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdSpecialtyName)).to.be.true;
                    });
                });
            });

            //if it is there , print a log
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.specialtyTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdSpecialtyName)){
                            console.log('=====> Campaign type created successfully : '+variables.createdSpecialtyName);
                        }
                    });
                });
            });
            //check if it already exists

            browser.pause(2000);
        });
    });
};
exports.create_Speciality = create_Speciality;

var inactivate_Speciality = function () {
    describe("", function () {
        it("inactivate category", function (browser) {
            browser.pause(2000);
            var nameIndex = 0;
            commands.getColIndexOf(browser, 'Name', 'Average Revenue Per Order', elements.adminPageElements.specialtyTable, function (result) {
                nameIndex = result;
            });
            browser.elements("xpath", elements.adminPageElements.specialtyTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdSpecialtyName)) {
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtyTable + "/tbody/tr[" + count + "]/td[3]/span");
                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(5000);
            //check if it is inactivated in table data
            browser.elements("xpath", elements.adminPageElements.specialtyTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        expect(text.value.includes(variables.createdSpecialtyName)).to.be.false;
                        if(count >= els.length) {
                            console.log('=====> Inactivated Specialty : ' + variables.createdSpecialtyName);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.inactivate_Speciality = inactivate_Speciality;

var reactivate_Speciality = function () {
    describe("", function () {
        it("reactivate category", function (browser) {
            variables.createdSpecialtyName = variables.specialtyFieldName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtyInactiveLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.specialtyInactiveTable + "/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                    browser.getValue(elements.adminPageElements.specialtyInactiveTable + "/tbody/tr[" + count + "]/td/input", function (result) {
                        if (result.value.includes(variables.createdSpecialtyName)) {
                            console.log('inactive name to reactivate : ' + result.value);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.specialtyInactiveTable + "/tbody/tr[" + count + "]/td[2]/span");
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactivate_Speciality = reactivate_Speciality;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;



//Test cases - end






//Function declaration - start

var admin_UserAdministration = function () {
    describe("ADMIN PAGE - USER ADMINISTRATION TAB", function () {

        navigate.navigate_To_UserAdmin_Tab();
        me.check_Create_User();                     // Create a New User
        me.check_Email_All_Users_Never_Logged_On(); // Email all Users never logged on
        me.check_Edit_User();   // User Edit > Roles
                                // User Edit > Insight Roles
                                // User Edit > Email
                                // User Edit > First and Last Names respectively
                                // User Edit > SMS Number
                                // User Edit > LMS
                                // User Edit > User Type
                                // User Edit > User Setting Profile
                                // User Edit > Lens Profile
                                // User Edit > Disable, Locked, Sales Rep respectively
                                // User Edit > Reassign Sales Rep's Account
                                // User Edit > Access Control Nodes
                                // User Edit > Audit/Access Log that User Accessed/Modified
                                // User Edit > Audit Log of User Modifications
        me.check_Authentication();// check  2 Factor Authentication

        me.check_Lock_User_Acc();       // Lock User Account - Verify the User cannot log in

        userAuthentication.logout();

        me.check_Lock_User_Acc_Working();

        userAuthentication.logout();

        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        navigate.navigate_To_UserAdmin_Tab();

        me.check_Disable_User_Acc();    // Disable a User's Account - Verify the User Cannot log in

        userAuthentication.logout();

        me.check_Disable_User_Acc_Working();

        userAuthentication.logout();


        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

        navigate.navigate_To_UserAdmin_Tab();

        me.reactivate_Account();

        commonOperations.check_Filters_Behave_Properly(elements.adminPageElements.userAdminQuickFilterLink,
            elements.adminPageElements.userAdminTable,
            elements.adminPageElements.userAdminFilterAvailColumns,
            elements.adminPageElements.userAdminFilterSelectedColumns,
            elements.adminPageElements.userAdminFilterAddColBtn,
            elements.adminPageElements.userAdminFilterRemoveColBtn,
            elements.adminPageElements.userAdminFilterApplyBtn);  // User List > Quick Filters - Verify Filtering Works Correctly

        // User List > Custom List - Verify Clone, Edit, Delete, New and Ordering of custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.userAdminSettingsIcon,
            elements.adminPageElements.userAdminNewCustomListLink,
            elements.adminPageElements.userAdminOrderCustomListLink,
            elements.adminPageElements.userAdminNewCustomListInput,
            elements.adminPageElements.userAdminNewCustomListSaveBtn,
            elements.adminPageElements.userAdminOrderCustomListDiv,
            elements.adminPageElements.userAdminOrderCustomListCloseBtn,
            elements.adminPageElements.userAdminCustomListAddColBtn,
            elements.adminPageElements.userAdminCustomListRemoveColBtn,
            elements.adminPageElements.userAdminCustomListAvailColumns,
            elements.adminPageElements.userAdminCustomListSelectedColumns,
            elements.adminPageElements.userAdminTable);
        commonOperations.edit_Custom_List(elements.adminPageElements.userAdminSettingsIcon,
            elements.adminPageElements.userAdminEditCustomListLink,
            elements.adminPageElements.userAdminDeleteCustomListBtn,
            elements.adminPageElements.userAdminNewCustomListInput,
            elements.adminPageElements.userAdminNewCustomListSaveBtn,
            elements.adminPageElements.userAdminOrderCustomListLink,
            elements.adminPageElements.userAdminOrderCustomListDiv,
            elements.adminPageElements.userAdminOrderCustomListCloseBtn)
        commonOperations.clone_Custom_List(elements.adminPageElements.userAdminSettingsIcon,
            elements.adminPageElements.userAdminCloneCustomListInput,
            elements.adminPageElements.userAdminCloneCustomListLink,
            elements.adminPageElements.userAdminCloneCustomListCreateBtn,
            elements.adminPageElements.userAdminOrderCustomListLink,
            elements.adminPageElements.userAdminOrderCustomListDiv,
            elements.adminPageElements.userAdminOrderCustomListCloseBtn);
        commonOperations.delete_Custom_List(elements.adminPageElements.userAdminSettingsIcon,
            elements.adminPageElements.userAdminEditCustomListLink,
            elements.adminPageElements.userAdminDeleteCustomListBtn,
            elements.adminPageElements.userAdminOrderCustomListLink,
            elements.adminPageElements.userAdminOrderCustomListDiv,
            elements.adminPageElements.userAdminOrderCustomListCloseBtn);

        me.check_Headers_Sorting(); // User List > Sort by Column Headers
    });
};
exports.admin_UserAdministration = admin_UserAdministration;

var admin_UserProfiles = function () {
    describe("ADMIN - USER PROFILES", function () {
        navigate.navigate_To_UserProfiles_Tab();
        me.check_User_ProfileTable();       // Ensure that the user profile are displayed correctly
                                            // Ensure that add new user profile link is present if user has correct permissions
        me.create_New_User_Profile();       // New User Profile
                                            // Add user profile name
                                            // Name and Description
                                            // User type
                                            // Default Activity Category
                                            // Lens Profile
                                            // 2-Factor Authentication Required checkbox
                                            // Users Tab
                                            // Ensure that the users can Add user to the user profile
                                            // Distribution Group Tab
                                            // Ensure that the user can add distribution groups
                                            // Visible Distribution Groups
                                            // Ensure that user can add visible distribution groups
                                            // Notification Settings Tab
                                            // Ensure that the user can add notification settings
                                            // Check the Audi Log

        me.disable_User_Profile();          // Disable a User Profile - Verify profile can be re-enabled

        // Profile List > Quick Filters - Verify Filtering Works Correctly
        commonOperations.check_Filters_Behave_Properly(elements.adminPageElements.userProfileQuickFilterLink,
            elements.adminPageElements.userProfileTable,
            elements.adminPageElements.userProfileFilterAvailColumns,
            elements.adminPageElements.userProfileFilterSelectedColumns,
            elements.adminPageElements.userProfileFilterAddColBtn,
            elements.adminPageElements.userProfileFilterRemoveColBtn,
            elements.adminPageElements.userProfileFilterApplyBtn);

        // Profile List > Custom List - Verify Clone, Edit, Delete, New and Ordering of custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.userProfileSettingsIcon,
            elements.adminPageElements.userProfileNewCustomListLink,
            elements.adminPageElements.userProfileOrderCustomListLink,
            elements.adminPageElements.userProfileNewCustomListInput,
            elements.adminPageElements.userProfileCustomListSaveBtn,
            elements.adminPageElements.userProfileOrderCustomListDiv,
            elements.adminPageElements.userProfileOrderCloseBtn,
            elements.adminPageElements.userProfileCustomListAddColBtn,
            elements.adminPageElements.userProfileCustomListRemoveBtn,
            elements.adminPageElements.userProfileCustomListAvailColumns,
            elements.adminPageElements.userProfileCustomListSelectedColumns,
            elements.adminPageElements.userProfileTable);
        commonOperations.edit_Custom_List(elements.adminPageElements.userProfileSettingsIcon,
            elements.adminPageElements.userProfileEditCustomListLink,
            elements.adminPageElements.userProfileCustomListDeleteBtn,
            elements.adminPageElements.userProfileNewCustomListInput,
            elements.adminPageElements.userProfileCustomListSaveBtn,
            elements.adminPageElements.userProfileOrderCustomListLink,
            elements.adminPageElements.userProfileOrderCustomListDiv,
            elements.adminPageElements.userProfileOrderCloseBtn);                // Edit  existing custom list
        commonOperations.clone_Custom_List(elements.adminPageElements.userProfileSettingsIcon,
            elements.adminPageElements.userProfileCloneInput,
            elements.adminPageElements.userProfileCloneCustomListLink,
            elements.adminPageElements.userProfileCloneCreateBtn,
            elements.adminPageElements.userProfileOrderCustomListLink,
            elements.adminPageElements.userProfileOrderCustomListDiv,
            elements.adminPageElements.userProfileOrderCloseBtn);       // clone custom list
        commonOperations.delete_Custom_List(elements.adminPageElements.userProfileSettingsIcon,
            elements.adminPageElements.userProfileEditCustomListLink,
            elements.adminPageElements.userProfileCustomListDeleteBtn,
            elements.adminPageElements.userProfileOrderCustomListLink,
            elements.adminPageElements.userProfileOrderCustomListDiv,
            elements.adminPageElements.userProfileOrderCloseBtn);// delete a custom list

        me.check_User_Profile_Headers_Sorting();    // Profile List > Sort by Column Headers

    });
};
exports.admin_UserProfiles = admin_UserProfiles;

var admin_UserRoles = function () {
    describe("ADMIN - USER ROLES", function () {
        navigate.navigate_To_Roles_Tab();
        me.create_User_Role();  // Create new roles
        me.edit_User_Role();    // Edit Role > Admin Permissions
                                // Edit Role > Healthcare Insight Permissions
                                // Edit Role > HRM Permissions
                                // Edit Role > User Access Control Permissions
                                // Edit Role > Integration Permissions
        navigate.navigate_To_Roles_Tab();
        // Check the Custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.userRolesSettingsIcon,
            elements.adminPageElements.userRolesNewCustomListLink,
            elements.adminPageElements.userRolesOrderCustomListLink,
            elements.adminPageElements.userRolesNewListInput,
            elements.adminPageElements.userRolesCustomListSaveBtn,
            elements.adminPageElements.userRolesOrderList,
            elements.adminPageElements.userRolesOrderListCloseBtn,
            elements.adminPageElements.userRolesCustomListAddColBtn,
            elements.adminPageElements.userRolesCustomListRemoveColBtn,
            elements.adminPageElements.userRolesCustomListAvailColumns,
            elements.adminPageElements.userRolesCustomListSelectedColumns,
            elements.adminPageElements.userRolesTable);

        commonOperations.edit_Custom_List(elements.adminPageElements.userRolesSettingsIcon,
            elements.adminPageElements.userRolesEditCustomListLink,
            elements.adminPageElements.userRolesCustomListDeleteBtn,
            elements.adminPageElements.userRolesNewListInput,
            elements.adminPageElements.userRolesCustomListSaveBtn,
            elements.adminPageElements.userRolesOrderCustomListLink,
            elements.adminPageElements.userRolesOrderList,
            elements.adminPageElements.userRolesOrderListCloseBtn );

        commonOperations.clone_Custom_List(elements.adminPageElements.userRolesSettingsIcon,
            elements.adminPageElements.userRolesCloneInput,
            elements.adminPageElements.userRolesCloneCustomListLink,
            elements.adminPageElements.userRolesCloneCreateBtn,
            elements.adminPageElements.userRolesOrderCustomListLink,
            elements.adminPageElements.userRolesOrderList,
            elements.adminPageElements.userRolesOrderListCloseBtn);

        commonOperations.delete_Custom_List(elements.adminPageElements.userRolesSettingsIcon,
            elements.adminPageElements.userRolesEditCustomListLink,
            elements.adminPageElements.userRolesCustomListDeleteBtn,
            elements.adminPageElements.userRolesOrderCustomListLink,
            elements.adminPageElements.userRolesOrderList,
            elements.adminPageElements.userRolesOrderListCloseBtn);

    });
};
exports.admin_UserRoles = admin_UserRoles;

var admin_DistributionGroups = function () {
    describe("ADMIN - DISTRIBUTION GROUPS", function () {

        navigate.navigate_To_Distribution_Tab();
        me.create_New_Distribution_Group();	//Add new Distribution Group
        me.add_Group_to_User_Profile();		//Add the Distribution Group to a User Profile
        navigate.navigate_To_Distribution_Tab();
        me.check_Users_List();				//After above check the users listed in the Ditribution Group
        activityOperations.navigate_To_New_Case();
        me.assign_Activity_To_Group();      //Assign a activity to a distribution group
        navigate.navigate_To_Distribution_Tab();
        // Check the Custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.distributionGrpSettingsIcon,
            elements.adminPageElements.distributionGrpNewListLink,
            elements.adminPageElements.distributionGrpOrderListLink,
            elements.adminPageElements.distributionGrpNewInput,
            elements.adminPageElements.distributionGrpSaveBtn,
            elements.adminPageElements.distributionGrpOrderListDiv,
            elements.adminPageElements.distributionGrpOrderListCloseBtn,
            elements.adminPageElements.distributionGrpListAddColBtn,
            elements.adminPageElements.distributionGrpListRemoveColBtn,
            elements.adminPageElements.distributionGrpListAvailColumns,
            elements.adminPageElements.distributionGrpListSelectedColumns,
            elements.adminPageElements.distributionGrpTable); //Create custom list

        commonOperations.edit_Custom_List(elements.adminPageElements.distributionGrpSettingsIcon,
            elements.adminPageElements.distributionGrpEditListLink,
            elements.adminPageElements.distributionGrpListDeleteBtn,
            elements.adminPageElements.distributionGrpNewInput,
            elements.adminPageElements.distributionGrpListSaveBtn,
            elements.adminPageElements.distributionGrpOrderListLink,
            elements.adminPageElements.distributionGrpOrderListDiv,
            elements.adminPageElements.distributionGrpOrderListCloseBtn); //Edit custom list

        commonOperations.clone_Custom_List(elements.adminPageElements.distributionGrpSettingsIcon,
            elements.adminPageElements.distributionGrpCloneInput,
            elements.adminPageElements.distributionGrpCloneListLink,
            elements.adminPageElements.distributionGrpCloneCreateBtn,
            elements.adminPageElements.distributionGrpOrderListLink,
            elements.adminPageElements.distributionGrpOrderListDiv,
            elements.adminPageElements.distributionGrpOrderListCloseBtn); //Clone custom list

        commonOperations.delete_Custom_List(elements.adminPageElements.distributionGrpSettingsIcon,
            elements.adminPageElements.distributionGrpEditListLink,
            elements.adminPageElements.distributionGrpListDeleteBtn,
            elements.adminPageElements.distributionGrpOrderListLink,
            elements.adminPageElements.distributionGrpOrderListDiv,
            elements.adminPageElements.distributionGrpOrderListCloseBtn); //Delete custom list

        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.check_Notifications_To_Users();  //Ensure that the users get notifications
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

    });
};
exports.admin_DistributionGroups = admin_DistributionGroups;

var admin_NotificationSettings = function () {
    describe("ADMIN - NOTIFICATION SETTINGS", function () {
        navigate.navigate_To_Notification_Tab();
        me.create_New_NotificationSetting();    // Add New Notification Setting
        me.edit_NotificationSetting();          // Edit the Default Setting and verify with a user
                                                // Edit a custom Notification Setting - When Field
                                                // Edit a custom Notification Setting - Match Update
                                                // Edit a custom Notification Setting - Match Relationship
                                                // Edit a custom Notification Setting - Match Priority (will not work with Memo and Opportunity)
                                                // Edit a custom Notification Setting - Then Send
        me.delete_NotificationSetting();        // Verify deleting a Notification Setting
        // Check the Custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.notifySettingSettingsIcon,
            elements.adminPageElements.notifySettingNewLink,
            elements.adminPageElements.notifySettingOrderLink,
            elements.adminPageElements.notifySettingNewListInput,
            elements.adminPageElements.notifySettingCustomListSaveBtn,
            elements.adminPageElements.notifySettingOrderList,
            elements.adminPageElements.notifySettingOrderListCloseBtn,
            elements.adminPageElements.notifySettingListAddColBtn,
            elements.adminPageElements.notifySettingListRemoveColBtn,
            elements.adminPageElements.notifySettingListAvailColumns,
            elements.adminPageElements.notifySettingListSelectedColumns,
            elements.adminPageElements.notifySettingsTable);

        commonOperations.edit_Custom_List (elements.adminPageElements.notifySettingSettingsIcon,
            elements.adminPageElements.notifySettingEditLink,
            elements.adminPageElements.notifySettingCustomListDeleteBtn,
            elements.adminPageElements.notifySettingNewListInput,
            elements.adminPageElements.notifySettingCustomListSaveBtn,
            elements.adminPageElements.notifySettingOrderLink,
            elements.adminPageElements.notifySettingOrderList,
            elements.adminPageElements.notifySettingOrderListCloseBtn);

        commonOperations.clone_Custom_List(elements.adminPageElements.notifySettingSettingsIcon,
            elements.adminPageElements.notifySettingCloneInput,
            elements.adminPageElements.notifySettingCloneLink,
            elements.adminPageElements.notifySettingCloneCreateBtn,
            elements.adminPageElements.notifySettingOrderLink,
            elements.adminPageElements.notifySettingOrderList,
            elements.adminPageElements.notifySettingOrderListCloseBtn);

        commonOperations.delete_Custom_List(elements.adminPageElements.notifySettingSettingsIcon,
            elements.adminPageElements.notifySettingEditLink,
            elements.adminPageElements.notifySettingCustomListDeleteBtn,
            elements.adminPageElements.notifySettingOrderLink,
            elements.adminPageElements.notifySettingOrderList,
            elements.adminPageElements.notifySettingOrderListCloseBtn);

    });
};
exports.admin_NotificationSettings = admin_NotificationSettings;

var admin_AccessControlTree = function () {
    describe("ADMIN - ACCESS CONTROL TREE", function () {
        me.create_New_Control_Tree();   // Create a New Tree
                                        // Edit Tree - add a new user and verify cloud search, activities, etc
                                        // Edit Tree - add an organization and verify cloud search, activities, etc
                                        // Edit Tree - add new node - complete the above tests
                                        // Edit Tree - delete child node - complete the above tests
                                        // Edit Tree - delete tree record
        navigate.navigate_To_AccessControlTree_Tab();
        // Clone a tree - ensure its copied correctly
        // Check the Custom lists
        commonOperations.create_New_Custom_List(elements.adminPageElements.controlTreeSettingsIcon,
            elements.adminPageElements.controlTreeNewListLink,
            elements.adminPageElements.controlTreeOrderListLink,
            elements.adminPageElements.controlTreeNewInput,
            elements.adminPageElements.controlTreeListSaveBtn,
            elements.adminPageElements.controlTreeOrderList,
            elements.adminPageElements.controlTreeOrderListCloseBtn,
            elements.adminPageElements.controlTreeListAddColBtn,
            elements.adminPageElements.controlTreeListRemoveColBtn,
            elements.adminPageElements.controlTreeListAvailColumns,
            elements.adminPageElements.controlTreeListSelectedColumns,
            elements.adminPageElements.controlTreeTable); //Create custom list

        commonOperations.edit_Custom_List(elements.adminPageElements.controlTreeSettingsIcon,
            elements.adminPageElements.controlTreeEditListLink,
            elements.adminPageElements.controlTreeListDeleteBtn,
            elements.adminPageElements.controlTreeNewInput,
            elements.adminPageElements.controlTreeListSaveBtn,
            elements.adminPageElements.controlTreeOrderListLink,
            elements.adminPageElements.controlTreeOrderList,
            elements.adminPageElements.controlTreeOrderListCloseBtn); //Edit custom list

        commonOperations.clone_Custom_List(elements.adminPageElements.controlTreeSettingsIcon,
            elements.adminPageElements.controlTreeCloneInput,
            elements.adminPageElements.controlTreeCloneListLink,
            elements.adminPageElements.controlTreeCloneCreateBtn,
            elements.adminPageElements.controlTreeOrderListLink,
            elements.adminPageElements.controlTreeOrderList,
            elements.adminPageElements.controlTreeOrderListCloseBtn); // Clone custom list

        commonOperations.delete_Custom_List(elements.adminPageElements.controlTreeSettingsIcon,
            elements.adminPageElements.controlTreeEditListLink,
            elements.adminPageElements.controlTreeListDeleteBtn,
            elements.adminPageElements.controlTreeOrderListLink,
            elements.adminPageElements.controlTreeOrderList,
            elements.adminPageElements.controlTreeOrderListCloseBtn);// delete a custom list

    });
};
exports.admin_AccessControlTree = admin_AccessControlTree;

var admin_ServerCredentials = function () {
    describe("ADMIN - SERVER CREDENTIALS", function () {
        navigate.navigate_To_ServerCred_Tab();
        me.create_ServerCredential();   // Add new server credential
        me.delete_ServerCredential();   // Remove the server credentials

    });
};
exports.admin_ServerCredentials = admin_ServerCredentials;

var admin_MergeRecordsTab = function () {
    describe("ADMIN - INFORMATION TAB", function () {
        // Merge Records two Organizations
        // orgOperations.create_New_Org();
        me.create_Second_Org();
        navigate.navigate_To_MergeRecords_Tab();
        me.merge_Both_Org();

        // Merge Records two Contacts
        // contactsOperations.create_Contact('contact');
        me.create_Temporary_Contact('contact');
        navigate.navigate_To_MergeRecords_Contact_Tab();
        me.merge_Both_Contact();

        // Merge Records two Providers
        // contactsOperations.create_Contact('provider');
        me.create_Temporary_Contact('provider');
        navigate.navigate_To_MergeRecords_Provider_Tab();
        me.merge_Both_Provider();

        me.search_Temporary_Org();
        me.search_Temporary_Contact();
        me.search_Temporary_Contact();

    });
};
exports.admin_MergeRecordsTab = admin_MergeRecordsTab;

var admin_PanelsAndFees_FeeSchedule = function () {
    describe("ADMIN - PANELS AND FEES - FEE SCHEDULES", function () {
            me.create_Panel();
            me.create_FeeSchedule();
            me.inactive_FeeSchedule();
            me.reactivate_FeeSchedule();
            // orgOperations.create_New_Org();
            orgOperations.navigate_To_New_Org_Page();
            me.check_FeeSchedule_With_Org();
    });
};
exports.admin_PanelsAndFees_FeeSchedule = admin_PanelsAndFees_FeeSchedule;

var admin_PanelsAndFees_Diagnosis = function () {
    describe("ADMIN - PANELS AND FEES - DIAGNOSIS", function () {
        navigate.navigate_To_DiagnosisTab();
        me.create_Diagnosis();
        me.inactivate_Diagnosis();
        me.reactivate_Diagnosis();
    });
};
exports.admin_PanelsAndFees_Diagnosis = admin_PanelsAndFees_Diagnosis;

var admin_PanelAndFees_Panels = function () {
    describe("ADMIN - PANEL AND FEES - PANELS", function () {
        me.create_Panel();
        me.inactivate_Panel();
        me.reactivate_Panel();
    });
};
exports.admin_PanelAndFees_Panels = admin_PanelAndFees_Panels;

var admin_PanelsAndFees_PanelTypes = function () {
    describe("ADMIN - PANELS AND FEES - PANELS TYPES", function () {
        navigate.navigate_To_Panels_Tab();
        me.create_PanelType();
        me.inactivate_PanelType();
        me.reactivate_PanelType();
    });
};
exports.admin_PanelsAndFees_PanelTypes = admin_PanelsAndFees_PanelTypes;

var admin_PanelsAndFees_Procedures = function () {
    describe("ADMIN - PANELS AND FEES - PROCEDURES", function () {
        navigate.navigate_To_Panels_Tab();
        me.create_Procedures();
        me.inactivate_Procedures();
        me.reactivate_Procedures();
    });
};
exports.admin_PanelsAndFees_Procedures = admin_PanelsAndFees_Procedures;

var admin_PanelsAndFees_Tests = function () {
    describe("ADMIN - PANELS AND FEES - TESTS", function () {
        navigate.navigate_To_Panels_Tab();
        me.create_Tests();
        me.inactivate_Tests();
        me.reactivate_Tests();
    });
};
exports.admin_PanelsAndFees_Tests = admin_PanelsAndFees_Tests;

var customizations_CustomLists = function () {
    describe("CUSTOMIZATIONS - CUSTOM LIST", function () {
        me.create_newCustomList('Activities');
        orgOperations.create_New_Org();
        activityOperations.create_New_Case();
        activityOperations.navigate_To_New_Case();
        me.check_newCustomListOption();
        me.edit_newCustomList('Activities');
        activityOperations.navigate_To_New_Case();
        me.check_newCustomListOption();
    });
};
exports.customizations_CustomLists = customizations_CustomLists;

var relationShip_Management_Activity_Category = function () {
    describe("ACTIVITY CATEGORY", function () {
        me.pauseBrowserSomeTime();
        me.create_Category();
        me.inactivate_category();
        // me.reactivate_category();
    });
};
exports.relationShip_Management_Activity_Category = relationShip_Management_Activity_Category;

var relationShip_Management_Activity_sub_Category = function () {
    describe("ACTIVITY SUB CATEGORY", function () {
        me.pauseBrowserSomeTime();
        me.create_Activity_Sub_Category();
        me.inactivate_Activity_Sub_Category();
        // me.reactivate_Activity_Sub_Category();
    });
};
exports.relationShip_Management_Activity_sub_Category = relationShip_Management_Activity_sub_Category;

var relationShip_Management_Activity_status = function () {
    describe("ACTIVITY STATUS", function () {
        me.pauseBrowserSomeTime();
        me.create_CategoryStatus();
        me.inactivate_categoryStatus();
        // me.reactivate_categoryStatus();
    });
};
exports.relationShip_Management_Activity_status = relationShip_Management_Activity_status;

var relationShip_Management_Campaign_type = function () {
    describe("CAMPAIGN TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Campaign_type();
        me.inactivate_Campaign_type();
        // me.reactivate_Campaign_type();
    });
};
exports.relationShip_Management_Campaign_type = relationShip_Management_Campaign_type;

var relationShip_Management_Campaign_stage = function () {
    describe("CAMPAIGN TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Campaign_stage();
        me.inactivate_Campaign_stage();
        // me.reactivate_Campaign_stage();
    });
};
exports.relationShip_Management_Campaign_stage = relationShip_Management_Campaign_stage;

var relationShip_Management_Competitive_Lab = function () {
    describe("COMPETITIVE LAB", function () {
        me.pauseBrowserSomeTime();
        me.create_Competitive_Lab();
        me.inactivate_Competitive_Lab();
        // me.reactivate_Competitive_Lab();
    });
};
exports.relationShip_Management_Competitive_Lab = relationShip_Management_Competitive_Lab;

var relationShip_Management_Contact_Type = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Contact_Type();
        me.inactivate_Contact_Type();
        // me.reactivate_Contact_Type();
    });
};
exports.relationShip_Management_Contact_Type = relationShip_Management_Contact_Type;

var relationShip_Management_Correction_Action = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Corrective_Action();
        me.inactivate_Corrective_Action();
        // me.reactivate_Corrective_Action();
    });
};
exports.relationShip_Management_Correction_Action = relationShip_Management_Correction_Action;

var relationShip_Management_EMR = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_EMR();
        me.inactivate_EMR();
        // me.reactivate_EMR();
    });
};
exports.relationShip_Management_EMR = relationShip_Management_EMR;

var relationShip_Management_Opportunity_Stage = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Opportunity_Stage();
        me.inactivate_Opportunity_Stage();
        // me.reactivate_Opportunity_Stage();
    });
};
exports.relationShip_Management_Opportunity_Stage = relationShip_Management_Opportunity_Stage;

var relationShip_Management_Organization_Type = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Organization_Type();
        me.inactivate_Organization_Type();
        // me.reactivate_Organization_Type();
    });
};
exports.relationShip_Management_Organization_Type = relationShip_Management_Organization_Type;

var relationShip_Management_Opportunity_Type = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Opportunity_Type();
        me.inactivate_Opportunity_Type();
        // me.reactivate_Contact_Type();
    });
};
exports.relationShip_Management_Opportunity_Type = relationShip_Management_Opportunity_Type;

var relationShip_Management_Root_Cause = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Root_Cause();
        me.inactivate_Root_Cause();
        // me.reactivate_Root_Cause();
    });
};
exports.relationShip_Management_Root_Cause = relationShip_Management_Root_Cause;

var relationShip_Management_Speciality = function () {
    describe("CONTACT TYPE", function () {
        me.pauseBrowserSomeTime();
        me.create_Speciality();
        me.inactivate_Speciality();
        // me.reactivate_Speciality ();
    });
};
exports.relationShip_Management_Speciality = relationShip_Management_Speciality;

var admin_RelationShipPanel_RecordTypes = function () {
    describe("ADMIN - RELATION SHIP MANAGEMENT RECORD TYPES", function () {
        me.relationShip_Management_Activity_Category();
        me.relationShip_Management_Activity_sub_Category();
        me.relationShip_Management_Activity_status();
        me.relationShip_Management_Campaign_type();
        me.relationShip_Management_Campaign_stage()
        me.relationShip_Management_Competitive_Lab();
        me.relationShip_Management_Contact_Type();
        me.relationShip_Management_Correction_Action();
        me.relationShip_Management_EMR();
        me.relationShip_Management_Opportunity_Stage();
        me.relationShip_Management_Organization_Type();
        me.relationShip_Management_Opportunity_Type();
        me.relationShip_Management_Root_Cause();
        me.relationShip_Management_Speciality();
    });
};
exports.admin_RelationShipPanel_RecordTypes = admin_RelationShipPanel_RecordTypes;

var tenantAdmin_Sheet = function () {
    describe("", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'createdORG',
            'createdCase',
            'createdORG2',
            'createdContact',
            'createdContact2',
            'createdProvider',
            'createdProvider2',
        ]);
        orgOperations.create_New_Org();
        activityOperations.create_New_Case();
        me.create_Second_Org();
        contactsOperations.create_Contact('contact');
        me.create_Temporary_Contact('contact');
        contactsOperations.create_Contact('provider');
        me.create_Temporary_Contact('provider');
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        me.admin_UserAdministration();
        me.admin_UserProfiles();
        me.admin_UserRoles();       //there is an error creating roles in our tenant (24-07-2017)
        me.admin_DistributionGroups();
        me.admin_NotificationSettings();
        me.admin_AccessControlTree();
        me.admin_ServerCredentials();
        me.admin_MergeRecordsTab();
        me.admin_PanelsAndFees_FeeSchedule();
        me.admin_PanelsAndFees_Diagnosis();
        me.admin_PanelAndFees_Panels();         //tst it
        me.admin_PanelsAndFees_PanelTypes();
        me.admin_PanelsAndFees_Procedures();
        me.admin_RelationShipPanel_RecordTypes();
        //SHEET EXECUTION - ENDS

    });
};
exports.tenantAdmin_Sheet = tenantAdmin_Sheet;
//Function declaration - end


