/**
 * Created by INDUSA
 */
require('mocha');
var chai = require('chai');
chai.use(require('chai-string'));
var moment = require('moment');
var expect = require('chai').expect;
var contactOperations = require('./contacts.js');
var patientOperations = require('./patient.js');
var orgOperations = require('./organization.js');
var navigateOperations = require('../utility/navigate.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var navigate = require('../utility/navigate.js');
var commonOperations = require('./commons.js');
var activityOperations = require('./activities.js');
var messagesOperations = require('./message.js');
var opportunityOperations = require('./opportunity.js');
var me = require('./collaborationcenter.js');
var nameindex = 0;
var radioBtnIndex = 0;
var relationshipColIndex = 0;
var updateTypeColIndex = 0;
var notificationsCount = 0;

//TEST CASE DECLARATION - START

var check_InternalMessage_Functionality = function () {
    describe("Internal messages in collaboration center", function () {
        it("should check internal messages functionalities", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);

            // User A - Create internal Message 1 to User B; CC: User C
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username2);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentMessage = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentMessage+"')]").to.be.present;


            // User A - Create internal Message 2 to User B
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject2);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage2);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMessageSubject2+"')]").to.be.present;

            browser.pause(2000);
        });
    });
};
exports.check_InternalMessage_Functionality = check_InternalMessage_Functionality;

var check_User2_Operations = function () {
    describe("User 2 Internal messages", function () {
        it("should check user 2 internal message operations", function (browser) {

            // User B - Confirm Messages Tab displays correct 'new message' count
            browser.pause(2000);
            var msgCount = 0;
            browser.getText(elements.collaborationPageElements.messageTabCount,function (result) {
                msgCount = result.value;
                expect(msgCount).to.be.above(0);
            });
            // User B - Confirm that New Messages appear in bold with Radio button Checked
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMessageSubject+"')]").to.be.present;
            var message1RowNumber = 0;
            browser.elements("xpath", elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        if(text.value.includes(variables.newMessageSubject)){
                            message1RowNumber = counter;
                        }
                    });
                });
            });

            browser.elements("xpath", elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr["+message1RowNumber+"]/td/a", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        expect(text.value).to.equal('unread');
                    });
                });
            });

            // User B - Confirm that Bold and Radio button desist after message is read
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.newMessageSubject+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(5000);
            browser.elements("xpath", elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr["+message1RowNumber+"]/td/a", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        expect(text.value).to.equal('read');
                    });
                });
            });

            // User B - Confirm that Message Tab count decreases as messages are read
            browser.getText(elements.collaborationPageElements.messageTabCount,function (result) {
                // msgCount = result.value;
                expect(result.value).to.equal((msgCount - 1).toString());
            });

            // User B - Reply to Message 1 to User A and C
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.newMessageSubject+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageReplyBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username2);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.replyMessageBody);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'RE: "+variables.newMessageSubject+"')]").to.be.present;


            // User B - Forward Message 2 to User C

            browser.pause(2000);
        });
    });
};
exports.check_User2_Operations = check_User2_Operations;

var check_User3_Operations = function () {
    describe("User 3 Internal messages", function () {
        it("should check user 3 internal message operations", function (browser) {
            browser.pause(2000);
            // User C - Verify that message thread is retained in Message 1
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMessageSubject+"')]").to.be.present;

            // User C - Verify that message thread is retained in Message 2
            //msg that is forwarded to this user in user2 operations

            // User C - Reply to Message 1 to User A and B - Relate a Provider (ensure proper Icons display)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.newMessageSubject+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageReplyBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.replyMessageBody);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdContact);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'RE: "+variables.newMessageSubject+"')]").to.be.present;

            // User C - Reply to Message 2 to User A and B - Relate a Contact  (ensure proper Icons display)
            // the message that is forwarded from user 2 operations

            browser.pause(2000);
        });
    });
};
exports.check_User3_Operations = check_User3_Operations;

var check_User1_Operations = function () {
    describe("User 1 Internal messages", function () {
        it("should check user 1 internal message operations", function (browser) {
            browser.pause(2000);
            // User A - Reply to Message 1 to User B and C - Relate a Patient and Order  (ensure proper Icons display??)  Ensure All Message Threads are Retained
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.newMessageSubject+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageReplyBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.replyMessageBody);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdPatientFName);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'RE: "+variables.newMessageSubject+"')]").to.be.present;

            browser.pause(2000);
        });
    });
};
exports.check_User1_Operations = check_User1_Operations;

var check_NotificationFilters = function () {
    describe("Notification filters and custom lists", function () {
        it("should check notification filters", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);

            browser.pause(4000);
            // View Defaults to 'All' (unless re-ordered by User)
            browser.pause(3000);
            browser.expect.element(elements.collaborationPageElements.notificationCustomListDropdown+"/option[1]").to.have.attribute('selected').which.contain('true');

            // Custom Filters tab defaults to Closed
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.notificationQuickFilterLink).to.have.attribute('class').which.not.contain('box-open');

            // Edit List button Displays
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationSettingsIcon);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.notificationEditCustomListLink).to.be.present;
            // New List button Displays
            browser.expect.element(elements.collaborationPageElements.notificationNewCustomListLink).to.be.present;
            // Order List Button Displays
            browser.expect.element(elements.collaborationPageElements.notificationOrderCustomListLink).to.be.present;
            // Drop-down on Custom Filters Displays Quick Filter Options, Apply Changes button, Clear Changes, Button, and Save New List
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationQuickFilterLink);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.notificationFilterByDropdown).to.be.present;
            browser.expect.element(elements.collaborationPageElements.notificationFilterOperatorByDropdown).to.be.present;
            browser.expect.element(elements.collaborationPageElements.notificationFilterApplyBtn).to.be.present;
            browser.expect.element(elements.collaborationPageElements.notificationFilterSaveBtn).to.be.present;
            browser.expect.element(elements.collaborationPageElements.notificationFilterCloseBtn).to.be.present;


            // Display Columns (???)
            browser.expect.element(elements.collaborationPageElements.notificationFilterSelectedColumn).to.be.present;
            // Filter List by Active
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationFilterByDropdown);
            browser.expect.element("//option[contains(@value,'active')]").to.be.present;
            // Filter List by First Created
            browser.expect.element("//option[contains(@value,'firstCreated')]").to.be.present;
            // Filter List by Last Updated
            browser.expect.element("//option[contains(@value,'lastUpdated')]").to.be.present;
            // Filter List by Received
            browser.expect.element("//option[contains(@value,'received')]").to.be.present;
            // Filter List by Relationship
            browser.expect.element("//option[contains(@value,'connectionType')]").to.be.present;
            // Filter List by Status
            browser.expect.element("//option[contains(@value,'activity.status')]").to.be.present;
            // Filter List by Subject
            browser.expect.element("//option[contains(@value,'subject')]").to.be.present;
            // Filter List by Type
            browser.expect.element("//option[contains(@value,'type')]").to.be.present;
            // Filter List by Update
            browser.expect.element("//option[contains(@value,'updateType')]").to.be.present;

            // Filter with Operator Equals
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationFilterOperatorByDropdown);
            // Filter with Operator Does not Equal
            browser.expect.element("//option[contains(@value,'EQ')]").to.be.present;
            browser.expect.element("//option[contains(@value,'NEQ')]").to.be.present;

            // Confirm Pre-Defined Lists / Admin created lists cannot be editted
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationCustomListDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationSettingsIcon);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.notificationEditCustomListLink).to.have.attribute('class').which.contain('disabled');

            // Re-Order Lists
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationOrderCustomListLink);
            browser.pause(2000);
            var firstElementName = '';
            browser.getText(elements.collaborationPageElements.notificationOrderList+"/li[1]",function (result) {
                firstElementName = result.value;
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.notificationOrderList+"/li[1]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationOrderDownBtn);
            browser.pause(5000);
            browser.getText(elements.collaborationPageElements.notificationOrderList+"/li[2]",function (result) {
                expect(result.value).to.equal(firstElementName);
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationOrderListCloseBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.notificationSettingsIcon);
            browser.pause(2000);
        });
    });
};
exports.check_NotificationFilters = check_NotificationFilters;

var check_MessageDetailPage = function () {
    describe("Internal Message detail page", function () {
        it("should check internal message detail page", function (browser) {
            browser.pause(2000);

            commands.checkAndPerform('click', browser, elements.collaborationPageElements.notificationSubTab);
            // Verify when a user logs in, the default page is still notifications on communication center.
            browser.expect.element(elements.collaborationPageElements.notificationSubTab).to.have.attribute('class').which.contain('selected');

            // Verify ever user has the Notifications, Messages and Sent Messages tabs on Communication Center
            browser.expect.element(elements.collaborationPageElements.notificationSubTab).to.be.present;
            browser.expect.element(elements.collaborationPageElements.messageSubTab).to.be.present;
            browser.expect.element(elements.collaborationPageElements.sentMessageSubTab).to.be.present;

            // Email sent to logged in user account should show up in the Messages container
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;

            // When a new email comes in for the logged in user (such as above), an indicator with the number of new items should show on the messages tab.
            var msgCount = 0;
            browser.getText(elements.collaborationPageElements.messageTabCount,function (result) {
                msgCount = result.value;
                expect(msgCount).to.be.above(0);
            });

            // With a new email the circle "new" (filled in circle) indicator should be filled in and email is bold.
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a").to.have.attribute('class').which.contain('unread');
            browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[3]").to.have.css('font-weight').which.contain('bold');

            // When the message is read or manually clicked on, the indicator should be an open circle and email not bold.
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a").to.have.attribute('class').which.contain('read');
            browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[3]").to.have.css('font-weight').which.contain('normal');

            //The indicator should change when manually clicked (i.e. new to read or read to new)
            browser.pause(2000);
            browser.getAttribute(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a","class",function (result) {
                if(result.value.includes('read')){
                    commands.checkAndPerform('click', browser, elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a");
                    browser.pause(3000);
                    browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a").to.have.attribute('class').which.contain('unread');
                }
                else{
                    commands.checkAndPerform('click', browser, elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a");
                    browser.pause(3000);
                    browser.expect.element(elements.collaborationPageElements.messagesRecordsTable+"/tbody/tr/td[2]/a").to.have.attribute('class').which.contain('read');
                }
            });

            // Send new message: Verify the page comes up correctly with To, Subject, Message, CC Users
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.newMessagePopup).to.be.present;
            browser.pause(2000);


            // Send new message: Verify adding User(s) "To" (and removing them).
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.be.present;
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRecipientWrapper+"/div", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.lastname)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageRecipientWrapper+"/div/span/i");
                            browser.pause(2000);
                            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);


            // Send new message: Verify subject works and goes through correctly.
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);


            //     Send new message: Verify message works and goes through correctly.
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);

            //Send new message: Verify adding CC User(s) "To" (and removing them).
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.be.present;
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageCCRecipientWrapper+"/div", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.lastname)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageCCRecipientWrapper+"/div/span/i");
                            browser.pause(2000);
                            browser.expect.element("//div[contains(@id,'"+variables.lastname+" , "+variables.firstname+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Send new message: Verify adding Related Items (and removing)
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdORG);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdORG)){
                            console.log('=====> Related item Organization found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);

            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.include(variables.createdORG);
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdORG)){
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageRelatedItemsTable+"/tbody/tr/td[5]/span");
                            browser.pause(2000);
                            browser.expect.element("//a[contains(.,'"+variables.createdORG+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);


            // Send new message: Verify the To field is required and will not send if required fields are not filled in.
            browser.expect.element(elements.collaborationPageElements.messageToRecipientDiv).to.have.attribute('class').which.contain('required');

            // Add a message to an Organization Record.
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdORG);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdORG)){
                            console.log('=====> Related item Organization found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.include(variables.createdORG);
                        console.log("=====> organization record found in related items table");
                    });
                });
            });

            //     Add a message to a Contact Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdContact);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdContact)){
                            console.log('=====> Related item Contact found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdContact);
                        console.log("=====> contact record found in related items table");
                    });
                });
            });
            browser.pause(2000);

            //     Add a message to a Provider Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdProvider);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProvider)){
                            console.log('=====> Related item Provider found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.include(variables.createdProvider);
                        console.log("=====> provider record found in related items table");
                    });
                });
            });
            browser.pause(2000);

            //     Add a message to a Case Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdCase);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdCase)){
                            console.log('=====> Related item Case found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        browser.expect.element("//td[@class='view hc1 icon-Case']").to.be.present;
                        console.log("=====> case record found in related items table");

                    });
                });
            });
            browser.pause(2000);

            //     Add a message to a Task Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdTask);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdTask)){
                            console.log('=====> Related item Task found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        browser.expect.element("//td[@class='view hc1 icon-Task']").to.be.present;
                        console.log("=====> task record found in related items table");

                    });
                });
            });
            browser.pause(2000);


            //     Add a message to a Memo Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdMemo);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdMemo)){
                            console.log('=====> Related item Memo found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        browser.expect.element("//td[@class='view hc1 icon-Memo']").to.be.present;
                        console.log("=====> memo record found in related items table");
                    });
                });
            });
            browser.pause(2000);

            //     Add a message to a Patient Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdPatient);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPatient)){
                            console.log('=====> Related item Patient found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        browser.expect.element("//td[@class='view hc1 icon-Patient']").to.be.present;
                        console.log("=====> patient record found in related items table");
                    });
                });
            });
            browser.pause(2000);

            // Send new message: Verify the message gets to each of the recipients (To and CC Users)
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMessageSubject+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_MessageDetailPage = check_MessageDetailPage;

var check_ReplyMessageDetails = function () {
    describe("Reply message details", function () {
        it("should check reply message details", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageSubTab);

            // Send reply message: Verify the page comes up correctly with To, Subject, Message, CC Users (should CC Users be required "*" ?)
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]");
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.newMessagePopup).to.be.present;
            browser.pause(2000);

            // Send reply message: Verify adding User(s) "To" (and removing them).
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageReplyBtn);
            browser.pause(2000);
            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.be.present;
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRecipientWrapper+"/div", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.lastname)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageRecipientWrapper+"/div/span/i");
                            browser.pause(2000);
                            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Send reply message: Verify subject works and goes through correctly.
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.replyMsgSubject);

            //     Send reply message: Verify message works and goes through correctly.
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.replyMessageBody);

            //     Send reply message: Verify adding CC User(s) "To" (and removing them).
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element("//div[contains(@id,'"+variables.username+"')]").to.be.present;
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageCCRecipientWrapper+"/div", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.lastname)){
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.messageCCRecipientWrapper+"/div/span/i");
                            browser.pause(2000);
                            browser.expect.element("//div[contains(@id,'"+variables.lastname+" , "+variables.firstname+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageCCInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Send new message: Verify the To field is required and will not send if required fields are not filled in.
            browser.expect.element(elements.collaborationPageElements.messageToRecipientDiv).to.have.attribute('class').which.contain('required');

            // Add a message to a Contact Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdContact);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdContact)){
                            console.log('=====> Related item Contact found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdContact);
                        console.log("=====> contact record found in related items table");
                    });
                });
            });
            browser.pause(2000);

            //     Add a message to a Provider Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdProvider);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdProvider)){
                            console.log('=====> Related item Provider found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdProvider);
                        console.log("=====> provider record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);

            //     Add a message to a Case Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdCase);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdCase)){
                            console.log('=====> Related item Case found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdCase);
                        console.log("=====> case record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);

            //     Add a message to a Task Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdTask);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdTask)){
                            console.log('=====> Related item Task found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdTask);
                        console.log("=====> task record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);


            //     Add a message to a Memo Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdMemo);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdMemo)){
                            console.log('=====> Related item Memo found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdMemo);
                        console.log("=====> memo record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);

            //     Add a message to a Opportunity Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdOpportunity);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdOpportunity)){
                            console.log('=====> Related item Opportunity found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdOpportunity);
                        console.log("=====> opportunity record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);

            //     Add a message to a Patient Record.
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdPatient);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdPatient)){
                            console.log('=====> Related item Patient found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);
            browser.elements("xpath", elements.collaborationPageElements.messageRelatedItemsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {

                        expect(text.value).to.include(variables.createdPatient);
                        console.log("=====> patient record found in related items table");
                    });
                });
            });
            browser.pause(2000);
            browser.pause(2000);

            // Send new message: Verify the message gets to each of the recipients (To and CC Users)
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMessageSubject+"')]").to.be.present;

            browser.pause(2000);
        });
    });
};
exports.check_ReplyMessageDetails = check_ReplyMessageDetails;

var check_sentMessagesPage = function () {
    describe("Sent messages", function () {
        it("should check sent messages functionality", function (browser) {
            browser.pause(2000);
            // Email sent from logged in user's email account should show up in the Sent message folder (and not in the message thread).
            // Send new message from hc1.com: Verify the messages sent from hc1.com is in the sent folder (not in the message thread)
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.sentMessageSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;


            // With the above test, the message detail should show up when clicking on the link
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]");
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.newMessagePopup).to.be.present;

            // Reply to an email from logged in user's email account should show up in the Sent message folder (not in the message thread).
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageReplyBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.replyMessageBody);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'RE: "+variables.sentOrgMessageSubject+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_sentMessagesPage = check_sentMessagesPage;

var check_Notifications = function () {
    describe("Notification settings", function () {
        it("should check notification settings in admin", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.notificationSubTab).to.have.attribute('class').to.contain('selected');
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTask+"')]").to.be.present;
            browser.expect.element("//a[contains(@title,'"+variables.createdCase+"')]").to.be.present;
        });
    });
};
exports.check_Notifications = check_Notifications;

var check_NotificationDefaultProfile = function () {
    describe("Notification default settings", function () {
        it("should check default notification settings", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.notificationDefaultSettingLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_NotificationDefaultProfile = check_NotificationDefaultProfile;

var check_User2_Notifications = function () {
    describe("User 2 notifications", function () {
        it("should check user 2 notifications", function (browser) {
            browser.pause(2000);

            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdCase)) {
                            console.log('name : ' + text.value);
                            browser.getText(elements.collaborationPageElements.notificationTable + "/tbody/tr[" + count + "]/td[" + updateTypeColIndex + "]", function (result) {
                                console.log('=====> Update type found : '+result.value);
                                // expect(result.value).to.include('Assigned');
                            });
                            browser.getText(elements.collaborationPageElements.notificationTable + "/tbody/tr[" + count + "]/td[" + relationshipColIndex + "]", function (result) {
                                console.log('=====> Relationship found : '+result.value);
                                // expect(result.value).to.include('Assignee');
                            });
                        }
                    });
                });
            });
            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if (text.value.includes(variables.createdTask)) {
                            browser.getText(elements.collaborationPageElements.notificationTable + "/tbody/tr[" + count + "]/td[" + updateTypeColIndex + "]", function (result) {
                                console.log('=====> Update type found : '+result.value);
                                // expect(result.value).to.include('Assigned');
                            });
                            browser.getText(elements.collaborationPageElements.notificationTable + "/tbody/tr[" + count + "]/td[" + relationshipColIndex + "]", function (result) {
                                console.log('=====> Relationship found : '+result.value);
                                // expect(result.value).to.include('Assignee');
                            });
                        }
                    });
                });
            });

            // User C - Confirm Update type of Memo 1 as 'Subscribed'
            // User C - Confirm Relationship type of Memo 1 as 'Subscriber'
            // User C - Confirm Update type of Case 1 as 'Modified'

            // User C - Reopen Task 1
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'100')]");
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTask+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdTask+"')]");
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskCompleteBtn).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskCompleteBtn);
            browser.pause(2000);

            // User C - Confirm Update type of Task 1 as 'Completed'
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskStatusDiv).text.to.contain('Completed');
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskReopenDiv).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskReopenBtn);
            browser.pause(2000);
    });
    });
};
exports.check_User2_Notifications = check_User2_Notifications;

var check_Admin_Notifications = function () {
    describe("Admin notifications", function () {
        it("should check admin notifications", function (browser) {
            browser.pause(8000);
            // User A - Confirm that Update type for Task 1 is 'Reopened'
            commands.checkAndPerform('click', browser, "//option[contains(.,'All')]");
            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdTask)){
                            browser.getText(elements.collaborationPageElements.notificationTable+"/tbody/tr/td[9]/span",function (result) {
                                console.log('=====> task status found : '+result.value);
                                // expect(result.value).to.include('Reopened');
                            });

                            // User A - Delete Task 1 ('x of y records' decreases?)
                            browser.pause(2000);
                            var totalrecords = 0;
                            browser.getText(elements.collaborationPageElements.notificationPagingTotalRecords,function (records) {
                                totalrecords = records.value;
                            });
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.notificationTable+"/tbody/tr/td[10]/i");
                            browser.pause(2000);
                            browser.getText(elements.collaborationPageElements.notificationPagingTotalRecords,function (records) {
                                expect(records.value).to.be.below(totalrecords);
                            });
                        }
                    });
                });
            });

            browser.pause(2000);
        });
    });
};
exports.check_Admin_Notifications = check_Admin_Notifications;

var check_No_Notification_For_Task = function () {
    describe("Task notification failure", function () {
        it("should check task notification do not come if not assigned to someone ", function (browser) {
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdTask+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_No_Notification_For_Task = check_No_Notification_For_Task;

var check_No_Notification_For_Case = function () {
    describe("Case notification failure", function () {
        it("should check case notification do not come if not assigned to someone ", function (browser) {
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdCase+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_No_Notification_For_Case = check_No_Notification_For_Case;

var check_Change_Task_Data = function () {
    describe("Task status update", function () {
        it("should update task status", function (browser) {
            browser.pause(3000);
            // Task - Completed - When a Task is Completed or a Case is Resolved
            // Task - Assigned - When a Case or Task is assigned to a new user or a distribution group.  This update only applies to the user that was assigned to the activities.
            // Task - Subscribed - When a user is added as a cc’d user on an activity for the first time
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskCompleteBtn);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskStatusDiv).text.to.contain('Completed');
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskSubjectInputField);
            browser.pause(2000);
            variables.createdTask = variables.updatedTaskName;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.updatedTaskName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskCCUserInput, variables.distribution_group_name);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Change_Task_Data = check_Change_Task_Data;

var verify_Task_Change_Notifications = function (afterModifying) {
    describe("Task change notifications", function () {
        it("Verify task change notifications", function (browser) {

            browser.pause(2000);
            if(afterModifying){
                browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr", function (result) {
                    var els = result.value;
                    els.forEach(function (el) {
                        browser.elementIdText(el.ELEMENT, function (text) {
                            if(text.value.includes(variables.createdTask)){
                                expect(text.value).to.satisfy(function (val) {
                                    if(val.includes('Subscribed') || val.includes('Modified') || val.includes('Reopened') || val.includes('Completed') || val.includes('Assigned')){
                                        return true;
                                    }
                                    else{
                                        return false;
                                    }
                                });
                            }
                        });
                    });
                });
            }else{
                browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr", function (result) {
                    var els = result.value;
                    els.forEach(function (el) {
                        browser.elementIdText(el.ELEMENT, function (text) {
                            if(text.value.includes(variables.createdTask)){
                                expect(text.value).to.satisfy(function (val) {
                                    if(val.includes('Subscribed') || val.includes('Modified') || val.includes('Reopened') || val.includes('Completed') || val.includes('Assigned')){
                                        return true;
                                    }
                                    else{
                                        return false;
                                    }
                                });
                            }
                        });
                    });
                });
            }

            browser.pause(2000);
        });
    });
};
exports.verify_Task_Change_Notifications = verify_Task_Change_Notifications;

var verify_Case_Change_Notifications = function (afterModifying) {
    describe("Case change notifications", function () {
        it("Verify case change notifications", function (browser) {
            browser.pause(2000);
            if(afterModifying){
                browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr", function (result) {
                    var els = result.value;
                    els.forEach(function (el) {
                        browser.elementIdText(el.ELEMENT, function (text) {
                            if(text.value.includes(variables.createdCase)){
                                expect(text.value).to.satisfy(function (val) {
                                    if(val.includes('Subscribed') || val.includes('Modified') || val.includes('Reopened') || val.includes('Completed') || val.includes('Assigned')){
                                        return true;
                                    }
                                    else{
                                        return false;
                                    }
                                });
                            }
                        });
                    });
                });
            }else{
                browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr", function (result) {
                    var els = result.value;
                    els.forEach(function (el) {
                        browser.elementIdText(el.ELEMENT, function (text) {
                            if(text.value.includes(variables.createdCase)){
                                expect(text.value).to.satisfy(function (val) {
                                    if(val.includes('Subscribed') || val.includes('Modified') || val.includes('Reopened') || val.includes('Completed') || val.includes('Assigned')){
                                        return true;
                                    }
                                    else{
                                        return false;
                                    }
                                });
                            }
                        });
                    });
                });
            }

            browser.pause(2000);
        });
    });
};
exports.verify_Case_Change_Notifications = verify_Case_Change_Notifications;

var check_Change_Case_Data = function () {
    describe("Case status update", function () {
        it("should modify case data", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseResolveBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.caseSubjectInputField);
            browser.pause(2000);
            variables.createdCase = variables.newCaseName_updated;
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField, variables.newCaseName_updated);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseCCUserInput, variables.distribution_group_name);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Change_Case_Data = check_Change_Case_Data;

var create_Message_With_Case = function () {
    describe("Message with a case", function () {
        it("should send a message with case as related item", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentCaseMessage = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdCase);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentCaseMessage+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.sentCaseMessage+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.create_Message_With_Case = create_Message_With_Case;

var create_Message_With_Task = function () {
    describe("Message with a task", function () {
        it("should send a message with task as related item", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentTaskMessage = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdTask);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentTaskMessage+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.sentTaskMessage+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.create_Message_With_Task = create_Message_With_Task;

var create_Message_With_Memo = function () {
    describe("Message with a memo", function () {
        it("should send a message with memo as related item", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentMemoMessage = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdMemo);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentMemoMessage+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.sentMemoMessage+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.create_Message_With_Memo = create_Message_With_Memo;

var create_Message_With_Opportunity = function () {
    describe("Message with a opportunity", function () {
        it("should send a message with opportunity as related item", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentOpportunityMessage = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdOpportunity);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOpportunityMessage+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.sentOpportunityMessage+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.create_Message_With_Opportunity = create_Message_With_Opportunity;

var check_MessageList = function (object) {
    describe("Message list", function () {
        it("should check message list", function (browser) {
            browser.pause(2000);
            var msgTab = '';
            var sentMsg = '';
            if(object === 'case'){
                msgTab = elements.activityPageElements.caseMessagesSubTabLink;
                sentMsg = variables.sentCaseMessage;
            }
            if(object === 'memo'){
                msgTab = elements.activityPageElements.memoMessagesSubTabLink;
                sentMsg = variables.sentMemoMessage;
            }
            if(object === 'task'){
                msgTab = elements.activityPageElements.taskMessagesSubTabLink;
                sentMsg = variables.sentTaskMessage;
            }
            if(object === 'opportunity'){
                msgTab = elements.opportunityPageElements.opportunityMessageSubTab;
                sentMsg = variables.sentOpportunityMessage;
            }
            browser.pause(2000);
            commands.checkAndPerform('click', browser,msgTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+sentMsg+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_MessageList = check_MessageList;

var check_Email_Contact_MetaTag = function () {
    describe("Associate Contact with message", function () {
        it("should associate contact with message", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentOrgMessageSubject = variables.newMessageSubject2;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject2);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, '[Contact: '+variables.createdContact+"]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.have.css('color').which.contain('rgba(255, 0, 0, 1)');
            browser.pause(2000);
        });
    });
};
exports.check_Email_Contact_MetaTag = check_Email_Contact_MetaTag;

var check_Email_Patient_MetaTag = function () {
    describe("Associate Contact with message", function () {
        it("should associate contact with message", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentOrgMessageSubject = variables.newMessageSubject2;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject2);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, '[Patient: '+variables.createdPatient+"]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.have.css('color').which.contain('rgba(255, 0, 0, 1)');
            browser.pause(2000);
        });
    });
};
exports.check_Email_Patient_MetaTag = check_Email_Patient_MetaTag;

var check_Email_Provider_MetaTag = function () {
    describe("Associate Contact with message", function () {
        it("should associate contact with message", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentOrgMessageSubject = variables.newMessageSubject2;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject2);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, '[Provider: '+variables.createdProvider+"]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.have.css('color').which.contain('rgba(255, 0, 0, 1)');
            browser.pause(2000);
        });
    });
};
exports.check_Email_Provider_MetaTag = check_Email_Provider_MetaTag;

var check_Email_Metadata = function () {
    describe("Email metadata", function () {
        it("should check email metadata", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.emailMetadataTable).to.be.present;
            // browser.pause(2000);
            // browser.elements("xpath", elements.adminPageElements.emailMetadataTable, function (result) {
            //     var els = result.value;
            //     els.forEach(function (el) {
            //         browser.elementIdText(el.ELEMENT, function (text) {
            //
            //         });
            //     });
            // });
            browser.pause(2000);
        });
    });
};
exports.check_Email_Metadata = check_Email_Metadata;

var check_Email_Org_MetaTag = function () {
    describe("Associate organization with message", function () {
        it("should associate organization with message", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentOrgMessageSubject = variables.newMessageSubject2;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject2);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, '[Organization: '+variables.createdORG+"]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.be.present;
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.sentOrgMessageSubject+"')]").to.have.css('color').which.contain('rgba(255, 0, 0, 1)');
            browser.pause(2000);
        });
    });
};
exports.check_Email_Org_MetaTag = check_Email_Org_MetaTag;

var get_User1_ColIndexes = function () {
    describe("Columns indexes", function () {
        it("should read all column indexes", function (browser) {
            browser.pause(2000);
            commands.getColIndexOf(browser,'subject','fieldid',elements.collaborationPageElements.notificationTable,function (response) {
                nameindex = response;
            });

            commands.getColIndexOf(browser,'status','fieldid',elements.collaborationPageElements.notificationTable,function (response) {
                radioBtnIndex = response;
            });

            commands.getColIndexOf(browser,'connectionType','fieldid',elements.collaborationPageElements.notificationTable,function (response) {
                relationshipColIndex = response;
            });

            commands.getColIndexOf(browser,'updateType','fieldid',elements.collaborationPageElements.notificationTable,function (response) {
                updateTypeColIndex = response;
            });
            browser.pause(2000);
        });
    });
};
exports.get_User1_ColIndexes = get_User1_ColIndexes;

var verify_User_NotificationCount = function () {
    describe("Notification counts", function () {
        it("should check notifications count", function (browser) {
            browser.pause(2000);
            // User A - Confirm Relationship type of Case 1, Task 1, Memo1 as 'Creator'
            // User B - Verify that Notification Counts on Comm Center Tab are Displayed Correctly
            browser.getText(elements.collaborationPageElements.collaborationTabCount,function (result) {
                expect(result.value).to.be.above(0);
            });
            // User B - Verify that Notification Counts on Notifications Sub-tab are Displayed Correctly
            browser.getText(elements.collaborationPageElements.notificationTabCount,function (result) {
                notificationsCount = result.value;
                expect(result.value).to.be.above(0);
            });
            // User B - Verify that new (unread) Notifications appear in Bold
            // User B - Verify that Radio button is checked for unread Notifications
            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value.includes(variables.createdCase)){
                            browser.getCssProperty(elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]/span",'font-weight',function (result) {
                                expect(result.value).to.include('bold');
                            });
                            browser.getAttribute(elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+radioBtnIndex+"]/span/a",'class',function (result) {
                                expect(result.value).to.include('unread');
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_User_NotificationCount = verify_User_NotificationCount;

var verify_User1_Case_Notifications = function () {
    describe("User1 case notifications", function () {
        it("should check user1 case notifications", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdCase+"')]");
            browser.pause(5000);
            browser.back();
            browser.pause(5000);
            // User B - Confirm that Radio button and Bold do not persist after reading messages
            // User B - Confirm Update type of Case 1 as 'Assigned'
            // User B - Confirm Relationship type of Case 1 as 'Assignee'
            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdCase)){
                            browser.getCssProperty(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+nameindex+"]/span",'font-weight',function (result) {
                                expect(result.value).to.include('normal');
                            });
                            browser.getAttribute(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+radioBtnIndex+"]/span/a",'class',function (result) {
                                expect(result.value).to.include('read');
                            });
                            browser.getText(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+updateTypeColIndex+"]",function (result) {
                                expect(result.value).to.include('Assigned');
                            });
                            browser.getText(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+relationshipColIndex+"]",function (result) {
                                expect(result.value).to.include('Assignee');
                            });
                        }
                    });
                });
            });

            // User B - Confirm that Tab counts decrease as new Items are read
            browser.getText(elements.collaborationPageElements.notificationTabCount,function (result) {
                expect(result.value).to.be.above(0);
            });

            browser.pause(2000);
        });
    });
};
exports.verify_User1_Case_Notifications = verify_User1_Case_Notifications;


var verify_User1_Task_Notifications = function () {
    describe("User1 task notifications", function () {
        it("should check user1 task notifications", function (browser) {
            browser.pause(2000);
            // User B - Confirm Update type of Task 1 as 'Subscribed'
            // User B - Confirm Relationship type of Task 1 as 'Subscriber'
            browser.elements("xpath", elements.collaborationPageElements.notificationTable+"/tbody/tr/td["+nameindex+"]", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdTask)){
                            browser.getText(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+updateTypeColIndex+"]",function (result) {
                                console.log('=====> Update type found : '+result.value);
                                // expect(result.value).to.include('Assigned');
                            });
                            browser.getText(elements.collaborationPageElements.notificationTable+"/tbody/tr["+count+"]/td["+relationshipColIndex+"]",function (result) {
                                console.log('=====> Relationship found : '+result.value);
                                // expect(result.value).to.include('Assignee');
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_User1_Task_Notifications = verify_User1_Task_Notifications;

//TEST CASE DECLARATION - END








//FUNCTIONS DECLARATION - START

var collaborationCenter_internalMessages = function () {
    describe("COLLABORATION CENTER - INTERNAL MESSAGES", function () {
        // User A = manthan.buch@indusa.com
        // User B = pooja.mistry@indusa.com
        // User C = himani.bhavsar@indusa.com
        orgOperations.create_New_Org();
        contactOperations.create_Contact('provider');
        patientOperations.create_Patient();


        me.check_InternalMessage_Functionality();   // User A - Create internal Message 1 to User B; CC: User C
                                                    // User A - Create internal Message 2 to User B
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);

        me.check_User2_Operations();                // User B - Confirm Messages Tab displays correct 'new message' count
                                                    // User B - Confirm that New Messages appear in bold with Radio button Checked
                                                    // User B - Confirm that Bold and Radio button desist after message is read
                                                    // User B - Confirm that Message Tab count decreases as messages are read
                                                    // User B - Reply to Message 1 to User A and C
                                                    // User B - Forward Message 2 to User C

        userAuthentication.logout();
        userAuthentication.login(variables.username2,variables.password2,variables.usernameDisplay2);


        me.check_User3_Operations();                // User C - Verify that message thread is retained in Message 1
                                                    // User C - Verify that message thread is retained in Message 2
                                                    // User C - Reply to Message 1 to User A and B - Relate a Provider (ensure proper Icons display)
                                                    // User C - Reply to Message 2 to User A and B - Relate a Contact  (ensure proper Icons display)

        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

        me.check_User1_Operations();                // User A - Reply to Message 1 to User B and C - Relate a Patient and Order  (ensure proper Icons display??)  Ensure All Message Threads are Retained
                                                    // User A - Reply to Message 2 to User B and C - Relate a Organization (ensure proper Icons display??) Ensure All Message Threads are Retained

    });
};
exports.collaborationCenter_internalMessages = collaborationCenter_internalMessages;

var collaborationCenter_NotificationFilters = function () {
    describe("COLLABORATION CENTER - NOTIFICATION FILTERS", function () {

        me.check_NotificationFilters(); // View Defaults to 'All' (unless re-ordered by User)
                                        // Custom Filters tab defaults to Closed
                                        // Edit List button Displays
                                        // New List button Displays
                                        // Order List Button Displays
                                        // Drop-down on Custom Filters Displays Quick Filter Options, Apply Changes button, Clear Changes, Button, and Save New List
                                        // Display Columns (???)
                                        // Filter List by Active
                                        // Filter List by First Created
                                        // Filter List by Last Updated
                                        // Filter List by Received
                                        // Filter List by Relationship
                                        // Filter List by Status
                                        // Filter List by Subject
                                        // Filter List by Type
                                        // Filter List by Update
                                        // Filter with Operator Equals
                                        // Filter with Operator Does not Equal
                                        // Filter with Operator Greater Than
                                        // Filter with Operator Less Than equal to
                                        // Filter with Operator Begins With
                                        // Filter with Operator Does not Begin with
                                        //     Filter with Operator is Empty
                                        // Filter with Operator is not Empty
                                        // Pre-defined fields show options in Drop-Down
                                        //     Confirm Pre-Defined Lists / Admin created lists cannot be editted
                                        // Re-Order Lists
                                        // Create Admin List - Persists on non-admin Users
        commonOperations.create_New_Custom_List(elements.collaborationPageElements.notificationSettingsIcon,
            elements.collaborationPageElements.notificationNewCustomListLink,
            elements.collaborationPageElements.notificationOrderCustomListLink,
            elements.collaborationPageElements.notificationNewCustomListInput,
            elements.collaborationPageElements.notificationNewCustomListSaveBtn,
            elements.collaborationPageElements.notificationOrderList,
            elements.collaborationPageElements.notificationOrderListCloseBtn,
            elements.collaborationPageElements.notificationAddColumnBtn,
            elements.collaborationPageElements.notificationRemoveColBtn,
            elements.collaborationPageElements.notificationCustomListAvailColumns,
            elements.collaborationPageElements.notificationCustomListSelectedColumns,
            elements.collaborationPageElements.notificationTable)// Create a New Custom List

        commonOperations.edit_Custom_List(elements.collaborationPageElements.notificationSettingsIcon,
            elements.collaborationPageElements.notificationEditCustomListLink,
            elements.collaborationPageElements.notificationDeleteCustomListLink,
            elements.collaborationPageElements.notificationNewCustomListInput,
            elements.collaborationPageElements.notificationNewCustomListSaveBtn,
            elements.collaborationPageElements.notificationOrderCustomListLink,
            elements.collaborationPageElements.notificationOrderList,
            elements.collaborationPageElements.notificationOrderListCloseBtn);                // Edit  existing custom list// Edit an Existing Custom List

        commonOperations.clone_Custom_List(elements.collaborationPageElements.notificationSettingsIcon,
            elements.collaborationPageElements.notificationCloneCustomListInput,
            elements.collaborationPageElements.notificationCloneCustomListLink,
            elements.collaborationPageElements.notificationCloneCreateBtn,
            elements.collaborationPageElements.notificationOrderCustomListLink,
            elements.collaborationPageElements.notificationOrderList,
            elements.collaborationPageElements.notificationOrderListCloseBtn);        // clone custom list// Clone an Existing list with 'Save as'

    });
};
exports.collaborationCenter_NotificationFilters = collaborationCenter_NotificationFilters;

var collaborationCenter_MessageDetailPage = function () {
    describe("COLLABORATION CENTER - MESSAGE DETAIL PAGE", function () {

        orgOperations.create_New_Org();
        patientOperations.create_Patient();
        contactOperations.create_Contact('contact');
        contactOperations.create_Contact('provider');
        activityOperations.create_New_Task();
        activityOperations.create_New_Case();
        activityOperations.create_New_Memo();
        messagesOperations.send_Message_With_Org();
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

        me.check_MessageDetailPage();// Verify when a user logs in, the default page is still notifications on communication center.
                                                        // Verify ever user has the Notifications, Messages and Sent Messages tabs on Communication Center

                                                            //     Email sent to logged in user account should show up in the Messages container
                                                            // When a new email comes in for the logged in user (such as above), an indicator with the number of new items should show on the messages tab.
                                                            //     With a new email the circle "new" (filled in circle) indicator should be filled in and email is bold. When the message is read or manually clicked on, the indicator should be an open circle and email not bold.
                                                            //     The indicator should change when manually clicked (i.e. new to read or read to new)
                                                            // Send new message: Verify the page comes up correctly with To, Subject, Message, CC Users
                                                            // Send new message: Verify adding User(s) "To" (and removing them).
                                                            // Send new message: Verify subject works and goes through correctly.
                                                            //     Send new message: Verify message works and goes through correctly.
                                                            //     Send new message: Verify adding CC User(s) "To" (and removing them).
                                                            // Send new message: Verify adding Related Items (and removing)
                                                            // Send new message: Verify the To field is required and will not send if required fields are not filled in.
                                                            // Add a message to an Organization Record.
                                                            //     Add a message to a Contact Record.
                                                            //     Add a message to a Provider Record.
                                                            //     Add a message to a Case Record.
                                                            //     Add a message to a Task Record.
                                                            //     Add a message to a Memo Record.
                                                            //     Add a message to a Opportunity Record.
                                                            //     Add a message to a Patient Record.
                                                            // Send new message: Verify the message gets to each of the recipients (To and CC Users)
                                                            //     Verify the message thread looks and is correct.
    });
};
exports.collaborationCenter_MessageDetailPage = collaborationCenter_MessageDetailPage;

var collaborationCenter_ReplyMessageDetails = function () {
    describe("COLLABORATION CENTER - REPLY MESSAGE DETAILS", function () {

        orgOperations.create_New_Org();
        patientOperations.create_Patient();
        messagesOperations.send_Message_With_Org();
        contactOperations.create_Contact('contact');
        contactOperations.create_Contact('provider');
        activityOperations.create_New_Task();
        activityOperations.create_New_Case();
        activityOperations.create_New_Memo();
        opportunityOperations.create_Opportunity();
        me.check_ReplyMessageDetails();     // Send reply message: Verify the page comes up correctly with To, Subject, Message, CC Users (should CC Users be required "*" ?)
                                                                // Send reply message: Verify adding User(s) "To" (and removing them).
                                                                // Send reply message: Verify subject works and goes through correctly.
                                                                //     Send reply message: Verify message works and goes through correctly.
                                                                //     Send reply message: Verify adding CC User(s) "To" (and removing them).
                                                                // Send reply message: Verify adding Related Items (and removing)
                                                                // Send new message: Verify the To field is required and will not send if required fields are not filled in.
                                                                // Send new message: Verify the message gets to each of the recipients (To and CC Users)
                                                                // Add a message to a Organization Record.
                                                                //     Add a message to a Contact Record.
                                                                //     Add a message to a Provider Record.
                                                                //     Add a message to a Case Record.
                                                                //     Add a message to a Task Record.
                                                                //     Add a message to a Memo Record.
                                                                //     Add a message to a Opportunity Record.
                                                                //     Add a message to a Patient Record.
                                                                //     Verify the message thread looks and is correct.
    });
};
exports.collaborationCenter_ReplyMessageDetails = collaborationCenter_ReplyMessageDetails;

var collaborationCenter_SentMessageDetailPage = function () {
    describe("COLLABORATION CENTER - SENT MESSAGE DETAIL PAGE", function () {
        orgOperations.create_New_Org();
        messagesOperations.send_Message_With_Org();
        me.check_sentMessagesPage();    // Email sent from logged in user's email account should show up in the Sent message folder (and not in the message thread).
                                                            // With the above test, the message detail should show up when clicking on the link
                                                            // Send new message from hc1.com: Verify the messages sent from hc1.com is in the sent folder (not in the message thread)
                                                            // Reply to an email from logged in user's email account should show up in the Sent message folder (not in the message thread).
                                                            // Reply to an email from hc1.com: Verify the messages sent from hc1.com is in the sent folder (not in the message thread).
                                                            // You should not be able to add an attachment to a message from within hc1.com. You would have to download it first.
    });
};
exports.collaborationCenter_SentMessageDetailPage = collaborationCenter_SentMessageDetailPage;

var collaborationCenter_NotificationSettings = function () {
    describe("COLLABORATION CENTER - NOTIFICATION SETTINGS", function () {
        orgOperations.create_New_Org();
        activityOperations.create_New_Case(variables.username1);
        activityOperations.create_New_Task(variables.username1);
        activityOperations.create_New_Memo(true);
        opportunityOperations.create_Opportunity(true);
        navigateOperations.navigate_To_Notification_Tab();
        me.check_NotificationDefaultProfile();  // Verify Notification Settings has a default profile and your user is part of that profile
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.check_Notifications();  // Create a Case and assign it to the user, verify the notification comes to collaboration center for the user
                                                            // Create a Task and assign it to the user, verify the notification comes to collaboration center for the user
                                                            // Create a Memo and CC the user, verify the notification comes to collaboration center for the user
                                                            // Create an Opportunity and CC the user, verify the notification comes to collaboration center for the user
                                                            // Create an Opportunity and assign to the user as a Sales Rep, verify the notification comes to collaboration center for the user
                                                            // Change the Notification Settings for email/SMS and verify email notifications for each of the above.
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.collaborationCenter_NotificationSettings = collaborationCenter_NotificationSettings;

var collaborationCenter_VerifyNotificationsSettings = function () {
    describe("COLLABORATION CENTER - VERIFYING NOTIFICATIONS", function () {
        // User a = pooja
        // User b = sonu
        // User c = himani
        orgOperations.create_New_Org();
        activityOperations.create_New_Case(variables.username1,variables.username2);    // Create Case 1 with User A - Assigned to User B; CC: User C
        activityOperations.create_New_Task(variables.username2,variables.username1);    // Create Task 1 with User A - Assigned to User C; CC: User B
        // Create Case 2 with User A - Assigned to User B; CC: User C via Quick Create bar
        // Create Task 2 with User A - CC: User B; Assigned to User C via Quick Create bar
        // Create Memo 2 with User A - CC: User B and C via Quick Create bar
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.get_User1_ColIndexes();

        me.verify_User_NotificationCount();     // User A - Confirm Relationship type of Case 1, Task 1, Memo1 as 'Creator'
                                                // User B - Verify that Notification Counts on Comm Center Tab are Displayed Correctly
                                                // User B - Verify that Notification Counts on Notifications Sub-tab are Displayed Correctly
                                                // User B - Verify that new (unread) Notifications appear in Bold
                                                // User B - Verify that Radio button is checked for unread Notifications

        me.verify_User1_Case_Notifications();   // User B - Confirm that Radio button and Bold do not persist after reading messages
                                                // User B - Confirm that Tab counts decrease as new Items are read
                                                // User B - Confirm Update type of Case 1 as 'Assigned'
                                                // User B - Confirm Relationship type of Case 1 as 'Assignee'

        me.verify_User1_Task_Notifications();   // User B - Confirm Update type of Task 1 as 'Subscribed'
                                                // User B - Confirm Relationship type of Task 1 as 'Subscriber'


        userAuthentication.logout();
        userAuthentication.login(variables.username2,variables.password2,variables.usernameDisplay2);
        me.check_User2_Notifications(); // User C - Confirm Update type of Case 1 as 'Subscribed'
                                                            // User C - Confirm Relationship type of Case 1 as 'Subscriber'
                                                            // User C - Confirm Update type of Task 1 as 'Assigned'
                                                            // User C - Confirm Relationship type of Task 1 as 'Assignee'
                                                            // User C - Confirm Update type of Memo 1 as 'Subscribed'
                                                            // User C - Confirm Relationship type of Memo 1 as 'Subscriber'
                                                            // User C - Confirm Update type of Case 1 as 'Modified'
                                                            // User C - Confirm Update type of Task 1 as 'Completed'
                                                            // User C - Reopen Task 1
                                                            // User C - Assign Case 1 to User C

        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        me.check_Admin_Notifications(); // User A - Confirm that Update type for Task 1 is 'Reopened'
                                                            // User A - Delete Task 1 ('x of y records' decreases?)
                                                            // User A - Delete Memo 1 (Items are findable via 'Active' / 'Inactive' Flag?)
                                                            // User A - Delete Case 1 (Items persist on other Users Notification lists?)
                                                            // User A - CC: 11 Users
                                                            // User A - CC: 21 Users
                                                            // User A - Create Task with no 'Assigned to' (Does a Notification Present?)
                                                            // User A - Create Case with no 'Assigned to' (Does a Notification Present?)
        activityOperations.create_New_Task();
        activityOperations.create_New_Case();
        me.check_No_Notification_For_Task();    // User A - Create Task with no 'Assigned to' (Does a Notification Present?)
        me.check_No_Notification_For_Case();    // User A - Create Case with no 'Assigned to' (Does a Notification Present?)
    });
};
exports.collaborationCenter_VerifyNotificationsSettings = collaborationCenter_VerifyNotificationsSettings;

var collaborationCenter_ChangeStatusNotifications_Effects = function () {
    describe("COLLABORATION CENTER - CHANGE OF STATUS NOTIFICATIONS", function () {

        orgOperations.create_New_Org();
        activityOperations.create_New_Task(variables.username1,variables.username1);
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.verify_Task_Change_Notifications();// Task - Completed - When a Task is Completed or a Case is Resolved
                                                                    // Task - Subscribed - When a user is added as a cc’d user on an activity for the first time
                                                                    // Task - Assigned - When a Case or Task is assigned to a new user or a distribution group.  This update only applies to the user that was assigned to the activities.
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        activityOperations.navigate_To_New_Task();
        me.check_Change_Task_Data();            // Task - Completed
                                                // Task - Modified - This is a catch-all for generic modifications to an activity that do not fit the criteria above.  This applies to all Relationship Types.
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.verify_Task_Change_Notifications(true);// Task - Notification to Assignee
                                                  // Task - Notification to Group (new with Distribution Groups)
                                                  // Task - Notification to Subscriber

                                                        // Task - All Update settings: Modified, Reopened, Subscribed, Assigned, Completed and Claimed
                                                        // Task - Multiple Update settings: Modified, Reopened, Assigned, Claimed
                                                        // Task - All Update settings with all Relationship: Assignee, Creator, Subscriber, Group
                                                        // Task - All Update settings with some Relationship: Assignee, Subscriber


        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        activityOperations.create_New_Case(variables.username1,variables.username1);
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.verify_Case_Change_Notifications();
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        activityOperations.navigate_To_New_Case();
        me.check_Change_Case_Data();
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.verify_Case_Change_Notifications(true);

        // Case - Completed - When a Task is Completed or a Case is Resolved
        // Case - Reopened
        // Case - Assigned - When a Case or Task is assigned to a new user or a distribution group.  This update only applies to the user that was assigned to the activities.
        // Case - Claimed - When a Distribution Group member assigns the task to another user, all non-assigned Distribution Group members receive a Claimed update.
        // Case - Subscribed - When a user is added as a cc’d user on an activity for the first time
        // Case - Modified - This is a catch-all for generic modifications to an activity that do not fit the criteria above.  This applies to all Relationship Types.
        // Case - Notification to Assignee
        // Case - Notification to Creator
        // Case - Notification to Subscriber
        // Case - Notification to Group (new with Distribution Groups)
        // Case - Notification base on Priority
        // Case - All Update settings: Modified, Reopened, Subscribed, Assigned, Completed and Claimed
        // Case - Multiple Update settings: Modified, Reopened, Assigned, Claimed
        // Case - All Update settings with all Relationship: Assignee, Creator, Subscriber, Group
        // Case - All Update settings with some Relationship: Assignee, Subscriber


        // Opportunity - Modified - This is a catch-all for generic modifications to an activity that do not fit the criteria above.  This applies to all Relationship Types.

        // Opportunity - Notification to Assignee
        // Opportunity - Notification to Creator
        // Opportunity - Notification to Subscriber

        // Opportunity - All Update settings: Modified, Reopened, Subscribed, Assigned, Completed and Claimed
        // Opportunity - Multiple Update settings: Modified, Reopened, Assigned, Claimed

    });
};
exports.collaborationCenter_ChangeStatusNotifications_Effects = collaborationCenter_ChangeStatusNotifications_Effects;

var collaborationCenter_EmailIntegration = function () {
    describe("COLLABORATION CENTER - EMAIL INTEGRATION", function () {

        orgOperations.create_New_Org();
        activityOperations.create_New_Case();
        me.create_Message_With_Case();  // With a message create a case and verify it is added to the case as a related item. Verify the ccd users are related to the case.
        activityOperations.navigate_To_New_Case();
        me.check_MessageList('case');

        activityOperations.create_New_Task();
        me.create_Message_With_Task();   // With a message create a task and verify it is added to the task as a related item. Verify the ccd users are related to the task.
        activityOperations.navigate_To_New_Task();
        me.check_MessageList('task');

        activityOperations.create_New_Memo();
        me.create_Message_With_Memo();  // With a message create a memo and verify it is added to the memo as a related item. Verify the ccd users are related to the memo
        activityOperations.navigate_To_New_Memo();
        me.check_MessageList('memo');

        opportunityOperations.create_Opportunity();
        me.create_Message_With_Opportunity();   // With a message create a opportunity and verify it is added to the opportunity as a related item. Verify the ccd users are related to the Opportunity
        opportunityOperations.navigate_To_New_Opportunity();
        me.check_MessageList('opportunity');

    });
};
exports.collaborationCenter_EmailIntegration = collaborationCenter_EmailIntegration;

var collaborationCenter_EmailMetaTags = function () {
    describe("COLLABORATION CENTER - EMAIL METATAGS", function () {

        orgOperations.create_New_Org();
        contactOperations.create_Contact('contact');
        contactOperations.create_Contact('provider');
        patientOperations.create_Patient();
        navigateOperations.navigate_To_EmailMetaData_Tab();
        me.check_Email_Metadata();  // Email messages will contain meta tags to allow for associating user, Contacts, Providers, Patient and organizations to the email.
                                    // The subject and/or body may contain tags that will be used to connect an organization, contact, provider or patient as a related item.

        me.check_Email_Org_MetaTag();// [Organization:<hostCode>] - Verify that record gets associated with the message
        // me.check_Message_List('Organization');//Verify that message appears in Org Detail Message List

        me.check_Email_Contact_MetaTag();// [Contact:<email address and/or host code>] - Verify that record gets associated with the message
        // me.check_Message_List('Contact');// Verify that message appears in Contact Detail Message List
        me.check_Email_Patient_MetaTag();// [Patient:<email address and/or host code>]  - Verify that record gets associated with the message
        // me.check_Message_List('Provider');// Verify that message appears in Provider Detail Message List
        me.check_Email_Provider_MetaTag();// [Provider:<email address and/or host code>]- Verify that record gets associated with the message
        // me.check_Message_List('Patient');// Verify that message appears in Patient Detail Message List

    });
};
exports.collaborationCenter_EmailMetaTags = collaborationCenter_EmailMetaTags;

var check_user2 = function () {
    describe("USER 2 - "+variables.username2+' CHECKS ', function () {
        me.check_User3_Operations();                // User C - Verify that message thread is retained in Message 1
        // User C - Verify that message thread is retained in Message 2
        // User C - Reply to Message 1 to User A and B - Relate a Provider (ensure proper Icons display)
        // User C - Reply to Message 2 to User A and B - Relate a Contact  (ensure proper Icons display)
        me.check_User2_Notifications(); // User C - Confirm Update type of Case 1 as 'Subscribed'
        // User C - Confirm Relationship type of Case 1 as 'Subscriber'
        // User C - Confirm Update type of Task 1 as 'Assigned'
        // User C - Confirm Relationship type of Task 1 as 'Assignee'
        // User C - Confirm Update type of Memo 1 as 'Subscribed'
        // User C - Confirm Relationship type of Memo 1 as 'Subscriber'
        // User C - Confirm Update type of Case 1 as 'Modified'
        // User C - Confirm Update type of Task 1 as 'Completed'
        // User C - Reopen Task 1
        // User C - Assign Case 1 to User C
    });
};
exports.check_user2 = check_user2;

var check_user1 = function () {
    describe("USER 2 - "+variables.username1+' CHECKS ', function () {
        me.check_User2_Operations();                // User B - Confirm Messages Tab displays correct 'new message' count
        // User B - Confirm that New Messages appear in bold with Radio button Checked
        // User B - Confirm that Bold and Radio button desist after message is read
        // User B - Confirm that Message Tab count decreases as messages are read
        // User B - Reply to Message 1 to User A and C
        // User B - Forward Message 2 to User C
        me.check_Notifications();  // Create a Case and assign it to the user, verify the notification comes to collaboration center for the user
        // Create a Task and assign it to the user, verify the notification comes to collaboration center for the user
        // Create a Memo and CC the user, verify the notification comes to collaboration center for the user
        // Create an Opportunity and CC the user, verify the notification comes to collaboration center for the user
        // Create an Opportunity and assign to the user as a Sales Rep, verify the notification comes to collaboration center for the user
        // Change the Notification Settings for email/SMS and verify email notifications for each of the above.
        me.get_User1_ColIndexes();

        me.verify_User_NotificationCount();     // User A - Confirm Relationship type of Case 1, Task 1, Memo1 as 'Creator'
                                                // User B - Verify that Notification Counts on Comm Center Tab are Displayed Correctly
                                                // User B - Verify that Notification Counts on Notifications Sub-tab are Displayed Correctly
                                                // User B - Verify that new (unread) Notifications appear in Bold
                                                // User B - Verify that Radio button is checked for unread Notifications

        me.verify_User1_Case_Notifications();   // User B - Confirm that Radio button and Bold do not persist after reading messages
                                                // User B - Confirm that Tab counts decrease as new Items are read
                                                // User B - Confirm Update type of Case 1 as 'Assigned'
                                                // User B - Confirm Relationship type of Case 1 as 'Assignee'

        me.verify_User1_Task_Notifications();   // User B - Confirm Update type of Task 1 as 'Subscribed'
                                                // User B - Confirm Relationship type of Task 1 as 'Subscriber'

        me.verify_Task_Change_Notifications();// Task - Completed - When a Task is Completed or a Case is Resolved
        // Task - Subscribed - When a user is added as a cc’d user on an activity for the first time
        // Task - Assigned - When a Case or Task is assigned to a new user or a distribution group.  This update only applies to the user that was assigned to the activities.
        me.verify_Task_Change_Notifications(true);// Task - Notification to Assignee
        // Task - Notification to Group (new with Distribution Groups)
        // Task - Notification to Subscriber

        // Task - All Update settings: Modified, Reopened, Subscribed, Assigned, Completed and Claimed
        // Task - Multiple Update settings: Modified, Reopened, Assigned, Claimed
        // Task - All Update settings with all Relationship: Assignee, Creator, Subscriber, Group
        // Task - All Update settings with some Relationship: Assignee, Subscriber
        me.verify_Case_Change_Notifications();
        me.verify_Case_Change_Notifications(true);
    });
};
exports.check_user1 = check_user1;

var collaborationCenter_Sheet = function () {
    describe("RESOLVE ORGANIZATION SHEET DEPENDENCIES", function () {
        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        // me.collaborationCenter_internalMessages();
        // me.collaborationCenter_NotificationFilters();
        // me.collaborationCenter_MessageDetailPage();
        // me.collaborationCenter_ReplyMessageDetails();
        // me.collaborationCenter_SentMessageDetailPage();
        // me.collaborationCenter_NotificationSettings();
        // me.collaborationCenter_VerifyNotificationsSettings();
        // me.collaborationCenter_ChangeStatusNotifications_Effects();
        me.collaborationCenter_EmailIntegration();
        me.collaborationCenter_EmailMetaTags();

        // me.check_user1();
        // me.check_user2();

        //SHEET EXECUTION - ENDS

    });
};
exports.collaborationCenter_Sheet = collaborationCenter_Sheet;
//FUNCTIONS DECLARATION - END

