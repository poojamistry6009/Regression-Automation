/**
 * Created by INDUSA
 */
require('mocha');
var expect = require('chai').expect;
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var clonedCustomListName = '';
exports.clonedCustomListName = clonedCustomListName;
var updatedCustomListName = '';
exports.updatedCustomListName = updatedCustomListName;
var customListForColumnsCheck = '';

//Display columns behaviour check - START
var check_Columns_Behave_Properly = function (settingsIcon,tableName,availColumns,selectedColumns,newCustomListLink,addColumnBtn,removeColumnBtn,customListInput) {
    describe("Ensure that  Columns behaves properly (check with all the fields)",function () {
        it("should check - Ensure that  Columns behaves properly (check with all the fields)",function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,newCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,customListInput);
            browser.pause(2000);
            customListForColumnsCheck = variables.testCustomListName;
            commands.checkAndSetValue(browser,customListInput,variables.testCustomListName);
            browser.pause(2000);

            //read avail columns count
            var availColumnsCount = 0;
            browser.elements("xpath", availColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if (text.value.trim() != '') {
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(",");
                            for (var i = 0; i < testArr.length; i++) {
                                availColumnsCount  = availColumnsCount + 1;
                            }
                            console.log('===Columns Check===> Available columns : '+availColumnsCount);
                            if(availColumnsCount > 0){
                                for(var i = 1;i < availColumnsCount + 1; i++){
                                    commands.checkAndPerform('click',browser,availColumns+"/li["+i+"]");
                                    browser.pause(1000);
                                }
                            }
                        }else{
                            commands.checkAndPerform('click',browser,selectedColumns+"/li[1]");
                            commands.checkAndPerform('click', browser,removeColumnBtn);
                        }
                    });
                });
            });

            //read selected columns div
            var selectedColumnsList = '';
            browser.elements("xpath",selectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(text.value.trim() != ''){
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(',');
                            console.log('===Columns Check===> Adding column names to selected list');
                            for(var i=0;i<testArr.length;i++){
                                console.log('===Columns Check===> '+testArr[i].toLowerCase());
                                selectedColumnsList = selectedColumnsList.concat((testArr[i].toLowerCase()+" "));
                            }
                        }
                    });
                });
            });

            //save custom list
            commands.checkAndPerform('click',browser,elements.organizationPageElements.createNewCustomListSaveBtn);
            browser.pause(10000);

            //check whether selected columns list match with the custom list table list
            browser.elements("xpath",tableName+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'fieldid', function(result) {
                        if(result.value){
                            browser.elementIdText(el.ELEMENT, function(text) {
                                if(text.value){
                                    console.log('===Columns Check===> Checking column :'+text.value.toLowerCase()+' against selected columns list');
                                    expect(selectedColumnsList.includes(text.value.toLowerCase().trim())).to.be.true;
                                }
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Columns_Behave_Properly = check_Columns_Behave_Properly;

var check_Filters_Behave_Properly = function (quickFilterLink,tableName,availColumns,selectedColumns,filterAddColumnBtn,filterRemoveColumnBtn,quickFilterApplyBtn) {
    describe("Ensure that  Quick Filter behaves properly (check with all the filters)",function () {
        it("should check - Ensure that  Quick Filter behaves properly (check with all the filters)",function (browser) {

            //click on quick filter link
            browser.pause(2000);
            commands.checkAndPerform('click',browser,quickFilterLink);
            browser.pause(2000);

            //read avail columns count
            var availColumnsCount = 0;

            //read available columns count and click all if count > 0
            browser.elements("xpath", availColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if (text.value.trim() != '') {
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(",");
                            for (var i = 0; i < testArr.length; i++) {
                                availColumnsCount  = availColumnsCount + 1;
                            }
                            console.log('===Filter Check===> Available columns :'+availColumnsCount);
                            if(availColumnsCount > 0){
                                for(var i = 1;i < availColumnsCount + 1; i++){
                                    commands.checkAndPerform('click',browser,availColumns+"/li["+i+"]");
                                    browser.pause(1000);
                                }
                            }
                        }else{
                            commands.checkAndPerform('click',browser,selectedColumns+"/li[1]");
                            commands.checkAndPerform('click',browser,filterRemoveColumnBtn);
                        }
                    });
                });
                commands.checkAndPerform('click',browser,filterAddColumnBtn);

            });

            //read selected columns div
            var selectedColumnsList = '';
            browser.elements("xpath",selectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT,function(text) {
                        if(text.value.trim() != ''){
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(',');
                            console.log('===Filter Check===> Adding columns to selected list');
                            for(var i=0;i<testArr.length;i++){
                                console.log('===Filter Check===> Column name : '+testArr[i]);
                                selectedColumnsList = selectedColumnsList.concat((testArr[i].toLowerCase()+" "));
                            }
                        }
                    });
                });
            });
            //after applying compare custom list table columns with selected columns
            commands.checkAndPerform('click',browser,quickFilterApplyBtn);

            browser.pause(2000);
            //match selected columns list from the custom list table columns
            browser.elements("xpath", tableName+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'fieldid', function(result) {
                        if(result.value){
                            browser.elementIdText(el.ELEMENT, function(text) {
                                if(text.value){
                                    console.log('===Filter Check===> Matching column name : '+text.value.toLowerCase()+' against : '+selectedColumnsList);
                                    expect(selectedColumnsList.includes(text.value.toLowerCase())).to.be.true;
                                }
                            });
                        }
                    });
                });
            });
            commands.checkAndPerform('click',browser,quickFilterLink);
            browser.pause(2000);
        });
    });
};
exports.check_Filters_Behave_Properly = check_Filters_Behave_Properly;
//Display columns behaviour check - END



//Custom list functions - START
var create_New_Custom_List = function (settingsIcon,newCustomListLink,orderCustomListLink,newCustomListInput,createNewCustomListSaveBtn,orderedCustomListDisplay,orderListCloseIcon,addColumnBtn,removeColumnBtn,customListAvailColumns,customListSelectedColumns,tableName) {
    describe("Create new custom list",function () {
        it("should check - Create new custom list",function (browser) {
            browser.pause(4000);
            browser.refresh();
            browser.pause(4000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,newCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,newCustomListInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,newCustomListInput,variables.newCustomListName);
            var savedCustomListName = variables.newCustomListName;
            variables.createdCustomList = savedCustomListName;


            //check columns behaves properly - START

            //read avail columns count
            var availColumnsCount = 0;
            browser.elements("xpath", customListAvailColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if (text.value.trim() != '') {
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(",");
                            for (var i = 0; i < testArr.length; i++) {
                                availColumnsCount  = availColumnsCount + 1;
                            }
                            console.log('===Create Custom List===> Available columns : '+availColumnsCount);
                            if(availColumnsCount > 0){
                                for(var i = 1;i < availColumnsCount + 1; i++){
                                    commands.checkAndPerform('click',browser,customListAvailColumns+"/li["+i+"]");
                                    browser.pause(1000);
                                }
                                commands.checkAndPerform('click', browser,addColumnBtn);
                            }
                        }else{
                            commands.checkAndPerform('click',browser,customListSelectedColumns+"/li[1]");
                            commands.checkAndPerform('click', browser,removeColumnBtn);
                        }
                    });
                });
            });
            browser.pause(5000);
            //read selected columns div
            var selectedColumnsList = '';
            browser.elements("xpath",customListSelectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        browser.pause(2000);
                        if(text.value.trim() != ''){
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(',');
                            console.log('===Create Custom List===> Adding columns to selected list');
                            for(var i=0;i<testArr.length;i++){
                                console.log('===Create Custom List===> '+testArr[i]);
                                selectedColumnsList = selectedColumnsList.concat((testArr[i].toLowerCase()+" "));
                            }
                        }
                        browser.pause(2000);
                    });
            });
            });

            //save custom list
            commands.checkAndPerform('click',browser,elements.organizationPageElements.createNewCustomListSaveBtn);
            browser.pause(5000);

            //check whether selected columns list match with the custom list table list
            browser.elements("xpath",tableName+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'fieldId',function (attrResult) {
                        if(attrResult.value){
                            browser.elementIdText(el.ELEMENT, function(text) {
                                if(text.value){
                                    console.log('===Create Custom List===> Checking ColName : '+text.value);
                                    expect(selectedColumnsList.includes(text.value.toLowerCase().trim())).to.be.true;
                                }
                            });
                        }
                    });

                });
            });
            //check columns behaves properly - END


            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,orderCustomListLink);
            browser.pause(2000);
            browser.elements("xpath",orderedCustomListDisplay, function(result){
                var els = result.value;
                els.forEach(function(el){
                    browser.elementIdAttribute(el.ELEMENT,'fieldId',function (attrResult) {
                        if(attrResult.value){
                            browser.elementIdText(el.ELEMENT, function(text) {
                                console.log('===Create Custom List===> Matching column name : '+text.value+' against : '+savedCustomListName);
                                expect(text.value.includes(savedCustomListName)).to.be.true;
                            });
                        }
                    });
                });
            });
            commands.checkAndPerform('click',browser,orderListCloseIcon);

            browser.pause(2000);
        });
    });
};
exports.create_New_Custom_List = create_New_Custom_List;

var edit_Custom_List = function (settingsIcon,editCustomListLink,deleteCustomListLink,newCustomListInput,createNewCustomListSaveBtn, orderCustomListLink,orderedCustomListDisplay,orderListCloseIcon ) {
    describe("Edit  existing custom list",function () {
        it("should check - Edit  existing custom list",function (browser) {
            browser.pause(4000);
            browser.refresh();
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdCustomList+"')]");
            browser.pause(2000);
            browser.expect.element(settingsIcon).to.be.present;
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,editCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,newCustomListInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,newCustomListInput,variables.newCustomListName_updated);
            updatedCustomListName = variables.newCustomListName_updated;
            variables.updatedCustomList = updatedCustomListName;
            browser.pause(2000);
            commands.checkAndPerform('click',browser,createNewCustomListSaveBtn);
            browser.pause(2000);
            browser.expect.element(settingsIcon).to.be.present;
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,orderCustomListLink);
            browser.pause(2000);
            browser.elements("xpath",orderedCustomListDisplay, function(result){
                var els = result.value;
                els.forEach(function(el){
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(updatedCustomListName)).to.be.true;
                    });
                });
            });
            commands.checkAndPerform('click',browser,orderListCloseIcon);


            //delete currently created custom list
            browser.pause(2000);
            commands.checkAndPerform('click',browser,editCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,deleteCustomListLink);
            browser.pause(5000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,orderCustomListLink);
            browser.pause(3000);
            browser.elements("xpath",orderedCustomListDisplay, function(result){
                var els = result.value;
                els.forEach(function(el){
                    browser.elementIdText(el.ELEMENT, function(text) {
                        console.log("");
                        expect(text.value.includes(updatedCustomListName)).to.be.false;
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,orderListCloseIcon);
            browser.pause(2000);
            browser.pause(2000);
        });
    });
};
exports.edit_Custom_List = edit_Custom_List;

var clone_Custom_List = function (settingsIcon,cloneCustomListInput,cloneCustomListLink,cloneCustomListSaveBtn,orderCustomListLink,orderedCustomListDisplay,orderListCloseIcon) {
    describe("clone custom list",function () {
        it("should check - clone custom list",function (browser) {
            browser.pause(4000);
            browser.refresh();
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,cloneCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,cloneCustomListInput);
            browser.pause(2000);
            clonedCustomListName = variables.newCloneCustomListName;
            variables.clonedCustomList = clonedCustomListName;
            commands.checkAndSetValue(browser,cloneCustomListInput,variables.newCloneCustomListName);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,cloneCustomListSaveBtn);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,orderCustomListLink);
            browser.pause(2000);
            commands.readValuesAndAssert(browser,orderedCustomListDisplay,'true');
            commands.checkAndPerform('click',browser,orderListCloseIcon);
            browser.pause(2000);
        });
    });
};
exports.clone_Custom_List = clone_Custom_List;

var delete_Custom_List = function (settingsIcon,editCustomListLink,deleteCustomListLink,orderCustomListLink,orderedCustomListDisplay,orderListCloseIcon) {
    describe("delete a custom list",function () {
        it("should check - delete a custom list",function (browser) {

            //delete cloned custom list
            browser.pause(4000);
            browser.refresh();
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.clonedCustomList+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,editCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,deleteCustomListLink);
            browser.pause(5000);
            commands.checkAndPerform('click',browser,settingsIcon);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,orderCustomListLink);
            browser.pause(3000);
            browser.elements("xpath",orderedCustomListDisplay, function(result){
                var els = result.value;
                els.forEach(function(el){
                    browser.elementIdText(el.ELEMENT, function(text) {
                        console.log("");
                        expect(text.value.includes(clonedCustomListName)).to.be.false;
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,orderListCloseIcon);
            browser.pause(2000);

        });
    });
};
exports.delete_Custom_List = delete_Custom_List;

var check_paging_display = function (pagingDiv) {
    describe("Ensure that the paging is displayed correctly",function () {
        it("should check - Ensure that the paging is displayed correctly",function (browser) {
            browser.pause(2000);
            browser.expect.element(pagingDiv).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_paging_display = check_paging_display;
//Custom list functions - END



//Audit log functions - START
var check_auditHistoryTab = function (editBtn,nameInput,saveBtn,auditLink,startDateDiv,endDateDiv,datePickerDiv,recordsTable,exportsLink,updatedFieldName) {
    describe("Audit history",function () {
        it("should check - audit history tab functionality",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,editBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,nameInput);
            commands.checkAndSetValue(browser,nameInput,updatedFieldName);
            commands.checkAndPerform('click',browser,saveBtn);
            browser.pause(3000);

            browser.moveToElement(auditLink, 100, 100, function() {
                commands.checkAndPerform('click',browser,auditLink);
                browser.pause(2000);
            });

            //Ensure that the start date and end date behaves properly
            commands.checkAndPerform('click',browser,startDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.ENTER);

            commands.checkAndPerform('click',browser,endDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.RIGHT_ARROW);
            browser.keys(browser.Keys.ENTER);


            //Edit a organization - changes should be displayed in the audit history
            browser.pause(2000);
            browser.elements("xpath",recordsTable+"/tbody/tr/td[4]", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(counter === 1){
                            expect(text.value.includes(updatedFieldName)).to.be.true;
                        }
                        counter = counter + 1;
                    });
                });
            });

            // Ensure that the start date and end date behaves properly
            browser.elements("xpath",recordsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(counter === 1){
                            expect(text.value).to.equal('Timestamp');
                        }
                        if(counter === 2){
                            expect(text.value).to.equal('Type');
                        }
                        if(counter === 3){
                            expect(text.value).to.equal('User');
                        }
                        if(counter === 4){
                            expect(text.value).to.equal('Changes');
                        }
                        counter = counter + 1;
                    });
                });
            });

            //If the user has export permission ensure that the user can export the file
            browser.expect.element(exportsLink).to.be.present;

        });
    });
};
exports.check_auditHistoryTab = check_auditHistoryTab;

var check_auditLink = function (auditLogLink) {
    describe("Audit Log Link",function () {
        it("should check - If the user has audit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page",function (browser) {
            browser.expect.element(auditLogLink).to.be.present;
        });
    });
};
exports.check_auditLink = check_auditLink;


var check_accessHistoryTab = function (tabLink,startDateDiv,endDateDiv,datePickerDiv,recordsTable,exportsLink) {
    describe("Access history tab ",function () {
        it("should check - access history tab functionality",function (browser) {

            // Ensure that the start date and end date behaves properly
            browser.pause(2000);
            commands.checkAndPerform('click',browser,tabLink);
            commands.checkAndPerform('click',browser,startDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,endDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.RIGHT_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(2000);
            //Ensure that the timestamp, type, Entity type, Entity Name and user are displayed correctly
            browser.elements("xpath",recordsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(counter === 1){
                            expect(text.value).to.equal('Timestamp');
                        }
                        if(counter === 2){
                            expect(text.value).to.equal('Type');
                        }
                        if(counter === 3){
                            expect(text.value).to.equal('Entity Type');
                        }
                        if(counter === 4){
                            expect(text.value).to.equal('Entity Name');
                        }
                        if(counter === 5){
                            expect(text.value).to.equal('User');
                        }
                        counter = counter + 1;
                    });
                });
            });

            browser.pause(2000);
            // Ensure that it displays the list of all the users who accessed the  record
            browser.elements("xpath",recordsTable+"/tbody/tr/td[5]", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(variables.username)).to.be.true;
                        counter = counter + 1;
                    });
                });
            });

            browser.pause(2000);
            // If the user has export permission ensure that the user can export the file
            browser.expect.element(exportsLink).to.be.present;

        });
    });
};
exports.check_accessHistoryTab = check_accessHistoryTab;

var check_allHistoryTab = function (tabLink,startDateDiv,endDateDiv,datePickerDiv,recordsTable,exportsLink) {
    describe("All History Tab",function () {
        it("should check - all history tab functionality",function (browser) {
            // Ensure that the start date and end date behaves properly
            browser.pause(2000);
            commands.checkAndPerform('click',browser,tabLink);
            commands.checkAndPerform('click',browser,startDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,endDateDiv);
            browser.expect.element(datePickerDiv).to.be.visible;
            browser.keys(browser.Keys.RIGHT_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(2000);
            //Ensure that the timestamp, type, Entity type, Entity Name and user are displayed correctly
            browser.elements("xpath",recordsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(counter === 1){
                            expect(text.value).to.equal('Timestamp');
                        }
                        if(counter === 2){
                            expect(text.value).to.equal('Type');
                        }
                        if(counter === 3){
                            expect(text.value).to.equal('Entity Type');
                        }
                        if(counter === 4){
                            expect(text.value).to.equal('Entity Name');
                        }
                        if(counter === 5){
                            expect(text.value).to.equal('User');
                        }
                        counter = counter + 1;
                    });
                });
            });

            browser.pause(2000);
            // Ensure that all history displays all the user who viewd the record and the changes they made
            browser.elements("xpath",recordsTable+"/tbody/tr/td[5]", function (result) {
                var els = result.value;
                var counter = 1;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(variables.username)).to.be.true;
                        counter = counter + 1;
                    });
                });
            });

            browser.pause(2000);
            // If the user has export permission ensure that the user can export the file
            browser.expect.element(exportsLink).to.be.present;
        });
    });
};
exports.check_allHistoryTab = check_allHistoryTab;

var close_AuditLogDiv = function () {
    describe("Close audit log div",function () {
        it("should close audit log div",function (browser) {
            commands.checkAndPerform('click',browser,elements.auditLogElements.auditLogDivCloseBtn);
        });
    });
};
exports.close_AuditLogDiv = close_AuditLogDiv;
//Audit log functions - END

//Default Profile checks
var check_Default_Profile = function () {
    describe("CHECK DEFAULT PROFILE", function () {
        it("should check default profile", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(.,'Default')]");
            browser.pause(2000);

            //Tabs profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileTabProfileDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileTabProfileDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileTabProfileDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Lens profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileLensProfileDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileLensProfileDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileLensProfileDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Campaign profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileCampaignLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileCampaignLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileCampaignLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Case profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileCaseLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileCaseLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileCaseLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Contact profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileContactLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileContactLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileContactLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Memo profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileMemoLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileMemoLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileMemoLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Opportunity profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileOpportunityLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileOpportunityLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileOpportunityLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Organization profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileOrgLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileOrgLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileOrgLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Panel profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfilePanelLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfilePanelLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfilePanelLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //Patient profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfilePatientLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfilePatientLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfilePatientLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            //task profile
            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileTaskLayoutDropdown);
            commands.checkAndPerform('click', browser, elements.adminPageElements.modelProfileTaskLayoutDropdown+"/option[contains(.,'Standard')]");
            browser.expect.element(elements.adminPageElements.modelProfileTaskLayoutDropdown+"/option[contains(.,'Standard')]").to.have.attribute('selected').which.equals('true');

            commands.checkAndPerform('click', browser,elements.adminPageElements.modelProfileProfileSaveBtn);

            browser.pause(2000);
        });
    });
};
exports.check_Default_Profile = check_Default_Profile;
//Default Profile checks
