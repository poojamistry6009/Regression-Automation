/**
 * Created by INDUSA
 */

require('mocha');
var expect = require('chai').expect;
var moment = require('moment');
var me = require('./campaigns.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var userAuthentication = require('./userAuthentication.js');
var orgOperations = require('./organization.js');
var contactOperations = require('./contacts.js');
var patientOperations = require('./patient.js');
var commonOperations = require('./commons.js');
var adminOperations = require('./admin.js');
var navigateOperations = require('../utility/navigate.js');



//TEST CASE DECLARATION - START

var create_Campaign = function (targetType,addAudience) {
    describe("Create a campaign", function () {
        it("should create a campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCreateLink);
            browser.pause(2000);

            // Create new campaign pop-up should be displayed properly
            browser.expect.element(elements.campaignPageElements.campaignPopupNameInput).to.be.present;

            // Ensure that campaign name is a required field
            // Ensure that target type is a required field
            // Error message is displayed if the users don't fill in the required field
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignPopupCreateBtn);
            browser.expect.element(elements.campaignPageElements.campaignNameErrorLabel).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignTargetTypeErrorLabel).to.be.present;

            // Add a campaign Name
            if(targetType ==='contact'){
                variables.createdCampaignContact = variables.newCampaignNameContact;
                commands.checkAndSetValue(browser,elements.campaignPageElements.campaignPopupNameInput, variables.newCampaignNameContact);
                browser.pause(2000);
            }else{
                variables.createdCampaignPatient = variables.newCampaignNamePatient;
                commands.checkAndSetValue(browser,elements.campaignPageElements.campaignPopupNameInput, variables.newCampaignNamePatient);
                browser.pause(2000);
            }



            // Select a target type
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignPopupTargetTypeDropdown);
            if(targetType === 'contact'){
                commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTargetContactType);
            }
            if(targetType === 'patient'){
                commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTargetPatientType);
            }
            browser.pause(2000);

            //select campaign type if any
            // commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignPopupCampaignTypeDropdown);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdCampaignType+"')]");
            browser.pause(2000);

            // Enter all the field and select save - campaign should be created successfully
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignPopupCreateBtn);
            browser.pause(3000);

            // Ensure that campaign number is auto generated
            browser.expect.element(elements.campaignPageElements.campaignNumberDiv).text.to.not.equal('');

            // User information, Audience, Message Templated subtab is displayed
            browser.expect.element(elements.campaignPageElements.campaignGeneralInfoSubTab).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignAudienceSubTab).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignMessageTemplatesSubTab).to.be.present;

            // Activities and messages subtab is displayed
            browser.expect.element(elements.campaignPageElements.campaignActivitySubTab).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignMessageSubTab).to.be.present;

            // Ensure that user can create a contacts/providers on campaigns
            //navigate to audience tab and add contact
            if(addAudience){
                commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
                browser.pause(2000);
                if(targetType === 'contact'){
                    commands.checkAndSetValue(browser,elements.campaignPageElements.campaignSearchContactInput, variables.createdContactFname);
                    browser.pause(2000);
                    browser.keys(browser.Keys.DOWN_ARROW);
                    browser.keys(browser.Keys.ENTER);
                    browser.expect.element("//a[contains(@title,'"+variables.createdContactLname+", "+variables.createdContactFname+" ')]").to.be.present;
                }
                if(targetType === 'patient'){
                    commands.checkAndSetValue(browser,elements.campaignPageElements.campaignSearchContactInput, variables.createdPatient);
                    browser.pause(2000);
                    browser.keys(browser.Keys.DOWN_ARROW);
                    browser.keys(browser.Keys.ENTER);
                    browser.expect.element("//a[contains(@title,'"+variables.createdPatientLName+", "+variables.createdPatientFName+" ')]").to.be.present;
                }
            }

            //save the campaign
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignSaveAndBackBtn);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.campaignPageElements.campaignsTable).to.be.present;
            browser.elements("xpath", elements.campaignPageElements.campaignsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(targetType === 'contact'){
                            expect(text.value.includes(variables.createdCampaignContact)).to.be.true;
                            variables.dependencyFulfilled.push('createdCampaignContact');
                        }
                        else{
                            expect(text.value.includes(variables.createdCampaignPatient)).to.be.true;
                            variables.dependencyFulfilled.push('createdCampaignPatient');
                        }

                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_Campaign = create_Campaign;

var navigate_To_New_Campaign = function (targetType) {
    describe("Navigate to new campaign", function () {
        it("should navigate to new campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            // commands.checkAndPerform('click', browser, "//option[contains(.,'100')]");
            browser.pause(2000);
            if(targetType == 'contact'){
                commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.createdCampaignContact+"')]");
            }
            else{
                commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.createdCampaignPatient+"')]");
            }

            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Campaign = navigate_To_New_Campaign;

var navigate_To_Messages_Tab = function () {
    describe("Navigate to messages subtab", function () {
        it("should navigate to messages subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Messages_Tab = navigate_To_Messages_Tab;

var navigate_To_Messages_Template_Tab = function () {
    describe("Navigate to messages template subtab", function () {
        it("should navigate to messages template subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Messages_Template_Tab = navigate_To_Messages_Template_Tab;

var navigate_To_Audience_Tab = function () {
    describe("Navigate to audience subtab", function () {
        it("should navigate to audience subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Audience_Tab = navigate_To_Audience_Tab;

var navigate_To_Activities_Tab = function () {
    describe("Navigate to activities subtab", function () {
        it("should navigate to activities subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignActivitySubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Activities_Tab = navigate_To_Activities_Tab;

var check_CampaignTab_Highlighted = function () {
    describe("Campaign Tab is highlighted", function () {
        it("should check - Campaign Tab is highlighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignTabLink).to.have.attribute('class').to.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_CampaignTab_Highlighted = check_CampaignTab_Highlighted;

var check_Campaign_Button_Present = function () {
    describe("create new campaign' button presents with Campaign Write permission", function () {
        it("should check - create new campaign' button presents with Campaign Write permission", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignCreateLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Button_Present = check_Campaign_Button_Present;

var check_Campaign_Edit_Link_Present = function () {
    describe("Edit' link presents with Campign Write permission", function () {
        it("should check - Edit' link presents with Campign Write permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Edit_Link_Present = check_Campaign_Edit_Link_Present;

var check_Campaign_Del_Link_Present = function () {
    describe("Del' link presents with Campaign Delete permission", function () {
        it("should check - Del' link presents with Campaign Delete permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Del_Link_Present = check_Campaign_Del_Link_Present;

var check_Pagination_Working = function () {
    describe("x of y' Presents and Pages correctly", function () {
        it("should check pagination", function (browser) {
            browser.pause(2000);
            browser.getText(elements.campaignPageElements.pagingTotalRecords,function (result) {
                if(result.value > 10){
                    commands.checkAndPerform('click',browser,elements.campaignPageElements.pagingNumberOfRecordsDropdown+"/option[4]");
                    browser.getText(elements.campaignPageElements.pagingTotalRecords,function (result) {
                        browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).text.to.equal('1 - '+result.value);
                    });
                    browser.pause(2000);
                    commands.checkAndPerform('click',browser,elements.campaignPageElements.pagingNumberOfRecordsDropdown+"/option[1]");
                    browser.getText(elements.campaignPageElements.pagingTotalRecords,function (result) {
                        if(result.value < 10){
                            browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).text.to.equal('1 - '+result.value);
                        }
                        if(result.value > 10){
                            browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).text.to.equal('1 - 10');
                            commands.checkAndPerform('click',browser,elements.campaignPageElements.pagingNextButton);
                            browser.pause(3000);
                            var totalNumberOfPages = Math.ceil(parseInt(result.value) / 10);
                            browser.getValue(elements.campaignPageElements.pagingCurrentPageInput,function (result2) {
                                if(parseInt(result2.value) === totalNumberOfPages){
                                    browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).text.to.equal('11 - '+result.value);
                                }
                                else{
                                    browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).text.to.equal('11 - '+(10 * parseInt(result2.value)));
                                }
                            });
                        }
                    });
                }
                else{
                    browser.expect.element(elements.campaignPageElements.pagingTotalRecords).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingNumberOfRecordsDropdown).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingRecordNumbers).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingNextButton).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingLastButton).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingPreviousButton).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingFirstButton).to.be.present;
                    browser.expect.element(elements.campaignPageElements.pagingCurrentPageInput).to.be.present;
                }
            });
        });
    });
};
exports.check_Pagination_Working = check_Pagination_Working;

var check_Campaign_Button_Present_Fail = function () {
    describe("New Campaign' button presents without Campaign Write permission (Fail)", function () {
        it("should check - New Campaign' button presents without Campaign Write permission (Fail)", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignCreateLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Button_Present_Fail = check_Campaign_Button_Present_Fail;

var check_Campaign_Edit_Link_Not_Present = function () {
    describe("Edit' link presents without Campaign Write permission (Fail)", function () {
        it("should check - Edit' link presents without Campaign Write permission (Fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('edit')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Edit_Link_Not_Present = check_Campaign_Edit_Link_Not_Present;

var check_Campaign_Del_Link_Not_Present = function () {
    describe("Del' link presents without Campaign Delete permission (Fail)", function () {
        it("should check - Del' link presents without Campaign Delete permission (Fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Del_Link_Not_Present = check_Campaign_Del_Link_Not_Present;

var edit_CampaignType = function () {
    describe("edit exiting campaign type", function () {
        it("should edit exiting campaign type", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//div[contains(.,'"+variables.createdCampaignType+"')]");
            browser.pause(2000);
            var campaignTypeCount = 0;

            //search for the row line number for the campaign type we want to modify
            browser.elements("xpath",elements.adminPageElements.campaignTypeTable+"/tbody/tr",  function  (result) {
                var  els = result.value;
                els.forEach(function  (el) {
                    campaignTypeCount = campaignTypeCount + 1;
                    browser.elementIdText(el.ELEMENT,function (text) {
                        if(text.value == (variables.createdCampaignType)){
                            browser.pause(1000);
                            variables.createdCampaignType = variables.campaignTypeNameInputUpdated;
                            //edit campaign type data
                            if(campaignTypeCount > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignTypeCount+']/td[1]/div/div');
                                commands.checkAndPerform('clearValue', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignTypeCount+']/td[1]/div/input');
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignTypeCount+']/td[1]/div/input', variables.campaignTypeNameInputUpdated);

                            }else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/div');
                                commands.checkAndPerform('clearValue', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/input');
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/input', variables.campaignTypeNameInputUpdated);
                            }
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignType+'")]').to.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.edit_CampaignType = edit_CampaignType;

var delete_CampaignType = function () {
    describe("Delete a camapaign type", function () {
        it("should delete a campaign type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            var campaignTypeCount = 0;

            //search for the row line number for the campaign type we want to modify
            browser.elements("xpath",elements.adminPageElements.campaignTypeTable+"/tbody/tr",  function  (result) {
                var  els = result.value;
                els.forEach(function  (el) {
                    campaignTypeCount = campaignTypeCount + 1;
                    browser.elementIdText(el.ELEMENT,function (text) {
                        if(text.value == (variables.createdCampaignType)){
                            browser.pause(1000);
                            //delete campaign type
                            if(campaignTypeCount > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignTypeCount+']/td[2]/div/span');
                            }else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[2]/div/span');
                            }
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignType+'")]').to.not.be.present;

                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeInactiveListLink);
                            browser.pause(2000);
                            var inactiveListStr = '';
                            browser.elements("xpath", elements.adminPageElements.campaignTypeInactiveListTable+"/tbody/tr/td/input", function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdValue(el.ELEMENT, function (text) {
                                        if(text.value == variables.createdCampaignType){
                                            expect(text.value).to.equal(variables.createdCampaignType);
                                        }
                                    });
                                });
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_CampaignType = delete_CampaignType;

var reactive_CampaignType = function () {
    describe("Re active a campaign type", function () {
        it("should Re active a campaign type", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeInactiveListLink);
            browser.pause(2000);
            var campaignTypeCount = 0;
            browser.elements("xpath", elements.adminPageElements.campaignTypeInactiveListTable+"/tbody/tr/td/input", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    campaignTypeCount = campaignTypeCount + 1;
                    browser.elementIdValue(el.ELEMENT, function (text) {
                        if(text.value == variables.createdCampaignType){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr['+campaignTypeCount+']/td[2]/span');
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignType+'")]').to.be.present;
                        }

                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactive_CampaignType = reactive_CampaignType;

var edit_CampaignStage = function () {
    describe("edit exiting campaign stage", function () {
        it("should edit exiting campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//div[contains(.,'"+variables.createdCampaignStage+"')]");
            browser.pause(2000);
            var campaignStageCount = 0;

            //search for the row line number for the campaign stage we want to modify
            browser.elements("xpath",elements.adminPageElements.campaignStageTable+"/tbody/tr",  function  (result) {
                var  els = result.value;
                els.forEach(function  (el) {
                    campaignStageCount = campaignStageCount + 1;
                    browser.elementIdText(el.ELEMENT,function (text) {
                        if(text.value == (variables.createdCampaignStage)){
                            browser.pause(1000);
                            variables.createdCampaignStage = variables.campaignStageNameInputUpdated;
                            //edit campaign type data
                            if(campaignStageCount > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignStageCount+']/td[1]/div/div');
                                commands.checkAndPerform('clearValue', browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignStageCount+']/td[1]/div/input');
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignStageCount+']/td[1]/div/input', variables.campaignStageNameInputUpdated);

                            }else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/div');
                                commands.checkAndPerform('clearValue', browser,'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/input');
                                commands.checkAndSetValue(browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/input', variables.campaignStageNameInputUpdated);
                            }
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignStage+'")]').to.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.edit_CampaignStage = edit_CampaignStage;

var delete_CampaignStage = function () {
    describe("Delete a campaign stage", function () {
        it("should delete a campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignStageTab);
            var campaignStageCount = 0;

            //search for the row line number for the campaign type we want to modify
            browser.elements("xpath",elements.adminPageElements.campaignStageTable+"/tbody/tr",  function  (result) {
                var  els = result.value;
                els.forEach(function  (el) {
                    campaignStageCount = campaignStageCount + 1;
                    browser.elementIdText(el.ELEMENT,function (text) {
                        if(text.value == (variables.createdCampaignStage)){
                            browser.pause(1000);
                            //delete campaign type
                            if(campaignStageCount > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr['+campaignStageCount+']/td[2]/div/span');
                            }else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[2]/div/span');
                            }
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignStage+'")]').to.not.be.present;

                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignStageInactiveLink);
                            browser.pause(2000);
                            var inactiveListStr = '';
                            browser.elements("xpath", elements.adminPageElements.campaignStageInactiveTable+"/tbody/tr/td/input", function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdValue(el.ELEMENT, function (text) {
                                        if(text.value == variables.createdCampaignStage){
                                            expect(text.value).to.equal(variables.createdCampaignStage);
                                        }
                                    });
                                });
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_CampaignStage = delete_CampaignStage;

var reactive_CampaignStage = function () {
    describe("Re active a campaign stage", function () {
        it("should Re active a campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignStageTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.campaignStageInactiveLink);
            browser.pause(2000);
            var campaignStageCount = 0;
            browser.elements("xpath", elements.adminPageElements.campaignStageInactiveTable+"/tbody/tr/td/input", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    campaignStageCount = campaignStageCount + 1;
                    browser.elementIdValue(el.ELEMENT, function (text) {
                        if(text.value == variables.createdCampaignStage){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr['+campaignStageCount+']/td[2]/span');
                            browser.pause(2000);
                            browser.execute("scrollTo(20, 40)");
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignStageSaveBtn);
                            browser.pause(2000);
                            browser.expect.element('//div[contains(.,"'+variables.createdCampaignStage+'")]').to.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.reactive_CampaignStage = reactive_CampaignStage;

var check_Campaign_Icon = function () {
    describe("Ensure that campaign icon is displayed properly", function () {
        it("should check - Ensure that campaign icon is displayed properly", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignIcon).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Icon = check_Campaign_Icon;

var check_Campaign_Name_Display = function () {
    describe("Ensure that the campaign name is displayed", function () {
        it("should check - Ensure that the campaign name is displayed", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignNameHeading).text.to.not.equal('');
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Name_Display = check_Campaign_Name_Display;

var edit_Campaign = function () {
    describe("Edit a campaign", function () {
        it("should edit a campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);

            // Edit campaign name
            browser.pause(2000);
            variables.createdCampaign = variables.updatedCampaignName;
            commands.checkAndPerform('clearValue', browser,elements.campaignPageElements.campaignDetailPageNameInput);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignDetailPageNameInput, variables.updatedCampaignName);

            // edit campaign type
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignDetailPageTypeDropdown);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdCampaignType+"')]");


            // Cancel changes - Ensure that the changes are not saved
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCancelChangesLink);
            // browser.expect.element(elements.campaignPageElements.campaignNameHeading).text.to.contain(variables.newCampaignName);

            // Save , save and back button are displayed
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignSaveBtn).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignSaveAndBackBtn).to.be.present;

            // Edit a campaign and save - Ensure that the changes are saved
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignSaveBtn);
            // browser.expect.element(elements.campaignPageElements.campaignNameHeading).text.to.contain(variables.createdCampaign);

            browser.pause(2000);
        });
    });
};
exports.edit_Campaign = edit_Campaign;

var check_SaveAndBack = function () {
    describe("Save and back", function () {
        it("Ensure that the user is brought to the previous page", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignListPageHeader).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_SaveAndBack = check_SaveAndBack;

var delete_Campaign = function (targettype) {
    describe("Delete record", function () {
        it("Ensure that the user can delete a campaign with delete permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignDetailPageDeleteBtn);
            browser.pause(2000);
            if(targettype === 'contact'){
                browser.expect.element("//a[contains(@title,'"+variables.createdCampaignContact+"')]").to.not.be.present;
            }else{
                browser.expect.element("//a[contains(@title,'"+variables.createdCampaignPatient+"')]").to.not.be.present;
            }

            browser.pause(2000);
        });
    });
};
exports.delete_Campaign = delete_Campaign;

var check_Activity_Icons = function () {
    describe("Activities icons are displayed if the user has activties permission", function () {
        it("should check - Activities icons are displayed if the user has activties permission", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignActivitySubTab).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Activity_Icons = check_Activity_Icons;

var create_Case_On_Campaign = function () {
    describe("Create case on campaign", function () {
        it("should create case on campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCreateCaseLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField, variables.newCaseName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newCaseName+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Case_On_Campaign = create_Case_On_Campaign;

var create_Task_On_Campaign = function () {
    describe("Create Task on campaign", function () {
        it("should create Task on campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCreateTaskLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.newTaskName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newTaskName+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Task_On_Campaign = create_Task_On_Campaign;

var create_Memo_On_Campaign = function () {
    describe("Create memo on campaign", function () {
        it("should create memo on campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCreateMemoLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField, variables.newMemoName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.newMemoName+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Memo_On_Campaign = create_Memo_On_Campaign;

var check_Campaign_Edit_Icon_Display = function () {
    describe("edit icon is displayed if the user has write permission", function () {
        it("should check - edit icon is displayed if the user has write permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Edit_Icon_Display = check_Campaign_Edit_Icon_Display;

var check_Campaign_Delete_Icon_Display = function () {
    describe("delete icon is displayed if the user has del permission", function () {
        it("should check - delete icon is displayed if the user has del permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Delete_Icon_Display = check_Campaign_Delete_Icon_Display;

var check_Campaign_DateAndTime = function () {
    describe("Create Date and Time presents", function () {
        it("should check - Create Date and Time presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.globalElements.objectDetailPageFooter).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_DateAndTime = check_Campaign_DateAndTime;

var check_Campaign_CreatedUser = function () {
    describe("Created By user presents", function () {
        it("should check - Created By user presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.globalElements.objectDetailPageFooter).text.to.contain(variables.usernameDisplay);
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_CreatedUser = check_Campaign_CreatedUser;

var check_Campaign_Modified_DateAndTime = function () {
    describe("Modifed Date and Time presents", function () {
        it("should check - Modifed Date and Time presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.globalElements.objectDetailPageFooter).text.to.contain("modified "+variables.today);
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Modified_DateAndTime = check_Campaign_Modified_DateAndTime;

var check_Campaign_Modified_User = function () {
    describe("Modified By user presents", function () {
        it("should check - Modified By user presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.globalElements.objectDetailPageFooter).text.to.contain("modified "+variables.today);
            browser.expect.element(elements.globalElements.objectDetailPageFooter).text.to.contain(variables.usernameDisplay);
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Modified_User = check_Campaign_Modified_User;

var check_Campaign_Edit_Icon_Display_Fail = function () {
    describe("edit icon is displayed without write permission (fail)", function () {
        it("should check - edit icon is displayed without write permission (fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('edit')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Edit_Icon_Display_Fail = check_Campaign_Edit_Icon_Display_Fail;

var check_Campaign_Delete_Icon_Display_Fail = function () {
    describe("del icon is displayed without del permission (fail)", function () {
        it("should check - del icon is displayed without del permission (fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Delete_Icon_Display_Fail = check_Campaign_Delete_Icon_Display_Fail;

var check_Campaign_Create_Contact_Link = function () {
    describe("Create new contact is present if the user has contact write permission", function () {
        it("should reate new contact is present if the user has contact write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignCreateContactLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_Create_Contact_Link = check_Campaign_Create_Contact_Link;

var check_Audience_Display = function () {
    describe("Ensure that audience list view is displayed correctly", function () {
        it("should check - Ensure that audience list view is displayed correctly", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Audience_Display = check_Audience_Display;

var add_Contact_in_Campaign = function () {
    describe("Add contact in campaign", function () {
        it("should add contact in campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignCreateContactLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdCampaignStage+"')]");
            browser.pause(2000);
            variables.createdCampaignContactFname = variables.contactFName;
            variables.createdCampaignContactLname = variables.contactLName;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudienceContactFNameInput, variables.contactFName);
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudienceContactLNameInput, variables.contactLName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.contactsPageQuickCreateBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdCampaignContactLname+", "+variables.createdCampaignContactFname+" ')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.add_Contact_in_Campaign = add_Contact_in_Campaign;

var edit_Contact_Campaign_Stage = function () {
    describe("Campaign stage", function () {
        it("should check - edit a campaign stage", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var contactsCounter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        contactsCounter = contactsCounter + 1;
                        if(text.value.includes(variables.createdCampaignContactLname)){
                            if(contactsCounter >= 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+contactsCounter+']/td[6]/span[2]/div/select');
                            }else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[6]/span[2]/div/select');
                            }
                            browser.pause(2000);
                            browser.expect.element("//option[contains(.,'"+variables.campaignDependencyStage_First+"')]").to.be.present;
                            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdCampaignStage+"')]");
                            browser.pause(2000);
                            browser.expect.element("//option[contains(.,'"+variables.campaignDependencyStage_First+"')]").to.have.attribute('selected').which.contain('true');
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.edit_Contact_Campaign_Stage = edit_Contact_Campaign_Stage;

var delete_Contact_From_Campaign = function () {
    describe("Delete a contact", function () {
        it("should delete a contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        if(text.value.includes(variables.createdCampaignContactLname)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+counter+']/td[8]/i'); //xpath of delete column
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_Contact_From_Campaign = delete_Contact_From_Campaign;

var edit_Campaign_Stage = function () {
    describe("edit campaign stage on a contact", function () {
        it("should edit campaign stage on a contact", function (browser) {
            browser.pause(2000);

            browser.pause(2000);
        });
    });
};
exports.edit_Campaign_Stage = edit_Campaign_Stage;

var create_Case_On_Contact = function () {
    describe("Create a case on a contact", function () {
        it("should create a case on a contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceCreateCaseLinkInContact);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField, variables.newCaseName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
            browser.expect.element("//a[contains(@title,'"+variables.newCaseName+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
        });
    });
};
exports.create_Case_On_Contact = create_Case_On_Contact;

var create_Task_On_Contact = function () {
    describe("Create a Task on contact", function () {
        it("should create a Task on contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceCreateTaskLinkInContact);
            browser.pause(2000);
            variables.createdOpporTask = variables.newTaskName;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.newTaskName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
            browser.expect.element("//a[contains(@title,'"+variables.createdOpporTask+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
        });
    });
};
exports.create_Task_On_Contact = create_Task_On_Contact;

var create_Memo_On_Contact = function () {
    describe("Create memo on contact", function () {
        it("should create memo on contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceCreateMemoLinkInContact);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField, variables.newMemoName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveAndBackBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
            browser.expect.element("//a[contains(@title,'"+variables.newMemoName+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
        });
    });
};
exports.create_Memo_On_Contact = create_Memo_On_Contact;

var create_Opportunity_On_Contact = function () {
    describe("create opportunity on contact", function () {
        it("should create opportunity on contact", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceCreateOpportunityLinkInContact);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrgInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPopupApplyBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNameInputField, variables.opportunityName);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCloseDateInput, variables.today);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveAndBackBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
            browser.expect.element("//a[contains(@title,'"+variables.opportunityName+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
        });
    });
};
exports.create_Opportunity_On_Contact = create_Opportunity_On_Contact;

var check_Campaign_In_Activity = function () {
    describe("Ensure that the campaign is listed as a related item on the activities", function () {
        it("should ensure that the campaign is listed as a related item on the activities", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.newCaseName+"')]");
            browser.pause(2000);
            browser.expect.element("//a[contains(.,'"+variables.createdCampaignContact+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Campaign_In_Activity = check_Campaign_In_Activity;

var check_SendMessage_Link_Present = function () {
    describe('"Send a message to this Audience" link is present with  campaign message write permission', function () {
        it('should check - "Send a message to this Audience" link is present with  campaign message write permission', function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceSendMessageLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_Link_Present = check_SendMessage_Link_Present;

var check_SendMessage_Popup_Display = function () {
    describe('Select "send a message to this audience"', function () {
        it("Send a message pop-up is displayed", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSendMessageLink);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceSendMessagePopup).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_Popup_Display = check_SendMessage_Popup_Display;

var check_SendMessage_Required_Fields = function () {
    describe("Required fields", function () {
        it("should check send message required fields", function (browser) {
            browser.pause(2000);

            // Send to is a required field
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupSendToDiv).to.have.attribute('class').which.contain('required');

            // From is a required field
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupFromDiv).to.have.attribute('class').which.contain('required');

            // Subject is a required field
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupSubjectDiv).to.have.attribute('class').which.contain('required');

            // Selete a template is a required field
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupSelectATemplateDiv).to.have.attribute('class').which.contain('required');


            // All is set as default

            // From field is auto populated with the current user

            // Edit the from field
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Enter a a subject
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.campaignMessageSubject);

            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_Required_Fields = check_SendMessage_Required_Fields;

var check_SendMessage_To_Specific_User = function () {
    describe("Ensure that the user can send the campaign message only to a specific stage audience", function () {
        it("should check - Ensure that the user can send the campaign message only to a specific stage audience", function (browser) {
            browser.pause(2000);

            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_To_Specific_User = check_SendMessage_To_Specific_User;

var check_SendMessage_Attachment_Link = function () {
    describe("Attachments in send a message", function () {
        it("should check attachments in send a message", function (browser) {
            browser.pause(2000);
            // Attachment link is present if users have attachment write permission
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupUploadAttachmentLink).to.be.present;

            // Ensure that the users can add attachments successfully
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignAudiencePopupUploadAttachmentLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupUploadFileInput, variables.testFileUploadPath);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupUploadAttachmentButton).to.be.present;
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignAudiencePopupUploadAttachmentButton);
            browser.pause(2000);


            // Attachments should be listed in the messages pop-up
            browser.expect.element(elements.campaignPageElements.campaignAudiencePopupAttachmentsTable).to.be.present;
            browser.pause(2000);
            browser.expect.element("//a[contains(.,'"+variables.testFileUploadName+"')]").to.be.present;

            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudiencePopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_Attachment_Link = check_SendMessage_Attachment_Link;

var check_SendMessage_Link_Not_Present = function () {
    describe('"Send a message to this Audience" link is present without campaign message write permission (fail)', function () {
        it('should check - "Send a message to this Audience" link is present without campaign message write permission (fail)', function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceSendMessageLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_SendMessage_Link_Not_Present = check_SendMessage_Link_Not_Present;

var check_MessageTemplate_Highlighted = function () {
    describe("Message template highlighted", function () {
        it("should check - Message template subtab is high lighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMessageTemplatesSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_MessageTemplate_Highlighted = check_MessageTemplate_Highlighted;

var check_CreateNewMessageTemplate_Display = function () {
    describe("Create new message template link", function () {
        it("should check - Create new message template link is displayed", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateCreateLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_CreateNewMessageTemplate_Display = check_CreateNewMessageTemplate_Display;

var check_CreateMessageTemplate = function () {
    describe("Create a new message template", function () {
        it("should create a new message template", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignMsgTemplateCreateLink);
            browser.pause(2000);

            // Message template pop-up is displayed
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateCreatePopup).to.be.present;
            browser.pause(2000);

            // Name is a required field
            // Subject is a required field
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateSaveBtn);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateNameErrorLabel).to.be.present;
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateSubjectErrorLabel).to.be.present;

            browser.pause(2000);

            // Enter message template name
            variables.createdMsgTemplate = variables.campaignMsgTemplateName;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateNameInput, variables.campaignMsgTemplateName);
            browser.pause(2000);

            // Add subject
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateSubjectInput, variables.campaignMsgTemplateSubject);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateSaveBtn);

            // Add email message text
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateEmailTextInput, variables.campaignMsgTemplateEmailText);

            // Add Text message text
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateSMSSubTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateSMSTextInput, variables.campaignMsgTempalteSMSText);

            // Attachment link is displayed with attachment write permission
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateAttachmentsSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateAttachmentLink).to.be.present;


            // Enter all the required fields and create a message template successfully
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateDetailNameInput).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateDetailSaveBtn);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdCampaignContact+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);

            // Ensure that the message template is displayed in list
            browser.expect.element("//a[contains(@title,'"+variables.createdMsgTemplate+"')]").to.be.present;

            // Ensure that the message template is displayed in template dropdown in messages pop-up
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateCreateLink);
            browser.pause(2000);
            browser.expect.element("//option[contains(.,'"+variables.createdMsgTemplate+"')]").to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateCreatePopupCloseBtn);

        });
    });
};
exports.check_CreateMessageTemplate = check_CreateMessageTemplate;

var check_MessageTemplateList_Display = function () {
    describe("Message template list", function () {
        it("should check - Ensure that the message template list is displayed properly", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_MessageTemplateList_Display = check_MessageTemplateList_Display;

var check_EditMessageTemplate = function () {
    describe("Edit message templates", function () {
        it("should check - Edit message templates", function (browser) {
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateDetailEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.campaignPageElements.campaignMsgTemplateDetailNameInput);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateDetailNameInput, variables.campaignMsgTemplateName_updated);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateDetailSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateDetailNameInput).to.have.attribute('value').which.contain(variables.campaignMsgTemplateName_updated);
            browser.pause(2000);
        });
    });
};
exports.check_EditMessageTemplate = check_EditMessageTemplate;

var check_DeleteMessageTemplate = function () {
    describe("Delete message templates", function () {
        it("should check - Users should be able to delete message templates", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateDetailDeleteLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdCampaignContact+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.campaignMsgTemplateName_updated+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_DeleteMessageTemplate = check_DeleteMessageTemplate;

var edit_MessageTemplate = function () {
    describe("Edit a message template", function () {
        it("should edit a message template", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignMsgTemplateTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        if(text.value.includes(variables.createdMsgTemplate)){
                            if(counter > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+counter+']/td[1]/a');
                            }
                            else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[1]/a');
                            }

                            commands.checkAndPerform('clearValue', browser,elements.campaignPageElements.campaignMsgTemplateDetailNameInput);
                            browser.pause(2000);
                            variables.createdMsgTemplate = variables.campaignMsgTemplateName_updated;
                            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignMsgTemplateDetailNameInput, variables.campaignMsgTemplateName_updated);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMsgTemplateDetailSaveBtn);
                            browser.pause(2000);
                            browser.expect.element(elements.campaignPageElements.campaignMsgTemplateDetailNameInput).to.have.value.to.contain(variables.createdMsgTemplate);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.edit_MessageTemplate = edit_MessageTemplate;

var delete_MessageTemplate = function () {
    describe("delete a message template", function () {
        it("should delete a message template", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignMessageTemplatesSubTab);
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignMsgTemplateTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var counter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        if(text.value.includes(variables.createdMsgTemplate)){
                            if(counter > 1){
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+counter+']/td[6]/i');
                            }
                            else{
                                commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[6]/i');
                            }
                            browser.expect.element("//a[contains(@title,'"+variables.createdMsgTemplate+"')]").to.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_MessageTemplate = delete_MessageTemplate;

var check_CreatePatientLink_Display = function () {
    describe("Create new patient link", function () {
        it("should check - Create new patient is present if the user has patient write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePatientCreateLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_CreatePatientLink_Display = check_CreatePatientLink_Display;

var check_CreatePatientLink_Display_Fail = function () {
    describe("Create new patient link", function () {
        it("should check - Create new patient is present without write permission (fail)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudiencePatientCreateLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_CreatePatientLink_Display_Fail = check_CreatePatientLink_Display_Fail;

var add_Patient_In_Campaign = function () {
    describe("Create a new patient", function () {
        it("should create a new patient and add on a campaign", function (browser) {
            browser.pause(2000);
            // Create a new patient and add on a campaign
            // Add campaign stage
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudiencePatientCreateLink);
            browser.pause(2000);
            browser.pause(2000);
            variables.createdCampaignPatientFName = variables.campaignPatientFName;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePatientFNameInput, variables.campaignPatientFName);
            browser.pause(2000);
            variables.createdCampaignPatientLName = variables.campaignPatientLName;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePatientLNameInput, variables.campaignPatientLName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudiencePatientCreateBtn);
            browser.pause(2000);

            // Ensure that the patient is added in the campaign audience
            browser.expect.element("//a[contains(@title,'"+variables.createdCampaignPatientLName+", "+variables.createdCampaignPatientFName+" ')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.add_Patient_In_Campaign = add_Patient_In_Campaign;


var add_Existing_Patient_In_Campaign = function () {
    describe("Add a existing patient on a campaign", function () {
        it("should add a existing patient on a campaign", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePatientSearchInput, variables.createdPatientFName);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.expect.element("//a[contains(.,'"+variables.createdPatientLName+",...')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.add_Existing_Patient_In_Campaign = add_Existing_Patient_In_Campaign;

var check_Patient_CustomList = function () {
    describe("Ensure that audience list view is displayed correctly", function () {
        it("should check - Ensure that audience list view is displayed correctly", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Patient_CustomList = check_Patient_CustomList;

var create_Case_In_Campaign_Patient = function () {
    describe("Create a case on a patient", function () {
        it("should check - Create a case on a patient", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr", function (result) {
                var els = result.value;
                var audienceCount = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        audienceCount = audienceCount + 1;
                        if(text.value.includes(variables.createdCampaignPatientLName)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+audienceCount+']/td[7]/span[1]/a');
                            browser.pause(2000);
                            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField, variables.newCaseName);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveAndBackBtn);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.newCaseName+"')]").to.be.present;
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.create_Case_In_Campaign_Patient = create_Case_In_Campaign_Patient;

var create_Task_In_Campaign_Patient = function () {
    describe("Create a Task on patient", function () {
        it("should Create a Task on patient", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr", function (result) {
                var els = result.value;
                var audienceCount = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        audienceCount = audienceCount + 1;
                        if(text.value.includes(variables.createdCampaignPatientLName)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+audienceCount+']/td[7]/span[3]/a');
                            browser.pause(2000);
                            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.newTaskName);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveAndBackBtn);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.newTaskName+"')]").to.be.present;
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.create_Task_In_Campaign_Patient = create_Task_In_Campaign_Patient;

var create_Memo_In_Campaign_Patient = function () {
    describe("Create memo on patient", function () {
        it("should create memo on patient", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr", function (result) {
                var els = result.value;
                var audienceCount = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        audienceCount = audienceCount + 1;
                        if(text.value.includes(variables.createdCampaignPatientLName)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+audienceCount+']/td[7]/span[2]/a');
                            browser.pause(2000);
                            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField, variables.newMemoName);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveAndBackBtn);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignGeneralInfoSubTab);
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.newMemoName+"')]").to.be.present;
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.create_Memo_In_Campaign_Patient = create_Memo_In_Campaign_Patient;

var delete_Patient = function () {
    describe("delete a patient", function () {
        it("should delete a patient", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.campaignPageElements.campaignAudienceTable+"/tbody/tr/td[3]", function (result) {
                var els = result.value;
                var audienceCount = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        audienceCount = audienceCount + 1;
                        if(text.value.includes(variables.createdCampaignPatientLName)){
                            commands.checkAndPerform('click', browser, "/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr["+audienceCount+"]/td[8]/i");
                            browser.expect.element("//a[contains(@title,'"+variables.campaignPatientLName+", "+variables.campaignPatientFName+" ')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_Patient = delete_Patient;

var select_Msg_Template = function () {
    describe("Select a message template", function () {
        it("should check - Selecting a message template from dropdown and ensure text is populated or not", function (browser) {
            // Send to is a required field
            // Select a template is a required field
            // From is a required field
            // Subject is a required field
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignAudienceMsgSendToDiv).to.have.attribute('class').which.contain('required');
            browser.expect.element(elements.campaignPageElements.campaignAudienceMsgSelectATemplateDiv).to.have.attribute('class').which.contain('required');
            browser.expect.element(elements.campaignPageElements.campaignAudienceMsgSubjectDiv).to.have.attribute('class').which.contain('required');
            browser.expect.element(elements.campaignPageElements.campaignAudienceMsgFromDiv).to.have.attribute('class').which.contain('required');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignMsgTemplateMsgTemplateDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(@value,'"+variables.createdMsgTemplate+"')]");
            browser.pause(2000);
        });
    });
};
exports.select_Msg_Template = select_Msg_Template;

var send_ContactsMessage_With_Msg = function () {
    describe("Audience messages", function () {
        it("should check -  Enter text in the message input box - ensure that the text is displayed in the user's email", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSendMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentCampaignMessageSubject = variables.campaignAudienceMsgSubject;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupSubjectInput, variables.campaignAudienceMsgSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupMessageInput,variables.campaignAudienceMsg);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceMsgPopupSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//button[contains(.,'Yes')]");
            browser.pause(4000);
            //
            // commands.checkAndPerform('click', browser, "//a[contains(.,'"+variables.createdContactLname+",...')]");
            // browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            // browser.pause(2000);
            // browser.expect.element("//a[contains(@title,'"+variables.sentCampaignMessageSubject+"')]").to.be.present;
            // browser.pause(2000);
            // browser.back();
        });
    });
};
exports.send_ContactsMessage_With_Msg = send_ContactsMessage_With_Msg;

var send_ContactsMessage_Without_Msg = function () {
    describe("Audience messages", function () {
        it("should check - Leave the message input box blank - no text is displayed in the users email", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSendMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentCampaignMessageSubject = variables.campaignAudienceMsgSubject_blank;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupSubjectInput, variables.campaignAudienceMsgSubject_blank);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceMsgPopupSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//button[contains(.,'Yes')]");
            browser.pause(2000);


            // commands.checkAndPerform('click', browser, "//a[contains(.,'"+variables.createdContactLname+",...')]");
            // browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            // browser.pause(2000);
            // browser.elements("xpath", elements.contactsPageElements.contactMessageTable, function (result) {
            //     var els = result.value;
            //     els.forEach(function (el) {
            //         browser.elementIdText(el.ELEMENT, function (text) {
            //             expect(text.value).to.not.equal(variables.campaignAudienceMsgSubject_blank);
            //         });
            //     });
            // });
            // browser.pause(2000);
            // browser.back();
        });
    });
};
exports.send_ContactsMessage_Without_Msg = send_ContactsMessage_Without_Msg;

var send_ContactsMessage_With_Msg_And_MsgTemplate = function () {
    describe("Sending a message to audience with message template", function () {
        it("should check - Enter text in the message input box - ensure that the text is displayed in the user's email", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSendMessageLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignMsgTemplateMsgTemplateDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(@value,'"+variables.createdMsgTemplate+"')]");
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentCampaignMessageSubject = variables.campaignAudienceMsgSubject;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupSubjectInput, variables.campaignAudienceMsgSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupMessageInput,variables.campaignAudienceMsg);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceMsgPopupSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//button[contains(.,'Yes')]");
            browser.pause(2000);

            // commands.checkAndPerform('click', browser, "//a[contains(.,'"+variables.createdContactLname+",...')]");
            // browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            // browser.pause(2000);
            // browser.expect.element("//a[contains(@title,'"+variables.sentCampaignMessageSubject+"')]").to.be.present;
            // browser.pause(2000);
            // browser.back();
            // browser.pause(2000);
        });
    });
};
exports.send_ContactsMessage_With_Msg_And_MsgTemplate = send_ContactsMessage_With_Msg_And_MsgTemplate;

var send_ContactsMessage_Without_Msg_And_MsgTemplate = function () {
    describe("Sending a campaign message to audience without message template", function () {
        it("should check - Leave the message input box blank - user wont get an  email", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceSendMessageLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignMsgTemplateMsgTemplateDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(@value,'"+variables.createdMsgTemplate+"')]");
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupFromInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            variables.sentCampaignMessageSubject = variables.campaignAudienceMsgSubject_blank;
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignAudiencePopupSubjectInput, variables.campaignAudienceMsgSubject_blank);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignAudienceMsgPopupSendBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//button[contains(.,'Yes')]");
            browser.pause(2000);

            // commands.checkAndPerform('click', browser, "//a[contains(.,'"+variables.createdContactLname+",...')]");
            // browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            // browser.pause(2000);
            // browser.elements("xpath", elements.contactsPageElements.contactMessageTable, function (result) {
            //     var els = result.value;
            //     els.forEach(function (el) {
            //         browser.elementIdText(el.ELEMENT, function (text) {
            //             expect(text.value).to.not.equal(variables.campaignAudienceMsgSubject_blank);
            //         });
            //     });
            // });
            // browser.pause(2000);
            // browser.back();
        });
    });
};
exports.send_ContactsMessage_Without_Msg_And_MsgTemplate = send_ContactsMessage_Without_Msg_And_MsgTemplate;

var check_Campaign_Search_In_Cloud_Search = function () {
    describe("Ensure user can search for a campaign in cloud search ", function () {
        it("Should check  - Ensure user can search for a campaign in cloud search ", function (browser) {

            var currentCampaignName = variables.createdCampaignContact;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCampaignName);
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('gs-campaigns');
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabHeaderOnCloudSearch);
            browser.pause(2000);
            browser.pause(2000);

        });
    });
}
exports.check_Campaign_Search_In_Cloud_Search = check_Campaign_Search_In_Cloud_Search;

var check_visibility_Of_Campaign_Object = function () {
    describe("Ensure campaign Name and campaign number is present ", function () {
        it("Should check - Ensure campaign Name and campaign number is present  ", function (browser) {
            browser.pause(2000);
            browser.assert.attributeContains(elements.campaignPageElements.campaignNumberCloudSearch, 'id', 'campaignNo');
            browser.expect.element(elements.campaignPageElements.campaignName).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_visibility_Of_Campaign_Object = check_visibility_Of_Campaign_Object;

var check_Case_Creation_From_CloudSearch_In_Campaign = function () {
    describe("Ensure user can create memo on campaign from cloud search  ", function () {
        it("Should check -Ensure user can create memo on campaign from cloud search  ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.campaignPageElements.campaignSearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.campaignPageElements.campaignSearchField, variables.createdCampaignContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignCaseIconOnCloudSearch);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.caseSubjectInputField, variables.caseSubject);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.saveBtnOnActivityPage);
            browser.expect.element(elements.activityPageElements.caseSubjectInputField).to.have.value.which.contains(variables.caseSubject);
            browser.pause(2000);
        });
    });
}
exports.check_Case_Creation_From_CloudSearch_In_Campaign = check_Case_Creation_From_CloudSearch_In_Campaign;

var check_Task_Creation_From_CloudSearch_In_Campaign = function () {
    describe("Ensure user can create Task on campaign from cloud search   ", function () {
        it("Should check -Ensure user can create Task on campaign from cloud search   ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.campaignPageElements.campaignSearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.campaignPageElements.campaignSearchField, variables.createdCampaignContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTaskIconOnCloudSearch);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.taskSubject);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.saveBtnOnActivityPage);
            browser.expect.element(elements.activityPageElements.taskSubjectInputField).to.have.value.which.contains(variables.taskSubject);
            browser.pause(2000);
        });
    });
}
exports.check_Task_Creation_From_CloudSearch_In_Campaign = check_Task_Creation_From_CloudSearch_In_Campaign;

var check_Memo_Creation_From_CloudSearch_In_Campaign = function () {
    describe("Ensure user can create Memo on campaign from cloud search   ", function () {
        it("Should check -Ensure user can create Memo on campaign from cloud search   ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.campaignPageElements.campaignSearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.campaignPageElements.campaignSearchField, variables.createdCampaignContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTaskIconOnCloudSearch);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.memoSubject);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.saveBtnOnActivityPage);
            browser.expect.element(elements.activityPageElements.memoSubjectInputField).to.have.value.which.contains(variables.memoSubject);
            browser.pause(2000);
        });
    });
}
exports.check_Memo_Creation_From_CloudSearch_In_Campaign = check_Memo_Creation_From_CloudSearch_In_Campaign;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END









//FUNCTIONS DECLARATION - START

var campaigns_Tab = function () {
    describe("CAMPAIGNS - TAB", function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Campaign_Tab();
        me.check_CampaignTab_Highlighted();             // Campaign Tab is highlighted
        me.check_Campaign_Button_Present();             // create new campaign' button presents with Campaign Write permission
        me.check_Campaign_Edit_Link_Present();          // Edit' link presents with Campign Write permission
        me.check_Campaign_Del_Link_Present();           // Del' link presents with Campaign Delete permission
        me.check_Pagination_Working();                  // x of y' Presents and Pages correctly

        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.check_Campaign_Button_Present_Fail();        // New Campaign' button presents without Campaign Write permission (Fail)
        // me.check_Campaign_Edit_Link_Not_Present();      // Edit' link presents without Campaign Write permission (Fail)
        // me.check_Campaign_Del_Link_Not_Present();       // Del' link presents without Campaign Delete permission (Fail)
        //
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.campaigns_Tab = campaigns_Tab;

var campaigns_CustomList = function () {
    describe("CAMPAIGNS - CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Campaign_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.campaignPageElements.campaignQuickFilterLink,
            elements.campaignPageElements.campaignsTable,
            elements.campaignPageElements.campaignFilterAvailColumns,
            elements.campaignPageElements.campaignFilterSelectedColumns,
            elements.campaignPageElements.campaignFilterAddColBtn,
            elements.campaignPageElements.campaignFilterRemoveColBtn,
            elements.campaignPageElements.campaignFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.campaignPageElements.campaignSettingsIcon,
            elements.campaignPageElements.campaignsTable,
            elements.campaignPageElements.campaignCustomListAvailColumns,
            elements.campaignPageElements.campaignCustomListSelectedColumns,
            elements.campaignPageElements.campaignCustomListNewLink,
            elements.campaignPageElements.campaignCustomListAddColBtn,
            elements.campaignPageElements.campaignCustomListRemoveColBtn,
            elements.campaignPageElements.campaignCustomListNewInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.campaignPageElements.campaignSettingsIcon,
            elements.campaignPageElements.campaignCustomListNewLink,
            elements.campaignPageElements.campaignCustomListOrderLink,
            elements.campaignPageElements.campaignCustomListNewInput,
            elements.campaignPageElements.campaignCustomListNewSaveBtn,
            elements.campaignPageElements.campaignCustomListOrderDiv,
            elements.campaignPageElements.campaignCustomListOrderCloseBtn,
            elements.campaignPageElements.campaignCustomListAddColBtn,
            elements.campaignPageElements.campaignCustomListRemoveColBtn,
            elements.campaignPageElements.campaignCustomListAvailColumns,
            elements.campaignPageElements.campaignCustomListSelectedColumns,
            elements.campaignPageElements.campaignsTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.campaignPageElements.campaignSettingsIcon,
            elements.campaignPageElements.campaignCustomListEditLink,
            elements.campaignPageElements.campaignCustomListDeleteLink,
            elements.campaignPageElements.campaignCustomListNewInput,
            elements.campaignPageElements.campaignCustomListNewSaveBtn,
            elements.campaignPageElements.campaignCustomListOrderLink,
            elements.campaignPageElements.campaignCustomListOrderDiv,
            elements.campaignPageElements.campaignCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.campaignPageElements.campaignSettingsIcon,
            elements.campaignPageElements.campaignCustomListCloneInput,
            elements.campaignPageElements.campaignCustomListCloneLink,
            elements.campaignPageElements.campaignCustomListCloneCreateBtn,
            elements.campaignPageElements.campaignCustomListOrderLink,
            elements.campaignPageElements.campaignCustomListOrderDiv,
            elements.campaignPageElements.campaignCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.campaignPageElements.campaignSettingsIcon,
            elements.campaignPageElements.campaignCustomListEditLink,
            elements.campaignPageElements.campaignCustomListDeleteLink,
            elements.campaignPageElements.campaignCustomListOrderLink,
            elements.campaignPageElements.campaignCustomListOrderDiv,
            elements.campaignPageElements.campaignCustomListOrderCloseBtn);// delete a custom list
    });
};
exports.campaigns_CustomList = campaigns_CustomList;

var campaigns_Create_CampaignType = function () {
    describe("CAMPAIGNS - CREATE CAMPAIGN TYPE", function () {
        // adminOperations.create_CampaignType();      // Create new campaign type
        me.pauseBrowserSomeTime();
        me.edit_CampaignType();                     // edit exiting campaign type
        me.delete_CampaignType();                   // Delete a campaign type
                                                    // Ensure that the deleted campaign types are displayed user the inactive list
        me.reactive_CampaignType();                 // Re active a campaign type
    });
};
exports.campaigns_Create_CampaignType = campaigns_Create_CampaignType;

var campaigns_Create_CampaignStage = function () {
    describe("CAMPAIGNS - CREATE CAMPAIGN STAGE", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_CampaignStage(); // Create new campaign stage

        me.edit_CampaignStage();                // edit exiting campaign stage
        me.delete_CampaignStage();              // Delete a campaign stage
                                                // Ensure that the deleted campaign stage are displayed user the inactive list
        me.reactive_CampaignStage();            // Re active a campaign stage
    });
};
exports.campaigns_Create_CampaignStage = campaigns_Create_CampaignStage;

var campaigns_GeneralInfo = function () {
    describe("CAMPAIGNS - GENERAL INFO", function () {
        // adminOperations.create_CampaignType();
        // orgOperations.create_New_Org();
        // me.create_Campaign('contact',false);
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_Campaign_Icon();           // Ensure that campaign icon is displayed properly
        me.check_Campaign_Name_Display();   // Ensure that the campaign name is displayed


        me.edit_Campaign();             // Cancel changes and delete links are displayed
                                        // Cancel changes - Ensure that the changes are not saved
                                        // Edit campaign name
                                        // edit campaign type
                                        // Save , save and back button are displayed
                                        // Edit a campaign and save - Ensure that the changes are saved

        me.check_SaveAndBack();         // Save and back - Ensure that the user is brought to the previous page
        me.navigate_To_New_Campaign('contact');
        me.delete_Campaign();           // Delete Record - Ensure that the user can delete a campaign with delete permission
    });
};
exports.campaigns_GeneralInfo = campaigns_GeneralInfo;

var campaigns_Activities_SubTab = function () {
    describe("CAMPAIGNS - ACTIVITIES SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Campaign('contact');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_Activity_Icons();                          // Activities icons are displayed if the user has activities permission
        me.create_Case_On_Campaign();                       // Create case on campaign
        me.create_Task_On_Campaign();                       // Create Task on campaign
        me.create_Memo_On_Campaign();                       // Create memo on campaign
        me.check_Campaign_Edit_Icon_Display();                       // edit icon is displayed if the user has write permission
        me.check_Campaign_Delete_Icon_Display();                     // delete icon is displayed if the user has del permission



        me.check_Campaign_DateAndTime();               // Ensure that the created name and timestamp are displayed correctly
        me.check_Campaign_CreatedUser();                // Ensure that Modified by and Timestamp are displayed correctly
        me.check_Campaign_Modified_DateAndTime();
        me.check_Campaign_Modified_User();
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.check_Campaign_Edit_Icon_Display_Fail();         // edit icon is displayed without write permission (fail)
        // me.check_Campaign_Delete_Icon_Display_Fail();       // del icon is displayed without del permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.campaigns_Activities_SubTab = campaigns_Activities_SubTab;

var campaigns_Activities_SubTab_CustomList = function () {
    describe("CAMPAIGN ACTIVITIES SUBTAB CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.navigate_To_Activities_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.campaignPageElements.campaignActivityQuickFilterLink,
            elements.campaignPageElements.campaignActivityTable,
            elements.campaignPageElements.campaignActivityFilterAvailColumns,
            elements.campaignPageElements.campaignActivityFilterSelectedColumns,
            elements.campaignPageElements.campaignActivityFilterAddColBtn,
            elements.campaignPageElements.campaignActivityFilterRemoveColBtn,
            elements.campaignPageElements.campaignActivityFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.campaignPageElements.campaignActivitySettingsIcon,
            elements.campaignPageElements.campaignActivityTable,
            elements.campaignPageElements.campaignActivityCustomListAvailColumns,
            elements.campaignPageElements.campaignActivityCustomListSelectedColumns,
            elements.campaignPageElements.campaignActivityCustomListNewLink,
            elements.campaignPageElements.campaignActivityCustomListAddColBtn,
            elements.campaignPageElements.campaignActivityCustomListRemoveColBtn,
            elements.campaignPageElements.campaignActivityCustomListNewNameInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.campaignPageElements.campaignActivitySettingsIcon,
            elements.campaignPageElements.campaignActivityCustomListNewLink,
            elements.campaignPageElements.campaignActivityCustomListOrderLink,
            elements.campaignPageElements.campaignActivityCustomListNewNameInput,
            elements.campaignPageElements.campaignActivityCustomListNewSaveBtn,
            elements.campaignPageElements.campaignActivityCustomListOrderDiv,
            elements.campaignPageElements.campaignActivityCustomListOrderCloseBtn,
            elements.campaignPageElements.campaignActivityCustomListAddColBtn,
            elements.campaignPageElements.campaignActivityCustomListRemoveColBtn,
            elements.campaignPageElements.campaignActivityCustomListAvailColumns,
            elements.campaignPageElements.campaignActivityCustomListSelectedColumns,
            elements.campaignPageElements.campaignActivityTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.campaignPageElements.campaignActivitySettingsIcon,
            elements.campaignPageElements.campaignActivityCustomListEditLink,
            elements.campaignPageElements.campaignActivityCustomListNewNameInput,
            elements.campaignPageElements.campaignActivityCustomListNewSaveBtn,
            elements.campaignPageElements.campaignActivityCustomListOrderLink,
            elements.campaignPageElements.campaignActivityCustomListOrderDiv,
            elements.campaignPageElements.campaignActivityCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.campaignPageElements.campaignActivitySettingsIcon,
            elements.campaignPageElements.campaignActivityCustomListCloneNameInput,
            elements.campaignPageElements.campaignActivityCustomListCloneLink,
            elements.campaignPageElements.campaignActivityCustomListCloneCreateBtn,
            elements.campaignPageElements.campaignActivityCustomListOrderLink,
            elements.campaignPageElements.campaignActivityCustomListOrderDiv,
            elements.campaignPageElements.campaignActivityCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.campaignPageElements.campaignActivitySettingsIcon,
            elements.campaignPageElements.campaignActivityCustomListEditLink,
            elements.campaignPageElements.campaignActivityCustomListDeleteLink,
            elements.campaignPageElements.campaignActivityCustomListOrderLink,
            elements.campaignPageElements.campaignActivityCustomListOrderDiv,
            elements.campaignPageElements.campaignActivityCustomListOrderCloseBtn);// delete a custom list

    });
};
exports.campaigns_Activities_SubTab_CustomList = campaigns_Activities_SubTab_CustomList;

var campaigns_Messages_SubTab = function () {
    describe("CAMPAIGNS - MESSAGES SUBTAB", function () {

        // me.create_Campaign('contact');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.navigate_To_Messages_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.campaignPageElements.campaignMessageQuickFilterLink,
            elements.campaignPageElements.campaignMessageTable,
            elements.campaignPageElements.campaignMessageFilterAvailColumns,
            elements.campaignPageElements.campaignMessageFilterSelectedColumns,
            elements.campaignPageElements.campaignMessageFilterAddColBtn,
            elements.campaignPageElements.campaignMessageFilterRemoveColBtn,
            elements.campaignPageElements.campaignMessageFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.campaignPageElements.campaignMessageSettingsIcon,
            elements.campaignPageElements.campaignMessageTable,
            elements.campaignPageElements.campaignMessageCustomListAvailColumns,
            elements.campaignPageElements.campaignMessageCustomListSelectedColumns,
            elements.campaignPageElements.campaignMessageCustomListNewLink,
            elements.campaignPageElements.campaignMessageCustomListAddColBtn,
            elements.campaignPageElements.campaignMessageCustomListRemoveColBtn,
            elements.campaignPageElements.campaignMessageCustomListNewNameInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.campaignPageElements.campaignMessageSettingsIcon,
            elements.campaignPageElements.campaignMessageCustomListNewLink,
            elements.campaignPageElements.campaignMessageCustomListOrderLink,
            elements.campaignPageElements.campaignMessageCustomListNewNameInput,
            elements.campaignPageElements.campaignMessageCustomListNewSaveBtn,
            elements.campaignPageElements.campaignMessageCustomListOrderDiv,
            elements.campaignPageElements.campaignMessageCustomListOrderCloseBtn,
            elements.campaignPageElements.campaignMessageCustomListAddColBtn,
            elements.campaignPageElements.campaignMessageCustomListRemoveColBtn,
            elements.campaignPageElements.campaignMessageCustomListAvailColumns,
            elements.campaignPageElements.campaignMessageCustomListSelectedColumns,
            elements.campaignPageElements.campaignMessageTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.campaignPageElements.campaignMessageSettingsIcon,
            elements.campaignPageElements.campaignMessageCustomListEditLink,
            elements.campaignPageElements.campaignMessageCustomListNewNameInput,
            elements.campaignPageElements.campaignMessageCustomListNewSaveBtn,
            elements.campaignPageElements.campaignMessageCustomListOrderLink,
            elements.campaignPageElements.campaignMessageCustomListOrderDiv,
            elements.campaignPageElements.campaignMessageCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.campaignPageElements.campaignMessageSettingsIcon,
            elements.campaignPageElements.campaignMessageCustomListCloneNameInput,
            elements.campaignPageElements.campaignMessageCustomListCloneLink,
            elements.campaignPageElements.campaignMessageCustomListCloneCreateBtn,
            elements.campaignPageElements.campaignMessageCustomListOrderLink,
            elements.campaignPageElements.campaignMessageCustomListOrderDiv,
            elements.campaignPageElements.campaignMessageCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.campaignPageElements.campaignMessageSettingsIcon,
            elements.campaignPageElements.campaignMessageCustomListEditLink,
            elements.campaignPageElements.campaignMessageCustomListDeleteLink,
            elements.campaignPageElements.campaignMessageCustomListOrderLink,
            elements.campaignPageElements.campaignMessageCustomListOrderDiv,
            elements.campaignPageElements.campaignMessageCustomListOrderCloseBtn);// delete a custom list

    });
};
exports.campaigns_Messages_SubTab = campaigns_Messages_SubTab;

var campaigns_Contacts_Audience = function () {
    describe("CAMPAIGNS - CONTACTS AND PROVIDERS AUDIENCE", function () {
        // adminOperations.create_CampaignType();
        // adminOperations.create_CampaignStage();
        // adminOperations.performCampaignDependencyMapping();
        // orgOperations.create_New_Org();
        // me.create_Campaign('contact',false);
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_Campaign_Create_Contact_Link();        // Create new contact is present if the user has contact write permission

        me.check_Audience_Display();                    // Ensure that audience list view is displayed correctly

        me.add_Contact_in_Campaign();                   // Add a existing contact on a campaign
                                                        // Ensure that the contact and provider is added in the campaign audience
                                                        // Ensure that the Audience count is displayed correctly in the list view
                                                        // Create a new contact and add on a campaign
                                                        // Add campaign stage
        // adminOperations.create_CampaignType();
        // adminOperations.create_CampaignStage();
        // adminOperations.performCampaign_MultipleDependencyMapping();
        me.navigate_To_New_Campaign('contact');
        me.edit_Contact_Campaign_Stage();               //edit campaign stage on a contact
        me.create_Case_On_Contact();                    // Create a case on a contact
        me.create_Task_On_Contact();                    // Create a Task on contact
        me.create_Memo_On_Contact();                    // Create memo on contact
        me.create_Opportunity_On_Contact();                   // create opportunity on contact
        me.check_Campaign_In_Activity();                // Ensure that the campaign is listed as a related item on the activities
        me.navigate_To_New_Campaign('contact');
        me.delete_Contact_From_Campaign();              // delete a contact

    });
};
exports.campaigns_Contacts_Audience = campaigns_Contacts_Audience;

var campaigns_Contacts_Audience_CustomList = function () {
    describe("CAMPAIGN CONTACTS AUDIENCE CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.navigate_To_Audience_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.campaignPageElements.campaignAudienceQuickFilterLink,
            elements.campaignPageElements.campaignAudienceTable,
            elements.campaignPageElements.campaignAudienceFilterAvailColumns,
            elements.campaignPageElements.campaignAudienceFilterSelectedColumns,
            elements.campaignPageElements.campaignAudienceFilterAddColBtn,
            elements.campaignPageElements.campaignAudienceFilterRemoveColBtn,
            elements.campaignPageElements.campaignAudienceFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.campaignPageElements.campaignAudienceSettingsIcon,
            elements.campaignPageElements.campaignAudienceTable,
            elements.campaignPageElements.campaignAudienceCustomListAvailColumns,
            elements.campaignPageElements.campaignAudienceCustomListSelectedColumns,
            elements.campaignPageElements.campaignAudienceCustomListNewLink,
            elements.campaignPageElements.campaignAudienceCustomListAddColBtn,
            elements.campaignPageElements.campaignAudienceCustomListRemoveColBtn,
            elements.campaignPageElements.campaignAudienceCustomListNewNameInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.campaignPageElements.campaignAudienceSettingsIcon,
            elements.campaignPageElements.campaignAudienceCustomListNewLink,
            elements.campaignPageElements.campaignAudienceCustomListOrderLink,
            elements.campaignPageElements.campaignAudienceCustomListNewNameInput,
            elements.campaignPageElements.campaignAudienceCustomListNewSaveBtn,
            elements.campaignPageElements.campaignAudienceCustomListOrderDiv,
            elements.campaignPageElements.campaignAudienceCustomListOrderCloseBtn,
            elements.campaignPageElements.campaignAudienceCustomListAddColBtn,
            elements.campaignPageElements.campaignAudienceCustomListRemoveColBtn,
            elements.campaignPageElements.campaignAudienceCustomListAvailColumns,
            elements.campaignPageElements.campaignAudienceCustomListSelectedColumns,
            elements.campaignPageElements.campaignAudienceTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.campaignPageElements.campaignAudienceSettingsIcon,
            elements.campaignPageElements.campaignAudienceCustomListEditLink,
            elements.campaignPageElements.campaignAudienceCustomListNewNameInput,
            elements.campaignPageElements.campaignAudienceCustomListNewSaveBtn,
            elements.campaignPageElements.campaignAudienceCustomListOrderLink,
            elements.campaignPageElements.campaignAudienceCustomListOrderDiv,
            elements.campaignPageElements.campaignAudienceCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.campaignPageElements.campaignAudienceSettingsIcon,
            elements.campaignPageElements.campaignAudienceCustomListCloneNameInput,
            elements.campaignPageElements.campaignAudienceCustomListCloneLink,
            elements.campaignPageElements.campaignAudienceCustomListCloneCreateBtn,
            elements.campaignPageElements.campaignAudienceCustomListOrderLink,
            elements.campaignPageElements.campaignAudienceCustomListOrderDiv,
            elements.campaignPageElements.campaignAudienceCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.campaignPageElements.campaignAudienceSettingsIcon,
            elements.campaignPageElements.campaignAudienceCustomListEditLink,
            elements.campaignPageElements.campaignAudienceCustomListDeleteLink,
            elements.campaignPageElements.campaignAudienceCustomListOrderLink,
            elements.campaignPageElements.campaignAudienceCustomListOrderDiv,
            elements.campaignPageElements.campaignAudienceCustomListOrderCloseBtn);// delete a custom list
    });
};
exports.campaigns_Contacts_Audience_CustomList = campaigns_Contacts_Audience_CustomList;

var campaigns_Send_A_Message_To_Contacts_Without_Msg_Template = function () {
    describe("CAMPAIGNS - SEND A MESSAGE TO CONTACTS AUDIENCE WITHOUT MESSAGE TEMPLATE", function () {
        // orgOperations.create_New_Org();
        // contactOperations.create_Contact('contact');
        // me.create_Campaign('contact',true);
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_SendMessage_Link_Present();        // "Send a message to this Audience" link is present with  campaign message write permission
        me.check_SendMessage_Popup_Display();       // Select "send a message to this audience" - Send a message pop-up is displayed
        me.check_SendMessage_Required_Fields();     // Send to is a required field
                                                    // All is set as default
                                                    // Select a template is a required field
                                                    // From is a required field
                                                    // From field is auto populated with the current user
                                                    // Edit the from field
                                                    // Subject is a required field
                                                    // Enter a a subject
        me.check_SendMessage_Attachment_Link();     // Attachment link is present if users have attachment write permission
                                                    // Ensure that the users can add attachments successfully
                                                    // Attachments should be listed in the messages pop-up

        me.send_ContactsMessage_With_Msg();         // Enter text in the message input box - ensure that the text is displayed in the user's email
                                                    // Send a message without entering the required field (fail)
                                                    // Enter all the fields and send a message
                                                    // Ensure that all the audience receive the campaign message
        me.send_ContactsMessage_Without_Msg();      // Leave the message input box blank - no text is displayed in the users email

        // Enter text in the text message input box- Ensure that the text is displayed in the text message
        // leave the text message input box blank - no text is displayed in the message
        // Ensure that the user gets a error when they try to enter more than 130 characters in the input field

        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Campaign('contact');
        // me.check_SendMessage_Link_Not_Present();    // "Send a message to this Audience" link is present without campaign message write permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

    });
};
exports.campaigns_Send_A_Message_To_Contacts_Without_Msg_Template = campaigns_Send_A_Message_To_Contacts_Without_Msg_Template;

var campaigns_MessageTemplate_SubTab = function () {
    describe("CAMPAIGNS - MESSAGE TEMPLATE SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Campaign('contact');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_MessageTemplate_Highlighted();         // Message template subtab is high lighted
        me.check_CreateNewMessageTemplate_Display();     // Create new message template link is displayed
        me.check_MessageTemplateList_Display();          // Ensure that the message template list is displayed properly
        //This will be covered in campaigns_CreateMessageTemplate                 // Edit message templates
        //This will be covered in campaigns_CreateMessageTemplate               // Users should be able to delete message templates
    });
};
exports.campaigns_MessageTemplate_SubTab = campaigns_MessageTemplate_SubTab;

var campaigns_MessageTemplate_SubTab_CustomList = function () {
    describe("CAMPAIGN MESSAGE TEMPLATE SUBTAB CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.navigate_To_Messages_Template_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.campaignPageElements.campaignMsgTemplateQuickFilterLink,
            elements.campaignPageElements.campaignMsgTemplateTable,
            elements.campaignPageElements.campaignMsgTemplateFilterAvailColumns,
            elements.campaignPageElements.campaignMsgTemplateFilterSelectedColumns,
            elements.campaignPageElements.campaignMsgTemplateFilterAddColBtn,
            elements.campaignPageElements.campaignMsgTemplateFilterRemoveColBtn,
            elements.campaignPageElements.campaignMsgTemplateFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.campaignPageElements.campaignMsgTemplateSettingsIcon,
            elements.campaignPageElements.campaignMsgTemplateTable,
            elements.campaignPageElements.campaignMsgTemplateCustomListAvailColumns,
            elements.campaignPageElements.campaignMsgTemplateCustomListSelectedColumns,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListAddColBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListRemoveColBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewNameInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.campaignPageElements.campaignMsgTemplateSettingsIcon,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewNameInput,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewSaveBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderDiv,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderCloseBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListAddColBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListRemoveColBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListAvailColumns,
            elements.campaignPageElements.campaignMsgTemplateCustomListSelectedColumns,
            elements.campaignPageElements.campaignMsgTemplateTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.campaignPageElements.campaignMsgTemplateSettingsIcon,
            elements.campaignPageElements.campaignMsgTemplateCustomListEditLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewNameInput,
            elements.campaignPageElements.campaignMsgTemplateCustomListNewSaveBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderDiv,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.campaignPageElements.campaignMsgTemplateSettingsIcon,
            elements.campaignPageElements.campaignMsgTemplateCustomListCloneNameInput,
            elements.campaignPageElements.campaignMsgTemplateCustomListCloneLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListCloneCreateBtn,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderDiv,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.campaignPageElements.campaignMsgTemplateSettingsIcon,
            elements.campaignPageElements.campaignMsgTemplateCustomListEditLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListDeleteLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderLink,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderDiv,
            elements.campaignPageElements.campaignMsgTemplateCustomListOrderCloseBtn);// delete a custom list
    });
};
exports.campaigns_MessageTemplate_SubTab_CustomList = campaigns_MessageTemplate_SubTab_CustomList;

var campaigns_CreateMessageTemplate = function () {
    describe("CAMPAIGNS - CREATE A MESSAGE TEMPLATE", function () {

        // orgOperations.create_New_Org();
        // me.create_Campaign('contact');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_CreateMessageTemplate();   // Message template pop-up is displayed
                                            // Name is a required field
                                            // Enter message template name
                                            // Subject is a required field
                                            // Add subject
                                            // Add email message text
                                            // Add Text message text
                                            // Attachment link is displayed with attachment write permission
                                            // Attachment link is displayed without attachment write permission (fail)
                                            // Send a message without entering the required field (fail)
                                            // Enter all the required fields and create a message template successfully
                                            // Ensure that the message template is displayed in list
                                            // Ensure that the message template is displayed in template dropdown in messages pop-up
        me.navigate_To_New_Campaign('contact');
        me.edit_MessageTemplate();         // Edit a message template
        me.navigate_To_New_Campaign('contact');
        me.delete_MessageTemplate();       // delete a message template

    });
};
exports.campaigns_CreateMessageTemplate = campaigns_CreateMessageTemplate;

var campaigns_PatientAudience = function () {
    describe("CAMPAIGNS - PATIENT AUDIENCE", function () {
        // adminOperations.create_CampaignType();
        // adminOperations.create_CampaignStage();
        // adminOperations.performCampaignDependencyMapping();
        // orgOperations.create_New_Org();
        // me.create_Campaign('patient');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('patient');
        me.check_CreatePatientLink_Display();       // Create new patient is present if the user has patient write permission

        me.add_Patient_In_Campaign();               // Create a new patient and add on a campaign
                                                    // Add campaign stage
                                                    // Ensure that the patient is added in the campaign audience
        me.create_Case_In_Campaign_Patient();       // Create a case on a patient
        me.create_Task_In_Campaign_Patient();       // Create a Task on patient
        me.create_Memo_In_Campaign_Patient();       // Create memo on patient
        me.delete_Patient();                        // delete a patient
        patientOperations.create_Patient();
        me.navigate_To_New_Campaign('patient');
        me.add_Existing_Patient_In_Campaign();      // Add a existing patient on a campaign
                                                    // Ensure that the patient is added in the campaign audience
        me.check_Patient_CustomList();              // Ensure that audience list view is displayed correctly

        //Already covered in campaigns_Contacts_Audience    // Ensure that the campaign is listed as a related item on the activities
        //Already covered in campaigns_Contacts_Audience    // Ensure that Display Columns behaves properly (check with all the fields)
        //Already covered in campaigns_Contacts_Audience    // Ensure that Quick Filter behaves properly (check with all the filters)
        //Already covered in campaigns_Contacts_Audience    // Create new custom list
        //Already covered in campaigns_Contacts_Audience    // Edit  existing custom list
        //Already covered in campaigns_Contacts_Audience    // clone custom list
        //Already covered in campaigns_Contacts_Audience    // Delete a custom list

        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Campaign('patient');
        // me.check_CreatePatientLink_Display_Fail();  // Create new patient is present without write permission (fail)
        //
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.campaigns_PatientAudience = campaigns_PatientAudience;

var campaigns_AuditLog = function () {
    describe("CAMPAIGNS - AUDIT LOG", function () {
        // If the user has aduit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page
        //
        // Edit a campaign - changes should be displayed in the audit history
        // Ensure that the timestamp, type, user and changes are displayed correctly
        // Ensure that the start date and end date behaves properly
        // If the user has export permission ensure that the user can export the file
        //
        // Ensure that it displays the list of all the users who accessed the  record
        // Ensure that the timestamp, type, Entity type, Entity Name and user are displayed correctly
        // Ensure that the start date and end date behaves properly
        // If the user has export permission ensure that the user can export the file
        //
        // Ensure that all history displays all the user who viewd the record and the changes they made
        // Ensure that the timestamp, type, Entity type, Entity Name, user and changes are displayed correctly
        // Ensure that the start date and end date behaves properly
        // If the user has export permission ensure that the user can export the file

        // orgOperations.create_New_Org();
        // me.create_Campaign('contact');
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        //Audit log
        commonOperations.check_auditLink(elements.auditLogElements.auditLogLink);// If the user has audit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page

        //Audit history
        commonOperations.check_auditHistoryTab(elements.campaignPageElements.campaignEditBtn,
            elements.campaignPageElements.campaignDetailPageNameInput,
            elements.campaignPageElements.campaignSaveBtn,
            elements.auditLogElements.auditLogLink,
            elements.auditLogElements.startDateDiv,
            elements.auditLogElements.endDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.auditLogsTable,
            elements.auditLogElements.exportLink,
            variables.updatedOrgName);

        //access history
        commonOperations.check_accessHistoryTab(elements.auditLogElements.accessHistoryTab,
            elements.auditLogElements.accessHistoryStartDateDiv,
            elements.auditLogElements.accessHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.accessHistoryRecordsTable,
            elements.auditLogElements.accessHistoryExportLink);

        // //All history
        commonOperations.check_allHistoryTab(elements.auditLogElements.allHistoryTab,
            elements.auditLogElements.allHistoryStartDateDiv,
            elements.auditLogElements.allHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.allHistoryRecordsTable,
            elements.auditLogElements.allHistoryExportLink);

        commonOperations.close_AuditLogDiv();
    });
};
exports.campaigns_AuditLog = campaigns_AuditLog;

var campaigns_Send_A_Message_To_Contacts_With_Msg_Template = function () {
    describe("CAMPAIGNS - SEND A MESSAGE TO CONTACTS AUDIENCE WITH MESSAGE TEMPLATE", function () {
        // orgOperations.create_New_Org();
        // contactOperations.create_Contact('contact');
        // me.create_Campaign('contact',true);
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Campaign('contact');
        me.check_CreateMessageTemplate();
        me.check_SendMessage_Link_Present();                // "Send a message to this Audience" link is present with  campaign message write permission
        me.check_SendMessage_Popup_Display();                // Select "send a message to this audience" - Send a message pop-up is displayed
        me.check_SendMessage_Required_Fields(); // Send to is a required field
                                                // Delete a template is a required field
                                                // From is a required field
                                                // Subject is a required field
        me.select_Msg_Template();               // Select a message template from the drop down
                                                // Ensure that the text is populated
                                                //Ensure that the attachments are added (if any)
                                                // Ensure that user can edit the text
        me.check_SendMessage_Attachment_Link(); // Attachment link is present if users have attachment write permission

        me.send_ContactsMessage_With_Msg_And_MsgTemplate();     // Enter text in the message input box - ensure that the text is displayed in the user's email
        me.send_ContactsMessage_Without_Msg_And_MsgTemplate();  // Leave the message input box blank - user wont get an  email
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Campaign('contact');
        // me.check_SendMessage_Link_Not_Present();    // "Send a message to this Audience" link is present without campaign message write permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.campaigns_Send_A_Message_To_Contacts_With_Msg_Template = campaigns_Send_A_Message_To_Contacts_With_Msg_Template;

var cloud_Search_For_Campaign = function () {
    describe(" SEARCH FOR CAMPAIGN OBJECT ", function () {
        // orgOperations.create_New_Org();
        // me.create_Campaign('contact', false);
        me.navigate_To_New_Campaign('contact');
        me.check_Campaign_Search_In_Cloud_Search();                             //Ensure that user can search for an Campaign
                                                                                // Ensure user can search for a campaign in cloud search
                                                                                //Select a campaign > Ensure user is redirected to campaign detail page
        me.check_visibility_Of_Campaign_Object();                               //Ensure campaign Name and campaign number is present
        me.check_Case_Creation_From_CloudSearch_In_Campaign();                  //Ensure user can create case on campaign from cloud search
        me.check_Task_Creation_From_CloudSearch_In_Campaign();                  //Ensure user can create task on campaign from cloud search
        me.check_Memo_Creation_From_CloudSearch_In_Campaign();                  //Ensure user can create memo on campaign from cloud search
    });
}
exports.cloud_Search_For_Campaign = cloud_Search_For_Campaign;

var campaigns_Sheet = function () {
    describe("RESOLVE CAMPAIGN SHEET DEPENDENCIES", function () {

        //CAMPAIGN DEPENDENCY RESOLVING - STARTS
        // commands.fillDependencyRequiredList([
        //     'controlTreeNameCreated',
        //     'createdORG',
        //     'createdContact',
        //     'createdPatient',
        //     'createdCampaignContact',
        //     'createdCampaignPatient',
        // ]);
        // adminOperations.create_New_Control_Tree();
        // orgOperations.create_New_Org();
        // contactOperations.create_Contact('contact');
        // patientOperations.create_Patient();
        // me.create_Campaign('patient');
        // me.create_Campaign('contact',true);
        // commands.checkDependencyListFulfilled();

        // Object created : controlTreeNameCreated with value : testControlTree_2017Jul19181417
        // Object created : createdORG with value : test_Org_2017Jul19181417
        // Object created : createdContact with value : conFName_17
        // Object created : createdPatient with value : testpatientFname_2017Jul19181417 testpatientLname_2017Jul19181417
        // Object created : createdCampaignContact with value : testCampaignContact_2017Jul19181417
        // Object created : createdCampaignPatient with value : testCampaignPatient_2017Jul19181417


        //CAMPAIGN DEPENDENCY RESOLVING - ENDS


        //CAMPAIGN SHEET EXECUTION - STARTS
        // me.campaigns_Tab();
        // me.campaigns_Create_CampaignType();
        // me.campaigns_Create_CampaignStage();
        me.campaigns_GeneralInfo();
        me.campaigns_Activities_SubTab();
        me.campaigns_Contacts_Audience();
        me.campaigns_Send_A_Message_To_Contacts_Without_Msg_Template();
        me.campaigns_MessageTemplate_SubTab();
        me.campaigns_CreateMessageTemplate();
        me.campaigns_PatientAudience();
        me.campaigns_AuditLog();
        me.campaigns_Send_A_Message_To_Contacts_With_Msg_Template();
        me.campaigns_Contacts_Audience();

        me.campaign_nonAdmin_Checks();

        me.campaigns_CustomList();
        me.campaigns_Activities_SubTab_CustomList();
        me.campaigns_Messages_SubTab();
        me.campaigns_Contacts_Audience_CustomList();
        me.campaigns_MessageTemplate_SubTab_CustomList();
        //CAMPAIGN SHEET EXECUTION - ENDS
    });
};
exports.campaigns_Sheet = campaigns_Sheet;

var campaign_nonAdmin_Checks = function () {
    describe("CAMPAIGNS NON ADMIN USER CHECKS", function () {
        me.pauseBrowserSomeTime();
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        navigateOperations.navigate_To_Campaign_Tab();
        me.check_Campaign_Button_Present_Fail();        // New Campaign' button presents without Campaign Write permission (Fail)
        me.check_Campaign_Edit_Link_Not_Present();      // Edit' link presents without Campaign Write permission (Fail)
        me.check_Campaign_Del_Link_Not_Present();       // Del' link presents without Campaign Delete permission (Fail)
        me.navigate_To_New_Campaign('contact');
        me.check_SendMessage_Link_Not_Present();    // "Send a message to this Audience" link is present without campaign message write permission (fail)
        me.check_CreatePatientLink_Display_Fail();  // Create new patient is present without write permission (fail)
        me.check_SendMessage_Link_Not_Present();    // "Send a message to this Audience" link is present without campaign message write permission (fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.campaign_nonAdmin_Checks = campaign_nonAdmin_Checks;
//FUNCTIONS DECLARATION - END

