// /**
//  * Created by INDUSA
//  */
// require('mocha');
// var moment = require('moment');
// var expect = require('chai').expect;
// var commands = require('../utility/commands.js');
// var elements = require('../utility/elements.js');
// var variables = require('../utility/variables.js');
// var commonOperations = require('../pages/commons.js');
// var userAuthentication = require('../pages/userAuthentication.js');
// var orgOperations = require('../pages/organization.js');
// var activityOperations = require('../pages/activities.js');
// var adminOperations = require('../pages/admin.js');
// var contactsOperations = require('../pages/contacts.js');
// var navigate = require('../utility/navigate.js');
// var me = require('../pages/test.js');
//
// var cate = function () {
//     describe("", function () {
//         // me.create_Category();
//         // me.inactivate_category();
//         // me.reactivate_category();
//         navigate.navigate_To_Org_Tab();
//         me.test_fileupload();
//     });
// };
// exports.cate = cate;
//
//
// var create_Category = function () {
//     describe("Create a new category", function () {
//         it("should create a new category", function (browser) {
//             browser.pause(2000);
//             commands.checkAndPerform('click',browser,elements.globalElements.headerUsernameDropdown);
//             browser.pause(2000);
//             commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
//             browser.pause(2000);
//
//             //check if it already exists
//             browser.elements("xpath", elements.adminPageElements.actCategoryTable, function (result) {
//                 var els = result.value;
//                 els.forEach(function (el) {
//                     browser.elementIdText(el.ELEMENT, function (text) {
//                         //if it exists,then do nothing
//                         if(!text.value.includes(variables.newCategoryName))
//                         {
//                             commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryAddLink);
//                             var categoriesCount = 0;
//                             browser.elements("xpath",elements.adminPageElements.actCategoryTable+"/tbody/tr",  function  (result) {
//                                 var  els = result.value;
//                                 els.forEach(function  () {
//                                     categoriesCount = categoriesCount + 1;
//                                 });
//                                 variables.createdActivityCategoryName = variables.newCategoryName;
//                                 commands.checkAndSetValue(browser,elements.adminPageElements.actCategoryTable+'/tbody/tr['+categoriesCount+']/td[1]/div/input',variables.newCategoryName);
//                             });
//                             commands.checkAndPerform('click', browser, elements.adminPageElements.actCategorySaveBtn);
//                             browser.pause(2000);
//                             browser.expect.element('//div[contains(.,"'+variables.newCategoryName+'")]').to.be.present;
//                         }
//                     });
//                 });
//             });
//
//             //check if it already exists
//
//             browser.pause(2000);
//         });
//     });
// };
// exports.create_Category = create_Category;
//
//
// var inactivate_category = function () {
//     describe("", function () {
//         it("inactivate category", function (browser) {
//             browser.pause(2000);
//             var nameIndex = 0;
//             commands.getColIndexOf(browser,'Activity Category','',elements.adminPageElements.actCategoryTable,function (result) {
//                console.log('name col index : '+result);
//                nameIndex = result;
//             });
//             browser.elements("xpath", elements.adminPageElements.actCategoryTable+"/tbody/tr", function (result) {
//                 var els = result.value;
//                 var count = 0;
//                 els.forEach(function (el) {
//                     browser.elementIdText(el.ELEMENT, function (text) {
//                         console.log('>>>>>>>>>>'+text.value);
//                         count = count + 1;
//                         if(text.value.includes(variables.createdActivityCategoryName)){
//                             console.log('name : '+text.value);
//                             browser.pause(2000);
//                             commands.checkAndPerform('click', browser, elements.adminPageElements.actCategoryTable+"/tbody/tr["+count+"]/td[2]/div/span");
//                             browser.pause(2000);
//                         }
//                     });
//                 });
//             });
//             browser.pause(2000);
//         });
//     });
// };
// exports.inactivate_category = inactivate_category;
//
// var reactivate_category = function () {
//     describe("", function () {
//         it("reactivate category", function (browser) {
//             variables.createdActivityCategoryName = 'testCategory_2017Apr19184658';
//
//
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser,elements.adminRelationshipManagementPageElements.activityCategoryInactiveLink);
//             browser.pause(2000);
//             browser.elements("xpath", elements.adminRelationshipManagementPageElements.activityCategoryInactiveTable+"/tbody/tr", function (result) {
//                 var els = result.value;
//                 var count = 0;
//                 els.forEach(function () {
//                     count= count + 1;
//                     browser.getValue(elements.adminRelationshipManagementPageElements.activityCategoryInactiveTable+"/tbody/tr["+count+"]/td/input",function (result) {
//
//                         if(result.value.includes(variables.createdActivityCategoryName)){
//                             console.log('inactive name to reactivate : '+result.value);
//                             commands.checkAndPerform('click', browser,elements.adminRelationshipManagementPageElements.activityCategoryInactiveTable+"/tbody/tr["+count+"]/td[2]/span");
//                         }
//                     });
//                 });
//             });
//             browser.pause(2000);
//         });
//     });
// };
// exports.reactivate_category = reactivate_category;
//
// var test_fileupload = function () {
//     describe("file upload test", function () {
//         it("should upload a file", function (browser) {
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser, "//a[contains(@title,'National Laboratories')]");
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser, elements.organizationPageElements.attachmentsSubTabBtn);
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsPageNewUploadLink);
//             browser.pause(2000);
//             commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadNameInput, variables.uploadName);
//             browser.pause(2000);
//             commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadFileInput, variables.uploadFilePath+"/"+variables.uploadFileName);
//             browser.pause(2000);
//             commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsUploadBtn);
//             browser.pause(4000);
//             browser.expect.element("//a[contains(@title,'"+variables.uploadFileName+"')]").to.be.present;
//         });
//     });
// };
// exports.test_fileupload = test_fileupload;




/**
 * Created by INDUSA
 */
require('mocha');
var expect = require('chai').expect;
var me = require('./test.js');
var adminOperations = require('./admin.js');
var messageOperations = require('./message.js');
var contactOperations = require('./contacts.js');
var navigateOperations = require('../utility/navigate.js');
var commonOperations = require('./commons.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var error = require('../utility/error.js');



var test1 = function () {
    describe("test1", function () {
        //test1

        me.test1_impl();
    });
};
module.exports.test1 = test1;

var test1_impl = function () {
    describe("test1_impl", function () {
        it("test1_impl", function (browser) {
            browser.pause(2000);
            console.log('############ inside test1_impl mocha test');
            browser.pause(2000);
        });
    });
};
exports.test1_impl = test1_impl;

///////////////////////////////////////////////////////////////////////////////////
var test2 = function () {
    describe("test2", function () {
        //test1
        // me.restart(function (results) {
        //     console.log('browser ends....now starting '+results);
        //     userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        // });

        me.test2_impl();
    });
};
module.exports.test2 = test2;

var test2_impl = function () {
    describe("test2_impl", function () {
        it("test2_impl", function (browser) {
            browser.pause(2000);
            console.log('############### inside test2_impl mocha test');
            browser.pause(2000);
        });
    });
};
exports.test2_impl = test2_impl;

///////////////////////////////////////////////////////////////////////////////////

var test3 = function () {
    describe("test3", function () {
        //test1
        // me.restart(function (results) {
        //     console.log('browser ends....now starting  : '+results);
        // });
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
        me.test3_impl();
    });
};
module.exports.test3 = test3;

var test3_impl = function () {
    describe("test3_impl", function () {
        it("test3_impl", function (browser) {
            browser.pause(2000);
            console.log('#################### inside test3_impl mocha test');
            browser.pause(2000);
        });
    });
};
exports.test3_impl = test3_impl;

///////////////////////////////////////////////////////////////////////////////////

var endbrowser = function (callback) {
    describe("restart browser", function () {
        it("should restart a browser", function (browser) {
            browser.pause(2000);
            browser.end();
            browser.pause(2000);
            callback(true);
        });
    });
};
exports.endbrowser = endbrowser;