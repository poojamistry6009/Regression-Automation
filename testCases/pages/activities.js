/**
 * Created by INDUSA
 */
require('mocha');
var moment = require('moment');
var expect = require('chai').expect;
var adminOperations = require('./admin.js');
var contactOperations = require('./contacts.js');
var patientOperations = require('./patient.js');
var orgOperations = require('./organization.js');
var navigateOperations = require('../utility/navigate.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var navigate = require('../utility/navigate.js');
var commonOperations = require('./commons.js');
var me = require('./activities.js');

//TEST CASE DECLARATION - START


var check_visibility_OF_Case_Object = function () {
    describe("Ensure that case object is displayed ", function () {
        it("Should check - Ensure that case object is displayed  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activityCaseSpan).to.be.present;
            browser.expect.element(elements.activityPageElements.activityOrgName).to.be.present;
            browser.expect.element(elements.activityPageElements.categoryCaseDiv).to.be.present;
            browser.expect.element(elements.activityPageElements.subCategoryCaseDiv).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_visibility_OF_Case_Object = check_visibility_OF_Case_Object;

var check_NewCase_Search_In_Cloud_Search = function () {
    describe("Ensure that user can search for a case in cloud search  ", function () {
        it(" Should check - Ensure that user can search for a case in cloud search ", function (browser) {
            browser.pause(2000);
            var elementValue = '';
            browser.getText(elements.activityPageElements.activityCaseSpan, function (result) {
                elementValue = result.value;
                browser.pause(2000);
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                browser.expect.element(elements.activityPageElements.activityTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('results-Case');
                commands.checkAndPerform('click', browser, elements.activityPageElements.activityTabHeaderOnCloudSearch);
                browser.pause(2000);
            });
            browser.pause(2000);

        });
    });
}
exports.check_NewCase_Search_In_Cloud_Search = check_NewCase_Search_In_Cloud_Search;

var check_Select_Display_Case_In_CloudSearch = function () {
    describe("Ensure that case detail page is displayed when the user selects the case ", function () {
        it("Should check - Ensure that case detail page is displayed when the user selects the case ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseCloudSearchDataLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activitiesTabLink).to.have.attribute('class').which.contain('selected-tab');
            browser.pause(2000);
        });
    });
}
exports.check_Select_Display_Case_In_CloudSearch = check_Select_Display_Case_In_CloudSearch;

var get_Case_value = function () {
    describe("Get case value  ", function () {
        it("Should check - Get case value  ", function (browser) {
            browser.pause(2000);
            var elementValue;
            browser.getText(elements.activityPageElements.activityCaseSpan, function (result) {
                elementValue = result.value;
            });
            browser.pause(2000);
            browser.perform(function (browser, done) {
                console.log('elementValue', elementValue);
                done();
            });
            browser.pause(2000);
        });
    });
}
exports.get_Case_value = get_Case_value;

var check_Create_Task_From_Activity_CloudSearch = function () {
    describe("Ensure that user can create task on a case from cloud search ", function () {
        it("Ensure that user can create task on a case from cloud search ", function (browser) {
            browser.pause(2000);
            var elementValue = '';
            browser.getText(elements.activityPageElements.activityCaseSpan, function (result) {
                elementValue = result.value;
                browser.pause(2000);
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.activityPageElements.activitiesTabLink);
                browser.pause(2000);
                var currentTask = variables.newTaskName;
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.homePageElements.cloudSearchTaskIcon);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.taskSubject)
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveBtn);
                browser.pause(2000);
            });
        });
    });
}
exports.check_Create_Task_From_Activity_CloudSearch = check_Create_Task_From_Activity_CloudSearch;

var check_Create_Memo_From_Activity_CloudSearch = function () {
    describe("Ensure that user can create Memo on a case from cloud search ", function () {
        it("Ensure that user can create Memo on a case from cloud search ", function (browser) {
            browser.getText(elements.activityPageElements.activityCaseSpan, function (result) {
                elementValue = result.value;
                browser.pause(2000);
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('click', browser, elements.activityPageElements.activitiesTabLink);
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.homePageElements.cloudSearchMemoIcon);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.memoSubject)
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
                browser.pause(2000);

            });
        });
    });
}
exports.check_Create_Memo_From_Activity_CloudSearch = check_Create_Memo_From_Activity_CloudSearch;

var check_visibility_OF_Task_Object = function () {
    describe("Ensure that case object is displayed ", function () {
        it("Should check - Ensure that case object is displayed  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activityTaskSpan).to.be.present;
            browser.expect.element(elements.activityPageElements.activityOrgName).to.be.present;
            browser.expect.element(elements.activityPageElements.categoryTaskDiv).to.be.present;
            browser.expect.element(elements.activityPageElements.subCategoryTaskDiv).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_visibility_OF_Task_Object = check_visibility_OF_Task_Object;

var check_New_Task_Search_In_Cloud_Search = function () {
    describe("Ensure that user can search for a Task in cloud search  ", function () {
        it(" Should check - Ensure that user can search for a task in cloud search ", function (browser) {
            browser.pause(2000);
            var elementValue;
            browser.getText(elements.activityPageElements.activityTaskSpan, function (result) {
                elementValue = result.value;
                browser.pause(2000);
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                browser.expect.element(elements.activityPageElements.activityTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('results-Task');
                commands.checkAndPerform('click', browser, elements.activityPageElements.activityTabHeaderOnCloudSearch);
                browser.pause(2000);
            });
            browser.pause(2000);
        });
    });
}
exports.check_New_Task_Search_In_Cloud_Search = check_New_Task_Search_In_Cloud_Search;

var check_Select_Display_Task_In_CloudSearch = function () {
    describe("Ensure that case detail page is displayed when the user selects the case ", function () {
        it("Should check - Ensure that case detail page is displayed when the user selects the case ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseCloudSearchDataLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activitiesTabLink).to.have.attribute('class').which.contain('selected-tab');
            browser.pause(2000);
        });
    });
}
exports.check_Select_Display_Task_In_CloudSearch = check_Select_Display_Task_In_CloudSearch;

var check_visibility_OF_Memo_Object = function () {
    describe("Ensure that case object is displayed ", function () {
        it("Should check - Ensure that case object is displayed  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activityTaskSpan).to.be.present;
            browser.expect.element(elements.activityPageElements.activityOrgName).to.be.present;
            browser.expect.element(elements.activityPageElements.categoryTaskDiv).to.be.present;
            browser.expect.element(elements.activityPageElements.subCategoryTaskDiv).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_visibility_OF_Memo_Object = check_visibility_OF_Memo_Object;

var check_New_Memo_Search_In_Cloud_Search = function () {
    describe("Ensure that user can search for a Task in cloud search  ", function () {
        it(" Should check - Ensure that user can search for a task in cloud search ", function (browser) {
            browser.pause(2000);
            var elementValue;
            browser.getText(elements.activityPageElements.activityMemoSpan, function (result) {
                elementValue = result.value;
                browser.pause(2000);
                var currentCaseNumber = elementValue;
                commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCaseNumber);
                browser.pause(2000);
                browser.expect.element(elements.activityPageElements.activityTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('results-Memo');
                commands.checkAndPerform('click', browser, elements.activityPageElements.activityTabHeaderOnCloudSearch);
                browser.pause(2000);
            });
            browser.pause(2000);
        });
    });
}
exports.check_New_Memo_Search_In_Cloud_Search = check_New_Memo_Search_In_Cloud_Search;

var check_Select_Display_Memo_In_CloudSearch = function () {
    describe("Ensure that case detail page is displayed when the user selects the case ", function () {
        it("Should check - Ensure that case detail page is displayed when the user selects the case ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseCloudSearchDataLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activitiesTabLink).to.have.attribute('class').which.contain('selected-tab');
            browser.pause(2000);
        });
    });
}
exports.check_Select_Display_Memo_In_CloudSearch = check_Select_Display_Memo_In_CloudSearch;

var create_New_Case = function (assignTo,ccUser) {
    describe("Create a new Case",function () {
        it("should check - Create a new Case",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(5000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.createCaseLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseSaveBtn).to.be.present;
            browser.expect.element(elements.activityPageElements.caseSaveAndBackBtn).to.be.present;
            browser.expect.element(elements.activityPageElements.caseCancelBtn).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.caseOrganizationInput,variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.createdCase = variables.newCaseName;
           
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInput,variables.newCaseName);
            browser.pause(2000);
            if(assignTo){
                commands.checkAndPerform('clearValue', browser, elements.activityPageElements.caseAssignedToInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.caseAssignedToInput, assignTo);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            if(ccUser)
            {
                commands.checkAndPerform('clearValue', browser,elements.activityPageElements.caseCCUserInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser,elements.activityPageElements.caseCCUserInput, ccUser);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);

            }
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//option[contains(.,'Cases Created By Me')]");
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.activityPageElements.activitiesTable).to.be.present;

            browser.elements("xpath", elements.activityPageElements.activitiesTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdCase)).to.be.true;
                        variables.dependencyFulfilled.push('createdCase');
                        console.log('=====> Case created successfully : '+variables.createdCase);
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_New_Case = create_New_Case;

var create_New_Memo = function (ccUser1,ccUser2) {
    describe("Create a new Memo",function () {
        it("should check - Create a new Memo",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoCreateLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoSaveButton).to.be.present;
            browser.expect.element(elements.activityPageElements.memoSaveAndBackBtn).to.be.present;
            browser.expect.element(elements.activityPageElements.memoCancelBtn).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.memoOrganizationInput,variables.createdORG);
            browser.pause(5000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.createdMemo = variables.newMemoName;
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.newMemoName);
            browser.pause(2000);
            if(ccUser1){
                commands.checkAndPerform('clearValue', browser, elements.activityPageElements.memoCCUsersInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.memoCCUsersInput, ccUser1);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            if(ccUser2){
                commands.checkAndPerform('clearValue', browser, elements.activityPageElements.memoCCUsersInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.memoCCUsersInput, ccUser2);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSaveButton);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.createdORG + '")]');
            browser.pause(2000);


            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.organizationPageElements.activitiesListTable).to.be.present;
            browser.elements("xpath", elements.organizationPageElements.activitiesListTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdMemo)).to.be.true;
                        variables.dependencyFulfilled.push('createdMemo');
                        console.log('=====> Memo created successfully : '+variables.createdMemo);
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_New_Memo = create_New_Memo;

var create_New_Task = function (assignTo,ccUser) {
    describe("Create a new Task",function () {
        it("should check - Create a new Task",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.createTaskLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskSaveButton).to.be.present;
            browser.expect.element(elements.activityPageElements.taskSaveAndBackBtn).to.be.present;
            browser.expect.element(elements.activityPageElements.taskCancelBtn).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskOrgInput,variables.createdORG);
            browser.pause(5000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.createdTask = variables.newTaskName;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField,variables.newTaskName);
            browser.pause(2000);
            if(assignTo){
                commands.checkAndPerform('clearValue', browser, elements.activityPageElements.taskAssignedToInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.taskAssignedToInput, assignTo);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            if(ccUser)
            {
                commands.checkAndPerform('clearValue', browser, elements.activityPageElements.taskCCUserInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.activityPageElements.taskCCUserInput, ccUser);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskSaveButton);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list

            browser.expect.element(elements.activityPageElements.activitiesTable).to.be.present;
            commands.checkAndPerform('click', browser, "//option[contains(.,'Tasks Created By Me')]");
            browser.pause(5000);
            browser.elements("xpath", elements.activityPageElements.activitiesTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdTask)).to.be.true;
                        variables.dependencyFulfilled.push('createdTask');
                        console.log('=====> Task created successfully : '+variables.createdTask);
                    });
                });
            });
            //assert and push value to dependency fulfilled list


        });
    });
};
exports.create_New_Task = create_New_Task;

var navigate_To_New_Case = function () {
    describe("Navigating to newly created case activity",function () {
        it("should check - must navigate to newly created case activity",function (browser) {
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            browser.expect.element(elements.organizationPageElements.pageTitle).text.to.equal('Organizations');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.createdORG + '")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.activitiesSubTabBtn);
            browser.pause(2000);
            browser.moveToElement("//a[@title='"+variables.createdCase+"']",3000,3000,function () {
                commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdCase+"']");
            });
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Case = navigate_To_New_Case;

var navigate_To_New_Case_User1 = function () {
    describe("Navigating to newly created case activity",function () {
        it("should check - must navigate to newly created case activity",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdCase+'")]');
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Case_User1 = navigate_To_New_Case_User1;

var navigate_To_New_Task = function () {
    describe("Navigating to newly created task activity",function () {
        it("should check - must navigate to newly created task activity",function (browser) {
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            browser.pause(5000);
            browser.expect.element(elements.organizationPageElements.pageTitle).text.to.equal('Organizations');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.createdORG + '")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.activitiesSubTabBtn);
            browser.pause(2000);
            browser.moveToElement("//a[@title='"+variables.createdTask+"']",3000,3000,function () {
                commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdTask+"']");
            });
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Task = navigate_To_New_Task;

var navigate_To_New_Task_User1 = function () {
    describe("Navigating to newly created task activity",function () {
        it("should check - must navigate to newly created task activity",function (browser) {
            variables.createdTask = 'testtask_201711110022';
            variables.createdORG = 'test_Org_2017Jul11110022';
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.activitiesSubTabBtn);
            browser.pause(2000);
            browser.moveToElement("//a[@title='"+variables.createdTask+"']",3000,3000,function () {
                commands.checkAndPerform('click', browser, "//a[@title='"+variables.createdTask+"']");
            });
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Task_User1 = navigate_To_New_Task_User1;

var navigate_To_New_Memo = function () {
    describe("Navigating to newly created memo activity",function () {
        it("should check - must navigate to newly created memo activity",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdMemo+'")]');
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Memo = navigate_To_New_Memo;

var navigate_To_Attachments_Subtab = function () {
    describe("Navigate to attachments sub tab", function () {
        it("should navigate to case detail page attachments subtab", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.activityPageElements.caseAttachmentLink,3000,3000,function () {
                commands.checkAndPerform('click',browser,elements.activityPageElements.caseAttachmentLink);
            });

            browser.pause(2000);
        });
    });
};
exports.navigate_To_Attachments_Subtab = navigate_To_Attachments_Subtab;

var navigate_To_Task_Attachments_Subtab = function () {
    describe("Navigate to task attachments sub tab", function () {
        it("should navigate to task detail page attachments subtab", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.activityPageElements.taskAttachmentsSubTabLink,2000,2000,function () {
                browser.pause(2000);
                commands.checkAndPerform('click',browser,elements.activityPageElements.taskAttachmentsSubTabLink);
            });
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Task_Attachments_Subtab = navigate_To_Task_Attachments_Subtab;

var navigate_To_Memo_Attachments_Subtab = function () {
    describe("Navigate to memo attachments sub tab", function () {
        it("should navigate to memo detail page attachments subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoAttachmentsSubTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Memo_Attachments_Subtab = navigate_To_Memo_Attachments_Subtab;

var navigate_To_Task_Attachments_Subtab = function () {
    describe("Navigate to task attachments sub tab", function () {
        it("should navigate to task detail page attachments subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskAttachmentsSubTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Task_Attachments_Subtab = navigate_To_Task_Attachments_Subtab;

var navigate_To_Messages_Subtab = function () {
    describe("Navigate to messages sub tab", function () {
        it("should navigate to case detail page messages subtab", function (browser) {
            browser.pause(2000);
            browser.getLocationInView(elements.activityPageElements.caseMessagesSubTabLink)
                .assert.visible(elements.activityPageElements.caseMessagesSubTabLink)
                .click(elements.activityPageElements.caseMessagesSubTabLink);
            // commands.checkAndPerform('click',browser,elements.activityPageElements.caseMessagesSubTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Messages_Subtab = navigate_To_Messages_Subtab;

var navigate_To_Task_Messages_Subtab = function () {
    describe("Navigate to task messages sub tab", function () {
        it("should navigate to task detail page messages subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskMessagesSubTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Task_Messages_Subtab = navigate_To_Task_Messages_Subtab;

var navigate_To_Memo_Messages_Subtab = function () {
    describe("Navigate to memo messages sub tab", function () {
        it("should navigate to memo detail page messages subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoMessagesSubTabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Memo_Messages_Subtab = navigate_To_Memo_Messages_Subtab;

var navigate_To_Activity_Subtab = function () {
    describe("Navigate to activity sub tab", function () {
        it("should navigate to case detail page activity subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseActivityTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Activity_Subtab = navigate_To_Activity_Subtab;

var check_requiredFields = function () {
    describe("Organization and Date are required fields and noted with an '*'", function () {
        it("should check -  Organization and Date are required fields", function (browser) {
            browser.elements("xpath",elements.activityPageElements.caseOrganizationLabel, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('required')).to.be.true;
                    });
                });
            });
            browser.elements("xpath",elements.activityPageElements.caseDateLabel, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('required')).to.be.true;
                    });
                });
            });
        });
    });
};
exports.check_requiredFields = check_requiredFields;

var check_DateField = function () {
    describe("Date field pre-populates with current date and time", function () {
        it("should check - Date field pre-populates with current date and time", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseDateInput).to.have.attribute('value').equals(variables.formattedDate);
            browser.pause(1000);
        });
    });
};
exports.check_DateField = check_DateField;

var check_caseNumber_AutoCreated = function () {
    describe("Case Number is automatically created", function () {
        it("should check - Case Number is automatically created", function (browser) {
            browser.expect.element(elements.activityPageElements.activityPageHeader).text.to.not.equal("");
        });
    });
};
exports.check_caseNumber_AutoCreated = check_caseNumber_AutoCreated;

var add_CaseData = function () {
    describe("Fill all case data", function () {
        it("should check - navigating to another screen shows pops up case save options", function (browser) {
            browser.pause(2000);

            commands.checkAndPerform('click',browser,elements.activityPageElements.caseCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSubcategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(1000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseAssignedToInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseAssignedToInput,variables.username);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.casePriorityDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(1000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseCCUserInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseCCUserInput,variables.username1);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(1000);

            browser.expect.element(elements.activityPageElements.caseSavePopup).to.be.present;

            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSavePopUpYesOption);
            browser.pause(1000);
        });
    });
};
exports.add_CaseData = add_CaseData;

var edit_CaseData = function () {
    describe("Edit fields in case data", function () {
        it("should edit case data and assert", function (browser) {
            //click on edit button
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.activityEditBtn);
            browser.pause(1000);

            //click on resolve case button to check status changed to completed
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseResolveBtn);
            browser.pause(2000);

            // Add Resolved by (Defaults to Current User)
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseResolveByInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseResolveByInput,variables.username1);
            browser.pause(1000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

            // Resolved on date automatically populates with the current date
            browser.expect.element(elements.activityPageElements.caseResolveDateInput).to.have.value.that.equals(variables.formattedDate);
            browser.pause(1000);

            // Select Corrective Action
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseResolveCorrectiveActionDropdown);
            browser.pause(1000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Add Resolution Description
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseResolveResolutionInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseResolveResolutionInput,variables.resolutionDesc);
            browser.pause(1000);

            // add contact in related items
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseResolveRelatedItemsInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseResolveRelatedItemsInput,variables.createdContact);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(5000);

            // Related Item - Contact - Name, Phone and Email are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.caseResolveRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Contact'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdContact);
                            });
                        }
                    });

                });
            });

            // add patient in related items
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseResolveRelatedItemsInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseResolveRelatedItemsInput,variables.createdPatient);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(5000);

            // Related Item - Contact - Name, Phone and Email are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.caseResolveRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Contact'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdContact);
                            });
                        }
                    });
                });
            });

            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.edit_CaseData = edit_CaseData;

var add_Memo_To_Case = function () {
    describe("Add memo to case", function () {
        it("should add memo to case and check breadcrumb trail", function (browser) {
            browser.pause(4000) ;
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseCreateMemoLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoOrganizationInput,variables.newCaseMemoName);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSaveButton);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoBreadcrumbDiv).to.be.present;
            browser.elements("xpath", elements.activityPageElements.memoRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 1;
                els.forEach(function (el) {
                    count = count + 1;
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(count === 2){ //when <td> is the name of the subject
                            expect(text.value.split(" ")[0]).to.equal(variables.createdCase);
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.back();
        });
    });
};
exports.add_Memo_To_Case = add_Memo_To_Case;

var check_Activity_Tab_Highlighted = function () {
    describe("Ensure that the Activties tab is high lighted", function () {
        it("must ensure that the Activties tab is high lighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activitiesTabLink).to.have.attribute('class').which.contain('selected-tab');
        });
    });
};
exports.check_Activity_Tab_Highlighted = check_Activity_Tab_Highlighted;

var check_Icons_Present = function () {
    describe("Case, Task and Memo Icons present or not", function () {
        it("should check case, task and memo icons present or not", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.createCaseLink).to.be.present;
            browser.expect.element(elements.activityPageElements.createTaskLink).to.be.present;
            browser.expect.element(elements.activityPageElements.memoCreateLink).to.be.present;
        });
    });
};
exports.check_Icons_Present = check_Icons_Present;

var check_Edit_Delete_Link_Present = function () {
    describe("Edit and Delete link", function () {
        it("should check - edit and delete link is present or not", function (browser) {
            browser.pause(2000);
            browser.elements("xpath",elements.activityPageElements.activitiesTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.true;
                        }
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Edit_Delete_Link_Present = check_Edit_Delete_Link_Present;

var check_Pagination = function () {
    describe("Pagination functionality check", function () {
        it("should check pagination functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.pagingNumberOfRecordsDropdown+"/option[4]"); //100 records per page
            browser.getText(elements.activityPageElements.pagingTotalRecords,function (result) {
                if(result.value > 100){
                    browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('1 - 100');
                }else{
                    browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('1 - '+result.value);
                }

            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.pagingNumberOfRecordsDropdown+"/option[1]");
            browser.getText(elements.activityPageElements.pagingTotalRecords,function (result) {
                if(result.value < 10){
                    browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('1 - '+result.value);
                }
                if(result.value > 10){
                    browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('1 - 10');
                    commands.checkAndPerform('click',browser,elements.activityPageElements.pagingNextButton);
                    browser.pause(3000);
                    var totalNumberOfPages = Math.ceil(parseInt(result.value) / 10);
                    browser.getValue(elements.activityPageElements.pagingCurrentPageInput,function (result2) {
                        if(parseInt(result2.value) === totalNumberOfPages){
                            browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('11 - '+result.value);
                        }
                        else{
                            browser.expect.element(elements.activityPageElements.pagingRecordNumbers).text.to.equal('11 - '+(10 * parseInt(result2.value)));
                        }
                    });
                }
            });
        });
    });
};
exports.check_Pagination = check_Pagination;

var check_Icons_Not_Present = function () {
    describe("Case, Task and Memo icons", function () {
        it("should check case, task and memo icons are not present without permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseCreateLink).to.not.be.present;
            browser.expect.element(elements.activityPageElements.taskCreateLink).to.not.be.present;
            browser.expect.element(elements.activityPageElements.memoCreateLink).to.not.be.present;
        });
    });
};
exports.check_Icons_Not_Present = check_Icons_Not_Present;

var check_Edit_Delete_Link_Not_Present = function () {
    describe("Edit and delete links", function () {
        it("should check edit and delete links are not present without permissions", function (browser) {
            browser.pause(2000);
            browser.elements("xpath",elements.activityPageElements.activitiesTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('edit')).to.be.false;
                    });
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        });
    });
};
exports.check_Edit_Delete_Link_Not_Present = check_Edit_Delete_Link_Not_Present;

var add_Task_To_Case = function () {
    describe("Create a task to case", function () {
        it("should create a task related item in case and check breadcrumb", function (browser) {
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseCreateTaskLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField,variables.newCaseTaskName);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskSaveButton);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskBreadcrumbDiv).to.be.present;
            browser.elements("xpath", elements.activityPageElements.taskRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 1;
                els.forEach(function (el) {
                    count = count + 1;
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(count === 2){ //when <td> is the name of the subject
                            expect(text.value.split(" ")[0]).to.equal(variables.createdCase);
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.back();
        });
    });
};
exports.add_Task_To_Case = add_Task_To_Case;

var check_Required_Field = function () {
    describe("Required fields check", function () {
        it("should check required fields error messages", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.activityEditBtn);
            browser.pause(1000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.caseOrganizationInput);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.caseRequiredFieldErrorPara).to.be.present;
            browser.pause(1000);
            browser.keys(browser.Keys.ESCAPE);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseOrganizationInput,variables.createdORG);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveBtn);
            browser.expect.element(elements.activityPageElements.caseRequiredFieldErrorPara).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Required_Field = check_Required_Field;

var check_Attachments = function () {
    describe("Check attachments", function () {
        it("should check either we can upload and attachment/not", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseAttachmentLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseNewAttachmentLink).to.be.present;
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseNewAttachmentLink);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.caseAttachmentPopUp).to.be.visible;
            browser.expect.element(elements.activityPageElements.caseAttachmentPopupFileInput).to.be.present;
            browser.expect.element(elements.activityPageElements.caseAttachmentUploadBtn).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseAttachmentPopupCloseBtn);
            browser.pause(2000);
            browser.elements("xpath",elements.organizationPageElements.attachmentsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });

        });
    });
};
exports.check_Attachments = check_Attachments;

var check_Memo_On_Case = function () {
    describe("Check memo on case", function () {
        it("should check memo on case", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.activityPageElements.caseCreateMemoLink,3000,3000,function () {
                browser.expect.element(elements.activityPageElements.caseCreateMemoLink).to.be.present;
                browser.pause(2000);

                commands.checkAndPerform('click',browser,elements.activityPageElements.caseCreateMemoLink);
                browser.pause(2000);
            });

            browser.expect.element(elements.activityPageElements.memoPageHeader).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.memoSubject);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSaveAndBackBtn);
            browser.pause(1000);
            browser.elements("xpath", elements.activityPageElements.caseActivitiesTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.split(' ')[0]).to.equal(variables.memoSubject);
                    });
                });
            });
        });
    });
};
exports.check_Memo_On_Case = check_Memo_On_Case;

var check_Memo_Icon_Not_Visible = function () {
    describe("Memo icon not visible without permissions", function () {
        it("should check memo icon not visible without memo permissions", function (browser) {
            browser.expect.element(elements.activityPageElements.caseCreateMemoLink).to.not.be.present;
        });
    });
};
exports.check_Memo_Icon_Not_Visible = check_Memo_Icon_Not_Visible;

var check_Task_On_Case = function () {
    describe("Check task on case", function () {
        it("should check task on case", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.activityPageElements.caseCreateMemoLink,3000,3000,function () {
                browser.expect.element(elements.activityPageElements.caseCreateTaskLink).to.be.present;
                browser.pause(1000);
                commands.checkAndPerform('click',browser,elements.activityPageElements.caseCreateTaskLink);
                browser.pause(1000);
            });

            browser.expect.element(elements.activityPageElements.taskPageHeader).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField,variables.taskSubject);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(1000);
            browser.elements("xpath", elements.activityPageElements.caseActivitiesTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.split(' ')[0]).to.equal(variables.taskSubject);
                    });
                });
            });
        });
    });
};
exports.check_Task_On_Case = check_Task_On_Case;

var check_Task_Icon_Not_Visible = function () {
    describe("Task icon not visible without permissions", function () {
        it("should check task icon not visible without task permissions", function (browser) {
            browser.expect.element(elements.activityPageElements.caseCreateTaskLink).to.not.be.present;
        });
    });
};
exports.check_Task_Icon_Not_Visible = check_Task_Icon_Not_Visible;

var check_Case_Attachments_Subtab = function () {
    describe("Case detail page attachments subtab", function () {
        it("should check - case detail page attachments subtab", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.activityPageElements.caseNewAttachmentLink,3000,3000,function () {
                browser.pause(2000);
                browser.expect.element(elements.activityPageElements.caseNewAttachmentLink).to.be.present;
                browser.pause(2000);
                commands.checkAndPerform('click',browser,elements.activityPageElements.caseNewAttachmentLink);
                browser.pause(2000);
            });
            browser.expect.element(elements.activityPageElements.caseAttachmentPopUp).to.be.present;
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseAttachmentUploadBtn);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseAttachmentNameErrorLabel).to.be.present;
            browser.expect.element(elements.activityPageElements.caseAttachmentFileErrorLabel).to.be.present;

            browser.expect.element(elements.activityPageElements.caseAttachmentDescriptionInput).to.be.present;
            commands.checkAndSetValue(browser,elements.activityPageElements.caseAttachmentDescriptionInput,variables.newAttachmentDescription);

            browser.expect.element(elements.activityPageElements.caseAttachmentPHICheckbox).to.be.present;

            //upload a test file
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadFileInput, variables.testFileUploadPath);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.newUploadName+"']").to.be.present;

            //check download attachment
            browser.elements("xpath",elements.activityPageElements.caseAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('downloadAttachment')).to.be.true;
                        }
                    });
                });
            });
            browser.getAttribute("//a[contains(@title,'"+variables.testFileUploadName+"')]","href",function (re) {
                console.log('>>>>>href : '+re.value);
                expect(re.value).to.include('download');
            });

            //check attachment delete functionality
            browser.elements("xpath",elements.activityPageElements.caseAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });

            //delete the attachment
            browser.elements("xpath", elements.activityPageElements.caseAttachmentsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        console.log('>>>>> table data : '+text.value);
                        if(text.value.includes(variables.newUploadName)){
                            commands.checkAndPerform('click', browser, elements.activityPageElements.caseAttachmentsTable+'/tbody/tr[1]/td[5]/i');
                            browser.pause(3000);
                            browser.expect.element("//a[contains(@title,'"+variables.newUploadName+"')]").to.not.be.present;
                        }
                    });
                });
            });

        });
    });
};
exports.check_Case_Attachments_Subtab = check_Case_Attachments_Subtab;

var case_Icon_isPresent_withPermission = function () {
    describe("Case icon is present with case permission", function () {
        it("Should check - Case icon is present with case permission", function (browser) {
            browser.pause(3000);
            browser.expect.element(elements.activityPageElements.caseOrganizationLabel).to.have.attribute('class').to.be.contain('required');
        });
    });
};
exports.case_Icon_isPresent_withPermission = case_Icon_isPresent_withPermission;

var category_Subcategory_DropDown = function () {
    describe("Category and subcategory is a drop down ", function () {
        it("Category and subcategory is a drop down ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//select[contains(@id, "' + variables.activityDropDownlist + '"  )]');
            browser.pause(1000);
        });
    });
};
exports.category_Subcategory_DropDown = category_Subcategory_DropDown;

var check_DescriptionTable_isDisplay = function () {
    describe("Create a activity > Select a subcategory > Ensure that the description  text is displayed", function () {
        it("Should check - Create a activity > Select a subcategory > Ensure that the description  text is displayed", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//option[contains(.,"'+variables.createdSubActivityCategoryName+'")]');
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.caseDescriptionInput).to.have.value.that.equals(variables.createdSubActivityCategoryDesc);
            browser.pause(2000);
        });
    });
};
exports.check_DescriptionTable_isDisplay = check_DescriptionTable_isDisplay;

var search_Behaves_Properly = function () {
    describe("Ensure that the search behaves properly", function () {
        it("Should check - Ensure that the search behaves properly", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.activityPageElements.caseOrganizationInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.caseOrganizationInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
};
exports.search_Behaves_Properly = search_Behaves_Properly;

var activities_Table_Display = function () {
    describe("Ensure that the list of activities are displayed properly ", function () {
        it("Should check - Ensure that the list of activities are displayed properly ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseActivitiesTable).to.be.visible;
            browser.pause(2000);
        });
    });
};
exports.activities_Table_Display = activities_Table_Display;

var check_editIcon_DisplayOn_WritePermission = function () {
    describe("edit icon is displayed if the user has write permission  ", function () {
        it("Should check -edit icon is displayed if the user has write permission  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseActivitiesTable+'/thead/tr/th[1]').to.have.attribute('class').to.equals('edit');
            browser.pause(1000);
        });
    });
};
exports.check_editIcon_DisplayOn_WritePermission  = check_editIcon_DisplayOn_WritePermission ;

var check_Activity_Delete_Link = function () {
    describe("delete icon is displayed if the user has del permission ",function () {
        it("delete icon is displayed if the user has del permission ",function (browser) {
            browser.elements("xpath", elements.activityPageElements.caseActivitiesTable +'/tbody/tr/td', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT, 'class', function (text) {
                        if (text.value.includes('delete')) {
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(3000);
        });
    });
};
exports.check_Activity_Delete_Link = check_Activity_Delete_Link;

var check_Memo_Icon_Present = function () {
    describe("Memo create icon on activities page", function () {
        it("should check - memo create icon is present on activities page", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoCreateLink).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Icon_Present = check_Memo_Icon_Present;

var check_Task_Icon_Present = function () {
    describe("Task create icon on activities page", function () {
        it("should check - Task create icon is present on activities page", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.createTaskLink).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Task_Icon_Present = check_Task_Icon_Present;

var check_Memo_Create_Modal = function () {
    describe("Memo create modal on activities page", function () {
        it("should check - create memo modal is opened or not on create memo link", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoCreateLink);
            browser.expect.element(elements.activityPageElements.memoPageHeader).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Create_Modal = check_Memo_Create_Modal;

var check_Task_Create_Modal = function () {
    describe("Task create modal on activities page", function () {
        it("should check - create Task modal is opened or not on create memo link", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.createTaskLink);
            browser.expect.element(elements.activityPageElements.taskPageHeader).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Task_Create_Modal = check_Task_Create_Modal;

var check_Memo_Required_Fields = function () {
    describe("Memo required fields", function () {
        it("should check - required fields while creating memo", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoDateInput).to.have.attribute('required').equals('true');
            browser.pause(1000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoOrganizationInput);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSaveButton);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoOrgRequiredErrorLabel).to.be.visible;
            browser.keys([browser.Keys.ESCAPE]);
        });
    });
};
exports.check_Memo_Required_Fields = check_Memo_Required_Fields;

var check_Task_Required_Fields = function () {
    describe("Task required fields", function () {
        it("should check - required fields while creating task", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskDateInput).to.have.attribute('required').equals('true');
            browser.pause(1000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.taskOrgInput);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskSaveButton);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskOrganizationErrorLabel).to.be.visible;
            browser.keys([browser.Keys.ESCAPE]);
        });
    });
};
exports.check_Task_Required_Fields = check_Task_Required_Fields;

var check_Memo_Org_Search = function () {
    describe("Organization search while creating memo", function () {
        it("should check - organization search must behave properly while creating memo", function (browser) {
            browser.pause(2000);
            //check Category and subcategory is a drop down
            browser.expect.element(elements.activityPageElements.memoCategoryDropdown).to.be.a('select');
            browser.expect.element(elements.activityPageElements.memoSubCategoryDropdown).to.be.a('select');
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.activityPageElements.memoOrganizationInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoOrganizationInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.activityPageElements.memoSaveButton);
            browser.pause(2000);
        });
    });
};
exports.check_Memo_Org_Search = check_Memo_Org_Search;

var check_Task_Org_Search = function () {
    describe("Organization search while creating task", function () {
        it("should check - organization search must behave properly while creating task", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser, elements.activityPageElements.taskOrgInput);
            browser.pause(1000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskOrgInput, variables.createdORG);
            browser.pause(1000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskCategoryDropdown).to.be.a('select');
            browser.expect.element(elements.activityPageElements.taskSubCategoryDropdown).to.be.a('select');
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskSaveAndBackBtn);
        });
    });
};
exports.check_Task_Org_Search = check_Task_Org_Search;

var check_Memo_Cat_Subcat_Is_Dropdown = function () {
    describe("Category and subcategory dropdown while creating memo", function () {
        it("should check - Category and subcategory must be dropdowns", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoCategoryDropdown).to.be.a('select');
            browser.expect.element(elements.activityPageElements.memoSubCategoryDropdown).to.be.a('select');
            browser.pause(2000);
        });
    });
};
exports.check_Memo_Cat_Subcat_Is_Dropdown = check_Memo_Cat_Subcat_Is_Dropdown;

var check_Task_Cat_Subcat_Is_Dropdown = function () {
    describe("Category and subcategory dropdown while creating task", function () {
        it("should check - Category and subcategory must be dropdowns", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskCategoryDropdown).to.be.a('select');
            browser.expect.element(elements.activityPageElements.taskSubCategoryDropdown).to.be.a('select');
            browser.pause(1000);
        });
    });
};
exports.check_Task_Cat_Subcat_Is_Dropdown = check_Task_Cat_Subcat_Is_Dropdown;

var check_Memo_Date_Field = function () {
    describe("Date field in memo detail page", function () {
        it("should check - Date field must be prepopulated with current date and time", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoDateInput).to.have.value.that.equals(variables.formattedDate);
        });
    });
};
exports.check_Memo_Date_Field = check_Memo_Date_Field;

var check_Task_Date_Field = function () {
    describe("Date field in task detail page", function () {
        it("should check - Date field must be prepopulated with current date and time", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.activityEditBtn);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskDateInput).to.have.value.that.equals(variables.formattedDate);
        });
    });
};
exports.check_Task_Date_Field = check_Task_Date_Field;

var check_Memo_Number = function () {
    describe("Memo number in memo detail page", function () {
        it("should check - memo number must be auto created on saving a memo", function (browser) {
            browser.expect.element(elements.activityPageElements.memoPageHeader).text.to.not.equal('');
        });
    });
};
exports.check_Memo_Number = check_Memo_Number;

var check_Task_Number = function () {
    describe("Task number in task detail page", function () {
        it("should check - task number must be auto created on saving a task", function (browser) {
            browser.expect.element(elements.activityPageElements.taskPageHeader).text.to.not.equal('');
        });
    });
};
exports.check_Task_Number = check_Task_Number;

var check_Add_Memo_Data = function () {
    describe("Add memo data and navigate to another page without saving", function () {
        it("should check - Must save memo if navigated to another page without saving data", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoSubjectInputField);
            variables.createdMemo = variables.newMemoName;
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.newMemoName);

            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoDescInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoDescInput,variables.memoDesc);

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSubCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoCCUsersInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoCCUsersInput,variables.username);

            //add contact in related item
            browser.pause(5000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoRelatedItemInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoRelatedItemInput,variables.createdContact);
            browser.pause(1500);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            //add patient in related item
            browser.pause(5000);
            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoRelatedItemInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoRelatedItemInput,variables.createdPatient);
            browser.pause(1500);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Related Item - Contact - Name, Phone and Email are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.memoRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Contact'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdContact);
                            });
                        }
                    });

                });
            });

            // Related Item - Patient - Name and MRN are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.memoRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Patient'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdPatient);
                            });
                        }
                    });

                });
            });
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoOrganizationInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationTabLink);
            browser.expect.element(elements.activityPageElements.memoSavePopup).to.be.present;
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSavePopUpYesOption);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.createdMemo+'")]').to.be.present;
        });
    });
};
exports.check_Add_Memo_Data = check_Add_Memo_Data;

var check_Modify_Memo_Data = function () {
    describe("Modify/Delete memo data and cross check", function () {
        it("should check - modifying / deleting memo data must persist after saving it", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.createdMemo+'")]');
            commands.checkAndPerform('click',browser,elements.activityPageElements.activityEditBtn);
            browser.pause(1000);

            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoSubjectInputField);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.memoSubjectUpdated);

            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoDescInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoDescInput,variables.memoDescUpdated);

            commands.checkAndPerform('clearValue',browser,elements.activityPageElements.memoOrganizationInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoOrganizationInput,variables.createdORG);

            commands.checkAndPerform('click',browser,elements.activityPageElements.memoCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSubCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            //delete cc user
            browser.pause(2000);
            browser.elements("xpath",elements.activityPageElements.memoCCUsersDiv, function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                });
                commands.checkAndPerform('click',browser,'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div['+count+']/span');
            });

            //delete contact related item
            browser.pause(2000);
            browser.elements("xpath",elements.activityPageElements.memoRelatedItemsTable, function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function () {
                    count = count + 1;
                });
                if(count > 1){
                    for(var i=1;i<=count;i++){
                        commands.checkAndPerform('click',browser,'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[2]/table/tbody/tr['+i+']/td[5]/span');
                        browser.expect.element('/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[2]/table/tbody/tr['+i+']/td[5]/span').to.not.be.present;
                    }
                }
                else{
                    commands.checkAndPerform('click',browser,'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[2]/table/tbody/tr/td[5]/span');
                    browser.expect.element('/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[2]/table/tbody/tr/td[5]/span').to.not.be.present;
                }
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveButton);

        });
    });
};
exports.check_Modify_Memo_Data = check_Modify_Memo_Data;

var check_Memo_From_Contact_Page = function () {
    describe("Create memo from contact page", function () {
        it("should check - memo created from contact page appears as related item in memo page", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.memoCreateLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.memoOrganizationInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoOrganizationInput, variables.createdORG);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveButton);
            browser.pause(2000);
            browser.elements("xpath",elements.activityPageElements.memoRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value).to.contain(variables.contactFName);
                    });
                });
            });
        });
    });
};
exports.check_Memo_From_Contact_Page = check_Memo_From_Contact_Page;

var check_Task_Message_List = function () {
    describe("check task message list", function () {
        it("should check task message list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskMessagesSubTabLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskMessagesTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Task_Message_List = check_Task_Message_List;

var check_Case_Message_List = function () {
    describe("check case message list", function () {
        it("should check case message list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseMessagesSubTabLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseMessagesTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Case_Message_List = check_Case_Message_List;

var check_Memo_Message_List = function () {
    describe("check memo message list", function () {
        it("should check memo message list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoMessagesSubTabLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoMessagesTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Memo_Message_List = check_Memo_Message_List;

var check_Delete_Icon_Present = function (activityType) {
    describe("Delete icon on messages subtab", function () {
        it("should check delete icon in messages table is present or not", function (browser) {
            browser.pause(2000);
            var tablename = '';

            if(activityType === 'case')
                tablename = elements.activityPageElements.caseMessagesTable;
            if(activityType === 'memo')
                tablename = elements.activityPageElements.memoMessagesTable;
            if(activityType === 'task')
                tablename = elements.activityPageElements.taskMessagesTable;

            browser.elements("xpath", tablename+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        if(text.value.includes('delete')){
                            expect(text.value).to.equal('delete');
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Delete_Icon_Present = check_Delete_Icon_Present;

var check_Memo_Attachment_UploadLinkPresent = function () {
    describe("Upload attachment link is present with attachment write permission", function () {
        it("should check - Upload attachment link is present with attachment write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoAttachmentsSubTabLink);
            browser.expect.element(elements.activityPageElements.memoAttachmentsUploadLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Memo_Attachment_UploadLinkPresent = check_Memo_Attachment_UploadLinkPresent;

var check_Task_Attachment_UploadLinkPresent = function () {
    describe("Upload attachment link is present with attachment write permission", function () {
        it("should check - Upload attachment link is present with attachment write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskAttachmentsSubTabLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsUploadLink).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Task_Attachment_UploadLinkPresent = check_Task_Attachment_UploadLinkPresent;

var check_Memo_Attachment_PopupIsDisplayed = function () {
    describe("Ensure that attachment pop-up is displayed", function () {
        it("should check - Ensure that attachment pop-up is displayed", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoAttachmentsUploadLink);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoAttachmentUploadPopup).to.be.visible;
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Attachment_PopupIsDisplayed = check_Memo_Attachment_PopupIsDisplayed;

var check_Task_Attachment_PopupIsDisplayed = function () {
    describe("Ensure that attachment pop-up is displayed", function () {
        it("should check - Ensure that attachment pop-up is displayed", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskAttachmentsUploadLink);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsPopup).to.be.visible;
            browser.pause(1000);
        });
    });
};
exports.check_Task_Attachment_PopupIsDisplayed = check_Task_Attachment_PopupIsDisplayed;

var check_Memo_Attachment_RequiredFields = function () {
    describe("Name and file are required fields", function () {
        it("should check - Name and file are required fields", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoAttachmentUploadBtn);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoAttachmentNameRequiredLabel).to.be.present;
            browser.expect.element(elements.activityPageElements.memoAttachmentFileRequiredLabel).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Attachment_RequiredFields = check_Memo_Attachment_RequiredFields;

var check_Task_Attachment_RequiredFields = function () {
    describe("Name and file are required fields", function () {
        it("should check - Name and file are required fields", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskAttachmentsUploadBtn);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsNameErrorLabel).to.be.present;
            browser.expect.element(elements.activityPageElements.taskAttachmentsFileErrorLabel).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Task_Attachment_RequiredFields = check_Task_Attachment_RequiredFields;

var check_Memo_Attachment_AddDescription = function () {
    describe("Add Description", function () {
        it("should check - Add description", function (browser) {
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoAttachmentDescriptionInput,variables.newAttachmentDescription);
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Attachment_AddDescription = check_Memo_Attachment_AddDescription;

var check_Task_Attachment_AddDescription = function () {
    describe("Add Description", function () {
        it("should check - Add description", function (browser) {
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskAttachmentsDescInput,variables.newAttachmentDescription);
            browser.pause(1000);
        });
    });
};
exports.check_Task_Attachment_AddDescription = check_Task_Attachment_AddDescription;

var check_Memo_Attachment_PhiCheckboxPresent = function () {
    describe("Phi is checkbox is present", function () {
        it("should check - Phi is checkbox is present", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoAttachmentPHICheckbox).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Attachment_PhiCheckboxPresent = check_Memo_Attachment_PhiCheckboxPresent;

var check_Task_Attachment_PhiCheckboxPresent = function () {
    describe("Phi is checkbox is present", function () {
        it("should check - Phi is checkbox is present", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsPHICheckbox).to.be.present;
            browser.pause(1000);
        });
    });
};
exports.check_Task_Attachment_PhiCheckboxPresent = check_Task_Attachment_PhiCheckboxPresent;

var check_Memo_Attachment_AddAttachment = function () {
    describe("Add a attachment with phi", function () {
        it("should check - Add a attachment with phi", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.memoAttachmentUploadBtn).to.be.present;
            //upload a test file
            commands.checkAndSetValue(browser,elements.activityPageElements.memoAttachmentNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoAttachmentFileInput, variables.testFileUploadPath);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoAttachmentUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.newUploadName+"']").to.be.present;
            browser.pause(1000);
            //check download attachment
            browser.elements("xpath",elements.activityPageElements.memoAttachmentTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('downloadAttachment')).to.be.true;
                        }
                    });
                });
            });
            browser.getAttribute("//a[contains(@title,'"+variables.testFileUploadName+"')]","href",function (re) {
                console.log('>>>>>href : '+re.value);
                expect(re.value).to.include('download');
            });
        });
    });
};
exports.check_Memo_Attachment_AddAttachment = check_Memo_Attachment_AddAttachment;

var check_Task_Attachment_AddAttachment = function () {
    describe("Add a attachment with phi", function () {
        it("should check - Add a attachment with phi", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsUploadBtn).to.be.present;
            browser.pause(1000);
            //upload a test file
            commands.checkAndSetValue(browser,elements.activityPageElements.taskAttachmentsNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskAttachmentsFilenameInput, variables.testFileUploadPath);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskAttachmentsUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.newUploadName+"']").to.be.present;
            browser.pause(1000);
            //check download attachment
            browser.elements("xpath",elements.activityPageElements.taskAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('downloadAttachment')).to.be.true;
                        }
                    });
                });
            });
            browser.getAttribute("//a[contains(@title,'"+variables.testFileUploadName+"')]","href",function (re) {
                console.log('>>>>>href : '+re.value);
                expect(re.value).to.include('download');
            });
        });
    });
};
exports.check_Task_Attachment_AddAttachment = check_Task_Attachment_AddAttachment;

var check_Memo_Attachment_DeleteIconPresent = function () {
    describe("delete icon is displayed if the user has del permission", function () {
        it("should check - delete icon is displayed if the user has del permission", function (browser) {
            browser.pause(1000);
            browser.elements("xpath", elements.activityPageElements.memoAttachmentTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(1000);

            //delete the attachment
            browser.elements("xpath", elements.activityPageElements.memoAttachmentTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        console.log('>>>>> table data : '+text.value);
                        if(text.value.includes(variables.newUploadName)){
                            commands.checkAndPerform('click', browser, elements.activityPageElements.memoAttachmentTable+'/tbody/tr[1]/td[5]/i');
                            browser.pause(3000);
                            browser.expect.element("//a[contains(@title,'"+variables.newUploadName+"')]").to.not.be.present;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Memo_Attachment_DeleteIconPresent = check_Memo_Attachment_DeleteIconPresent;

var check_Task_Attachment_DeleteIconPresent = function () {
    describe("delete icon is displayed if the user has del permission", function () {
        it("should check - delete icon is displayed if the user has del permission", function (browser) {
            browser.pause(1000);
            browser.elements("xpath", elements.activityPageElements.taskAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(1000);

            //delete the attachment
            browser.elements("xpath", elements.activityPageElements.taskAttachmentsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        console.log('>>>>> table data : '+text.value);
                        if(text.value.includes(variables.newUploadName)){
                            commands.checkAndPerform('click', browser, elements.activityPageElements.taskAttachmentsTable+'/tbody/tr[1]/td[5]/i');
                            browser.pause(3000);
                            browser.expect.element("//a[contains(@title,'"+variables.newUploadName+"')]").to.not.be.present;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Task_Attachment_DeleteIconPresent = check_Task_Attachment_DeleteIconPresent;

var check_Memo_Attachment_UploadLinkNotPresent = function () {
    describe("Upload attachment link is present without attachment without write permission", function () {
        it("should check - Upload attachment link is present without attachment without write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoAttachmentsSubTabLink);
            browser.expect.element(elements.activityPageElements.memoAttachmentsUploadLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Memo_Attachment_UploadLinkNotPresent = check_Memo_Attachment_UploadLinkNotPresent;

var check_Task_Attachment_UploadLinkNotPresent = function () {
    describe("Upload attachment link is present without attachment without write permission", function () {
        it("should check - Upload attachment link is present without attachment without write permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskAttachmentsSubTabLink);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsUploadLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Task_Attachment_UploadLinkNotPresent = check_Task_Attachment_UploadLinkNotPresent;

var check_Memo_Attachment_DeleteIconNotPresent = function () {
    describe("del icon is displayed without del permission (fail)", function () {
        it("should check - del icon is displayed without del permission (fail)", function (browser) {
            browser.pause(1000);
            browser.elements("xpath", elements.activityPageElements.memoAttachmentTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value).to.not.equal('delete');
                    });
                });
            });
            browser.pause(1000);
        });
    });
};
exports.check_Memo_Attachment_DeleteIconNotPresent = check_Memo_Attachment_DeleteIconNotPresent;

var check_Task_Attachment_DeleteIconNotPresent = function () {
    describe("del icon is displayed without del permission (fail)", function () {
        it("should check - del icon is displayed without del permission (fail)", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskAttachmentsTable).to.be.present;
            browser.elements("xpath", elements.activityPageElements.taskAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value).to.not.equal('delete');
                    });
                });
            });
            browser.pause(1000);
        });
    });
};
exports.check_Task_Attachment_DeleteIconNotPresent = check_Task_Attachment_DeleteIconNotPresent;

var check_Task_DueDateDefault = function () {
    describe("Due date input box in task detail page", function () {
        it("check default due date input in task detail page", function (browser) {
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskDueDateInput).to.have.value.that.equals(variables.nextDay);
            browser.pause(1000);
        });
    });
};
exports.check_Task_DueDateDefault = check_Task_DueDateDefault;

var add_Task_Data = function () {
    describe("adding task data", function () {
        it("should check - navigating to other page without saving task data prompts to save it", function (browser) {

            //click on edit button in activity detail page
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);

            // Select Status
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskStatusDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Add Subject
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskSubjectInputField);
            variables.createdTask = variables.taskSubjectUpdated;
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.taskSubjectUpdated);
            browser.pause(1000);

            // Add description
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskDescriptionInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskDescriptionInput, variables.taskDescUpdated);
            browser.pause(1000);

            // add Organization
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskOrgInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskOrgInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Select Category
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Select Sub-Category (Dependant on Category)
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSubCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Select Assigned to (Defaults to current user)
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskAssignedToInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskAssignedToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Select Priority (Low, Medium, High)
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskPriorityDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Add CC: User
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskCCUserInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskCCUserInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Navigating to a new screen presents message to Save Changes
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(1000);
            browser.expect.element(elements.activityPageElements.taskSavePopup).to.be.present;

            // Save' commites Changes to DB
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSavePopUpYesOption);
            browser.pause(2000);
        });
    });
};
exports.add_Task_Data = add_Task_Data;

var edit_Task_Data = function () {
    describe("editing task data", function () {
        it("should check - editing task data will persist task data after saving", function (browser) {
            //click on edit button
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityEditBtn);
            browser.pause(2000);

            // Edit Status
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskStatusDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Edit Subject
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskSubjectInputField);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.taskSubjectUpdated);
            browser.pause(1000);

            // Edit description
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskDescriptionInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskDescriptionInput, variables.taskDescUpdated);
            browser.pause(1000);

            // Edit Organization
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskOrgInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskOrgInput, variables.createdORG);
            browser.pause(1000);

            // Edit Category
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Edit Sub-Category (Dependant on Category)
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSubCategoryDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Edit Assigned to
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskAssignedToInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskAssignedToInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Edit Priority (Low, Medium, High)
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskPriorityDropdown+"/option[3]"); //option 3 is 'Medium'
            browser.pause(1000);

            // Edit CC: User
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskCCUserInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskCCUserInput, variables.username1);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Delete Status
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskStatusDropdown+"/option[1]");   //Option 1 is default 'Select status' for sure
            browser.pause(1000);

            // Delete Subject
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskSubjectInputField);
            browser.pause(1000);

            // Delete description
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskDescriptionInput);
            browser.pause(1000);

            // Delete Assigned to
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskAssignedToInput);
            browser.pause(1000);

            // Delete CC: User
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskCCUserInput);
            browser.pause(1000);

            // Add Related Item - Contact - Contact Icon Presents
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskRelatedItemsInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskRelatedItemsInput, variables.createdContact);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Add Related Item - Provider Provider Icon Presents

            // Add Related Item - Order - Order Icon Presents

            // Add Related Item - Patient - Patient Icon Presents
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.taskRelatedItemsInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskRelatedItemsInput, variables.createdPatient);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            // Add Related Item - Specimen - Specimen Icon Presents

            // Related Item - Contact - Name, Phone and Email are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.taskRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Contact'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdContact);
                            });
                        }
                    });

                });
            });

            // Related Item - Patient - Name and MRN are displayed after item is added
            browser.elements("xpath",elements.activityPageElements.taskRelatedItemsTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'data-entitytype',function (result) {
                        if(result.value === 'Patient'){
                            browser.elementIdText(el.ELEMENT, function (text) {
                                expect(text.value).to.contain(variables.createdPatient);
                            });
                        }
                    });

                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveButton);
            browser.pause(2000);

            // Related Item - Provider - Name, Phone and Email are displayed after item is added
            // Related Item - Order - Order ID, Provider and Order date are displayed after item is added
            // Related Item - Specimen - Specimen Id, Panel Name, and Specimen type are dispalyed after item is added
        });
    });
};
exports.edit_Task_Data = edit_Task_Data;

var verify_Recent_Organization = function () {
    describe("Recent organization items", function () {
        it("should check - Recent organization list in organization page", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            //check first row of organization list table
            browser.elements("xpath", elements.organizationPageElements.orgListTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdORG);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_Recent_Organization = verify_Recent_Organization;

var verify_Recent_Activity = function () {
    describe("Recent activity items", function () {
        it("should check - Recent activity list in activities page", function (browser) {
            browser.pause(4000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);

            //create a case with next day date
            commands.checkAndPerform('click',browser,elements.activityPageElements.createCaseLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseOrganizationInput,variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.createdCase = variables.newCaseName;
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInput,variables.newCaseName);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.activityPageElements.caseDateInput);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseDateInput, moment(new Date()).add(48, 'hours').format('MM/DD/YYYY'));
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveBtn);
            browser.pause(2000);

            //check recent items
            commands.checkAndPerform('click', browser,elements.activityPageElements.activitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.activityCustomListOutstandingOpt);
            browser.elements("xpath", elements.activityPageElements.activitiesTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdCase);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_Recent_Activity = verify_Recent_Activity;

var verify_Recent_Opportunity = function () {
    describe("Recent opportunity items", function () {
        it("should check - recent opportunity list table", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTabLink);
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.equal(variables.createdOpportunity);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_Recent_Opportunity = verify_Recent_Opportunity;

var verify_Recent_Contact = function () {
    describe("Recent contact/provider items", function () {
        it("should check - recent contacts/providers list table", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(4000);
            browser.elements("xpath",elements.contactsPageElements.contactsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdContact);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_Recent_Contact = verify_Recent_Contact;

var verify_Recent_Patient = function () {
    describe("Recent patients list", function () {
        it("should check - whether patient search returns in the results table", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.ordersAndPatientsTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.ordersandPatientsPageElements.patientSearchFNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.ordersandPatientsPageElements.patientSearchFNameInput, variables.createdPatientFName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.ordersandPatientsPageElements.patientSearchBtn);
            browser.pause(2000);
            browser.elements("xpath", elements.ordersandPatientsPageElements.patientsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdPatientFName);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.verify_Recent_Patient = verify_Recent_Patient;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END







//FUNCTIONS DECLARATION - STARTS

var activities_tab = function () {
    describe("ACTIVITIES TAB - BASIC",function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Activities_Tab();
        me.check_Activity_Tab_Highlighted();    // Ensure that the Activties tab is high lighted
        me.check_Icons_Present();               // Case icon is present with case write permission
                                                // Task icon is present with task write permission
                                                // Memo icon is present with memo write permission
        me.check_Edit_Delete_Link_Present();    // Edit' link presents with activties (case/task/memo) Write permission
                                                // Del' link presents with activties (case/task/memo) permission
        me.check_Pagination();                  // x of y' Presents and Pages correctly
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);

        navigateOperations.navigate_To_Activities_Tab();
        me.check_Icons_Not_Present();           // Case icon is present without case write permission (fail)
                                                // Task icon is present without task write permission (fail)
                                                // Memo icon is present without Memo write permission (fail)
        me.check_Edit_Delete_Link_Not_Present();// Edit' link presents without activties (case/task/memo) Write permission (Fail)
                                                // Del' link presents without activties (case/task/memo) permission (Fail)

        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });

};
exports.activities_tab = activities_tab;

var activities_customList = function () {
    describe("ACTIVITIES TAB - CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Activities_Tab();

        // commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.activityQuickFilterLink,
        //     elements.activityPageElements.activitiesTable,
        //     elements.activityPageElements.activityFilterAvailableColumns,
        //     elements.activityPageElements.activityFilterSelectedColumns,
        //     elements.activityPageElements.activityFilterAddBtn,
        //     elements.activityPageElements.activityFilterRemoveBtn,
        //     elements.activityPageElements.activityFilterApplyBtn);

        commonOperations.create_New_Custom_List(elements.activityPageElements.activitySettingsIcon,
            elements.activityPageElements.activityCustomListNewLink,
            elements.activityPageElements.activityCustomListOrderLink,
            elements.activityPageElements.activityCustomListNewNameInput,
            elements.activityPageElements.activityCustomListNewSaveBtn,
            elements.activityPageElements.activityCustomListOrderListDiv,
            elements.activityPageElements.activityCustomListOrderCloseBtn,
            elements.activityPageElements.activityCustomListAddColBtn,
            elements.activityPageElements.activityCustomListRemoveColBtn,
            elements.activityPageElements.activityCustomListAvailableColumns,
            elements.activityPageElements.activityCustomListSelectedColumns,
            elements.activityPageElements.activitiesTable); //Create new custom list

        commonOperations.edit_Custom_List(elements.activityPageElements.activitySettingsIcon,
            elements.activityPageElements.activityCustomListEditLink,
            elements.activityPageElements.activityCustomListDeleteBtn,
            elements.activityPageElements.activityCustomListNewNameInput,
            elements.activityPageElements.activityCustomListNewSaveBtn,
            elements.activityPageElements.activityCustomListOrderLink,
            elements.activityPageElements.activityCustomListOrderListDiv,
            elements.activityPageElements.activityCustomListOrderCloseBtn); //Edit  existing custom list

        commonOperations.clone_Custom_List(elements.activityPageElements.activitySettingsIcon,
            elements.activityPageElements.activityCustomListCloneInput,
            elements.activityPageElements.activityCustomListCloneLink,
            elements.activityPageElements.activityCustomListCloneCreateBtn,
            elements.activityPageElements.activityCustomListOrderLink,
            elements.activityPageElements.activityCustomListOrderListDiv,
            elements.activityPageElements.activityCustomListOrderCloseBtn); //clone a custom list

        commonOperations.delete_Custom_List(elements.activityPageElements.activitySettingsIcon,
            elements.activityPageElements.activityCustomListEditLink,
            elements.activityPageElements.activityCustomListDeleteBtn,
            elements.activityPageElements.activityCustomListOrderLink,
            elements.activityPageElements.activityCustomListOrderListDiv,
            elements.activityPageElements.activityCustomListOrderCloseBtn); //delete a custom list                                                          //delete custom list

        commonOperations.check_paging_display(elements.activityPageElements.activityPaginationDiv); //Ensure that the paging is displayed correctly

    });
};
exports.activities_customList = activities_customList;

var activities_createCase = function () {
    describe("ACTIVITIES TAB - CREATE NEW CASE", function () {


        // Ensure that default category and subcategory are auto populated (if any)

        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_ActivityCatg_Tab();
        // adminOperations.create_SubCategory();
        // navigateOperations.navigate_To_Activities_Tab();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.case_Icon_isPresent_withPermission();// Case icon is present with case permission
                                                // Create modal is displayed
                                                // Related Entity is a required field
                                                // Ensure that the search behaves properly
                                                // Ensure that user can enter text in the input fields
        me.category_Subcategory_DropDown();
        me.check_DescriptionTable_isDisplay();
        me.search_Behaves_Properly();
    });
};
exports.activities_createCase = activities_createCase;

var activities_Case_Detail_Page = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE",function () {

        // adminOperations.create_Category();
        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_Contacts_Tab();
        // contactOperations.create_Contact('contact');
        // patientOperations.create_Patient();
        // navigateOperations.navigate_To_Activities_Tab();
        // me.create_New_Case();                               //Create a new Case

        //This test case is covered in create_New_Case();   //Save and Back', 'Save', and 'Cancel' Display at top and bottom of screen
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.check_requiredFields();                          //Organization and Date are required fields and noted with an "*"
        me.check_DateField();                               //Date field pre-populates with current date and time
        me.add_CaseData();                                  // Select Status
                                                            // Add Subject
                                                            // Add Description
                                                            // add Organization
                                                            // Select Category
                                                            // Select Sub-Category (dependent on Category)
                                                            // Select Assigned to (Defaults to current user)
                                                            // Select Priority
                                                            // Add CC User
                                                            // Navigating to a new screen presents message to Save Changes

        me.navigate_To_New_Case();

        // this cases are covered in add_CaseData()         // Edit Organization
        // Edit Subject
        // Edit Category
        // Edit Sub-Category (dependannt on Category)
        // Select Root Cause
        // Edit Assigned to (Defaults to current user)
        // Edit Status
        // Edit Priority
        // Edit Description
        // Edit CC User

        me.edit_CaseData();                                 // Add Resolved by (Defaults to Current User)
                                                            // Activity Status automatically switches to Closed
                                                            // Resolved on date automatically populates with the current date
                                                            // Ensure that resolved date is displayed correctly
                                                            // Ensure all the fields are displayed correctly
                                                            // Add Resolution Description
                                                            // Select Corrective Action
                                                            // Add Related Item - Contact - Contact Icon Presents
                                                            // Related Item - Contact - Name, Phone and Email are displayed after item is added
        me.navigate_To_New_Case();
        me.add_Memo_To_Case();                              // Add Memo - Case is added to Memo as a Related Item
                                                            // BreadCrumb trail presents Case --> Memo Trail

        me.add_Task_To_Case();                              // Add Task - Case is added to Task as a Related Item
                                                            // BreadCrumb trail presents Case --> Task Trail

        me.navigate_To_New_Case();
        me.check_Attachments();                             // Add Attachment
                                                            // Delete Attachment

        //This test case is covered in add_Memo_To_Case     // Add Memo - Current Case appears as a related Item
        //This test case is covered in add_Memo_To_Case     // Add Task - Current Case appears as a related item

        me.navigate_To_New_Case();                          // Save after deleting a required field (fail)
        me.check_Required_Field();                          // Save after refilling the required field
    });
};
exports.activities_Case_Detail_Page = activities_Case_Detail_Page;

var activities_Memo_On_Case = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - MEMO ON CASE", function () {
        // orgOperations.create_New_Org();

        // navigateOperations.navigate_To_Activities_Tab();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.check_Memo_On_Case();            // Memo icon is present with memo write permission
                                            // Ensure that the create memo modal is displayed
                                            // Enter all the requried fields and save
                                            // Memo should be displayed in the activties subtab
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_New_Case_User1();
        me.check_Memo_Icon_Not_Visible();   // Memo icon is present without memo write permission
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.activities_Memo_On_Case = activities_Memo_On_Case;

var activities_Task_On_Case = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - TASK ON CASE", function () {
        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_Activities_Tab();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.check_Task_On_Case();    // Task icon is present with Task write permission
                                    // Ensure that the create Task modal is displayed
                                    // Enter all the requried fields and save
                                    // Task should be displayed in the activties subtab

        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_New_Case_User1();
        me.check_Task_Icon_Not_Visible();   // Task icon is present without Task write permission
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.activities_Task_On_Case = activities_Task_On_Case;

var activities_Case_Activity_SubTab = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - ACTIVITY SUBTAB", function () {
        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_Activities_Tab();                                                             //Clicking the Activity Tab navigates to the Activity List Screen
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.activities_Table_Display();      //Ensure that the list of activities are displayed properly
        me.check_editIcon_DisplayOn_WritePermission(); //edit icon is displayed if the user has write permission
        me.check_Activity_Delete_Link();    //delete icon is displayed if the user has del permission
    });
};
exports.activities_Case_Activity_SubTab =activities_Case_Activity_SubTab;

var activities_Case_Activity_SubTab_CustomList = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - ACTIVITY SUBTAB CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.navigate_To_Activity_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.caseActSubTabQuickFilterLink,
            elements.activityPageElements.activitiesTable,
            elements.activityPageElements.caseActSubTabFilterAvailColumns,
            elements.activityPageElements.caseActSubTabFilterSelectedColumns,
            elements.activityPageElements.caseActSubTabFilterAddBtn,
            elements.activityPageElements.caseActSubTabFilterRemoveBtn,
            elements.activityPageElements.caseActSubTabFilterApplyBtn);              //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.create_New_Custom_List(elements.activityPageElements.caseActSubTabSettingsIcon,
            elements.activityPageElements.caseActSubTabCustomListNewLink,
            elements.activityPageElements.caseActSubTabCustomListOrderLink,
            elements.activityPageElements.caseActSubTabNewCustomListInput,
            elements.activityPageElements.caseActSubTabNewCustomListSaveBtn,
            elements.activityPageElements.caseActSubTabOrderCustomListDiv,
            elements.activityPageElements.caseActSubTabOrderCustomListCloseBtn,
            elements.activityPageElements.caseActSubTabCustomListAddBtn,
            elements.activityPageElements.caseActSubTabCustomListRemoveBtn,
            elements.activityPageElements.caseActSubTabCustomListAvailColumns,
            elements.activityPageElements.caseActSubTabCustomListSelectedColumns,
            elements.activityPageElements.caseActivitiesTable);                                //Create new custom list

        // me.navigate_To_Activity_Subtab();
        // commonOperations.edit_Custom_List(elements.activityPageElements.caseActSubTabSettingsIcon,
        //     elements.activityPageElements.caseActSubTabCustomListEditLink,
        //     elements.activityPageElements.caseActSubTabCustomListDeleteBtn,
        //     elements.activityPageElements.caseActSubTabNewCustomListInput,
        //     elements.activityPageElements.caseActSubTabNewCustomListSaveBtn,
        //     elements.activityPageElements.caseActSubTabCustomListOrderLink,
        //     elements.activityPageElements.caseActSubTabOrderCustomListDiv,
        //     elements.activityPageElements.caseActSubTabOrderCustomListCloseBtn);              //Edit  existing custom list
        //
        // me.navigate_To_Activity_Subtab();
        // commonOperations.clone_Custom_List(elements.activityPageElements.caseActSubTabSettingsIcon,
        //     elements.activityPageElements.caseActSubTabCloneCustomListInput,
        //     elements.activityPageElements.caseActSubTabCustomListCloneLink,
        //     elements.activityPageElements.caseActSubTabCloneCustomListCreateBtn,
        //     elements.activityPageElements.caseActSubTabCustomListOrderLink,
        //     elements.activityPageElements.caseActSubTabOrderCustomListDiv,
        //     elements.activityPageElements.caseActSubTabOrderCustomListCloseBtn);          //clone a custom list
        //
        // me.navigate_To_Activity_Subtab();
        // commonOperations.delete_Custom_List(elements.activityPageElements.caseActSubTabSettingsIcon,
        //     elements.activityPageElements.caseActSubTabCustomListEditLink,
        //     elements.activityPageElements.caseActSubTabCustomListDeleteBtn,
        //     elements.activityPageElements.caseActSubTabCustomListOrderLink,
        //     elements.activityPageElements.caseActSubTabOrderCustomListDiv,
        //     elements.activityPageElements.caseActSubTabOrderCustomListCloseBtn);          //delete custom list
        //
        // me.navigate_To_Activity_Subtab();
        // commonOperations.check_paging_display(elements.activityPageElements.caseActSubTabPaginationDiv);                 //Ensure that the paging is displayed correctly
    });
};
exports.activities_Case_Activity_SubTab_CustomList = activities_Case_Activity_SubTab_CustomList;

var activities_Case_Attachment_SubTab = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - ATTACHMENTS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_Activities_Tab();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.navigate_To_Attachments_Subtab();
        me.check_Case_Attachments_Subtab();                                                             // Upload attachment link is present with attachement write permission
                                                                                                        // Ensure that attachment pop-up is displayed
                                                                                                        // Name and file are required fields
                                                                                                        // Add description
                                                                                                        // Phi is checkbox is present
                                                                                                        // Add a attachment with phi
        //ask doubt to eric in this test case                                                           //edit icon is displayed if the user has write permission
        // delete icon is displayed if the user has del permission

        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_New_Case_User1();
        //ask doubt to eric in this test case   // edit icon is displayed without write permission (fail)
        // del icon is displayed without del permission (fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.activities_Case_Attachment_SubTab = activities_Case_Attachment_SubTab;

var activities_Case_Attachment_SubTab_CustomList = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - ATTACHMENTS SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.navigate_To_Attachments_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.caseAttachmentsQuickFilterLink,
            elements.activityPageElements.caseAttachmentsTable,
            elements.activityPageElements.caseAttachmentsFilterAvailColumns,
            elements.activityPageElements.caseAttachmentsFilterSelectedColumns,
            elements.activityPageElements.caseAttachmentsFilterAddBtn,
            elements.activityPageElements.caseAttachmentsFilterRemoveBtn,
            elements.activityPageElements.caseAttachmentsFilterApplyBtn);                               // Ensure that  Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Attachments_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.caseAttachmentsSettingsIcon,
            elements.activityPageElements.caseAttachmentsNewCustomListLink,
            elements.activityPageElements.caseAttachmentsOrderCustomListLink,
            elements.activityPageElements.caseAttachmentsNewCustomListInput,
            elements.activityPageElements.caseAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.caseAttachmentCustomListOrderListDiv,
            elements.activityPageElements.caseAttachmentOrderListCloseBtn,
            elements.activityPageElements.caseAttachmentsCustomListAddBtn,
            elements.activityPageElements.caseAttachmentsCustomListRemoveBtn,
            elements.activityPageElements.caseAttachmentsCustomListAvailColumns,
            elements.activityPageElements.caseAttachmentsCustomListSelectedColumns,
            elements.activityPageElements.caseAttachmentsTable);                             //Create new custom list
        me.navigate_To_Attachments_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.caseAttachmentsSettingsIcon,
            elements.activityPageElements.caseAttachmentsEditCustomListLink,
            elements.activityPageElements.caseAttachmentsDeleteCustomListLink,
            elements.activityPageElements.caseAttachmentsNewCustomListInput,
            elements.activityPageElements.caseAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.caseAttachmentsOrderCustomListLink,
            elements.activityPageElements.caseAttachmentCustomListOrderListDiv,
            elements.activityPageElements.caseAttachmentOrderListCloseBtn);                             //Edit  existing custom list
        me.navigate_To_Attachments_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.caseAttachmentsSettingsIcon,
            elements.activityPageElements.caseAttachmentsCloneCustomListInput,
            elements.activityPageElements.caseAttachmentsCloneCustomListLink,
            elements.activityPageElements.caseAttachmentsCloneCustomListCreateBtn,
            elements.activityPageElements.caseAttachmentsOrderCustomListLink,
            elements.activityPageElements.caseAttachmentCustomListOrderListDiv,
            elements.activityPageElements.caseAttachmentOrderListCloseBtn);                             //clone a custom list
        me.navigate_To_Attachments_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.caseAttachmentsSettingsIcon,
            elements.activityPageElements.caseAttachmentsEditCustomListLink,
            elements.activityPageElements.caseAttachmentsDeleteCustomListLink,
            elements.activityPageElements.caseAttachmentsOrderCustomListLink,
            elements.activityPageElements.caseAttachmentCustomListOrderListDiv,
            elements.activityPageElements.caseAttachmentOrderListCloseBtn);                             //delete a custom list                                                          //delete custom list
        me.navigate_To_Attachments_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.caseAttachmentsPaginationDiv); //Ensure that the paging is displayed correctly
    });
};
exports.activities_Case_Attachment_SubTab_CustomList = activities_Case_Attachment_SubTab_CustomList;

var activities_Case_Messages_SubTab = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - MESSAGES SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.check_Case_Message_List();                                                                        //Ensure that message list view is displayed properly
        me.check_Delete_Icon_Present('case');                                                                 // Ensure that delete icon is displayed if user has delete permission


    });
};
exports.activities_Case_Messages_SubTab = activities_Case_Messages_SubTab;

var activities_Case_Messages_SubTab_CustomList = function () {
    describe("ACTIVITIES TAB - CASE DETAIL PAGE - MESSAGES SUBTAB - CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Case();
        me.navigate_To_Messages_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.caseMessagesQuickFilterLink,
            elements.activityPageElements.caseMessagesTable,
            elements.activityPageElements.caseMessagesFilterAvailColumns,
            elements.activityPageElements.caseMessagesFilterSelectedColumns,
            elements.activityPageElements.caseMessagesFilterAddBtn,
            elements.activityPageElements.caseMessagesFilterRemoveBtn,
            elements.activityPageElements.caseMessagesFilterApplyBtn);                                  //Ensure that  Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Messages_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.caseMessagesSettingsIcon,
            elements.activityPageElements.caseMessagesNewCustomListLink,
            elements.activityPageElements.caseMessagesOrderCustomListLink,
            elements.activityPageElements.caseMessagesNewCustomListInput,
            elements.activityPageElements.caseMessagesNewCustomListSaveBtn,
            elements.activityPageElements.caseMessagesOrderCustomListDiv,
            elements.activityPageElements.caseMessagesOrderCustomListCloseBtn,
            elements.activityPageElements.caseMessagesCustomListAddColBtn,
            elements.activityPageElements.caseMessagesCustomListRemoveBtn,
            elements.activityPageElements.caseMessagesCustomListAvailColumns,
            elements.activityPageElements.caseMessagesCustomListSelectedColumns,
            elements.activityPageElements.caseMessagesTable);                         // Create new custom list
        me.navigate_To_Messages_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.caseMessagesSettingsIcon,
            elements.activityPageElements.caseMessagesEditCustomListLink,
            elements.activityPageElements.caseMessagesDeleteCustomListLink,
            elements.activityPageElements.caseMessagesNewCustomListInput,
            elements.activityPageElements.caseMessagesNewCustomListSaveBtn,
            elements.activityPageElements.caseMessagesOrderCustomListLink,
            elements.activityPageElements.caseMessagesOrderCustomListDiv,
            elements.activityPageElements.caseMessagesOrderCustomListCloseBtn);                         // Edit  existing custom list
        me.navigate_To_Messages_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.caseMessagesSettingsIcon,
            elements.activityPageElements.caseMessagesCloneCustomListInput,
            elements.activityPageElements.caseMessagesCloneCustomListLink,
            elements.activityPageElements.caseMessagesCloneCustomListCreateBtn,
            elements.activityPageElements.caseMessagesOrderCustomListLink,
            elements.activityPageElements.caseMessagesOrderCustomListDiv,
            elements.activityPageElements.caseMessagesOrderCustomListCloseBtn);                         // clone custom list
        me.navigate_To_Messages_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.caseMessagesSettingsIcon,
            elements.activityPageElements.caseMessagesEditCustomListLink,
            elements.activityPageElements.caseMessagesDeleteCustomListLink,
            elements.activityPageElements.caseMessagesOrderCustomListLink,
            elements.activityPageElements.caseMessagesOrderCustomListDiv,
            elements.activityPageElements.caseMessagesOrderCustomListCloseBtn);                         // delete a custom list
        me.navigate_To_Messages_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.caseMessagesPaginationDiv); // Ensure that the paging is displayed correctly
    });
};
exports.activities_Case_Messages_SubTab_CustomList = activities_Case_Messages_SubTab_CustomList;

var activities_createMemo = function () {
    describe("ACTIVITIES TAB - CREATE NEW MEMO", function () {
        // orgOperations.create_New_Org();
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Activities_Tab();
        me.check_Memo_Icon_Present();           // Case icon is present with case permission
        me.check_Memo_Create_Modal();           // Create modal is displayed
        me.check_Memo_Required_Fields();        // Related Entity is a required field
        me.check_Memo_Org_Search();             // Ensure that the search behaves properly
        //Skipped this test case                // Ensure that user can enter text in the input fields
        //This test case is covered in check_Memo_Org_Search // Category and subcategory is a drop down
        //Confirm with eric                     // Ensure that default category and subcategory are auto populated (if any)
    });
};
exports.activities_createMemo = activities_createMemo;

var activities_Memo_Detail_Page = function () {
    describe("ACTIVITIES TAB - MEMO DETAIL PAGE", function () {
        // orgOperations.create_New_Org(true);                 // 'true' parameter for organization with hostcode
        // contactOperations.create_Contact('contact');
        // patientOperations.create_Patient();
        // adminOperations.create_Category();
        // adminOperations.create_SubCategory();
        // me.create_New_Memo();                               // Create a new Memo
        //This test case is covered in create_New_Memo()    // Save and Back', 'Save', and 'Cancel' Display at top and bottom of screen
        //This test case is covered later                   // Breadcrumb trail presents navigation history
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Memo();
        me.check_Memo_Date_Field();                         // Date field pre-populates with current date and time
        me.check_Memo_Number();                             // Memo Number is automatically created
        me.check_Memo_Required_Fields();                    // Organization and Date is required fields and noted with an "*"
        me.check_Add_Memo_Data();                           // Add Subject
                                                            // Add Description
                                                            // Add Organization
                                                            // Find Organization via Host Code
                                                            // Find Organization via Org number
                                                            // Select Category from Drop-down
                                                            // Select Subcategory from Drop-Down
                                                            // Subcategory list changes depending on Category selection
                                                            // Add CC Users
                                                            // Add Related Item - Contact - Contact Icon Presents
                                                            // Add Related Item - Patient - Patient Icon Presents
                                                            // Related Item - Contact - Name, Phone and Email are displayed after item is added
                                                            // Related Item - Patient - Name and MRN are displayed after item is added
                                                            // Navigating to a new screen presents message to Save Changes
                                                            // Save' commites Changes to DB

        me.check_Modify_Memo_Data();                        // Edit Subject
                                                            // Edit Description
                                                            // Edit Organization
                                                            // Edit Category from Drop-down
                                                            // Edit Subcategory from Drop-Down
                                                            // Delete CC Users
                                                            // Delete Related Item - Contact
                                                            // Delete Related Item - Patient

        contactOperations.navigate_To_New_Contact();
        me.check_Memo_From_Contact_Page();                  // Create a Memo from a Provider Record - Provider is added as a related item
                                                            // Create a Memo from a Contact Record - Contact is added as a related item
        //this test case is covered in create_Memo_fromOrgGeneralTab()      // Create a Memo from a Organization Record - Org is added as the Organization
        //this test case is covered in memo attachments subtab   // Add an Attachment while in view mode

    });
};
exports.activities_Memo_Detail_Page = activities_Memo_Detail_Page;

var activities_Memo_Attachments_Subtab = function () {
    describe("ACTIVITIES TAB - MEMO DETAIL PAGE ATTACHMENTS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_New_Memo();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Memo();
        me.check_Memo_Attachment_UploadLinkPresent();           // Upload attachment link is present with attachment write permission
        me.check_Memo_Attachment_PopupIsDisplayed();            // Ensure that attachment pop-up is displayed
        me.check_Memo_Attachment_RequiredFields();              // Name and file are required fields
        me.check_Memo_Attachment_AddDescription();              // Add description
        me.check_Memo_Attachment_PhiCheckboxPresent();          // Phi is checkbox is present
        me.check_Memo_Attachment_AddAttachment();               // Add a attachment with phi
        me.check_Memo_Attachment_DeleteIconPresent();           // delete icon is displayed if the user has del permission
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Memo();
        // me.check_Memo_Attachment_UploadLinkNotPresent();        // Upload attachment link is present without attachment without write permission
        // me.check_Memo_Attachment_DeleteIconNotPresent();        // del icon is displayed without del permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.activities_Memo_Attachments_Subtab = activities_Memo_Attachments_Subtab;

var activities_Memo_Attachments_Subtab_CustomList = function () {
    describe("ACTIVITIES TAB - MEMO DETAIL PAGE ATTACHMENTS SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Memo();
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.memoAttachmentsQuickFilterLink,
            elements.activityPageElements.memoAttachmentTable,
            elements.activityPageElements.memoAttachmentsFilterAvailColumns,
            elements.activityPageElements.memoAttachmentsFilterSelectedColumns,
            elements.activityPageElements.memoAttachmentsFilterAddColumnBtn,
            elements.activityPageElements.memoAttachmentsFilterRemoveColumnBtn,
            elements.activityPageElements.memoAttachmentsFilterApplyBtn);// Ensure that Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.memoAttachmentsSettingsIcon,
            elements.activityPageElements.memoAttachmentsNewCustomListLink,
            elements.activityPageElements.memoAttachmentsOrderCustomListLink,
            elements.activityPageElements.memoAttachmentsNewCustomListInput,
            elements.activityPageElements.memoAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.memoAttachmentsOrderCustomListDiv,
            elements.activityPageElements.memoAttachmentsOrderCustomListCloseBtn,
            elements.activityPageElements.memoAttachmentsCustomListAddColumnBtn,
            elements.activityPageElements.memoAttachmentsCustomListRemoveBtn,
            elements.activityPageElements.memoAttachmentsCustomListAvailColumns,
            elements.activityPageElements.memoAttachmentsCustomListSelectedColumns,
            elements.activityPageElements.memoAttachmentTable);// Create new custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.memoAttachmentsSettingsIcon,
            elements.activityPageElements.memoAttachmentsCustomListEditLink,
            elements.activityPageElements.memoAttachmentsDeleteCustomListLink,
            elements.activityPageElements.memoAttachmentsNewCustomListInput,
            elements.activityPageElements.memoAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.memoAttachmentsOrderCustomListLink,
            elements.activityPageElements.memoAttachmentsOrderCustomListDiv,
            elements.activityPageElements.memoAttachmentsOrderCustomListCloseBtn);// Edit  existing custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.memoAttachmentsSettingsIcon,
            elements.activityPageElements.memoAttachmentsCloneCustomListInput,
            elements.activityPageElements.memoAttachmentsCustomListCloneLink,
            elements.activityPageElements.memoAttachmentsCloneCustomListCreateBtn,
            elements.activityPageElements.memoAttachmentsOrderCustomListLink,
            elements.activityPageElements.memoAttachmentsOrderCustomListDiv,
            elements.activityPageElements.memoAttachmentsOrderCustomListCloseBtn);// clone custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.memoAttachmentsSettingsIcon,
            elements.activityPageElements.memoAttachmentsCustomListEditLink,
            elements.activityPageElements.memoAttachmentsDeleteCustomListLink,
            elements.activityPageElements.memoAttachmentsOrderCustomListLink,
            elements.activityPageElements.memoAttachmentsOrderCustomListDiv,
            elements.activityPageElements.memoAttachmentsOrderCustomListCloseBtn);// delete a custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.memoAttachmentPagingDiv);// Ensure that the paging is displayed correctly
    });
};
exports.activities_Memo_Attachments_Subtab_CustomList = activities_Memo_Attachments_Subtab_CustomList;

var activities_Memo_Messages_Subtab = function () {
    describe("ACTIVITIES TAB - MEMO DETAIL PAGE MESSAGES SUBTAB", function () {

        // orgOperations.create_New_Org();
        // me.create_New_Memo();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Memo();
        me.check_Memo_Message_List();        // Ensure that message list view is displayed properly
        me.check_Delete_Icon_Present('memo'); // Ensure that delete icon is displayed if user has delete permission


    });
};
exports.activities_Memo_Messages_Subtab = activities_Memo_Messages_Subtab;

var activities_Memo_Messages_Subtab_CustomList = function () {
    describe("ACTIVITIES TAB - MEMO DETAIL PAGE MESSAGES SUBTAB CUSTOM LIST", function () {
        // me.pauseBrowserSomeTime();
        me.navigate_To_New_Memo();
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.memoMessageQuickFilterLink,
            elements.activityPageElements.memoMessagesTable,
            elements.activityPageElements.memoMessageFilterAvailColumns,
            elements.activityPageElements.memoMessageFilterSelectedColumns,
            elements.activityPageElements.memoMessageFilterAddBtn,
            elements.activityPageElements.memoMessageFilterRemoveBtn,
            elements.activityPageElements.memoMessageFilterApplyBtn);       //Ensure that  Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.memoMessageSettingsIcon,
            elements.activityPageElements.memoMessageNewCustomListLink,
            elements.activityPageElements.memoMessageOrderCustomListLink,
            elements.activityPageElements.memoMessageNewCustomListInput,
            elements.activityPageElements.memoMessageCustomListSaveBtn,
            elements.activityPageElements.memoMessageOrderCustomListDiv,
            elements.activityPageElements.memoMessageOrderCustomListCloseBtn,
            elements.activityPageElements.memoMessageCustomListAddColumnBtn,
            elements.activityPageElements.memoMessageCustomListRemoveColumnBtn,
            elements.activityPageElements.memoMessageCustomListAvailColumns,
            elements.activityPageElements.memoMessageCustomListSelectedColumns,
            elements.activityPageElements.memoMessagesTable);// Create new custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.memoMessageSettingsIcon,
            elements.activityPageElements.memoMessageCustomListEditLink,
            elements.activityPageElements.memoMessageCustomListDeleteLink,
            elements.activityPageElements.memoMessageNewCustomListInput,
            elements.activityPageElements.memoMessageCustomListSaveBtn,
            elements.activityPageElements.memoMessageOrderCustomListLink,
            elements.activityPageElements.memoMessageOrderCustomListDiv,
            elements.activityPageElements.memoMessageOrderCustomListCloseBtn);// Edit  existing custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.memoMessageSettingsIcon,
            elements.activityPageElements.memoMessageCloneCustomListInput,
            elements.activityPageElements.memoMessageCustomListCloneLink,
            elements.activityPageElements.memoMessageCloneCustomListCreateBtn,
            elements.activityPageElements.memoMessageOrderCustomListLink,
            elements.activityPageElements.memoMessageOrderCustomListDiv,
            elements.activityPageElements.memoMessageOrderCustomListCloseBtn);// clone custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.memoMessageSettingsIcon,
            elements.activityPageElements.memoMessageCustomListEditLink,
            elements.activityPageElements.memoMessageCustomListDeleteLink,
            elements.activityPageElements.memoMessageOrderCustomListLink,
            elements.activityPageElements.memoMessageOrderCustomListDiv,
            elements.activityPageElements.memoMessageOrderCustomListCloseBtn);// delete a custom list
        me.navigate_To_Memo_Messages_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.memoMessagePaginationDiv);// Ensure that the paging is displayed correctly
    });
};
exports.activities_Memo_Messages_Subtab_CustomList = activities_Memo_Messages_Subtab_CustomList;

var activity_createTask = function () {
    describe("ACTIVITIES TAB - CREATE NEW TASK", function () {
        // orgOperations.create_New_Org();
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Activities_Tab();
        me.check_Task_Icon_Present();                            // Case icon is present with case permission
        me.check_Task_Create_Modal();                            // Create modal is displayed
        me.check_Task_Required_Fields();                         // Related Entity is a required field
        me.check_Task_Org_Search();                              // Ensure that the search behaves properly
        //Skipped this test case                                 // Ensure that user can enter text in the input fields
        //this test case is covered in check_Task_Org_Search()   // Category and subcategory is a drop down
        //Confirm with eric                                      // Ensure that default category and subcategory are auto populated (if any)
    });
};
exports.activity_createTask = activity_createTask;

var activity_Task_Detail_Page = function () {
    describe("ACTIVITIES TAB - TASK DETAIL PAGE", function () {

        // orgOperations.create_New_Org();
        // me.create_New_Task();               // Create a new Task
                                            // Save and Back', 'Save', and 'Cancel' Display at top and bottom of screen
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        me.check_Task_Date_Field();         // Date field pre-populates with current date and time
        me.check_Task_Number();             // Task Number is automatically created
        me.check_Task_DueDateDefault();     // Add Due Date - Defaults to 24 hours from current time
        me.navigate_To_New_Task();
        me.add_Task_Data();                 // Select Status
                                            // Add Subject
                                            // Add description
                                            // add Organization
                                            // Select Category
                                            // Select Sub-Category (Dependant on Category)
                                            // Select Assigned to (Defaults to current user)
                                            // Select Priority (Low, Medium, High)
                                            // Add CC: User
                                            // Navigating to a new screen presents message to Save Changes
                                            // Save' commites Changes to DB
        // orgOperations.create_New_Org();
        // contactOperations.create_Contact('contact');
        // patientOperations.create_Patient();
        me.navigate_To_New_Task();
        me.edit_Task_Data();                // Edit Status
                                            // Edit Subject
                                            // Edit description
                                            // Edit Organization
                                            // Edit Category
                                            // Edit Sub-Category (Dependant on Category)
                                            // Edit Assigned to
                                            // Edit Priority (Low, Medium, High)
                                            // Edit CC: User
                                            // Delete Status
                                            // Delete Subject
                                            // Delete description
                                            // Delete Assigned to
                                            // Delete CC: User
                                            // Add Related Item - Contact - Contact Icon Presents
                                            // Add Related Item - Provider Provider Icon Presents
                                            // Add Related Item - Order - Order Icon Presents
                                            // Add Related Item - Patient - Patient Icon Presents
                                            // Add Related Item - Specimen - Specimen Icon Presents
                                            // Related Item - Contact - Name, Phone and Email are displayed after item is added
                                            // Related Item - Provider - Name, Phone and Email are displayed after item is added
                                            // Related Item - Order - Order ID, Provider and Order date are displayed after item is added
                                            // Related Item - Patient - Name and MRN are displayed after item is added
                                            // Related Item - Specimen - Specimen Id, Panel Name, and Specimen type are dispalyed after item is added
                                            // Add Attachment
                                            // Delete Attachment
    });
};
exports.activity_Task_Detail_Page = activity_Task_Detail_Page;

var activity_Task_Attachments_Subtab = function () {
    describe("ACTIVITIES TAB - TASK DETAIL PAGE ATTACHMENTS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_New_Task();

        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        me.navigate_To_Task_Attachments_Subtab();
        me.check_Task_Attachment_UploadLinkPresent();           // Upload attachment link is present with attachement write permission
        me.check_Task_Attachment_PopupIsDisplayed();            // Ensure that attachment pop-up is displayed
        me.check_Task_Attachment_RequiredFields();              // Name and file are required fields
        me.check_Task_Attachment_AddDescription();              // Add description
        me.check_Task_Attachment_PhiCheckboxPresent();          // Phi is checkbox is present
        me.check_Task_Attachment_AddAttachment();               // Add a attachment with phi
        me.check_Task_Attachment_DeleteIconPresent();           // delete icon is displayed if the user has del permission
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Task();
        // me.navigate_To_Task_Attachments_Subtab();
        // me.check_Task_Attachment_UploadLinkNotPresent();                            // Upload attachment link is present without attachment without write permission
        // me.check_Task_Attachment_DeleteIconNotPresent();                            // del icon is displayed without del permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

        // edit icon is displayed if the user has write permission
        // edit icon is displayed without write permission (fail)
    });
};
exports.activity_Task_Attachments_Subtab = activity_Task_Attachments_Subtab;

var activity_Task_Attachments_Subtab_CustomList = function () {
    describe("ACTIVITIES TAB - TASK DETAIL PAGE ATTACHMENTS SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.taskAttachmentsQuickFilterLink,
            elements.activityPageElements.taskAttachmentsTable,
            elements.activityPageElements.taskAttachmentsFilterAvailColumns,
            elements.activityPageElements.taskAttachmentsFilterSelectedColumns,
            elements.activityPageElements.taskAttachmentsFilterAddBtn,
            elements.activityPageElements.taskAttachmentsFilterRemoveBtn,
            elements.activityPageElements.taskAttachmentsFilterApplyBtn);           // Ensure that Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.taskAttachmentsSettingsIcon,
            elements.activityPageElements.taskAttachmentsNewCustomListLink,
            elements.activityPageElements.taskAttachmentsOrderCustomListLink,
            elements.activityPageElements.taskAttachmentsNewCustomListInput,
            elements.activityPageElements.taskAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.taskAttachmentsOrderCustomListDiv,
            elements.activityPageElements.taskAttachmentsOrderCustomListCloseBtn,
            elements.activityPageElements.taskAttachmentsCustomListAddBtn,
            elements.activityPageElements.taskAttachmentsCustomListRemoveBtn,
            elements.activityPageElements.taskAttachmentsCustomListAvailColumns,
            elements.activityPageElements.taskAttachmentsCustomListSelectedColumns,
            elements.activityPageElements.taskAttachmentsTable);  // Create new custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.taskAttachmentsSettingsIcon,
            elements.activityPageElements.taskAttachmentsEditCustomListLink,
            elements.activityPageElements.taskAttachmentsCustomListDeleteLink,
            elements.activityPageElements.taskAttachmentsNewCustomListInput,
            elements.activityPageElements.taskAttachmentsNewCustomListSaveBtn,
            elements.activityPageElements.taskAttachmentsOrderCustomListLink,
            elements.activityPageElements.taskAttachmentsOrderCustomListDiv,
            elements.activityPageElements.taskAttachmentsOrderCustomListCloseBtn);  // Edit  existing custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.taskAttachmentsSettingsIcon,
            elements.activityPageElements.taskAttachmentsCloneCustomListInput,
            elements.activityPageElements.taskAttachmentsCloneCustomListLink,
            elements.activityPageElements.taskAttachmentsCloneCustomListCreateBtn,
            elements.activityPageElements.taskAttachmentsOrderCustomListLink,
            elements.activityPageElements.taskAttachmentsOrderCustomListDiv,
            elements.activityPageElements.taskAttachmentsOrderCustomListCloseBtn);  // clone custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.taskAttachmentsSettingsIcon,
            elements.activityPageElements.taskAttachmentsEditCustomListLink,
            elements.activityPageElements.taskAttachmentsCustomListDeleteLink,
            elements.activityPageElements.taskAttachmentsOrderCustomListLink,
            elements.activityPageElements.taskAttachmentsOrderCustomListDiv,
            elements.activityPageElements.taskAttachmentsOrderCustomListCloseBtn);  // delete a custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.taskAttachmentsPaginationDiv);// Ensure that the paging is displayed correctly
    });
};
exports.activity_Task_Attachments_Subtab_CustomList = activity_Task_Attachments_Subtab_CustomList;

var activity_Task_Messages_Subtab = function () {
    describe("ACTIVITIES TAB - TASK DETAIL PAGE MESSAGES SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_New_Task();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        me.check_Task_Message_List();        // Ensure that message list view is displayed properly
        me.check_Delete_Icon_Present('task'); // Ensure that delete icon is displayed if user has delete permission



    });
};
exports.activity_Task_Messages_Subtab = activity_Task_Messages_Subtab;

var activity_Task_Messages_Subtab_CustomList = function () {
    describe("ACTIVITIES TAB - TASK DETAIL PAGE MESSAGES SUBTAB CUSTOMLIST", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.activityPageElements.taskMessagesQuickFilterLink,
            elements.activityPageElements.taskMessagesTable,
            elements.activityPageElements.taskMessagesFilterAvailColumns,
            elements.activityPageElements.taskMessagesFilterSelectedColumns,
            elements.activityPageElements.taskMessagesFilterAddColBtn,
            elements.activityPageElements.taskMessagesFilterRemoveColBtn,
            elements.activityPageElements.taskMessagesFilterApplyBtn);       //Ensure that  Quick Filter behaves properly (check with all the filters)
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.create_New_Custom_List(elements.activityPageElements.taskMessagesSettingsIcon,
            elements.activityPageElements.taskMessagesNewCustomListLink,
            elements.activityPageElements.taskMessagesOrderCustomListLink,
            elements.activityPageElements.taskMessagesNewCustomListInput,
            elements.activityPageElements.taskMessagesNewCustomListSaveBtn,
            elements.activityPageElements.taskMessagesOrderCustomListDiv,
            elements.activityPageElements.taskMessagesOrderCustomListCloseBtn,
            elements.activityPageElements.taskMessagesCustomListAddColumnBtn,
            elements.activityPageElements.taskMessagesCustomListRemoveBtn,
            elements.activityPageElements.taskMessagesCustomListAvailColumns,
            elements.activityPageElements.taskMessagesCustomListSelectedColumns,
            elements.activityPageElements.taskMessagesTable);// Create new custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.edit_Custom_List(elements.activityPageElements.taskMessagesSettingsIcon,
            elements.activityPageElements.taskMessagesEditCustomListLink,
            elements.activityPageElements.taskMessagesDeleteCustomListLink,
            elements.activityPageElements.taskMessagesNewCustomListInput,
            elements.activityPageElements.taskMessagesNewCustomListSaveBtn,
            elements.activityPageElements.taskMessagesOrderCustomListLink,
            elements.activityPageElements.taskMessagesOrderCustomListDiv,
            elements.activityPageElements.taskMessagesOrderCustomListCloseBtn);// Edit  existing custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.clone_Custom_List(elements.activityPageElements.taskMessagesSettingsIcon,
            elements.activityPageElements.taskMessagesCloneCustomListInput,
            elements.activityPageElements.taskMessagesCloneCustomListLink,
            elements.activityPageElements.taskMessagesCloneCustomListCreateBtn,
            elements.activityPageElements.taskMessagesOrderCustomListLink,
            elements.activityPageElements.taskMessagesOrderCustomListDiv,
            elements.activityPageElements.taskMessagesOrderCustomListCloseBtn);// clone custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.delete_Custom_List(elements.activityPageElements.taskMessagesSettingsIcon,
            elements.activityPageElements.taskMessagesEditCustomListLink,
            elements.activityPageElements.taskMessagesDeleteCustomListLink,
            elements.activityPageElements.taskMessagesOrderCustomListLink,
            elements.activityPageElements.taskMessagesOrderCustomListDiv,
            elements.activityPageElements.taskMessagesOrderCustomListCloseBtn);// delete a custom list
        me.navigate_To_Task_Messages_Subtab();
        commonOperations.check_paging_display(elements.activityPageElements.taskMessagesPaginationDiv);// Ensure that the paging is displayed correctly
    });
};
exports.activity_Task_Messages_Subtab_CustomList = activity_Task_Messages_Subtab_CustomList;

var activity_Recent_Items = function () {
    describe("RECENT ITEMS", function () {

        // adminOperations.create_New_Control_Tree();
        // orgOperations.create_New_Org();
        me.pauseBrowserSomeTime();
        orgOperations.navigate_To_New_Org_Page();
        orgOperations.check_Sales_Rep_Is_Auto_Populated();
        orgOperations.check_Sales_Territory_Association();
        orgOperations.select_Sales_Territory_AndVerify_Name();
        orgOperations.navigate_To_New_Org_Page();
        orgOperations.create_Opportunity_fromOrgGeneralTab();
        me.verify_Recent_Organization();            // Verify after going to a Organization the recent items show correctly.

        me.verify_Recent_Activity();                //Verify after going to a number of Activities (from above) the recent items show correctly.
        me.verify_Recent_Opportunity();             //Verify after going to an Opportunity the recent items show correctly.
        //This will be covered in campaign.js       //Verify after going to a Campaign the recent items show correctly.

        // contactOperations.create_Contact('contact');
        me.verify_Recent_Contact();                 //Verify after going to a Contact/Provider the recent items show correctly

        // patientOperations.create_Patient();
        me.verify_Recent_Patient();                 // Verify after going to a Patient the recent items show correctly

    });
};
exports.activity_Recent_Items = activity_Recent_Items;

var activity_AuditLog = function () {
    describe("ACTIVITIES TAB - AUDIT LOG",function () {
        // orgOperations.create_New_Org();
        // me.create_New_Case();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Task();
        //Audit log
        commonOperations.check_auditLink(elements.auditLogElements.auditLogLink);// If the user has audit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page

        //Audit history
        commonOperations.check_auditHistoryTab(elements.activityPageElements.activityEditBtn,
            elements.activityPageElements.caseSubjectInputField,
            elements.activityPageElements.caseSaveBtn,
            elements.auditLogElements.auditLogLink,
            elements.auditLogElements.startDateDiv,
            elements.auditLogElements.endDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.auditLogsTable,
            elements.auditLogElements.exportLink,
            variables.newCaseName_updated);

        //access history
        commonOperations.check_accessHistoryTab(elements.auditLogElements.accessHistoryTab,
            elements.auditLogElements.accessHistoryStartDateDiv,
            elements.auditLogElements.accessHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.accessHistoryRecordsTable,
            elements.auditLogElements.accessHistoryExportLink);

        //All history
        commonOperations.check_allHistoryTab(elements.auditLogElements.allHistoryTab,
            elements.auditLogElements.allHistoryStartDateDiv,
            elements.auditLogElements.allHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.allHistoryRecordsTable,
            elements.auditLogElements.allHistoryExportLink);

        commonOperations.close_AuditLogDiv();

    });
};
module.exports.activity_AuditLog = activity_AuditLog;

var cloud_Search_For_Case = function () {
    describe(" SEARCH FOR CASE OBJECT ", function () {
        orgOperations.create_New_Org();
        me.create_New_Case();
        me.check_visibility_OF_Case_Object();                            //Ensure that case number is displayed
        me.check_NewCase_Search_In_Cloud_Search();                  //Ensure that user can search for a case in cloud search
                                                                    //Edit a case > Ensure that the information is displayed correctly in cloud search
                                                                    //Search by case number
        me.check_Select_Display_Case_In_CloudSearch();              //Ensure that case detail page is displayed when the user selects the case
        me.check_Create_Task_From_Activity_CloudSearch();        //Ensure that user can create task on a case from cloud search
        me.check_Create_Memo_From_Activity_CloudSearch();        //Ensure that user can create Memo on a case from cloud search


    });
}
exports.cloud_Search_For_Case = cloud_Search_For_Case;

var cloud_Search_For_Task = function () {
    describe(" SEARCH FOR TASK OBJECT ", function () {
        orgOperations.create_New_Org();
        me.create_New_Task();
        me.check_visibility_OF_Task_Object();                         //Ensure that Task number is displayed
                                                                      //Ensure that Organization name is displayed
                                                                      //Ensure that category and subcategory is displayed
                                                                      //Ensure that Task number, subject, Organization name, Category,subcategory name, priority,date and resolved on fields are displayed

        me.check_New_Task_Search_In_Cloud_Search();                 //Ensure that user can search for a Task in cloud search
                                                                    //Edit a case > Ensure that the information is displayed correctly in cloud search
                                                                    //Search by Task number
        me.check_Select_Display_Case_In_CloudSearch();              //Ensure that Task detail page is displayed when the user selects the Task
    });
}
exports.cloud_Search_For_Task = cloud_Search_For_Task;




var cloud_Search_For_Memo = function () {
    describe(" SEARCH FOR TASK OBJECT ", function () {
        orgOperations.create_New_Org();
        me.create_New_Memo();
        me.check_visibility_OF_Memo_Object();                            //Ensure that Memo number is displayed
                                                                         //Ensure that category and subcategory is displayed
                                                                         //Ensure that Organization name is displayed
                                                                         //Ensure that Memo number, subject, Organization name, Category,subcategory name, priority,date and resolved on fields are displayed

        me.check_New_Memo_Search_In_Cloud_Search();                  //Ensure that user can search for a Memo in cloud search
        //Edit a Memo > Ensure that the information is displayed correctly in cloud search
        //Search by Memo number
        me.check_Select_Display_Memo_In_CloudSearch();              //Ensure that Memo detail page is displayed when the user selects the Memo

    });
}
exports.cloud_Search_For_Memo = cloud_Search_For_Memo;

var activityDependencies = function () {
    describe("RESOLVE ACTIVITY SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            // 'controlTreeNameCreated',
            // 'createdActivityCategoryName',
            // 'createdSubActivityCategoryName',
            'createdORG',
            'createdCase',
            // 'createdMemo',
            // 'createdTask',
            // 'createdContact',
            // 'createdPatient'
        ]);
        // adminOperations.create_New_Control_Tree();
        // adminOperations.create_Category();
        // adminOperations.create_SubCategory();
        orgOperations.create_New_Org();
        me.create_New_Case();
        // me.create_New_Memo();
        // me.create_New_Task();
        // contactOperations.create_Contact('contact');
        // patientOperations.create_Patient();
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        me.activities_createCase();//
        me.activities_Case_Detail_Page();//
        me.activities_Case_Activity_SubTab(); //
        me.activities_Case_Messages_SubTab();//
        me.activities_createMemo();
        me.activities_Memo_Detail_Page();
        me.activities_Memo_Messages_Subtab();
        me.activity_createTask();
        me.activity_Task_Detail_Page();
        me.activity_Task_Messages_Subtab();
        me.activity_Recent_Items();
        me.activity_AuditLog();

        //test suites which requires two users
        me.activities_tab();
        me.activities_Memo_On_Case();
        me.activities_Task_On_Case();
        me.activities_Case_Attachment_SubTab();
        me.activities_Memo_Attachments_Subtab();
        me.activity_Task_Attachments_Subtab();

        //test suites having custom list tests only
        me.activities_customList();
        me.activities_Case_Activity_SubTab_CustomList(); //check again
        me.activities_Case_Messages_SubTab_CustomList();
        me.activities_Memo_Messages_Subtab_CustomList();
        me.activity_Task_Messages_Subtab_CustomList();
        me.activities_Case_Attachment_SubTab_CustomList();
        me.activities_Memo_Attachments_Subtab_CustomList();
        me.activity_Task_Attachments_Subtab_CustomList();
        //SHEET EXECUTION - ENDS

    });
};
exports.activityDependencies = activityDependencies;
//FUNCTIONS DECLARATION - ENDS

