/**
 * Created by INDUSA
 */
require('mocha');
var userAuth = require('../pages/userAuthentication.js');
var variables = require('../utility/variables.js');
var elements = require('../utility/elements.js');
var commands = require('../utility/commands.js');

var changeLocator = function (locator) {
    describe("Change browser locator",function () {
        it("should change locator of browser",function (browser) {
            switch (locator){
                case  'xpath' : {
                    browser.useXpath();
                    break;
                }
                case 'css' : {
                    browser.useCss();
                    break;
                }
            }
        });
    });
}
module.exports.changeLocator = changeLocator;

var restartBrowser = function (browser,callback) {
    describe("Restart a browser",function () {
        it("should restart a browser",function () {
            browser.end();
            browser.changeLocator('xpath');
            browser.url(variables.fullUrl,function() {
                browser.pause(3000);
                browser.expect.element(elements.loginPageElements.body).to.be.visible;
                browser.expect.element(elements.loginPageElements.usernameInput).to.be.visible;
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.usernameInput);
                commands.checkAndPerform('clearValue',browser,elements.loginPageElements.passwordInput);
                commands.checkAndSetValue(browser,elements.loginPageElements.usernameInput,variables.username);
                commands.checkAndSetValue(browser,elements.loginPageElements.passwordInput,variables.password);
                commands.checkAndPerform('click',browser,elements.loginPageElements.submitBtn);
                browser.pause(6000);
                browser.expect.element(elements.loginPageElements.container).to.be.visible;
                browser.assert.containsText(elements.loginPageElements.container, variables.usernameDisplay);
                var browserStatus = true;
                callback(JSON.stringify(browserStatus));
            });
        });
    })
};
module.exports.restartBrowser = restartBrowser;

var endBrowserSession = function () {
    describe("Close browser session",function () {
        it("should close browser session",function (browser) {
            browser.end();
        });
    });
}
module.exports.endBrowserSession = endBrowserSession;
