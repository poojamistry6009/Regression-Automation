/**
 * Created by INDUSA
 */

require('mocha');
var expect = require('chai').expect;
var fs = require('fs');
var me = require('./bulkimporter.js');
var navigate = require('../utility/navigate.js');
var variables = require('../utility/variables.js');
var elements = require('../utility/elements.js');
var commands = require('../utility/commands.js');

var bulk_Import = function (type) {
    describe("Import "+type, function () {
        it("should import contacts in bulk", function (browser) {

            //READ DATA FROM IMPORT FILES FOR CROSS VERIFYING
            var tblHeaders = [];
            var tbldata = [];
            if(type === 'Contacts')
            {
                fs.readFile(variables.testContactsImportFilePath, 'utf8', function(err, contents) {
                    var data = contents;
                    var tst = data.split('\r\n');
                    var tst2 = tst[0].split(',');
                    for(var obj in tst2){
                        tblHeaders.push(tst2[obj]);
                    }

                    for(var obj=1;obj<tst.length;obj++){
                        var tst3 = tst[obj].split(',');
                        for(var obj3 in tst3){
                            tbldata.push(tst3[obj3]);
                        }
                    }
                });
            }
            else{
                fs.readFile(variables.testOrganizationImportFilePath, 'utf8', function(err, contents) {
                    var data = contents;
                    var tst = data.split('\r\n');
                    var tst2 = tst[0].split(',');
                    for(var obj in tst2){
                        tblHeaders.push(tst2[obj]);
                    }

                    for(var obj=1;obj<tst.length;obj++){
                        var tst3 = tst[obj].split(',');
                        for(var obj3 in tst3){
                            tbldata.push(tst3[obj3]);
                        }
                    }
                });
            }

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.startNewImportBtn);
            browser.pause(2000);

            //SELECT OBJECT TYPE
            browser.expect.element(elements.bulkImportElements.modalHeader).to.be.visible;
            commands.checkAndPerform('click', browser,elements.bulkImportElements.objectTypesDropdown);
            if(type === 'Contacts')
                commands.checkAndPerform('click', browser, "//option[@value='Contact']");
            else
                commands.checkAndPerform('click', browser, "//option[@value='Organization']");
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.importNextBtn);
            browser.pause(1000);

            //UPLOAD FILE
            browser.expect.element(elements.bulkImportElements.uploadFileLi).to.have.attribute('class').which.equals('active');
            if(type === 'Contacts')
                commands.checkAndSetValue(browser,elements.bulkImportElements.fileUploadInput, variables.testContactsImportFilePath);
            else
                commands.checkAndSetValue(browser,elements.bulkImportElements.fileUploadInput, variables.testOrganizationImportFilePath);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.uploadBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.importNextBtn);
            browser.pause(2000);

            //SELECT DELIMETERS
            browser.expect.element(elements.bulkImportElements.selectDelimetersLi).to.have.attribute('class').which.equals('active');
            browser.elements("xpath", elements.bulkImportElements.previewTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        console.log('header : '+text.value);
                        expect(tblHeaders.indexOf(text.value)!=-1).to.be.true;
                    });
                });
            });
            browser.elements("xpath", elements.bulkImportElements.previewTable+"/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        var g = text.value.split('\\s+');
                        var vals = g[0].split(' ');
                        for(var obj in vals){
                            var tst = vals[obj].split(',');
                            console.log(tst);
                            for(var obj2 in tst){
                                expect(tbldata.indexOf(tst[obj2])!=-1).to.be.true;
                            }
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.importNextBtn);

            //SELECT MAPPING
            commands.checkAndPerform('click', browser,elements.bulkImportElements.mappingDropdown);
            commands.checkAndPerform('click', browser, "//option[contains(.,'Create New Mapping')]");
            commands.checkAndPerform('click', browser,elements.bulkImportElements.importNextBtn);
            browser.pause(10000);

            //CREATE MAPPING
            browser.expect.element(elements.bulkImportElements.mappingPageHeader).to.be.present;
            browser.expect.element(elements.bulkImportElements.mappingPageHeader).text.to.equal('Bulk Import Mapping');
            if(type === 'Contacts')
                commands.checkAndSetValue(browser,elements.bulkImportElements.mappingNameInput, variables.newConMappingName);
            else
                commands.checkAndSetValue(browser,elements.bulkImportElements.mappingNameInput, variables.newOrgMappingName);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.saveModeDropdown);
            commands.checkAndPerform('click', browser, "//option[@title='Insert Only']");

            //MAP COLUMNS
            browser.elements("xpath", elements.bulkImportElements.sourceUl+"/li", function (result) {
                var els = result.value;
                var i = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        i = i + 1;
                        console.log('=====> Mapping column named : '+text.value);
                        var colNameToMap = '';

                        browser.getAttribute(elements.bulkImportElements.sourceUl+"/li["+i+"]/div",'data-path',function (re) {
                           colNameToMap = re.value;

                            commands.checkAndPerform('click', browser,elements.bulkImportElements.sourceUl+"/li["+i+"]");
                            browser.pause(1000);

                            //CHECK IF MAPPING FOR THIS COLUMN ALREADY EXISTS
                            browser.elements("xpath", elements.bulkImportElements.mapsDiv+"/div", function (result) {
                                var els = result.value;
                                if(els.length > 0){
                                    console.log(text.value+' already have mapping, recreating it ... ');
                                    browser.elements("xpath", elements.bulkImportElements.mapsDiv+"/div", function (result) {
                                        var els = result.value;
                                        var count = 0;
                                        var removeDiv = false;
                                        els.forEach(function (el) {
                                                browser.elementIdText(el.ELEMENT, function (txt) {
                                                    count = count + 1;
                                                    browser.pause(2000);
                                                    if (txt.status == 0) {
                                                        if (!txt.value.includes('Field Behavior')) {
                                                            removeDiv = true;
                                                            commands.checkAndPerform('click', browser, elements.bulkImportElements.mapsDiv + "/div[" + count + "]/ul/li[1]/span");
                                                            browser.pause(2000);
                                                        }
                                                    }
                                                });
                                        });
                                    });
                                    //ADD NEW DIVISION
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.sourceUl+"/li["+i+"]");
                                    browser.pause(2000);
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.addMappingPlusBtn);
                                    browser.pause(2000);
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.sourceDropdownExpandBtn);
                                    browser.pause(2000);
                                    commands.checkAndPerform('click', browser,"//a[contains(.,'"+colNameToMap+"')]");
                                    browser.pause(2000);
                                    colNameToMap = colNameToMap+' *';
                                    commands.checkAndPerform('click', browser, elements.bulkImportElements.targetDropdown);
                                    browser.pause(2000);
                                    browser.elements("xpath", elements.bulkImportElements.targetDropdown+"/option", function (result) {
                                        var els = result.value;
                                        var count = 0;
                                        els.forEach(function (el) {
                                            browser.elementIdText(el.ELEMENT, function (te) {
                                                count = count + 1;
                                                if(te.value === colNameToMap)
                                                {
                                                    console.log('mapping  : '+te.value+' and '+colNameToMap);
                                                    commands.checkAndPerform('click', browser, elements.bulkImportElements.targetDropdown+"/option["+count+"]");
                                                    browser.pause(2000);
                                                    browser.expect.element(elements.bulkImportElements.sourceUl+"/li["+i+"]/div/span").to.have.attribute('class').to.equal('glyphicon glyphicon-ok');
                                                }
                                            });
                                        });
                                    });
                                }else{
                                    console.log(text.value+' doesnt have mapping, creating one ... ');

                                    //ADD NEW DIVISION
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.sourceUl+"/li["+i+"]");
                                    browser.pause(1000);
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.addMappingPlusBtn);
                                    browser.pause(1000);
                                    commands.checkAndPerform('click', browser,elements.bulkImportElements.sourceDropdownExpandBtn);
                                    browser.pause(1000);
                                    commands.checkAndPerform('click', browser,"//a[contains(.,'"+colNameToMap+"')]");
                                    browser.pause(2000);
                                    colNameToMap = colNameToMap+' *';
                                    commands.checkAndPerform('click', browser, elements.bulkImportElements.targetDropdown);
                                    browser.pause(2000);
                                    browser.elements("xpath", elements.bulkImportElements.targetDropdown+"/option", function (result) {
                                        var els = result.value;
                                        var count = 0;
                                        els.forEach(function (el) {
                                            browser.elementIdText(el.ELEMENT, function (te) {
                                                count = count + 1;
                                                if(te.value === colNameToMap)
                                                {
                                                    console.log('mapping  : '+te.value+' and '+colNameToMap);
                                                    commands.checkAndPerform('click', browser, elements.bulkImportElements.targetDropdown+"/option["+count+"]");
                                                    browser.pause(2000);
                                                    browser.expect.element(elements.bulkImportElements.sourceUl+"/li["+i+"]/div/span").to.have.attribute('class').to.equal('glyphicon glyphicon-ok');
                                                }
                                            });
                                        });
                                    });
                                }
                            });
                        });
                        browser.pause(2000);
                    });
                });
            });
            commands.checkAndPerform('click', browser, elements.bulkImportElements.saveMappingBtn);
            browser.pause(3000);
            commands.checkAndPerform('click', browser,elements.bulkImportElements.createJobBtn);
            browser.pause(3000);
            browser.elements("xpath", elements.bulkImportElements.bulkImportJobsTable, function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(type === 'Contacts')
                            expect(text.value.includes(variables.newConMappingName)).to.be.true;
                        else
                            expect(text.value.includes(variables.newOrgMappingName)).to.be.true;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.bulk_Import = bulk_Import;


var import_Contacts_And_Organizations = function () {
    describe("BULK IMPORT CONTACTS AND ORGANIZATIONS", function () {
        navigate.navigate_To_BulkImport_Tab();
        me.bulk_Import('Contacts');
        me.bulk_Import('Organization');
    });
};
exports.import_Contacts_And_Organizations = import_Contacts_And_Organizations;

var bulkImport_Sheet = function () {
    describe("BULK IMPORT SHEET EXECUTION", function () {
        me.import_Contacts_And_Organizations();
    });
};
exports.bulkImport_Sheet = bulkImport_Sheet;