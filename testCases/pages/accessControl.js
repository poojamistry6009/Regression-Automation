/**
 * Created by INDUSA
 */
require('mocha');
var expect = require('chai').expect;
var moment = require('moment');
var me = require('./accessControl.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var userAuthentication = require('./userAuthentication.js');
var orgOperations = require('./organization.js');
var contactOperations = require('./contacts.js');
var adminOperations = require('./admin.js');

//TEST CASE DECLARATION - START

var navigate_To_New_ControlTree = function () {
    describe("navigate to new control tree", function () {
        it("should navigate to new control tree", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerMenu);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.hrmAccessControlLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.controlTreeNameCreated+"')]");
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_ControlTree = navigate_To_New_ControlTree;


var add_Org_To_ControlTree = function () {
    describe("Add organization to control node", function () {
        it("should add organization in custom list of control node", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.controlTreeOrgTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.adminPageElements.controlTreeAddOrgInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

            browser.elements("xpath", elements.adminPageElements.controlTreeOrgTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdORG);
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.add_Org_To_ControlTree = add_Org_To_ControlTree;

var clone_Control_Tree = function () {
    describe("Clone a control tree", function () {
        it("should clone a control tree", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.adminPageElements.hrmAccessControlLink);
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.controlTreeTable+"/tbody/tr", function (result) {
                var els = result.value;
                var treeCounter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        treeCounter = treeCounter + 1;
                        console.log('>>>>>>>>>>'+text.value);
                            if(text.value.includes(variables.controlTreeNameCreated)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+treeCounter+']/td[6]/span[2]/a'); //xpath of clone tree link
                            browser.pause(2000);
                            variables.createdClonedTree = variables.cloneControlTreeName;
                            commands.checkAndSetValue(browser,elements.adminPageElements.controlTreeCloneNameInput, variables.cloneControlTreeName);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.adminPageElements.controlTreeCloneBtn);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser,elements.adminHRMAccessControlPageElements.controlTreeSaveBtn);
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdClonedTree+"')]").to.be.present;

                        }
                    });
                });
                browser.pause(5000);
            });
            browser.pause(2000);
        });
    });
};
exports.clone_Control_Tree = clone_Control_Tree;

var delete_Control_Tree = function () {
    describe("Delete a control tree", function () {
        it("should delete a control tree", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.adminPageElements.controlTreeTable+"/tbody/tr", function (result) {
                var els = result.value;
                var treeCounter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        treeCounter = treeCounter + 1;
                        if(text.value.includes(variables.createdClonedTree)){
                            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+treeCounter+']/td[7]/i');
                            browser.pause(2000);
                            browser.keys(browser.Keys.ENTER);
                            browser.pause(2000);
                            browser.expect.element("//a[contains(@title,'"+variables.createdClonedTree+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.delete_Control_Tree = delete_Control_Tree;

var verify_Secure_Messages_For_Contacts = function () {
    describe("Secure messages in collaboration center", function () {
        it("should secure messages associated with contacts display in collaboration center", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput, variables.createdContactFname);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput, variables.username);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageSubject);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            commands.readValuesAndAssert(browser,elements.collaborationPageElements.messagesRecordsTable,variables.newMessageSubject,true);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.newMessageSubject+'")]');
            browser.pause(3000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageCloseBtn);
        });
    });
};
exports.verify_Secure_Messages_For_Contacts = verify_Secure_Messages_For_Contacts;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END





//FUNCTIONS DECLARATION - START
var accessControl_Basic = function () {
    describe("ACCESS CONTROL - BASIC", function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_New_ControlTree();
        me.add_Org_To_ControlTree();            // Create an Org and make sure it can be associated to a Node
        me.clone_Control_Tree();                // Clone a Tree
        me.delete_Control_Tree();               // Delete a Node
    });
};
exports.accessControl_Basic = accessControl_Basic;

var accessControl_CollaborationUser = function () {
    describe("ACCESS CONTROL - COLLABORATION USER", function () {
        me.pauseBrowserSomeTime();
        me.verify_Secure_Messages_For_Contacts();            // Verify that Secure Message displays on Collaboration Center Messages
    });
};
exports.accessControl_CollaborationUser = accessControl_CollaborationUser;

var accessControl_Sheet = function () {
    describe("RESOLVE ACCESS CONTROL TEST CASES DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'controlTreeNameCreated',
            'createdORG',
            'createdContact',
        ]);
        orgOperations.create_New_Org();
        adminOperations.create_New_Control_Tree();
        contactOperations.create_Contact('contact');
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //SHEET EXECUTION - STARTS
        me.accessControl_Basic();
        me.accessControl_CollaborationUser();
        //SHEET EXECUTION - ENDS

    });
};
exports.accessControl_Sheet = accessControl_Sheet;
//FUNCTIONS DECLARATION - END





