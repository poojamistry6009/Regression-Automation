/**
 * Created by INDUSA
 */
require('mocha');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');

var send_Message_With_Org = function () {
    describe("Sends a message to user with Organization",function () {
        it("should check - should send a message to user with Organization",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput,variables.firstname.charAt(0).toUpperCase() + variables.firstname.slice(1));
            browser.pause(3000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.sentOrgMessageSubject = variables.newMessageSubject;
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput,variables.newMessageSubject);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput,variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput,variables.createdORG);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdORG)){
                            console.log('=====> Related item Organization found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(5000);
            browser.expect.element("//a[@title='"+variables.sentOrgMessageSubject+"']").to.be.present;
            browser.pause(2000);
        });
    });
}
module.exports.send_Message_With_Org = send_Message_With_Org;

var send_Message_With_Opportunity = function () {
    describe("Sends a message to user with Opportunity",function () {
        it("should check - should send a message to user with Opportunity",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.collaborationTabLink);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.messageSubTab);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageLink);
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageToInput,variables.firstname.charAt(0).toUpperCase() + variables.firstname.slice(1));
            browser.pause(3000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageSubjectInput,variables.newMessageSubject);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageMessageInput,variables.newMessage);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.collaborationPageElements.newMessageRelatedItemInput,variables.createdOpportunity);
            browser.pause(2000);
            browser.expect.element(elements.collaborationPageElements.relatedItemsSearchResultsDiv).to.be.visible;
            browser.elements("xpath", elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        if(text.value.includes(variables.createdOpportunity)){
                            console.log('=====> Related item Opportunity found in search results : '+text.value);
                            commands.checkAndPerform('click', browser, elements.collaborationPageElements.relatedItemsSearchResultsDiv+"/li["+count+"]");
                        }
                    });
                });
            });
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(5000);
            commands.readValuesAndAssert(browser,elements.collaborationPageElements.messagesRecordsTable,variables.newMessageSubject,true);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.newMessageSubject+'")]');
            browser.pause(3000);
            commands.checkAndPerform('click',browser,elements.collaborationPageElements.newMessageCloseBtn);
        });
    });
}
module.exports.send_Message_With_Opportunity = send_Message_With_Opportunity;