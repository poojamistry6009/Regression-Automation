/**
 * Created by INDUSA
 */
require('mocha');
var moment = require('moment');
var expect = require('chai').expect;
var me = require('./opportunity.js');
var adminOperations = require('./admin.js');
var orgOperations = require('./organization.js');
var messageOperations = require('./message.js');
var contactOperations = require('./contacts.js');
var navigateOperations = require('../utility/navigate.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var navigate = require('../utility/navigate.js');
var commonOperations = require('./commons.js');


//TEST CASE DECLARATION - START

var create_Opportunity = function (ccUser2) {
    describe("Create a new opportunity", function () {
        it("should create a new opportunity", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityCreateLink);
            browser.pause(2000);
            // Create new Opportunity brings up the new Opportunity page
            browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).to.be.present;
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrgInput,variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesTerritoryDropdown).to.have.value.not.equals("");
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesRepDropdown).to.have.value.not.equals("");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPopupApplyBtn);
            browser.pause(2000);
            variables.createdOpportunity = variables.opportunityName;
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNameInputField,variables.opportunityName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityStageDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCloseDateInput, variables.nextDay);
            browser.pause(2000);

            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySpecialtyDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            if(ccUser2)
            {
                browser.pause(2000);
                commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityCCUsersInput);
                commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityCCUsersInput, variables.username1);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
            }
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveAndBackBtn);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.opportunityPageElements.opportunitiesTable).to.be.present;
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdOpportunity)).to.be.true;
                        variables.dependencyFulfilled.push('createdOpportunity');
                    });
                });
            });
            //assert and push value to dependency fulfilled list
        });
    });
};
exports.create_Opportunity = create_Opportunity;

var check_Opportunity_Tab_Highlighted = function () {
    describe("Opportunities tab highlighted", function () {
        it("should check opportunities tab is highlighted", function (browser) {
            browser.pause(2000);
            browser.assert.attributeContains(elements.opportunityPageElements.opportunitiesTabLink, 'class', 'selected-tab');
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Tab_Highlighted = check_Opportunity_Tab_Highlighted;

var check_Opportunity_Table = function () {
    describe("Opportunities table", function () {
        it("should check opportunities table is present or not", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunitiesTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Table = check_Opportunity_Table;

var navigate_To_New_Opportunity = function (editMode) {
    describe("Navigate to new opportunity", function () {
        it("should navigate to new opportunity", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//option[contains(.,"All")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdOpportunity+'")]');
            browser.pause(2000);
            if(editMode === true){
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditBtn);
                browser.pause(2000);
            }
        });
    });
};
exports.navigate_To_New_Opportunity = navigate_To_New_Opportunity;

var check_Column_Sortable = function () {
    describe("Columns are sortable or not", function () {
        it("should check - columns are sortable or not", function (browser) {
            browser.pause(2000);
            var counter = 0;
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        //find 'Name' column and its index
                        if(text.value === 'Name'){
                            browser.pause(2000);

                            //check ascending order
                            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th["+counter+"]");
                            browser.pause(1000);
                            browser.expect.element(elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th["+counter+"]").to.have.attribute('class').which.contains('sort-ascending');

                            browser.pause(2000);

                            //check descending order
                            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th["+counter+"]");
                            browser.pause(1000);
                            browser.expect.element(elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th["+counter+"]").to.have.attribute('class').which.contains('sort-descending');

                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Column_Sortable = check_Column_Sortable;

var check_Opportunity_Deactivation = function () {
    describe("Deactivate opportunity", function () {
        it("should check - user can deactivate opportunity", function (browser) {
        });
    });
};
exports.check_Opportunity_Deactivation = check_Opportunity_Deactivation;

var check_CustomList_Order = function () {
    describe("Order of custom lists", function () {
        it("should check - can change order of custom lists", function (browser) {
            browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySettingsIcon);
            // browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrderCustomListLink);
            browser.pause(1000);

            //select one column and try to change the order of it
            var counter = 0;
            var orderChangedColName = '';
            browser.elements("xpath",elements.opportunityPageElements.opportunityOrderCustomListDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT,function(text) {
                        counter = counter + 1;
                        if(counter === 1){
                            browser.pause(1000);
                            browser.getText(elements.opportunityPageElements.opportunityOrderCustomListDiv+"/li["+counter+"]",function (result) {
                               orderChangedColName = result.value;
                            });
                            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrderCustomListDiv+"/li["+counter+"]");
                            browser.pause(1000);
                            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrderCustomListDownBtn);
                            browser.pause(1000);
                        }
                    });
                });
            });

            //Read order list again and verify if order of the column has changed or not
            browser.elements("xpath",elements.opportunityPageElements.opportunityOrderCustomListDiv, function (result) {
                var els = result.value;
                var counter2 = 0;
                var changedIndexColName = '';
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT,function(text) {
                        counter2 = counter2 + 1;
                        if(counter2 === counter){ //get the name of the column at the index which we have changed above
                            browser.pause(1000);
                            browser.getText(elements.opportunityPageElements.opportunityOrderCustomListDiv+"/li["+counter2+"]",function (result) {
                                changedIndexColName = result.value;
                                expect(changedIndexColName).to.not.equal(orderChangedColName);
                            });
                        }
                    });
                });
            });
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrderCustomListCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_CustomList_Order = check_CustomList_Order;

var check_Opportunity_QuickFilter = function () {
    describe("Opportunity quick filter", function () {
        it("should check - opportunities quick filter functionality", function (browser) {
            browser.pause(2000);
            // Selecting Quick filter expands filter menu
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityQuickFilterLink);
            browser.expect.element(elements.opportunityPageElements.opportunityFilterApplyBtn).to.be.visible;

            // Filter by field has drop down menu
            browser.expect.element(elements.opportunityPageElements.opportunityFilterByDropdown).to.be.present;
            browser.expect.element(elements.opportunityPageElements.opportunityFilterByDropdown).to.be.a('select');

            // Operator field has drop down menu
            browser.expect.element(elements.opportunityPageElements.opportunityFilterOperatorDropdown).to.be.present;
            browser.expect.element(elements.opportunityPageElements.opportunityFilterOperatorDropdown).to.be.a('select');

            // Value has drop down menu and date selection tool
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityFilterByDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);
            browser.expect.element(elements.opportunityPageElements.opportunityValueDropdown).to.be.present;
            browser.expect.element(elements.opportunityPageElements.opportunityValueDateInput).to.be.present;

            // If sorting by date static/relative dates work
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityValueDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);
            browser.expect.element(elements.opportunityPageElements.opportunityValueRelativeDatePlusBtn).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityValueRelativeDateMinusBtn).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityValueRelativeDaysInput).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityValueRelativeDatePeriodDropdown).to.be.visible;

            // Selected Columns has all the columns  currently shown in table
            var filterSelectedCols = '';
            browser.elements("xpath",elements.opportunityPageElements.opportunityFilterSelectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        filterSelectedCols = filterSelectedCols.concat(text.value);
                    });
                });
            });
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(text.value != ''){
                            expect(filterSelectedCols.includes(text.value)).to.be.true;
                        }
                    });
                });
            });
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityFilterApplyBtn);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityQuickFilterLink);
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_QuickFilter = check_Opportunity_QuickFilter;

var check_Create_Popup_Highlighted_Until_Filled_Up = function () {
    describe("Create opportunity pop up", function () {
        it("should check - create opportunity pop up is highlighted unless required fields are entered", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityCreateLink);
            browser.pause(1000);
            browser.expect.element(elements.opportunityPageElements.opportunityChangeOrgPopup).to.be.visible;
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityChangeOrgPopupCloseBtn);
            browser.pause(2000);

        });
    });
};
exports.check_Create_Popup_Highlighted_Until_Filled_Up = check_Create_Popup_Highlighted_Until_Filled_Up;

var check_Opportunity_Type_Dropdown = function () {
    describe("Opportunity type dropdown", function () {
        it("should check - opportunity type is a dropdown", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityTypeDropdown).to.be.a('select');
            browser.pause(2000);

        });
    });
};
exports.check_Opportunity_Type_Dropdown = check_Opportunity_Type_Dropdown;

var check_Opportunity_Org_Search = function () {
    describe("Organization search in opportunity", function () {
        it("should check - organization search in opportunity works properly", function (browser) {
            browser.pause(2000);
            // Organization brings up search bar for organizations
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrgEditBtn);
            browser.expect.element(elements.opportunityPageElements.opportunityChangeOrgPopup).to.be.visible;

            // Search bar only shows close results that the user has access to
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrgInput, variables.createdORG);
            browser.expect.element(elements.opportunityPageElements.opportunityOrgSearchResultContainer).to.be.visible;

            // Selecting Organization forces update of sales territory/rep
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesTerritoryDropdown).to.have.value.not.equals('');
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesRepDropdown).to.have.value.not.equals('');

            // Cannot select organization without updating information(fields are required)
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityOrgInput);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPopupUpdateOrgBtn);
            browser.pause(1000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrgRequiredErrorLabel).to.be.visible;

            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityChangeOrgPopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Org_Search = check_Opportunity_Org_Search;

var check_Opportunity_Close_Date_Input = function () {
    describe("Close date input", function () {
        it("should check - close input pop ups for date selection", function (browser) {
            browser.pause(2000);
            // Expected close date brings up date selector
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityCloseDateDiv);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityCloseDatePicker).to.be.visible;

            // Can select dates even ones in the past
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCloseDateInput,variables.nextDay);
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Close_Date_Input = check_Opportunity_Close_Date_Input;

var check_Opportunity_Stage_Dropdown = function () {
    describe("Stage dropdown", function () {
        it("should check - Stage has functional drop down menu", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityStageDropdown).to.be.a('select');
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Stage_Dropdown = check_Opportunity_Stage_Dropdown;

var check_Opportunity_Specialty_Dropdown = function () {
    describe("Specialty drop down menu", function () {
        it("should check - Specialty imports Organizations specialties in functional drop down menu", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunitySpecialtyDropdown).to.be.a('select');
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Specialty_Dropdown = check_Opportunity_Specialty_Dropdown;

var check_Opportunity_Required_Fields = function () {
    describe("Opportunity required fields", function () {
        it("should check - required field labels are shown up", function (browser) {
            browser.pause(2000);

            // Required field error messages show up correctly
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySavePopUpNoOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityCreateLink);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityChangeOrgPopupCloseBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityNameErrorLabel).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityOrgErrorLabel).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityStageErrorLabel).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityProbabilityErrorLabel).to.be.visible;
            browser.expect.element(elements.opportunityPageElements.opportunityCloseDateErrorLabel).to.be.visible;
            browser.pause(1000);
            browser.keys(browser.Keys.ESCAPE);

            // Error given if sales rep is not in sales territory
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Required_Fields = check_Opportunity_Required_Fields;

var change_Existing_Opportunity = function () {
    describe("Modify existing opportunity", function () {
        it("should check - modifiy existing opportunity and check the elements proper working or not", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditBtn);
            browser.pause(2000);

            // Can change organization
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrgEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityOrgInput);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrgInput, variables.createdORG);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPopupApplyBtn);
            // Changing the sales territory deletes the old sales rep
            // Changing the sales rep does not delete the sales territory

            // Stage has a drop down menu that changes probability with each stage
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityStageDropdown);
            browser.pause(1000);
            browser.expect.element("//option[contains(.,'"+variables.createdOpporStage+"')]").to.be.present;
            commands.checkAndPerform('click', browser, "//option[contains(.,'"+variables.createdOpporStage+"')]");
            browser.pause(1000);

            // Probabilities are changeable according to Admin permissions(relationship management)
            browser.pause(2000);
            browser.elements("xpath",elements.opportunityPageElements.opportunityStageDropdown+"/option", function (result) {
                var els = result.value;
                var stageCounter = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        stageCounter = stageCounter + 1;
                        if(text.value === variables.createdOpporStage){
                            console.log('clicking...'+elements.opportunityPageElements.opportunityStageDropdown+"/option["+stageCounter+"]");
                            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityStageDropdown+"/option["+stageCounter+"]");
                        }
                    });
                });
            });
            browser.pause(2000);

            // Can cc and search for users
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCCUsersInput, variables.username);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityCCUsersSearchResults).to.have.css('display').which.equals('block');
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);

            //Users will get a message when information is updated(dependent on notification rules)

            // Can change close date
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityCloseDateInput);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCloseDateInput, variables.nextDay);
            browser.pause(1000);

            // Can tag providers/contacts to the opportunity
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveBtn);
            browser.pause(2000);
        });
    });
};
exports.change_Existing_Opportunity = change_Existing_Opportunity;

var check_Switch_Tabs = function () {
    describe("Can switch between panels (orders revenue and panel mix)", function () {
        it("should check - Can switch between panels (orders revenue and panel mix)", function (browser) {
            browser.pause(2000);

            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrdersSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityOrdersSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityRevenueSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityRevenueSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPanelMixSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityPanelMixSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_Switch_Tabs = check_Switch_Tabs;

var check_Lifecycle_Order = function () {
    describe(" Total lifecycle in has a dropdown menu of years quarters and months", function () {
        it("should check -  Total lifecycle in has a dropdown menu of years quarters and months", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrdersSubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrderLifeCycleDropdown).to.be.a('select');
            browser.expect.element('//option[@value="Months"]').to.be.present;
            browser.expect.element('//option[@value="Quarters"]').to.be.present;
            browser.expect.element('//option[@value="Years"]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Lifecycle_Order = check_Lifecycle_Order;

var check_Estimated_Orders = function () {
    describe("Estimated orders per has years quarters months weeks and day options", function () {
        it("should check - Estimated orders per has years quarters months weeks and day options", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrdersEstimatedOrderDropdown).to.be.a('select');
            browser.expect.element('//option[@value="Day"]').to.be.present;
            browser.expect.element('//option[@value="Week"]').to.be.present;
            browser.expect.element('//option[@value="Month"]').to.be.present;
            browser.expect.element('//option[@value="Quarter"]').to.be.present;
            browser.expect.element('//option[@value="Year"]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Estimated_Orders = check_Estimated_Orders;

var check_Average_Revenue = function () {
    describe("Average revenue per order is populated accurately from specialty(defined in admin)", function () {
        it("should check - Average revenue per order is populated accurately from specialty(defined in admin)", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrderPerOrderCalculation).to.have.value.that.equals(variables.defaultSpecialtyRevenuePerOrder);
            browser.pause(2000);
        });
    });
};
exports.check_Average_Revenue = check_Average_Revenue;

var     check_Days_In_Week = function () {
    describe("days in a work has a drop down menu", function () {
        it("should check - days in a work has a drop down menu", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrderDaysInWork).to.be.a('select');
            browser.pause(2000);
        });
    });
};
exports.check_Days_In_Week = check_Days_In_Week;

var check_Estimated_Revenues_Calculated = function () {
    describe("estimated revenues are accurate", function () {
        it("should check - estimated revenues are accurate", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityOrderLifeCycleInputBox);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrderLifeCycleInputBox,variables.totalLifecycleMonths);
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityOrdersEstimatedOrderInput);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrdersEstimatedOrderInput, variables.estimatedOrdersDay);
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityOrderDaysInWorkInput);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityOrderDaysInWorkInput, variables.daysInWork);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityOrderApplyBtn);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityEstimatedMonthlyRevenueDiv).text.to.not.equal('0.00');

            browser.pause(2000);
        });
    });
};
exports.check_Estimated_Revenues_Calculated = check_Estimated_Revenues_Calculated;

var check_Lifecycle_Revenue = function () {
    describe("LifeCycle drop down menu works correclty (Quarters Months and Years)", function () {
        it("should check - LifeCycle drop down menu works correclty (Quarters Months and Years)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityRevenueSubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityRevenueLifecycleDropdown).to.be.a('select');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityRevenueLifecycleDropdown);
            browser.expect.element('//option[@value="Months"]').to.be.present;
            browser.expect.element('//option[@value="Quarters"]').to.be.present;
            browser.expect.element('//option[@value="Years"]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Lifecycle_Revenue = check_Lifecycle_Revenue;

var check_Total_Estimated_Revenue = function () {
    describe("total revenue estimate comes up correctly and is properly weighted", function () {
        it("should check - total revenue estimate comes up correctly and is properly weighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityTotalEstimatedRevenueDiv).text.to.not.equal('0.00');
            browser.expect.element(elements.opportunityPageElements.opportunityTotalWeightedRevenueDiv).text.to.not.equal('0.00');
            browser.pause(2000);
        });
    });
};
exports.check_Total_Estimated_Revenue = check_Total_Estimated_Revenue;

var check_Search_Panel_Mix = function () {
    describe("can search for and add panels", function () {
        it("should check - can search for and add panels", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityAddPanelInput, variables.createdPanel);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPanelMixApplyBtn);
            browser.pause(2000);
            browser.elements("xpath", elements.opportunityPageElements.opportunityPanelMixPanelsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdPanel);
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
        });
    });
};
exports.check_Search_Panel_Mix = check_Search_Panel_Mix;

var check_LifeCycle_Panel = function () {
    describe("drop down menus for lifecycle in and volume by", function () {
        it("should check - drop down menus for lifecycle in and volume by", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPanelMixSubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityPanelMixLifeCycleDropdown).to.be.a('select');
            browser.pause(2000);
            browser.expect.element('//option[@value="Months"]').to.be.present;
            browser.expect.element('//option[@value="Quarters"]').to.be.present;
            browser.expect.element('//option[@value="Years"]').to.be.present;

            browser.expect.element(elements.opportunityPageElements.opportunityPanelMixVolumePeriodDropdown).to.be.a('select');
            browser.expect.element('//option[@value="Month"]').to.be.present;
            browser.expect.element('//option[@value="Quarter"]').to.be.present;
            browser.expect.element('//option[@value="Year"]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_LifeCycle_Panel = check_LifeCycle_Panel;

var check_Search_FeeSchedule = function () {
    describe("Can search for and change fee schedules", function () {
        it("should check - Can search for and change fee schedules", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityPanelMixSubTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityPanelMixFeeScheduleSearch,variables.createdFeeSchedule);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.expect.element(elements.opportunityPageElements.opportunityPanelMixFeeScheduleSearch).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Search_FeeSchedule = check_Search_FeeSchedule;

var check_Total_Estimated_Revenue_Panel = function () {
    describe("total revenue estimate comes up correctly and is properly weighted", function () {
        it("should check - total revenue estimate comes up correctly and is properly weighted", function (browser) {
            browser.pause(2000);

            browser.pause(2000);
        });
    });
};
exports.check_Total_Estimated_Revenue_Panel = check_Total_Estimated_Revenue_Panel;

var check_Details_Highlighted = function () {
    describe("Details tab is highlighted", function () {
        it("should check - Details tab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsSubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityDetailsSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_Details_Highlighted = check_Details_Highlighted;

var check_Switch_Between_Tabs = function () {
    describe("Can switch back and forth between details and general information", function () {
        it("should check - Can switch back and forth between details and general information", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityGeneralSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityGeneralSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityDetailsSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_Switch_Between_Tabs = check_Switch_Between_Tabs;

var check_Select_EMR = function () {
    describe("can select emr type defined in Admin", function () {
        it("should check - can select emr type defined in Admin", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsEMRDiv);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityDetailsEMRInput, variables.createdEMR);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
        });
    });
};
exports.check_Select_EMR = check_Select_EMR;

var check_Toggle_Orders_And_Results = function () {
    describe("Can toggle Orders and Results", function () {
        it("should check - Can toggle Orders and Results", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsOrderInterfaceCheckbox);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsResultInterfaceCheckbox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsOrderInterfaceCheckbox);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsResultInterfaceCheckbox);
            browser.pause(2000);
        });
    });
};
exports.check_Toggle_Orders_And_Results = check_Toggle_Orders_And_Results;

var check_Select_Competitive_Lab = function () {
    describe("Can select one competitive lab from drop down menu", function () {
        it("should check - Can select one competitive lab from drop down menu", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsCurrentLabDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.expect.element(elements.opportunityPageElements.opportunityDetailsCurrentLabDropdown).to.have.value.that.not.equals('Select Current Lab');
            browser.expect.element(elements.opportunityPageElements.opportunityDetailsCurrentLabDropdown).to.have.value.that.not.equals('');
            browser.pause(2000);
        });
    });
};
exports.check_Select_Competitive_Lab = check_Select_Competitive_Lab;

var check_Can_Add_Notes = function () {
    describe("Can add notes", function () {
        it("should check - Can add notes", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.opportunityPageElements.opportunityDetailsDescriptionInput);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityDetailsDescriptionInput,variables.opporDetailsDesc);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityDetailsSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Can_Add_Notes = check_Can_Add_Notes;

var check_Activities_Tab_Highlighted = function () {
    describe("Opportunities tab highlighted", function () {
        it("should check opportunities tab is highlighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityActivitySubTab).to.have.attribute('class').which.contain('tab btn btn-default selected');
            browser.pause(2000);
        });
    });
};
exports.check_Activities_Tab_Highlighted = check_Activities_Tab_Highlighted;

var create_Task_Activity_From_Opportunity = function () {
    describe("Can create a new task from here", function () {
        it("Should create  - Can create a new task from here", function (browser) {
            browser.pause(2000);
            browser.execute('scrollTo(30,70)');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityTaskActivityTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.taskSubject);
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
        });
    });

};
exports.create_Task_Activity_From_Opportunity = create_Task_Activity_From_Opportunity;

var create_Memo_Activity_From_Opportunity = function () {
    describe("Can create a new memo from here", function () {
        it("Should create  - Can create a new memo from here", function (browser) {
            browser.pause(2000);
            browser.execute('scrollTo(30,90)');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityMemoActivityTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.memoSubject);
            commands.checkAndPerform('click', browser, elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
        });
    });

};
exports.create_Memo_Activity_From_Opportunity = create_Memo_Activity_From_Opportunity;

var created_Activity_showUp_inTable = function () {
    describe("Created activities will show up in the table after saving", function () {
        it("Should check - Created activities will show up in the table after saving", function (browser) {

            browser.elements("xpath", elements.opportunityPageElements.opportunityActivityCustomListDropdown + "/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.createdTask);
                    });
                });
            });

        });
    });
};
exports.created_Activity_showUp_inTable = created_Activity_showUp_inTable;

var filter_Activity_FromQuick_Filter = function () {
    describe("Can filter activities with quick filter", function () {
        it("Should check - Can filter activities with quick filter", function (browser) {
            browser.pause(2000);
            browser.getLocationInView(elements.opportunityPageElements.opportunityActivityQuickFilterLink, function(result) {
                browser.execute('scrollTo('+result.value.x+','+result.value.y+')');
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityActivityQuickFilterLink);
                browser.pause(2000);
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityActivityFilterOption);
                browser.pause(1000);
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityActivityFilterByStatusOption);
                browser.pause(2000);
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityActivityFilterValueDropdown);
                browser.pause(1000);
                commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivityFilterValueOpenOption);
                browser.pause(2000);
                commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityActivityFilterApplyBtn);
            });
            browser.pause(3000);
            browser.elements("xpath", elements.opportunityPageElements.opportunityActivityTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain('Open');
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.filter_Activity_FromQuick_Filter = filter_Activity_FromQuick_Filter;

var navigate_to_attachments_and_messages = function () {
    describe("Can switch to attatchments and messages", function () {
        it("Should check - Can switch to attatchments and messages", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentsSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityAttachmentsSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityMessageSubTab);
            browser.expect.element(elements.opportunityPageElements.opportunityMessageSubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.navigate_to_attachments_and_messages = navigate_to_attachments_and_messages;

var navigate_to_oppor_activityTab = function () {
    describe("Navigate to opportunity activity subtab", function () {
        it("Should navigate to opportunity activity subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivitySubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityActivitySubTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.navigate_to_oppor_activityTab = navigate_to_oppor_activityTab;

var create_New_CustomList = function () {
    describe("New list allows user to create a new custom list", function () {
        it("Should check - New list allows user to create a new custom list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivitySettingsIcon);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivityNewCustomListLink);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityActivityNewCustomListInput, variables.opportunityNewCustomListName);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivityFilterOptionCustomList);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityActivityNewCustomListSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityActivityCustomListDropdown).text.to.contain(variables.opportunityNewCustomListName);
            browser.pause(2000);
        });
    });
};
exports.create_New_CustomList = create_New_CustomList;

var check_Sorting_Column = function () {
    describe("Can sort  columns in ascending and descending order", function () {
        it("should check - Can sort  columns in ascending and descending order", function (browser) {
            browser.pause(2000);
            var counter = 0;
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable + "/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        counter = counter + 1;
                        //find 'Name' column and its index
                        if (text.value === 'Name') {
                            browser.pause(2000);

                            //check ascending order
                            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTable + "/thead/tr/th[" + counter + "]");
                            browser.pause(1000);
                            browser.expect.element(elements.opportunityPageElements.opportunitiesTable + "/thead/tr/th[" + counter + "]").to.have.attribute('class').which.contains('sort-ascending');

                            browser.pause(2000);

                            //check descending order
                            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTable + "/thead/tr/th[" + counter + "]");
                            browser.pause(1000);
                            browser.expect.element(elements.opportunityPageElements.opportunitiesTable + "/thead/tr/th[" + counter + "]").to.have.attribute('class').which.contains('sort-descending');

                            browser.pause(2000);
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Sorting_Column = check_Sorting_Column;

var file_Attachments_And_Storing = function () {
    describe("Can attatch files to store with opportunity", function () {
        it("Should check - Can attatch files to store with opportunity", function (browser) {
            browser.pause(2000);
            browser.execute('scrollTo(10,50)');
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentsSubTab);
            // browser.execute('scrollTo(10,50)');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentsUploadLink);
            browser.pause(1000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityAttachmentNameInput, variables.opportunityAttachmentsName);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentUploadBtn);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentCloseBtn);
        });
    });
};
exports.file_Attachments_And_Storing = file_Attachments_And_Storing;

var message_Concerning_Opportunity = function () {
    describe("Messages concerning opportunity show up here", function () {
        it("Should check - Messages concerning opportunity show up here", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityMessageSubTab);
            browser.elements("xpath", elements.opportunityPageElements.opportunityMessagesTable + "/tbody/tr", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain(variables.newMessageSubject);
                    });
                });
            });
            browser.pause(1500);
        });
    });
};
exports.message_Concerning_Opportunity = message_Concerning_Opportunity;

var attachments_unAuthorizeUser_Attach_Record = function () {
    describe("if PHI is enabled users without permission cannot see the record", function () {
        it("Should check - if PHI is enabled users without permission cannot see the record", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityAttachmentsSubTab);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityAttachmentsUploadLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.attachments_unAuthorizeUser_Attach_Record = attachments_unAuthorizeUser_Attach_Record;

var check_Opportunity_Pagination = function () {
    describe("Can expand how many Opportunities to show on the page", function () {
        it("should check - Can expand how many Opportunities to show on the page", function (browser) {
            browser.pause(2000);
            browser.getText(elements.globalElements.pagingTotalRecords,function (totalRecords) {
               if(totalRecords.value > 10){
                   commands.checkAndPerform('click', browser,elements.globalElements.pagingNumberOfRecordsDropdown);
                   browser.pause(1000);
                   commands.checkAndPerform('click', browser, '//option[contains(.,"10")]');
                   browser.pause(2000);
                   browser.elements("xpath",elements.opportunityPageElements.opportunitiesTable+"/tbody/tr", function (result) {
                       var els = result.value;
                       var rowsCount = 0;
                       els.forEach(function (el) {
                           rowsCount = rowsCount + 1;
                       });
                       expect(rowsCount).to.equal(10);
                   });
               }
            });
            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Pagination = check_Opportunity_Pagination;

var add_Contact_In_Recent_Items = function () {
    describe("Add contact in recent items", function () {
        it("should add contact in opportunity recent items", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditBtn);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityRecentItemsInput, variables.createdContactFname);
            browser.pause(3000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySaveBtn);
            browser.pause(2000);
        });
    });
};
exports.add_Contact_In_Recent_Items = add_Contact_In_Recent_Items;

var check_Opportunity_Filter_CustomList = function () {
    describe("Can filter by Preset and custom lists", function () {
        it("should check - Can filter by Preset and custom lists", function (browser) {


            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitiesTabLink);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunitySettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityNewCustomListLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNewCustomListInput, variables.newfilterCustomListName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityNewCustomListFilterByDropdown);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityNewCustomListFilterByDropdown+"/option[4]");
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNewCustomListFilterByValueInput,variables.createdContact);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityNewCustomListSaveBtn);
            browser.pause(5000);

            //search table and find this contact records
            browser.elements("xpath", elements.opportunityPageElements.opportunitiesTable+"/tbody/tr/td[7]", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        console.log(text.value);
                        expect(text.value).to.contain(variables.createdContact);
                    });
                });
            });

            //delete this temporary custom list
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditCustomListLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityDeleteCustomListLink);
            browser.pause(4000);


            browser.pause(2000);
        });
    });
};
exports.check_Opportunity_Filter_CustomList = check_Opportunity_Filter_CustomList;


var check_visibility_Of_Opportunity_Object = function () {
    describe("Ensure that case object is displayed ", function () {
        it("Should check - Ensure that case object is displayed  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.activityTaskSpan).to.be.present;
            browser.expect.element(elements.activityPageElements.activityOrgName).to.be.present;
            browser.expect.element(elements.activityPageElements.categoryTaskDiv).to.be.present;
            browser.expect.element(elements.activityPageElements.subCategoryTaskDiv).to.be.present;
            browser.pause(2000);
        });
    });
}
exports.check_visibility_Of_Opportunity_Object = check_visibility_Of_Opportunity_Object;

var check_Opportunity_Search_In_Cloud_Search = function () {
    describe("Ensure that user can search for a Task in cloud search  ", function () {
        it(" Should check - Ensure that user can search for a task in cloud search ", function (browser) {
            browser.pause(4000);
            var currentOpportunityName = variables.createdOpportunity;
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentOpportunityName);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('results-Opportunity');
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityTabHeaderOnCloudSearch);
            browser.pause(2000);
            browser.pause(2000);
        });
    });
}
exports.check_Opportunity_Search_In_Cloud_Search = check_Opportunity_Search_In_Cloud_Search;

var check_Select_Display_Opportunity_In_CloudSearch = function () {
    describe("Ensure that case detail page is displayed when the user selects the case ", function () {
        it("Should check - Ensure that case detail page is displayed when the user selects the case ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityCloudSearchDataLink);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunitiesTabLink).to.have.attribute('class').which.contain('selected-tab');
            browser.pause(2000);
        });
    });
}
exports.check_Select_Display_Opportunity_In_CloudSearch = check_Select_Display_Opportunity_In_CloudSearch;

var check_Display_Opportunity_Field_Object = function () {
    describe("Ensure that opportunity number, organization name, opportunity type, stage and probability, Expected close date and expected start date is present ", function () {
        it("Should check - Ensure that opportunity number, organization name, opportunity type, stage and probability, Expected close date and expected start date is present ", function (browser) {

            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityOrgName).text.to.contain(variables.createdORG);
            browser.expect.element(elements.opportunityPageElements.opportunitiesTabLink).to.have.attribute('class').which.contain('opportunities');

            browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).to.be.present;
            browser.expect.element(elements.opportunityPageElements.opportunityType).to.be.present;
            browser.expect.element(elements.opportunityPageElements.opportunitiesTabLink).to.have.attribute('class').which.contain('opportunities');

            browser.pause(2000);
        });
    });
}
exports.check_Display_Opportunity_Field_Object = check_Display_Opportunity_Field_Object;

var check_Task_Creation_From_CloudSearch_In_opportunity = function () {
    describe("Ensure user can create Task on opportunity from cloud search  ", function () {
        it("Should check -Ensure user can create Task on opportunity from cloud search  ", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTabLink); //task icon on cloud search
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.opportunityPageElements.opportunitySearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunitySearchField, variables.createdOpportunity);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.taskIconOnCloudSearch); //task link on cs
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.taskSubject);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.saveBtnOnActivityPage);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskViewSubjectFiled).text.to.contains(variables.taskSubject);
            browser.pause(2000);
        });
    });
}
exports.check_Task_Creation_From_CloudSearch_In_opportunity = check_Task_Creation_From_CloudSearch_In_opportunity;

var check_Memo_Creation_From_CloudSearch_In_opportunity = function () {
    describe("Ensure user can create memo on opportunity from cloud search  ", function () {
        it("Should check -Ensure user can create memo on opportunity from cloud search  ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.opportunityPageElements.opportunitySearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunitySearchField, variables.opportunityUpdateName);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.memoIconOnCloudSearch);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.memoSubject);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.saveBtnOnActivityPage);
            browser.expect.element(elements.activityPageElements.memoViewSubjectFiled).text.to.contains(variables.memoSubject);
            browser.pause(2000);
        });
    });
}
exports.check_Memo_Creation_From_CloudSearch_In_opportunity = check_Memo_Creation_From_CloudSearch_In_opportunity;

var update_Opportunity = function () {
    describe("Update a new opportunity", function () {
        it("should Update a new opportunity", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityCreateLink);
            browser.pause(2000);
            // Create new Opportunity brings up the new Opportunity page
            browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).to.be.present;
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityOrgInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesTerritoryDropdown).to.have.value.not.equals("");
            browser.expect.element(elements.opportunityPageElements.opportunityPopupSalesRepDropdown).to.have.value.not.equals("");
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityPopupApplyBtn);
            browser.pause(2000);
            variables.createdOpportunity = variables.opportunityName;
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityNameInputField, variables.opportunityName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityStageDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('clearValue', browser, elements.opportunityPageElements.opportunityProbabilityInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityProbabilityInput, variables.opportunityProbability);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityCloseDateInput, variables.nextDay);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.opportunityPageElements.opportunityProjectedStartDateInput);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityProjectedStartDateInput, variables.nextDay);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySpecialtyDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.opportunityPageElements.opportunityEditBtn);
            browser.pause(2000);
            browser.getValue(elements.opportunityPageElements.opportunityProbabilityInput,function (re) {
                console.log('> opportunityProbabilityInput  : '+re.value);
            });
            browser.getValue(elements.opportunityPageElements.opportunityCloseDateInput,function (re) {
                console.log('> oppoerunity close date : '+re.value);
            });
            browser.getValue(elements.opportunityPageElements.opportunityProjectedStartDateInput,function (re) {
                console.log('> oppoerunity start date : '+re.value);
            });
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(1000);
        });
    });
};
exports.update_Opportunity = update_Opportunity;

var check_update_Opportunity_Cloud_Search_value = function () {
    describe("Update a new opportunity", function () {
        it("should Update a new opportunity", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityEditBtn);
            browser.pause(2000);
            browser.pause(2000);
            variables.createdOpportunity = variables.opportunityUpdateName;
            commands.checkAndPerform('clearValue', browser, elements.opportunityPageElements.opportunityNameInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityNameInputField, variables.opportunityUpdateName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
            var currentOpportunityUpdateName = variables.opportunityUpdateName;
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentOpportunityUpdateName);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('results-Opportunity');
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityTabHeaderOnCloudSearch);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityCloudSearchDataLink).text.to.contain(currentOpportunityUpdateName);
            browser.pause(2000);

        });
    });
};
exports.check_update_Opportunity_Cloud_Search_value = check_update_Opportunity_Cloud_Search_value;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END






//FUNCTIONS DECLARATION - START

var opportunity_Tab = function () {
    describe("OPPORTUNITY - TAB", function () {
        // orgOperations.create_New_Org();
        // contactOperations.create_Contact('contact');

        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Opportunities_Tab();         // Clicking the Opportunity Tab navigates to Opportunity Screen
        me.check_Opportunity_Tab_Highlighted();                     // Opportunity Tab is Highlighted
        //covered in create_Opportunity()                           // Create new Opportunity brings up the new Opportunity page
        me.check_Opportunity_Table();                               // Page displays all saved opportunities
        me.check_Opportunity_Pagination();                          // Can expand how many Opportunities to show on the page
        // me.create_Opportunity();
        me.navigate_To_New_Opportunity(false);
        me.add_Contact_In_Recent_Items();
        // me.check_Opportunity_Filter_CustomList();                   // Can filter by Preset and custom lists


        me.check_CustomList_Order();                                // Can change the order of custom lists in drop down menu
        me.check_Column_Sortable();                                 // Can sort columns by ascending and descending order
                                                                    // Can deactivate opportunities with x button

        // me.create_Opportunity();
        me.navigate_To_New_Opportunity();                           // Can select and see old Opportunities
    });
};
exports.opportunity_Tab = opportunity_Tab;

var opportunity_Tab_CustomList = function () {
    describe("OPPORTUNITY TAB - CUSTOM LIST", function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Opportunities_Tab();
        commonOperations.create_New_Custom_List(elements.opportunityPageElements.opportunitySettingsIcon,
            elements.opportunityPageElements.opportunityNewCustomListLink,
            elements.opportunityPageElements.opportunityOrderCustomListLink,
            elements.opportunityPageElements.opportunityNewCustomListInput,
            elements.opportunityPageElements.opportunityNewCustomListSaveBtn,
            elements.opportunityPageElements.opportunityOrderCustomListDiv,
            elements.opportunityPageElements.opportunityOrderCustomListCloseBtn,
            elements.opportunityPageElements.opportunityCustomListAddColBtn,
            elements.opportunityPageElements.opportunityCustomListRemoveColBtn,
            elements.opportunityPageElements.opportunityCustomListAvailColumns,
            elements.opportunityPageElements.opportunityCustomListSelectedColumns,
            elements.opportunityPageElements.opportunitiesTable);   // Can create a new custom list

        commonOperations.edit_Custom_List(elements.opportunityPageElements.opportunitySettingsIcon,
            elements.opportunityPageElements.opportunityEditCustomListLink,
            elements.opportunityPageElements.opportunityDeleteCustomListLink,
            elements.opportunityPageElements.opportunityNewCustomListInput,
            elements.opportunityPageElements.opportunityNewCustomListSaveBtn,
            elements.opportunityPageElements.opportunityOrderCustomListLink,
            elements.opportunityPageElements.opportunityOrderCustomListDiv,
            elements.opportunityPageElements.opportunityOrderCustomListCloseBtn);  // Can edit old custom lists

        commonOperations.clone_Custom_List(elements.opportunityPageElements.opportunitySettingsIcon,
            elements.opportunityPageElements.opportunityCloneCustomListInput,
            elements.opportunityPageElements.opportunityCloneCustomListLink,
            elements.opportunityPageElements.opportunityCloneCustomListCreateBtn,
            elements.opportunityPageElements.opportunityOrderCustomListLink,
            elements.opportunityPageElements.opportunityOrderCustomListDiv,
            elements.opportunityPageElements.opportunityOrderCustomListCloseBtn);  // clone custom list

        commonOperations.delete_Custom_List(elements.opportunityPageElements.opportunitySettingsIcon,
            elements.opportunityPageElements.opportunityEditCustomListLink,
            elements.opportunityPageElements.opportunityDeleteCustomListLink,
            elements.opportunityPageElements.opportunityOrderCustomListLink,
            elements.opportunityPageElements.opportunityOrderCustomListDiv,
            elements.opportunityPageElements.opportunityOrderCustomListCloseBtn);  // Can delete customs lists

    });
};
exports.opportunity_Tab_CustomList = opportunity_Tab_CustomList;

var opportunity_QuickFilter = function () {
    describe("OPPORTUNITY - QUICK FILTER", function () {
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Opportunities_Tab();
        me.check_Opportunity_QuickFilter();                         // Selecting Quick filter expands filter menu
                                                                    // Filter by field has drop down menu
                                                                    // Operator field has drop down menu
                                                                    // Value has drop down menue and date selection tool
                                                                    // Selected Columns has all the columns  currently shown in table
                                                                    // If sorting by date static and relative dates work
        commonOperations.check_Filters_Behave_Properly(elements.opportunityPageElements.opportunityQuickFilterLink,
            elements.opportunityPageElements.opportunitiesTable,
            elements.opportunityPageElements.opportunityFilterAvailColumns,
            elements.opportunityPageElements.opportunityFilterSelectedColumns,
            elements.opportunityPageElements.opportunityFilterAddColBtn,
            elements.opportunityPageElements.opportunityFilterRemoveColBtn,
            elements.opportunityPageElements.opportunityFilterApplyBtn);  // Adding columns applies to table after clicking apply

    });
};
exports.opportunity_QuickFilter = opportunity_QuickFilter;

var opportunity_Create_Window = function () {
    describe("OPPORTUNITY - CREATE WINDOW", function () {

        // orgOperations.create_New_Org();
        me.pauseBrowserSomeTime();
        navigateOperations.navigate_To_Opportunities_Tab();
        me.check_Create_Popup_Highlighted_Until_Filled_Up();        // Create is unhighlighted until required fields are entered
                                                                    // Page will not advance until required fields are entered

        me.check_Opportunity_Type_Dropdown();                       // Opportunity Type shows drop down menu

        me.check_Opportunity_Org_Search();                          // Organization brings up search bar for organizations
                                                                    // Search bar only shows close results that the user has access to
                                                                    // Selecting Organization forces update of sales territory/rep
                                                                    // Cannot select organization without updating information(fields are required)

        me.check_Opportunity_Close_Date_Input();                    // Expected close date brings up date selector
                                                                    // Can select dates even ones in the past

        me.check_Opportunity_Stage_Dropdown();                      // Stage has functional drop down menu
        me.check_Opportunity_Specialty_Dropdown();                  // Specialty imports Organizations specialties in functional drop down menu

        me.check_Opportunity_Required_Fields();                     // Required field error messages show up correctly
                                                                    // Error given if sales rep is not in sales territory
    });
};
exports.opportunity_Create_Window = opportunity_Create_Window;

var opportunity_General_Information = function () {
    describe("OPPORTUNITY - GENERAL INFORMATION", function () {
        // adminOperations.create_Stage();
        // orgOperations.create_New_Org();
        // me.create_Opportunity();
        //this is covered in check_Opportunity_Required_Fields      // All required fields must be filled out before saving
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Opportunity(false);
        me.change_Existing_Opportunity();                           // Can change organization
                                                                    // Changing the sales territory deletes the old sales rep
                                                                    // Changing the sales rep does not delete the sales territory
                                                                    // Stage has a drop down menu that changes probability with each stage
                                                                    // Probabilities are changeable according to Admin permissions(relationship management)
                                                                    // Can cc and search for users
                                                                    //     Users will get a message when information is updated(dependent on notification rules)
                                                                    // Can change close date
                                                                    // Can tag providers/contacts to the opportunity
        //this is covered in check_Opportunity_Type_Dropdown        // Opportunity type has a functional drop down menu
    });
};
exports.opportunity_General_Information = opportunity_General_Information;

var opportunity_Revenue_Calculator = function () {
    describe("OPPORTUNITY - REVENUE CALCULATOR", function () {
        // adminOperations.create_Panel();
        // adminOperations.create_FeeSchedule();
        // orgOperations.create_New_Org();
        // me.create_Opportunity();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Opportunity(true);
        me.check_Switch_Tabs();                     //Can switch between panels (orders revenue and panel mix)
        me.check_Lifecycle_Order();                 //Total lifecycle in has a dropdown menu of years quarters and months
        me.check_Estimated_Orders();                //Estimated orders per has years quarters months weeks and day options
        me.check_Average_Revenue();                 //Average revenue per order is populated accurately from specialty(defined in admin)
        me.check_Days_In_Week();                    //days in a work has a drop down menu
        me.check_Estimated_Revenues_Calculated();   //estimated revenues are accurate
        me.check_Lifecycle_Revenue();               //LifeCycle drop down menu works correctly (Quarters Months and Years)
        me.check_Total_Estimated_Revenue();         //total revenue estimate comes up correctly and is properly weighted
        me.check_LifeCycle_Panel();                 //drop down menus for lifecycle in and volume by
        me.check_Search_FeeSchedule();              //Can search for and change fee schedules
        me.check_Search_Panel_Mix();                //can search for and add panels
        me.check_Total_Estimated_Revenue_Panel();   //total revenue estimate comes up correctly and is properly weighted
    });
};
exports.opportunity_Revenue_Calculator = opportunity_Revenue_Calculator;

var opportunity_Details_Tab = function () {
    describe("OPPORTUNITY - DETAILS TAB", function () {
        // adminOperations.create_EMR();
        // orgOperations.create_New_Org();
        // me.create_Opportunity();
        me.pauseBrowserSomeTime();
        me.navigate_To_New_Opportunity(false);
        me.check_Details_Highlighted();         // Details tab is highlighted
        me.check_Switch_Between_Tabs();         // Can switch back and forth between details and general information
        me.check_Select_EMR();                  // can select emr type defined in Admin
        me.check_Toggle_Orders_And_Results();   // Can toggle Orders and Results
        me.check_Select_Competitive_Lab();      // Can select one competitive lab from drop down menu
        me.check_Can_Add_Notes();               // Can add notes
    });
};
exports.opportunity_Details_Tab = opportunity_Details_Tab;

var opportunity_Activities_Attachments_Messages_SubTab = function () {
    describe("OPPORTUNITY - Activities/Attachments/Messages", function () {
        // orgOperations.create_New_Org();
        // navigateOperations.navigate_To_Opportunities_Tab();         // Clicking the Opportunity Tab navigates to Opportunity Screen
        // me.create_Opportunity();

        me.pauseBrowserSomeTime();
        me.navigate_To_New_Opportunity('false');
        messageOperations.send_Message_With_Opportunity();          //Message creation and sending from Collaboration page
        me.navigate_To_New_Opportunity(true);
        me.message_Concerning_Opportunity();                        //Messages concerning opportunity show up here
        me.navigate_to_oppor_activityTab();
        me.check_Activities_Tab_Highlighted();                      //Activities tab is highlighted
        me.create_Task_Activity_From_Opportunity();                 //Can create a new task from here
        me.create_Memo_Activity_From_Opportunity();                 //Can create a new memo from here
        me.created_Activity_showUp_inTable();                       //Created activities will show up in the table after saving
        me.filter_Activity_FromQuick_Filter();                      //Can filter activities with quick filter

        me.create_New_CustomList();                                 //New list allows user to create a new custom list
                                                                    //Can filter activities with custom lists

        me.check_Sorting_Column();                                  //Can sort  columns in ascending and descending order
        me.file_Attachments_And_Storing();                          //Can attach files to store with opportunity

        userAuthentication.logout();
        userAuthentication.login(variables.username1, variables.password1, variables.usernameDisplay1);
        me.navigate_To_New_Opportunity('false');
        me.check_Activities_Tab_Highlighted();                      //Activities tab is highlighted
        me.attachments_unAuthorizeUser_Attach_Record();             //if PHI is enabled users without permission cannot see the record
    });
};
exports.opportunity_Activities_Attachments_Messages_SubTab = opportunity_Activities_Attachments_Messages_SubTab;


var cloud_Search_For_Opportunity = function () {
    describe(" SEARCH FOR OPPORTUNITY OBJECT ", function () {
        // orgOperations.create_New_Org();
        // me.create_Opportunity();
        me.navigate_To_New_Opportunity(false);
        me.check_update_Opportunity_Cloud_Search_value();                   //Edit fields > Ensure the information is updated correctly in cloud search
        
                                                                            // Ensure that opportunity name and opportunity number is present
        me.check_Task_Creation_From_CloudSearch_In_opportunity();        //Ensure user can create Task on opportunity from cloud search
        me.check_Memo_Creation_From_CloudSearch_In_opportunity();       //Ensure user can create memo on opportunity from cloud search
        me.check_Opportunity_Search_In_Cloud_Search();                      //Ensure that user can search for an opportunity
        me.check_Select_Display_Opportunity_In_CloudSearch();               //Select the opportunity name > Ensure user is redirected to opportunity detail page
        me.check_Display_Opportunity_Field_Object();                        //Ensure that opportunity number, organization name, opportunity type, stage and probability, Expected close date and expected start date is present
        
    });
}
exports.cloud_Search_For_Opportunity = cloud_Search_For_Opportunity;

var opportunity_Sheet = function () {
    describe("RESOLVE OPPORTUNITY SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'controlTreeNameCreated',
            'createdOpporStage',
            'createdPanel',
            'createdFeeSchedule',
            'createdEMR',
            'createdCompetitiveLab',
            'createdORG',
            'createdContact',
            'createdOpportunity',
        ]);
        adminOperations.create_Panel();
        adminOperations.create_Stage();
        adminOperations.create_FeeSchedule();
        adminOperations.create_EMR();
        adminOperations.create_competitive_Lab();
        adminOperations.create_New_Control_Tree();
        orgOperations.create_New_Org();
        contactOperations.create_Contact('contact');
        me.create_Opportunity();
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        // test suites which require admin user only
        me.opportunity_Tab();
        me.opportunity_Create_Window();
        me.opportunity_General_Information();
        me.opportunity_Revenue_Calculator();
        me.opportunity_Details_Tab();

        //test suites which requires two users
        me.opportunity_Activities_Attachments_Messages_SubTab();

        //test suites having custom list tests only
        me.opportunity_Tab_CustomList();
        me.opportunity_QuickFilter();
        //SHEET EXECUTION - ENDS

    });
};
exports.opportunity_Sheet = opportunity_Sheet;

//FUNCTIONS DECLARATION - END
