/**
 * Created by INDUSA
 */

require('mocha');
var navigate = require('../utility/navigate.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var adminOperations = require('./admin.js');
var me = require('./customizations.js');

var customList_Attachments = function () {
    describe("CUSTOM LIST ATTACHMENTS", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Attachments');
        adminOperations.edit_newCustomList('Attachments');
        adminOperations.delete_newCustomList('Attachments');

    });
};
exports.customList_Attachments = customList_Attachments;

var customList_AudienceContact = function () {
    describe("CUSTOM LIST AUDIENCE CONTACT", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('AudienceContact');
        adminOperations.edit_newCustomList('AudienceContact');
        adminOperations.delete_newCustomList('AudienceContact');
    });
};
exports.customList_AudienceContact = customList_AudienceContact;

var customList_AudiencePatient = function () {
    describe("CUSTOM LIST AUDIENCE PATIENT", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('AudiencePatient');
        adminOperations.edit_newCustomList('AAudiencePatient');
        adminOperations.delete_newCustomList('AudiencePatient');
    });
};
exports.customList_AudiencePatient = customList_AudiencePatient;

var customList_Campaign = function () {
    describe("CUSTOM LIST CAMPAIGN", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Campaign');
        adminOperations.edit_newCustomList('Campaign');
        adminOperations.delete_newCustomList('Campaign');
    });
};
exports.customList_Campaign = customList_Campaign;

var customList_CampaignActivities = function () {
    describe("CUSTOM LIST CAMPAIGN ACTIVITIES", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Campaign Activities');
        adminOperations.edit_newCustomList('Campaign Activities');
        adminOperations.delete_newCustomList('Campaign Activities');
    });
};
exports.customList_CampaignActivities = customList_CampaignActivities;

var customList_CampaignEntity = function () {
    describe("CUSTOM LIST CAMPAIGN ENTITY", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('CampaignEntity');
        adminOperations.edit_newCustomList('CampaignEntity');
        adminOperations.delete_newCustomList('CampaignEntity');
    });
};
exports.customList_CampaignEntity = customList_CampaignEntity;

var customList_CampaignMessage = function () {
    describe("CampaignMessage", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('CampaignMessage');
        adminOperations.edit_newCustomList('CampaignMessage');
        adminOperations.delete_newCustomList('CampaignMessage');
    });
};
exports.customList_CampaignMessage = customList_CampaignMessage;

var customList_Activities = function () {
    describe("CUSTOM LIST ACTIVITIES", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Activities');
        adminOperations.edit_newCustomList('Activities');
        adminOperations.delete_newCustomList('Activities');
    });
};
exports.customList_Activities = customList_Activities;

var customList_Contact = function () {
    describe("CUSTOM LIST CONTACT", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Contact');
        adminOperations.edit_newCustomList('Contact');
        adminOperations.delete_newCustomList('Contact');
    });
};
exports.customList_Contact = customList_Contact;

var customList_ContactActivities = function () {
    describe("CUSTOM LIST CONTACT ACTIVITIES", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Contact Activities');
        adminOperations.edit_newCustomList('Contact Activities');
        adminOperations.delete_newCustomList('Contact Activities');
    });
};
exports.customList_ContactActivities = customList_ContactActivities;

var customList_ContactOrganizations = function () {
    describe("CUSTOM LIST CONTACT ORGANIZATIONS", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Contact Organizations');
        adminOperations.edit_newCustomList('Contact Organizations');
        adminOperations.delete_newCustomList('Contact Organizations');
    });
};
exports.customList_ContactOrganizations = customList_ContactOrganizations;

var customList_Diagnosis = function () {
    describe("CUSTOM LIST DIAGNOSIS", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Diagnosis');
        adminOperations.edit_newCustomList('Diagnosis');
        adminOperations.delete_newCustomList('Diagnosis');
    });
};
exports.customList_Diagnosis = customList_Diagnosis;

var customList_DistributionGroup = function () {
    describe("CUSTOM LIST DISTRIBUTION GROUP", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Distribution Group');
        adminOperations.edit_newCustomList('Distribution Group');
        adminOperations.delete_newCustomList('Distribution Group');
    });
};
exports.customList_DistributionGroup = customList_DistributionGroup;

var customList_EntityMessage = function () {
    describe("CUSTOM LIST ENTITY MESSAGE", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('EntityMessage');
        adminOperations.edit_newCustomList('EntityMessage');
        adminOperations.delete_newCustomList('EntityMessage');
    });
};
exports.customList_EntityMessage = customList_EntityMessage;

var customList_FeeSchedule = function () {
    describe("CUSTOM LIST FEE SCHEDULE", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Fee Schedule');
        adminOperations.edit_newCustomList('Fee Schedule');
        adminOperations.delete_newCustomList('Fee Schedule');
    });
};
exports.customList_FeeSchedule = customList_FeeSchedule;

var customList_HL7Interface = function () {
    describe("CUSTOM LIST HL7 INTERFACE", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('HL7 Interface');
        adminOperations.edit_newCustomList('HL7 Interface');
        adminOperations.delete_newCustomList('HL7 Interface');
    });
};
exports.customList_HL7Interface = customList_HL7Interface;

var customList_HRMAccessControl = function () {
    describe("CUSTOM LIST HRM ACCESS CONTROL", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('HRM Access Control');
        adminOperations.edit_newCustomList('HRM Access Control');
        adminOperations.delete_newCustomList('HRM Access Control');
    });
};
exports.customList_HRMAccessControl = customList_HRMAccessControl;

var customList_MainActivity = function () {
    describe("CUSTOM LIST MAIN ACTIVITY", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Main Activity');
        adminOperations.edit_newCustomList('Main Activity');
        adminOperations.delete_newCustomList('Main Activity');
    });
};
exports.customList_MainActivity = customList_MainActivity;

var customList_Message = function () {
    describe("CUSTOM LIST MESSAGE", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Message');
        adminOperations.edit_newCustomList('Message');
        adminOperations.delete_newCustomList('Message');
    });
};
exports.customList_Message = customList_Message;

var customList_MessageThread = function () {
    describe("CUSTOM LIST MESSAGE THREAD", function () {
        me.pauseBrowserSomeTime();
        adminOperations.create_newCustomList('Message Thread');
        adminOperations.edit_newCustomList('Message Thread');
        adminOperations.delete_newCustomList('Message Thread');
    });
};
exports.customList_MessageThread = customList_MessageThread;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

var customization_Sheet = function () {
    describe("CUSTOMIZATION OPERATION", function () {

        //SHEET EXECUTION - STARTS
        me.customList_Attachments();
        me.customList_AudienceContact();
        me.customList_AudiencePatient();
        me.customList_Campaign();
        me.customList_CampaignActivities();
        me.customList_CampaignEntity();
        me.customList_CampaignMessage();
        me.customList_Activities();
        me.customList_Contact();
        me.customList_ContactActivities();
        me.customList_ContactOrganizations();
        me.customList_Diagnosis();
        me.customList_DistributionGroup();
        me.customList_EntityMessage();
        me.customList_FeeSchedule();
        me.customList_HL7Interface();
        me.customList_HRMAccessControl();
        me.customList_MainActivity();
        me.customList_Message();
        me.customList_MessageThread();
        //SHEET EXECUTION - ENDS

    });
};
exports.customization_Sheet = customization_Sheet;