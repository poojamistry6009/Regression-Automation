/**
 * Created by INDUSA
 */


require('mocha');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var variables = require('../utility/variables.js');
var me = require('./webServices.js');
var navigate = require('../utility/navigate.js');

//test cases declaration - starts
var navigate_And_check_Interface_Visibility = function () {
    describe("Ensure that users can see the interface tab if they have appropriate permissions  ", function () {
        it("Should check - Ensure that users can see the interface tab if they have appropriate permissions  ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.globalElements.headerMenu, 3000, 3000, function () {
                browser.pause(2000);
                browser.click(elements.globalElements.headerMenu);
                browser.pause(2000);
                browser.expect.element(elements.webservicePageElements.interfaceOption).text.to.contains('Interfaces');
                browser.pause(2000);
                browser.click(elements.adminPageElements.interfaceLink);
                browser.pause(3000);
            });
            browser.pause(5000);
        });
    });
}
exports.navigate_And_check_Interface_Visibility = navigate_And_check_Interface_Visibility;

var check_Data_Transaction_And_SettingsTab_Visibility = function () {
    describe(" Ensure that DataTransaction and settings subtab is present ", function () {
        it("should check - Ensure that DataTransaction and settings subtab is present  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.interfaceDataTransactionsTab).text.to.contains('Transactions');
            browser.expect.element(elements.webservicePageElements.interfaceDataSettingsTab).text.to.contains('Settings');
            browser.pause(2000);
        });
    });
}
exports.check_Data_Transaction_And_SettingsTab_Visibility = check_Data_Transaction_And_SettingsTab_Visibility;

var check_reset_Search = function () {
    describe(" Ensure that users can reset search ", function () {
        it("Should check - Ensure that users can reset search  ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceSystemInput);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.interfaceMessageInput, 'Test Message')
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceResetBtn);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.interfaceMessageInput).text.to.not.contain('Test Message');
            browser.pause(2000);
        });
    });
}
exports.check_reset_Search = check_reset_Search;

var check_Fields_Visibility = function () {
    describe("Ensure that Name, connection ID, Resend URL, Last Error and Last received are displayed correctly ", function () {
        it("Should check - Ensure that Name, connection ID, Resend URL, Last Error and Last received are displayed correctly ", function (browser) {
            browser.pause(2000);
            var defSettingTableRow = variables.settinsTableRow;
            browser.click(elements.webservicePageElements.interfaceDataSettingsTab);
            browser.pause(2000);
            browser.getText(elements.webservicePageElements.interfaceSettingTable+"/thead/tr",function (re) {
               console.log('======> Settings table header rows : '+re.value);
                browser.expect.element(elements.webservicePageElements.interfaceSettingTable+"/thead/tr").text.to.contains(defSettingTableRow);
            });
            browser.pause(2000);

        });
    });
}
exports.check_Fields_Visibility = check_Fields_Visibility;

var check_Create_Interface = function () {
    describe("Ensure that user can creat a new interface if the user has interface write permission ", function () {
        it("Should check -Ensure that user can creat a new interface if the user has interface write permission ", function (browser) {
            browser.pause(2000);
            browser.click(elements.webservicePageElements.interfaceDataSettingsTab);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceCreateLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceSaveBtn);
            browser.expect.element(elements.webservicePageElements.interfaceNameMessageLabel).text.to.contain('This field is required');
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.webservicePageElements.interfaceNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.interfaceNameInput, variables.interfaceName)
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.webservicePageElements.interfaceConnectionIdInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.interfaceConnectionIdInput, variables.interfaceConnectionId)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.interfaceTabel).text.to.contain(variables.interfaceName);
            browser.pause(2000);
        });
    });
}
exports.check_Create_Interface = check_Create_Interface;

var check_Edit_Interface = function () {
    describe("Edit a existing interface  ", function () {
        it("sHould check -Edit a existing interface  ", function (browser) {
            browser.pause(2000);
            var createdInterface = variables.interfaceName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(., '" + createdInterface + "')]");
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.webservicePageElements.interfaceNameInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.interfaceNameInput, variables.interfaceUpdateName)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.interfaceTabel).text.to.contain(variables.interfaceName);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceDeactiveIcon); //For convert a active mode
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.interfaceDeactiveIcon);//For convert a deactive
            browser.pause(2000);
        });
    });
}
exports.check_Edit_Interface = check_Edit_Interface;

var check_Distribution_Group = function () {
    describe("Create New Distribution group - Complete All Standard Fields ", function () {
        it("Should check -Create New Distribution group - Complete All Standard Fields ", function (browser) {
            browser.pause(2000);
            variables.createdDistributionGroup = variables.distributionGroupName;
            browser.moveToElement(elements.globalElements.headerMenu, 3000, 3000, function () {
                browser.pause(2000);
                browser.click(elements.globalElements.headerMenu);
                browser.pause(2000);
                browser.click(elements.adminPageElements.adminLink);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.adminPageElements.distributionLink);
                browser.pause(3000);
            });
            commands.checkAndPerform('click', browser, elements.webservicePageElements.addNewDistributionGroupLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.distributionGroupNameInput, variables.createdDistributionGroup )
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.distributionGroupEmailInput, variables.distributionGroupEmail)
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.distributionGroupDescInput, variables.distributionGroupDesc)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.saveBtnDistributionGroup);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.distributionGroupTable).text.to.contain(variables.createdDistributionGroup );
        });
    });
}
exports.check_Distribution_Group = check_Distribution_Group;

var add_User_TO_Distribution_Group = function () {
    describe("Add user to distribution group ", function () {
        it("Should check - Add user to distribution group ", function (browser) {
            browser.pause(2000);
            // variables.createdDistributionGroup = variables.distributionGroupName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='" + variables.createdDistributionGroup + "']");
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.webservicePageElements.addUserInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.addUserInputField, variables.firstname)
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.saveBtnDistributionGroup);
        });
    });
}
exports.add_User_TO_Distribution_Group = add_User_TO_Distribution_Group;

var user_can_search_With_Distribution_Group = function () {
    describe("Sends a message to user with Organization", function () {
        it("should check - should send a message to user with Organization", function (browser) {
            browser.pause(2000);
            var createdDistributionGroup = variables.distributionGroupName;
            var createSubjectName = variables.newMessageDistributionSubject;
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.collaborationTabLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/button[3]');
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageLink);
            browser.pause(1000);
            commands.checkAndSetValue(browser, elements.collaborationPageElements.newMessageToInput, variables.firstname.charAt(0).toUpperCase() + variables.firstname.slice(1));
            browser.pause(3000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.sendMessageToSearchInput, createdDistributionGroup)
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            variables.sentOrgMessageSubject = variables.newMessageSubject;
            commands.checkAndSetValue(browser, elements.collaborationPageElements.newMessageSubjectInput, variables.newMessageDistributionSubject);
            commands.checkAndSetValue(browser, elements.collaborationPageElements.newMessageMessageInput, variables.newMessage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.collaborationPageElements.newMessageSendBtn);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.sentMsgTable).text.to.contain(createSubjectName);
        });
    });
}
exports.user_can_search_With_Distribution_Group = user_can_search_With_Distribution_Group;

var cloud_Search_Distribution_Group = function () {
    describe("Ensure that the user can search for the Distribution group in search ", function () {
        it("Should check -Ensure that the user can search for the Distribution group in search ", function (browser) {
            browser.pause(2000);
            var createdDistributionGroup = variables.distributionGroupName;
            commands.checkAndPerform('click', browser, elements.webservicePageElements.cloudSearchField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.cloudSearchField, createdDistributionGroup)
            browser.pause(2000);
            browser.expect.element(elements.campaignPageElements.campaignTabHeaderOnCloudSearchDiv).to.have.attribute('class').which.contain('gs-campaigns');
            browser.pause(2000);
        });
    });
}
exports.cloud_Search_Distribution_Group = cloud_Search_Distribution_Group;

var update_Distribution_Group = function () {
    describe("Update Distribution group record using update ", function () {
        it("Should check - Update Distribution group record using update ", function (browser) {
            var createdDistributionGroup = variables.distributionGroupName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[@title='" + createdDistributionGroup + "']");
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.webservicePageElements.distributionGroupDescInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.distributionGroupDescInput, variables.distributionGroupUpdateDesc)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.saveBtnDistributionGroup);
            browser.pause(2000);
        });
    });
}
exports.update_Distribution_Group = update_Distribution_Group;

var create_New_User_Profile = function () {
    describe("Create New user profile  - Complete All Standard Fields ", function () {
        it("Should check - Create New user profile  - Complete All Standard Fields ", function (browser) {
            variables.createdUserProfile = variables.userProfileName;
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.userProfileTab);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.createNewUserProfile);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.userProfileNameInput, variables.userProfileName);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.userProfileDescInput, variables.userProfileDesc);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.userProfileReadOnlyCheckBox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.userProfileVisibleDistributionGroupTab);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.webservicePageElements.userProfileVisibleDistributionGroupTab, variables.createdDistributionGroup )
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.userProfileSaveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.webservicePageElements.userProfilePopWindowYesBtn);
            browser.pause(2000);
            browser.expect.element(elements.webservicePageElements.userProfileTable).text.to.contain(variables.createdUserProfile);
            browser.pause(2000);
        });
    });
}
exports.create_New_User_Profile = create_New_User_Profile;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//test cases declaration - ends



//function declaration - starts

var interface_Page = function () {
    describe(" INTERFACE PAGE", function () {
        me.pauseBrowserSomeTime();
        me.navigate_And_check_Interface_Visibility();                       //Ensure that users can see the interface tab if they have appropriate permissions
        me.check_Data_Transaction_And_SettingsTab_Visibility();          //Ensure that DataTransaction and settings subtab is present
        me.check_reset_Search();                                        //Ensure that users can reset search
    });
}
exports.interface_Page = interface_Page;

var user_Profile = function () {
    describe("USER PROFILE", function () {
        me.pauseBrowserSomeTime();
        navigate.navigate_To_UserProfiles_Tab();
        me.create_New_User_Profile();       //Create New user profile - Complete All Standard Fields
                                            //Add visible distribution group to user profile
                                            //Ensure that the records are displayed correctly in the UI
    });
}
exports.user_Profile = user_Profile;

var distribution_Group = function () {
    describe("DISTRIBUTION GROUP ", function () {
        me.pauseBrowserSomeTime();
        me.check_Distribution_Group();                        //Create New Distribution group - Complete All Standard Fields
        me.update_Distribution_Group();                      //Update distribution group description
        me.add_User_TO_Distribution_Group();                //Add user to distribution group
        me.user_can_search_With_Distribution_Group();      //Ensure that the user can search for the Distribution group in search
    });
}
exports.distribution_Group = distribution_Group;

var Create_interface = function () {
    describe(" CREATE INTERFACE", function () {
        me.pauseBrowserSomeTime();
        me.navigate_And_check_Interface_Visibility();
        me.check_Create_Interface();                //Ensure that user can creat a new interface if the user has interface write permission
                                                    //Name and connection id is a required field
                                                    //Ensure that a error message is displayed if the user don't fill in the required fields
        me.check_Edit_Interface();          //Interface detail page               //Edit a existing interface
                                            //Select save > ensure that the values are saved successfully
                                            //Ensure that the user can reactive a connection
                                            //Deactivate a interface from the interface detail page > Ensure that the interface is deactivated successfully
    });
}
exports.Create_interface = Create_interface;

var setting_SubTab = function () {
    describe("SETTING SUBTAB ", function () {
        me.pauseBrowserSomeTime();
        me.navigate_And_check_Interface_Visibility();
        me.check_Fields_Visibility();                   //Ensure that the list is displayed correctly
    });
}
exports.setting_SubTab = setting_SubTab;

var webServices_Sheet = function () {
    describe("WEBSERVICES SHEET", function () {

        //SHEET EXECUTION - STARTS
        me.interface_Page();
        me.setting_SubTab();
        me.Create_interface();
        me.distribution_Group();
        me.user_Profile();
        //SHEET EXECUTION - ENDS

    });
};
exports.webServices_Sheet = webServices_Sheet;

//function declaration - starts