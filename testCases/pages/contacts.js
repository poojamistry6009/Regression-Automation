/**
 * Created by INDUSA
 */

require('mocha');
var expect = require('chai').expect;
var moment = require('moment');
var me = require('./contacts.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var navigate = require('../utility/navigate.js');
var orgOperations = require('./organization.js');
var campaignOperations = require('./campaigns.js');
var userAuthentication = require('./userAuthentication.js');
var adminOperations = require('./admin.js');
var commonOperations = require('./commons.js');
var error = require('../utility/error.js');


//TEST CASE DECLARATION - START

var create_Contact = function (type) {
    describe("Create New Contact/Provider", function() {
        it("should create New Contact/Provider", function(browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(8000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.createContactLink);
            browser.pause(2000);
            if(type.toLowerCase() === 'provider') {
                commands.checkAndPerform('click', browser, elements.contactsPageElements.providerTypeInput);
                browser.pause(2000);
                commands.checkAndSetValue(browser, elements.contactsPageElements.providerNPIInput, variables.providerNPI);
                variables.createdProvider = variables.providerFName;
                variables.createdProviderFname = variables.providerFName;
                variables.createdProviderLname = variables.providerLName;
                variables.createdProviderMname = variables.providerMName;
                commands.checkAndSetValue(browser, elements.contactsPageElements.firstNameInput, variables.providerFName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.middleNameInput, variables.providerMName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.lastNameInput, variables.providerLName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.suffixInput, variables.contactSuffix);
                commands.checkAndSetValue(browser, elements.contactsPageElements.emailInput, variables.contactEmail);
                commands.checkAndSetValue(browser, elements.contactsPageElements.phoneInput, variables.contactPhone);
                commands.checkAndSetValue(browser, elements.contactsPageElements.mobileInput, variables.contactMobile);
                commands.checkAndSetValue(browser, elements.contactsPageElements.organizationInput, variables.createdORG);
            }
            else{
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactTypeInput);
                browser.pause(2000);
                variables.createdContact = variables.contactFName;
                variables.createdContactFname = variables.contactFName;
                variables.createdContactLname = variables.contactLName;
                variables.createdContactMname = variables.contactMName;
                commands.checkAndSetValue(browser, elements.contactsPageElements.firstNameInput, variables.contactFName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.middleNameInput, variables.contactMName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.lastNameInput, variables.contactLName);
                commands.checkAndSetValue(browser, elements.contactsPageElements.suffixInput, variables.contactSuffix);
                commands.checkAndSetValue(browser, elements.contactsPageElements.emailInput, variables.contactEmail);
                commands.checkAndSetValue(browser, elements.contactsPageElements.phoneInput, variables.contactPhone);
                commands.checkAndSetValue(browser, elements.contactsPageElements.mobileInput, variables.contactMobile);
                commands.checkAndSetValue(browser, elements.contactsPageElements.organizationInput, variables.createdORG);
            }
            browser.pause(5000);
            browser.keys(browser.Keys.RIGHT_ARROW);
            browser.keys(browser.Keys.ENTER);
            // commands.checkAndPerform('click', browser, elements.contactsPageElements.orgSearchPopUp);
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.createLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsPageSaveBtn);
            if(type.toLowerCase() === 'provider'){
                browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.equal(variables.providerFName+" "+variables.providerMName+" "+variables.providerLName+" "+variables.contactSuffix);
            }
            else{
                browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.equal(variables.contactFName+" "+variables.contactMName+" "+variables.contactLName+" "+variables.contactSuffix);
            }
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.contactsPageElements.contactsTable).to.be.present;
            browser.elements("xpath", elements.contactsPageElements.contactsTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if(type.toLowerCase() === 'provider'){
                            expect(text.value.includes(variables.createdProvider)).to.be.true;
                            variables.dependencyFulfilled.push('createdProvider');
                            console.log('=====> Provider created successfully : '+variables.createdProvider);
                        }
                        else{
                            expect(text.value.includes(variables.createdContact)).to.be.true;
                            variables.dependencyFulfilled.push('createdContact');
                            console.log('=====> Contact created successfully : '+variables.createdContact);
                        }
                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
module.exports.create_Contact = create_Contact;

var create_Contact_Advance = function (type) {
    describe("Create New Contact/Provider", function() {
        it("should create New Contact/Provider", function(browser) {
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
                browser.pause(8000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.createContactLink);
                browser.pause(2000);
                if (type.toLowerCase() === 'provider') {
                    commands.checkAndPerform('click', browser, elements.contactsPageElements.providerTypeInput);
                    browser.pause(2000);
                    commands.checkAndSetValue(browser,elements.contactsPageElements.providerNPIInput, variables.providerNPI);
                    browser.pause(2000);
                }
                // Ensure that new contact modal is displayed properly
                browser.expect.element(elements.contactsPageElements.contactCreateModalDiv).to.be.visible;
                // Ensure that a error message is displayed if the user's saves without entering the required fields
                commands.checkAndPerform('click', browser, elements.organizationPageElements.contactsPageCreateBtn);
                browser.expect.element(elements.contactsPageElements.contactFNameErrorLabel).to.be.present;
                browser.expect.element(elements.contactsPageElements.contactLNameErrorLabel).to.be.present;
                browser.expect.element(elements.contactsPageElements.contactOrgErrorLabel).to.be.present;
                // Ensure Contact radio button is selected
                if(type.toLowerCase() === 'contact'){
                    browser.expect.element(elements.contactsPageElements.contactCreateModalContactOption).to.have.attribute('checked').which.contain('true');
                }
                // Ensure that user can select the contact type from the contact type drop down
                browser.pause(1000);
                commands.checkAndPerform('click', browser, '/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[3]/div/div');
                browser.pause(1000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactCreateModalTypeDropdown);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                // First name is a required field
                browser.expect.element(elements.contactsPageElements.firstNameInput).to.have.attribute('required').which.contain('true');
                // Enter First Name
                variables.createdContactFname = variables.contactFName;
                variables.createdContactLname = variables.contactLName;
                variables.createdContactMname = variables.contactMName;
                commands.checkAndSetValue(browser, elements.contactsPageElements.firstNameInput, variables.contactFName);
                // Ensure that user can select the salutation from the drop down
                browser.pause(1000);
                commands.checkAndPerform('click', browser, '/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[4]/div/div');
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactCreateModalSalutationDropdown);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                // Ensure that existing contacts search behaves properly
                browser.pause(2000);
                browser.expect.element(elements.contactsPageElements.contactExistingSearchResultsDiv).to.not.be.present;
                // Enter Middle Name
                commands.checkAndSetValue(browser, elements.contactsPageElements.middleNameInput, variables.contactMName);
                // Last Name is required field
                browser.expect.element(elements.contactsPageElements.lastNameInput).to.have.attribute('required').which.contain('true');
                commands.checkAndSetValue(browser, elements.contactsPageElements.lastNameInput, variables.contactLName);
                // Add Suffix
                commands.checkAndSetValue(browser, elements.contactsPageElements.suffixInput, variables.contactSuffix);
                // Add Email
                commands.checkAndSetValue(browser, elements.contactsPageElements.emailInput, variables.contactEmail);
                // Add Phone
                commands.checkAndSetValue(browser, elements.contactsPageElements.phoneInput, variables.contactPhone);
                // Add Mobile
                commands.checkAndSetValue(browser, elements.contactsPageElements.mobileInput, variables.contactMobile);
                // Ensure that Organization is a required field
                browser.expect.element(elements.contactsPageElements.contactOrgInputDiv).to.have.attribute('class').which.contain('required');
                // Search for a organization
                // Ensure that the organization search behaves properly
                commands.checkAndSetValue(browser, elements.contactsPageElements.organizationInput, variables.createdORG);
                browser.pause(5000);
                browser.expect.element(elements.contactsPageElements.orgSearchPopUp).to.be.visible;
                browser.pause(3000);
                // Create new organization - Ensure that the contact is saved correctly
                browser.keys(browser.Keys.RIGHT_ARROW);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactCreateOrgBtnInOrgSearch);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactSaveOrgBtnInOrgSearch);
                // Check "Copy the organization's mailing address to this record" checkbox
                browser.expect.element(elements.contactsPageElements.contactCopyOrgAddressCheckbox).to.be.present;
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactCopyOrgAddressCheckbox);
                // Check "Make important contact" checkbox
                browser.expect.element(elements.contactsPageElements.contactMakePrimaryCheckBox).to.be.present;
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactMakePrimaryCheckBox);
                // Enter all the required fields and select Quick create - Ensure that the contact is created successfully
                browser.pause(1000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.quickCreateLink);
                // Enter all the required fields and select Create - Ensue that the contact is saved successfully
                browser.pause(2000);
                commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.createdContactLname + ', ' + variables.createdContactFname + ' ' + variables.createdContactMname + '")]');
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactEditBtn);
                browser.pause(2000);
                commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsPageSaveBtn);
                // Ensure that the user is brought to the contact detail page
                browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.contain(variables.contactFName + " " + variables.contactMName + " " + variables.contactLName + " " + variables.contactSuffix);
        });
    });
};
module.exports.create_Contact_Advance = create_Contact_Advance;

var navigate_To_New_Contact = function () {
    describe("Navigate to newly created contact", function () {
        it("should navigate to newly created contact", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(1000);
            var contactName = variables.createdContactLname+", "+variables.createdContactFname+" "+variables.createdContactLname;
            commands.checkAndPerform('click', browser,'//a[contains(@title,"'+variables.createdContactLname+'")]');
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Contact = navigate_To_New_Contact;

var navigate_To_New_Provider = function () {
    describe("Navigate to newly created provider", function () {
        it("should navigate to newly created provider", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,'//a[contains(@title,"'+variables.createdProviderLname+'")]');
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Provider = navigate_To_New_Provider;

var navigate_To_New_Contact_Org_Subtab = function () {
    describe("Navigate to newly created contact organization subtab", function () {
        it("should navigate to newly created contact organization subtab", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(1000);
            var contactName = variables.contactLName+", "+variables.contactFName+" "+variables.contactMName;
            commands.checkAndPerform('click', browser,'//a[contains(@title,"'+contactName+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgSubtab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_New_Contact_Org_Subtab = navigate_To_New_Contact_Org_Subtab;

var check_Primary_Contact = function () {
    describe("Check primary contact in organization", function () {
        it("should check - primary contact in organization", function (browser) {
                browser.pause(2000);
                browser.expect.element('//a[contains(.,"' + variables.contactFName + ' ' + variables.contactLName + '")]').to.be.present;
                browser.pause(2000);
        });
    });
};
exports.check_Primary_Contact = check_Primary_Contact;

var check_Upload_Pic_Permissions = function () {
    describe("Upload permissions", function () {
        it("should check - Upload profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicUploadBtn).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Upload_Pic_Permissions = check_Upload_Pic_Permissions;

var check_Resize_Pic_Permissions = function () {
    describe("Resize permissions", function () {
        it("should check - Resize profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicResizeControl).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Resize_Pic_Permissions = check_Resize_Pic_Permissions;

var check_Edit_Pic_Permissions = function () {
    describe("Edit permissions", function () {
        it("should check - Edit profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicEditBtn).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Edit_Pic_Permissions = check_Edit_Pic_Permissions;

var check_Delete_Pic_Permission = function () {
    describe("Delete permissions", function () {
        it("should check - Delete profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicDeleteBtn).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Delete_Pic_Permission = check_Delete_Pic_Permission;

var check_Cancel_Pic_Permissions = function () {
    describe("Cancel save permissions", function () {
        it("should check - Cancel save profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicCancelBtn).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Cancel_Pic_Permissions = check_Cancel_Pic_Permissions;

var check_Upload_Pic_Permissions_Fail = function () {
    describe("Upload permissions", function () {
        it("should check - must not have upload profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicUploadBtn).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Upload_Pic_Permissions_Fail = check_Upload_Pic_Permissions_Fail;

var check_Delete_Pic_Permissions_Fail = function () {
    describe("Delete permissions", function () {
        it("should check - must not have delete profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicDeleteBtn).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Delete_Pic_Permissions_Fail = check_Delete_Pic_Permissions_Fail;

var check_Edit_Pic_Permissions_Fail = function () {
    describe("Edit permissions", function () {
        it("should check - must not have edit profile pic permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPicEditBtn).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Edit_Pic_Permissions_Fail = check_Edit_Pic_Permissions_Fail;

var check_HostCode_Permissions = function () {
    describe("Users with Host Code permissions can Add / Delete Host Codes", function () {
        it("should check - Users with Host Code permissions can Add / Delete Host Codes", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSettingsIcon);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactHostCodeEditBtn).to.be.present;
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSettingsPopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_HostCode_Permissions = check_HostCode_Permissions;

var check_VCard_ContactType = function () {
    describe("Ensure that the contact type is present", function () {
        it("should check - Ensure that the contact type is present", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactVCardContactTypeDiv).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_VCard_ContactType = check_VCard_ContactType;

var check_VCard_ContactOrg_Display = function () {
    describe("Ensure that the organizations that the contact is associated with is displayed in the vcard", function () {
        it("should check - Ensure that the organizations that the contact is associated with is displayed in the vcard", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactVCardOrgDetailsDiv).to.be.present;
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactVCardOrgDetailNameDiv).text.to.contain(variables.createdORG);
            browser.pause(2000);
        });
    });
};
exports.check_VCard_ContactOrg_Display = check_VCard_ContactOrg_Display;

var check_VCard_HostCode = function () {
    describe("Ensure that the contact hostcode is present in the vcard", function () {
        it("should check - Ensure that the contact hostcode is present in the vcard", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactVCardHostCodeDiv).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_VCard_HostCode = check_VCard_HostCode;

var check_HostCode_Permissions_Fail = function () {
    describe("Users without Host Code Permissions have read-only Access to host codes", function () {
        it("should check - Users without Host Code Permissions have read-only Access to host codes", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSettingsIcon);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactHostCodeEditBtn).to.not.be.present;
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSettingsPopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_HostCode_Permissions_Fail = check_HostCode_Permissions_Fail;

var check_Contact_Subtabs_Display = function () {
    describe("Subtabs General, Address, Org, Create User, Activties, Orders and Attachments are displayed", function () {
        it("should check - Subtabs General, Address, Org, Create User, Activties, Orders and Attachments are displayed", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactGeneralTab).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactAddressTab).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactOrgSubtab).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactActivitiesSubTab).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactAttachmentsSubTab).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactMessagesSubTab).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Subtabs_Display = check_Contact_Subtabs_Display;

var check_General_Tab = function () {
    describe(" General Subtab is highlighted", function () {
        it("should check -  General Subtab is highlighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactGeneralTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_General_Tab = check_General_Tab;

var check_Edit_Mode_Write_Permissions = function () {
    describe("Selecting a Record name Enters in Edit mode (If user has write permissions)", function () {
        it("should check - Selecting a Record name Enters in Edit mode (If user has write permissions)", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactEditBtn).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Edit_Mode_Write_Permissions = check_Edit_Mode_Write_Permissions;

var check_Edit_Mode_Read_Permissions = function () {
    describe("Selecting a Record name enters in View mode, if user has read-only permissions", function () {
        it("should check - Selecting a Record name enters in View mode, if user has read-only permissions", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactEditBtn).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Edit_Mode_Read_Permissions = check_Edit_Mode_Read_Permissions;

var check_ContactIcon_Display = function () {
    describe("Contact Icon Displays on Page", function () {
        it("should check - Contact Icon Displays on Page", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactPageIcon).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_ContactIcon_Display = check_ContactIcon_Display;

var add_ContactData = function (type) {
    describe("Add contact data", function () {
        it("should check - whether contact data is added or not", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);
            browser.pause(2000);
            // Select a Salutation
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageSalutationDropdown);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);

            // Add First Name (Required)
            // Add Middle name
            // Add Last Name (required
            // Add suffix
            // Add email address
            // Add phone
            // Add Mobile phone

            // Add Other phone
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageOtherPhoneInput, variables.contactPhone);

            // Add fax
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageFaxInput, variables.contactFax);

            // Add Job Title
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactJobTitleInput, variables.contactJobTitle);

            if(type.toLowerCase() === 'provider') {
                browser.pause(2000);
                browser.expect.element(elements.contactsPageElements.providerDetailPageSpecialtyDropdown).to.be.present;
                commands.checkAndPerform('click', browser,elements.contactsPageElements.providerDetailPageSpecialtyDropdown);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
            }

            // Add Department
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactDepartmentInput, variables.contactDepartment);

            // Add Credentials
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactCredentialsInput, variables.contactCredential);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCommPrefTab);

            // Select 'Email opt out'
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEmailOptOutCheckbox);

            // Perferred communication - Select Email
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPreferEmailsCheckbox);

            // Preferred communication - Select Text
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPreferTextsCheckbox);

            // Campaign email opt out - Unsubcribe from a campaign, ensure that the email opt out checkbox is checked

            // Select Text opt out
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactTextOptOutCheckbox);
            // browser.assert.attributeContains(elements.contactsPageElements.contactTextOptOutCheckbox, 'checked', 'true');

            // Campaign text opt out - Unsubcribe from a campaign text, Ensure that the text opt out is checked

            // Select  Do Not Call  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactDoNotCallCheckbox);

            // Select  Customer  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCustomerCheckbox);

            // Select  Lead  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactLeadCheckbox);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsPageSaveBtn);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactGeneralTab);
            browser.expect.element(elements.contactsPageElements.contactJobTitleDiv).text.to.contain(variables.contactJobTitle);

            browser.pause(2000);
        });
    });
};
exports.add_ContactData = add_ContactData;

var edit_ContactData = function () {
    describe("Modify contact data", function () {
        it("should check - whether contact data is modified or not", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);

            // Edit First Name (Required)
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageFnameInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageFnameInput, variables.contactFNameUpdated);

            // Edit Middle name
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageMNameInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageMNameInput, variables.contactMNameUpdated);

            // Edit Last Name (required
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageLNameInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageLNameInput, variables.contactLNameUpdated);

            // Edit suffix
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageSuffixInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageSuffixInput, variables.contactSuffixUpdated);

            // Edit email address
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageEmailInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageEmailInput, variables.username1);

            // Edit phone
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPagePhoneInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPagePhoneInput, variables.contactPhoneUpdated);

            // Edit Mobile phone
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageMobileInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageMobileInput, variables.contactMobileUpdated);

            // Edit Other phone
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageOtherPhoneInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageOtherPhoneInput, variables.contactPhoneUpdated);

            // Edit fax
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactPageFaxInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactPageFaxInput, variables.contactFaxUpdated);

            // Edit Job Title
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactJobTitleInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactJobTitleInput, variables.contactJobTitleUpdated);

            // Edit Department
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactDepartmentInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactDepartmentInput, variables.contactDepartmentUpdated);

            // Edit Credentials
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactCredentialsInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactCredentialsInput, variables.contactCredentialUpdated);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCommPrefTab);


            // uncheck 'Email opt out'
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEmailOptOutCheckbox);

            // uncheck   Do Not Call  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactDoNotCallCheckbox);

            // uncheck  Customer  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCustomerCheckbox);

            // uncheck  Lead  checkbox
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactLeadCheckbox);

            // Save and Back button commits changes and returns to the previous screen
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsPageSaveBtn);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactGeneralTab);
            browser.expect.element(elements.contactsPageElements.contactJobTitleDiv).text.to.contain(variables.contactJobTitleUpdated);

            browser.pause(2000);
        });
    });
};
exports.edit_ContactData = edit_ContactData;

var check_Breadcrumb_Trail = function () {
    describe("Breadcrumb trail captures navigation history", function () {
        it("should check - Breadcrumb trail captures navigation history", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
        });
    });
};
exports.check_Breadcrumb_Trail = check_Breadcrumb_Trail;

var check_DateAndTime = function () {
    describe("Create Date and Time presents", function () {
        it("should check - Create Date and Time presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactFooterInfo).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_DateAndTime = check_DateAndTime;

var check_CreatedUser = function () {
    describe("Created By user presents", function () {
        it("should check - Created By user presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactFooterInfo).text.to.contain(variables.usernameDisplay);
            browser.pause(2000);
        });
    });
};
exports.check_CreatedUser = check_CreatedUser;

var check_Modified_DateAndTime = function () {
    describe("Modifed Date and Time presents", function () {
        it("should check - Modifed Date and Time presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactFooterInfo).text.to.contain("modified "+variables.today);
            browser.pause(2000);
        });
    });
};
exports.check_Modified_DateAndTime = check_Modified_DateAndTime;

var check_Modified_User = function () {
    describe("Modified By user presents", function () {
        it("should check - Modified By user presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactFooterInfo).text.to.contain("modified "+variables.today);
            browser.expect.element(elements.contactsPageElements.contactFooterInfo).text.to.contain(variables.usernameDisplay);
            browser.pause(2000);
        });
    });
};
exports.check_Modified_User = check_Modified_User;

var create_Memo_In_Contact = function () {
    describe("Create a Memo", function () {
        it("should check - Create a Memo", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageNewMemoLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField, variables.newMemoName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.memoSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.newMemoName+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Memo_In_Contact = create_Memo_In_Contact;

var create_Task_In_Contact = function () {
    describe("Create a Task", function () {
        it("should check - Create a Task", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageNewTaskLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField, variables.newTaskName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.taskSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.newTaskName+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Task_In_Contact = create_Task_In_Contact;

var create_Case_In_Contact = function () {
    describe("Create a Case", function () {
        it("should check - Create a Case", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageNewCaseLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField, variables.newCaseName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.newCaseName+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Case_In_Contact = create_Case_In_Contact;

var create_Opportunity_In_Contact = function () {
    describe("Create a Opportunity", function () {
        it("should check - Create a Opportunity", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactPageNewOpporLink);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityNameInputField);
            browser.pause(2000);
            variables.createdOpportunity = variables.opportunityName;
            browser.setValue(elements.opportunityPageElements.opportunityNameInputField,variables.opportunityName);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesTerritorySelect);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesRepSelect);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityCloseDateOption);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveAndBackBtn);
            browser.expect.element('//a[contains(@title,"'+variables.createdOpportunity+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_Opportunity_In_Contact = create_Opportunity_In_Contact;

var modify_Contact_To_Provider = function () {
    describe("Change Contact to Provider Record type", function () {
        it("should check - Change Contact to Provider Record type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageTypeDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//option[contains(@value,"Provider")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsPageSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactSpecialtiesDiv).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.modify_Contact_To_Provider = modify_Contact_To_Provider;

var modify_Provider_To_Contact = function () {
    describe("Change Provider to Contact Record type", function () {
        it("should check - Change Provider to Contact Record type", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactPageTypeDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//option[contains(@value,"Contact")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsPageSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactSpecialtiesDiv).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.modify_Provider_To_Contact = modify_Provider_To_Contact;

var check_Contact_Address_Subtab = function () {
    describe("Address Subtab is highlighted", function () {
        it("should check - Address Subtab is highlighted", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactAddressTab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Address_Subtab = check_Contact_Address_Subtab;

var navigate_To_Contact_Address_Tab = function () {
    describe(" Navigate to the Address Sub-tab in Edit Mode", function () {
        it("should navigate to the Address Sub-tab in Edit Mode", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAddressTab);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSameAsMailingCheckbox);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Contact_Address_Tab = navigate_To_Contact_Address_Tab;

var navigate_To_Contact_Activity_Tab = function () {
    describe(" Navigate to the Activity Sub-tab in Edit Mode", function () {
        it("should navigate to the Activity Sub-tab in Edit Mode", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactActivitiesSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Contact_Activity_Tab = navigate_To_Contact_Activity_Tab;

var navigate_To_Contact_Message_Tab = function () {
    describe(" Navigate to the Message Sub-tab in Edit Mode", function () {
        it("should navigate to the Message Sub-tab in Edit Mode", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Contact_Message_Tab = navigate_To_Contact_Message_Tab;

var navigate_To_Contact_Attachment_Tab = function () {
    describe(" Navigate to the Address Sub-tab in Edit Mode", function () {
        it("should navigate to the Address Sub-tab in Edit Mode", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(1000);
            var contactName = variables.contactLName+", "+variables.contactFName+" "+variables.contactMName;
            commands.checkAndPerform('click', browser,'//a[contains(@title,"'+contactName+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentsSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Contact_Attachment_Tab = navigate_To_Contact_Attachment_Tab;

var navigate_To_Contact_Campaigns_Tab = function () {
    describe(" Navigate to the Campaigns Sub-tab in Edit Mode", function () {
        it("should navigate to the Campaigns Sub-tab in Edit Mode", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsTabLink);
            browser.pause(1000);
            var contactName = variables.contactLName+", "+variables.contactFName+" "+variables.contactMName;
            commands.checkAndPerform('click', browser,'//a[contains(@title,"'+contactName+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCampaignsSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Contact_Campaigns_Tab = navigate_To_Contact_Campaigns_Tab;

var add_Contact_Address_Data = function () {
    describe("Adding contact address data", function () {
        it("should check - whether added address data is saved properly or not", function (browser) {
            browser.pause(2000);
            // Add Mailing Address Street 1
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressStreet1Input, variables.contactMailingStreet1);

            // Add Mailing Address Street 2
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressStreet2Input, variables.contactMailingStreet2);

            // Add Mailing Address City
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressCityInput, variables.contactMailingCity);

            // Add Mailing Address State
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressStateInput, variables.contactMailingState);

            // Add Mailing Address Postal Code
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressPostalInput, variables.contactMailingPostal);

            // Add Mailing Address Country
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactMailingAddressCountryInput, variables.contactMailingCountry);

            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactsPageSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressStreet1Div).text.to.contain(variables.contactMailingStreet1);

            browser.pause(2000);
        });
    });
};
exports.add_Contact_Address_Data = add_Contact_Address_Data;

var check_Edit_Mailing_Address = function () {
    describe("Edit mailing address", function () {
        it("should effect on billing address", function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactEditBtn);

            // Check 'Same As' toggle - confirm Billing Address is populated with Mailing Address Data
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSameAsMailingCheckbox);

            // Edit Mailing Address Street 1 - Billing Changes
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressStreet1Div).text.to.contain(variables.contactMailingStreet1);

            // Edit Mailing Address Street 2 - Billing Changs
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressStreet2Div).text.to.contain(variables.contactMailingStreet2);

            // Edit Mailing Address City - Billing Changs
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressCityDiv).text.to.contain(variables.contactMailingCity);


            // Edit Mailing Address State - Billing Changs
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressStateDiv).text.to.contain(variables.contactMailingState);


            // Edit Mailing Address Postal Code - Billing Changs
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressPostalDiv).text.to.contain(variables.contactMailingPostal);

            // Edit Mailing Address Country - Billing Changs
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactBillingAddressCountryDiv).text.to.contain(variables.contactMailingCountry);

            browser.pause(2000);
        });
    });
};
exports.check_Edit_Mailing_Address = check_Edit_Mailing_Address;

var check_Edit_Billing_Address = function () {
    describe("Edit billing address", function () {
        it("should effect on mailing address", function (browser) {

            browser.pause(2000);
            // Uncheck 'Same As' toggle
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSameAsMailingCheckbox);

            // Edit Billing Address Street 1 - Mailing Address Persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressStreet1Input);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressStreet1Input, variables.contactBillingStreet1);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressStreet1Input).to.have.value.to.contain(variables.contactMailingStreet1);

            // Edit Billing Address Street 2 - Mailing Address Persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressStreet2Input);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressStreet2Input, variables.contactBillingStreet2);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressStreet2Input).to.have.value.to.contain(variables.contactMailingStreet2);

            // Edit Billing Address City - Mailing Address Persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressCityInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressCityInput, variables.contactBillingCity);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressCityInput).to.have.value.to.contain(variables.contactMailingCity);

            // Edit Billing Address State - Mailing Address persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressStateInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressStateInput, variables.contactBillingState);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressStateInput).to.have.value.to.contain(variables.contactMailingState);

            // Edit Billing Address Postal Code - Mailing Address persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressPostalInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressPostalInput, variables.contactBillingPostal);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressPostalInput).to.have.value.to.contain(variables.contactMailingPostal);


            // Edit Billing Address Country - Mailing Address persists
            browser.pause(1000);
            commands.checkAndPerform('clearValue', browser,elements.contactsPageElements.contactBillingAddressCountryInput);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactBillingAddressCountryInput, variables.contactBillingCountry);
            browser.keys(browser.Keys.TAB);
            browser.expect.element(elements.contactsPageElements.contactMailingAddressCountryInput).to.have.value.to.contain(variables.contactMailingCountry);

            browser.pause(2000);
        });
    });
};
exports.check_Edit_Billing_Address = check_Edit_Billing_Address;

var save_Contact = function () {
    describe("Confirm that message prompts to save before exiting screene", function () {
        it("should check -  Click Organization tab - Confirm that message prompts to save before exiting screene", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactSavePopup).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactSavePopUpYesOption);
            browser.pause(2000);
        });
    });
};
exports.save_Contact = save_Contact;

var check_Organization_subTab = function () {
    describe("Organization Subtab is highlighted", function () {
        it("should check - Organization Subtab is highlighted", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgSubtab);
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactOrgSubtab).to.have.attribute('class').which.contain('selected');
            browser.pause(2000);
        });
    });
};
exports.check_Organization_subTab = check_Organization_subTab;

var check_Filter_Elements = function () {
    describe("Display Columns, Quick filter and Quick filters present", function () {
        it("should check - Display Columns, Quick filter and Quick filters present", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgQuickFilterLink);
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactOrgFilterByDropdown).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactOrgFilterOperatorDropdown).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactOrgFilterAvailColumns).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactOrgFilterSelectedColumns).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactOrgFilterValueInput).to.be.present;
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgQuickFilterLink);
            browser.pause(2000);

        });
    });
};
exports.check_Filter_Elements = check_Filter_Elements;

var check_Org_Search_Box = function () {
    describe("Organization Search Box Presents", function () {
        it("should check - Organization Search Box Presents", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactOrgSearchBoxInput).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Org_Search_Box = check_Org_Search_Box;

var check_Adding_Organization = function () {
    describe("Adding organization from searchbox", function () {
        it("should add organization in custom list", function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactOrgSearchBoxInput, variables.createdORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgContactAssociateBtn);
            browser.expect.element('//a[contains(@title,"'+variables.createdORG+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Adding_Organization = check_Adding_Organization;

var create_New_Org_From_Contact = function () {
    describe("Creating new organization from organization subtab", function () {
        it("should create and add organization in custom list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgCreateOrgLink);
            browser.pause(1000);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactOrgCreatePopupOrgNameInput, variables.newOrgNameContact);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgCreatePopupNextStepBtn);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactOrgContactAssociateBtn);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.newOrgNameContact+'")]').to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_New_Org_From_Contact = create_New_Org_From_Contact;

var check_Contact_Activity_List = function () {
    describe("Ensure that the activities list is displayed properly", function () {
        it("should check - Ensure that the activities list is displayed properly", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactActivitiesSubTab);
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactActivityTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Activity_List = check_Contact_Activity_List;

var check_Contact_Activity_Edit_Col = function () {
    describe("edit icon is displayed if the user has write permission", function () {
        it("should check - edit icon is displayed if the user has write permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Activity_Edit_Col = check_Contact_Activity_Edit_Col;

var check_Contact_Activity_Delete_Col = function () {
    describe("delete icon is displayed if the user has del permission", function () {
        it("should check - delete icon is displayed if the user has del permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Activity_Delete_Col = check_Contact_Activity_Delete_Col;

var check_Contact_Activity_Edit_Col_Fail = function () {
    describe("edit icon is displayed without write permission (fail)", function () {
        it("should check - edit icon is displayed without write permission (fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('edit')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Activity_Edit_Col_Fail = check_Contact_Activity_Edit_Col_Fail;

var check_Contact_Activity_Delete_Col_Fail = function () {
    describe(" del icon is displayed without del permission (fail)", function () {
        it("should check -  del icon is displayed without del permission (fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactActivityTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Activity_Delete_Col_Fail = check_Contact_Activity_Delete_Col_Fail;

var check_Contact_UploadAttachment = function () {
    describe("Add attachment", function () {
        it("should check - upload attahcment is present or not", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentsSubTab);
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactAttachmentUploadLink).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentUploadLink);
            browser.pause(2000);

            //upload a test file
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactAttachmentUploadPopupNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactAttachmentUploadPopupFilenameInput, variables.testFileUploadPath);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.newUploadName+"']").to.be.present;
            browser.pause(1000);
            //check download attachment
            browser.elements("xpath",elements.contactsPageElements.contactAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('downloadAttachment')).to.be.true;
                        }
                    });
                });
            });
            browser.getAttribute("//a[contains(@title,'"+variables.testFileUploadName+"')]","href",function (re) {
                console.log('>>>>>href : '+re.value);
                expect(re.value).to.include('download');
            });
        });
    });
};
exports.check_Contact_UploadAttachment = check_Contact_UploadAttachment;

var check_Contact_UploadAttachmentFail = function () {
    describe("Add an attachment with User without write Attachment permission (Fail)", function () {
        it("should check - Add an attachment with User without write Attachment permission (Fail)", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentsSubTab);
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactAttachmentUploadLink).to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Contact_UploadAttachmentFail = check_Contact_UploadAttachmentFail;

var check_Contact_DeleteAttachmentFail = function () {
    describe("Delete an Attachment with user without Delete Attachment permissions (Fail)", function () {
        it("should check - Delete an Attachment with user without Delete Attachment permissions (Fail)", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.false;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_DeleteAttachmentFail = check_Contact_DeleteAttachmentFail;

var check_Contact_DeleteAttachment = function () {
    describe("Delete an Attachment with user with Delete Attachment permissions", function () {
        it("should check - Delete an Attachment with user with Delete Attachment permissions", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactAttachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);

            //delete the attachment
            browser.elements("xpath", elements.contactsPageElements.contactAttachmentsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        console.log('>>>>> table data : '+text.value);
                        if(text.value.includes(variables.newUploadName)){
                            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactAttachmentsTable+'/tbody/tr[1]/td[5]/i');
                            browser.pause(3000);
                            browser.expect.element("//a[contains(@title,'"+variables.newUploadName+"')]").to.not.be.present;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Contact_DeleteAttachment = check_Contact_DeleteAttachment;

var check_Contact_attachmentRequiredFields = function () {
    describe("Name and file are required fields", function () {
        it("should check - Name and file are required fields", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentUploadLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentPopupUploadBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactAttachmentsNameErrorLabel).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactAttachmentsFilenameErrorLabel).to.be.present;
            browser.expect.element(elements.contactsPageElements.contactAttachmentUploadPhiCheckbox).to.be.present;
            commands.checkAndSetValue(browser,elements.contactsPageElements.contactAttachmentUploadPopupDescInput, variables.contactAttachmentDesc);

            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactAttachmentPopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Contact_attachmentRequiredFields = check_Contact_attachmentRequiredFields;

var check_Contact_messageList = function () {
    describe("Enure that message list view is displayed properly", function () {
        it("should check - Enure that message list view is displayed properly", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactMessagesSubTab);
            browser.expect.element(elements.contactsPageElements.contactMessageTable).to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Contact_messageList = check_Contact_messageList;

var check_Contact_CampaignSubtab = function () {
    describe("Ensure that campaign subtab is displayed", function () {
        it("should check -  Ensure that campaign subtab is displayed", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactCampaignsSubTab).to.be.present;
            commands.checkAndPerform('click', browser,elements.contactsPageElements.contactCampaignsSubTab);
            browser.pause(2000);
        });
    });
};
exports.check_Contact_CampaignSubtab = check_Contact_CampaignSubtab;

var check_Contact_Campaigns_List = function () {
    describe("Ensure that all the campaigns that the contact is associated with is displayed in the campaign subtab", function () {
        it("should check - Ensure that all the campaigns that the contact is associated with is displayed in the campaign subtab", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactCampaignsTable).to.be.present;
            browser.expect.element("//a[contains(@title,'"+variables.createdCampaignContact+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Contact_Campaigns_List = check_Contact_Campaigns_List;

var check_Contact_CampaignIsEditable = function () {
    describe("Edit icon is displayed if user has write permission", function () {
        it("should check - Edit icon is displayed if user has write permission", function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.contactsPageElements.contactCampaignsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.true;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Contact_CampaignIsEditable = check_Contact_CampaignIsEditable;

var edit_Campaign_From_Contact = function () {
    describe("Edit a campaign > ensure that the campaign name is displayed correctly", function () {
        it("should check - Edit a campaign > ensure that the campaign name is displayed correctly", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdCampaign+"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser,elements.campaignPageElements.campaignDetailPageNameInput);
            commands.checkAndSetValue(browser,elements.campaignPageElements.campaignDetailPageNameInput, variables.updatedCampaignName);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignSaveAndBackBtn);
            browser.pause(3000);
            browser.expect.element("//a[contains(@title,'"+variables.updatedCampaignName +"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.edit_Campaign_From_Contact = edit_Campaign_From_Contact;

var delete_Campaign_From_Contact = function () {
    describe("Delete a campaign > Ensure that the campaign is deleted in the campaign subtab", function () {
        it("should check - Delete a campaign > Ensure that the campaign is deleted in the campaign subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,"//a[contains(@title,'"+variables.updatedCampaignName +"')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignEditBtn);
            browser.pause(1000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignDetailPageDeleteBtn);
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.campaignPageElements.campaignTabLink);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.updatedCampaignName+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.delete_Campaign_From_Contact = delete_Campaign_From_Contact;


var check_Contact_Name = function () {
    describe(" Ensure contact name is displayed correctly", function () {
        it("Should check - Ensure contact name is displayed correctly", function (browser) {
            var currentContact = variables.createdContact;
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.collaborationHomeTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            browser.pause(2000);
            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
            browser.pause(2000);
            browser.elements("xpath", elements.homePageElements.cloudSearchAllTabLink, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        var allTabCount = text.value;
                        if (allTabCount > 0) {
                            browser.pause(5000);
                            browser.keys([browser.Keys.TAB]);
                            browser.expect.element(elements.contactsPageElements.collaborationHomeTabLink).to.have.attribute('class').which.contains('name');
                            browser.expect.element(elements.homePageElements.cloudSearchContactResultHeader).text.to.contain('Contacts');
                            browser.elements("xpath", elements.homePageElements.cloudSearchContactResultData, function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdText(el.ELEMENT, function (text) {
                                        var searchedContactName = text.value;
                                        expect(searchedContactName.includes(currentContact)).to.be.true;
                                    });
                                });
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
        });
    });
}
exports.check_Contact_Name = check_Contact_Name;

var check_Contact_Object_And_Redirecting = function () {
    describe("Select a Provider > Ensure user is redirected is Provider detail page  ", function () {
        it("Should check -Select a Provider > Ensure user is redirected is Provider detail page  ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.editBtnOnHostCodeCreationPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.hostCodeInput);
            commands.checkAndSetValue(browser, elements.contactsPageElements.hostCodeInput, variables.hostCodeNumber);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.saveBtnOnHostCodePage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactSettingsPopupCloseBtn);
            browser.pause(2000);
        });
    });
}
exports.check_Contact_Object_And_Redirecting = check_Contact_Object_And_Redirecting;

var create_Case_From_Contact_Cloud_Search = function () {
    describe(" Ensure user can create Case on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Case on Contact from cloud search", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            browser.pause(2000);
            var currentContact = variables.createdContact;
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.cloudSearchCaseIcon);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.caseSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
}
exports.create_Case_From_Contact_Cloud_Search = create_Case_From_Contact_Cloud_Search;

var create_Task_From_Contact_Cloud_Search = function () {
    describe(" Ensure user can create Task on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Task on Contact from cloud search", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentContact = variables.createdContact;
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.cloudSearchTaskIcon);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
}
exports.create_Task_From_Contact_Cloud_Search = create_Task_From_Contact_Cloud_Search;

var create_Memo_From_Contact_Cloud_Search = function () {
    describe(" Ensure user can create Memo on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Memo on Contact from cloud search", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentContact = variables.createdContact;
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.cloudSearchMemoIcon);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
}
exports.create_Memo_From_Contact_Cloud_Search = create_Memo_From_Contact_Cloud_Search;

var create_Opportunity_From_Contact_Cloud_Search = function () {
    describe("Ensure user can create opportunity on Contact from cloud search ", function () {
        it("should check - Ensure user can create opportunity on Contact from cloud search ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentContact = variables.createdContact;
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.cloudSearchOpportunityIcon);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityNameInputField, variables.opportunityName)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesTerritorySelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesRepSelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityCloseDateInput, variables.today);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).text.to.equal(variables.opportunityName);
            browser.pause(2000);
        });
    });
}
exports.create_Opportunity_From_Contact_Cloud_Search = create_Opportunity_From_Contact_Cloud_Search;

var check_Provider_Name = function () {
    describe(" Ensure contact name is displayed correctly", function () {
        it("Should check - Ensure contact name is displayed correctly", function (browser) {
            var currentProvider = variables.createdProvider;
            browser.pause(4000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.collaborationHomeTabLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentProvider);
            browser.pause(5000);
            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
            browser.pause(2000);
            browser.elements("xpath", elements.homePageElements.cloudSearchAllTabLink, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        var allTabCount = text.value;
                        if (allTabCount > 0) {
                            browser.pause(5000);
                            browser.keys([browser.Keys.TAB]);
                            browser.expect.element(elements.contactsPageElements.collaborationHomeTabLink).to.have.attribute('class').which.contains('name');
                            browser.expect.element(elements.homePageElements.cloudSearchProviderResultHeader).text.to.contain('Provider');
                            browser.elements("xpath", elements.homePageElements.cloudSearchProviderResultData, function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdText(el.ELEMENT, function (text) {
                                        var searchedProviderName = text.value;
                                        expect(searchedProviderName.includes(currentProvider)).to.be.true;
                                    });
                                });
                            });
                        }
                    });
                });
            });
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
        });
    });
}
exports.check_Provider_Name = check_Provider_Name;

var check_Provider_Object_And_Redirecting = function () {
    describe("Select a Provider > Ensure user is redirected is Provider detail page  ", function () {
        it("Should check -Select a Provider > Ensure user is redirected is Provider detail page  ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.editBtnOnHostCodeCreationPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.hostCodeInput);
            commands.checkAndSetValue(browser, elements.contactsPageElements.hostCodeInput, variables.hostCodeNumber);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.saveBtnOnHostCodePage);
            browser.pause(4000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactSettingsPopupCloseBtn);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.mobileNumberDiv).text.to.equal(variables.contactMobile);
            browser.expect.element(elements.contactsPageElements.phoneNumberDiv).text.to.equal(variables.contactPhone);
            browser.pause(2000);

        });
    });
}
exports.check_Provider_Object_And_Redirecting = check_Provider_Object_And_Redirecting;

var create_Case_From_Provider_Cloud_Search = function () {
    describe(" Ensure user can create Case on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Case on Provider from cloud search", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentCase = variables.createdProvider;
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentCase);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.providerCloudSearchCaseIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.caseSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
}
exports.create_Case_From_Provider_Cloud_Search = create_Case_From_Provider_Cloud_Search;

var create_Task_From_Provider_Cloud_Search = function () {
    describe(" Ensure user can create Task on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Task on Provider from cloud search", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            browser.pause(2000);
            var currentContact = variables.createdProvider;
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.providerCloudSearchTaskIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.taskSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);
        });
    });
}
exports.create_Task_From_Provider_Cloud_Search = create_Task_From_Provider_Cloud_Search;

var create_Memo_From_Provider_Cloud_Search = function () {
    describe(" Ensure user can create Memo on Contact from cloud search", function () {
        it(" Should check - Ensure user can create Memo on Provider from cloud search", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentContact = variables.createdProvider;
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentContact);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.providerCloudSearchMemoIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.activityPageElements.memoSubjectInputField, variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(2000);

        });
    });
}
exports.create_Memo_From_Provider_Cloud_Search = create_Memo_From_Provider_Cloud_Search;

var create_Opportunity_From_Provider_Cloud_Search = function () {
    describe("Ensure user can create opportunity on Contact from cloud search ", function () {
        it("should check - Ensure user can create opportunity on Contact from cloud search ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            var currentProvider = variables.createdProvider;
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.homePageElements.cloudSearchInputField);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.homePageElements.cloudSearchInputField, currentProvider);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.providerCloudSearchOpportunityIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityNameInputField, variables.opportunityName)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesTerritorySelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesRepSelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.opportunityPageElements.opportunityCloseDateInput, variables.today);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
            // browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).text.to.equal(variables.opportunityName);
            browser.pause(2000);
        });
    });
}
exports.create_Opportunity_From_Provider_Cloud_Search = create_Opportunity_From_Provider_Cloud_Search;


//TEST CASE DECLARATION - END











//FUNCTIONS DECLARATION - START

var contact_CreateContact = function () {
    describe("CONTACTS - CREATE CONTACT", function () {
        // orgOperations.create_New_Org();
        me.create_Contact_Advance('contact');   // Ensure that new contact modal is displayed properly
                                                // Ensure Contact radio button is selected
                                                // Ensure that user can select the contact type from the contact type drop down
                                                // Ensure that user can select the salutation from the drop down
                                                // First name is a required field
                                                // Enter First Name
                                                // Ensure that existing contacts search behaves properly
                                                // Enter Middle Name
                                                // Last Name is required field
                                                // Add Suffix
                                                // Add Email
                                                // Add Phone
                                                // Add Mobile
                                                // Ensure that Organization is a required field
                                                // Search for a organization
                                                // Ensure that the organization search behaves properly
                                                // Create new organization - Ensure that the contact is saved correctly
                                                // Check "Copy the organization's mailing address to this record" checkbox
                                                // Check "Make important contact" checkbox
                                                // Ensure that a error message is displayed if the user's saves without entering the required fields
                                                // Enter all the required fields and select Quick create - Ensure that the contact is created successfully
                                                // Enter all the required fields and select Create - Ensue that the contact is saved successfully
                                                // Ensure that the user is brought to the contact detail page
        orgOperations.navigate_To_New_Org_Page();
        me.check_Primary_Contact();             // Ensure that the contact is added as the primary contact on the organization

    });
};
exports.contact_CreateContact = contact_CreateContact;

var contact_DetailScreen_ProfilePic = function () {
    describe("CONTACTS - DETAIL SCREEN", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Upload_Pic_Permissions();      // Users should be able to upload profile pictures for a contact if the user has upload photo permission
        me.check_Resize_Pic_Permissions();      // Users should be able to resize the pictures
        me.check_Edit_Pic_Permissions();        // User should be able to edit  the profile picture if the user has edit permissions
        me.check_Delete_Pic_Permission();       // Users should be able to delete the profile picture if the user has delete permission
        me.check_Cancel_Pic_Permissions();      // User should be able to cancel the changes
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Contact();
        // me.check_Upload_Pic_Permissions_Fail();      // upload profile pictures for a contact without upload photo permission (fail)
        // me.check_Delete_Pic_Permissions_Fail();      // Delete profile picture without delete permission (fail)
        // me.check_Edit_Pic_Permissions_Fail();        // Edit Profile picture without edit permission (fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

    });
};
exports.contact_DetailScreen_ProfilePic = contact_DetailScreen_ProfilePic;

var contact_SettingsIcon_And_VCard = function () {
    describe("CONTACT - SETTINGS ICON AND VCARD", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_HostCode_Permissions();         // Users with Host Code permissions can Add / Delete Host Codes
        me.check_VCard_ContactType();            // Ensure that the contact type is present
        me.check_VCard_ContactOrg_Display();     // Ensure that the organizations that the contact is associated with is displayed in the vcard
        me.check_VCard_HostCode();               // Ensure that the contact hostcode is present in the vcard
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Contact();
        // me.check_HostCode_Permissions_Fail();    // Users without Host Code Permissions have read-only Access to host codes
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.contact_SettingsIcon_And_VCard = contact_SettingsIcon_And_VCard;

var contact_General_SubTab = function () {
    describe("CONTACT - GENERAL SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_Subtabs_Display();             // Subtabs General, Address, Org, Create User, Activties, Orders and Attachments are displayed
        me.check_General_Tab();                         // General Subtab is highlighted
        me.check_Edit_Mode_Write_Permissions();         // Selecting a Record name Enters in Edit mode (If user has write permissions)
        //this is covered in check_General_Tab          // Create a New Contact - Detail Screen presents the General Sub-Tab
        me.check_ContactIcon_Display();                 // Contact Icon Displays on Page
        me.add_ContactData('contact');                 // Select a Salutation
                                                        // Add First Name (Required)
                                                        // Add Middle name
                                                        // Add Last Name (required
                                                        // Add suffix
                                                        // Add email address
                                                        // Add phone
                                                        // Add Mobile phone
                                                        // Add Other phone
                                                        // Add fax
                                                        // Select 'Email opt out'
                                                        // Perferred communication - Select Email
                                                        // Preferred communication - Select Text
                                                        // Campaign email opt out - Unsubcribe from a campaign, ensure that the email opt out checkbox is checked
                                                        // Select Text opt out
                                                        // Campaign text opt out - Unsubcribe from a campaign text, Ensure that the text opt out is checked
                                                        // Select Do Not Call  checkbox
                                                        // Select  Customer  checkbox
                                                        // Select  Lead  checkbox
                                                        // Add Job Title
                                                        // Add Department
                                                        // Add Credentials
        me.edit_ContactData();                          // Edit First Name (Required)
                                                        // Edit Middle name
                                                        // Edit Last Name (required
                                                        // Edit suffix
                                                        // Edit email address
                                                        // Edit phone
                                                        // Edit Mobile phone
                                                        // Edit Other phone
                                                        // Edit fax
                                                        // uncheck 'Email opt out'
                                                        // uncheck   Do Not Call  checkbox
                                                        // uncheck  Customer  checkbox
                                                        // uncheck  Lead  checkbox
                                                        // Edit Job Title
                                                        // Edit Department
                                                        // Edit Credentials
                                                        // Delete an Attachment
                                                        // Cancel button does not commit changes
                                                        // Save Button commits changes
                                                        // Save and Back button commits changes and returns to the previous screen
        me.check_DateAndTime();                         // Create Date and Time presents
        me.check_CreatedUser();                         // Created By user presents
        me.check_Modified_DateAndTime();                // Modified Date and Time presents
        me.check_Modified_User();                       // Modified By user presents
        me.create_Memo_In_Contact();                    // Create a Memo
        me.check_Breadcrumb_Trail();                    // Breadcrumb trail captures navigation history
        me.create_Task_In_Contact();                    // Create a Task
        me.create_Case_In_Contact();                    // Create a Case
        me.create_Opportunity_In_Contact();             // Create an Opportunity
        //will be covered in create_Opportunity_In_Contact // Opportunities appear in Activity List
        me.modify_Contact_To_Provider();                // Change Contact to Provider Record type
        //this will be covered in attachments subtab    // Add an Attachment
        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_New_Contact();
        // me.check_Edit_Mode_Read_Permissions();          // Selecting a Record name enters in View mode, if user has read-only permissions

    });
};
exports.contact_General_SubTab = contact_General_SubTab;

var contact_Address_SubTab = function () {
    describe("CONTACT - ADDRESS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_Subtabs_Display();         // Subtabs General, Address, Org, Settings, Activitie, Orders and Attachments are displayed
        me.navigate_To_Contact_Address_Tab();       // Navigate to the Address Sub-tab in Edit Mode
        me.check_Contact_Address_Subtab();          // Address Subtab is highlighted
        me.add_Contact_Address_Data();              // Add Mailing Address Street 1
                                                    // Add Mailing Address Street 2
                                                    // Add Mailing Address City
                                                    // Add Mailing Address State
                                                    // Add Mailing Address Postal Code
                                                    // Add Mailing Address Country
        me.check_Edit_Mailing_Address();            // Check 'Same As' toggle - confirm Billing Address is populated with Mailing Address Data
                                                    // Edit Mailing Address Street 1 - Billing Changs
                                                    // Edit Mailing Address Street 2 - Billing Changs
                                                    // Edit Mailing Address City - Billing Changs
                                                    // Edit Mailing Address State - Billing Changs
                                                    // Edit Mailing Address Postal Code - Billing Changs
                                                    // Edit Mailing Address Country - Billing Changs
        me.check_Edit_Billing_Address();            // Uncheck 'Same As' toggle
                                                    // Edit Billing Address Street 1 - Mailing Address Persists
                                                    // Edit Billing Address Street 2 - Mailing Address Persists
                                                    // Edit Billing Address City - Mailing Address Persists
                                                    // Edit Billing Address State - Mailing Address persists
                                                    // Edit Billing Address Postal Code - Mailing Address persists
                                                    // Edit Billing Address Country - Mailing Address persists
        me.save_Contact();                          // Click Organization tab - Confirm that message prompts to save before exiting screene
    });
};
exports.contact_Address_SubTab = contact_Address_SubTab;

var contact_Organization_SubTab = function () {
    describe("CONTACT - ORGANIZATION SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_Subtabs_Display();     // Subtabs General, Address, Org, Settings, Activities are displayed
        me.check_Organization_subTab();         // Organization Subtab is highlighted
        me.check_Filter_Elements();             // Display Columns, Quick filter and Quick filters present
        me.check_Org_Search_Box();              // Organization Search Box Presents
        me.check_Adding_Organization();         // Selecting and Organization from Search results adds the Organization to the Organization List
                                                // Selecting an Organization Name re-directs to the Org Detail Screen
        me.create_New_Org_From_Contact();       // Create a New Organization
                                                // Ensure that the organization is displayed in list view
    });
};
exports.contact_Organization_SubTab = contact_Organization_SubTab;

var contact_Organization_CustomList = function () {
    describe("CONTACT - ORGANIZATION CUSTOM LIST", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact_Org_Subtab();

        commonOperations.check_Filters_Behave_Properly(elements.contactsPageElements.contactOrgQuickFilterLink,
            elements.contactsPageElements.contactOrgTable,
            elements.contactsPageElements.contactOrgFilterAvailColumns,
            elements.contactsPageElements.contactOrgFilterSelectedColumns,
            elements.contactsPageElements.contactOrgFilterAddColBtn,
            elements.contactsPageElements.contactOrgFilterRemoveColBtn,
            elements.contactsPageElements.contactOrgFilterApplyBtn);       //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.contactsPageElements.contactOrgSettingsIcon,
            elements.contactsPageElements.contactOrgTable,
            elements.contactsPageElements.contactOrgCustomListAvailColumns,
            elements.contactsPageElements.contactOrgCustomListSelectedColumns,
            elements.contactsPageElements.contactOrgCustomListNewLink,
            elements.contactsPageElements.contactOrgCustomListAddColBtn,
            elements.contactsPageElements.contactOrgCustomListRemoveColBtn,
            elements.contactsPageElements.contactOrgCustomListNewNameInput);             // Ensure that activities Display Columns behaves properly (check with all the fields)


            commonOperations.create_New_Custom_List(elements.contactsPageElements.contactOrgSettingsIcon,
            elements.contactsPageElements.contactOrgCustomListNewLink,
            elements.contactsPageElements.contactOrgCustomListOrderLink,
            elements.contactsPageElements.contactOrgCustomListNewNameInput,
            elements.contactsPageElements.contactOrgCustomListNewListSaveBtn,
            elements.contactsPageElements.contactOrgCustomListOrderDiv,
            elements.contactsPageElements.contactOrgCustomListOrderCloseBtn,
            elements.contactsPageElements.contactOrgCustomListAddColBtn,
            elements.contactsPageElements.contactOrgCustomListRemoveColBtn,
            elements.contactsPageElements.contactOrgCustomListAvailColumns,
            elements.contactsPageElements.contactOrgCustomListSelectedColumns,
            elements.contactsPageElements.contactOrgTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.contactsPageElements.contactOrgSettingsIcon,
            elements.contactsPageElements.contactOrgCustomListEditLink,
            elements.contactsPageElements.contactOrgCustomListNewNameInput,
            elements.contactsPageElements.contactOrgCustomListNewListSaveBtn,
            elements.contactsPageElements.contactOrgCustomListOrderLink,
            elements.contactsPageElements.contactOrgCustomListOrderDiv,
            elements.contactsPageElements.contactOrgCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.contactsPageElements.contactOrgSettingsIcon,
            elements.contactsPageElements.contactOrgCustomListCloneInput,
            elements.contactsPageElements.contactOrgCustomListCloneLink,
            elements.contactsPageElements.contactOrgCustomListCloneCreateBtn,
            elements.contactsPageElements.contactOrgCustomListOrderLink,
            elements.contactsPageElements.contactOrgCustomListOrderDiv,
            elements.contactsPageElements.contactOrgCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.contactsPageElements.contactOrgSettingsIcon,
            elements.contactsPageElements.contactOrgCustomListEditLink,
            elements.contactsPageElements.contactOrgCustomListDeleteLink,
            elements.contactsPageElements.contactOrgCustomListOrderLink,
            elements.contactsPageElements.contactOrgCustomListOrderDiv,
            elements.contactsPageElements.contactOrgCustomListOrderCloseBtn);// delete a custom list
    });
};
exports.contact_Organization_CustomList = contact_Organization_CustomList;

var contact_Activities_SubTab = function () {
    describe("CONTACT - ACTIVITIES SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_Activity_List();       // Ensure that the activities list is displayed properly
                                                // Ensure that all the activities ceated on the contact are displayed in  the list
        me.check_Contact_Activity_Edit_Col();   // edit icon is displayed if the user has write permission
        me.check_Contact_Activity_Delete_Col(); // delete icon is displayed if the user has del permission



        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        //
        // me.check_Contact_Activity_Edit_Col_Fail();   // edit icon is displayed without write permission (fail)
        // me.check_Contact_Activity_Delete_Col_Fail(); // del icon is displayed without del permission (fail)
        //
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);

    });
};
exports.contact_Activities_SubTab = contact_Activities_SubTab;

var contact_Activities_SubTabCustomList = function () {
    describe("CONTACT ACTIVITY SUBTAB CUSTOM LIST", function () {
        me.navigate_To_New_Contact();
        me.navigate_To_Contact_Activity_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.contactsPageElements.contactActivityQuickFilterLink,
            elements.contactsPageElements.contactActivityTable,
            elements.contactsPageElements.contactActivityFilterAvailColumns,
            elements.contactsPageElements.contactActivityFilterSelectedColumns,
            elements.contactsPageElements.contactActivityFilterAddColBtn,
            elements.contactsPageElements.contactActivityFilterRemoveColBtn,
            elements.contactsPageElements.contactActivityFilterApplyBtn);       //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.contactsPageElements.contactActivitySettingsIcon,
            elements.contactsPageElements.contactActivityTable,
            elements.contactsPageElements.contactActivityCustomListAvailColumns,
            elements.contactsPageElements.contactActivityCustomListSelectedColumns,
            elements.contactsPageElements.contactActivityCustomListNewLink,
            elements.contactsPageElements.contactActivityCustomListAddColBtn,
            elements.contactsPageElements.contactActivityCustomListRemoveBtn,
            elements.contactsPageElements.contactActivityCustomListNewInput);             // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.contactsPageElements.contactActivitySettingsIcon,
            elements.contactsPageElements.contactActivityCustomListNewLink,
            elements.contactsPageElements.contactActivityCustomListOrderLink,
            elements.contactsPageElements.contactActivityCustomListNewInput,
            elements.contactsPageElements.contactActivityCustomListNewSaveBtn,
            elements.contactsPageElements.contactActivityCustomListOrderDiv,
            elements.contactsPageElements.contactActivityCustomListOrderCloseBtn,
            elements.contactsPageElements.contactActivityCustomListAddColBtn,
            elements.contactsPageElements.contactActivityFilterRemoveColBtn,
            elements.contactsPageElements.contactActivityCustomListAvailColumns,
            elements.contactsPageElements.contactActivityCustomListSelectedColumns,
            elements.contactsPageElements.contactActivityTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.contactsPageElements.contactActivitySettingsIcon,
            elements.contactsPageElements.contactActivityCustomListEditLink,
            elements.contactsPageElements.contactActivityCustomListNewInput,
            elements.contactsPageElements.contactActivityCustomListNewSaveBtn,
            elements.contactsPageElements.contactActivityCustomListOrderLink,
            elements.contactsPageElements.contactActivityCustomListOrderDiv,
            elements.contactsPageElements.contactActivityCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.contactsPageElements.contactActivitySettingsIcon,
            elements.contactsPageElements.contactActivityCustomListCloneInput,
            elements.contactsPageElements.contactActivityCustomListCloneLink,
            elements.contactsPageElements.contactActivityCustomListCloneCreateBtn,
            elements.contactsPageElements.contactActivityCustomListOrderLink,
            elements.contactsPageElements.contactActivityCustomListOrderDiv,
            elements.contactsPageElements.contactActivityCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.contactsPageElements.contactActivitySettingsIcon,
            elements.contactsPageElements.contactActivityCustomListEditLink,
            elements.contactsPageElements.contactActivityCustomListDeleteLink,
            elements.contactsPageElements.contactActivityCustomListOrderLink,
            elements.contactsPageElements.contactActivityCustomListOrderDiv,
            elements.contactsPageElements.contactActivityCustomListOrderCloseBtn);// delete a custom list
    });
};
exports.contact_Activities_SubTabCustomList = contact_Activities_SubTabCustomList;

var contact_Attachments_SubTab = function () {
    describe("CONTACT - ATTACHMENTS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_UploadAttachment();           // Add an attachment with User with write Attachment permission
                                                       // Ensure that attachment pop-up is displayed

        me.check_Contact_DeleteAttachment();           // Delete an Attachment with user with Delete Attachment permissions
        me.check_Contact_attachmentRequiredFields();   // Name and file are required fields
                                                       // Add description
                                                       // Phi is checkbox is present
                                                       // Add a attachment with phi

        // userAuthentication.logout();
        // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        // me.navigate_To_Contact_Attachment_Tab();
        // me.check_Contact_UploadAttachmentFail();       // Add an attachment with User without write Attachment permission (Fail)
        // me.check_Contact_DeleteAttachmentFail();       // Delete an Attachment with user without Delete Attachment permissions (Fail)
        // userAuthentication.logout();
        // userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.contact_Attachments_SubTab = contact_Attachments_SubTab;

var contact_Attachments_SubTab_CustomList = function () {
    describe("CONTACT ATTACHMENTS SUBTAB - CUSTOM LIST", function () {
        me.navigate_To_New_Contact();
        me.navigate_To_Contact_Attachment_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.contactsPageElements.contactAttachmentQuickFilterLink,
            elements.contactsPageElements.contactAttachmentsTable,
            elements.contactsPageElements.contactAttachmentFilterAvailColumns,
            elements.contactsPageElements.contactAttachmentFilterSelectedColumns,
            elements.contactsPageElements.contactAttachmentFilterAddColBtn,
            elements.contactsPageElements.contactAttachmentFilterRemoveColBtn,
            elements.contactsPageElements.contactAttachmentFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.contactsPageElements.contactAttachmentSettingsIcon,
            elements.contactsPageElements.contactAttachmentsTable,
            elements.contactsPageElements.contactAttachmentCustomListAvailColumns,
            elements.contactsPageElements.contactAttachmentCustomListSelectedColumns,
            elements.contactsPageElements.contactAttachmentCustomListNewLink,
            elements.contactsPageElements.contactAttachmentCustomListAddColBtn,
            elements.contactsPageElements.contactAttachmentCustomListRemoveColBtn,
            elements.contactsPageElements.contactAttachmentCustomListNewInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.contactsPageElements.contactAttachmentSettingsIcon,
            elements.contactsPageElements.contactAttachmentCustomListNewLink,
            elements.contactsPageElements.contactAttachmentCustomListOrderLink,
            elements.contactsPageElements.contactAttachmentCustomListNewInput,
            elements.contactsPageElements.contactAttachmentCustomListNewSaveBtn,
            elements.contactsPageElements.contactAttachmentCustomListOrderDiv,
            elements.contactsPageElements.contactAttachmentCustomListOrderCloseBtn,
            elements.contactsPageElements.contactAttachmentCustomListAddColBtn,
            elements.contactsPageElements.contactAttachmentCustomListRemoveColBtn,
            elements.contactsPageElements.contactAttachmentCustomListAvailColumns,
            elements.contactsPageElements.contactAttachmentCustomListSelectedColumns,
            elements.contactsPageElements.contactAttachmentsTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.contactsPageElements.contactAttachmentSettingsIcon,
            elements.contactsPageElements.contactAttachmentCustomListEditLink,
            elements.contactsPageElements.contactAttachmentCustomListNewInput,
            elements.contactsPageElements.contactAttachmentCustomListNewSaveBtn,
            elements.contactsPageElements.contactAttachmentCustomListOrderLink,
            elements.contactsPageElements.contactAttachmentCustomListOrderDiv,
            elements.contactsPageElements.contactAttachmentCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.contactsPageElements.contactAttachmentSettingsIcon,
            elements.contactsPageElements.contactAttachmentCustomListCloneInput,
            elements.contactsPageElements.contactAttachmentCustomListCloneLink,
            elements.contactsPageElements.contactAttachmentCustomListCloneCreateBtn,
            elements.contactsPageElements.contactAttachmentCustomListOrderLink,
            elements.contactsPageElements.contactAttachmentCustomListOrderDiv,
            elements.contactsPageElements.contactAttachmentCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.contactsPageElements.contactAttachmentSettingsIcon,
            elements.contactsPageElements.contactAttachmentCustomListEditLink,
            elements.contactsPageElements.contactAttachmentCustomListDeleteLink,
            elements.contactsPageElements.contactAttachmentCustomListOrderLink,
            elements.contactsPageElements.contactAttachmentCustomListOrderDiv,
            elements.contactsPageElements.contactAttachmentCustomListOrderCloseBtn);// delete a custom list

        commonOperations.check_paging_display(elements.contactsPageElements.contactAttachmentPagingDiv); // Ensure that the paging is displayed correctly
    });
};
exports.contact_Attachments_SubTab_CustomList = contact_Attachments_SubTab_CustomList;

var contact_Messages_SubTab = function () {
    describe("CONTACT - MESSAGES SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        me.check_Contact_messageList();     // Enure that message list view is displayed properly
    });
};
exports.contact_Messages_SubTab = contact_Messages_SubTab;

var contact_Messages_SubTab_CustomList = function () {
    describe("CONTACT MESSAGES SUBTAB - CUSTOM LIST", function () {
        me.navigate_To_New_Contact();
        me.navigate_To_Contact_Message_Tab();
        commonOperations.check_Filters_Behave_Properly(elements.contactsPageElements.contactMessageQuickFilterLink,
            elements.contactsPageElements.contactMessageTable,
            elements.contactsPageElements.contactMessageFilterAvailColumns,
            elements.contactsPageElements.contactMessageFilterSelectedColumns,
            elements.contactsPageElements.contactMessageFilterAddColBtn,
            elements.contactsPageElements.contactMessageFilterRemoveColBtn,
            elements.contactsPageElements.contactMessageFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.contactsPageElements.contactMessageSettingsIcon,
            elements.contactsPageElements.contactMessageTable,
            elements.contactsPageElements.contactMessageCustomListAvailColumns,
            elements.contactsPageElements.contactMessageCustomListSelectedColumns,
            elements.contactsPageElements.contactMessageCustomListNewLink,
            elements.contactsPageElements.contactMessageCustomListAddColBtn,
            elements.contactsPageElements.contactMessageCustomListRemoveColBtn,
            elements.contactsPageElements.contactMessageCustomListNewInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.contactsPageElements.contactMessageSettingsIcon,
            elements.contactsPageElements.contactMessageCustomListNewLink,
            elements.contactsPageElements.contactMessageCustomListOrderLink,
            elements.contactsPageElements.contactMessageCustomListNewInput,
            elements.contactsPageElements.contactMessageCustomListNewSaveBtn,
            elements.contactsPageElements.contactMessageCustomListOrderDiv,
            elements.contactsPageElements.contactMessageCustomListOrderCloseBtn,
            elements.contactsPageElements.contactMessageCustomListAddColBtn,
            elements.contactsPageElements.contactMessageCustomListRemoveColBtn,
            elements.contactsPageElements.contactMessageCustomListAvailColumns,
            elements.contactsPageElements.contactMessageCustomListSelectedColumns,
            elements.contactsPageElements.contactMessageTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.contactsPageElements.contactMessageSettingsIcon,
            elements.contactsPageElements.contactMessageCustomListEditLink,
            elements.contactsPageElements.contactMessageCustomListNewInput,
            elements.contactsPageElements.contactMessageCustomListNewSaveBtn,
            elements.contactsPageElements.contactMessageCustomListOrderLink,
            elements.contactsPageElements.contactMessageCustomListOrderDiv,
            elements.contactsPageElements.contactMessageCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.contactsPageElements.contactMessageSettingsIcon,
            elements.contactsPageElements.contactMessageCustomListCloneInput,
            elements.contactsPageElements.contactMessageCustomListCloneLink,
            elements.contactsPageElements.contactMessageCustomListCloneCreateBtn,
            elements.contactsPageElements.contactMessageCustomListOrderLink,
            elements.contactsPageElements.contactMessageCustomListOrderDiv,
            elements.contactsPageElements.contactMessageCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.contactsPageElements.contactMessageSettingsIcon,
            elements.contactsPageElements.contactMessageCustomListEditLink,
            elements.contactsPageElements.contactMessageCustomListDeleteLink,
            elements.contactsPageElements.contactMessageCustomListOrderLink,
            elements.contactsPageElements.contactMessageCustomListOrderDiv,
            elements.contactsPageElements.contactMessageCustomListOrderCloseBtn);// delete a custom list

        commonOperations.check_paging_display(elements.contactsPageElements.contactMessagePagingDiv); // Ensure that the paging is displayed correctly
    });
};
exports.contact_Messages_SubTab_CustomList = contact_Messages_SubTab_CustomList;

var contact_Campaigns_SubTab = function () {
    describe("CONTACT - CAMPAIGNS SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        // campaignOperations.create_Campaign();
        me.navigate_To_New_Contact();
        me.check_Contact_CampaignSubtab();      // Ensure that campaign subtab is displayed
        me.check_Contact_Campaigns_List();      // Ensure that all the campaigns that the contact is asssociated with is displayed in the campaign subtab
        me.check_Contact_CampaignIsEditable();  // Edit icon is displayed if user has write permission
        me.edit_Campaign_From_Contact();        // Edit a campaign > ensure that the campaign name is displayed correctly
        me.delete_Campaign_From_Contact();      // Delete a campaign > Ensure that the campaign is deleted in the campaign subtab

    });
};
exports.contact_Campaigns_SubTab = contact_Campaigns_SubTab;

var contact_Campaigns_SubTab_CustomList = function () {
    describe("CONTACT CAMPAIGNS SUBTAB - CUSTOM LIST", function () {
        me.navigate_To_New_Contact();
        me.navigate_To_Contact_Campaigns_Tab();

        commonOperations.check_Filters_Behave_Properly(elements.contactsPageElements.contactCampaignsQuickFilterLink,
            elements.contactsPageElements.contactCampaignsTable,
            elements.contactsPageElements.contactCampaignsFilterAvailaColumns,
            elements.contactsPageElements.contactCampaignsFilterSelectedColumns,
            elements.contactsPageElements.contactCampaignsFilterAddColBtn,
            elements.contactsPageElements.contactCampaignsFilterRemoveColBtn,
            elements.contactsPageElements.contactCampaignsFilterApplyBtn);  //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.contactsPageElements.contactCampaignsSettingsIcon,
            elements.contactsPageElements.contactCampaignsTable,
            elements.contactsPageElements.contactCampaignsCustomListAvailColumns,
            elements.contactsPageElements.contactCampaignsCustomListSelectedColumns,
            elements.contactsPageElements.contactCampaignsCustomListNewLink,
            elements.contactsPageElements.contactCampaignsCustomListAddColBtn,
            elements.contactsPageElements.contactCampaignsCustomListRemoveColBtn,
            elements.contactsPageElements.contactCampaignsCustomListNewNameInput);  // Ensure that activities Display Columns behaves properly (check with all the fields)


        commonOperations.create_New_Custom_List(elements.contactsPageElements.contactCampaignsSettingsIcon,
            elements.contactsPageElements.contactCampaignsCustomListNewLink,
            elements.contactsPageElements.contactCampaignsCustomListOrderLink,
            elements.contactsPageElements.contactCampaignsCustomListNewNameInput,
            elements.contactsPageElements.contactCampaignsCustomListSaveBtn,
            elements.contactsPageElements.contactCampaignsCustomListOrderDiv,
            elements.contactsPageElements.contactCampaignsCustomListOrderCloseBtn,
            elements.contactsPageElements.contactCampaignsCustomListAddColBtn,
            elements.contactsPageElements.contactCampaignsCustomListRemoveColBtn,
            elements.contactsPageElements.contactCampaignsCustomListAvailColumns,
            elements.contactsPageElements.contactCampaignsCustomListSelectedColumns,
            elements.contactsPageElements.contactCampaignsTable);// Create new custom list

        commonOperations.edit_Custom_List(elements.contactsPageElements.contactCampaignsSettingsIcon,
            elements.contactsPageElements.contactCampaignsCustomListEditLink,
            elements.contactsPageElements.contactCampaignsCustomListNewNameInput,
            elements.contactsPageElements.contactCampaignsCustomListSaveBtn,
            elements.contactsPageElements.contactCampaignsCustomListOrderLink,
            elements.contactsPageElements.contactCampaignsCustomListOrderDiv,
            elements.contactsPageElements.contactCampaignsCustomListOrderCloseBtn);// Edit  existing custom list

        commonOperations.clone_Custom_List(elements.contactsPageElements.contactCampaignsSettingsIcon,
            elements.contactsPageElements.contactCampaignsCustomListCloneNameInput,
            elements.contactsPageElements.contactCampaignsCustomListCloneLink,
            elements.contactsPageElements.contactCampaignsCustomListCloneCreateBtn,
            elements.contactsPageElements.contactCampaignsCustomListOrderLink,
            elements.contactsPageElements.contactCampaignsCustomListOrderDiv,
            elements.contactsPageElements.contactCampaignsCustomListOrderCloseBtn);// clone custom list

        commonOperations.delete_Custom_List(elements.contactsPageElements.contactCampaignsSettingsIcon,
            elements.contactsPageElements.contactCampaignsCustomListEditLink,
            elements.contactsPageElements.contactCampaignsCustomListDeleteLink,
            elements.contactsPageElements.contactCampaignsCustomListOrderLink,
            elements.contactsPageElements.contactCampaignsCustomListOrderDiv,
            elements.contactsPageElements.contactCampaignsCustomListOrderCloseBtn);// delete a custom list

        commonOperations.check_paging_display(elements.contactsPageElements.contactCampaignsPagingDiv); // Ensure that the paging is displayed correctly
    });
};
exports.contact_Campaigns_SubTab_CustomList = contact_Campaigns_SubTab_CustomList;

var contact_Audit_Log = function () {
    describe("CONTACT - AUDIT LOG", function () {

        // orgOperations.create_New_Org();
        // me.create_Contact('contact');
        me.navigate_To_New_Contact();
        //Audit log
        commonOperations.check_auditLink(elements.auditLogElements.auditLogLink);// If the user has audit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page

        //Audit history
        commonOperations.check_auditHistoryTab(elements.contactsPageElements.contactEditBtn,
            elements.contactsPageElements.contactPageFnameInput,
            elements.contactsPageElements.contactsPageSaveBtn,
            elements.auditLogElements.auditLogLink,
            elements.auditLogElements.startDateDiv,
            elements.auditLogElements.endDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.auditLogsTable,
            elements.auditLogElements.exportLink,
            variables.updatedOrgName);

        //access history
        commonOperations.check_accessHistoryTab(elements.auditLogElements.accessHistoryTab,
            elements.auditLogElements.accessHistoryStartDateDiv,
            elements.auditLogElements.accessHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.accessHistoryRecordsTable,
            elements.auditLogElements.accessHistoryExportLink);

        // //All history
        commonOperations.check_allHistoryTab(elements.auditLogElements.allHistoryTab,
            elements.auditLogElements.allHistoryStartDateDiv,
            elements.auditLogElements.allHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.allHistoryRecordsTable,
            elements.auditLogElements.allHistoryExportLink);

        commonOperations.close_AuditLogDiv();

    });
};
exports.contact_Audit_Log = contact_Audit_Log;

var contact_CreateProvider = function () {
    describe("CONTACTS - CREATE CONTACT", function () {
        // orgOperations.create_New_Org();
        me.create_Contact_Advance('provider');  // Ensure that new provider modal is displayed properly
                                                // Ensure provider radio button is selected
                                                // Ensure that user can select the contact type from the provider type drop down
                                                // Ensure that user can select the salutation from the drop down
                                                // First name is a required field
                                                // Enter First Name
                                                // Ensure that existing contacts search behaves properly
                                                // Enter Middle Name
                                                // Last Name is required field
                                                // Add Suffix
                                                // Add Email
                                                // Add Phone
                                                // Add Mobile
                                                // Ensure that Organization is a required field
                                                // Search for a organization
                                                // Ensure that the organization search behaves properly
                                                // Create new organization - Ensure that the contact is saved correctly
                                                // Check "Copy the organization's mailing address to this record" checkbox
                                                // Check "Make important provider" checkbox
                                                // Ensure that a error message is displayed if the user's saves without entering the required fields
                                                // Enter all the required fields and select Quick create - Ensure that the provider is created successfully
                                                // Enter all the required fields and select Create - Ensue that the provider is saved successfully
                                                // Ensure that the user is brought to the provider detail page

    });
};
exports.contact_CreateProvider = contact_CreateProvider;

var contacts_Provider_General_SubTab = function () {
    describe("CONTACT - GENERAL SUBTAB", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('provider');
        me.navigate_To_New_Provider();
        me.check_Contact_Subtabs_Display();             // Subtabs General, Address, Org, Create User, Activties, Orders and Attachments are displayed
        me.check_General_Tab();                         // General Subtab is highlighted
        me.check_Edit_Mode_Write_Permissions();         // Selecting a Record name Enters in Edit mode (If user has write permissions)
        //this is covered in check_General_Tab          // Create a New provider - Detail Screen presents the General Sub-Tab
        me.check_ContactIcon_Display();                 // Provider Icon Displays on Page
        me.add_ContactData('provider');                           // Select a Salutation
                                                        // Add First Name (Required)
                                                        // Add Middle name
                                                        // Add Last Name (required
                                                        // Add suffix
                                                        // Add email address
                                                        // Add phone
                                                        // Add Mobile phone
                                                        // Add Other phone
                                                        // Add fax
                                                        // Select 'Email opt out'
                                                        // Perferred communication - Select Email
                                                        // Preferred communication - Select Text
                                                        // Campaign email opt out - Unsubcribe from a campaign, ensure that the email opt out checkbox is checked
                                                        // Select Text opt out
                                                        // Campaign text opt out - Unsubcribe from a campaign text, Ensure that the text opt out is checked
                                                        // Select Do Not Call  checkbox
                                                        // Select  Customer  checkbox
                                                        // Select  Lead  checkbox
                                                        // Add Job Title
                                                        // Add Department
                                                        // Add Credentials
        me.edit_ContactData();                          // Edit First Name (Required)
                                                        // Edit Middle name
                                                        // Edit Last Name (required
                                                        // Edit suffix
                                                        // Edit email address
                                                        // Edit phone
                                                        // Edit Mobile phone
                                                        // Edit Other phone
                                                        // Edit fax
                                                        // uncheck 'Email opt out'
                                                        // uncheck   Do Not Call  checkbox
                                                        // uncheck  Customer  checkbox
                                                        // uncheck  Lead  checkbox
                                                        // Edit Job Title
                                                        // Edit Department
                                                        // Edit Credentials
                                                        // Delete an Attachment
                                                        // Cancel button does not commit changes
                                                        // Save Button commits changes
                                                        // Save and Back button commits changes and returns to the previous screen
        me.check_DateAndTime();                         // Create Date and Time presents
        me.check_CreatedUser();                         // Created By user presents
        me.check_Modified_DateAndTime();                // Modified Date and Time presents
        me.check_Modified_User();                       // Modified By user presents
        me.create_Memo_In_Contact();                    // Create a Memo
        me.check_Breadcrumb_Trail();                    // Breadcrumb trail captures navigation history
        me.create_Task_In_Contact();                    // Create a Task
        me.create_Case_In_Contact();                    // Create a Case
        me.create_Opportunity_In_Contact();             // Create an Opportunity
        //will be covered in create_Opportunity_In_Contact // Opportunities appear in Activity List
        me.modify_Provider_To_Contact();                // Change Provider to Contact Record type
    });
};
exports.contacts_Provider_General_SubTab = contacts_Provider_General_SubTab;

var cloud_Search_For_contact = function () {
    describe(" SEARCH FOR CONTACT", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('contact');                               //Create new contact
        me.navigate_To_New_Contact();
        me.check_Contact_Name();                                    //Ensure that organizations that the contact is associated is displayed correctlys
        me.check_Contact_Object_And_Redirecting();                  //Ensure that email, Host code, phone number, Mobile and other phone number is displayed correctly
                                                                    //Select a contact > Ensure user is redirected is contact detail page
        me.create_Case_From_Contact_Cloud_Search();                 //Ensure user can create Case on Contact from cloud search
        me.create_Task_From_Contact_Cloud_Search();                 //Ensure user can create Task on Contact from cloud search
        me.create_Memo_From_Contact_Cloud_Search();                 //Ensure user can create Memo on Contact from cloud search
        me.create_Opportunity_From_Contact_Cloud_Search();          //Ensure user can create opportunity on Contact from cloud search

    });
}
exports.cloud_Search_For_contact = cloud_Search_For_contact;


var cloud_Search_For_provider = function () {
    describe(" SEARCH FOR CONTACT", function () {
        // orgOperations.create_New_Org();
        // me.create_Contact('provider');                                      // Create new provider
        me.navigate_To_New_Provider();
        me.check_Provider_Name();                                           //Ensure that organizations that the Provider is associated is displayed correctly
        me.check_Provider_Object_And_Redirecting();                         //Select a Provider > Ensure user is redirected is Provider detail page
                                                                            //Ensure that email, Host code, phone number, Mobile and other phone number is displayed correctly
        me.create_Case_From_Provider_Cloud_Search();                        //Ensure user can create Case on Provider from cloud search
        me.create_Task_From_Provider_Cloud_Search();                        //Ensure user can create Task on Provider from cloud search
        me.create_Memo_From_Provider_Cloud_Search();                        //Ensure user can create Memo on Provider from cloud search
        me.create_Opportunity_From_Provider_Cloud_Search();                 //Ensure user can create Opportunity on Provider from cloud search

    });
}
exports.cloud_Search_For_provider = cloud_Search_For_provider;

var contact_nonAdmin_Checks = function () {
    describe("Contact non admin checks", function () {
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_New_Contact();
        me.check_Upload_Pic_Permissions_Fail();      // upload profile pictures for a contact without upload photo permission (fail)
        me.check_Delete_Pic_Permissions_Fail();      // Delete profile picture without delete permission (fail)
        me.check_Edit_Pic_Permissions_Fail();        // Edit Profile picture without edit permission (fail)
        me.check_HostCode_Permissions_Fail();
        me.check_Edit_Mode_Read_Permissions();

        me.navigate_To_Contact_Activity_Tab();
        me.check_Contact_Activity_Edit_Col_Fail();   // edit icon is displayed without write permission (fail)
        me.check_Contact_Activity_Delete_Col_Fail(); // del icon is displayed without del permission (fail)

        me.navigate_To_Contact_Attachment_Tab();
        me.check_Contact_UploadAttachmentFail();       // Add an attachment with User without write Attachment permission (Fail)
        me.check_Contact_DeleteAttachmentFail();       // Delete an Attachment with user without Delete Attachment permissions (Fail)

        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.contact_nonAdmin_Checks = contact_nonAdmin_Checks;

var contacts_Sheet = function () {
    describe("RESOLVE CONTACT SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'createdCampaignType',
            'createdORG',
            'createdContact',
            'createdProvider',
            'createdCampaign'
        ]);
        adminOperations.create_CampaignType();
        orgOperations.create_New_Org();
        me.create_Contact('contact');
        me.create_Contact('provider');
        campaignOperations.create_Campaign('contact',true);
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        me.contact_CreateContact();
        me.contact_Address_SubTab();
        me.contact_Organization_SubTab();
        me.contact_Activities_SubTab();
        me.contact_Attachments_SubTab();
        me.contact_Messages_SubTab();
        me.contact_Campaigns_SubTab();   //check
        me.contact_Audit_Log();
        me.contact_CreateProvider();
        me.contacts_Provider_General_SubTab();
        me.contact_DetailScreen_ProfilePic();
        me.contact_SettingsIcon_And_VCard();
        me.contact_General_SubTab();

        //test case which require non admin user
        me.contact_nonAdmin_Checks();

        //test suites having custom list tests only
        me.contact_Organization_CustomList();
        me.contact_Activities_SubTabCustomList();
        me.contact_Attachments_SubTab_CustomList();
        me.contact_Messages_SubTab_CustomList();
        me.contact_Campaigns_SubTab_CustomList();
        //SHEET EXECUTION - ENDS

    });
};
exports.contacts_Sheet = contacts_Sheet;

//FUNCTIONS DECLARATION - END






