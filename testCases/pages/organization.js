/**
 * Created by INDUSA
 */
require('mocha');
var expect = require('chai').expect;
var me = require('./organization.js');
var adminOperations = require('./admin.js');
var messageOperations = require('./message.js');
var contactOperations = require('./contacts.js');
var navigateOperations = require('../utility/navigate.js');
var commonOperations = require('./commons.js');
var userAuthentication = require('./userAuthentication.js');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var navigate = require('../utility/navigate.js');
var elements = require('../utility/elements.js');
var error = require('../utility/error.js');
var orgNameCreated = '';


//TEST CASE DECLARATION - STARTS
var navigate_To_Org_Tab = function () {
    describe("Clicking the Organization Tab navigates to the Organization List Screen",function () {
        it("should check - Clicking the Organization Tab navigates to the Organization List Screen",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationTabLink);
            browser.expect.element(elements.organizationPageElements.pageTitle).text.to.equal('Organizations');
            browser.pause(2000);
        });
    });
};
module.exports.navigate_To_Org_Tab = navigate_To_Org_Tab;

var navigate_To_New_Org_Page = function () {
    describe("Navigating to newly created organization",function () {
        it("should check - must navigate to newly created organization",function (browser) {
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            browser.expect.element(elements.organizationPageElements.pageTitle).text.to.equal('Organizations');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.createdORG + '")]');
        });
    });
};
module.exports.navigate_To_New_Org_Page = navigate_To_New_Org_Page;

var check_Org_Tab_Highlighted = function () {
    describe("Organization Tab is highlighted",function () {
        it("should check - Organization Tab is highlighted",function (browser) {
            browser.expect.element(elements.organizationPageElements.organizationTabLink).to.have.attribute('class').which.equals('organizations selected-tab');
        });
    });
};
exports.check_Org_Tab_Highlighted = check_Org_Tab_Highlighted;

var navigate_To_Attachments_Subtab = function () {
    describe("navigate to Attachments Subtab",function () {
        it("should navigate to attachments subtab",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageLink);
        });
    });
};
exports.navigate_To_Attachments_Subtab = navigate_To_Attachments_Subtab;

var navigate_To_Activities_Subtab = function () {
    describe("navigate to Activities Subtab",function () {
        it("should navigate to Activities subtab",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.activitiesSubTabBtn);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Activities_Subtab = navigate_To_Activities_Subtab;

var navigate_To_Messages_Subtab = function () {
    describe("navigate to Messages Subtab",function () {
        it("should navigate to Messages subtab",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagesPageLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Messages_Subtab = navigate_To_Messages_Subtab;

var navigate_To_Location_Subtab = function () {
    describe("navigate to Location and Information Subtab",function () {
        it("should navigate to location and information subtab",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.locationAndContactSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Location_Subtab = navigate_To_Location_Subtab;

var navigate_To_ContactsSubTab = function () {
    describe("Navigate to Contacts subtab",function () {
        it("should navigate to contacts subtab",function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.pause(1000);
        });
    });
};
exports.navigate_To_ContactsSubTab = navigate_To_ContactsSubTab;

var navigate_To_ChildOrgSubTab = function () {
    describe("Navigate to Child Org subtab",function () {
        it("should navigate to Child Org subtab",function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.childOrgSubTab);
            browser.pause(1000);
        });
    });
};
exports.navigate_To_ChildOrgSubTab = navigate_To_ChildOrgSubTab;

var navigate_To_OrdersSubTab = function () {
    describe("Navigate to Orders subtab",function () {
        it("should navigate to Orders subtab",function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersSubTabBtn);
            browser.pause(1000);
        });
    });
};
exports.navigate_To_OrdersSubTab = navigate_To_OrdersSubTab;

var check_CustomList_Defaults_To_All = function () {
    describe("Custom List is defaulted to first list in order (Defaults to All)",function () {
        it("should check - Custom List is defaulted to first list in order (Defaults to All)",function (browser) {
            browser.expect.element(elements.organizationPageElements.customListDropdown).to.be.present;
            browser.expect.element(elements.organizationPageElements.customListDropDownOption1).to.have.attribute('selected');
        });
    });
};
exports.check_CustomList_Defaults_To_All = check_CustomList_Defaults_To_All;

var check_DisplayColumns_Is_Hidden = function () {
    describe("Display Columns is hidden (Default), with Drop-down arrow",function () {
        it("should check - Display Columns is hidden (Default), with Drop-down arrow",function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.displayColumnContentDiv).to.have.css('display').which.equals('none');
        });
    });
};
exports.check_DisplayColumns_Is_Hidden = check_DisplayColumns_Is_Hidden;

var check_DisplayColumns_Is_Visible = function () {
    describe("Clicking display Column Drop Arrow exands to display available fields",function () {
        it("should check - Clicking display Column Drop Arrow exands to display available fields",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.quickFilterLink);
            browser.expect.element(elements.organizationPageElements.displayColumnContentDiv).to.have.css('display').which.equals('block');
            browser.pause(5000);
        });
    });
};
exports.check_DisplayColumns_Is_Visible = check_DisplayColumns_Is_Visible;

var check_Create_Org_User_Btn_Visible = function () {
    describe("User With Organization Write Access Can Create New Org",function () {
        it("should check - User With Organization Write Access Can Create New Org",function (browser) {
            browser.expect.element(elements.organizationPageElements.organizationCreateLink).to.be.present;
            browser.pause(5000);
        });
    });
};
exports.check_Create_Org_User_Btn_Visible = check_Create_Org_User_Btn_Visible;


var check_Create_Org_User_Btn_Hidden = function () {
    describe("Create New Button does not display for user with Read only access",function () {
        it("should check - Create New Button does not display for user with Read only access",function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.organizationCreateLink).to.not.be.present;
        });
    });
};
exports.check_Create_Org_User_Btn_Hidden = check_Create_Org_User_Btn_Hidden;

var check_Create_Org_Modal_Display = function () {
    describe("Create new organization modal is displayed",function () {
        it("should check - Create new organization modal is displayed",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationCreateLink);
            browser.expect.element(elements.organizationPageElements.orgModalDiv).to.be.present;
        });
    });
};
exports.check_Create_Org_Modal_Display = check_Create_Org_Modal_Display;

var check_Create_Org_Input_Required = function () {
    describe("Organization name is a required field",function () {
        it("should check - Organization name is a required field",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgCreateBtn);
            browser.expect.element(elements.organizationPageElements.errorDiv).to.be.present;
        });
    });
};
exports.check_Create_Org_Input_Required = check_Create_Org_Input_Required;

var check_Existing_Organization_List_Behaviour = function () {
    describe("Ensure that existing organization list behaves properly ",function () {
        it("should check - Ensure that existing organization list behaves properly ",function (browser) {
            browser.keys(browser.keys.ESCAPE);
            browser.expect.element(elements.organizationPageElements.orgListTable).to.be.present;
        });
    });
};
exports.check_Existing_Organization_List_Behaviour = check_Existing_Organization_List_Behaviour;

var check_Edit_Link_Visible = function () {
    describe("Edit Link in Grid displays for Users with Write and Delete Permissions on Org",function () {
        it("should check - Edit Link in Grid displays for Users with Write and Delete Permissions on Org",function (browser) {
            browser.expect.element(elements.organizationPageElements.orgListTable+'/thead/tr/th[1]').to.have.attribute('class').to.equals('edit');
        });
    });
};
exports.check_Edit_Link_Visible = check_Edit_Link_Visible;

var check_Delete_Link_Visible = function () {
    describe("DEL Link in Grid Displays for Users with Delete Permission on Orgs",function () {
        it("should check - DEL Link in Grid Displays for Users with Delete Permission on Orgs",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.orgListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Delete_Link_Visible = check_Delete_Link_Visible;

var check_Host_Code_Display = function () {
  describe("Host Codes display (if present)",function () {
     it("should check -  Host Codes display (if present)",function (browser) {
         commands.checkAndPerform('click',browser,elements.organizationPageElements.orgRecordHeadingSettingsIcon);
         commands.checkAndPerform('click',browser,elements.organizationPageElements.orgSettingsEditBtn);
         browser.pause(1000);
         browser.expect.element(elements.organizationPageElements.hostCodesDisplayHeader).to.be.visible;
     });
  });
};
exports.check_Host_Code_Display = check_Host_Code_Display;

var check_Host_Code_Is_Editable = function () {
  describe("Users with Host Code Permission can add / remove Host codes",function () {
     it("should check - Users with Host Code Permission can add / remove Host codes",function (browser) {
         commands.checkAndPerform('click',browser,elements.organizationPageElements.orgHostCodeInput);
         commands.checkAndSetValue(browser,elements.organizationPageElements.orgHostCodeInput,variables.hostCodeNumber);
     });
  });
};
exports.check_Host_Code_Is_Editable = check_Host_Code_Is_Editable;

var check_UAC_Node_Search_Box_Visible = function () {
    describe("UAC Node Search Box displays (with permissions)",function () {
        it("should check - UAC Node Search Box displays (with permissions)",function (browser) {
                browser.expect.element(elements.organizationPageElements.uacNodeSearchInput).to.be.present;
        });
    });
};
exports.check_UAC_Node_Search_Box_Visible = check_UAC_Node_Search_Box_Visible;

var check_UAC_Node_Search_Box_Editable = function () {
    describe("Users with UAC Permissions can Add/ remove UAC Nodes",function () {
        it("should check - Users with UAC Permissions can Add/ remove UAC Nodes",function (browser) {
            browser.elements("xpath", elements.organizationPageElements.orgAccessControlNodeTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(variables.newRootNodeName)).to.be.true;
                    });
                });
            });

        });
    });
};
exports.check_UAC_Node_Search_Box_Editable = check_UAC_Node_Search_Box_Editable;

var check_Accessioning_Toggle_Displays = function () {
    describe("Accessioning Location Toggle displays (defaults to False)",function () {
        it("should check - Accessioning Location Toggle displays (defaults to False)",function (browser) {
            browser.expect.element(elements.organizationPageElements.accessioningLocationRadio1).to.be.present;
            browser.expect.element(elements.organizationPageElements.accessioningLocationRadio2).to.be.present;
            browser.expect.element(elements.organizationPageElements.accessioningLocationRadio2).to.have.attribute('checked');
        });
    });
};
exports.check_Accessioning_Toggle_Displays = check_Accessioning_Toggle_Displays;

var check_Collection_Location_Toggle_Displays = function () {
    describe("Collection Location Toggle displays (defaults to False)",function () {
        it("should check - Collection Location Toggle displays (defaults to False)",function (browser) {
            browser.expect.element(elements.organizationPageElements.collectionLocationRadio1).to.be.present;
            browser.expect.element(elements.organizationPageElements.collectionLocationRadio2).to.be.present;
            browser.expect.element(elements.organizationPageElements.collectionLocationRadio2).to.have.attribute('checked');
        });
    });
};
exports.check_Collection_Location_Toggle_Displays = check_Collection_Location_Toggle_Displays;

var check_Ordering_Location_Toggle_Displays = function () {
    describe("Ordering Location Toggle displays (defaults to True)",function () {
        it("should check - Ordering Location Toggle displays (defaults to True)",function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.orderingLocationRadio1).to.be.present;
            browser.expect.element(elements.organizationPageElements.orderingLocationRadio2).to.be.present;
            browser.expect.element(elements.organizationPageElements.orderingLocationRadio1).to.have.attribute('checked');
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.settingsPopupCloseBtn);
            browser.pause(2000);
        });
    });
};
exports.check_Ordering_Location_Toggle_Displays = check_Ordering_Location_Toggle_Displays;

var check_VCard_Display = function () {
    describe("Confirm that the data is displayed correctly in the v-card",function () {
        it("should check - Confirm that the data is displayed correctly in the v-card",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageEditBtn);
            browser.expect.element(elements.organizationPageElements.vCardDiv).to.be.present;
            browser.expect.element(elements.organizationPageElements.orgRecordPageHeading).text.to.equal(orgNameCreated);
        });
    });
};
exports.check_VCard_Display = check_VCard_Display;

var check_VCard_Data_Display = function () {
    describe("Edit field and ensure that the data is displayed correctly in the v-card",function () {
        it("should check - Edit field and ensure that the data is displayed correctly in the v-card",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgLocationAndContactTab);
            browser.getText(elements.organizationPageElements.orgOfficeInfoPhone,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardPhone).text.to.equal(result.value);
            });
            browser.getText(elements.organizationPageElements.orgOfficeInfoFax,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardFax).text.to.equal(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressStreet1Input,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressStreet2Input,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressCityInput,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressStateInput,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressPostalInput,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
            browser.getText(elements.organizationPageElements.orgAddressCountryInput,function (result) {
                browser.expect.element(elements.organizationPageElements.vCardMailingAddress).text.to.contain(result.value);
            });
        });
    });
};
exports.check_VCard_Data_Display = check_VCard_Data_Display;

var create_New_Org = function (withHostCode) {
    describe("Create a new Org",function () {
        it("should create a new organization",function (browser) {
                browser.pause(2000);
                commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
                browser.pause(2000);
                commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationCreateLink);
                browser.pause(2000);
                orgNameCreated = variables.newOrgName;
                variables.createdORG = orgNameCreated;
                commands.checkAndSetValue(browser,elements.organizationPageElements.orgNameInput,variables.newOrgName);
                browser.pause(2000);
                browser.keys(browser.Keys.TAB);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
                if(withHostCode){
                    browser.pause(2000);
                    commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageSettingsIcon);
                    browser.pause(2000);
                    variables.createdORGHostCode = variables.orgCompleteHostCode;
                    commands.checkAndSetValue(browser,elements.organizationPageElements.hc1TestHostCodeInput,variables.orgCompleteHostCode);
                    browser.keys(browser.Keys.ENTER);
                    browser.pause(2000);
                    commands.checkAndPerform('click',browser,elements.organizationPageElements.orgCreateBtn);
                    browser.pause(2000);
                    commands.checkAndPerform('click',browser,elements.organizationPageElements.settingsIconCloseBtn);
                    browser.pause(2000);
                }
                browser.pause(5000);
                commands.checkAndPerform('click', browser,elements.organizationPageElements.orgDetailPageSpecialtyDiv);
                browser.pause(2000);
                browser.keys(browser.Keys.DOWN_ARROW);
                browser.keys(browser.Keys.ENTER);
                commands.checkAndPerform('click',browser,elements.organizationPageElements.orgSaveBtn);
                browser.pause(2000);
                browser.expect.element(elements.organizationPageElements.orgRecordPageHeading).text.to.equal(variables.newOrgName);
                browser.pause(5000);
                commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
                browser.pause(2000);

                //assert and push value to dependency fulfilled list
                browser.expect.element(elements.organizationPageElements.orgListTable).to.be.present;
                browser.elements("xpath", elements.organizationPageElements.orgListTable, function (result) {
                    var els = result.value;
                    els.forEach(function (el) {
                        browser.elementIdText(el.ELEMENT, function (text) {
                            expect(text.value.includes(variables.createdORG)).to.be.true;
                            variables.dependencyFulfilled.push('createdORG');
                            console.log('=====> Organization created successfully : '+variables.createdORG);
                        });
                    });
                });
                //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_New_Org = create_New_Org;

var create_New_Child_Org = function (withHostCode) {
    describe("Create a new Org",function () {
        it("should create a new organization",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationCreateLink);
            browser.pause(2000);
            orgNameCreated = variables.newChildOrgName;
            variables.createdChildORG = orgNameCreated;
            commands.checkAndSetValue(browser,elements.organizationPageElements.orgNameInput,variables.newChildOrgName);
            browser.pause(2000);
            browser.keys(browser.Keys.TAB);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            if(withHostCode){
                browser.pause(2000);
                commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageSettingsIcon);
                browser.pause(2000);
                variables.createdChildORGHostCode = variables.orgCompleteHostCode;
                commands.checkAndSetValue(browser,elements.organizationPageElements.hc1TestHostCodeInput,variables.orgCompleteHostCode);
                browser.keys(browser.Keys.ENTER);
                browser.pause(2000);
                commands.checkAndPerform('click',browser,elements.organizationPageElements.orgCreateBtn);
                browser.pause(2000);
                commands.checkAndPerform('click',browser,elements.organizationPageElements.settingsIconCloseBtn);
                browser.pause(2000);
            }
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.orgDetailPageSpecialtyDiv);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.orgRecordPageHeading).text.to.equal(variables.newChildOrgName);
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);

            //assert and push value to dependency fulfilled list
            browser.expect.element(elements.organizationPageElements.orgListTable).to.be.present;
            browser.elements("xpath", elements.organizationPageElements.orgListTable, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value.includes(variables.createdChildORG)).to.be.true;
                        variables.dependencyFulfilled.push('createdChildORG');
                    });
                });
            });
            //assert and push value to dependency fulfilled list

        });
    });
};
exports.create_New_Child_Org = create_New_Child_Org;

var check_Sales_Rep_Is_Auto_Populated = function () {
    describe("sales rep is auto populated (if the creator is a sales rep)",function () {
        it("should check - sales rep is auto populated (if the creator is a sales rep)",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.salesRepAddButton);
            browser.pause(2000);

            //With non sales rep user test (test case number 34 from the sheet)
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.addSalesRepInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.addSalesRepInput,variables.username1);
            browser.pause(10000);
            browser.elements("xpath",elements.organizationPageElements.salesRepAutoPopUpDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value).to.equal('No Results');
                    });
                });
            });
            browser.pause(2000);
            //With sales rep user test
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.addSalesRepInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.addSalesRepInput,variables.username);
            browser.pause(2000);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.keys([browser.Keys.ENTER]);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.addSalesRepInput).to.have.value.which.contain(variables.firstname);
            browser.pause(2000);
        });
    });
};
exports.check_Sales_Rep_Is_Auto_Populated = check_Sales_Rep_Is_Auto_Populated;

var check_Sales_Territory_Association = function () {
    describe("List of sales territory user is assoicated with is listed",function () {
        it("should check - List of sales territory user is assoicated with is listed",function (browser) {
            browser.pause(2000);
            commands.readValuesAndAssert(browser,elements.organizationPageElements.salesTerritoryListDiv,variables.newRootNodeName,true);
            browser.pause(2000);
        });
    });
};
exports.check_Sales_Territory_Association = check_Sales_Territory_Association;

var check_Activities_List_Display = function () {
    describe("Ensure that the list of activities are displayed properly ",function () {
        it("should check - Ensure that the list of activities are displayed properly ",function (browser) {
            browser.pause(5000);
            browser.expect.element(elements.organizationPageElements.activitiesListTable).to.be.present;
        });
    });
};
exports.check_Activities_List_Display = check_Activities_List_Display;

var check_Sales_Territory_Not_In_Association = function () {
    describe("List of sales territory user is not assoicated with is listed  (fail)",function () {
        it("should check - List of sales territory user is not assoicated with is listed  (fail)",function (browser) {
            browser.pause(2000);
            commands.readValuesAndAssert(browser,elements.organizationPageElements.salesTerritoryListDiv,variables.newChildNodeName,false);
            browser.pause(2000);
        });
    });
};
exports.check_Sales_Territory_Not_In_Association = check_Sales_Territory_Not_In_Association;

var select_Sales_Territory_AndVerify_Name = function () {
    describe("User should be able to select from the list of sales territories",function () {
        it("should check - User should be able to select from the list of sales territories",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.addSalesRepInput);
            browser.pause(2000);
            var markedNodeName = '';
            browser.elements("xpath", elements.organizationPageElements.salesTerritoryListDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        var testArr = text.value.split("\n");
                        markedNodeName = testArr[0];
                    });
                });
            });
            browser.keys([browser.Keys.TAB]);
            browser.keys([browser.Keys.SPACE]);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.salesRepOkButton);
            browser.pause(2000);
            browser.elements("xpath", elements.organizationPageElements.salesTerritorySavedListDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(markedNodeName)).to.be.true;
                    });
                });
            });
            commands.checkAndPerform('click', browser,elements.organizationPageElements.orgSaveBtn);
            browser.pause(2000);
        });
    });
};
exports.select_Sales_Territory_AndVerify_Name = select_Sales_Territory_AndVerify_Name;

var check_Edit_Icon_Display = function () {
    describe("edit icon is displayed if the user has write permission ",function () {
        it("should check - edit icon is displayed if the user has write permission ",function (browser) {
            browser.expect.element(elements.organizationPageElements.activitiesListTable+'/thead/tr/th[1]').to.have.attribute('class').equals('edit');
        });
    });
};
exports.check_Edit_Icon_Display = check_Edit_Icon_Display;

var check_Delete_Icon_Display = function (table) {
    describe("delete icon is displayed if the user has del permission ",function () {
        it("should check - delete icon is displayed if the user has del permission ",function (browser) {
            browser.elements("xpath", table+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Delete_Icon_Display = check_Delete_Icon_Display;

var check_Edit_Icon_Display_Fail = function () {
    describe("edit icon not displayed without write permission (fail)",function () {
        it("should check - edit icon not displayed without write permission (fail)",function (browser) {
            browser.pause(2000);
            browser.elements("xpath", elements.organizationPageElements.orgListTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        console.log(text.value);
                        expect(text.value).to.not.equal('edit');
                    });
                });
            });
            browser.pause(2000);

            // commands.checkAndPerform('click',browser,elements.organizationPageElements.pagingRecordsPerPageMaxOption);
            // browser.pause(2000);
            // commands.checkAndPerform('click',browser,elements.organizationPageElements.defaultTestOrg);
            // browser.pause(2000);
            // browser.expect.element(elements.organizationPageElements.activitiesListTable+'/thead/tr/th[1]').to.have.attribute('class').not.equals('edit');
        });
    });
};
exports.check_Edit_Icon_Display_Fail = check_Edit_Icon_Display_Fail;

var check_Delete_Icon_Display_Fail = function () {
    describe("del icon is displayed without del permission (fail)",function () {
        it("should check - del icon is displayed without del permission (fail)",function (browser) {
            browser.elements("xpath", elements.organizationPageElements.activitiesListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        });
    });
};
exports.check_Delete_Icon_Display_Fail = check_Delete_Icon_Display_Fail;

var check_Activities_Display_columns = function () {
    describe("del icon is displayed without del permission (fail)",function () {
        it("should check - del icon is displayed without del permission (fail)",function (browser) {
            browser.elements("xpath", elements.organizationPageElements.activitiesListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        });
    });
};
exports.check_Activities_Display_columns = check_Activities_Display_columns;

var check_Date_Dropdown_present = function () {
    describe("List Date Range drop down is present",function () {
        it("should check - List Date Range drop down is present",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersSubTabBtn);
            browser.pause(1500);
            browser.expect.element(elements.organizationPageElements.ordersPageDateRangeDropDown).to.be.present;
        });
    });
};
exports.check_Date_Dropdown_present = check_Date_Dropdown_present;

var check_Start_Date_Is_Calendar = function () {
    describe("Start date is a calendar - Ensure that the user can filter by date",function () {
        it("should check - Start date is a calendar - Ensure that the user can filter by date",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageStartDateDiv);
            browser.pause(1000);
            browser.expect.element(elements.organizationPageElements.ordersPageDatePicker).to.be.visible;
        });
    });
};
exports.check_Start_Date_Is_Calendar = check_Start_Date_Is_Calendar;

var check_Range_Slide_Behaviour = function () {
    describe("Ensure that Range slide bar behaves properly",function () {
        it("should check - Ensure that Range slide bar behaves properly",function (browser) {
            browser.pause(2000);
            var previousValue = '';
            browser.elements("xpath", elements.organizationPageElements.ordersPageRangeValueDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        previousValue = text.value;
                    });
                });
            });
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageRangeSlider);
            browser.elements("xpath", elements.organizationPageElements.ordersPageRangeValueDiv, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(15)).to.be.true;
                    });
                });
            });


        });
    });
};
exports.check_Range_Slide_Behaviour = check_Range_Slide_Behaviour;

var check_Orders_Filter_Behaviour = function () {
    describe("Ensure that Quick Filter behaves properly (check with all the filters)",function () {
        it("should check - Ensure that Quick Filter behaves properly (check with all the filters)",function (browser) {

            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageQuickFilterLink);
            browser.pause(2000);

            //read avail columns count
            var availColumnsCount = 0;
            browser.elements("xpath", elements.organizationPageElements.ordersPageFilterAvailColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if (text.value.trim() != '') {
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(",");
                            for (var i = 0; i < testArr.length; i++) {
                                availColumnsCount  = availColumnsCount + 1;
                            }
                            if(availColumnsCount > 0){
                                for(var i = 1;i < availColumnsCount; i++){
                                    commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageFilterAvailColumns+"/li["+i+"]");
                                    browser.pause(1000);
                                }
                            }
                        }
                    });
                });
            });

            browser.pause(1500);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageFilterAddBtn);
            browser.pause(2000);

            //read selected columns div
            var selectedColumnsList = [];
            browser.elements("xpath",elements.organizationPageElements.ordersPageCustomListSelectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(text.value.trim() != ''){
                            var textValueTrimmed = text.value.replace(/\n|\r/g,',');
                            var testArr = textValueTrimmed.split(',');
                            for(var i=0;i<testArr.length;i++){
                                selectedColumnsList.push(testArr[i]);
                            }
                        }
                    });
                });
            });

            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageFilterApplyBtn);
            browser.pause(2000);
            browser.elements("xpath", elements.organizationPageElements.ordersPageRecordsListTable+"/thead", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        for(var i=0;i<selectedColumnsList;i++){
                            expect(text.value.includes(selectedColumnsList[i])).to.be.true;
                        }
                    });
                });
            });
            commands.checkAndPerform('click',browser,elements.organizationPageElements.ordersPageQuickFilterLink);
        });
    });
};
exports.check_Orders_Filter_Behaviour = check_Orders_Filter_Behaviour;

var check_Add_Attachment = function () {
    describe("Add an attachment with User with write Attachment permission",function () {
        it("should check - Add an attachment with User with write Attachment permission",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageLink);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadLink).to.be.present;
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageNewUploadLink);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadPopUp).to.be.visible;
            browser.expect.element(elements.organizationPageElements.attachmentsUploadFileInput).to.be.present;
            browser.expect.element(elements.organizationPageElements.attachmentsUploadBtn).to.be.present;
            browser.pause(2000);
            //upload a test file
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadFileInput, variables.testFileUploadPath);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.testFileUploadName+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Add_Attachment = check_Add_Attachment;

var check_Add_Attachment_With_PHI = function () {
    describe("Add an attachment(with PHI) with User with write Attachment permission", function () {
        it("should check - Add an attachment(with PHI) with User with write Attachment permission", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageLink);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadLink).to.be.present;
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageNewUploadLink);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadPopUp).to.be.visible;
            browser.expect.element(elements.organizationPageElements.attachmentsUploadFileInput).to.be.present;
            browser.expect.element(elements.organizationPageElements.attachmentsUploadBtn).to.be.present;
            browser.pause(2000);
            //upload a test file
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadNameInput, variables.newUploadName);
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadFileInput, variables.testFileUploadPath);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsUploadPHICheckBox);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsUploadBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.testFileUploadName+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.check_Add_Attachment_With_PHI = check_Add_Attachment_With_PHI;

var check_Add_Attachment_Fail = function () {
    describe("Add an attachment with User without write Attachment permission (Fail)",function () {
        it("should check - Add an attachment with User without write Attachment permission (Fail)",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.attachmentsSubTabBtn);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadLink).to.not.be.present;
        });
    });
};
exports.check_Add_Attachment_Fail = check_Add_Attachment_Fail;

var check_Delete_Attachment = function () {
    describe("Delete an Attachment with user with Delete Attachment permissions",function () {
        it("should check - Delete an Attachment with user with Delete Attachment permissions",function (browser) {
            browser.pause(2000);
            var tblHeaders = 0;
            browser.elements("xpath", elements.organizationPageElements.attachmentsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function () {
                    tblHeaders = tblHeaders+1;
                });
            });

            // Delete icon is present with delete permission
            browser.elements("xpath", elements.organizationPageElements.attachmentsTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value != 'delete'){
                            if(count === tblHeaders){
                                expect('deleteColumn').to.equal('deleteColumnNotFound');
                            }
                        }
                    });
                });
            });

            //delete the attachment
            browser.elements("xpath", elements.organizationPageElements.attachmentsTable+"/tbody/tr", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        count = count + 1;
                        console.log('>>>>> table data : '+text.value);
                        if(text.value.includes(variables.newUploadName)){
                            commands.checkAndPerform('click', browser, elements.organizationPageElements.attachmentsTable+'/tbody/tr[1]/td[5]/i');
                            browser.pause(3000);
                            browser.expect.element("//a[contains(@title,'"+variables.newUploadName+"')]").to.not.be.present;
                        }
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_Delete_Attachment = check_Delete_Attachment;

var check_Download_Attachment = function () {
    describe("Download Attachment with user with read permission",function () {
        it("should check - Download Attachment with user with read permission",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.attachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('download')).to.be.true;
                        }
                    });
                });
            });
            browser.getAttribute("//a[contains(@title,'"+variables.testFileUploadName+"')]","href",function (re) {
               console.log('>>>>>href : '+re.value);
               expect(re.value).to.include('download');
            });

        });
    });
};
exports.check_Download_Attachment = check_Download_Attachment;

var check_Download_Attachment_With_PHI = function () {
    describe("Download Attachment(with PHI) with user with read permission", function () {
        it("should check - Download Attachment(with PHI) with user with read permission", function (browser) {
            browser.elements("xpath",elements.organizationPageElements.attachmentsTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('download')){
                            expect(text.value.includes('download')).to.be.true;
                        }
                    });
                });
            });
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.testFileUploadName+"')]");
            browser.pause(2000);
            browser.getAttribute("/html/body","class",function (re) {
               console.log('>>body class'+re.value);
               expect(re.value).to.equal('modal-open');
            });
            // browser.expect.element(elements.globalElements.phiDisclaimerPopup).to.be.visible;
            // browser.pause(2000);
            // commands.checkAndPerform('click', browser,elements.globalElements.phiDisclaimerPopupYesOpt);
            // browser.pause(2000);
        });
    });
};
exports.check_Download_Attachment_With_PHI = check_Download_Attachment_With_PHI;

var check_Attachment_Pop_Up_Display = function () {
    describe("Ensure that attachment pop-up is displayed",function () {
        it("should check - Ensure that attachment pop-up is displayed",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageNewUploadLink);
            browser.expect.element(elements.organizationPageElements.attachmentsPageNewUploadPopUp).to.be.present;
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsUploadPopUpCloseBtn);
        });
    });
};
exports.check_Attachment_Pop_Up_Display = check_Attachment_Pop_Up_Display;

var check_Name_And_File_Required = function () {
    describe("Name and file are required fields",function () {
        it("should check - Name and file are required fields",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsPageNewUploadLink);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.attachmentsUploadBtn);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.attachmentsNameInputErrorLabel).to.be.present;
            browser.expect.element(elements.organizationPageElements.attachmentsFileInputErrorLabel).to.be.present;
        });
    });
};
exports.check_Name_And_File_Required = check_Name_And_File_Required;

var check_Add_Description = function () {
    describe("Add description",function () {
        it("should check - Add description",function (browser) {
            browser.expect.element(elements.organizationPageElements.attachmentsUploadDescInput).to.be.present;
            commands.checkAndSetValue(browser,elements.organizationPageElements.attachmentsUploadDescInput,variables.newAttachmentDescription);
        });
    });
};
exports.check_Add_Description = check_Add_Description;

var check_PHI_CheckBox_Present = function () {
    describe("PHI checkbox is present",function () {
        it("should check - PHI checkbox is present",function (browser) {
            browser.expect.element(elements.organizationPageElements.attachmentsUploadPHICheckBox).to.be.present;
        });
    });
};
exports.check_PHI_CheckBox_Present = check_PHI_CheckBox_Present;

var check_Delete_Attachment_Fail = function () {
    describe("Delete an Attachment with user without Delete Attachment permissions (Fail)",function () {
        it("should check - Delete an Attachment with user without Delete Attachment permissions (Fail)",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.attachmentsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        });
    });
};
exports.check_Delete_Attachment_Fail = check_Delete_Attachment_Fail;

var check_Download_Attachment_Fail = function () {
    describe("Download Attachment with user without read permission (Fail)",function () {
        it("should check - Download Attachment with user without read permission (Fail)",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.attachmentsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        expect(text.value.includes('download')).to.be.false;
                    });
                });
            });
        });
    });
};
exports.check_Download_Attachment_Fail = check_Download_Attachment_Fail;

var search_New_Org = function () {
    describe("Ensure that the user can search for the new organization in the cloud search",function () {
        it("should check - Ensure that the user can search for the new organization in the cloud search",function (browser) {
            var currentORG = variables.createdORG;
            browser.pause(10000);
            commands.checkAndPerform('clearValue',browser,elements.homePageElements.cloudSearchInputField);
            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,currentORG);
            browser.pause(2000);
            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
            browser.elements("xpath",elements.homePageElements.cloudSearchAllTabLink, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        var allTabCount = text.value;
                        if (allTabCount > 0) {
                            browser.pause(5000);
                            browser.keys([browser.Keys.TAB]);
                            browser.expect.element(elements.homePageElements.cloudSearchOrgTabLink).to.have.attribute('class').which.contains('selected');
                            browser.expect.element(elements.homePageElements.cloudSearchOrgResultHeader).text.to.equal('Organization');
                            browser.elements("xpath", elements.homePageElements.cloudSearchOrgResultData, function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdText(el.ELEMENT, function (text) {
                                        var searchedOrgName = text.value;
                                        expect(searchedOrgName.includes(currentORG)).to.be.true;
                                    });
                                });
                            });
                        }
                    });
                });
            });
        });
    });
};
exports.search_New_Org = search_New_Org;

var search_Existing_Org = function () {
    describe("Ensure that the user can search for a existing organization in cloud search",function () {
        it("should check - Ensure that the user can search for a existing organization in cloud search",function (browser) {
            browser.pause(5000);
            var orgNames = '';
            var existingORG = '';
            browser.elements("xpath",elements.organizationPageElements.orgList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        var responseData =  text.value;
                        if (responseData != undefined || responseData != '' || responseData != null) {
                            orgNames = responseData.split('\n');
                            existingORG = orgNames[0].split(" ")[0];
                            browser.pause(1000);
                            commands.checkAndPerform('clearValue',browser,elements.homePageElements.cloudSearchInputField);
                            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,existingORG);
                            browser.pause(2000);
                            browser.keys(browser.Keys.TAB);
                            browser.keys(browser.Keys.DOWN_ARROW);
                            browser.pause(2000);
                            browser.expect.element(elements.homePageElements.cloudSearchOrgResultHeader).text.to.equal('Organization');
                            browser.elements("xpath", elements.homePageElements.cloudSearchOrgResultData, function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdText(el.ELEMENT, function (text) {
                                        var searchedOrgName = text.value;
                                        expect(searchedOrgName.includes(existingORG)).to.be.true;
                                    });
                                });
                            });
                        }
                    });
                });
            });
        });
    });
}
exports.search_Existing_Org = search_Existing_Org;

var create_Case_From_Search = function () {
    describe("Create case on a organization from cloud search",function () {
        it("should check - Create case on a organization from cloud search",function(browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.homePageElements.cloudSearchCaseIcon);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.activityPageElements.caseSubjectInputField);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInputField,variables.caseSubject)
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.caseSaveButton);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.caseViewSubjectFiled).text.to.equal(variables.caseSubject);
            browser.pause(2000);
        });
    });
}
exports.create_Case_From_Search = create_Case_From_Search;

var create_Task_From_Search = function () {
    describe("Create Task on a organization from cloud search",function () {
        it("should check - Create Task on a organization from cloud search",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.homePageElements.cloudSearchTaskIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField,variables.taskSubject)
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.taskSaveButton);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.taskViewSubjectFiled).text.to.equal(variables.taskSubject);
            browser.pause(2000);
        });
    });
}
exports.create_Task_From_Search = create_Task_From_Search;

var create_Memo_From_Search = function () {

    describe("Create Memo on a organization from cloud search",function () {
        it("should check - Create Memo on a organization from cloud search",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.homePageElements.cloudSearchMemoIcon);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.memoSubject)
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.activityPageElements.memoSaveButton);
            browser.pause(2000);
            browser.expect.element(elements.activityPageElements.memoViewSubjectFiled).text.to.equal(variables.memoSubject);
            browser.pause(2000);
        });
    });
}
exports.create_Memo_From_Search = create_Memo_From_Search;

var create_Opportunity_From_Search = function () {
    describe("Create Opportunity on a organization from cloud search",function () {
        it("should check - Create Opportunity on a organization from cloud search",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.homePageElements.cloudSearchOpportunityIcon);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNameInputField,variables.opportunityName)
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.opportunityPageElements.opportunitySalesTerritorySelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.opportunityPageElements.opportunitySalesRepSelect);
            browser.keys([browser.Keys.DOWN_ARROW]);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityCloseDateInput, variables.today);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.opportunityPageElements.opportunitySaveButton);
            browser.pause(2000);
            browser.expect.element(elements.opportunityPageElements.opportunityViewPageHeader).text.to.equal(variables.opportunityName);
            browser.pause(2000);
        });
    });
}
exports.create_Opportunity_From_Search = create_Opportunity_From_Search;

var copy_MetaTag_From_Search = function () {
    describe("Copy meta tag from cloud search",function () {
        it("should check - Copy meta tag from cloud search",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.homePageElements.cloudSearchMetaTagIcon);
            browser.pause(2000);
            browser.expect.element(elements.homePageElements.cloudSearchMetaTagClipboard).text.to.equal(variables.metaTagClipboardTitle);
            browser.pause(2000);
            browser.keys(browser.Keys.ESCAPE);
            browser.pause(2000);
        });
    });
}
exports.copy_MetaTag_From_Search = copy_MetaTag_From_Search;

var update_Existing_Org_And_Search = function () {
    describe("Update And Search Organization",function () {
        it("should update and search existing Organization on cloud",function (browser) {
            var orgNames = '';
            var existingORG = '';
            var orgToUpdate = variables.updatedOrgName;
            browser.elements("xpath",elements.organizationPageElements.orgList, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        var responseData =  text.value;
                        if (responseData != undefined || responseData != '' || responseData != null) {
                            orgNames = responseData.split('\n');
                            existingORG = orgNames[1].split(' ')[0]; // For Selecting 1st Org Details from list
                            console.log('>>existing org name : '+existingORG);
                            var extractedORG = existingORG.split(' '); // For filtering Org Name from All Details
                            commands.checkAndPerform('clearValue',browser,elements.homePageElements.cloudSearchInputField);
                            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,extractedORG);
                            browser.pause(2000);
                            browser.keys([browser.Keys.TAB]);
                            browser.keys([browser.Keys.DOWN_ARROW]);
                            browser.pause(1000);
                            commands.checkAndPerform('click', browser, elements.homePageElements.cloudSearchOrgOptionLink);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
                            browser.pause(2000);
                            commands.checkAndPerform('clearValue', browser, elements.organizationPageElements.orgEditPageNameInputField);
                            browser.pause(2000);
                            commands.checkAndSetValue(browser,elements.organizationPageElements.orgEditPageNameInputField,orgToUpdate);
                            browser.pause(2000);
                            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgEditPageSaveButton);
                            browser.pause(10000);
                            commands.checkAndPerform('click', browser,elements.organizationPageElements.organizationTabLink);
                            browser.pause(2000);
                            commands.checkAndSetValue(browser,elements.homePageElements.cloudSearchInputField,orgToUpdate);
                            browser.pause(2000);
                            browser.expect.element(elements.homePageElements.cloudSearchResultBox).to.have.css('display').which.equals('block');
                            browser.pause(2000);
                            browser.expect.element(elements.homePageElements.cloudSearchOrgResultHeader).text.to.equal('Organization');
                            browser.elements("xpath", elements.homePageElements.cloudSearchOrgResultData, function (result) {
                                var els = result.value;
                                els.forEach(function (el) {
                                    browser.elementIdText(el.ELEMENT, function (text) {
                                        var searchedOrgName = text.value;
                                        expect(searchedOrgName.includes(orgToUpdate)).to.be.true;
                                    });
                                });
                            });
                        }
                    });
                });
            });
        });
    });

}
exports.update_Existing_Org_And_Search = update_Existing_Org_And_Search;

var check_Message_List_Display = function () {
    describe("Ensure that message list view is displayed properly",function () {
        it("should check - Ensure that message list view is displayed properly",function (browser) {
            browser.expect.element(elements.organizationPageElements.messagePageRecordsListTable).to.be.present;
        });
    });
};
exports.check_Message_List_Display = check_Message_List_Display;

var check_General_Messages_Listed = function () {
    describe("Ensure that general messages are listed",function () {
        it("should check - Ensure that general messages are listed",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.organizationPageElements.messagesSubTabBtn);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.newMessageSubject+'")]');
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messageBoxCloseBtn);
        });
    });
};
exports.check_General_Messages_Listed = check_General_Messages_Listed;

var check_Message_Filters_Behave_Properly = function () {
    describe("Ensure that  Quick Filter behaves properly (check with all the filters)",function () {
        it("should check - Ensure that  Quick Filter behaves properly (check with all the filters)",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageQuickFilterLink);
            browser.pause(2000);
            browser.elements("xpath",elements.organizationPageElements.messagePageFilterSelectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        if(text.value.trim() != ''){
                            var testArr = text.value.split("\n");
                            var count = 0;
                            variables.filterAvailableColumnsName.push(testArr[0]);
                            count ++;
                            var elementName = elements.organizationPageElements.messagePageFilterSelectedColumns+"/li["+1+"]";
                            commands.checkAndPerform('click',browser,elementName);
                            browser.pause(1500);
                            variables.filterAvailableColumnsCount = count;
                        }
                    });
                });
            });
            browser.pause(1500);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageFilterRemoveBtn);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageFilterApplyBtn);
            browser.pause(2000);
            browser.elements("xpath", elements.organizationPageElements.messagePageRecordsListTable+"/thead", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        for(var i=0;i<variables.filterAvailableColumnsName.length-1;i++){
                            expect(text.value.includes(variables.filterAvailableColumnsName[i])).to.be.true;
                        }
                    });
                });
            });
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageQuickFilterLink);
        });
    });
};
exports.check_Message_Filters_Behave_Properly = check_Message_Filters_Behave_Properly;

var check_Message_Columns_Behave_Properly = function () {
    describe("Ensure that  Columns behaves properly (check with all the fields)",function () {
        it("should check - Ensure that  Columns behaves properly (check with all the fields)",function (browser) {
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageSettingsIcon);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageCustomListNewLink);
            browser.pause(2000);
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.messagePageNewListInput);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.messagePageNewListInput,variables.testCustomListName);
            browser.pause(2000);

            browser.elements("xpath",elements.organizationPageElements.messagePageCustomListSelectedColumns, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        var testArr = text.value.split("\n");
                        var count = 0;
                        variables.availableColumnsName.push(testArr[0]);
                        count ++;
                        var elementName = elements.organizationPageElements.messagePageCustomListSelectedColumns+"/li["+1+"]";
                        commands.checkAndPerform('click',browser,elementName);
                        variables.availableColumnsCount = count;
                    });
                });
            });
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.messagePageCustomListRemoveBtn);
            browser.pause(2000);
            browser.elements("xpath",elements.organizationPageElements.messagePageCustomListAvailColumns, function(result){
                var els = result.value;
                els.forEach(function(el){
                    browser.elementIdText(el.ELEMENT, function(text) {
                        for(var i = 0;i<variables.availableColumnsName.length-1;i++){
                            expect(text.value.includes(variables.availableColumnsName[i])).to.be.true;
                        }
                    });
                });
            });

            commands.checkAndPerform('click',browser,elements.organizationPageElements.createNewCustomListSaveBtn);
            browser.pause(2000);

            browser.elements("xpath",elements.organizationPageElements.messagePageRecordsListTable+"/thead", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        for(var i=0;i<variables.availableColumnsName.length-1;i++){
                            expect(text.value.includes(variables.availableColumnsName[i])).to.be.true;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Message_Columns_Behave_Properly = check_Message_Columns_Behave_Properly;

var check_Subtabs_Display = function () {
    describe("Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed",function () {
        it("should check - Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.expect.element(elements.organizationPageElements.messageSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.locationAndContactSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.contactsSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.feeSchedulesSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.childOrgSubTab).to.be.present;
        });
    });
};
exports.check_Subtabs_Display = check_Subtabs_Display;

var check_Address_Tab_Highlighted = function () {
    describe("Address Subtab is highlighted",function () {
        it("should check - Address Subtab is highlighted",function (browser) {
            browser.expect.element(elements.organizationPageElements.locationAndContactSubTab).to.have.attribute('class').which.contains('selected');
        });
    });
};
exports.check_Address_Tab_Highlighted = check_Address_Tab_Highlighted;

var address_add_Phone = function () {
    describe("Add Phone",function () {
        it("should check - Add Phone",function (browser) {
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPagePhoneInput,variables.phone);
        });
    });
};
exports.address_add_Phone = address_add_Phone;

var address_Add_Fax = function () {
    describe("Add Fax",function () {
        it("should check - Add Fax",function (browser) {
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageFaxInput,variables.fax);
        });
    });
};
exports.address_Add_Fax = address_Add_Fax;

var address_Add_Website = function () {
    describe("Add Website",function () {
        it("should check - Add Website",function (browser) {
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageWebsiteInput,variables.testURL);
        });
    });
};
exports.address_Add_Website = address_Add_Website;

var check_Website_Redirects = function () {
    describe("Confirm that Website redirects to the website",function () {
        it("should check - Confirm that Website redirects to the website",function (browser) {
            browser.click('//a[contains(@href,"'+variables.fullUrl+'")]');
            browser.expect.element(elements.organizationPageElements.organizationTabLink).to.be.present;
        });
    });
};
exports.check_Website_Redirects = check_Website_Redirects;

var address_Add_Desc = function () {
    describe("Add description",function () {
        it("should check - Add description",function (browser) {
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageDescriptionInput,variables.locationDesc);
        });
    });
};
exports.address_Add_Desc = address_Add_Desc;

var address_Edit_Phone = function () {
    describe("Edit Phone",function () {
        it("should check - Edit Phone",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.locationPageEditBtn);
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.locationPagePhoneInput);
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPagePhoneInput,variables.phone_updated);
        });
    });
};
exports.address_Edit_Phone = address_Edit_Phone;

var address_Edit_Fax = function () {
    describe("Edit Fax",function () {
        it("should check - Edit Fax",function (browser) {
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.locationPageFaxInput);
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageFaxInput,variables.fax_updated);
        });
    });
};
exports.address_Edit_Fax = address_Edit_Fax;

var address_Edit_Desc = function () {
    describe("Edit Description",function () {
        it("should check - Edit Description",function (browser) {
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.locationPageDescriptionInput);
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageDescriptionInput,variables.locationDesc_updated);
        });
    });
};
exports.address_Edit_Desc = address_Edit_Desc;

var address_Edit_Website = function () {
    describe("Edit Website",function () {
        it("should check - Edit Website",function (browser) {
            commands.checkAndPerform('clearValue',browser,elements.organizationPageElements.locationPageWebsiteInput);
            commands.checkAndSetValue(browser,elements.organizationPageElements.locationPageWebsiteInput,variables.fullUrl);
        });
    });
};
exports.address_Edit_Website = address_Edit_Website;

var navigate_Address_Tab_Edit_Mode = function () {
    describe("Navigate to the Address Subtab in Edit Mode",function () {
        it("should check - Navigate to the Address Subtab in Edit Mode",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.locationPageEditBtn);
            browser.expect.element(elements.organizationPageElements.locationPagePhoneInput).to.be.present;
        });
    });
};
exports.navigate_Address_Tab_Edit_Mode = navigate_Address_Tab_Edit_Mode;

var save_Location_And_Check = function (afterUpdate) {
    describe("Saving the location info and asserting",function () {
        it("should check - save the location information and validate",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.locationPageSaveBtn);
            browser.pause(2000);
            if(afterUpdate){
                browser.expect.element(elements.organizationPageElements.locationPagePhoneDiv).text.to.contain(variables.phone_updated);
                browser.expect.element(elements.organizationPageElements.locationPageFaxDiv).text.to.contain(variables.fax_updated);
                browser.expect.element(elements.organizationPageElements.locationPageWebsiteDiv).text.to.contain(variables.fullUrl);
                browser.expect.element(elements.organizationPageElements.locationPageDescriptionInput).text.to.contain(variables.locationDesc_updated);
            }
            else{
                browser.expect.element(elements.organizationPageElements.locationPagePhoneDiv).text.to.contain(variables.phone);
                browser.expect.element(elements.organizationPageElements.locationPageFaxDiv).text.to.contain(variables.fax);
                browser.expect.element(elements.organizationPageElements.locationPageWebsiteDiv).text.to.contain(variables.testURL);
                browser.expect.element(elements.organizationPageElements.locationPageDescriptionInput).text.to.contain(variables.locationDesc);
            }
        });
    });
};
exports.save_Location_And_Check = save_Location_And_Check;

var edit_OrgType = function () {
    describe("To edit org type", function () {
        it("should check - Edit org type", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
        });
    });
};
module.exports.edit_OrgType = edit_OrgType;

var add_organizationType = function () {
    describe("ADD ORGANIZATION TYPE FROM ADMIN SECTION", function () {
        it("should check - organization type is added or not ", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageTypeDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
        });
    });
}
module.exports.add_organizationType = add_organizationType;

var create_Case_fromOrgGeneralTab = function () {
    describe("TO CREATE A CASE FROM ORG GENRAL TAB", function () {
        it("should be - create a Case from the Org General tab ", function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageNewCaseLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.caseSubjectInput,variables.newCaseName);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(3000);
            browser.expect.element('//a[contains(@title,"'+variables.newCaseName+'")]').to.be.present;
        });
    });
};
module.exports.create_Case_fromOrgGeneralTab = create_Case_fromOrgGeneralTab;

var create_Task_fromOrgGeneralTab = function () {
    describe("TO CREATE A TASK FROM ORG GENRAL TAB", function () {
        it("should be - create a TASK from the Org General tab ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageNewTaskLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.taskSubjectInputField,variables.newTaskName);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(3000);
            browser.expect.element('//a[contains(@title,"'+variables.newTaskName+'")]').to.be.present;
        });
    });
};
module.exports.create_Task_fromOrgGeneralTab = create_Task_fromOrgGeneralTab;

var create_Memo_fromOrgGeneralTab = function () {
    describe("create a MEMO from the Org General tab", function () {
        it("should check - create a MEMO from the Org General tab ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageNewMemoLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.activityPageElements.memoSubjectInputField,variables.newMemoName);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.caseSaveAndBackBtn);
            browser.pause(3000);
            browser.expect.element('//a[contains(@title,"'+variables.newTaskName+'")]').to.be.present;
        });
    });
};
module.exports.create_Memo_fromOrgGeneralTab = create_Memo_fromOrgGeneralTab;

var create_Opportunity_fromOrgGeneralTab = function () {
    describe("create a Opportunity from the Org General tab ", function () {
        it("should check - create a Opportunity from the Org General tab ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageNewOpportunityLink);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityNameInputField);
            browser.pause(2000);
            variables.createdOpportunity = variables.opportunityName;
            commands.checkAndSetValue(browser,elements.opportunityPageElements.opportunityNameInputField, variables.opportunityName);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesTerritorySelect);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySalesRepSelect);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(5000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunityCloseDateOption);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitySaveAndBackBtn);
            browser.pause(5000);
        });
    });
};
module.exports.create_Opportunity_fromOrgGeneralTab = create_Opportunity_fromOrgGeneralTab;

var removeOrg_Specialties = function () {
    describe("DELETE Speciality in Orgs General tab", function () {
        it("should check - DELETE Speciality in Orgs", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageSpecialtiesMainDiv+'/div[2]/span');
            browser.pause(3000);
        });
    });
};
module.exports.removeOrg_Specialties = removeOrg_Specialties;

var check_contact_list_inorgGeneralTab = function () {
    describe("TO CHECK THE LIST OF CONTACTS IN ORG GENERAL TAB  ", function () {
        it("should check - list of contact is present or not", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.createdORG+'")]');
            commands.checkAndPerform('click', browser, elements.organizationPageElements.contactsSubTab);
            browser.expect.element(elements.organizationPageElements.contactsPageRecordsList).to.be.present;
        });
    });
};
module.exports.check_contact_list_inorgGeneralTab = check_contact_list_inorgGeneralTab;

var check_General_Tab_Highlighted = function () {
    describe("General tab is highlighted",function () {
        it("should check - General tab is highlighted",function (browser) {
            browser.expect.element(elements.organizationPageElements.orgGeneralTab).to.have.attribute('class').which.contain('selected');
        });
    });
};
exports.check_General_Tab_Highlighted = check_General_Tab_Highlighted;

var add_Org_Number = function () {
    describe("Add Organization number",function () {
        it("should check - Add Organization number",function (browser) {
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageEditBtn);
            commands.checkAndSetValue(browser,elements.organizationPageElements.orgDetailPageNumberInput,variables.orgNumber);
            browser.pause(1000);
        });
    });
};
exports.add_Org_Number = add_Org_Number;

var add_Multiple_Specialties = function () {
    describe("Add Multiple Specialties",function () {
        it("should check - Add Multiple Specialties",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageSpecialtiesInput);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.orgDetailPageSpecialtiesInput);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
        });
    });
};
exports.add_Multiple_Specialties = add_Multiple_Specialties;

var add_Relationship_Mnr = function () {
    describe("Add Relationship Mgr associated to the Sales Territory",function () {
        it("should check - Add Relationship Mgr associated to the Sales Territory",function (browser) {
            commands.checkAndSetValue(browser,elements.organizationPageElements.orgDetailPageRelationshipMnrInput,variables.firstname);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
        });
    });
};
exports.add_Relationship_Mnr = add_Relationship_Mnr;

var add_Primary_Contact = function () {
    describe("Add a Primary contact in the Associated Sales Territory",function () {
        it("should check - Add a Primary contact in the Associated Sales Territory",function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(1000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsPageNewContactLink);
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.contactsPageFirstNameInput,variables.contactFName);
            commands.checkAndSetValue(browser,elements.organizationPageElements.contactsPageLastNameInput,variables.contactLName);
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsPageCreateBtn);
            browser.pause(5000);
            commands.checkAndPerform('click',browser,elements.contactsPageElements.contactsPageSaveAndBackBtn);
            browser.pause(2000);
            browser.expect.element("//a[@title='"+variables.contactLName+","+variables.contactFName+" "+variables.contactMName+"']").to.be.present;
        });
    });
};
exports.add_Primary_Contact = add_Primary_Contact;

var check_Contact_Redirects = function () {
    describe("Ensure that the user is brought to the contact detail page when the user selects the contact icon or name",function () {
        it("should check - Ensure that the user is brought to the contact detail page when the user selects the contact icon or name",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,"//a[@title='"+variables.contactLName+","+variables.contactFName+" "+variables.contactMName+"']");
            browser.pause(4000);
            browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.equal(variables.contactFName+" "+variables.contactLName);
            browser.pause(2000);
            browser.expect.element(elements.contactsPageElements.contactsTabLink).to.have.attribute('class').to.equals('contacts selected-tab');
        });
    });
};
exports.check_Contact_Redirects = check_Contact_Redirects;

var check_Subtabs_Display = function () {
    describe("Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed",function () {
        it("should check - Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed",function (browser) {
            browser.expect.element(elements.organizationPageElements.orgGeneralTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.locationAndContactSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.contactsSubTab).to.be.present;
            browser.expect.element(elements.organizationPageElements.childOrgSubTab).to.be.present;
            browser.expect.element(elements.activityPageElements.activitiesTabLink).to.be.present;
            browser.expect.element(elements.organizationPageElements.ordersSubTabBtn).to.be.present;
            browser.expect.element(elements.organizationPageElements.attachmentsSubTabBtn).to.be.present;
        });
    });
};
exports.check_Subtabs_Display = check_Subtabs_Display;

var check_Contacts_Subtab_Highlighted = function () {
    describe("Contacts & Providers Subtab is highlighted",function () {
        it("should check - Contacts & Providers Subtab is highlighted",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.expect.element(elements.organizationPageElements.contactsSubTab).to.have.attribute('class').which.contain('selected');
        });
    });
};
exports.check_Contacts_Subtab_Highlighted  = check_Contacts_Subtab_Highlighted;

var check_NewContact_Btn_Display = function () {
    describe("New Contact button displays for Users with Contacts Write permission",function () {
        it("should check - New Contact button displays for Users with Contacts Write permission",function (browser) {
            browser.expect.element(elements.organizationPageElements.contactsPageNewContactLink).to.be.present;
        });
    });
};
exports.check_NewContact_Btn_Display = check_NewContact_Btn_Display;

var check_Contacts_SearchBox_Display = function () {
    describe("Contacts search box displays",function () {
        it("should check - Contacts search box displays",function (browser) {
            browser.expect.element(elements.organizationPageElements.contactsPageSearchInput).to.be.present;
        });
    });
};
exports.check_Contacts_SearchBox_Display = check_Contacts_SearchBox_Display;

var check_SearchBox_Returns_Contacts_Providers = function () {
    describe("Search box returns both contacts and providers",function () {
        it("should check - Search box returns both contacts and providers",function (browser) {
            browser.pause(4000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.contactsPageSearchInput,variables.createdContact);
            browser.pause(2000);
            browser.elements("xpath",elements.organizationPageElements.contactsPageSearchFirstResult, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function(text) {
                        expect(text.value.includes(variables.createdContact)).to.be.true;
                    });
                });
            });
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsPagePrimaryContactCheckbox);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsPageAssociateContactBtn);
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.contactsPagePaginationDiv).to.be.present;
        });
    });
};
exports.check_SearchBox_Returns_Contacts_Providers = check_SearchBox_Returns_Contacts_Providers;

var check_Contact_Name_Redirects = function () {
    describe("Contact name redirects to the Contact Detail screen",function () {
        it("should check - Contact name redirects to the Contact Detail screen",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.contactLName+','+variables.contactFName+'")]');
            browser.pause(1000);
            browser.expect.element(elements.contactsPageElements.contactsPageHeader).text.to.equal(variables.contactFName+" "+variables.contactMName+" "+variables.contactLName+" "+variables.contactSuffix);
        });
    });
};
exports.check_Contact_Name_Redirects = check_Contact_Name_Redirects;

var check_Contact_Phone_Redirects = function () {
    describe("Phone number field presents link that redirects to new browser",function () {
        it("should check - Phone number field presents link that redirects to new browser",function (browser) {
            browser.expect.element('//a[contains(.,"'+variables.contactPhone+'")]').to.be.present;
        });
    });
};
exports.check_Contact_Phone_Redirects = check_Contact_Phone_Redirects;

var check_Contact_Edit_Link = function () {
    describe("Edit' link presents with contact and provider Write permission",function () {
        it("should check - Edit' link presents with contact and provider Write permission",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.expect.element(elements.organizationPageElements.contactsPageRecordsListTable+'/thead/tr/th[1]').to.have.attribute('class').to.equals('edit');
        });
    });
};
exports.check_Contact_Edit_Link = check_Contact_Edit_Link;

var check_Contact_Delete_Link = function () {
    describe("Del' link presents with contact and provider Delete permission",function () {
        it("should check - Del' link presents with contact and provider Delete permission",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.contactsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.true;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Contact_Delete_Link = check_Contact_Delete_Link;

var check_Contacts_Icon = function () {
    describe("Ensure that the contacts and provider icons are displayed correctly",function () {
        it("should check - Ensure that the contacts and provider icons are displayed correctly",function (browser) {
            browser.expect.element(elements.organizationPageElements.contactsPageContactIcon).to.be.visible;
        });
    });
};
exports.check_Contacts_Icon = check_Contacts_Icon;

var check_Contact_Edit_Link_fail = function () {
    describe("Edit' link presents without contact and provider Write permission (Fail)",function () {
        it("should check - Edit' link presents without contact and provider Write permission (Fail)",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.contactsSubTab);
            browser.elements("xpath",elements.organizationPageElements.contactsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('edit')){
                            expect(text.value.includes('edit')).to.be.false;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Contact_Edit_Link_fail = check_Contact_Edit_Link_fail;

var check_Contact_Delete_Link_fail = function () {
    describe("Del' link presents without contact and provider Delete permission (Fail)",function () {
        it("should check - Del' link presents without contact and provider Delete permission (Fail)",function (browser) {
            browser.elements("xpath",elements.organizationPageElements.contactsPageRecordsListTable+'/thead/tr/th', function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            expect(text.value.includes('delete')).to.be.false;
                        }
                    });
                });
            });
        });
    });
};
exports.check_Contact_Delete_Link_fail = check_Contact_Delete_Link_fail;

var check_NewContact_Btn_Display_Fail = function () {
    describe("New Contact' button doesn't display for Users without Contacts Write permission (Fail)",function () {
        it("should check - New Contact' button doesn't display for Users without Contacts Write permission (Fail)",function (browser) {
            browser.expect.element(elements.organizationPageElements.contactsPageNewContactLink).to.not.be.present;
        });
    });
};
exports.check_NewContact_Btn_Display_Fail = check_NewContact_Btn_Display_Fail;

var check_Subtabs_Not_Display = function () {
    describe("Activities, Orders and Attachments tabs are not Displayed",function () {
        it("should check - Activities, Orders and Attachments tabs are not Displayed",function (browser) {
            browser.expect.element(elements.organizationPageElements.activitiesSubTabBtn).to.not.be.visible;
            browser.expect.element(elements.organizationPageElements.ordersSubTabBtn).to.not.be.visible;
            browser.expect.element(elements.organizationPageElements.attachmentsSubTabBtn).to.not.be.visible;
        });
    });
};
exports.check_Subtabs_Not_Display = check_Subtabs_Not_Display;

var check_Child_Subtab_Highlighted = function () {
    describe("Child Organizations Subtab is highlighted",function () {
        it("should check - Child Organizations Subtab is highlighted",function (browser) {
            browser.expect.element(elements.organizationPageElements.childOrgSubTab).to.have.attribute('class').which.contain('selected');
        });
    });
};
exports.check_Child_Subtab_Highlighted = check_Child_Subtab_Highlighted;

var check_Child_Org_Searchbox_Present = function () {
    describe("Add Child Org Search box presents",function () {
        it("should check - Add Child Org Search box presents",function (browser) {
            browser.expect.element(elements.organizationPageElements.childOrgPageChildOrgSearchInput).to.be.present;
        });
    });
};
exports.check_Child_Org_Searchbox_Present = check_Child_Org_Searchbox_Present;

var check_Add_Parent_Searchbox_Present = function () {
    describe("Add Parent Org Search box presents",function () {
        it("should check - Add Parent Org Search box presents",function (browser) {
            browser.expect.element(elements.organizationPageElements.childOrgPageParentOrgSearchInput).to.be.present;
        });
    });
};
exports.check_Add_Parent_Searchbox_Present = check_Add_Parent_Searchbox_Present;

var check_Add_Org_To_Child_Grid = function () {
    describe("Selecting an Org from the Child Org Search box adds Organization to the Child Org grid",function () {
        it("should check - Selecting an Org from the Child Org Search box adds Organization to the Child Org grid",function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.childOrgPageChildOrgSearchInput,variables.createdChildORG);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.createdORG+'")]').to.be.present;
        });
    });
};
exports.check_Add_Org_To_Child_Grid = check_Add_Org_To_Child_Grid;

var check_Add_Org_To_Parent_Grid = function () {
    describe("Selecting an Org from the Parent Org Search box adds Organization to the Parent Org grid",function () {
        it("should check - Selecting an Org from the Parent Org Search box adds Organization to the Parent Org grid",function (browser) {
            browser.pause(2000);
            commands.checkAndSetValue(browser,elements.organizationPageElements.childOrgPageParentOrgSearchInput,variables.defaultOrgName);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            browser.expect.element('//a[contains(.,"test")]').to.be.present;
        });
    });
};
exports.check_Add_Org_To_Parent_Grid = check_Add_Org_To_Parent_Grid;

var check_Org_Name_Redirects = function () {
    describe("Organization Name link redirects user to the Organization Detail screen",function () {
        it("should check - Organization Name link redirects user to the Organization Detail screen",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,'//a[contains(@title,"'+variables.createdORG+'")]');
            browser.pause(2000);
            browser.expect.element(elements.organizationPageElements.orgPageHeader).text.to.equal(variables.createdORG);
            browser.pause(2000);
            browser.back();
        });
    });
};
exports.check_Org_Name_Redirects = check_Org_Name_Redirects;

var check_Delete_Removes_Org = function () {
    describe("Organization grids Present DEL option - Selecting Link removes the Org from grid",function () {
        it("should check - Organization grids Present DEL option - Selecting Link removes the Org from grid",function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationTabLink);
            browser.pause(2000);
            var i = 1;
            var j = 1;
            var deleteColumnIndex = 0;
            browser.elements('xpath',elements.organizationPageElements.orgListTable+"/thead/tr/th",function (links) {
                var els = links.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function(text) {
                        if(text.value.includes('delete')){
                            deleteColumnIndex = i;
                        }
                        i = i +1;
                    });
                });
            });
            browser.elements('xpath',elements.organizationPageElements.orgListTable+"/tbody/tr/td[3]",function (links) {
                var els = links.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT,function (result) {
                        if(result.value.includes(variables.createdORG)){
                            if(j >= 1){
                                commands.checkAndPerform('click',browser,'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/table/tbody/tr['+j+']/td['+deleteColumnIndex+']/i');
                            }
                            else{
                                commands.checkAndPerform('click',browser,'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td['+deleteColumnIndex+']/i');
                            }
                        }
                        j = j+1;
                    });
                });
            });
            browser.pause(2000);
            browser.expect.element('//a[contains(@title,"'+variables.createdORG+'")]').to.not.be.present;
        });
    });
};
exports.check_Delete_Removes_Org = check_Delete_Removes_Org;


var to_SelectOrgTab_andOrgList = function () {
    describe("To open a Organization tab and select first org from list", function () {
        it("Should  be go on - To open a Organization tab and select first org from list", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"'+variables.createdORG+'")]');
        });
    });
};
module.exports.to_SelectOrgTab_andOrgList = to_SelectOrgTab_andOrgList;

var check_hoursOfOperation_display = function () {
    describe("Ensure that hours of operation is displayed", function () {
        it("Should check -Ensure that hours of operation is displayed", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationAndContactSubTab);
            browser.expect.element(elements.organizationPageElements.hoursOfOperationDiv).text.to.contains("Hours of Operation");

        });
    });
};
module.exports.check_hoursOfOperation_display = check_hoursOfOperation_display;

var check_timeFieldIsDisplay = function () {
    describe("To display time is add or not", function () {
        it("should check - To display time is add or not", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(4000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageHOOSundayDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgSaveBtn);
        });
    });
};
module.exports.check_timeFieldIsDisplay = check_timeFieldIsDisplay;

var check_setTime24hour = function () {
    describe("Set time to 24 hours ", function () {
        it("should check - Set time to 24 hours ", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationAndContactSubTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageHOOSundayDropdown);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgSaveBtn);
            browser.pause(3000);
        });
    });
};
module.exports.check_setTime24hour = check_setTime24hour;


var navigate_LocationAndContact = function () {
    describe("For Open a Location And Contact Tab", function () {
        it("Should check - For Open a Location And Contact Tab", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationAndContactSubTab);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(3000);
            browser.pause(3000);
        });
    });
};
module.exports.navigate_LocationAndContact = navigate_LocationAndContact;


var orgSaveBtnOperation = function () {
    describe("To save and exitst on same page  ", function () {
        it("Should check -To save and exitst on same page ", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageSaveBtn);
        });
    });
};
module.exports.orgSaveBtnOperation = orgSaveBtnOperation;


var add_maillingAddress_Street = function () {
    describe("Add Street1 address", function () {
        it("should check - Must add Street1 address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgAddressStreet1Input);
            browser.pause(3000);
            commands.checkAndSetValue(browser, elements.organizationPageElements.orgAddressStreet1Input, variables.mailing_streetValue);
            browser.pause(3000);

        });
    });
};
module.exports.add_maillingAddress_Street = add_maillingAddress_Street;

var add_maillingAddress_Street2 = function () {
    describe("Add Street2 address", function () {
        it("Should check - Must add Street2 address", function (browser) {
            commands.checkAndSetValue(browser, elements.organizationPageElements.orgAddressStreet2Input, variables.mailing_street2Value);
            browser.keys(browser.Keys.TAB);
            browser.pause(2000);
        });
    });
};
module.exports.add_maillingAddress_Street2 = add_maillingAddress_Street2;


var add_maillingAddress_City = function () {
    describe("Add City address", function () {
        it("Should check - Must add City address", function (browser) {
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageMailingCityInput, variables.mailing_cityValue);
            browser.keys(browser.Keys.TAB);
            browser.pause(2000);
        });
    });
};
module.exports.add_maillingAddress_City = add_maillingAddress_City;


var add_maillingAddress_State = function () {
    describe("Add State address", function () {
        it("Should check - Must Add State address", function (browser) {
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageMailingStateInput, variables.mailing_stateValue);
            browser.keys(browser.Keys.TAB);
            browser.pause(1000);
        });
    });
};
module.exports.add_maillingAddress_State = add_maillingAddress_State;


var add_maillingAddress_PostalCode = function () {
    describe("Add Postal Code", function () {
        it("Should check - Must add Postal Code", function (browser) {
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageMailingPostalInput, variables.mailling_postalCodeValue);
            browser.keys(browser.Keys.TAB);
            browser.pause(1000);
        });
    });
};
module.exports.add_maillingAddress_PostalCode = add_maillingAddress_PostalCode;


var add_maillingAddress_Country = function () {
    describe("Add Country", function () {
        it("Should check - Must Add Country", function (browser) {
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageMailingCountryInput, variables.mailling_countryValue);
            browser.keys(browser.Keys.TAB);
            browser.pause(2000);

        });
    });
};
module.exports.add_maillingAddress_Country = add_maillingAddress_Country;

// For uncheck toggle Btn

var toggel_Btn_On = function () {
    describe("Check toggle checkbox", function () {
        it("Should check - must check toggle checkbox", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageSameAsBillingCheckbox);
            browser.pause(5000);
        });
    });
};
module.exports.toggel_Btn_On = toggel_Btn_On;

var add_billing_street1Address = function () {
    describe("Add Street1 address", function () {
        it("Should check - Must add street1 address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingStreetInput);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingStreetInput, variables.mailing_streetValueBilling);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_street1Address = add_billing_street1Address;

var add_billing_street2Address = function () {
    describe("Add Street2 address", function () {
        it("Should check - Must add street2 address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingStreet2Input);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingStreet2Input, variables.mailing_streetValueBilling);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_street2Address = add_billing_street2Address;

var add_billing_AddressCity = function () {
    describe("Add city address", function () {
        it("Should check - must add city address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingCityInput);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingCityInput, variables.billing_cityValue);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_AddressCity = add_billing_AddressCity;


var add_billing_AddressState = function () {
    describe("Add state in billing address", function () {
        it("Should check - Must add state in billing address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingStateInput);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingStateInput, variables.billing_stateValue);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_AddressState = add_billing_AddressState;

var add_billing_AddressPostalCode = function () {
    describe("Add postal code in billing address", function () {
        it("Should check - Must add postal code in billing address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingPostalInput);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingPostalInput, variables.billing_postalCodeValue);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_AddressPostalCode = add_billing_AddressPostalCode;

var add_billing_AddressCountry = function () {
    describe("Add country code in billing address", function () {
        it("Should check - Must add country code in billing address", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.locationPageBillingCountryInput);
            commands.checkAndSetValue(browser, elements.organizationPageElements.locationPageBillingCountryInput, variables.billing_CounrValue);
            browser.keys(browser.Keys.TAB);
        });
    });
};
module.exports.add_billing_AddressCountry = add_billing_AddressCountry;

var editBtn_LocationAndContact = function () {
    describe("For Edit Location And Contact Tab", function () {
        it("Should check - For Edit Location And Contact Tab", function (browser) {
            commands.checkAndPerform('click', browser, elements.organizationPageElements.orgDetailPageEditBtn);
            browser.pause(3000);
        });
    });
};
module.exports.editBtn_LocationAndContact = editBtn_LocationAndContact;

var edit_BillingStreet = function () {
    describe("Check street1 address for mailing and billing are same", function () {
        it("should check - street1 address for mailing and billing are same", function (browser) {
            browser.expect.element(elements.organizationPageElements.locationPageBillingStreetDiv).text.to.equal(variables.mailing_streetValue);
        });
    });
};
module.exports.edit_BillingStreet = edit_BillingStreet;

var edit_BillingStreet2 = function () {
    describe("Check street2 address for mailing and billing are same", function () {
        it("should check - street2 address for mailing and billing are same", function (browser) {
            browser.expect.element(elements.organizationPageElements.locationPageBillingStreet2Div).text.to.equal(variables.mailing_street2Value);
        });
    });
};
module.exports.edit_BillingStreet2 = edit_BillingStreet2;

var edit_BillingCity = function () {
    describe("Check city address for mailing and billing are same", function () {
        it("should check - city address for mailing and billing are same", function (browser) {
           browser.expect.element(elements.organizationPageElements.locationPageBillingCityDiv).text.to.equal(variables.mailing_cityValue);
        });
    });
};
module.exports.edit_BillingCity = edit_BillingCity

var edit_BillingState = function () {
    describe("Check state address for mailing and billing are same", function () {
        it("should check - state address for mailing and billing are same", function (browser) {
           browser.expect.element(elements.organizationPageElements.locationPageBillingStateDiv).text.to.equal(variables.mailing_stateValue);
        });
    });
};
module.exports.edit_BillingState = edit_BillingState;

var edit_BillingPostal = function () {
    describe("Check postal code address for mailing and billing are same", function () {
        it("should check - postal code address for mailing and billing are same", function (browser) {
            browser.expect.element(elements.organizationPageElements.locationPageBillingPostalDiv).text.to.equal(variables.mailling_postalCodeValue);
        });
    });
};
module.exports.edit_BillingPostal = edit_BillingPostal

var edit_BillingCountry = function () {
    describe("Check country address for mailing and billing are same", function () {
        it("should check - country address for mailing and billing are same", function (browser) {
          browser.expect.element(elements.organizationPageElements.locationPageBillingCountryDiv).text.to.equal(variables.mailling_countryValue);
        });
    });
};
module.exports.edit_BillingCountry = edit_BillingCountry

var check_Delete_Icon_Without_Permission_ForPanels = function () {
    describe("Panels - Delete icon is present without delete permission (fail)", function () {
        it("Panels - Delete icon is present without delete permission (fail)", function (browser) {

            commands.checkAndPerform('click', browser, elements.organizationPageElements.feeSchedulesSubTab);
            browser.elements("xpath", elements.organizationPageElements.feeSchedulesSubTab, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT, 'class', function (text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        })
    })
};
module.exports.check_Delete_Icon_Without_Permission_ForPanels = check_Delete_Icon_Without_Permission_ForPanels;

var check_Delete_Icon_Without_Permission_ForFeesSchedule = function () {
    describe("Fee Schedule - Delete icon is present without delete permission (fail)", function () {
        it("Should  check - Fee Schedule - Delete icon is present without delete permission (fail)", function (browser) {
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.newOrgName + '")]');
            commands.checkAndPerform('click', browser, elements.organizationPageElements.feeSchedulesSubTab);
            browser.elements("xpath", elements.organizationPageElements.feeSchedulesSubTab, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT, 'class', function (text) {
                        expect(text.value.includes('delete')).to.be.false;
                    });
                });
            });
        });
    });
};
module.exports.check_Delete_Icon_Without_Permission_ForFeesSchedule = check_Delete_Icon_Without_Permission_ForFeesSchedule;

var check_deleteIconPersent = function () {
    describe("Panels - Delete icon is present with delete permission ", function () {
        it("Should Panels - Delete icon is present with delete permission ", function (browser) {

            var i = 1;
            browser.elements("xpath", elements.organizationPageElements.deletePanel, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        if (text.value === variables.nameFeesSchedule) {
                            if (i > 1) {
                                browser.expect.element(elements.organizationPageElements.deletePanel + '/span[' + i + ']/i').to.be.present;
                            }
                            else {
                                browser.expect.element(elements.organizationPageElements.deletePanel + '/span/i').to.be.present;
                            }
                        }
                        i = i + 1;
                    });
                });
            });
            browser.pause(3000);

        });
    });
};
module.exports.check_deleteIconPersent = check_deleteIconPersent;

var add_Multiple_feeSchedule = function () {
    describe("Add multiple fee Schedules to an Org ", function () {
        it("should check - Add multiple fee Schedules to an Org", function (browser) {

            browser.pause(3000);
            commands.checkAndSetValue(browser, elements.organizationPageElements.searchFieldFeeSchedule, variables.nameFeesSchedule);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);

            commands.checkAndSetValue(browser, elements.organizationPageElements.searchFieldFeeSchedule2, variables.nameFeesScheduleName2);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.pause(2000);
            browser.keys(browser.Keys.ENTER);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.panelSelectField, variables.panelValue);   // Those panel is not Assosiated but try to add
            // This test cover (Grid displays Host code, Name, Abbreviation, Fee Schedule name, and Panel price)

            browser.expect.element(elements.organizationPageElements.hostCodFeeScheduleMenu).to.be.present;
            browser.expect.element(elements.organizationPageElements.nameFeeScheduleMenu).to.be.present;
            browser.expect.element(elements.organizationPageElements.abbreviationFeeScheduleMenu).to.be.present;
            browser.expect.element(elements.organizationPageElements.listPriceFeeScheduleMenu).to.be.present;
            browser.expect.element(elements.organizationPageElements.feeScheduleMenu).to.be.present;

            browser.expect.element(elements.organizationPageElements.panelSearchBox).to.be.present;
            browser.expect.element(elements.organizationPageElements.panelDeleteIcon).to.be.present;
        });
    });
};
module.exports.add_Multiple_feeSchedule = add_Multiple_feeSchedule;

var editBtn_ForOrg_GeneralTab = function () {
    describe("For Edit Organization General Subtab", function () {
        it("Should check - For Edit Organization General Subtab", function (browser) {
            commands.checkAndPerform('click', browser, elements.globalElements.editOrgTab);
            browser.pause(3000);
        });
    });
};
module.exports.editBtn_ForOrg_GeneralTab = editBtn_ForOrg_GeneralTab;

var check_FeeScheduleTab_isHighlated = function () {
    describe(" Fee Schedule Tab is highlated", function () {
        it("Should check - Fee Schedule Tab is highlated", function (browser) {
            browser.expect.element(elements.organizationPageElements.orgFeeSchedulesTab).to.have.attribute('class').which.contain('selected');
        });
    });
};
module.exports.check_FeeScheduleTab_isHighlated = check_FeeScheduleTab_isHighlated;

var navigate_To_Panels_Tab = function () {
    describe("Clicking on Panels link in Admin to check the functionality", function () {
        it("should Click - Clicking on Panels link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.panelsLink);
            browser.pause(3000);
        });
    });
};
module.exports.navigate_To_Panels_Tab = navigate_To_Panels_Tab;



var create_FeeSchedule = function () {
    describe("To create a feesSchedule in panel section and also add panel ", function () {
        it("Should - To create a feesSchedule in panel section and also add panel ", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerMenu);
            browser.pause(4000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerAdminOption);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.panelsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.feeScheduleAdmin);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.feeScheduleLinkSchedule);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.nameInputFeeSchedule, variables.nameFeesSchedule);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.descriptionInputFeeSchedule, variables.descriptionFeesSchedule);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createBtnFeeSchedule);
            browser.pause(2000);
            browser.expect.element(elements.panelsAdminSectionElements.tBodyFeeScheduleTab).text.to.contains(variables.nameFeesSchedule);
            browser.pause(3000);

            // For 2nd time fees schedule creating
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.feeScheduleLinkSchedule);
            browser.pause(3800);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.nameInputFeeSchedule, variables.nameFeesScheduleName2);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.descriptionInputFeeSchedule, variables.descriptionFeesSchedule)
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createBtnFeeSchedule);

            browser.pause(3000);
            commands.checkAndPerform('click', browser, '//a[contains(@title,"' + variables.nameFeesScheduleName2 + '")]');
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.addPanel);
            browser.pause(2000);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.panelNameInput, variables.panelsNameValue);
            browser.pause(2000);
            browser.keys(browser.Keys.DOWN_ARROW);
            browser.keys(browser.Keys.ENTER);
            commands.checkAndSetValue(browser, elements.panelsAdminSectionElements.panelPriceInput, variables.panelPriceValue);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.createBtnPanel);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.panelsAdminSectionElements.saveBtnPanelsCreatedPage);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.organizationTabLink);
        });
    });
};
module.exports.create_FeeSchedule = create_FeeSchedule;

var navigate_To_FeeSchedule_Subtab = function () {
    describe("Navigate to Fees And Schedule Subtab", function () {
        it("should check - Navigate to Fees And Schedule subtab", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.organizationPageElements.feeSchedulesSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_FeeSchedule_Subtab = navigate_To_FeeSchedule_Subtab;

var pauseBrowserSomeTime = function () {
    describe("", function () {
        it("", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.globalElements.goToHomeIcon);
            browser.pause(20000);
        });
    });
};
exports.pauseBrowserSomeTime = pauseBrowserSomeTime;

//TEST CASE DECLARATION - END







//FUNCTIONS DECLARATION - STARTS

var organizationTab_basic = function () {
  describe("ORGANIZATION TAB - BASIC",function () {
          me.pauseBrowserSomeTime();
          me.navigate_To_Org_Tab();                     //Clicking the Organization Tab navigates to the Organization List Screen
          me.check_Org_Tab_Highlighted();               //Organization Tab is highlighted
          me.check_CustomList_Defaults_To_All();        //Custom List is defaulted to first list in oder (Defaults to All)
          me.check_DisplayColumns_Is_Hidden();          //Display Columns is hidden (Default), with Drop-down arrow
          me.check_DisplayColumns_Is_Visible();         //Clicking display Column Drop Arrow expands to display available fields
          me.navigate_To_Org_Tab();
          me.check_Create_Org_User_Btn_Visible();       //User With Organization Write Access Can Create New Org
          me.check_Edit_Link_Visible();                 //Edit Link in Grid displays for Users with Write and Delete Permissions on Org
          me.check_Delete_Link_Visible();               //DEL Link in Grid Displays for Users with Delete Permission on Orgs
          userAuthentication.logout();
          userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
          me.navigate_To_Org_Tab();
          me.check_Create_Org_User_Btn_Hidden();        //Create New Button does not display for user with Read only access
          userAuthentication.logout();
          userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
  });
};
module.exports.organizationTab_basic = organizationTab_basic;

var organizationTab_customList = function () {
    describe("ORGANIZATION TAB - CUSTOM LIST",function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_Org_Tab();                                                                       //Clicking the Organization Tab navigates to the Organization List Screen

        commonOperations.check_Filters_Behave_Properly(elements.organizationPageElements.quickFilterLink,
                            elements.organizationPageElements.orgListTable,
                            elements.organizationPageElements.filterAvailableColumnsDiv,
                            elements.organizationPageElements.filterSelectedColumnsDiv,
                            elements.organizationPageElements.filterAddColumnBtn,
                            elements.organizationPageElements.filterRemoveColumnBtn,
                            elements.organizationPageElements.quickFilterApplyBtn);                    //Ensure that  Quick Filter behaves properly (check with all the filters)

            //this test case is covered in create_New_Custom_List                                      //Ensure that  Columns behaves properly (check with all the fields)
        commonOperations.create_New_Custom_List(elements.organizationPageElements.settingsIcon,
                            elements.organizationPageElements.newCustomListLink,
                            elements.organizationPageElements.orderCustomListLink,
                            elements.organizationPageElements.newCustomListInput,
                            elements.organizationPageElements.createNewCustomListSaveBtn,
                            elements.organizationPageElements.orderedCustomListDisplay,
                            elements.organizationPageElements.orderListCloseIcon,
                            elements.organizationPageElements.addColumnBtn,
                            elements.organizationPageElements.removeColumnBtn,
                            elements.organizationPageElements.availableColumnsDiv,
                            elements.organizationPageElements.selectedColumnsDiv,
                            elements.organizationPageElements.orgListTable);                      //Create new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.settingsIcon,
                            elements.organizationPageElements.editCustomListLink,
                            elements.organizationPageElements.deleteCustomListLink,
                            elements.organizationPageElements.newCustomListInput,
                            elements.organizationPageElements.createNewCustomListSaveBtn,
                            elements.organizationPageElements.orderCustomListLink,
                            elements.organizationPageElements.orderedCustomListDisplay,
                            elements.organizationPageElements.orderListCloseIcon);                      //Edit  existing custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.settingsIcon,
                            elements.organizationPageElements.cloneCustomListInput,
                            elements.organizationPageElements.cloneCustomListLink,
                            elements.organizationPageElements.cloneCustomListSaveBtn,
                            elements.organizationPageElements.orderCustomListLink,
                            elements.organizationPageElements.orderedCustomListDisplay,
                            elements.organizationPageElements.orderListCloseIcon);                      //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.settingsIcon,
            elements.organizationPageElements.editCustomListLink,
            elements.organizationPageElements.deleteCustomListLink,
            elements.organizationPageElements.orderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                                      //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.customListPagingDiv);                 //Ensure that the paging is displayed correctly
    });
};
module.exports.organizationTab_customList = organizationTab_customList;

var organizationDetail_OrdersSubTab_customList = function () {
    describe("ORGANIZATION ORDERS SUBTAB - CUSTOM LIST", function () {
        me.navigate_To_New_Org_Page();
        me.navigate_To_OrdersSubTab();
        commonOperations.create_New_Custom_List(elements.organizationPageElements.ordersPageSettingsIcon,
            elements.organizationPageElements.ordersPageNewCustomListLink,
            elements.organizationPageElements.ordersPageOrderCustomListLink,
            elements.organizationPageElements.ordersPageEditCustomListInput,
            elements.organizationPageElements.createNewCustomListSaveBtn,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon,
            elements.organizationPageElements.ordersPageCustomListAddBtn,
            elements.organizationPageElements.ordersPageCustomListRemoveBtn,
            elements.organizationPageElements.ordersPageCustomListAvailColumns,
            elements.organizationPageElements.ordersPageCustomListSelectedColumns,
            elements.organizationPageElements.ordersPageRecordsListTable);                                // Create new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.ordersPageSettingsIcon,
            elements.organizationPageElements.ordersPageEditCustomListLink,
            elements.organizationPageElements.ordersPageDeleteLink,
            elements.organizationPageElements.ordersPageEditCustomListInput,
            elements.organizationPageElements.createNewCustomListSaveBtn,
            elements.organizationPageElements.ordersPageOrderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //Edit  existing custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.ordersPageSettingsIcon,
            elements.organizationPageElements.ordersPageCloneListInput,
            elements.organizationPageElements.ordersPageCloneCustomListLink,
            elements.organizationPageElements.ordersPageCloneCustomListCreateBtn,
            elements.organizationPageElements.ordersPageOrderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.ordersPageSettingsIcon,
            elements.organizationPageElements.ordersPageEditCustomListLink,
            elements.organizationPageElements.ordersPageDeleteLink,
            elements.organizationPageElements.ordersPageOrderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.ordersPagePagingDiv);                 // Ensure that the paging is displayed correctly
    });
};
exports.organizationDetail_OrdersSubTab_customList = organizationDetail_OrdersSubTab_customList;


var createOrganization_createNewOrg = function () {
    describe("ORGANIZATION TAB - CREATE NEW ORGANIZATION",function () {
        me.pauseBrowserSomeTime();
        me.navigate_To_Org_Tab();
        me.check_Create_Org_User_Btn_Visible();         // Create new org link is present with org write permission
        me.check_Create_Org_Modal_Display();            // Create new organization modal is displayed
        me.check_Create_Org_Input_Required();           // Organization name is a required field
        me.check_Existing_Organization_List_Behaviour();// Ensure that existing organization list behaves properly
        // me.create_New_Org();                            // Enter a organization name and select create
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_Org_Tab();
        me.check_Create_Org_User_Btn_Hidden();          // Create new org link is present without org write permission (fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
module.exports.createOrganization_createNewOrg = createOrganization_createNewOrg;

var createOrganization_AddSalesRep = function () {
    describe("ORGANIZATION TAB - ADD A SALES REP POP-UP",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        // adminOperations.create_New_Control_Tree();      //create new control tree and associate username
        me.navigate_To_New_Org_Page();
        me.check_Sales_Rep_Is_Auto_Populated();         //sales rep is autopopulated (if the creator is a sales rep)
        me.check_Sales_Territory_Association();         //List of sales territory user is assoicated with is listed
        me.check_Sales_Territory_Not_In_Association();  // List of sales territory user is not assoicated with is listed  (fail)
        me.select_Sales_Territory_AndVerify_Name();     // User should be able to select from the list of sales territories
    });
};
module.exports.createOrganization_AddSalesRep =  createOrganization_AddSalesRep;

var organizationDetail_settingsIcon = function () {
    describe("ORGANIZATION DETAIL - SETTINGS ICON",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_Host_Code_Display();                   // Host Codes display (if present)
        me.check_Host_Code_Is_Editable();               //Users with Host Code Permission can add / remove Host codes
        me.check_UAC_Node_Search_Box_Visible();         //UAC Node Search Box displays (with permissions)
        me.check_UAC_Node_Search_Box_Editable();        //Users with UAC Permissions can Add/ remove UAC Nodes
        me.check_Accessioning_Toggle_Displays();        //Accessioning Location Toggle displays (defaults to False)
        me.check_Collection_Location_Toggle_Displays(); //Collection Location Toggle displays (defaults to False)
        me.check_Ordering_Location_Toggle_Displays();   //Ordering Location Toggle displays (defaults to True)
    });
};
module.exports.organizationDetail_settingsIcon = organizationDetail_settingsIcon;

var organizationDetail_vCard = function () {
    describe("ORGANIZATION DETAIL - V-CARD",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_VCard_Display();                       //Confirm that the data is displayed correctly in the v-card
        me.check_VCard_Data_Display();                  //Edit field and ensure that the data is displayed correctly in the v-card
    });
};
module.exports.organizationDetail_vCard = organizationDetail_vCard;

var organizationDetail_ActivitiesSubTab = function () {
    describe("ORGANIZATION DETAIL - ACTIVITIES SUBTAB",function () {
        me.pauseBrowserSomeTime();
        me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_Activities_List_Display();                                                             // Ensure that the list of activities are displayed properly
        me.check_Edit_Icon_Display();                                                                   // edit icon is displayed if the user has write permission
        me.check_Delete_Icon_Display(elements.organizationPageElements.activitiesListTable);                                                                 // delete icon is displayed if the user has del permission
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_Org_Tab();
        me.check_Edit_Icon_Display_Fail();                                                              // edit icon not displayed without write permission (fail)
        me.check_Delete_Icon_Display_Fail();                                                            // del icon is displayed without del permission (fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
module.exports.organizationDetail_ActivitiesSubTab = organizationDetail_ActivitiesSubTab;

var organizationDetail_ActivitiesSubTab_customList = function () {
    describe("ORGANIZATION ACTIVITIES SUBTAB - CUSTOM LIST", function () {
        me.navigate_To_New_Org_Page();
        me.navigate_To_Activities_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.organizationPageElements.quickFilterLinkActivities,
            elements.organizationPageElements.activitiesListTable,
            elements.organizationPageElements.filterAvailableColumnsDiv,
            elements.organizationPageElements.filterSelectedColumnsDiv,
            elements.organizationPageElements.filterAddColumnBtn,
            elements.organizationPageElements.filterRemoveColumnBtn,
            elements.organizationPageElements.quickFilterApplyBtn);                     // Ensure that activities Quick Filter behaves properly (check with all the filters)

        commonOperations.check_Columns_Behave_Properly(elements.organizationPageElements.settingsIconActivities,
            elements.organizationPageElements.activitiesListTable,
            elements.organizationPageElements.availableColumnsDiv,
            elements.organizationPageElements.selectedColumnsDiv,
            elements.organizationPageElements.newCustomListLink,
            elements.organizationPageElements.addColumnBtn,
            elements.organizationPageElements.removeColumnBtn,
            elements.organizationPageElements.newCustomListInput);                       // Ensure that activities Display Columns behaves properly (check with all the fields)

        commonOperations.create_New_Custom_List(elements.organizationPageElements.settingsIconActivities,
            elements.organizationPageElements.newCustomListLink,
            elements.organizationPageElements.orderCustomListLink,
            elements.organizationPageElements.newCustomListInput,
            elements.organizationPageElements.createNewCustomListSaveBtn,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon,
            elements.organizationPageElements.addColumnBtn,
            elements.organizationPageElements.removeColumnBtn,
            elements.organizationPageElements.availableColumnsDiv,
            elements.organizationPageElements.selectedColumnsDiv,
            elements.organizationPageElements.activitiesListTable);                                //Create new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.settingsIconActivities,
            elements.organizationPageElements.editCustomListLink,
            elements.organizationPageElements.deleteCustomListLink,
            elements.organizationPageElements.newCustomListInput,
            elements.organizationPageElements.createNewCustomListSaveBtn,
            elements.organizationPageElements.orderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //Edit  existing custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.settingsIconActivities,
            elements.organizationPageElements.cloneCustomListInputActivities,
            elements.organizationPageElements.cloneCustomListLink,
            elements.organizationPageElements.cloneCustomListSaveBtn,
            elements.organizationPageElements.orderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.settingsIconActivities,
            elements.organizationPageElements.editCustomListLink,
            elements.organizationPageElements.deleteCustomListLink,
            elements.organizationPageElements.orderCustomListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                      //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.activitiesListPagingDiv);             // Ensure that the paging is displayed correctly
    });
};
exports.organizationDetail_ActivitiesSubTab_customList = organizationDetail_ActivitiesSubTab_customList;

var organizationDetail_OrdersSubTab = function () {
    describe("ORGANZIATION DETAIL - ORDERS SUBTAB",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_Date_Dropdown_present();                                                               // List Date Range drop down is present
        // me.check_Filter(date);                                                                   // Filter by OrderDate - Ensure that the correct results are displayed
        // me.check_Filter(accessioned_Date);                                                       // Filter by Accessioned Date  - Ensure that the correct results are displayed
        // me.check_Filter(result_Date);                                                            // Filter by Result Date  - Ensure that the correct results are displayed
        // me.check_Filter(collection_Date);                                                        // Filter by Collection Date  - Ensure that the correct results are displayed
        me.check_Start_Date_Is_Calendar();                                                              // Start date is a calendar - Ensure that the user can filter by date
        me.check_Range_Slide_Behaviour();                                                               // Ensure that Range slide bar behaves properly

        me.check_Orders_Filter_Behaviour();                                                             // Ensure that Quick Filter behaves properly (check with all the filters)
    });
};
module.exports.organizationDetail_OrdersSubTab = organizationDetail_OrdersSubTab;

var organizationDetail_AttachmentsSubTab = function () {
    describe("ORGANIZATION DETAIL - ATTACHMENTS SUBTAB",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_Add_Attachment();                                  // Add an attachment with User with write Attachment permission
        me.check_Download_Attachment();                             // Download Attachment with user with read permission
        me.check_Delete_Attachment();                               // Delete an Attachment with user with Delete Attachment permissions
        me.check_Add_Attachment_With_PHI();                                  // Add an attachment with User with write Attachment permission
        me.check_Download_Attachment_With_PHI();                             // Download Attachment with user with read permission


        me.check_Attachment_Pop_Up_Display();                       // Ensure that attachment pop-up is displayed
        me.check_Name_And_File_Required();                          // Name and file are required fields
        me.check_Add_Description();                                 // Add description
        me.check_PHI_CheckBox_Present();                            // PHI checkbox is present
        // This test case is covered in check_PHI_CheckBox_Present  // Add a attachment with PHI

        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_Org_Tab();
        me.navigate_To_New_Org_Page();
        me.check_Add_Attachment_Fail();                             // Add an attachment with User without write Attachment permission (Fail)
        me.check_Delete_Attachment_Fail();                          // Delete an Attachment with user without Delete Attachment permissions (Fail)
        me.check_Download_Attachment_Fail();                        // Download Attachment with user without read permission (Fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
module.exports.organizationDetail_AttachmentsSubTab = organizationDetail_AttachmentsSubTab;

var organizationDetail_AttachmentsSubTab_customList = function () {
    describe("ORGANIZATION DETAIL - ATTACHMENTS SUBTAB CUSTOM LIST",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_Attachments_Subtab();
        commonOperations.check_Filters_Behave_Properly(elements.organizationPageElements.attachmentsPageQuickFilterLink,
            elements.organizationPageElements.attachmentsPageRecordsListTable,
            elements.organizationPageElements.attachmentsPageFilterAvailColumns,
            elements.organizationPageElements.attachmentsPageFilterSelectedColumns,
            elements.organizationPageElements.attachmentsPageFilterAddBtn,
            elements.organizationPageElements.attachmentsPageFilterRemoveBtn,
            elements.organizationPageElements.attachmentsPageFilterApplyBtn);                    // Ensure that Quick Filter behaves properly (check with all the filters)

        commonOperations.create_New_Custom_List(elements.organizationPageElements.attachmentsPageSettingsIcon,
            elements.organizationPageElements.attachmentsPageCustomListNewListLink,
            elements.organizationPageElements.attachmentsPageCustomListOrderListLink,
            elements.organizationPageElements.attachmentsPageCustomListNewListInput,
            elements.organizationPageElements.attachmentsPageNewCustomListSaveBtn,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon,
            elements.organizationPageElements.attachmentsPageCustomListAddColBtn,
            elements.organizationPageElements.attachmentsPageCustomListRemoveColBtn,
            elements.organizationPageElements.attachmentsPageCustomListAvailColumns,
            elements.organizationPageElements.attachmentsPageCustomListSelectedColumns,
            elements.organizationPageElements.attachmentsPageRecordsListTable);                                        // Create new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.attachmentsPageSettingsIcon,
            elements.organizationPageElements.attachmentsPageCustomListEditLink,
            elements.organizationPageElements.attachmentsPageDeleteListLink,
            elements.organizationPageElements.attachmentsPageCustomListNewListInput,
            elements.organizationPageElements.attachmentsPageNewCustomListSaveBtn,
            elements.organizationPageElements.attachmentsPageCustomListOrderListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                              // Edit existing custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.attachmentsPageSettingsIcon,
            elements.organizationPageElements.attachmentsPageCloneListInput,
            elements.organizationPageElements.attachmentsPageCustomListCloneLink,
            elements.organizationPageElements.attachmentsPageCloneListCreateBtn,
            elements.organizationPageElements.attachmentsPageCustomListOrderListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                              //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.attachmentsPageSettingsIcon,
            elements.organizationPageElements.attachmentsPageCustomListEditLink,
            elements.organizationPageElements.attachmentsPageDeleteListLink,
            elements.organizationPageElements.attachmentsPageCustomListOrderListLink,
            elements.organizationPageElements.orderedCustomListDisplay,
            elements.organizationPageElements.orderListCloseIcon);                              //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.attachmentsPagePagingDiv);    // Ensure that the paging is displayed correctly
    });
};
module.exports.organizationDetail_AttachmentsSubTab_customList = organizationDetail_AttachmentsSubTab_customList;

var cloud_Search = function () {
    describe("ORGANIZATION CLOUD SEARCH",function () {
        me.pauseBrowserSomeTime();
        // adminOperations.create_New_Control_Tree();
        // me.create_New_Org();                 //Create New Organization
        me.navigate_To_New_Org_Page();
        me.check_Sales_Rep_Is_Auto_Populated();
        me.check_Sales_Territory_Association()
        me.select_Sales_Territory_AndVerify_Name();
         me.search_New_Org();                //Searches Above Created Organization On Cloud

         me.navigate_To_Org_Tab();           //Clicking the Organization Tab navigates to the Organization List Screen
         me.search_Existing_Org();           //Searches Existing Organization On Cloud

         me.create_Case_From_Search();       //Creates A New Case From Cloud Search

         me.navigate_To_Org_Tab();           //Clicking the Organization Tab navigates to the Organization List Screen
         me.search_Existing_Org();           //Searches Existing Organization On Cloud
         me.create_Task_From_Search();       //Creates A New Task From Cloud Search

         me.navigate_To_Org_Tab();           //Clicking the Organization Tab navigates to the Organization List Screen
         me.search_Existing_Org();           //Searches Existing Organization On Cloud
         me.create_Memo_From_Search();       //Creates A New Memo From Cloud Search

        me.navigate_To_Org_Tab();           //Clicking the Organization Tab navigates to the Organization List Screen
        me.search_New_Org();                //Searches Existing Organization On Cloud
        me.create_Opportunity_From_Search();//Creates A New Opportunity From Cloud Search

        me.navigate_To_Org_Tab();            //Clicking the Organization Tab navigates to the Organization List Screen
        me.update_Existing_Org_And_Search();

        me.navigate_To_Org_Tab();            //Clicking the Organization Tab navigates to the Organization List Screen
        me.search_Existing_Org();            //Searches Existing Organization On Cloud
        me.copy_MetaTag_From_Search();       //Copies Meta Tag From Cloud
    });
};
module.exports.cloud_Search =  cloud_Search;

var organizationDetail_MessagesSubTab = function () {
    describe("ORGANIZATION DETAIL - MESSAGES SUBTAB",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_Messages_Subtab();
        me.check_Message_List_Display();                                                               // Ensure that message list view is displayed properly
        messageOperations.send_Message_With_Org();
        me.navigate_To_Org_Tab();
        me.navigate_To_New_Org_Page();
        me.check_Delete_Icon_Display(elements.organizationPageElements.messagePageRecordsListTable);   // Ensure that delete icon is displayed if user has delete permission
        me.check_General_Messages_Listed();                                                            // Ensure that general messages are listed
    });
};
module.exports.organizationDetail_MessagesSubTab = organizationDetail_MessagesSubTab;

var organizationDetail_MessagesSubTab_CustomList = function () {
    describe("ORGANIZATION DETAIL - MESSAGES SUBTAB CUSTOM LIST",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_Messages_Subtab();
        me.check_Message_Filters_Behave_Properly();
        me.check_Message_Columns_Behave_Properly();

        commonOperations.create_New_Custom_List(elements.organizationPageElements.messagePageSettingsIcon,
            elements.organizationPageElements.messagePageCustomListNewLink,
            elements.organizationPageElements.messagePageCustomListOrderLink,
            elements.organizationPageElements.messagePageNewListInput,
            elements.organizationPageElements.messagePageNewCustomListSaveBtn,
            elements.organizationPageElements.messagePageOrderList,
            elements.organizationPageElements.messagePageOrderListCloseBtn,
            elements.organizationPageElements.messagePageCustomListAddBtn,
            elements.organizationPageElements.messagePageCustomListRemoveBtn,
            elements.organizationPageElements.messagePageCustomListAvailColumns,
            elements.organizationPageElements.messagePageCustomListSelectedColumns,
            elements.organizationPageElements.messagePageRecordsListTable);                                  // Create new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.messagePageSettingsIcon,
            elements.organizationPageElements.messagePageCustomListEditLink,
            elements.organizationPageElements.messagePageCustomListDeleteBtn,
            elements.organizationPageElements.messagePageNewListInput,
            elements.organizationPageElements.messagePageNewCustomListSaveBtn,
            elements.organizationPageElements.messagePageCustomListOrderLink,
            elements.organizationPageElements.messagePageOrderList,
            elements.organizationPageElements.messagePageOrderListCloseBtn);                 // Edit existing custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.messagePageSettingsIcon,
            elements.organizationPageElements.messagePageCloneListInput,
            elements.organizationPageElements.messagePageCustomListCloneLink,
            elements.organizationPageElements.messagePageCloneListCreateBtn,
            elements.organizationPageElements.messagePageCustomListOrderLink,
            elements.organizationPageElements.messagePageOrderList,
            elements.organizationPageElements.messagePageOrderListCloseBtn);                //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.messagePageSettingsIcon,
            elements.organizationPageElements.messagePageCustomListEditLink,
            elements.organizationPageElements.messagePageCustomListDeleteBtn,
            elements.organizationPageElements.messagePageCustomListOrderLink,
            elements.organizationPageElements.messagePageOrderList,
            elements.organizationPageElements.messagePageOrderListCloseBtn);                //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.messagePagePaginationDiv);// Ensure that the paging is displayed correctly
    });
};
module.exports.organizationDetail_MessagesSubTab_CustomList = organizationDetail_MessagesSubTab_CustomList;

var organizationDetail_LocationSubTab_OfficeInfo = function () {
    describe("ORGANIZATION DETAIL - LOCATION SUBTAB OFFICE INFO",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_Location_Subtab();
        me.check_Subtabs_Display();                     							// Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed
        me.check_Address_Tab_Highlighted();             							// Address Subtab is highlighted
        me.navigate_Address_Tab_Edit_Mode();            							// Navigate to the Address Subtab in Edit Mode
        me.address_add_Phone();                         							// Add Phone
        me.address_Add_Fax();                           							// Add Fax
        me.address_Add_Website();                       							// Add Website
        me.address_Add_Desc();                          							// Add description
        me.save_Location_And_Check(false);
        me.address_Edit_Phone();                        							// Edit Phone
        me.address_Edit_Fax();                          							// Edit Fax
        me.address_Edit_Website();                      							// Edit Website
        me.address_Edit_Desc();                         							// Edit Description
        me.save_Location_And_Check(true);
        me.check_Website_Redirects();                							   // Confirm that Website redirects to the website
    });
};
module.exports.organizationDetail_LocationSubTab_OfficeInfo = organizationDetail_LocationSubTab_OfficeInfo;

var organizationTab_generalTab = function () {
    describe("ORGANIZATION TAB - GENERAL TAB", function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();                                    					// To create new Organization
        me.navigate_To_New_Org_Page();
        me.check_General_Tab_Highlighted();                     					// General tab is highlighted
        me.add_Org_Number();                                    					// Add Organization number
        me.add_Multiple_Specialties();                          					// Add Multiple
        me.removeOrg_Specialties();                             					// To Remove Specialities in Org General tab
        //This test case is already covered in createOrganization_AddSalesRep()     // Confirm that Sales Reps associated with Sales Territory are added automatically
        //This test case is already covered in createOrganization_AddSalesRep()     // Add Multiple Sales Territories
        //This test case is already covered in createOrganization_AddSalesRep()     // Confirm that Sales Reps associated with Sales Territory are added automatically
        //This test case is already covered in createOrganization_AddSalesRep()     // Remove a Sales Territory
        me.add_Relationship_Mnr();                              					// Add Relationship Mgr associated to the Sales Territory
        me.edit_OrgType();                                      					// To edit Org type in Org General tab
        me.add_organizationType();                              					// To add Org type in Org General tab
        me.add_Primary_Contact();                               					// Add a Primary contact in the Associated Sales Territory
        me.check_Contact_Redirects();                           					// Ensure that the user is brought to the contact detail page when the user selects the contact icon or name
        me.navigate_To_New_Org_Page();
        me.create_Case_fromOrgGeneralTab();                     					// To create a Case in Org General tab
        me.create_Task_fromOrgGeneralTab();                     					// To create a Task in Org General tab
        me.create_Memo_fromOrgGeneralTab();                     					// To create a Memo in Org General tab
        me.create_Opportunity_fromOrgGeneralTab();              					// To create a Opportuinity in Org General tab
        me.check_contact_list_inorgGeneralTab();                					// To check list of contact in Org General tab
    });
};
module.exports.organizationTab_generalTab = organizationTab_generalTab;

var organizationTab_LocationSubTab_hoursOfOperation = function () {
    describe("ORGANIZATION TAB - LOCATION SUBTAB HOURS OF OPERATION",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_hoursOfOperation_display();        //Ensure that hours of operation is displayed
        me.check_timeFieldIsDisplay();              //Set hours of operation to open - Ensure that the time fields are displayed
        me.check_setTime24hour();                   //Set time to 24 hours
        me.editBtn_LocationAndContact();            //Edit the time and save
        me.add_maillingAddress_Street();            //Add Mailing Address Street 1
        me.add_maillingAddress_Street2();           //Add Mailing Address Street 2
        me.add_maillingAddress_City();              //Add Mailing Address City
        me.add_maillingAddress_State();             //Add Mailing Address State
        me.add_maillingAddress_PostalCode();        //Add Mailing Address Postal Code
        me.add_maillingAddress_Country();           //Add Mailing Address country
        me.orgSaveBtnOperation();                   // To Save and navigate on same page save button
        me.edit_BillingStreet();                    //check whether billing street address is same as mailing address
        me.edit_BillingStreet2();                   //check whether billing street2 address is same as mailing address
        me.edit_BillingCity();                      //check whether billing city address is same as mailing address
        me.edit_BillingState();                     //check whether billing state address is same as mailing address
        me.edit_BillingPostal();                    //check whether billing postal address is same as mailing address
        me.edit_BillingCountry();                   //check whether billing country address is same as mailing address
        me.orgSaveBtnOperation();                   //save the change
        me.editBtn_LocationAndContact();            //again edit the organization
        me.toggel_Btn_On();                         //check 'same as' toggle btn to make mailing and billing addresses similar
        me.add_billing_street1Address();            //check whether billing street address is same as mailing address
        me.add_billing_street2Address();            //check whether billing street2 address is same as mailing address
        me.add_billing_AddressCity();               //check whether billing city address is same as mailing address
        me.add_billing_AddressState();              //check whether billing state address is same as mailing address
        me.add_billing_AddressPostalCode();         //check whether billing postal address is same as mailing address
        me.add_billing_AddressCountry();            //check whether billing country address is same as mailing address
        me.orgSaveBtnOperation();                   //save the changes
    });
};
module.exports.organizationTab_LocationSubTab_hoursOfOperation = organizationTab_LocationSubTab_hoursOfOperation;

var organizationDetail_ContactsSubTab = function () {
    describe("ORGANIZATION DETAIL - CONTACTS SUBTAB",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        // navigateOperations.navigate_To_Contacts_Tab();
        // contactOperations.create_Contact('contact');
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.check_Subtabs_Display();                         // Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed
        me.check_Contacts_Subtab_Highlighted();             // Contacts & Providers Subtab is highlighted
        me.check_NewContact_Btn_Display();                  // New Contact' button displays for Users with Contacts Write permission
        // //this test case is covered in others            // New Provider' button displays for Users with Providers write permission
        me.check_Contacts_SearchBox_Display();              // Contacts search box displays
        me.check_SearchBox_Returns_Contacts_Providers();    // Search box returns both contacts and providers
        //this test case is already covered in others       // Selecting Contact or Provider from search results adds contact to Contact List
        //this test case is already covered in others       // Contact Type column in list presents drop-down with Contact Types
        me.check_Contact_Name_Redirects();                  // Contact name redirects to the Contact Detail screen
        me.check_Contact_Phone_Redirects();                 // Phone number field presents link that redirects to new browser (with Enable Phone Links)
        //this test case is already covered in others       // x of y' displays and pages correctly
        me.navigate_To_New_Org_Page();
        me.check_Contact_Edit_Link();                       // Edit' link presents with contact and provider Write permission
        me.check_Contact_Delete_Link();                     // Del' link presents with contact and provider Delete permission
        me.check_Contacts_Icon();                           // Ensure that the contacts and provider icons are displayed correctly
        userAuthentication.logout();
        userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);
        me.navigate_To_New_Org_Page();
        me.check_Contact_Edit_Link_fail();                  // Edit' link presents without contact and provider Write permission (Fail)
        check_Contact_Delete_Link_fail();                   // Del' link presents without contact and provider Delete permission (Fail)
        me.check_NewContact_Btn_Display_Fail();             // New Contact' button doesn't display for Users without Contacts Write permission (Fail)
        //this test case is already covered in others       // New Provider' button doesn't display for Users without Providers write permission (Fail)
        userAuthentication.logout();
        userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
module.exports.organizationDetail_ContactsSubTab = organizationDetail_ContactsSubTab;

var organizationDetail_ContactsSubTab_CustomList = function () {
    describe("ORGANIZATION DETAIL - CONTACTS SUBTAB CUSTOM LIST",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_ContactsSubTab();

        commonOperations.check_Filters_Behave_Properly(elements.organizationPageElements.contactsPageQuickFilterLink,
            elements.organizationPageElements.contactsPageRecordsListTable,
            elements.organizationPageElements.contactsPageFilterAvailColumns,
            elements.organizationPageElements.contactsPageFilterSelectedColumns,
            elements.organizationPageElements.contactsPageFilterAddBtn,
            elements.organizationPageElements.contactsPageFilterRemoveBtn,
            elements.organizationPageElements.contactsPageFilterApplyBtn);                      //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.create_New_Custom_List(elements.organizationPageElements.contactsPageSettingsIcon,
            elements.organizationPageElements.contactsPageCustomListNewListLink,
            elements.organizationPageElements.contactsPageCustomListOrderListLink,
            elements.organizationPageElements.contactsPageCustomListNewListInput,
            elements.organizationPageElements.contactsPageCustomListNewListSaveBtn,
            elements.organizationPageElements.contactsPageCustomListOrderListDiv,
            elements.organizationPageElements.orderListCloseIcon,
            elements.organizationPageElements.contactsPageCustomListAddBtn,
            elements.organizationPageElements.contactsPageCustomListRemoveBtn,
            elements.organizationPageElements.contactsPageCustomListAvailColumns,
            elements.organizationPageElements.contactsPageCustomListSelectedColumns,
            elements.organizationPageElements.contactsPageRecordsListTable);                              //create a new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.contactsPageSettingsIcon,
            elements.organizationPageElements.contactsPageCustomListEditLink,
            elements.organizationPageElements.contactsPageCustomListNewListDeleteBtn,
            elements.organizationPageElements.contactsPageCustomListNewListInput,
            elements.organizationPageElements.contactsPageCustomListNewListSaveBtn,
            elements.organizationPageElements.contactsPageCustomListOrderListLink,
            elements.organizationPageElements.contactsPageCustomListOrderListDiv,
            elements.organizationPageElements.orderListCloseIcon);                              //edit a custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.contactsPageSettingsIcon,
            elements.organizationPageElements.contactsPageCustomListCloneInput,
            elements.organizationPageElements.contactsPageCustomListCloneLink,
            elements.organizationPageElements.contactsPageCustomListCloneCreateBtn,
            elements.organizationPageElements.contactsPageCustomListOrderListLink,
            elements.organizationPageElements.contactsPageCustomListOrderListDiv,
            elements.organizationPageElements.orderListCloseIcon);                              //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.contactsPageSettingsIcon,
            elements.organizationPageElements.contactsPageCustomListEditLink,
            elements.organizationPageElements.contactsPageCustomListNewListDeleteBtn,
            elements.organizationPageElements.contactsPageCustomListOrderListLink,
            elements.organizationPageElements.contactsPageCustomListOrderListDiv,
            elements.organizationPageElements.orderListCloseIcon);                              //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.contactsPagePaginationDiv);   // Ensure that the paging is displayed correctly

    });
};
module.exports.organizationDetail_ContactsSubTab_CustomList = organizationDetail_ContactsSubTab_CustomList;

var organizationDetail_ChildOrgSubTab = function () {
    describe("ORGANIZATION DETAIL - CHILD ORG SUBTAB ",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_ChildOrgSubTab();
        me.check_Subtabs_Display();                                 // Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed
        me.check_Subtabs_Not_Display();                             // Activities, Orders and Attachments tabs are not Displayed
        me.check_Child_Subtab_Highlighted();                        // Child Organizations Subtab is highlighted
        me.check_Child_Org_Searchbox_Present();                     // Add Child Org Search box presents
        me.check_Add_Parent_Searchbox_Present();                    // Add Parent Org Search box presents
        me.check_Add_Org_To_Parent_Grid();                          // Selecting an Org from the Parent Org Search box adds Organization to the Parent Org grid
        me.navigate_To_New_Org_Page();
        me.navigate_To_ChildOrgSubTab();
        me.check_Add_Org_To_Child_Grid();                           // Selecting an Org from the Child Org Search box adds Organization to the Child Org grid
        me.check_Org_Name_Redirects();                              // Organization Name link redirects user to the Organization Detail screen
        me.check_Delete_Removes_Org();                              // Organization grids Present DEL option - Selecting Link removes the Org from grid
        //this test case will be covered in custom list test cases  // x of y' displays - paging works correctly
    });
};
module.exports.organizationDetail_ChildOrgSubTab = organizationDetail_ChildOrgSubTab;

var organizationDetail_ChildOrgSubTab_CustomList = function () {
    describe("ORGANIZATION DETAIL - CHILD ORG SUBTAB CUSTOM LIST",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        me.navigate_To_ChildOrgSubTab();

        commonOperations.check_Filters_Behave_Properly(elements.organizationPageElements.childOrgPageQuickFilterLink,
            elements.organizationPageElements.childOrgPageRecordsTable,
            elements.organizationPageElements.childOrgPageFilterAvailColumns,
            elements.organizationPageElements.childOrgPageFilterSelectedColumns,
            elements.organizationPageElements.childOrgPageFilterAddBtn,
            elements.organizationPageElements.childOrgPageFilterRemoveBtn,
            elements.organizationPageElements.childOrgPageFilterApplyBtn);                              //Ensure that  Quick Filter behaves properly (check with all the filters)

        commonOperations.create_New_Custom_List(elements.organizationPageElements.childOrgPageSettingsIcon,
            elements.organizationPageElements.childOrgPageCustomListNewLink,
            elements.organizationPageElements.childOrgPageCustomListOrderLink,
            elements.organizationPageElements.childOrgPageCustomListNewInput,
            elements.organizationPageElements.childOrgPageCustomListNewSaveBtn,
            elements.organizationPageElements.childOrgPageCustomListOrderListDiv,
            elements.organizationPageElements.childOrgPageCustomListOrderListCloseBtn,
            elements.organizationPageElements.childOrgPageCustomListAddBtn,
            elements.organizationPageElements.childOrgPageCustomListRemoveBtn,
            elements.organizationPageElements.childOrgPageCustomListAvailColumns,
            elements.organizationPageElements.childOrgPageCustomListSelectedColumns,
            elements.organizationPageElements.childOrgPageRecordsTable);                 // create a new custom list

        commonOperations.edit_Custom_List(elements.organizationPageElements.childOrgPageSettingsIcon,
            elements.organizationPageElements.childOrgPageCustomListEditLink,
            elements.organizationPageElements.childOrgPageCustomListDeleteBtn,
            elements.organizationPageElements.childOrgPageCustomListNewInput,
            elements.organizationPageElements.childOrgPageCustomListNewSaveBtn,
            elements.organizationPageElements.childOrgPageCustomListOrderLink,
            elements.organizationPageElements.childOrgPageCustomListOrderListDiv,
            elements.organizationPageElements.childOrgPageCustomListOrderListCloseBtn);                 //edit a custom list

        commonOperations.clone_Custom_List(elements.organizationPageElements.childOrgPageSettingsIcon,
            elements.organizationPageElements.childOrgPageCustomListCloneInput,
            elements.organizationPageElements.childOrgPageCustomListCloneLink,
            elements.organizationPageElements.childOrgPageCustomListCloneCreateBtn,
            elements.organizationPageElements.childOrgPageCustomListOrderLink,
            elements.organizationPageElements.childOrgPageCustomListOrderListDiv,
            elements.organizationPageElements.childOrgPageCustomListOrderListCloseBtn);                 //clone a custom list

        commonOperations.delete_Custom_List(elements.organizationPageElements.childOrgPageSettingsIcon,
            elements.organizationPageElements.childOrgPageCustomListEditLink,
            elements.organizationPageElements.childOrgPageCustomListDeleteBtn,
            elements.organizationPageElements.childOrgPageCustomListOrderLink,
            elements.organizationPageElements.childOrgPageCustomListOrderListDiv,
            elements.organizationPageElements.childOrgPageCustomListOrderListCloseBtn);                 //delete custom list

        commonOperations.check_paging_display(elements.organizationPageElements.childOrgPagePaginationDiv);           // Ensure that the paging is displayed correctly
    });
};
module.exports.organizationDetail_ChildOrgSubTab_CustomList = organizationDetail_ChildOrgSubTab_CustomList;

var organizationTab_AuditLog = function () {
    describe("ORGANIZATION TAB - AUDIT LOG",function () {
        me.pauseBrowserSomeTime();
        // me.create_New_Org();
        me.navigate_To_New_Org_Page();
        //Audit log
        commonOperations.check_auditLink(elements.auditLogElements.auditLogLink);// If the user has audit log read access user should be able to see Show audit/Access log link is displayed at the bottom of the page

        //Audit history
        commonOperations.check_auditHistoryTab(elements.organizationPageElements.orgDetailPageEditBtn,
            elements.organizationPageElements.orgEditPageNameInputField,
            elements.organizationPageElements.orgEditPageSaveButton,
            elements.auditLogElements.auditLogLink,
            elements.auditLogElements.startDateDiv,
            elements.auditLogElements.endDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.auditLogsTable,
            elements.auditLogElements.exportLink,
            variables.updatedOrgName);

        //access history
        commonOperations.check_accessHistoryTab(elements.auditLogElements.accessHistoryTab,
            elements.auditLogElements.accessHistoryStartDateDiv,
            elements.auditLogElements.accessHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.accessHistoryRecordsTable,
            elements.auditLogElements.accessHistoryExportLink);

        // //All history
        commonOperations.check_allHistoryTab(elements.auditLogElements.allHistoryTab,
            elements.auditLogElements.allHistoryStartDateDiv,
            elements.auditLogElements.allHistoryEndDateDiv,
            elements.auditLogElements.datePickerDiv,
            elements.auditLogElements.allHistoryRecordsTable,
            elements.auditLogElements.allHistoryExportLink);

        commonOperations.close_AuditLogDiv();
    });
};
module.exports.organizationTab_AuditLog = organizationTab_AuditLog;

var organizationDetail_FeeScheduleSubTab = function () {
    describe("ORGANIZATION DETAIL - LOCATION SUBTAB FEE SCHEDULE", function () {
        me.pauseBrowserSomeTime();
        // me.navigate_To_Panels_Tab();                                     //Clicking on Panels link in Admin to check the functionality
        // me.create_PanelFromAdminMenu();                                  //To Add Panele From grid Fees Schedule
        // me.create_FeeSchedule();                                         //To create a feesSchedule in panel section and also add panel
        // me.create_New_Org();                                             //Create a new Org
        me.navigate_To_New_Org_Page();
        me.navigate_To_FeeSchedule_Subtab();                             //Navigate to Fees And Schedule subtab
        me.check_Subtabs_Display();                						 // Subtabs General, Address, Contacts, Fee Schedule, Child Org, Settings, Activities, Orders and Attachments are displayed
        me.check_FeeScheduleTab_isHighlated();                           //Fee Schedule Tab is highlated
        me.editBtn_ForOrg_GeneralTab();                                  // For Edit Organization General Subtab
        me.add_Multiple_feeSchedule();                                   //Add multiple fee Schedules to an Org
        me.check_deleteIconPersent();                                    //Panels - Delete icon is present with delete permission
        userAuthentication.logout();
        userAuthentication.login(variables.username1, variables.password1, variables.usernameDisplay1);
        me.navigate_To_Org_Tab();
        me.check_Delete_Icon_Without_Permission_ForFeesSchedule();
        me.check_Delete_Icon_Without_Permission_ForPanels();
        userAuthentication.logout();
        userAuthentication.login(variables.username, variables.password, variables.usernameDisplay);
    });
};
module.exports.organizationDetail_FeeScheduleSubTab = organizationDetail_FeeScheduleSubTab;

var orgDependencies = function () {
    describe("RESOLVE ORGANIZATION SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'createdPanel',
            'createdFeeSchedule',
            'controlTreeNameCreated',
            'createdORG',
            'createdChildORG',
            'createdContact'
        ]);
        adminOperations.create_Panel();
        adminOperations.create_FeeSchedule();
        adminOperations.create_New_Control_Tree();
        me.create_New_Org();
        me.create_New_Child_Org();
        contactOperations.create_Contact('contact');
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS

        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS

        //SHEET EXECUTION - STARTS
        me.organizationDetail_settingsIcon(); //done //done
        me.organizationDetail_vCard(); //done //done
        me.organizationTab_generalTab(); //done //done
        me.organizationDetail_OrdersSubTab(); //done //done
        me.organizationDetail_MessagesSubTab(); //done //done
        me.organizationDetail_LocationSubTab_OfficeInfo(); //done //done
        me.organizationTab_LocationSubTab_hoursOfOperation(); //done //done
        me.organizationDetail_ChildOrgSubTab(); //done //done
        me.organizationTab_AuditLog(); //done //done

        // test plans which needs 2 users
        me.organizationTab_basic(); //done
        me.createOrganization_createNewOrg(); //done
        me.organizationDetail_ActivitiesSubTab(); //done
        me.organizationDetail_AttachmentsSubTab(); //done
        me.organizationDetail_ContactsSubTab(); //done
        me.organizationDetail_FeeScheduleSubTab(); //done
        me.createOrganization_AddSalesRep(); //done
        me.cloud_Search(); //done

        // Test suites having custom list tests only
        me.organizationTab_customList();
        me.organizationDetail_OrdersSubTab_customList();
        me.organizationDetail_AttachmentsSubTab_customList();
        me.organizationDetail_MessagesSubTab_CustomList();
        me.organizationDetail_ContactsSubTab_CustomList();
        me.organizationDetail_ChildOrgSubTab_CustomList();
        me.organizationDetail_ActivitiesSubTab_customList();
        //SHEET EXECUTION - ENDS

    });
};
exports.orgDependencies = orgDependencies;
//FUNCTIONS DECLARATION - ENDS