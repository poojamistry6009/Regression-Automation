/**
 * Created by INDUSA
 */

require('mocha');
var me = require('./provision.js');
var navigate = require('../utility/navigate.js');
var elements = require('../utility/elements.js');
var commands = require('../utility/commands.js');
var variables = require('../utility/variables.js');
var expect = require('chai').expect;
var userAdministration = require('./userAuthentication.js');

//TEST CASE DECLARATION - START

var check_appInfo = function () {
    describe("Application information", function () {
        it("should check application information", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.otherSheet.aboutThisApplicationDropDownList);
            browser.expect.element(elements.otherSheet.aboutThisAppPopUp).to.be.present;
            browser.expect.element(elements.otherSheet.aboutThisPopupContentDiv).to.be.present;
            browser.getText(elements.otherSheet.aboutThisPopupContentDiv,function (re) {
               console.log('=====> Application information : '+re.value);
            });
            commands.checkAndPerform('click', browser,elements.otherSheet.aboutThisCloseBtn);
        });
    });
};
exports.check_appInfo = check_appInfo;

var check_accountInfo = function () {
    describe("Account information", function () {
        it("should check account information", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.firstname+" "+variables.lastname+"')]");
            browser.pause(2000);
            // Ensure that account information detail page is displayed
            browser.expect.element(elements.otherSheet.userAdminDiv).text.to.contain(variables.firstname);
            // Email is a required field
            // FirstName is a required field
            // Last Name is a required field
            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.userAdminEditUserEmailInput);
            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.userAdminEditUserAliasNameInput);
            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.userAdminEditUserFullNameInput);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.expect.element(elements.adminPageElements.userAdminEmailErrorLabel).to.be.visible;
            browser.expect.element(elements.adminPageElements.userAdminAliasErrorLabel).to.be.visible;
            browser.expect.element(elements.adminPageElements.userAdminFullNameErrorLabel).to.be.visible;
            browser.pause(2000);
            // Edit First Name - Ensure that the First Name is saved successfully
            // Edit Last Name - Ensure that the LastName is saved successfully
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserEmailInput, variables.username);
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserAliasNameInput, variables.firstname);
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserFullNameInput, variables.firstname+" "+variables.lastname);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(4000);

            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.firstname+" "+variables.lastname+"')]");
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminEditUserEmailInput).to.have.value.that.equals(variables.username);
            browser.expect.element(elements.adminPageElements.userAdminEditUserAliasNameInput).to.have.value.that.equals(variables.firstname);
            browser.expect.element(elements.adminPageElements.userAdminEditUserFullNameInput).to.have.value.that.equals(variables.firstname+" "+variables.lastname);

            // Ensure Authentication field is displayed properly
            browser.expect.element(elements.adminPageElements.userAdminEditUserTwoFactorAuthDropdown).to.be.present;
        });
    });
};
exports.check_accountInfo = check_accountInfo;

var check_UsersTab = function () {
    describe("Users tab", function () {
        it("should check users tab - administration page", function (browser) {
            browser.pause(2000);
            // User Tab is highlighted
            browser.getAttribute(elements.adminPageElements.userAdminLinkTemp,'class',function (re) {
                expect(re.value).to.equal('active-link');
            });
            // Create new user link is present with write permission
            browser.expect.element(elements.adminPageElements.userAdminCreateUserLink).to.be.present;
            // List of users are present
            browser.expect.element(elements.adminPageElements.userAdminTable).to.be.present;
            // Name, Email and Roles are present in the list view
            browser.elements("xpath", elements.adminPageElements.userAdminTable+"/thead", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain('Name');
                        expect(text.value).to.contain('Email');
                        expect(text.value).to.contain('Roles');
                    });
                });
            });
            var tblHeaders = 0;
            browser.elements("xpath", elements.adminPageElements.userAdminTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function () {
                    tblHeaders = tblHeaders+1;
                });
            });

            // Delete icon is present with delete permission
            browser.elements("xpath", elements.adminPageElements.userAdminTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value != 'delete'){
                            if(count === tblHeaders){
                                expect('deleteColumn').to.equal('deleteColumnNotFound');
                            }
                        }
                    });
                });
            });
            // Ensure that the Paging is behaving properly
            browser.expect.element(elements.adminPageElements.userAdminPagingDiv).to.be.present;

            browser.pause(2000);
        });
    });
};
exports.check_UsersTab = check_UsersTab;

var create_User = function () {
    describe("Create a user", function () {
        it("should create a user", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userAdminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminCreateUserLink);
            browser.pause(2000);
            // User detail page is displayed
            browser.expect.element(elements.adminPageElements.userAdminPageDiv).to.be.present;
            // Add a Email
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserEmailInput, variables.newUserEmail);
            browser.pause(2000);
            // Add the First Name
            variables.createdNewUser = variables.newUserFName;
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserAliasNameInput, variables.newUserFName);
            browser.pause(2000);
            // Add the Last Name
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserFullNameInput, variables.newUserFName+" "+variables.newUserLName);
            browser.pause(2000);
            // Roles list is displayed
            browser.expect.element(elements.adminPageElements.userAdminEditUserRoles).to.be.present;
            // Users should be able to assign a role to a user
            commands.checkAndPerform('click', browser,"//label[contains(.,'Admin')]");
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminCreateSuccessHeader).to.be.present;
            browser.pause(2000);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminCreateSuccessHeaderCloseBtn);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdNewUser+"')]").to.be.present;
            browser.pause(2000);
        });
    });
};
exports.create_User = create_User;

var edit_User = function () {
    describe("Edit a user", function () {
        it("should edit a user", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userAdminLink);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdNewUser+"')]").to.be.present;
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdNewUser+"')]");
            browser.pause(2000);

            //edit user
            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.userAdminEditUserAliasNameInput);
            variables.createdNewUser = variables.newUserFNameUpdated;
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserAliasNameInput, variables.newUserFNameUpdated);
            commands.checkAndPerform('clearValue', browser,elements.adminPageElements.userAdminEditUserFullNameInput);
            commands.checkAndSetValue(browser,elements.adminPageElements.userAdminEditUserFullNameInput, variables.newUserFNameUpdated+" "+variables.newUserLNameUpdated);
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminEditUserSaveBtn);
            browser.pause(5000);
            browser.expect.element("//a[contains(@title,'"+variables.newUserFNameUpdated+" "+variables.newUserLNameUpdated+"')]").to.be.present;

            browser.pause(2000);
        });
    });
};
exports.edit_User = edit_User;

var delete_User = function () {
    describe("Delete a user", function () {
        it("should delete a user", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userAdminLink);
            browser.pause(2000);
            browser.expect.element("//a[contains(@title,'"+variables.createdNewUser+"')]").to.be.present;
            commands.checkAndPerform('click', browser, "//a[contains(@title,'"+variables.createdNewUser+"')]");
            browser.pause(2000);
            browser.expect.element(elements.adminPageElements.userAdminDeleteLink).to.be.present;
            commands.checkAndPerform('click', browser,elements.adminPageElements.userAdminDeleteLink);
            browser.pause(4000);
            // Delete a user - Ensure that the user is not displayed in the list view
            browser.expect.element("//a[contains(@title,'"+variables.createdNewUser+"')]").to.not.be.present;
            browser.pause(2000);
        });
    });
};
exports.delete_User = delete_User;

var check_nonAdmin_usersTab = function () {
    describe("Non Admin users tab", function () {
        it("should check non admin users tab", function (browser) {
            browser.pause(2000);
            // Create new user link is present without write permission (fail)
            browser.expect.element(elements.adminPageElements.userAdminCreateUserLink).to.not.be.present;
            // Delete icon is present without delete permission (fail)
            browser.elements("xpath", elements.adminPageElements.userAdminTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        expect(text.value).to.not.equal('delete');
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_nonAdmin_usersTab = check_nonAdmin_usersTab;

var check_UserRoles_Tab = function () {
    describe("User roles tab", function () {
        it("should check user roles tab", function (browser) {
            browser.pause(2000);
            // Roles Tab is highlighted
            browser.getAttribute(elements.adminPageElements.rolesLinkLi,'class',function (re) {
                expect(re.value).to.equal('active-link');
            });
            // Create new role link is present with write permission
            browser.expect.element(elements.adminPageElements.userRolesCreateNewRoleLink).to.be.present;
            // Delete is present with delete permission
            var tblHeaders = 0;
            browser.elements("xpath", elements.adminPageElements.userRolesTable+"/thead/tr/th", function (result) {
                var els = result.value;
                els.forEach(function () {
                    tblHeaders = tblHeaders+1;
                });
            });

            // Delete icon is present with delete permission
            browser.elements("xpath", elements.adminPageElements.userRolesTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                        if(text.value != 'delete'){
                            if(count === tblHeaders){
                                expect('deleteColumn').to.equal('deleteColumnNotFound');
                            }
                        }
                    });
                });
            });
            // List of roles are present
            browser.expect.element(elements.adminPageElements.userRolesTable).to.be.present;
            // Name and Permissions displayed in the columns
            browser.elements("xpath", elements.adminPageElements.userRolesTable+"/thead", function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        expect(text.value).to.contain('Role Name');
                        expect(text.value).to.contain('Role Permissions');
                    });
                });
            });
            // Ensure that paging behaves expected
            browser.expect.element(elements.adminPageElements.userRolesPagingDiv).to.be.present;

            browser.pause(2000);
        });
    });
};
exports.check_UserRoles_Tab = check_UserRoles_Tab;

var create_Role = function () {
    describe("Create a role", function () {
        it("should create a role", function (browser) {
            browser.pause(2000);
            // New role link is present with Role write permission
            browser.expect.element(elements.adminPageElements.userRolesCreateNewRoleLink).to.be.present;
            commands.checkAndPerform('click', browser, elements.adminPageElements.userRolesCreateNewRoleLink);

            //Ensure that save and cancel changes link is present
            browser.expect.element(elements.adminPageElements.userRolesRoleSaveBtn).to.be.present;
            browser.expect.element(elements.adminPageElements.userRoleCancelBtn).to.be.present;
            commands.checkAndPerform('click', browser,elements.adminPageElements.userRoleCancelBtn);

            browser.pause(2000);
        });
    });
};
exports.create_Role = create_Role;

var check_nonAdmin_RolesTab = function () {
    describe("User Role non admin checks", function () {
        it("should check user roles tab for non admin user", function (browser) {
            browser.pause(2000);
            // Create new role link is present without write permission (fail)
            browser.expect.element(elements.adminPageElements.userRolesCreateNewRoleLink).to.not.be.present;
            // Delete is present without delete permission (fail)
            browser.elements("xpath", elements.adminPageElements.userRolesTable+"/thead/tr/th", function (result) {
                var els = result.value;
                var count = 0;
                els.forEach(function (el) {
                    browser.elementIdAttribute(el.ELEMENT,'class', function (text) {
                        count = count + 1;
                       expect(text.value).to.not.equal('delete');
                    });
                });
            });
            browser.pause(2000);
        });
    });
};
exports.check_nonAdmin_RolesTab = check_nonAdmin_RolesTab;

//TEST CASE DECLARATION - END









//FUNCTIONS CASE DECLARATION - START

var accountInfo_And_AppInfo = function () {
    describe("ACCOUNT INFORMATION", function () {

        navigate.navigate_To_UserAdmin_Tab();
        me.check_accountInfo(); // Ensure that account information detail   page is displayed
                                // Ensure save and change changes is present
                                // Email is a required field
                                // FirstName is a required field
                                // Last Name is a required field
                                // Edit First Name - Ensure that the First Name is saved successfully
                                // Edit Last Name - Ensure that the LastName is saved successfully
                                // Ensure Authentication field is displayed properly
                                // Ensure that user can login to insight with valid user name and password

        me.check_appInfo();// Ensure that About This Application pop-up is  present
                        // Ensure that version number is  present
                        // Ensure that build number is  present
                        // Ensure that Build On is present
    });
};
exports.accountInfo_And_AppInfo = accountInfo_And_AppInfo;

var users_Tab = function () {
    describe("USERS TAB", function () {
        navigate.navigate_To_UserAdmin_Tab();
        me.check_UsersTab();// User Tab is highlighted
                            // Create new user link is present with write permission
                            // List of users are present
                            // Name, Email and Roles are present in the list view
                            // Delete icon is present with delete permission
                            // Ensure that the Paging is behaving properly
        me.create_User();
        me.edit_User();
        me.delete_User();
    });
};
exports.users_Tab = users_Tab;

var usersTab_nonAdmin_Check = function () {
    describe("USER ADMINISTRATION PAGE - NON ADMIN CHECKS", function () {

        userAdministration.logout();
        userAdministration.login(variables.username1,variables.password1,variables.usernameDisplay1);
        navigate.navigate_To_UserAdmin_Tab();
        me.check_nonAdmin_usersTab();// Create new user link is present without write permission (fail)
                                    // Delete icon is present without delete permission (fail)

        userAdministration.logout();
        userAdministration.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.usersTab_nonAdmin_Check = usersTab_nonAdmin_Check;

var userRoles_Tab = function () {
    describe("USER ROLES TAB", function () {
        navigate.navigate_To_Roles_Tab();

        me.check_UserRoles_Tab();   // Roles Tab is highlighted
                                    // Create new role link is present with write permission
                                    // Delete is present with delete permission
                                    // List of roles are present
                                    // Name and Permissions displayed in the columns
                                    // Ensure that paging behaves expected
                                    // Select a role - User should be redirected to the roles detail page with read permission

        me.create_Role(); //Ensure that save and cancel changes link is present
        // New role link is present with Role write permission
    });
};
exports.userRoles_Tab = userRoles_Tab;

var userRole_NonAdmin_Checks = function () {
    describe("USER ROLES TAB NON ADMIN CHECKS", function () {
        userAdministration.logout();
        userAdministration.login(variables.username1,variables.password1,variables.usernameDisplay1);

        navigate.navigate_To_UserAdmin_Tab();
        me.check_nonAdmin_usersTab();// Create new user link is present without write permission (fail)
        // Delete icon is present without delete permission (fail)

        navigate.navigate_To_Roles_Tab();
        me.check_nonAdmin_RolesTab();// Create new role link is present without write permission (fail)
        // Delete is present without delete permission (fail)
        // New role link is present without Role write permission (fail)

        userAdministration.logout();
        userAdministration.login(variables.username,variables.password,variables.usernameDisplay);
    });
};
exports.userRole_NonAdmin_Checks = userRole_NonAdmin_Checks;

var provision_Sheet = function () {
    describe("PROVISION SHEET EXECUTION", function () {

        //SHEET EXECUTION - STARTS
        me.accountInfo_And_AppInfo();
        me.users_Tab();
        me.userRoles_Tab();
        me.userRole_NonAdmin_Checks();
        //SHEET EXECUTION - ENDS

    });
};
exports.provision_Sheet = provision_Sheet;
//FUNCTIONS CASE DECLARATION - END