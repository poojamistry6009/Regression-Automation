/**
 * Created by Indusa on 5/31/2017.
 */

require('mocha');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');
var variables = require('../utility/variables.js');
var organizationOperations = require('./organization.js');
var userAuthentication = require('./userAuthentication.js');
var opportunityOperations = require('./opportunity.js');
var campaignsOperations = require('./campaigns.js');
var contactOperations = require('./contacts.js');
var footerLinks = require('./footer.js');
var navigate = require('../utility/navigate.js');
var commonOperations = require('./commons.js');

var me = require('./other.js');


//Test cases declaration - start

var change_Password = function () {
    describe("Give a weak password and save (fail) ", function () {
        it(" Should check - Give a weak password and save (fail)", function (browser) {
            browser.pause(10000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.userNameInput);
            browser.setValue(elements.otherSheet.userNameInput, variables.username);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.passwordInput);
            browser.setValue(elements.otherSheet.passwordInput, variables.weakUserPassword);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.submitBtnOnLoginPage);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.okayBtnOnLoginFailedPage);
            browser.pause(2000);
        });
    });
}
exports.change_Password = change_Password;

var check_User_Can_Change_Password = function () {
    describe(" Ensure that user can change password successfully from my account page", function () {
        it("Should check - Ensure that user can change password successfully from my account page ", function (browser) {
            browser.pause(2000);
            var newPassword = variables.confirmPassword;
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.oldPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.oldPasswordInput, variables.password);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.newPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.newPasswordInput, newPassword);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.confirmPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.confirmPasswordInput, newPassword);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.submitBtnChangePwdPage);
            browser.pause(2000);
        });
    });
}
exports.check_User_Can_Change_Password = check_User_Can_Change_Password;

var revert_Changed_Password = function () {
    describe("Revert back changed password", function () {
        it("should revert back changed password to old one", function (browser) {
            browser.pause(2000);
            var newPassword = variables.password;
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.oldPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.oldPasswordInput, variables.confirmPassword);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.newPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.newPasswordInput, newPassword);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.confirmPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.confirmPasswordInput, newPassword);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.submitBtnChangePwdPage);
            browser.pause(2000);
        });
    });
};
exports.revert_Changed_Password = revert_Changed_Password;

var check_Recent_Items_DropDownList = function () {
    describe("Ensure that the recent items dropdown is displayed correctly ", function () {
        it("Should check - Ensure that the recent items dropdown is displayed correctly", function (browser) {
            browser.pause(2000);
            var createdOrg = variables.createdORG;
            browser.pause(2000);
            browser.moveToElement(elements.otherSheet.recentItemsDropDownList, 3000, 3000, function () {
                browser.pause(2000);
                browser.click(elements.otherSheet.recentItemsDropDownList);
                browser.pause(5000);
                browser.expect.element(elements.otherSheet.recentItemsDropUlList).text.to.contain(createdOrg);
            });
            browser.pause(2000);
        });
    });
}
exports.check_Recent_Items_DropDownList = check_Recent_Items_DropDownList;

var navigate_To_My_Account = function () {
    describe("Navigate to User name on header panel drop down list ", function () {
        it(" should check - Navigate to User name on header panel drop down list", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerUsernameDropdown);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.myAccountDropDownList);
            browser.pause(2000);
        });
    });
}
exports.navigate_To_My_Account = navigate_To_My_Account;

var check_Out_Of_Office_CheckBox_Availability = function () {
    describe("Ensure that Out of office checkbox is present  ", function () {
        it("Should check - Ensure that Out of office checkbox is present  ", function (browser) {
            browser.pause(2000);
            browser.expect.element(elements.otherSheet.OutOfOfficeDiv).to.be.present
            browser.pause(2000);
            browser.expect.element(elements.otherSheet.accountDate).to.be.present
            browser.pause(2000);
        });
    });
}
exports.check_Out_Of_Office_CheckBox_Availability = check_Out_Of_Office_CheckBox_Availability;

var account_Email_Read_Only = function () {
    describe("Ensure that Account email is read only ", function () {
        it("Should check - Ensure that Account email is read only ", function (browser) {
            browser.pause(2000);
            //Ensure that Account email is read only
            browser.expect.element(elements.otherSheet.accountEmailInput).to.have.attribute('readonly').to.equal("true");
            browser.pause(2000);
            browser.expect.element(elements.otherSheet.firstNameInput).to.have.value.that.equals(variables.firstname);
            browser.pause(2000);
            browser.expect.element(elements.otherSheet.lastNameInput).to.have.value.that.equals(variables.firstname+" "+variables.lastname);
            browser.pause(2000);
            //First Name field is present and is auto populated
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.firstNameInput);
            commands.checkAndSetValue(browser, elements.otherSheet.firstNameInput, variables.accountFirstName);

            //Last Name field is present and is auto populated
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.lastNameInput);
            commands.checkAndSetValue(browser, elements.otherSheet.lastNameInput, variables.accountLastName);

            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.smsNumberInput);
            commands.checkAndSetValue(browser, elements.otherSheet.smsNumberInput, variables.accountsmsNumber);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.saveButtonOntAccountPage);
            browser.pause(2000);
            var firstName = variables.accountFirstName;
            var lastName = variables.accountLastName;
            var fullName = firstName.concat(lastName);
            browser.pause(2000);
            // For entry previous name
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.firstNameInput);
            commands.checkAndSetValue(browser, elements.otherSheet.firstNameInput, variables.firstname);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.lastNameInput);
            commands.checkAndSetValue(browser, elements.otherSheet.lastNameInput, variables.lastname);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.saveButtonOntAccountPage);
            browser.pause(2000);
        });
    });
}
exports.account_Email_Read_Only = account_Email_Read_Only;

var check_Online_help_Documentation_Open = function () {
    describe("Online help > Ensure user is redirected to online help documentation  ", function () {
        it("Online help > Ensure user is redirected to online help documentation ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.otherSheet.headerHelpMenuDropdown, 100, 100);
            browser.pause(2000);
            browser.click(elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            browser.click(elements.otherSheet.onlineHelpDropDownList);
            browser.pause(5000);
            browser.windowHandles(function (result) {
                browser.switchWindow(result.value[1]);
                browser.pause(8000);
                browser.getAttribute(elements.otherSheet.spaceDirectoryList, "id", function (result2) { //find an element present in another window and get its id
                });
                browser.expect.element(elements.otherSheet.spaceDirectoryList).to.be.present; //assert an element from another window is present or not
                browser.pause(5000);
                browser.switchWindow(result.value[0]);  //after waiting for 5 seconds switch back to previous window otherwise logout will give error
            })
        });
    });
}
exports.check_Online_help_Documentation_Open = check_Online_help_Documentation_Open;

var check_Redirect_To_Video_Tutorial = function () {
    describe("Video Tutorials > Ensure user is redirected to video tutorials documentation ", function () {
        it("Should check - Video Tutorials > Ensure user is redirected to video tutorials documentation ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.otherSheet.headerHelpMenuDropdown, 100, 100);
            browser.pause(2000);
            browser.click(elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            browser.click(elements.otherSheet.videoDropDownList);
            browser.pause(5000);
            browser.windowHandles(function (result) {
                browser.switchWindow(result.value[1]);
                browser.pause(8000);
                browser.getAttribute(elements.otherSheet.spaceDirectoryList, "id", function (result2) { //find an element present in another window and get its id
                });
                browser.expect.element(elements.otherSheet.spaceDirectoryList).to.be.present; //assert an element from another window is present or not
                browser.pause(5000);
                browser.switchWindow(result.value[0]);  //after waiting for 5 seconds switch back to previous window otherwise logout will give error
            });
            browser.pause(2000);
        });
    });
};
exports.check_Redirect_To_Video_Tutorial = check_Redirect_To_Video_Tutorial;

var check_About_This_Application = function () {
    describe("About This Application > Ensure that the About this application pop-up is displayed with the information  ", function () {
        it("Should check - About This Application > Ensure that the About this application pop-up is displayed with the information  ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.otherSheet.headerHelpMenuDropdown, 100, 100);
            browser.pause(2000);
            browser.click(elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            browser.click(elements.otherSheet.videoDropDownList);
            browser.pause(5000);

        });
    });
}
exports.check_About_This_Application = check_About_This_Application;

var check_Online_help_Hc1_Academy = function () {
    describe("Hc1Academy > Ensure user is redirected to hc1acdaemy   ", function () {
        it("Online help > Hc1Academy > Ensure user is redirected to hc1acdaemy  ", function (browser) {
            browser.pause(2000);
            browser.click(elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            browser.click(elements.otherSheet.hc1AcademyDropDownList);
            browser.pause(5000);
            browser.windowHandles(function (result) {
                browser.switchWindow(result.value[1]);
                browser.pause(8000);
                browser.getAttribute(elements.otherSheet.spaceDirectoryList, "id", function (result2) { //find an element present in another window and get its id

                });
                browser.expect.element(elements.otherSheet.spaceDirectoryList).to.be.present; //assert an element from another window is present or not
                browser.pause(5000);
                browser.switchWindow(result.value[0]);  //after waiting for 5 seconds switch back to previous window otherwise logout will give error
            });
            browser.pause(2000);
        });
    });
}
exports.check_Online_help_Hc1_Academy = check_Online_help_Hc1_Academy;

var check_Online_help_About_This_Application = function () {
    describe("About This Application > Ensure that the About this application pop-up is displayed with the information   ", function () {
        it("should check -About This Application > Ensure that the About this application pop-up is displayed with the information ", function (browser) {
            browser.pause(2000);
            browser.moveToElement(elements.otherSheet.headerHelpMenuDropdown, 100, 100);
            browser.pause(2000);
            browser.click(elements.otherSheet.headerHelpMenuDropdown);
            browser.pause(2000);
            browser.click(elements.otherSheet.aboutThisApplicationDropDownList);
            browser.pause(5000);
            commands.checkAndPerform('click', browser,elements.otherSheet.aboutThisCloseBtn);
            browser.pause(2000);
            browser.refresh();
        });
    });
}
exports.check_Online_help_About_This_Application = check_Online_help_About_This_Application;

var check_Weak_Pwd = function () {
    describe("Weak password", function () {
        it("should check weak password functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.changePasswordLink);
            browser.pause(2000);
            var weakPassword = variables.weakPassword;
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.oldPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.oldPasswordInput, variables.password);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.newPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.newPasswordInput, weakPassword);
            browser.pause(2000);
            commands.checkAndPerform('clearValue', browser, elements.otherSheet.confirmPasswordInput);
            browser.pause(2000);
            browser.setValue(elements.otherSheet.confirmPasswordInput, weakPassword);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.otherSheet.submitBtnChangePwdPage);
            browser.pause(2000);
            browser.expect.element(elements.otherSheet.newPasswordLabelMsg).text.to.contains("Too weak");
            browser.pause(2000);
        });
    });
};
exports.check_Weak_Pwd = check_Weak_Pwd;

//Test cases declaration - end













//Functions declaration - start

var help_Menu = function () {
    describe("Help menu  ", function () {

        me.check_Redirect_To_Video_Tutorial();
        me.check_Online_help_Documentation_Open();
        // me.check_Online_help_Hc1_Academy();  //Hc1 academy in help menu doesn't open any page as of now ( 20-07-2017 )
        me.check_Online_help_About_This_Application();

    });
}
exports.help_Menu = help_Menu;

var user_Preferences = function () {
    describe("USER PREFERENCES", function () {
        me.navigate_To_My_Account();        //To navigate on my account page
        me.account_Email_Read_Only();       //Ensure that Account email is read only
                                            //First Name field is present and is auto populated
                                            //Last name is present and it auto populated
                                            //Edit First name > save > ensure that the name is updated successfully
                                            //Edit last name  > save > ensure that the name is updated successfully
                                            //SMS number field is present
                                            //Add SMS number > Ensure that user can save successfully
    });
}
exports.user_Preferences = user_Preferences;

var others_ChangePassword = function () {
    describe("CHANGE PASSWORD", function () {
        me.navigate_To_My_Account();                                //Navigate to my account page
        me.check_Weak_Pwd();
        me.check_User_Can_Change_Password();                       //Ensure that user can change password successfully from my account page
        me.navigate_To_My_Account();
        me.revert_Changed_Password();
        //Give a weak password and save (fail)
    });
}
exports.others_ChangePassword = others_ChangePassword;

var recent_Items = function () {
    describe("RECENT ITEMS CHECK", function () {
        organizationOperations.navigate_To_New_Org_Page();
        me.check_Recent_Items_DropDownList();
    });
}
exports.recent_Items = recent_Items;

var my_Account = function () {
    describe("MY ACCOUNT ", function () {
        me.navigate_To_My_Account();
        me.check_Out_Of_Office_CheckBox_Availability();
    });
}
exports.my_Account = my_Account;

var others_Sheet = function () {
    describe("RESOLVE OTHERS SHEET DEPENDENCIES", function () {

        //DEPENDENCY RESOLVING FUNCTION - STARTS
        commands.fillDependencyRequiredList([
            'createdORG',
            'createdOpportunity',
            'createdCampaignContact',
            'createdContact',
            'createdProvider',
        ]);
        organizationOperations.create_New_Org();
        opportunityOperations.create_Opportunity();
        campaignsOperations.create_Campaign('contact', false);
        contactOperations.create_Contact('contact');
        contactOperations.create_Contact('provider');
        commands.checkDependencyListFulfilled();
        //DEPENDENCY RESOLVING FUNCTION - ENDS


        //DEFAULT PROFILE - STARTS
        navigate.navigate_To_CustomizationProfiles_Tab();
        commonOperations.check_Default_Profile();
        //DEFAULT PROFILE - ENDS


        //SHEET EXECUTION - STARTS
        me.others_ChangePassword();
        me.recent_Items();

        me.help_Menu();
        me.my_Account();
        me.user_Preferences();
        opportunityOperations.cloud_Search_For_Opportunity();
        campaignsOperations.cloud_Search_For_Campaign();
        contactOperations.cloud_Search_For_contact();
        contactOperations.cloud_Search_For_provider();
        footerLinks.check_Footer_Link_Availability();
        //SHEET EXECUTION - ENDS

    });
};
exports.others_Sheet = others_Sheet;

//Functions declaration - end