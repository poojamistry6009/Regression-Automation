/**
 * Created by INDUSA
 */
require('mocha');
var browserOperations = require('./pages/browserControl.js');
var userAuthentication = require('./pages/userAuthentication.js');
var organizationOperations = require('./pages/organization.js');
var patientOperations = require('./pages/patient.js');
var navigateOperations = require('./utility/navigate.js');
var variables = require('./utility/variables.js');
var contactOperations = require('./pages/contacts.js');
var activityOperations = require('./pages/activities.js');
var opportunityOperations = require('./pages/opportunity.js');
var commonOperations = require('./pages/commons.js');
var accessControlOperations = require('./pages/accessControl.js');
var otherOperations = require('./pages/other.js');
var campaignsOperations = require('./pages/campaigns.js');
var adminOperations = require('./pages/admin.js');
var collaborationPageOperations = require('./pages/collaborationcenter.js');
var elements = require('./utility/elements.js');
var patientsOperation = require('./pages/patient.js');
var footerLinks = require('./pages/footer.js');
var otherOperation = require('./pages/other.js');
var customizationsOperation = require('./pages/customizations.js');
var provisionOperations = require('./pages/provision.js');
var bulkImporterOperations = require('./pages/bulkimporter.js');
var webServicesOperations = require('./pages/webservices.js');

var test = require('./pages/test.js');

describe("HC1 UI TEST",function () {
    browserOperations.changeLocator('xpath');

    userAuthentication.login(variables.username,variables.password,variables.usernameDisplay);
    // userAuthentication.login(variables.username1,variables.password1,variables.usernameDisplay1);

    // organizationOperations.orgDependencies();    //tested
    // activityOperations.activityDependencies();   //tested
    // opportunityOperations.opportunity_Sheet();   //tested
    // contactOperations.contacts_Sheet();  //tested
    // campaignsOperations.campaigns_Sheet();       //pending
    // accessControlOperations.accessControl_Sheet();   //tested
    // patientOperations.patients_Sheet();  //tested
    // otherOperations.others_Sheet();  //tested
    // collaborationPageOperations.collaborationCenter_Sheet();
    // adminOperations.tenantAdmin_Sheet();
    // webServicesOperations.webServices_Sheet();   //tested
    // bulkImporterOperations.bulkImport_Sheet();
    // customizationsOperation.customization_Sheet();
    provisionOperations.provision_Sheet();



    // userAuthentication.logout();
    browserOperations.endBrowserSession();
});