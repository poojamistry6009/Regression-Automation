
/**
 * Created by INDUSA
 */
require('mocha');
var variables = require('../utility/variables.js');
var commands = require('../utility/commands.js');
var elements = require('../utility/elements.js');

//Main page navigation - STARTS
var navigate_To_Org_Tab = function () {
    describe("Clicking the Organization Tab navigates to the Organization List Screen",function () {
        it("should check - Clicking the Organization Tab navigates to the Organization List Screen",function (browser) {
            commands.checkAndPerform('click',browser,elements.organizationPageElements.organizationTabLink);
            browser.expect.element(elements.organizationPageElements.pageTitle).text.to.equal('Organizations');
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Org_Tab = navigate_To_Org_Tab;

var navigate_To_Dashboard_Tab = function () {
    describe("Clicking the Dashboard Tab navigates to the Dashboard List Screen", function () {
        it("should check - Clicking the Dashboard Tab navigates to the Dashboard List Screen", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.dashboardPageElements.dashboardTabLink);
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Dashboard_Tab = navigate_To_Dashboard_Tab;

var navigate_To_Contacts_Tab = function () {
    describe("Clicking the Contacts Tab navigates to the Contacts List Screen", function () {
        it("should check - Clicking the Contacts Tab navigates to the Contacts List Screen", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.contactsPageElements.contactsTabLink);
            browser.expect.element(elements.contactsPageElements.contactPageTitle).text.to.equal('Contacts');
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Contacts_Tab = navigate_To_Contacts_Tab;

var navigate_To_Campaign_Tab = function () {
    describe("Clicking the Campaign Tab navigates to the Campaign List Screen", function () {
        it("should check - Clicking the Campaign Tab navigates to the Campaign List Screen", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.campaignPageElements.campaignTabLink);
            browser.expect.element(elements.campaignPageElements.pageTitle).text.to.equal('Campaigns');
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Campaign_Tab = navigate_To_Campaign_Tab;

var navigate_To_Activities_Tab = function () {
    describe("Clicking the Activities Tab navigates to the Activities List Screen", function () {
        it("should check - Clicking the Activities Tab navigates to the Activities List Screen", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.activityPageElements.activitiesTabLink);
            browser.expect.element(elements.activityPageElements.activityPageHeader).text.to.equal('Activities');
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Activities_Tab = navigate_To_Activities_Tab;

var navigate_To_Opportunities_Tab = function () {
    describe("Clicking the Opportunities Tab navigates to the Opportunities List Screen", function () {
        it("should check - Clicking the Opportunities Tab navigates to the Opportunities List Screen", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.opportunityPageElements.opportunitiesTabLink);
            browser.expect.element(elements.opportunityPageElements.opportunityPageHeader).text.to.equal('Opportunities');
            browser.pause(2000);
        });
    });
}
module.exports.navigate_To_Opportunities_Tab = navigate_To_Opportunities_Tab;

var navigate_To_Reports_Tab = function () {
    describe("Clicking the Reports Tab navigates to the Reports List Screen", function () {
        it("should check - Clicking the Reports Tab navigates to the Reports List Screen", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.reportsPageElements.reportsTabLink);
            browser.pause(3000);
        });
    });
}
module.exports.navigate_To_Reports_Tab = navigate_To_Reports_Tab;

var navigate_To_OrdersAndPatients_Tab = function () {
    describe("Clicking the Orders and Patients Tab navigates to the Orders and Patients List Screen", function () {
        it("should check - Clicking the Orders and Patients Tab navigates to the Orders and Patients List Screen", function (browser) {
            browser.pause(3000);
            commands.checkAndPerform('click', browser, elements.ordersandPatientsPageElements.ordersandPatientsTabLink);
            browser.pause(3000);
        });
    });
}
module.exports.navigate_To_OrdersAndPatients_Tab = navigate_To_OrdersAndPatients_Tab;

//Main page navigation - ENDS



//Admin page navigation - STARTS
var navigate_To_Admin_Tab = function () {
    describe("Clicking Admin link to check the functionality", function () {
        it("should Click - Clicking the Admin Link navigates to the Admin Screen consisting of Admin Menu", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(1000);browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Admin_Tab = navigate_To_Admin_Tab;

var navigate_To_UserAdmin_Tab = function () {
    describe("Clicking on User Admin link in Admin to check the functionality", function () {
        it("should Click - Clicking on User Admin link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userAdminLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_UserAdmin_Tab = navigate_To_UserAdmin_Tab;

var navigate_To_UserProfiles_Tab = function () {
    describe("Clicking on User Profiles link in Admin to check the functionality", function () {
        it("should Click - Clicking on User Profiles link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userProfilesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_UserProfiles_Tab = navigate_To_UserProfiles_Tab;

var navigate_To_Roles_Tab = function () {
    describe("Clicking on Roles link in Admin to check the functionality", function () {
        it("should Click - Clicking on Roles link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.rolesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Roles_Tab = navigate_To_Roles_Tab;

var navigate_To_AuthProvider_Tab = function () {
    describe("Clicking on Authentication Provider link in Admin to check the functionality", function () {
        it("should Click - Clicking on Authentication Provider link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.authProviderLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_AuthProvider_Tab = navigate_To_AuthProvider_Tab;

var navigate_To_LoginConfig_Tab = function () {
    describe("Clicking on Login Configuration link in Admin to check the functionality", function () {
        it("should Click - Clicking on Login Configuration link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.loginConfigLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_LoginConfig_Tab = navigate_To_LoginConfig_Tab;

var navigate_To_Distribution_Tab = function () {
    describe("Clicking on Distribution link in Admin to check the functionality", function () {
        it("should Click - Clicking on Distribution link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.distributionLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Distribution_Tab = navigate_To_Distribution_Tab;

var navigate_To_Calendar_Tab = function () {
    describe("Clicking on Calendar Display link in Admin to check the functionality", function () {
        it("should Click - Clicking on Calendar Display link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.calendarDispLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Calendar_Tab = navigate_To_Calendar_Tab;

var navigate_To_Notification_Tab = function () {
    describe("Clicking on Notification link in Admin to check the functionality", function () {
        it("should Click - Clicking on Notification link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.notificationLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Notification_Tab = navigate_To_Notification_Tab;

var navigate_To_HrmAccess_Tab = function () {
    describe("Clicking on HRM Access link in Admin to check the functionality", function () {
        it("should Click - Clicking on HRM Access link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.hrmAccessLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_HrmAccess_Tab = navigate_To_HrmAccess_Tab;

var navigate_To_ServerCred_Tab = function () {
    describe("Clicking on Server Credentials link in Admin to check the functionality", function () {
        it("should Click - Clicking on Server Credentials link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.serverCredentialsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_ServerCred_Tab = navigate_To_ServerCred_Tab;

var navigate_To_AdminInfo_Tab = function () {
    describe("Clicking on Information link in Admin to check the functionality", function () {
        it("should Click - Clicking on Information link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.informationLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_AdminInfo_Tab = navigate_To_AdminInfo_Tab;

var navigate_To_AdminRelation_Tab = function () {
    describe("Clicking on Relationship link in Admin to check the functionality", function () {
        it("should Click - Clicking on Relationship link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_AdminRelation_Tab = navigate_To_AdminRelation_Tab;

var navigate_To_AccessControlTree_Tab = function () {
    describe("Clicking on Panels link in Admin to check the functionality", function () {
        it("should Click - Clicking on Panels link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.hrmAccessControlLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_AccessControlTree_Tab = navigate_To_AccessControlTree_Tab;

var navigate_To_Panels_Tab = function () {
    describe("Clicking on Panels link in Admin to check the functionality", function () {
        it("should Click - Clicking on Panels link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.panelsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Panels_Tab = navigate_To_Panels_Tab;

var navigate_To_Customizations_Tab = function () {
    describe("Clicking on Customizations link in Admin to check the functionality", function () {
        it("should Click - Clicking on Customizations link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Customizations_Tab = navigate_To_Customizations_Tab;

var navigate_To_ActivityCatg_Tab = function () {
    describe("Clicking on Activity category link in Admin to check the functionality", function () {
        it("should Click - Clicking on Activity category link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityCategoryLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_ActivityCatg_Tab = navigate_To_ActivityCatg_Tab;

var navigate_To_ActivityStatus_Tab = function () {
    describe("Clicking on Activity Status link in Admin to check the functionality", function () {
        it("should Click - Clicking on Activity Status link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityStatusLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_ActivityStatus_Tab = navigate_To_ActivityStatus_Tab;

var navigate_To_ActivityPriority_Tab = function () {
    describe("Clicking on Activity Priority link in Admin to check the functionality", function () {
        it("should Click - Clicking on Activity Priority link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.activityPriorityLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_ActivityPriority_Tab = navigate_To_ActivityPriority_Tab;

var navigate_To_CampaignType_Tab = function () {
    describe("Clicking on Campaign Type link in Admin to check the functionality", function () {
        it("should Click - Clicking on Campaign Type link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.campaignTypeLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_ToCampaignType_Tab = navigate_To_CampaignType_Tab;

var navigate_To_MergeRecords_Tab = function () {
    describe("Navigate to Merge records tab", function () {
        it("should navigate to merge records page in Administration", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.informationLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.mergeRecordsTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_MergeRecords_Tab = navigate_To_MergeRecords_Tab;

var navigate_To_MergeRecords_Contact_Tab = function () {
    describe("Navigate to Merge records tab", function () {
        it("should navigate to merge records page in Administration", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.informationLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.mergeRecordsTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.mergeRecordContactSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_MergeRecords_Contact_Tab = navigate_To_MergeRecords_Contact_Tab;

var navigate_To_MergeRecords_Provider_Tab = function () {
    describe("Navigate to Merge records tab", function () {
        it("should navigate to merge records page in Administration", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.informationLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.mergeRecordsTab);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.mergeRecordProviderSubTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_MergeRecords_Provider_Tab = navigate_To_MergeRecords_Provider_Tab;

var navigate_To_DiagnosisTab = function () {
    describe("Navigate to diagnosis tab", function () {
        it("should navigate to diagnosis tab", function (browser) {
            browser.pause(2000);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.panelsLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.diagnosisTab);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_DiagnosisTab = navigate_To_DiagnosisTab;

var navigate_To_CompetitiveLab_Tab = function () {
    describe("Clicking on Competitive Lab link in Admin to check the functionality", function () {
        it("should Click - Clicking on Competitive Lab link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.competitiveLabLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_CompetitiveLab_Tab = navigate_To_CompetitiveLab_Tab;

var navigate_To_ContactType_Tab = function () {
    describe("Clicking on Contact Type link in Admin to check the functionality", function () {
        it("should Click - Clicking on Contact Type link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click',browser,elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.contactTypeLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_ContactType_Tab = navigate_To_ContactType_Tab;

var navigate_To_CorrectiveAction_Tab = function () {
    describe("Clicking on Corrective Action link in Admin to check the functionality", function () {
        it("should Click - Clicking on Corrective Action link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.correctiveActionLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_CorrectiveAction_Tab = navigate_To_CorrectiveAction_Tab;

var navigate_To_EMR_Tab = function () {
    describe("Clicking on EMR link in Admin to check the functionality", function () {
        it("should Click - Clicking on EMR link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.emrLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_EMR_Tab = navigate_To_EMR_Tab;

var navigate_To_OpportunityStage_Tab = function () {
    describe("Clicking on Opportunity Stage link in Admin to check the functionality", function () {
        it("should Click - Clicking on Opportunity Stage link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.opportunityStageLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_OpportunityStage_Tab = navigate_To_OpportunityStage_Tab;

var navigate_To_OpportunityType_Tab = function () {
    describe("Clicking on Opportunity Type link in Admin to check the functionality", function () {
        it("should Click - Clicking on Opportunity Type link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.opportunityTypeLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_OpportunityType_Tab = navigate_To_OpportunityType_Tab;

var navigate_To_OrganizationType_Tab = function () {
    describe("Clicking on Organization Type link in Admin to check the functionality", function () {
        it("should Click - Clicking on Organization Type link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.organizationTypeLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_OrganizationType_Tab = navigate_To_OrganizationType_Tab;

var navigate_To_RootCause_Tab = function () {
    describe("Clicking on Root Cause link in Admin to check the functionality", function () {
        it("should Click - Clicking on Root Cause link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.rootCauseLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_RootCause_Tab = navigate_To_RootCause_Tab;

var navigate_To_Speciality_Tab = function () {
    describe("Clicking on Speciality link in Admin to check the functionality", function () {
        it("should Click - Clicking on Speciality link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.relationshipLink);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.specialityLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Speciality_Tab = navigate_To_Speciality_Tab;

var navigate_To_CustomList_Tab = function () {
    describe("Clicking on Custom List link in Admin to check the functionality", function () {
        it("should Click - Clicking on Custom List link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customListLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_CustomList_Tab = navigate_To_CustomList_Tab;

var navigate_To_EmailMetaData_Tab = function () {
    describe("Clicking on Email MetaData link in Admin to check the functionality", function () {
        it("should Click - Clicking on Email MetaData link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.emailMetaDataLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_EmailMetaData_Tab = navigate_To_EmailMetaData_Tab;

var navigate_To_CustomizationProfiles_Tab = function () {
    describe("Clicking on Customization Profiles link in Admin to check the functionality", function () {
        it("should Click - Clicking on Customization Profiles link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customProfilesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_CustomizationProfiles_Tab = navigate_To_CustomizationProfiles_Tab;

var navigate_To_LensProfiles_Tab = function () {
    describe("Clicking on Lens Profiles link in Admin to check the functionality", function () {
        it("should Click - Clicking on Lens Profiles link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.lensProfilesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_LensProfiles_Tab = navigate_To_LensProfiles_Tab;

var navigate_To_TabProfiles_Tab = function () {
    describe("Clicking on Tab Profiles link in Admin to check the functionality", function () {
        it("should Click - Clicking on Tab Profiles link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.tabProfilesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_TabProfiles_Tab = navigate_To_TabProfiles_Tab;

var navigate_To_CustomTabs_Tab = function () {
    describe("Clicking on Custom Tabs link in Admin to check the functionality", function () {
        it("should Click - Clicking on Custom Tabs link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customTabsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_CustomTabs_Tab = navigate_To_CustomTabs_Tab;

var navigate_To_UserDefinedLayouts_Tab = function () {
    describe("Clicking on User Defined Layouts link in Admin to check the functionality", function () {
        it("should Click - Clicking on User Defined Layouts link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userDefinedLayoutsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_UserDefinedLayouts_Tab = navigate_To_UserDefinedLayouts_Tab;

var navigate_To_QuickCreateComponents_Tab = function () {
    describe("Clicking on Quick Create Components link in Admin to check the functionality", function () {
        it("should Click - Clicking on Quick Create Components link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.quickCreateCompLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_QuickCreateComponents_Tab = navigate_To_QuickCreateComponents_Tab;

var navigate_To_UserDefinedFields_Tab = function () {
    describe("Clicking on User Defined Fields link in Admin to check the functionality", function () {
        it("should Click - Clicking on User Defined Fields link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.userDefinedFieldsLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_UserDefinedFields_Tab = navigate_To_UserDefinedFields_Tab;

var navigate_To_WorkflowTemplates_Tab = function () {
    describe("Clicking on Workflow Templates link in Admin to check the functionality", function () {
        it("should Click - Clicking on Workflow Templates link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            commands.checkAndPerform('click', browser, elements.adminPageElements.adminLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.customizationsLink);
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.workflowTemplatesLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_WorkflowTemplates_Tab = navigate_To_WorkflowTemplates_Tab;

var navigate_To_Interface_Tab = function () {
    describe("Clicking on Interface link in Admin to check the functionality", function () {
        it("should Click - Clicking on Interface link in Admin to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.interfaceLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_Interface_Tab = navigate_To_Interface_Tab;

var navigate_To_BulkImport_Tab = function () {
    describe("Clicking on Bulk Import link in User Menu to check the functionality", function () {
        it("should Click - Clicking on Workflow Templates link in User Menu to check the functionality", function (browser) {
            browser.pause(2000);
            commands.checkAndPerform('click', browser, elements.globalElements.headerMenu);
            browser.pause(1000);
            commands.checkAndPerform('click', browser, elements.adminPageElements.bulkImportLink);
            browser.pause(2000);
        });
    });
};
exports.navigate_To_BulkImport_Tab = navigate_To_BulkImport_Tab;

//Admin page navigation - ENDS