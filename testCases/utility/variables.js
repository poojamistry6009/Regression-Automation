/**
* variables.js
* @desc: setting information for JSON calls
* @options: none
* @returns: user defined variables
*/
var moment = require('moment');
var environment = 'test';
var url;
// var exports;
var path = require('path');
switch (environment) {
    case "test":
        exports.url = 'engr.hc1test.com';
        exports.hcAppsURL = 'test.hc1apps.com/healthcheck';
        exports.hcwebApiURL = 'wa.' + url + '/healthcheck';
        exports.tenantId = 'indusa';
        exports.campaignName = 'Add a person to a campaign';
        break;
    case "stage":
        exports.url = 'engr.hc1stage.com';
        exports.hcAppsURL = 'test.hc1apps.com/healthcheck';
        exports.hcwebApiURL = 'wa.' + url + '/healthcheck';
        exports.tenantId = 'indusa';
        exports.campaignName = 'Add a person to a campaign';
        break;
    case "wtaza":
        exports.url = 'wtaza.hc1test.com';
        exports.apiToken = '5b773eef-b2b8-4107-a44a-3fca57298310';
        exports.healthcheckURL = 'wtaza.hc1apps.com/healthcheck';
        exports.hcwebApiURL = 'wa.' + url + '/healthcheck';
        exports.tenantId = 'indusa';
        break;
    default: //test
        exports.url = 'engr.hc1test.com';
        exports.apiToken = 'a1fcbb8a-17ef-43fe-94a0-7ef5b11c999f';
        exports.healthcheckURL = 'test.hc1apps.com/healthcheck';
        exports.hcwebApiURL = 'wa.' + url + '/healthcheck';
        exports.campaignName = 'QACheckout - Campaign';
        exports.tenantId = 'qacheckout';
};
exports.username = 'pooja.mistry@indusa.com';
exports.password = 'Indusa@221';
// exports.username = 'manthan.buch@indusa.com';
// exports.password = 'Indusa@220';
// exports.password = 'Testing123$'; //stage
exports.usernameDisplay = 'Pooja';
exports.firstname = 'Pooja';
exports.lastname = 'Mistry';
// exports.firstname = 'Manthan';
// exports.lastname = 'Buch';
exports.username1 = 'sonu.patel@indusa.com';
exports.password1 = 'Indusa@220';
exports.username2 = 'himani.bhavsar@indusa.com';
exports.password2 = 'Indusa@220';
exports.usernameDisplay1 = 'Sonu';
exports.usernameDisplay2 = 'Himani';
exports.firstname1 = 'Sonu';
exports.lastname1 = 'Patel';
exports.base = 'https://';
exports.fullUrl = this.base + this.tenantId + '.' + this.url;
// exports.fullUrl = 'https://www.test.hc1cas.com/login?realm=indusa&redirect_uri=https%3A%2F%2Findusa.engr.hc1test.com%2F%23auth%2Flanding';

exports.biBaseUrl = this.base + this.tenantId +".bi."+this.url;
exports.contentType = 'application/json; charset=utf-8';
exports.accept = 'application/json, text/javascript, */*; q=0.01';
exports.apiTokenHead = '"X-HC1-API-TOKEN"';
exports.sessionIdHead = '"X-HC1-SESSIONID"';
var date = moment(new Date());
var today = moment(new Date()).format('MM/DD/YYYY');
exports.today = today;
var nextDay = moment(new Date()).add(24, 'hours').format('MM/DD/YYYY');
exports.nextDay = nextDay;




//Organization tab variables - START
exports.defaultOrgName = 'test';
exports.testCustomListName = 'testCustomList_'+date.format("YYYYMMMDHHmmss");
exports.newCustomListName = 'newCustomList123_'+date.format("YYYYMMMDHHmmss");
exports.newCustomListTestName = 'newCustomListTest123_'+date.format("YYYYMMMDHHmmss");
exports.createdCustomList = '';
exports.retrievedCustomListName = '';
exports.newCloneCustomListName = 'cloneCustomList_'+date.format("YYYYMMMDHHmmss");
exports.clonedCustomList = '';
exports.newCustomListName_updated = 'updatedTestCustomList_'+date.format("YYYYMMMDHHmmss");
exports.updatedCustomList = '';
exports.numberAvailableColumnName = 'Number';
exports.newOrgName = 'test_Org_'+date.format("YYYYMMMDHHmmss");
exports.newChildOrgName = 'test_childOrg_'+date.format("YYYYMMMDHHmmss");
exports.newOrgName2 = 'test_Org2_'+date.format("YYYYMMMDHHmmss");
exports.newOrgNameContact = 'contact_Org_'+date.format("YYYYMMMDHHmmss");
exports.orgHostCode = 'ORG';
exports.orgHostCodeNumber = Math.floor((Math.random() * 2000) + 1);
exports.orgCompleteHostCode = this.orgHostCode+""+this.orgHostCodeNumber+date.format("YYYYMMMDHHmmss");
exports.hostCodeNumber = 'test_12345_'+date.format("YYYYMMMDHHmmss");
exports.availableColumnsName = [];
exports.availableColumnsCount = '';
exports.filterAvailableColumnsName = [];
exports.filterAvailableColumnsCount = '';
exports.filterAvailableColumnsCount = '';
exports.newAttachmentDescription = 'test attachment description';
exports.createdORG = '';
exports.createdChildORG = '';
exports.createdORG2 = '';
exports.createdORGHostCode = '';
exports.createdChildORGHostCode = '';
exports.orgNumber = '12345';
exports.opportunityName = 'SampleOpportunity_'+date.format("YYYYMMMDHHmmss");
exports.opportunityProbability = '30';
exports.metaTagClipboardTitle = 'Copy to Clipboard';
exports.updatedOrgName = 'Test_Updated'+date.format("HHmmss");
exports.phone = '123456';
exports.phone_updated = '12345678';
exports.fax = '123456';
exports.fax_updated = '12345678';
exports.testURL = 'https://google.com';
exports.locationDesc = 'test description';
exports.locationDesc_updated = 'test description updated';
exports.mailing_streetValue = 'mailing_streetValue1';
exports.mailing_streetValue2 = 'mailing_street2Value1';
exports.mailing_streetValueBilling = 'mailing_streetBillingValue1';
exports.mailing_street2Value = 'mailing_streetValue2';
exports.mailing_stateValue = 'mailing_stateMaillingValue2';
exports.mailing_cityValue  = 'city1' ;
exports.mailling_postalCodeValue = '5454554';
exports.mailling_countryValue = 'IND';
exports.billing_cityValue = 'mailing_cityBillingValue2';
exports.billing_stateValue = 'mailing_stateBillingValue2';
exports.billing_postalCodeValue = '4654654';
exports.billing_CounrValue = 'xyz';
exports.newUploadName = 'testUploadName_'+date.format('mmss');
//Organization tab variables - END


//Opportunity tab variables - START
exports.opporDetailsDesc = 'test opportunity details description';
exports.opportunityNewCustomListName = 'customListName_'+date.format("YYYYMMMDHHmmss");
exports.opportunityAttachmentsName = 'attachment_'+date.format("YYYYMMMDHHmmss");
exports.createdOpporTask = '';
//Opportunity tab variables - END


//Activities tab variables - START
exports.newCaseName = 'testOrgCase_'+date.format("YYYYMMMDHHmmss");
exports.newCaseName_updated = 'updated_case_'+date.format("YYYYMMMDHHmmss");
exports.newCaseMemoName = 'testCase_Memo_'+date.format("YYYYMMMDHHmmss");
exports.newCaseTaskName = 'testCase_Task_'+date.format("YYYYMMMDHHmmss");
exports.createdCase = '';
exports.createdMemo = '';
exports.createdTask = '';
exports.createdOpportunity = '';
exports.opportunityUpdateName = 'updateOpportunity_'+date.format("YYYYMMMDHHmmss");
exports.newTaskName = 'testOrgTask_'+date.format("YYYYMMMDHHmmss");
exports.updatedTaskName = 'testOrgTask_updated_'+date.format("YYYYMMMDHHmmss");
exports.newMemoName = 'testOrgMemo_'+date.format("YYYYMMMDHHmmss");
exports.caseSubject = 'Sample Case From Cloud  ';
exports.taskSubject = 'Sample Task From Cloud  ';
exports.taskSubjectUpdated = 'testOrgTask_updated_'+date.format("YYYYMMMDHHmmss");
exports.memoSubject = 'Sample Memo From Cloud  ';
exports.taskDesc = 'sample task description';
exports.taskDescUpdated = 'sample task description updated';
exports.memoSubjectForContact = 'Sample Memo created from contact';
exports.memoSubjectUpdated = 'Sample Memo From Cloud updated';
exports.memoDesc = 'sample memo description';
exports.memoDescUpdated = 'sample memo description updated';
exports.formattedDate = date.format("L");
exports.formattedTime = date.format("h:mmA");
exports.resolutionDesc = 'test resolution description';


exports.activityPageElements = '';
exports.activityDropDownlist = 'activitySubCategory';
exports.activitySubCategoryFieldInput = 'ActivitySub' + date.format("YYYYMMMDHHmmss");
exports.activitySubCategoryDescInput = 'testing';
exports.activitySubCategoryDescAppendInput = 'testing2';
exports.distribution_group_name = 'test group';
exports.campaignTypeInput = 'ActivitySub' + date.format("YYYYMMMDHHmmss");
//Activities tab variables - END


//Admin - HRM Accesscontrol page variables - START
exports.controlTreeNameCreated = '';
exports.newControlTreeName = 'testControlTree_'+date.format("YYYYMMMDHHmmss");
exports.newChildNodeName = '';
exports.newRootNodeName = '';
//Admin - HRM Accesscontrol page variables - END

//Admin - Activity Category page variables - START
exports.activitySubCategoryName = 'ActivitySub' + date.format("YYYYMMMDHHmmss");
exports.newCategoryStatusName = 'testCategoryStatus_'+date.format("YYYYMMMDHHmmss");
//Admin - Activity Category page variables - END

//Admin - Correction action page variables - START
exports.correctionActionNameInput = 'testCorrectionAction_' + date.format("YYYYMMMDHHmmss");
exports.correctionActionName = 'testCorrectionAction_' + date.format("YYYYMMMDHHmmss");
//Admin - Correction action page variables - END

//Admin - EMR page variables - START
exports.emrNameInput = 'test_Emr_'+date.format("YYYYMMMDHHmmss");
exports.emrName = 'test_Emr_'+date.format("YYYYMMMDHHmmss");
exports.createdEMR = '';
//Admin - EMR page variables - END

//Competitive lab  element - END
exports.competitiveLabName = 'competitiveLab_'+date.format("YYYYMMMDHHmmss");
//Competitive lab element - END

//Admin - Campaign type variables - START
exports.campaignTypeNameInput = 'testCampaignType_' + date.format("YYYYMMMDHHmmss");
exports.campaignTypeName = 'testCampaignType_' + date.format("YYYYMMMDHHmmss");
exports.campaignTypeNameInputUpdated = 'updatedCampaignType_' + date.format("YYYYMMMDHHmmss");
exports.createdCampaignType = '';
//Admin - Campaign type variables - END

//Admin - Contact Type page variables - START
exports.contactTypeNameInput = 'testContactType_' + date.format("YYYYMMMDHHmmss");
exports.contactTypeName = 'testContactType_' + date.format("YYYYMMMDHHmmss");
//Admin - Contact Type page variables - END

//Contacts tab variables - START
exports.contactFName  = 'testContactFirstname';
exports.contactLName  = 'testContactLastname';
//Contacts tab variables - END

//Messages tab variables - START 
exports.newMessageSubject = 'test_'+date.format("YYYYMMMDHHmmss");
exports.newMessage = 'Test message';
//Messages tab variables - END

//Malling address
exports.mailing_streetValue =                                        'mailing_streetValue1';
exports.mailing_streetValue2 =                                       'mailing_street2Value1';
exports.mailing_streetValueBilling =                                 'mailing_streetBillingValue1';
exports.mailing_street2Value =                                       'mailing_streetValue2';
exports.mailing_stateValue =                                         'mailing_stateMaillingValue2';
exports.mailing_cityValue  =                                         'city1' ;
exports.mailling_postalCodeValue =                                   '5454554';
exports.mailling_countryValue =                                      'IND';

exports.billing_cityValue =                                          'mailing_cityBillingValue2';
exports.billing_stateValue =                                         'mailing_stateBillingValue2';
exports.billing_postalCodeValue =                                    '4654654';
exports.billing_CounrValue =                                         'xyz';

// Fee schedule
exports.searchField_FeeScheduleValue =                                'test2';
exports.searchField_FeeScheduleValue2 =                                'test';

exports.panelsNameValueNot  =						     'panelNameNot';
// For Fees And Schedule
exports.createdCase = '';
exports.nameFeesSchedule =				    		 'feesName1_'+date.format("YYYYMMMDHHmmss");
exports.nameFeesScheduleName2 =						 'feesName2_'+date.format("YYYYMMMDHHmmss");
exports.descriptionFeesSchedule =					 'fees_'+date.format("YYYYMMMDHHmmss");
exports.panelValue =						    	 'fees_'+date.format("YYYYMMMDHHmmss");
exports.panelPriceValue =						     '265';
exports.createdMemo = '';
exports.createdTask = '';
exports.createdOpportunity = '';
exports.panelsNameValue  =						     'panelName1' + date.format("YYYYMMMDHHmmss");
exports.panelsDescriptionValue =					 'panelName1' + date.format("YYYYMMMDHHmmss");
exports.caseSubject = 'Sample Case From Cloud  ';
exports.taskSubject = 'Sample Task From Cloud  ';
exports.panelsName2Value  =						     'panelName2' + date.format("YYYYMMMDHHmmss");
exports.memoSubject = 'Sample Memo From Cloud  ';
exports.taskDesc = 'sample task description';
exports.taskDescUpdated = 'sample task description updated';
exports.memoSubjectForContact = 'Sample Memo created from contact';
exports.memoSubjectUpdated = 'Sample Memo From Cloud updated';
exports.memoDesc = 'sample memo description';
exports.panelsDescriptionValue2 =					 'panelName2' + date.format("YYYYMMMDHHmmss");
exports.formattedDate = date.format("L");
exports.formattedTime = date.format("h:mmA");
exports.resolutionDesc = 'test resolution description';

//For Activity
exports.activityPageElements = '';

exports.activityDropDownlist = 				'activitySubCategory';
exports.activitySubCategoryFieldInput =  		'ActivitySub' + date.format("YYYYMMMDHHmmss");
exports.activitySubCategoryDescInput = 			'testing';
exports.activitySubCategoryDescAppendInput = 		'testing2';
//Admin - HRM Access control page variables - START
exports.controlTreeNameCreated = '';
exports.newControlTreeName = 'testControlTree_'+date.format("YYYYMMMDHHmmss");
exports.newChildNodeName = '';
exports.newRootNodeName = '';
exports.newCategoryName = 'testCategory_'+date.format("YYYYMMMDHHmmss");
exports.createdActivityCategoryName = '';
exports.createdSubActivityCategoryName = '';
exports.createdSubActivityCategoryDesc = '';
exports.cloneControlTreeName = 'clonedTree_'+date.format("YYYYMMMDHHmmss");
exports.createdClonedTree = '';
//Admin - HRM Access control page variables - END


//Admin - opportunity stage pge variables - START
exports.opporStageFieldInput = 'testOpporStage_'+date.format("YYYYMMMDHHmmss");
exports.opporStageProbablity = Math.floor((Math.random() * 100) + 1);
exports.createdOpporStage = '';
exports.newfilterCustomListName = 'filterBy_Contact_'+date.format("YYYYMMMDHHmmss");
exports.totalLifecycleMonths = 1;
exports.estimatedOrdersDay = 1;
exports.daysInWork = 1;
//Admin - opportunity stage pge variables - END

//Root ceause element - START
exports.rootCauseName = 'rootCause_'+date.format("YYYYMMMDHHmmss");
//Root clause element - END

//Competitive lab  element - END
exports.competitiveLabName = 'competitiveLab_'+date.format("YYYYMMMDHHmmss");
exports.createdCompetitiveLab = '';
//Competitive lab element - END

//Admin - Specialty page variables - START
exports.specialtyFieldInput = 'testSpecialty_'+date.format("YYYYMMMDHHmmss");
exports.specialtyRevenuePerOrder = Math.floor((Math.random() * 100) + 1);
exports.createdSpecialty = '';
exports.specialtyFieldName = 'testSpecialty_'+date.format("YYYYMMMDHHmmss");
exports.createdSpecialtyRevenuePerOrder = '';
exports.defaultSpecialtyName = 'Plastic Surgery';
exports.defaultSpecialtyRevenuePerOrder = '227';
//Admin - Specialty page variables - END

//Admin - Contact Type page variables - START
exports.contactTypeNameInput = 'testContactType_' + date.format("YYYYMMMDHHmmss");
//Admin - Contact Type page variables - END

//Admin - Correction action page variables - START
exports.correctionActionNameInput = 'testCorrectionAction_' + date.format("YYYYMMMDHHmmss");
//Admin - Correction action page variables - END


//Admin - Activity status page variables - START
exports.activityStatusName = 'testactivityStatus_' + date.format("YYYYMMMDHHmmss");
//Admin - Activity status page variables - END


//Admin - Opportunity Stag page variables - START
exports.opportunityStageName = 'testOpportunityStage_' + date.format("YYYYMMMDHHmmss");
exports.opportunityStageProbabillity = '21';
//Admin - Opportunity Stag page variables - START

//Admin - Opportunity Type page variables - START
exports.opportunityTypeName = 'testOpportunityStage_' + date.format("YYYYMMMDHHmmss");
//Admin - Opportunity Type page variables - START


//Admin - Organization Type page variables - START
exports.organizationTypeName = 'testOrganizationStage_' + date.format("YYYYMMMDHHmmss");
//Admin - Organization Type page variables - START

//Admin - EMR page variables - START
exports.emrNameInput = 'test_Emr_'+date.format("YYYYMMMDHHmmss");
exports.createdEMR = '';
//Admin - EMR page variables - END

//Admin - Panels and fee schedules variables - START
exports.createdPanel = '';
exports.nameFeesSchedule = 'test_feeSchedule1_'+date.format("YYYYMMMDHHmmss");
exports.nameFeesScheduleName2 = 'test_feeSchedule2_'+date.format("YYYYMMMDHHmmss");
exports.descriptionFeesSchedule = 'test_feeSchedule_'+date.format("YYYYMMMDHHmmss");
exports.panelValue = 'test_panel_'+date.format("YYYYMMMDHHmmss");
exports.panelPriceValue = '265';
exports.panelsNameValue  = 'test_panel1' + date.format("YYYYMMMDHHmmss");
exports.panelsDescriptionValue = 'test_panel1desc' + date.format("YYYYMMMDHHmmss");
exports.panelsName2Value  = 'test_panel2' + date.format("YYYYMMMDHHmmss");
exports.panelsDescriptionValue2 = 'test_panel2desc' + date.format("YYYYMMMDHHmmss");

//Admin - Panels and fee schedules variables - END

//Admin - Campaign type variables - START
exports.campaignTypeNameInput = 'testCampaignType_' + date.format("YYYYMMMDHHmmss");
exports.campaignTypeNameInputUpdated = 'updatedCampaignType_' + date.format("YYYYMMMDHHmmss");
exports.createdCampaignType = '';
//Admin - Campaign type variables - END


//Admin - Campaign stage variables - START
exports.campaignStageName = 'testCampaignStage_' + date.format("YYYYMMMDHHmmss");
exports.campaignStageNameInputUpdated = 'updatedCampaignStage_' + date.format("YYYYMMMDHHmmss");
exports.createdCampaignStage = '';
exports.campaignDependencyStage_First = '';
//Admin - Campaign stage variables - END

//Admin - User admin variables - START
exports.newUserEmail = 'testUser_'+date.format("mmss")+"@test.in";
exports.newUserFName = 'userFName_'+date.format("mmss");
exports.newUserFNameUpdated = 'updatedFName_'+date.format("mmss");
exports.newUserLNameUpdated = 'updatedLName_'+date.format("mmss");
exports.newUserLName = 'userLName_'+date.format("mmss");
exports.createdNewUser
//Admin - User admin variables - END


//Admin - User profile variables - START
exports.newUserProfileName = 'SampleProfile_'+date.format("YYYYMMMDHHmmss");
exports.newUserProfileDesc = 'sample user profile description';
exports.createdUserProfile = '';
//Admin - User profile variables - END

//Admin - User roles variables - START
exports.newRoleName = 'SampleRole_'+date.format("YYYYMMMDHHmmss");
exports.newRoleDesc = 'sample role description';
exports.createUserRole = '';
//Admin - User roles variables - START

//Admin - distribution groups variables - START
exports.newGroupName = 'SampleGroup_'+date.format("YYYYMMMDHHmmss");
exports.newGroupEmail = 'testmail_'+date.format("YYYYMMMDHHmmss")+'@test.in';
exports.newGroupDesc = 'sample description';
exports.createdGroupName = '';
//Admin - distribution groups variables - END

//Admin - Notification settings variables - START
exports.createdNotificationSetting = '';
exports.newNotificationSettingName = 'sampleSetting_'+date.format("YYYYMMMDHHmmss");
//Admin - Notification settings variables - END


//Admin - Server credentials variables - START
exports.newServerName = 'SampleServer_'+date.format("YYYYMMMDHHmmss");
exports.newServerUsername = 'testuser';
exports.newServerPwd= 'testserver';
exports.newServerDesc = 'test desc';
exports.createdServer = '';
//Admin - Server credentials variables - END


//Admin - Diagnosis variables - START
exports.newDiagnosisCode = 'sampleDiagnosis_'+date.format("mmss");
exports.createdDiagnosis = '';
//Admin - Diagnosis variables - END


//Admin - Panel types variables - START
exports.newPanelTypeName = 'testPanelType_'+date.format("mmss");
exports.createdPanelType = '';
//Admin - Panel types variables - END


//Admin - Procedures variables - START
exports.createdProcedure = '';
exports.newProcedureCode = 'testProcedure_'+date.format("mmss");
//Admin - Procedures variables - END


//Admin - Tests variables - START
exports.newTestName = 'sampleTest_'+date.format("mmss");
exports.createdTest = '';
//Admin - Tests variables - END

//Admin - Customizations variables - START
exports.newListName = 'sampleList_'+date.format("mmss");
exports.newListNameUpdated = 'updatedSampleList_'+date.format("mmss");
exports.createdList = '';
//Admin - Customizations variables - END


//Contacts tab variables - START
exports.contactFName   = 'conFName_'+date.format("ss");
exports.contactFName2   = 'conFName2_'+date.format("ss");
exports.providerFName   = 'provFName_'+date.format("ss");
exports.providerFName2   = 'provFName2_'+date.format("ss");
exports.contactFNameUpdated   = 'conUpdatedFName_'+date.format("ss");
exports.createdContact = '';
exports.createdContact2 = '';
exports.createdContactFname = '';
exports.createdContactLname = '';
exports.createdProvider = '';
exports.createdProvider2 = '';
exports.createdProviderFname = '';
exports.createdProviderLname = '';
exports.createdProviderMname = '';
exports.createdContactMname = '';
exports.contactLName   = 'conLName_'+date.format("ss");
exports.contactLName2   = 'conLName2_'+date.format("ss");
exports.providerLName   = 'provLName_'+date.format("ss");
exports.providerLName2   = 'provLName2_'+date.format("ss");
exports.contactLNameUpdated  = 'conUpdatedLName_'+date.format("ss");
exports.contactMName = 'conMName_'+date.format("ss");
exports.providerMName = 'provMName_'+date.format("ss");
exports.contactMNameUpdated = 'conUpdatedMName_'+date.format("ss");
exports.contactSuffix = 'test';
exports.contactSuffixUpdated = 'updated';
exports.contactEmail = this.username;
exports.contactPhone = '12345';
exports.contactPhoneUpdated = '12345678';
exports.contactMobile = '12345';
exports.contactMobileUpdated = '12345678';
exports.contactFax = '12345';
exports.contactFaxUpdated = '12345678';
exports.contactJobTitle = 'jobtitle';
exports.contactJobTitleUpdated = 'jobtitleupdated';
exports.contactDepartment = 'department';
exports.contactDepartmentUpdated = 'departmentupdated';
exports.contactCredential = 'credential';
exports.contactCredentialUpdated = 'credentialupdated';
exports.contactMailingStreet1 = 'mailing street1';
exports.contactMailingStreet2 = 'mailing street2';
exports.contactMailingCity = 'mailing city';
exports.contactMailingState = 'mailing state';
exports.contactMailingPostal = 'mailing postal';
exports.contactMailingCountry = 'mailing country';
exports.contactBillingStreet1 = 'billing street1';
exports.contactBillingStreet2 = 'billing street2';
exports.contactBillingCity = 'billing city';
exports.contactBillingState = 'billing state';
exports.contactBillingPostal = 'billing postal';
exports.contactBillingCountry = 'billing country';
exports.contactAttachmentDesc = 'test description';
exports.providerNPI = 'test';
//Contacts tab variables - END


//Messages tab variables - START 
exports.newMessageSubject = 'test_'+date.format("YYYYMMMDHHmmss");
exports.newMessage = 'Test message';
exports.newMessageSubject2 = 'test2_'+date.format("YYYYMMMDHHmmss");
exports.newMessage2 = 'Test message 2';
exports.replyMsgSubject  = 'testReply_'+date.format("YYYYMMMDHHmmss");
exports.replyMessageBody = 'Test reply message';
exports.sentOrgMessageSubject = '';
exports.sentMessage = '';
exports.sentCaseMessage = '';
exports.sentTaskMessage = '';
exports.sentMemoMessage = '';
exports.sentOpportunityMessage = '';

//Messages tab variables - END


//Orders and patients tab variables - START
exports.newPatientFName = 'testpatientFname_'+date.format('YYYYMMMDHHmmss');
exports.newPatientLName = 'testpatientLname_'+date.format('YYYYMMMDHHmmss');
exports.newSSNNumber                    = 'ssn_'+date.format('YYYYMMMDHHmmss');
exports.newMRNNumber                    = 'mrn_'+date.format('YYYYMMMDHHmmss');
exports.newSuffixAdd                    = 'sTest_'+date.format('YYYYMMMDHHmmss');
exports.newEmailAdd                     = 'email_Test'+date.format('YYYYMMMDHHmmss');
exports.newMobileNumberAdd              = 'mobile_Number'+date.format('YYYYMMMDHHmmss');
exports.newOrgForPatients               = 'test_'+date.format('YYYYMMMDHHmmss');
exports.createdPatient = '';

exports.createdPatientFName = '';
exports.createdPatientLName = '';
exports.orderAndPatientFirstNameSuffix      = 'test';
exports.patientFirstNameSuffix              = 'test';
exports.startDateOrderLsitPage              = '03/30/2017';
exports.newPhoneNumber                      = 'mobile_Number'+date.format('YYYYMMMDHHmmss');
exports.patientMiddleName                = 'middleName_'+date.format('YYYYMMMDHHmmss');
exports.newPatientFirstName                 = 'newFirstName_'+date.format('YYYYMMMDHHmmss');
exports.hostCodeNumber                      ='hostCode_'+date.format('YYYYMMMDHHmmss');

//Orders and patients tab variables - END


//Campaigns tab variables - START
exports.newCampaignNameContact = 'testCampaignContact_'+date.format('YYYYMMMDHHmmss');
exports.newCampaignNamePatient = 'testCampaignPatient_'+date.format('YYYYMMMDHHmmss');
exports.createdCampaign = '';
exports.createdCampaignPatient = '';
exports.createdCampaignContact = '';
exports.updatedCampaignName = 'updatedCampaign_'+date.format('YYYYMMMDHHmmss');
exports.createdCampaignContactFname = '';
exports.createdCampaignContactLname = '';
exports.campaignMessageSubject = 'test message subject';
exports.campaignMsgTemplateName = 'testMsgTemplate_'+date.format('YYYYMMMDHHmmss');
exports.campaignMsgTemplateSubject = 'test message subject';
exports.campaignMsgTemplateName_updated = 'testTemplateUpdated_'+date.format('YYYYMMMDHHmmss');
exports.campaignMsgTemplateEmailText = 'test email text';
exports.createdMsgTemplate = '';
exports.campaignMsgTempalteSMSText = 'test sms text';
exports.campaignPatientFName = 'patientFName_'+date.format('YYYYMMMD');
exports.campaignPatientLName = 'patientLName_'+date.format('YYYYMMMD');
exports.createdCampaignPatientFName = '';
exports.createdCampaignPatientLName = '';
exports.campaignAudienceMsgSubject = 'testSubject_'+date.format('YYYYMMMD');
exports.campaignAudienceMsgSubject_blank = 'testSubject_'+date.format('YYYYMMMD')+"_blankTest";
exports.campaignAudienceMsg = 'test message_'+date.format('YYYYMMMD');
exports.sentCampaignMessageSubject = '';
//Campaigns tab variables - END

exports.tempUrl = '';

exports.weakUserName  = 'indusa';
exports.weakUserPassword  = 'indusa';
exports.newPassword = this.password+'_new'; //To create a new password
exports.confirmPassword = this.password+'_new'; //To create a new password
exports.accountFirstName = 'First';
exports.accountLastName = 'Last';
exports.accountsmsNumber = '2015555555';
exports.weakPassword = '@19';

//Execution related variables - start
exports.executionCounter = 0;
exports.executionLimit = 1; //Run maximum 2 test plan together, if executionCounter exceed this, restart the browser

//Execution related variables - end

//dependency variables - start
exports.createdPanel = '';
exports.dependencyFullfilledFlag = false;
//dependency variables - end

exports.uploadName = 'test upload';
exports.uploadFileName = 'test_document.txt';
exports.uploadFilePath = 'E:/workspace';

exports.dependencyFulfilled = [];
exports.dependenciesRequired = [];

//Application variables - start
exports.currentAppVersion = "6.4.7";
exports.currentAppBuildNo = '21';
exports.currentAppBuildDate = '2017-07-21_13-11-14';
//Application variables - end

exports.testFileUploadName = 'testUploadFile.txt';
exports.testFileUploadPath = path.resolve()+"\\"+this.testFileUploadName;
exports.testContactsImportFileName = 'testContactsList.txt';
exports.testContactsImportFilePath = path.resolve()+"\\"+this.testContactsImportFileName;
exports.testOrganizationImportFileName = 'testOrgList.txt';
exports.testOrganizationImportFilePath = path.resolve()+"\\"+this.testOrganizationImportFileName;

//bulk import
exports.newConMappingName = 'map_contact_'+date.format('mmss');
exports.newOrgMappingName = 'map_org_'+date.format('mmss');

exports.wftCaseName         = 'WftCaseName_' + date.format("YYYYMMMDHHmmss");
exports.wftCaseDesc         = 'WftCaseDesc_' + date.format("YYYYMMMDHHmmss");
exports.wftTaskName         = 'WftTaskName_' + date.format("YYYYMMMDHHmmss");
exports.wftTaskDesc         = 'WftTaskDesc_' + date.format("YYYYMMMDHHmmss");

//Web service variable - starts
exports.settinsTableRow              = 'Name Connection ID Resend URL';
exports.interfaceName                = 'interface_' + date.format("YYYYMMMDHHmmss");
exports.interfaceUpdateName          = 'Updateinterface_' + date.format("YYYYMMMDHHmmss");
exports.interfaceConnectionId        = date.format("YYYYMMMDHHmmss");;
exports.distributionGroupName        = 'distribution_' + date.format("YYYYMMMDHHmmss");
exports.createdDistributionGroup = '';
exports.distributionGroupUpdateName  = 'updateDistribution_' + date.format("YYYYMMMDHHmmss");
exports.distributionGroupEmail       = 'test_' + date.format("YYYYMMMDHHmmss") + '@test.in';
exports.distributionGroupDesc        = 'test Description';
exports.distributionGroupUpdateDesc  = 'test update Description';
exports.userProfileName              = 'userProfileName_' + date.format("YYYYMMMDHHmmss");
exports.createdUserProfile = '';
exports.userProfileDesc              = 'userProfileDesc_' + date.format("YYYYMMMDHHmmss");
exports.newMessageDistributionSubject = 'testDistribution_'+date.format("YYYYMMMDHHmmss");
//Web service variable - ends