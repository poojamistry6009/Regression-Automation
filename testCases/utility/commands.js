
var expect = require('chai').expect;
var assert = require('assert');
var variables = require('./variables.js');
var browserOperations = require('../pages/browserControl.js');

module.exports.checkAndPerform = function(operation,browser,elementXPath) {
    return browser.waitForElementPresent(elementXPath,10000,function () {
        // browser.moveToElement(elementXPath,300,300,function () {
            browser.expect.element(elementXPath).to.be.visible;
            switch(operation){
                case 'url':{
                    browser.url(elementXPath);
                    break;
                }
                case 'click':{
                    browser.getLocationInView(elementXPath)
                        .assert.visible(elementXPath)
                        .click(elementXPath);
                    break;
                }
                case 'clearValue':{
                    browser.clearValue(elementXPath);
                    break;
                }
            }
        // });
    });
};

module.exports.checkAndSetValue = function(browser,elementXPath,value) {
    return browser.waitForElementPresent(elementXPath,5000,function () {
        browser.expect.element(elementXPath).to.be.visible;
        browser.setValue(elementXPath,value);
    });
};

module.exports.readValuesAndAssert = function(browser,elementXPath,value,tobe){
    return browser.waitForElementPresent(elementXPath,5000,function () {
        browser.expect.element(elementXPath).to.be.present;
        browser.elements("xpath", elementXPath, function (result) {
            var els = result.value;
            els.forEach(function (el) {
                browser.elementIdText(el.ELEMENT, function(text) {
                    if(tobe){
                        if(text.value.includes(value)){
                            expect(text.value.includes(value)).to.be.true;
                        }
                        else{
                            expect(text.value.includes(value)).to.be.false;
                        }
                    }
                    else{
                        expect(text.value.includes(value)).to.be.false;
                    }
                });
            });
        });
    });
};

module.exports.checkAndGetText  =  function (browser,elementXPath) {
    browser.waitForElementPresent(elementXPath,5000,function () {
        browser.expect.element(elementXPath).to.be.visible;
        browser.getText(elementXPath,function (result) {
            return result.value;
        });
    });
}

module.exports.getColIndexOf = function (browser,colIndexOf,attributeToRead,table,callback) {
    browser.expect.element(table).to.be.visible;
    browser.elements("xpath", table+"/thead/tr/th", function (result) {
        var els = result.value;
        var count = 0;
        els.forEach(function (el) {
            if(attributeToRead){
                browser.elementIdAttribute(el.ELEMENT,attributeToRead, function (text) {
                    count = count + 1;
                    if(text.value){
                        if(text.value.includes(colIndexOf)){
                            callback(count);
                        }
                    }
                });
            }
            else{
                browser.elementIdText(el.ELEMENT,function (result) {
                    count = count + 1;
                    if(result.value.includes(colIndexOf)){
                        callback(count);
                    }
                });
            }

        });
    });
}

module.exports.checkAndRestartBrowser = function (browser) {
    console.log('inside check and restart browser');
    variables.executionCounter = variables.executionCounter + 1;
    if(variables.executionCounter > variables.executionLimit){
        console.log('TEST PLAN EXECUTION COUNTER : '+variables.executionCounter+' exceeds '+variables.executionLimit+'. Restarting browser now ...');
        browserOperations.restartBrowser(browser,function (result) {
            console.log('>>result : '+JSON.stringify(result));
            variables.executionCounter = 0;
        });
    }
}


var endBrowserSession = function (callback) {
    describe("End browser session", function () {
        it("should end browser session", function (browser) {
            browser.pause(2000);
            browser.end();
            browser.pause(2000);
            callback(true);
        });
    });
};
exports.endBrowserSession = endBrowserSession;

var checkDependencyListFulfilled = function () {
    describe("Dependency List fulfilled", function () {
        it("should check dependency list fulfilled", function (browser) {
            browser.pause(2000);
            for(var i=0;i<variables.dependenciesRequired.length;i++){
                try {
                    expect(variables.dependencyFulfilled.indexOf(variables.dependenciesRequired[i]) != -1).to.be.true;
                    assert.notEqual(variables.dependencyFulfilled.indexOf(variables.dependenciesRequired[i]), -1, 'Dependency '+variables.dependenciesRequired[i]+' is not fulfilled !');
                    var varValue = eval('variables.'+variables.dependenciesRequired[i]);
                    console.log('Object created : '+variables.dependenciesRequired[i]+' with value : '+varValue);
                    variables.dependencyFullfilledFlag = true;
                }

                catch(err) {
                    console.log('dependencyFulfilled : '+variables.dependencyFulfilled);
                    console.log('dependenciesRequired : '+variables.dependenciesRequired);
                    console.log('dependency is not fullfilled. !!! ');
                    process.exit();
                    browser.end();
                }

            }
        });
    });

};
exports.checkDependencyListFulfilled = checkDependencyListFulfilled;

var fillDependencyRequiredList = function (list) {
    describe("Dependency List", function () {
        it("should check dependency list", function (browser) {
            browser.pause(2000);
            variables.dependenciesRequired = list;
            console.log(variables.dependenciesRequired);
            console.log(variables.dependenciesRequired.length);
        });
    });
};
exports.fillDependencyRequiredList = fillDependencyRequiredList;

var assertAndFillDependencyList = function (dependencyName,tableName) {
    describe("Assert and fill dependency list", function () {
        it("should assert and fill dependency fulfilled list", function (browser) {
            var varValue = eval('variables.'+dependencyName);
            browser.elements("xpath", tableName, function (result) {
                var els = result.value;
                els.forEach(function (el) {
                    browser.elementIdText(el.ELEMENT, function (text) {
                        console.log(text.value);
                        console.log('var value found for '+dependencyName+' : '+varValue);
                        expect(text.value.includes(varValue)).to.be.true;
                        variables.dependencyFulfilled.push(dependencyName);
                    });
                });
            });
        });
    });
};
exports.assertAndFillDependencyList = assertAndFillDependencyList;

