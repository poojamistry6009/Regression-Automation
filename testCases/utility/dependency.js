/**
 * Created by INDUSA
 */
var exports;

 var dependencyMap = {
    'userLogin'               :'false',
    'userLogout'              :'true',
    'organizationObject'      :'false',
    'contactObject'           :'false'
};
exports.dependencyMap = dependencyMap;


function getDependencyState(keyToMatch) {
    for (key in dependencyMap){
        if(key === keyToMatch){
            return dependencyMap[key];
        }
    }
}
exports.getDependencyState = getDependencyState;


function changeStateOf(keyToChange,value){
    for (key in dependencyMap){
        if(key === keyToChange){
            dependencyMap[key] = value;
        }
    }
}
exports.changeStateOf = changeStateOf;



//Function to get error text for the dependency variables
var getErrorText = function (varName) {
    switch(varName){
        case 'userLogin':
        {
            return 'User login required';
        }
        case 'userLogout':
        {
            return 'User logout required';
        }
        case 'organizationObject':
        {
            return 'Organization object required';
        }
        case 'contactObject':
        {
            return 'Contact object required';
        }
    }
};
exports.getErrorText = getErrorText;

//Array of error texts to show in the error output
var errorList = [];
exports.errorList = errorList;




var checkDependency = function (arrOfVars) {
    errorList = [];
    var result = true;
    for(var i=0;i<arrOfVars.length;i++){
        if(arrOfVars[i].toString().includes("=")) {
            var varStatus = false;

            if(arrOfVars[i].split('=')[1] === 'false')
                varStatus = false;
            else
                varStatus = true;

            var varName = arrOfVars[i].split('=')[0];
            if (!varStatus) {
                result = false;
                errorList.push(getErrorText(varName));
            }
        }
    }
    return result;
};
exports.checkDependency = checkDependency;


var getErrorList = function () {
    var errorListStr = '';
    for(var i =0;i<errorList.length;i++){
        errorListStr = errorListStr.concat(errorList[i]+", ");
    }
    if(errorListStr.endsWith(', ')){
        errorListStr = errorListStr.substr(0,errorListStr.length-2);
    }
    return errorListStr;
};
exports.getErrorList = getErrorList;