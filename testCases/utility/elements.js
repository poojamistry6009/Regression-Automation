//GLOBAL ELEMENTS - START
var globalElements =
    {
        goToHomeIcon                                    :'/html/body/div[1]/header/div[1]/div/div/div/div/a', //HC1 icon link redirecting to home
        headerUsernameDropdown      					:'/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[1]/a',  //Admin menu username link (firefox xpath)
        headerAdminOption           					:'/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[1]/ul/li[2]',   //Admin option from header menu
        submitBtn                   					:'//*[@id="submit"]',   //Global submit button
        headerMenu                                      :'//*[@id="global_nav"]/ul/li[1]',      //Admin menu username link (Chrome xpath)
        logoutOption                                    :'//*[@id="logout"]',   //Logout link from Admin menu
        editOrgTab                                      :'//*[@id="action_buttons"]/div/div/button[2]', //Edit organization button

        pagingNumberOfRecordsDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select', //Paging number of records drop down
        pagingTotalRecords                              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[6]/span/span', //Paging total records
        pagingRecordNumbers                             :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[3]/span', //Paging record numbers
        pagingNextButton                                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[4]/a', //Paging next button
        pagingLastButton                                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Paging last button
        pagingPreviousButton                            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[2]/a', //Paging previous button
        pagingFirstButton                               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[1]/a', //Paging first button
        pagingCurrentPageInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/input', //Paging current page
        pagingGoBtn                                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/button', //Go button in paging division
        objectDetailPageFooter                          :'/html/body/div[1]/main/div[3]/div[1]/div[5]/div/span', //Detail page footer information

        resetPasswordDiv                                :'/html/body/div[1]/div/div[1]', //Reset password pop up when user account is locked on login page
        disabledAccountAlertDiv                         :'/html/body/div[1]/div/div[1]', //Disabled user account alert on login page
        phiDisclaimerPopup                              :'/html/body/div[4]/div[2]/div/div[2]/div', //phi disclaimer pop up
        phiDisclaimerPopupYesOpt                        :'/html/body/div[6]/div[2]/div/div[3]/button[1]', //phi disclaimer pop up yes option
        phiDisclaimerPopupYesOpt                        :'/html/body/div[6]/div[2]/div/div[3]/button[2]', //phi disclaimer pop up no option
    };
module.exports.globalElements = globalElements;
//GLOBAL ELEMENTS - END



//ADMIN HRM ACCESS CONTROL PAGE ELEMENTS - START
var adminHRMAccessControlPageElements =
    {
        controlTreeNameInput    						:'//*[@id="tree_name"]',        //HRM Access Control tree page name input box
        controlTreeDescInput    						:'//*[@id="tree_description"]', //HRM Access Control tree page description input box
        controlTreeSaveBtn							: '//*[@id="btn_save"]', //HRM Access Control tree page save button
        controlTreeSalesTerritoryCheckbox   			:'//*[@id="tree_sales_territory"]',     //HRM Access Control tree page sales territory check box
        controlTreeSaveBtn      						:'//*[@id="btn_save"]', //HRM Access Control tree page save button
        controlTreeCancelLink   						:'//*[@id="tree_detail_form"]/div[2]/div/a[1]', //HRM Access Control tree page cancel link
        controlTreeDeleteLink   						:'//*[@id="tree_detail_form"]/div[2]/div/a[2]', //HRM Access Control tree page delete link
        controlNodeRootNodeNameInput        			:'//*[@id="node_name"]',        //HRM Access Control tree page root node name input
        controlNodeRootNodeDescInput        			:'//*[@id="node_description"]', //HRM Access Control tree page root node description input
        controlNodeAddChildLink 						:'//*[@id="add_child"]',        //HRM Access Control tree page add child link
        controlNodeSaveBtn      						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[2]/div/button', //HRM Access Control tree page control node save button
        controlNodeUsersTab     						:'//*[@id="node_content"]/div[3]/button[1]', //HRM Access Control tree page control node users tab
        controlNodeOrgTab       						:'//*[@id="node_content"]/div[3]/button[2]', //HRM Access Control tree page control node organization tab
        controlNodeSearchUserInput          			:'//*[@id="search_user"]', //HRM Access Control tree page control node search user input
        controlNodeAddUserInput 						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[4]/div[1]/div[2]/ul/li/div/div/div/div[1]/input', //HRM Access Control tree page control node add user input
        controlNodeChildNameInput           			:'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[1]/div/input', //HRM Access Control tree page control node child name input
        controlNodeChildSaveBtn 						:'/html/body/div[4]/div[2]/div/div[5]/button', //HRM Access Control tree page control node child save button
    };
module.exports.adminHRMAccessControlPageElements = adminHRMAccessControlPageElements;
//ADMIN HRM ACCESS CONTROL PAGE ELEMENTS - END



//LOGIN PAGE ELEMENTS - START
var loginPageElements =
    {
        body         	            					:'/html/body', //Login page main body
        usernameInput               					:'//*[@id="username"]', //Login page username input
        passwordInput               					:'//*[@id="password"]', //Login page password input
        submitBtn                   					:'//*[@id="submit"]', //Login page submit button
        container                   					:'//*[@id="container"]' //Login page main container div
    };
module.exports.loginPageElements = loginPageElements;
//LOGIN PAGE ELEMENTS - END



//ORGANIZATION PAGE ELEMENTS - START
var organizationPageElements =
    {
        messageSubTab                                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[1]', //Organization page messages sub tab
        locationAndContactSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[2]', //Organization page location and contacts sub tab
        contactsSubTab                                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[3]', //Organization page contacts sub tab
        feeSchedulesSubTab                              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[4]', //Organization page fee schedules sub tab
        childOrgSubTab                                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[5]', //Organization page child organization sub tab

        duplicateHostCodeError                          :'/html/body/div[1]/div/div[1]/div[2]/div/div[2]/p', //Duplicate host code entry error box
        pagingRowsPerPageDropdown                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select', //Rows per page drop down in organization list page
        organizationTabLink         					:'//*[@id="site_nav"]/ul/li[3]/a', //Organization tab in header menu
        orgPageHeader                                   :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span',//Organization page org name title (firefox xpath)
        orgModalDiv                     				:'/html/body/div[4]/div[2]/div', //Create new organization modal pop up
        orgListTable                    				:'//*[@id="main_content_view"]/div[1]/div[2]/div[2]/div/div/div[2]/table', //List of organizations table
        pageTitle                       				:'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //Organization page org name title (Chrome xpath)
        availableColumnsDiv             				:'//*[@id="edit_available_values"]', //Organization custom list available columns
        selectedColumnsDiv              				:'//*[@id="edit_selected_values"]', //Organization custom list selected columns
        filterAvailableColumnsDiv       				:'//*[@id="available_values"]', //Organization quick filter available columns
        filterSelectedColumnsDiv        				:'//*[@id="selected_values"]', //Organization quick filter selected columns
        filterAddColumnBtn              				:'//*[@id="btn_add"]', //Organization quick filter add columns button
        filterRemoveColumnBtn           				:'//*[@id="btn_remove"]', //Organization quick filter remove columns button
        addColumnBtn                    				:'//*[@id="btn_edit_add"]', //Organization custom list add columns button
        removeColumnBtn                                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in organizations page
        windowCloseIcon                 				:'//*[@id="edit_list_window"]/div[2]/div/div/a/i', //Organization custom list close button
        settingsIcon                    				:'//*[@id="main_content_view"]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization settings icon
        settingsIconActivities          				:'//*[@id="customlist_views"]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization page settings icon
        orgDetailPageSettingsIcon                       :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/i', //Organization detail page settings icon
        hc1TestHostCodeInput                            :'/html/body/div[4]/div[2]/div/div[2]/div/div/div/div/div/div[1]/input', //Host code input field for hc1test interface in organization detail page
        settingsIconCloseBtn                            :'/html/body/div[4]/div/div/div[1]/a/i',//'/html/body/div[4]/div[2]/div/div[1]/a',
        customListDropdown              				:'//*[@id="custom_lists"]', //Organization custom list drop down
        customListDropDownOption1       				:'//*[@id="custom_lists"]/option[1]', //Organization custom list drop down option 1
        newCustomListLink               				:'//*[@id="new_list_btn"]', //Organization new custom list link
        orderCustomListLink             				:'//*[@id="order_list_btn"]', //Organization order custom list link
        quickFilterTrueValueOpt         				:'//*[@id="bool_val_true"]', //Organization quick filter true option in value
        quickFilterFalseValueOpt        				:'//*[@id="bool_val_false"]', //Organization quick filter false option in value
        quickFilterByDropdown           				:'//*[@id="filter_options"]', //Organization quick filter by drop down
        quickFilterOperatorDropdown     				:'//*[@id="filter_conditions"]', //Organization quick filter operator drop down
        quickFilterApplyBtn             				:'//*[@id="apply_quick_list"]', //Organization quick filter apply button
        quickFilterSaveBtn              				:'//*[@id="apply_quick_list"]', //Organization quick filter save button
        quickFilterResetBtn             				:'//*[@id="reset_quick_list"]', //Organization quick filter reset button
        quickFilterCloseBtn             				:'//*[@id="main_content_view"]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Organization quick filter close button
        quickFilterLoadingCircle        				:'//*[@id="main_content_view"]/div[1]/div[2]/div[2]/div/div/div[1]', //Organization quick filter loading circle
        orderedCustomListDisplay        				:'//*[@id="current_list_values"]', //Organization custom list order list records div
        newCustomListInput              				:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization new custom list name input
        cloneCustomListInput            				:'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization clone custom list name input
        cloneCustomListInputActivities					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization new custom list name input Activities page

        cloneCustomListLink         					:'//*[@id="clone_list_btn"]', //Organization custom list clone link
        cloneCustomListSaveBtn      					:'//*[@id="submit_email"]', //Organization clone custom list save button
        createNewCustomListSaveBtn  					:'//*[@id="btn_save_list"]', //Organization new custom list save button
        orderListCloseIcon          					:'//*[@id="order_lists_window"]/div[2]/div/div/a/i', //Organization order custom list close icon
        editCustomListLink          					:'//*[@id="edit_list_btn"]', //Organization custom list edit link
        deleteCustomListLink        					:'//*[@id="disable_list_btn"]', //Organization custom list delete link
        customListPagingDiv         					:'//*[@id="main_content_view"]/div[1]/div[2]/div[2]/div/div/div[3]', //Organization custom list paging div
        activitiesListPagingDiv     					:'//*[@id="customlist_views"]/div[1]/div[2]/div/div/div[3]/div/div', //Activities list paging div
        displayColumnContentDiv     					:'//*[@id="main_content_view"]/div[1]/div[2]/div[1]/div/div[2]/div', //Organization display column div
        quickFilterLink             					:'//*[@id="main_content_view"]/div[1]/div[2]/div[1]/div/div[1]/div[2]/a', //Organization quick filter link
        quickFilterLinkActivities   					:'//*[@id="customlist_views"]/div[1]/div[1]/div/div[1]/div[2]/a', //Activities quick filter link
        organizationCreateLink      					:'//*[@id="organization_create"]', //Organization create link
        errorDiv                    					:'//*[@id="error_header"]', //Error div
        hostCodesDisplayHeader      					:'/html/body/div[4]/div/div/div[2]/div/div/h3[1]', //Organization host codes displya div
        orgNameInput                					:'//*[@id="org_name"]', //Organization creation name input
        orgSettingsEditBtn          					:'//button[@class="btn btn-warning btn-edit"]', //Organization settings popup  edit button
        orgCreateBtn                					:'/html/body/div[4]/div[2]/div/div[5]/button', //Organization create button
        orgAccessControlNodeTable   					:'//*[@id="access_control_node_table"]', //Organization settings popup access control node table
        orgRecordHeadingSettingsIcon					:'//*[@id="org_record_heading"]/i', //Organization settings popup icon
        orgRecordPageHeading        					:'//*[@id="record_heading"]', //Organization record page v-card heading
        orgHostCodeInput            					:'//*[@id="filter_input"]/input', //Organization settings popup host code input
        uacNodeSearchInput          					:'/html/body/div[4]/div/div/div[2]/div/div/div/div/div/div[1]/input', //Organization settings popup node search input
        testAccessControlNodeSuggestionBox  			:'//*[@id="mCSB_10"]/div[1]/li[1]/div[1]/div', //Organization settings popup access control node suggestion box

        accessioningLocationRadio1  					:'//*[@id="collection_location_1"][@name="accessioning_location"]', //Organization settings popup accessioning location option 1
        accessioningLocationRadio2  					:'//*[@id="collection_location_2"][@name="accessioning_location"]',//Organization settings popup accessioning location option 2
        collectionLocationRadio1    					:'//*[@id="collection_location_1"][@name="collection_location"]', //Organization settings popup collection location option 1
        collectionLocationRadio2    					:'//*[@id="collection_location_2"][@name="collection_location"]', //Organization settings popup collection location option 2
        orderingLocationRadio1      					:'//*[@id="collection_location_1"][@name="ordering_location"]', //Organization settings popup ordering location option 1
        orderingLocationRadio2      					:'//*[@id="collection_location_2"][@name="ordering_location"]', //Organization settings popup ordering location option 2
        orgGeneralTab               					:'//*[@id="generalTab"]', //Organization general sub tab (chrome xpath)
        orgLocationAndContactTab    					:'//*[@id="locationTab"]', //Organization location and contact sub tab (chrome xpath)
        orgContactsTab              					:'//*[@id="contacts"]',//Organization contacts sub tab (chrome xpath)
        orgFeeSchedulesTab          					:'//*[@id="feeSchedule"]', //Organization fee schedule sub tab (chrome xpath)
        orgChildOrganizationsTab    					:'//*[@id="relatedOrganizations"]', //Organization child org sub tab (chrome xpath)
        vCardDiv                    					:'//*[@id="vcard"]', //Organization V-card div
        vCardPhone                  					:'//*[@id="phone"]', //Organization V-card phone div
        vCardMailingAddress         					:'//*[@id="mailing"]', //Organization V-card mailing address div
        vCardFax                    					:'//*[@id="fax"]', //Organization V-card fax div
        orgAddressStreet1Input      					:'//*[@id="mailing_street"]', //Organization address street 1 input box
        orgAddressStreet2Input      					:'//*[@id="mailing_street2"]', //Organization address street 2 input box
        orgAddressCityInput         					:'//*[@id="mailing_city"]', //Organization address city input box
        orgAddressStateInput        					:'//*[@id="mailing_state"]', //Organization address state input box
        orgAddressPostalInput       					:'//*[@id="mailing_postal"]', //Organization address postal input box
        orgAddressCountryInput      					:'//*[@id="mailing_country"]', //Organization address country input box
        orgOfficeInfoPhone          					:'//*[@id="phone"]', //Organization V-card office information phone info
        orgOfficeInfoFax            					:'//*[@id="fax"]', //Organization V-card office information fax info
        settingsPopupCloseBtn       					:'/html/body/div[4]/div/div/div[1]/a', //Organization settings popup close button
        orgDetailPageEditBtn        					:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Organization detail page edit button
        salesRepAddButton           					:'//*[@id="subtabviews"]/div/div/div[1]/div/div/ul/div[5]/div/li[1]/div/div/span/i', //Organization add sales rep button
        salesRepAutoPopUpDiv        					:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/div/div[2]/div/form/div[1]/ul[1]/li/div/div/div/div[3]/ul/div/div[1]/li', //Organization sales rep auto pop up div
        addSalesRepInput            					:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/div/div[2]/div/form/div[1]/ul[1]/li/div/div/div/div[1]/input', //Organization add sales rep input
        salesRepOkButton            					:'//*[@id="sales_territory_form"]/div[2]/button[2]', //Organization sales rep OK button
        salesTerritoryListDiv       					:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/div/div[2]/div/form/div[1]/ul[2]/li/div', //Organization sales territory list div
        salesTerritorySavedListDiv  					:'//*[@id="subtabviews"]/div/div/div[1]/div/div/ul/div[5]/div/li[2]/div',//Organization sales territory saved list div
        orgSaveBtn                  					:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Organization save button
        caseCreateLink              					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //Organization create case link
        taskCreateLink              					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a', //Organization create task link
        memoCreateLink              					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[4]/a', //Organization create memo link
        opportunityCreateLink       					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[5]/a', //Organization create opportunity link
        activitiesListTable         					:'//*[@id="customlist_views"]/div[1]/div[2]/div/div/div[2]/table', //Activities records list table
        salesRepPopUpCloseBtn       					:'//*[@id="sales_rep_sales_territory_modal_dialog"]/div[2]/div/div/a', //Organization add sales rep close button
        defaultTestOrg              					:'//a[@title="OrgForUITest"]', //Sample default pre created organization
        pagingRecordsPerPageMaxOption					:'//*[@id="rows_per_page"]/option[4]', //Pagination max option
        orgDetailPageEditBtn                            :'//*[@id="action_buttons"]/div/div/button[2]', //Organization detail page edit button
        orgDetailPageTypeDropdown                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/select', //Organization detail page type drop down
        orgDetailPageSaveAndBackBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Organization detail page save and back button
        orgDetailPageSpecialtiesMainDiv                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div', //Organization detail page specialties div
        orgDetailPageSpecialtiesInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div/span[1]/input[1]', //Organization detail page specialties input
        orgDetailPageSpecialtyDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div', //Specialty input div
        orgDetailPageNewCaseLink                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //Organization detail page new case link
        orgDetailPageNewTaskLink                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a', //Organization detail page new task link
        orgDetailPageNewMemoLink                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[4]/a', //Organization detail page new memo link
        orgDetailPageNewOpportunityLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[5]/a', //Organization detail page new opportunity link
        orgDetailPageNumberInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input', //Organization detail page number input
        orgDetailPageRelationshipMnrInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/div/div[1]/input', //Organization detail page relationship manager input

        activitiesSubTabBtn         					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]', //Organization detail page activities sub tab
        ordersSubTabBtn             					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]', //Organization detail page orders sub tab
        attachmentsSubTabBtn        					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]', //Organization detail page attachments sub tab
        messagesSubTabBtn           					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[4]', //Organization detail page messages sub tab
        ordersPageDateRangeDropDown 					:'//*[@id="listType"]', //Organization detail page orders sub tab date range drop down
        ordersPageStartDateDiv      					:'//*[@id="customlist_views"]/div[2]/div[1]/div[1]/div[2]/div/div', //Organization detail page orders sub tab start date div
        ordersPageDatePicker        					:'/html/body/div[4]', //Organization detail page orders sub tab date picker
        ordersPageRangeSlider       					:'//*[@id="rangeslide"]', //Organization detail page orders sub tab range slider
        ordersPageRangeValueDiv     					:'//*[@id="rangevalue"]', //Organization detail page orders sub tab range value div
        ordersPageFilterRangeDiv    					:'//*[@id="customlist_views"]/div[2]/div[1]/div[1]/div[4]/small', //Organization detail page orders sub tab filters range div
        ordersPageSettingsIcon      					:'//*[@id="customlist_views"]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization detail page orders sub tab  settings icon
        ordersPageRecordsListTable  					:'//*[@id="customlist_views"]/div[2]/div[2]/div[2]/div/div/div[2]/table', //Organization detail page orders sub tab records table
        ordersPageQuickFilterLink   					:'//*[@id="customlist_views"]/div[2]/div[2]/div[1]/div/div[1]/div[2]/a', //Organization detail page orders sub tab quick filter link

        ordersPageFilterAvailColumns        			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Organization detail page orders sub tab filter available columns
        ordersPageFilterSelectedColumns     			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Organization detail page orders sub tab filter selected columns
        ordersPageCustomListAvailColumns    			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Organization detail page orders sub tab custom list available columns
        ordersPageCustomListSelectedColumns 			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Organization detail page orders sub tab custom list selected columns
        ordersPageEditCustomListLink        			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]',//Organization detail page orders sub tab edit custom list link
        ordersPageCloneCustomListLink       			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Organization detail page orders sub tab  clone custom list link
        ordersPageNewCustomListLink 					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Organization detail page orders sub tab  new custom list link
        ordersPageFilterAddBtn      					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Organization detail page orders sub tab filter add button
        ordersPageFilterApplyBtn    					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]',//Organization detail page orders sub tab  filter apply button
        ordersPageCloneListInput    					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization detail page orders sub tab clone custom list input
        ordersPageEditCustomListInput       			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization detail page orders sub tab edit custom list input
        ordersPagePagingDiv         					:'//*[@id="customlist_views"]/div[2]/div[2]/div[2]/div/div/div[3]/div/div', //Organization detail page orders sub tab paging div
        ordersPageOrderCustomListLink       			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Organization detail page orders sub tab order custom list link
        ordersPageCloneCustomListCreateBtn  			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Organization detail page orders sub tab  clone custom list create button
        ordersPageDeleteLink        					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Organization detail page orders sub tab custom list delete link
        ordersPageCustomListDropDown					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[1]/select', //Organization detail page orders sub tab custom list drop down
        ordersPageCustomListAddBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in organization detail page
        ordersPageCustomListRemoveBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in organization detail page

        attachmentsPageLink         					:'//*[@id="customlist_tabs"]/div/ul/li[3]', //Organization detail page attachments sub tab
        attachmentsPageNewUploadLink					:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[3]/ul[2]/li[2]/a', //Organization detail page attachments sub tab ne wupload link
        attachmentsPageNewUploadPopUp       			:'//*[@id="modalwrapper"]/div/div[2]/div', //Organization detail page attachments sub tab new upload pop up
        attachmentsUploadNameInput  					:'//*[@id="attachment_name"]', //Organization detail page attachments sub tab new upload name input
        attachmentsUploadDescInput  					:'//*[@id="attachment_description"]', //Organization detail page attachments sub tab new upload description input
        attachmentsUploadFileInput  					:'//*[@id="attachment_file"]', //Organization detail page attachments sub tab new upload file input
        attachmentsUploadPHICheckBox					:'//*[@id="attachment_phi"]', //Organization detail page attachments sub tab new upload PHI check box
        attachmentsUploadBtn        					:'//*[@id="create_attachment_form"]/div/div[2]/button', //Organization detail page attachments sub tab new upload button
        attachmentsUploadPopUpCloseBtn      			:'//*[@id="modalwrapper"]/div/div[2]/div/div/a', //Organization detail page attachments sub tab new upload close button
        attachmentsNameInputErrorLabel      			:'//*[@id="create_attachment_form"]/div/ul/li[1]/div/label', //Organization detail page attachments sub tab new upload name inout error label
        attachmentsFileInputErrorLabel      			:'//*[@id="create_attachment_form"]/div/ul/li[3]/div/div/label', //Organization detail page attachments sub tab new upload file input error label

        attachmentsTable                                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table', //attachments list table
        attachmentsPageRecordsListTable     			:'//*[@id="customlist_views"]/div[3]/div[2]/div/div/div[2]/table', //Organization detail page attachments sub tab records table
        attachmentsPageSettingsIcon         			:'//*[@id="customlist_views"]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization detail page attachments sub tab settings icon
        attachmentsPageQuickFilterLink      			:'//*[@id="customlist_views"]/div[3]/div[1]/div/div[1]/div[2]/a', //Organization detail page attachments sub tab quick filter link
        attachmentsPageFilterAddBtn         			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Organization detail page attachments sub tab filter add button
        attachmentsPageFilterRemoveBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Organization detail page attachments sub tab filter remove column button
        attachmentsPageFilterAvailColumns   			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Organization detail page attachments sub tab filter available columns
        attachmentsPageFilterSelectedColumns			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Organization detail page attachments sub tab filter selected columns
        attachmentsPageFilterApplyBtn       			:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Organization detail page attachments sub tab filter apply button
        attachmentsPageCustomListEditLink       		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Organization detail page attachments sub tab edit custom list link
        attachmentsPageCustomListCloneLink      		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Organization detail page attachments sub tab clone custom list link
        attachmentsPageCustomListNewListLink    		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Organization detail page attachments sub tab new custom list link
        attachmentsPageCustomListOrderListLink  		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Organization detail page attachments sub tab order custom list link
        attachmentsPageCustomListNewListInput   		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization detail page attachments sub tab new custom list input
        attachmentsPageCustomListAvailColumns   		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Organization detail page attachments sub tab custom list available columns
        attachmentsPageCustomListSelectedColumns		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Organization detail page attachments sub tab custom list selected columns
        attachmentsPageNewCustomListSaveBtn     		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Organization detail page attachments sub tab new custom list save button
        attachmentsPageCloneListInput           		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization detail page attachments sub tab clone custom list input
        attachmentsPageCloneListCreateBtn       		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Organization detail page attachments sub tab clone custom list create button
        attachmentsPageDeleteListLink           		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Organization detail page attachments sub tab delete custom list link
        attachmentsPageCloneListCloseBtn        		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/div/a',//Organization detail page attachments sub tab clone custom list close button
        attachmentsPageCustomListAddColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in attachments subtab organization detail page
        attachmentsPageCustomListRemoveColBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in attachments subtab organization detail page
        attachmentsPagePagingDiv                		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[3]/div/div', //Organization detail page attachments sub tab paging div

        orgEditPageNameInputField                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/input', //Organization edit page name input
        orgEditPageSaveButton                           :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Organization edit page save button
        orgList                                         :'//*[@id="main_content_view"]/div[1]/div[2]/div[2]/div/div/div[2]/table/tbody', //Organizations list

        messagesPageLink                                :'//*[@id="customlist_tabs"]/div/ul/li[4]/button', //Organization detail page messages sub tab
        messagePageCustomListDropDown                   :'//*[@id="custom_lists"]', //Organization detail page messages sub tab custom list drop down
        messagePageSettingsIcon                         :'//*[@id="customlist_views"]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization detail page messages sub tab settings icon
        messagePageCustomListEditLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Organization detail page messages sub tab edit custom list link
        messagePageCustomListCloneLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Organization detail page messages sub tab clone custom list link
        messagePageCustomListNewLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Organization detail page messages sub tab new custom list link
        messagePageCustomListOrderLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Organization detail page messages sub tab order custom list link
        messagePageQuickFilterLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[2]/a', //Organization detail page messages sub tab quick filter link
        messagePageFilterAvailableColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Organization detail page messages sub tab filter available columns
        messagePageFilterSelectedColumns                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Organization detail page messages sub tab filter selected columns
        messagePageFilterAddBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Organization detail page messages sub tab filter add button
        messagePageFilterRemoveBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Organization detail page messages sub tab filter remove column button
        messagePageFilterApplyBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Organization detail page messages sub tab filter apply button
        messagePageFilterSaveBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Organization detail page messages sub tab filter save button
        messagePageCloneListInput                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization detail page messages sub tab clone custom list input
        messagePageCloneListCreateBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Organization detail page messages sub tab clone custom list create button
        messagePageNewListInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization detail page messages sub tab new custom list input
        messagePageCustomListAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Organization detail page messages sub tab custom list available columns
        messagePageCustomListSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Organization detail page messages sub tab custom list selected columns
        messagePageNewCustomListSaveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Organization detail page messages sub tab custom list save button
        messagePageCustomListAddBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Organization detail page messages sub tab custom list add button
        messagePageCustomListRemoveBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Organization detail page messages sub tab custom list remove button
        messagePageNewCustomListCloseBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/div/a', //Organization detail page messages sub tab new custom list close button
        messagePageCustomListDeleteBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Organization detail page messages sub tab custom list delete button
        messagePageRecordsListTable                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[2]/table', //Organization detail page messages sub tab records table
        messagePagePaginationDiv                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[3]/div/div', //Organization detail page messages sub tab pagination div
        messageBoxCloseBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[1]/a', //Organization detail page messages sub tab message box close button
        messagePageOrderList                            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Organization detail page messages sub tab page order list
        messagePageOrderListCloseBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/div/a', //Organization detail page messages sub tab order custom list close button
        messageCustomListErrorDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/label', //Organization detail page messages sub tab custom list error div

        locationPageEditBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Organization detail page location sub tab edit button
        locationPageSaveBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Organization detail page location sub tab save button
        locationPageSaveAndBackBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Organization detail page location sub tab save and back button
        locationPagePhoneInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[1]/div/li/div/div/input', //Organization detail page location sub tab phone input
        locationPageFaxInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[2]/div/li/div/div/input', //Organization detail page location sub tab fax input
        locationPageWebsiteInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[3]/div/li/div/div/input', //Organization detail page location sub tab website input
        locationPageDescriptionInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[4]/div/li/div/div/textarea', //Organization detail page location sub tab description input
        locationPageMailingStreetInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[1]/div/input', //Organization detail page location sub tab mailing street 1 input
        locationPageMailingStreet2Input                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[2]/div/input', //Organization detail page location sub tab mailing street2 input
        locationPageMailingCityInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[3]/div/input', //Organization detail page location sub tab mailing city input
        locationPageMailingStateInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[4]/div/input', //Organization detail page location sub tab mailing state input
        locationPageMailingPostalInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[5]/div/input', //Organization detail page location sub tab mailing postal input
        locationPageMailingCountryInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[6]/div/input', //Organization detail page location sub tab mailing phone input
        locationPageSameAsBillingCheckbox               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/h3/span/small/span/input', //Organization detail page location sub tab same as billing checkbox
        locationPageBillingStreetInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[1]/div/input', //Organization detail page location sub tab billing street1 input
        locationPageBillingStreet2Input                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[2]/div/input', //Organization detail page location sub tab billing street2 input
        locationPageBillingCityInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[3]/div/input', //Organization detail page location sub tab billing city input
        locationPageBillingStateInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[4]/div/input', //Organization detail page location sub tab billing state input
        locationPageBillingPostalInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[5]/div/input', //Organization detail page location sub tab billing postal input
        locationPageBillingCountryInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[6]/div/input', //Organization detail page location sub tab billing country input
        locationPageHOOSundayDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[1]/td[2]/div/div/select', //Organization detail page location sub tab sunday hours of operation drop down
        locationPageHOOMondayDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[2]/td[2]/div/div/select', //Organization detail page location sub tab monday hours of operation drop down
        locationPageHOOTuesdayDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[3]/td[2]/div/div/select', //Organization detail page location sub tab tuesday hours of operation drop down
        locationPageHOOWednesdayDropdown                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[4]/td[2]/div/div/select', //Organization detail page location sub tab wednesday hours of operation drop down
        locationPageHOOThursdayDropdown                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[5]/td[2]/div/div/select', //Organization detail page location sub tab thursday hours of operation drop down
        locationPageHOOFridayDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[6]/td[2]/div/div/select', //Organization detail page location sub tab friday hours of operation drop down
        locationPageHOOSaturdayDropdown                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div/table/tbody/tr[7]/td[2]/div/div/select', //Organization detail page location sub tab saturday hours of operation drop down
        locationPageCompetitiveLabsInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[3]/div/div/div/ul/div/div/li/div/div/input', //Organization detail page location sub tab competitive labs input
        locationPagePhoneDiv                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[1]/div/li/div/div/div', //Organization detail page location sub tab phone div
        locationPageFaxDiv                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[2]/div/li/div/div/div', //Organization detail page location sub tab fax div
        locationPageWebsiteDiv                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/div/div/ul/div[3]/div/li/div/div/div', //Organization detail page location sub tab website div
        locationPageMailingStreetDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[1]/div/div', //Organization detail page location sub tab mailing street1 div
        locationPageMailingStreet2Div                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[2]/div/div', //Organization detail page location sub tab mailing street2 div
        locationPageMailingCityDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[3]/div/div', //Organization detail page location sub tab mailing city div
        locationPageMailingStateDiv                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[4]/div/div', //Organization detail page location sub tab mailing state div
        locationPageMailingPostalDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[5]/div/div', //Organization detail page location sub tab mailing postal div
        locationPageMailingCountryDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/div/ul/div/div/ul/li[6]/div/div', //Organization detail page location sub tab mailing country div
        locationPageBillingStreetDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[1]/div/div', //Organization detail page location sub tab billing street1 div
        locationPageBillingStreet2Div                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[2]/div/div', //Organization detail page location sub tab billing street2 div
        locationPageBillingCityDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[3]/div/div', //Organization detail page location sub tab billing city div
        locationPageBillingStateDiv                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[4]/div/div', //Organization detail page location sub tab billing state div
        locationPageBillingPostalDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[5]/div/div', //Organization detail page location sub tab billing postal div
        locationPageBillingCountryDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/ul/div/div/ul/li[6]/div/div', //Organization detail page location sub tab billing country div

        contactsPageRecordsList             			:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Organization detail page contacts sub tab records table
        contactsPageNewContactLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[4]/a', //Organization detail page contacts sub tab new contact link
        contactsPageFirstNameInput                      :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[5]/div/input', //Organization detail page contacts sub tab contact first name input
        contactsPageLastNameInput                       :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[7]/div/input', //Organization detail page contacts sub tab contact last name input
        contactsPageCreateBtn                           :'/html/body/div[4]/div[2]/div/div[5]/button[1]', //Organization detail page contacts sub tab contact create button
        contactsPageQuickCreateBtn                      :'/html/body/div[4]/div[2]/div/div[5]/button[2]', //Organization detail page contacts sub tab contact quick create button
        contactsPageSearchInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[1]/input', //Organization detail page contacts sub tab search input
        contactsPageCustomListDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Organization detail page contacts sub tab custom list drop down
        contactsPageSettingsIcon                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization detail page contacts sub tab settings icon
        contactsPageCustomListEditLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Organization detail page contacts sub tab edit custom list link
        contactsPageCustomListCloneLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Organization detail page contacts sub tab clone custom list link
        contactsPageCustomListNewListLink               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Organization detail page contacts sub tab new custom list link
        contactsPageCustomListOrderListLink             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Organization detail page contacts sub tab order custom list link
        contactsPageCustomListCloneInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization detail page contacts sub tab clone custom list input
        contactsPageCustomListCloneCreateBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Organization detail page contacts sub tab clone custom list create button
        contactsPageCustomListCloneCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Organization detail page contacts sub tab clone custom list close button
        contactsPageCustomListNewListInput              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization detail page contacts sub tab new custom list input
        contactsPageCustomListAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Organization detail page contacts sub tab custom list available columns
        contactsPageCustomListSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Organization detail page contacts sub tab custom list selected columns
        contactsPageCustomListNewListSaveBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Organization detail page contacts sub tab new custom list save button
        contactsPageCustomListNewListCancelBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //Organization detail page contacts sub tab new custom list cancel button
        contactsPageCustomListNewListDeleteBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Organization detail page contacts sub tab new custom list delete button
        contactsPageCustomListAddBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Organization detail page contacts sub tab custom list add button
        contactsPageCustomListRemoveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',  //Organization detail page contacts sub tab custom list remove button
        contactsPageCustomListOrderListDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Organization detail page contacts sub tab order custom list div
        contactsPageCustomListOrderListSaveBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', //Organization detail page contacts sub tab order custom list save button
        contactsPageCustomListOrderListCancelBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]', //Organization detail page contacts sub tab order custom list cancel button
        contactsPageRecordsListTable                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Organization detail page contacts sub tab records table
        contactsPagePaginationDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div', //Organization detail page contacts sub tab pagination div
        contactsPageQuickFilterLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Organization detail page contacts sub tab quick filter link
        contactsPageFilterAvailColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Organization detail page contacts sub tab filter available columns
        contactsPageFilterSelectedColumns				:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Organization detail page contacts sub tab filter selected columns
        contactsPageFilterAddBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Organization detail page contacts sub tab filter add button
        contactsPageFilterRemoveBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Organization detail page contacts sub tab filter remove button
        contactsPageFilterApplyBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Organization detail page contacts sub tab filter apply button
        contactsPageFilterSaveBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Organization detail page contacts sub tab filter save button
        contactsPageFilterResetBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Organization detail page contacts sub tab filter reset button
        contactsPageFilterCloseBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Organization detail page contacts sub tab filter close button
        contactsPageSearchFirstResult                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[3]/ul/div/div[1]/li', //Organization detail page contacts sub tab search first result
        contactsPagePrimaryContactCheckbox              :'/html/body/div[4]/div[2]/div/div[2]/div/div/ul[2]/li/div/div/input', //Organization detail page contacts sub tab primary contact check box
        contactsPageAssociateContactBtn                 :'/html/body/div[4]/div[2]/div/div[5]/button', //Organization detail page contacts sub tab associate contact button
        contactsPageContactTypeDropDown                 :'/html/body/div[4]/div[2]/div/div[2]/div/div/ul[1]/li/div/div/select', //Organization detail page contacts sub tab contact type drop down
        contactsPageContactIcon                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[2]/span/a/i', //Organization detail page contacts sub tab contact icon

        childOrgPageParentOrgSearchInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[1]/ul/li/div/div[1]/div/div[1]/input', //Organization detail page child org sub tab parent org search input
        childOrgPageChildOrgSearchInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div[2]/div/ul/li[1]/div/div/div/div[1]/input', //Organization detail page child org sub tab child org search input
        childOrgPageCustomListDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Organization detail page child org sub tab custom list drop down
        childOrgPageSettingsIcon                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Organization detail page child org sub tab settings icon
        childOrgPageCustomListEditLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Organization detail page child org sub tab edit custom list link
        childOrgPageCustomListCloneLink 				:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Organization detail page child org sub tab clone custom list link
        childOrgPageCustomListNewLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Organization detail page child org sub tab new custom list link
        childOrgPageCustomListOrderLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Organization detail page child org sub tab order custom list link
        childOrgPageCustomListCloneInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Organization detail page child org sub tab clone custom list input
        childOrgPageCustomListCloneCreateBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Organization detail page child org sub tab clone custom list create button
        childOrgPageCustomListCloneCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Organization detail page child org sub tab clone custom list close button
        childOrgPageCustomListNewInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Organization detail page child org sub tab new custom list input
        childOrgPageCustomListNewSaveBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Organization detail page child org sub tab new custom list save button
        childOrgPageCustomListNewCancelBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //Organization detail page child org sub tab nw custom list remove button
        childOrgPageCustomListDeleteBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Organization detail page child org sub tab custom list delete button
        childOrgPageCustomListAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Organization detail page child org sub tab custom list available columns
        childOrgPageCustomListSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Organization detail page child org sub tab custom list selected columns
        childOrgPageCustomListAddBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Organization detail page child org sub tab custom list add button
        childOrgPageCustomListRemoveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Organization detail page child org sub tab custom list remove button
        childOrgPageCustomListOrderListDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div', //Organization detail page child org sub tab order custom list div
        childOrgPageCustomListOrderListSaveBtn			:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', //Organization detail page child org sub tab custom order list save button
        childOrgPageCustomListOrderListCancelBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]', //Organization detail page child org sub tab custom order list cancel button
        childOrgPageCustomListOrderListCloseBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Organization detail page child org sub tab custom order list close button
        childOrgPageQuickFilterLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Organization detail page child org sub tab quick filter link
        childOrgPageFilterAvailColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Organization detail page child org sub tab filter available columns
        childOrgPageFilterSelectedColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Organization detail page child org sub tab filter selected columns
        childOrgPageFilterAddBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Organization detail page child org sub tab filter add button
        childOrgPageFilterRemoveBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Organization detail page child org sub tab filter remove button
        childOrgPageFilterApplyBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Organization detail page child org sub tab filter apply button
        childOrgPageFilterSaveBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Organization detail page child org sub tab filter save button
        childOrgPageFilterResetBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Organization detail page child org sub tab filter reset button
        childOrgPageFilterCloseBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Organization detail page child org sub tab filter close button
        childOrgPageRecordsTable                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Organization detail page child org sub tab records table
        childOrgPagePaginationDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div', //Organization detail page child org sub tab pagination div

        hoursOfOperationDiv								:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div/ul/div/div', //Organization detail page fee schedule sub tab hours of operation div
        deletePanel					                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[1]/h2', //Organization detail page fee schedule sub tab delete panel
        searchFieldFeeSchedule                          : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div/div/div/div[1]/input', //Organization detail page fee schedule sub tab fee schedule search field 1
        searchFieldFeeSchedule2				            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[2]/div/div[1]/div/div/div[1]/input', //Organization detail page fee schedule sub tab fee schedule search field 2
        hostCodFeeScheduleMenu                          : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[3]/table/tbody/tr/th[1]', //Organization detail page fee schedule sub tab host code menu
        nameFeeScheduleMenu                             : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[3]/table/tbody/tr/th[2]', //Organization detail page fee schedule sub tab name menu
        abbreviationFeeScheduleMenu                     : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[3]/table/tbody/tr/th[3]', //Organization detail page fee schedule sub tab abbreviation menu
        listPriceFeeScheduleMenu                        : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[3]/table/tbody/tr/th[4]', //Organization detail page fee schedule sub tab list price menu
        feeScheduleMenu                                 : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[3]/table/tbody/tr/th[5]', //Organization detail page fee schedule sub tab fee schedule menu
        panelSearchBox                                  : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[2]/div/div[2]/span', //Organization detail page fee schedule sub tab panel search box
        panelDeleteIcon                                 : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[1]/h2/span/i', //Organization detail page fee schedule sub tab panel delete icon
        orgFeeSchedulesDiv                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[1]', //Added fee schedules div
        orgFeeScheduleSearchResults                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div/div/div/div[3]/ul/div/div[1]/li', //Fee schedules search results div
    };
module.exports.organizationPageElements = organizationPageElements;
//ORGANIZATION PAGE ELEMENTS - END



//ACTIVITY PAGE ELEMENTS - START
var activityPageElements =
    {
        activityPageHeader                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div[1]/h1',  //To check activity name on activity tab
        activitiesTabLink                               :'//*[@id="site_nav"]/ul/li[6]/a/span', //Activity tab  on home page
        activityPageHeader                              :'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //To check activity name on activity tab
        activitiesTabLink                               :'/html/body/div[1]/header/div[2]/div/nav/ul/li[6]/a', //Activity tab  on home page
        createCaseLink                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div[2]/div/ul/li[2]/a', //Create Case link on Activity page
        activitiesTable                                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/table', //Record table of activity on activity page
        activityEditBtn                                 :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //edit button on activity page
        activityCustomListCasesCreatedByMeOpt           :'//option[contains(.,"Cases Created By Me")]', //Cases created by me option in activities page custom lists
        activityCustomListTasksCreatedByMeOpt           :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select/option[5]', //Tasks created by me option in activities page custom lists
        activityCustomListOutstandingOpt                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select/option[1]', //Outstanding option in activities custom list drop down
        errorDiv                    					:'//*[@id="error_header"]', //Error div
        activityCustomListDropdown                      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select',  //Activity page custom list drop down
        activitySettingsIcon                            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Activity page settings icon
        activityCustomListAddColBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in activity page custom list
        activityCustomListRemoveColBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in activity page custom list
        activityCustomListAvailableColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Activity page available columns in custom list
        activityCustomListSelectedColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Activity page selected columns in custom list
        activityCustomListEditLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Activity page edit link in custom list
        activityCustomListCloneLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Activity page clone link in custom list
        activityCustomListNewLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Activity page new list link in custom list
        activityCustomListOrderLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Activity page order list link in custom list
        activityCustomListNewNameInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //Activity page name input box in new custom list
        activityCustomListNewSaveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Activity page new custom list save button
        activityCustomListNewCancelBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //Activity page new custom list cancel button
        activityCustomListNewCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/div/a', //Activity page new custom list close button
        activityCustomListCloneInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Activity page new clone custom list name input
        activityCustomListCloneCreateBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Activity page new clone csutom list create button
        activityCustomListOrderListDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Activity page order list div
        activityCustomListOrderSaveBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', //Activity page custom list order save button
        activityCustomListOrderCancelBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]',//Activity page custom list order cancel button
        activityCustomListOrderCloseBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Activity page custom list order close button
        activityCustomListDeleteBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Activity page custom list delete button
        activityQuickFilterLink                         :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[2]/a', //Activity page quick filter link
        activityFilterByDropdown                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Activity page filter by drop down
        activityOperatorDropdown                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Activity page filter operator drop down
        activityFilterAvailableColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Activity page filter available columns
        activityFilterSelectedColumns                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Activity page filter selected columns
        activityFilterAddBtn                            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Activity page filter add button
        activityFilterRemoveBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Activity page filter remove button
        activityFilterApplyBtn                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Activity page filter apply button
        activityFilterSaveBtn                           :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Activity page filter save button
        activityFilterResetBtn                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Activity page filter reset button
        activityFilterCloseBtn                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Activity page filter close button
        activityPaginationDiv                           :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div', //Activity page pagination div
        pagingNumberOfRecordsDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select', //Activity page paging number of records drop down
        pagingTotalRecords                              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[6]/span/span', //Activity page paging total records
        pagingRecordNumbers                             :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[3]/span', //Activity page paging record numbers
        pagingNextButton                                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[4]/a', //Activity page paging next button
        pagingLastButton                                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Activity page paging last button
        pagingPreviousButton                            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[2]/a', //Activity page paging previous button
        pagingFirstButton                               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[1]/a', //Activity page paging first button
        pagingCurrentPageInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/input', //Activity page paging current page


        caseActivitiesTable                             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Activities table in case detail page
        caseOrganizationInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/div[1]/input', //Activity page organization input box for case
        caseSubjectInputField   						:'//html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',  //Activity page subject input box for case
        caseSaveButton          						:'//html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]', //save button on Case edited page
        caseViewSubjectFiled    						:'//html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/div', //case view subject
        caseSubjectInput                    			:'//input[@id="subject"]', //Case subject input on case activity page
        caseSaveBtn                         			:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]', //Save button on Case edited page
        caseSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and back button on Case edited page
        caseCancelBtn                                   :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[3]/a[1]', //cancel link on Case edited page
        caseDeleteBtn                                   :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[3]/a[2]', //Delete icon on activiy record page
        caseOrganizationLabel                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li', //Case organization input fied on case activity page
        caseDateLabel                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li', //Case date label fied on case activity page
        caseDateInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div/div/div[1]/div/input',    //Case date input fied on case activity page
        caseTimeInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div/div/div[2]/div/input',    //Case time input fied on case activity page
        caseStatusDropdown                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/select',  //Case status drop down input field on case activity page
        caseDescriptionInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[10]/div/li/div/div/textarea',       //Case description input fied on case activity page
        caseCategoryDropdown                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/select',  //Case category input field on case activity page
        caseSubcategoryDropdown                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select',  //Case sub category input field on case activity page
        caseAssignedToInput                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div/div/div[1]/input',        //Case assigned input field on case activity page
        casePriorityDropdown                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[8]/div/li/div/div/select',  //Case  drop down input field on case activity page
        caseCCUserInput                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[11]/div/li/div/div[1]/div/div/div[1]/input',        //Case  cc user input field on case activity page
        caseRootCauseDropDown                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/select', //activity page root cause drop down
        caseSavePopup                                   :'/html/body/div[4]/div[2]/div', //Activity page case save pop up (while navigating to another page without saving current case data)
        caseSavePopUpYesOption                          :'/html/body/div[4]/div[2]/div/div[3]/button[1]', //Activity page case save pop up option YES
        caseSavePopUpNoOption                           :'/html/body/div[4]/div[2]/div/div[3]/button[2]', //Activity page case save pop up option NO
        caseResolveLink                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[1]/h3/a', //Activity page case resolve link
        caseResolveBtn                                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button', //Activity page case resolve button
        caseResolveCancelBtn                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/small/a', //Activity page case cancel button
        caseResolveByInput                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[1]/div/div/div/div[1]/input', //Activity page case resolve by input
        caseResolvedDateDiv                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[2]/div/div[1]/div/input', //Activity page case resolve date div
        caseResolveDateInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[2]/div/div[1]/div/input', //Resolve by date input in case detail page
        caseResolvedTimeInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[2]/div/div[2]/div/input', //Activity page case resolve time input
        caseResolveCorrectiveActionDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[3]/div/div/select', //Activity page case resolve corrective action drop down
        caseResolveResolutionInput                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/ul/li[4]/div/textarea', //Activity page case resolve resolution input
        caseResolveRelatedItemsInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div[1]/div/div/div/div/div[1]/input', //Activity page case resolve related items input
        caseResolveRelatedItemsTable                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div[2]/table', //Activity page case resolve related items table
        caseCreateMemoLink                              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //Activity page case create memo link
        caseCreateTaskLink                              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]',//Activity page case create task link
        caseRequiredFieldErrorPara                      :'/html/body/div[1]/div/div[1]/div[2]/div/div[2]/p', //Error paragraph for empty required organization field
        caseActSubTabCustomListDropdown                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list drop down in case detail page activity subtab
        caseActSubTabSettingsIcon                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings Icon in case detail page activity subtab
        caseActSubTabCustomListEditLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in case detail page activity subtab
        caseActSubTabCustomListCloneLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in case detail page activity subtab
        caseActSubTabCustomListNewLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in case detail page activity subtab
        caseActSubTabCustomListOrderLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in case detail page activity subtab
        caseActSubTabCloneCustomListInput               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in case detail page activity subtab
        caseActSubTabCloneCustomListCreateBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button case detail page activity subtab
        caseActSubTabCloneCustomListCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/div/a', // Clone custom list close button case detail page activity subtab
        caseActSubTabNewCustomListInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', // New custom list input case detail page activity subtab
        caseActSubTabNewCustomListSaveBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', // New custom list save button case detail page activity subtab
        caseActSubTabNewCustomListCancelBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //New custom list cancel button case detail page activity subtab
        caseActSubTabOrderCustomListDiv                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', // Order custom list div case detail page activity subtab
        caseActSubTabOrderCustomListSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', // Order custom list save button case detail page activity subtab
        caseActSubTabOrderCustomListCancelBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]', //Order custom list cancel button case detail page activity subtab
        caseActSubTabOrderCustomListCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a', // Order custom list close button case detail page activity subtab
        caseActSubTabCustomListAvailColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns in custom list case detail page activity subtab
        caseActSubTabCustomListSelectedColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns in custom list case detail page activity subtab
        caseActSubTabCustomListAddBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', // Add column button in custom list case detail page activity subtab
        caseActSubTabCustomListRemoveBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', // Remove column button in custom list case detail page activity subtab
        caseActSubTabCustomListDeleteBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete button in custom list in case detail page activities subtab
        caseActSubTabQuickFilterLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', // Quick filter link case detail page activity subtab
        caseActSubTabFilterAvailColumns                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', // Available columns in quick filter case detail page activity subtab
        caseActSubTabFilterSelectedColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', // Selected columns in quick filter case detail page activity subtab
        caseActSubTabFilterAddBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', // Add column button in quick filter case detail page activity subtab
        caseActSubTabFilterRemoveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', // Remove column button in quick filter case detail page activity subtab
        caseActSubTabFilterApplyBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', // Apply button in quick filter case detail page activity subtab
        caseActSubTabFilterSaveBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Save button in quick filter case detail page activity subtab
        caseActSubTabFilterResetBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', // Reset button in quick filter case detail page activity subtab
        caseActSubTabFilterCloseBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', // Close button in quick filter case detail page activity subtab
        caseActSubTabPaginationDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[3]/div/div', //Pagination div in case detail page activities subtab
        caseAttachmentLink                              :'//*[@id="customlist_tabs"]/div/ul/li[2]/button',//'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Attachments sub tab on case detail page
        caseActivityTab                                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]/button', //Activity tab in case detail page
        caseNewAttachmentLink                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[2]/ul[2]/li[2]/a', //Upload attachment link in case detail page attachments subtab
        caseAttachmentNameErrorLabel                    :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/label', //Name error label in case detail page attachments subtab
        caseAttachmentFileErrorLabel                    :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/label', //File error label in case detail page attachments subtab
        caseAttachmentDescriptionInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[2]/div/input', //Description input in new upload pop up
        caseAttachmentPHICheckbox                       :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[4]/div/label/input', //PHI checkbox in new upload pop up
        caseAttachmentPopUp                             :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div', //Attachment pop up on click on upload attachment link
        caseAttachmentPopupFileInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/input', //Filename input in attachment pop up
        caseAttachmentUploadBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload button in upload attachment pop up
        caseAttachmentPopupCloseBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a', //Close button of upload attachment pop up
        caseAttachmentsTable                            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[2]/table', //Attachments table in case detail page
        caseAttachmentsCustomList                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list drop down in case detail page attachments subtab
        caseAttachmentsSettingsIcon                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in case detail page attachments subtab
        caseAttachmentsEditCustomListLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in case detail page attachments subtab
        caseAttachmentsCloneCustomListLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in case detail page attachments subtab
        caseAttachmentsNewCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in case detail page attachments subtab
        caseAttachmentsOrderCustomListLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in case detail page attachments subtab
        caseAttachmentsNewCustomListInput               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in case detail page attachments subtab
        caseAttachmentsDeleteCustomListLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in case detail page attachments subtab
        caseAttachmentsNewCustomListSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Save button for new custom list in case detail page attachments subtab
        caseAttachmentsNewCustomListCancelBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //Cancel button for new custom list in case detail page attachments subtab
        caseAttachmentsNewCustomListCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/div/a', //Close button for new custom list in case detail page attachments subtab
        caseAttachmentsCloneCustomListCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Close button for clone custom list in case detail page attachments subtab
        caseAttachmentsCloneCustomListInput             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in case detail page attachments subtab
        caseAttachmentsCloneCustomListCreateBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in case detail page attachments subtab
        caseAttachmentsCustomListAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in case detail page attachments subtab
        caseAttachmentsCustomListSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in case detail page attachments subtab
        caseAttachmentsCustomListAddBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add button in case detail page attachments subtab
        caseAttachmentsCustomListRemoveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove button in case detail page attachments subtab
        caseAttachmentCustomListOrderListDiv            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div', //Order custom list div
        caseAttachmentOrderListCloseBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order list close button
        caseAttachmentsQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in case detail page attachments subtab
        caseAttachmentsFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Quick filter available columns in case detail page attachments subtab
        caseAttachmentsFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Quick filter selected columns in case detail page attachments subtab
        caseAttachmentsFilterAddBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Quick filter column add button in case detail page attachments subtab
        caseAttachmentsFilterRemoveBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Quick filter remove button in case detail page attachments subtab
        caseAttachmentsFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Quick filter apply button in case detail page attachments subtab
        caseAttachmentsFilterSaveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Quick filter save button in case detail page attachments subtab
        caseAttachmentsFilterResetBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Quick filter reset button in case detail page attachments subtab
        caseAttachmentsFilterCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Quick filter close button in case detail page attachments subtab
        caseAttachmentsPaginationDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[3]/div/div', //Pagination div in case detail page attachments subtab

        caseMessagesSubTabLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]', //Messages subtab in case detail page
        caseMessagesCustomListDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list dropdown in case detail page messages subtab
        caseMessagesSettingsIcon                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in case detail page messages subtab
        caseMessagesEditCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list in case detail page messages subtab
        caseMessagesCloneCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list in case detail page messages subtab
        caseMessagesNewCustomListLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list in case detail page messages subtab
        caseMessagesOrderCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list in case detail page messages subtab
        caseMessagesNewCustomListInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input in case detail page messages subtab
        caseMessagesNewCustomListSaveBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in case detail page messages subtab
        caseMessagesNewCustomListCancelBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //New custom list cancel button in case detail page messages subtab
        caseMessagesDeleteCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in case detail page messages subtab
        caseMessagesCloneCustomListInput                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in case detail page messages subtab
        caseMessagesCloneCustomListCreateBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in case detail page messages subtab
        caseMessagesCloneCustomListCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone custom list close button in case detail page messages subtab
        caseMessagesCustomListAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in case detail page messages subtab
        caseMessagesCustomListSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', // Custom list selected columns in case detail page messages subtab
        caseMessagesCustomListAddColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in case detail page messages subtab
        caseMessagesCustomListRemoveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in case detail page messages subtab
        caseMessagesOrderCustomListDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order list div in case detail page messages subtab
        caseMessagesOrderCustomListCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in case detail page messages subtab
        caseMessagesQuickFilterLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[2]/a', //Quick filter link in case detail page messages subtab
        caseMessagesFilterApplyBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in case detail page messages subtab
        caseMessagesFilterSaveBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button in case detail page messages subtab
        caseMessagesFilterResetBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button in case detail page messages subtab
        caseMessagesFilterCloseBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button in case detail page messages subtab
        caseMessagesFilterAvailColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in case detail page messages subtab
        caseMessagesFilterSelectedColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in case detail page messages subtab
        caseMessagesFilterAddBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add button in case detail page messages subtab
        caseMessagesFilterRemoveBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove button in case detail page messages subtab
        caseMessagesTable                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table', //Messages list table in case detail page messages subtab
        caseMessagesPaginationDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[3]/div/div', //Pagination div in case detail page messages subtab


        taskPageHeader                                  :'//span[@id="task_heading"]', //Page header in task detail page
        taskSaveBtn                                     :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]', //Save button in task detail page
        taskSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and back button in task detail page
        taskSubjectInputField                           :'//html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',  //Task subject input on case activity page
        taskSaveButton                                  :'//html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]',      //Save button on Task edited page
        taskViewSubjectFiled                            :'//html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/div',    //Task view subject
        createTaskLink                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div[2]/div/ul/li[3]/a',   //Create Task link on activity page
        taskBreadcrumbDiv                               :'/html/body/div[1]/main/div[2]/div/div/div/ul', //Task detail page bread crumb navigation div
        taskRelatedItemsTable                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div[2]/table', //task detail page related items table
        taskDateInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[8]/div/li/div/div/div/div[1]/div/input', //Date input on task detail page
        taskOrgInput                                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/div[1]/input', //Organization input on task detail page
        taskOrganizationErrorLabel                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/div[1]/label', //Organization error label on task detail page
        taskCategoryDropdown                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/select', //Category dropdown on task detail page
        taskSubCategoryDropdown                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select', //Sub Category drop down on task detail page
        taskCancelBtn                                   :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[3]/a[1]', //Cancel button on task detail page
        taskDueDateInput                                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div/div/div[1]/div/input', //Due date input in task detail page
        taskStatusDropdown                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div/select', //Status drop down in task detail page
        taskDescriptionInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[10]/div/li/div/div/textarea', //Description input in task detail page
        taskAssignedToInput                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/div/div[1]/input', //Assigned to input in task detail page
        taskPriorityDropdown                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/select', //Priority dropdown in task detail page
        taskCCUserInput                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[11]/div/li/div/div[1]/div/div/div[1]/input', //CC USer inputbox in task detail page
        taskSavePopup                                   :'/html/body/div[4]/div[2]/div', //Task Save pop up (on navigating to another page without saving a task)
        taskSavePopUpYesOption                          :'/html/body/div[4]/div[2]/div/div[3]/button[1]', //Activity page task save pop up option YES
        taskSavePopUpNoOption                           :'/html/body/div[4]/div[2]/div/div[3]/button[2]', //Activity page task save pop up option NO
        taskCompleteBtn                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button', //Complete the task button
        taskReopenDiv                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/span', //Reopen task span div
        taskStatusDiv                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div', //Status display for tasks
        taskReopenBtn                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button', //task reopen button
        taskRelatedItemsInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div[1]/div/div/div/div/div[1]/input', //Related items input in task detail page
        taskAttachmentsSubTabLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Attachments sub tab in attachments sub tab task detail page
        taskAttachmentsUploadLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[2]/ul[2]/li[2]/a', //Upload attachment link in task detail page
        taskAttachmentsNameInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/input', //Name input in upload attachment pop up in attachments sub tab task detail page
        taskAttachmentsDescInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[2]/div/input', //Description input in upload attachment pop up in attachments sub tab task detail page
        taskAttachmentsFilenameInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/input', //Filename input in upload attachment pop up in attachments sub tab task detail page
        taskAttachmentsPHICheckbox                      :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[4]/div/label/input', //PHI checkbox in upload attachment pop up in attachments sub tab task detail page
        taskAttachmentsUploadBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload button in upload attachment pop up in attachments sub tab task detail page
        taskAttachmentsPopupCloseBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a', //Attachment pop up close button in attachments sub tab task detail page
        taskAttachmentsPopup                            :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div', //Upload pop up in attachments subtab task detail page
        taskAttachmentsNameErrorLabel                   :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/label', //Name input error label in attachments subtab task detail page
        taskAttachmentsFileErrorLabel                   :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/label', //Filename input error label in attachments subtab task detail page
        taskAttachmentsCustomListDropdown               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list in attachments sub tab task detail page
        taskAttachmentsSettingsIcon                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in attachments sub tab task detail page
        taskAttachmentsEditCustomListLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in attachments sub tab task detail page
        taskAttachmentsCloneCustomListLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in attachments sub tab task detail page
        taskAttachmentsNewCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in attachments sub tab task detail page
        taskAttachmentsOrderCustomListLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in attachments sub tab task detail page
        taskAttachmentsCloneCustomListInput             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in attachments sub tab task detail page
        taskAttachmentsCloneCustomListCreateBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in attachments sub tab task detail page
        taskAttachmentsCloneCustomListCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone custom list close button in attachments sub tab task detail page
        taskAttachmentsNewCustomListInput               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in attachments sub tab task detail page
        taskAttachmentsNewCustomListSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in attachments sub tab task detail page
        taskAttachmentsNewCustomListCancelBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //New custom list cancel button in attachments sub tab task detail page
        taskAttachmentsCustomListAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in attachments sub tab task detail page
        taskAttachmentsCustomListSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in attachments sub tab task detail page
        taskAttachmentsCustomListAddBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in attachments sub tab task detail page
        taskAttachmentsCustomListRemoveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in attachments sub tab task detail page
        taskAttachmentsCustomListDeleteLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Custom list delete link in attachments sub tab task detail page
        taskAttachmentsOrderCustomListDiv               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in attachments sub tab task detail page
        taskAttachmentsOrderCustomListSaveBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', //Order custom list save button in attachments sub tab task detail page
        taskAttachmentsOrderCustomListCancelBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]', //Order custom list cancel button in attachments sub tab task detail page
        taskAttachmentsOrderCustomListCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a/i', //Order custom list close button in attachments sub tab task detail page
        taskAttachmentsQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in attachments sub tab task detail page
        taskAttachmentsFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in attachments sub tab task detail page
        taskAttachmentsFilterSaveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button in attachments sub tab task detail page
        taskAttachmentsFilterResetBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button in attachments sub tab task detail page
        taskAttachmentsFilterCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button in attachments sub tab task detail page
        taskAttachmentsFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in attachments sub tab task detail page
        taskAttachmentsFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columnsin attachments sub tab task detail page
        taskAttachmentsFilterAddBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in attachments sub tab task detail page
        taskAttachmentsFilterRemoveBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in attachments sub tab task detail page
        taskAttachmentsTable                            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[2]/table', //Attachments table list in attachments sub tab task detail page
        taskAttachmentsPaginationDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[3]/div/div', //Attachments pagination  div in attachments sub tab task detail page
        taskMessagesSubTabLink                          :'//*[@id="customlist_tabs"]/div/ul/li[3]/button',//'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]/button', //Messages subtab in messages subtab task detail page
        taskMessagesCustomListDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list in messages subtab task detail page
        taskMessagesSettingsIcon                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in messages subtab task detail page
        taskMessagesEditCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list in messages subtab task detail page
        taskMessagesCloneCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list in messages subtab task detail page
        taskMessagesNewCustomListLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list in messages subtab task detail page
        taskMessagesOrderCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list in messages subtab task detail page
        taskMessagesDeleteCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in messages subtab task detail page
        taskMessagesCloneCustomListInput                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //CLone custom list input in messages subtab task detail page
        taskMessagesCloneCustomListCreateBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in messages subtab task detail page
        taskMessagesCloneCustomListCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone custom list close button in messages subtab task detail page
        taskMessagesNewCustomListInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in messages subtab task detail page
        taskMessagesNewCustomListSaveBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in messages subtab task detail page
        taskMessagesCustomListAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in messages subtab task detail page
        taskMessagesCustomListSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in messages subtab task detail page
        taskMessagesCustomListAddColumnBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in messages subtab task detail page
        taskMessagesCustomListRemoveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in messages subtab task detail page
        taskMessagesOrderCustomListDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in messages subtab task detail page
        taskMessagesOrderCustomListCloseBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list  close button in messages subtab task detail page
        taskMessagesQuickFilterLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[2]/a', //Quick filter link in messages subtab task detail page
        taskMessagesFilterApplyBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in messages subtab task detail page
        taskMessagesFilterSaveBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button in messages subtab task detail page
        taskMessagesFilterResetBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button in messages subtab task detail page
        taskMessagesFilterCloseBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button in messages subtab task detail page
        taskMessagesFilterAvailColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in messages subtab task detail page
        taskMessagesFilterSelectedColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in messages subtab task detail page
        taskMessagesFilterAddColBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in messages subtab task detail page
        taskMessagesFilterRemoveColBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in messages subtab task detail page
        taskMessagesTable                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table', //Messages list table in messages subtab task detail page
        taskMessagesPaginationDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[3]/div/div', //Messages page pagination div in messages subtab task detail page

        memoPageHeader                                  :'//span[contains(@id,"memo_heading")]', //Page header in memo detail page
        memoOrganizationInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/div[1]/input', //Memo detail page organization input
        memoSubjectInputField                           :'//html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',  //Memo subject input on memo detail page
        memoCategoryDropdown                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/select', //Memo category drop down in memo detail page
        memoSubCategoryDropdown                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select', //Memo sub category drop down in case memo detail page
        memoDateInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/div/div[1]/div/input', //Memo category drop down in memo detail page
        memoTimeInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/div/div[2]/div/input', //Memo detail page time input
        memoDescInput                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div/textarea', //Memo detail page description input
        memoSaveButton                                  :'//html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]',     //Save button on Memo edited page
        memoSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Memo save and back button
        memoCancelBtn                                   :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[3]/a[1]', //Memo page cancel button
        memoViewSubjectFiled                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/div', //Memo view subject field
        memoRelatedItemsTable                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[2]/table', //Memo detail page related items table
        memoCreateLink                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div[2]/div/ul/li[4]/a', //Activity page create memo link
        memoBreadcrumbDiv                               :'/html/body/div[1]/main/div[2]/div/div/div/ul', //Memo detail page bread crumb navigation div
        memoOrgRequiredErrorLabel                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/div[1]/label', //Error label for invalid/empty organization input in memo detail page
        memoCCUsersInput                                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div[1]/div/div/div[1]/input', //Memo detail page cc users input
        memoRelatedItemInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div[1]/div/div/div/div/div[1]/input', //Memo detail page related item input search box
        memoSavePopup                                   :'/html/body/div[4]/div[2]/div', //Memo detail page case save pop up (while navigating to another page without saving current case data)
        memoSavePopUpYesOption                          :'/html/body/div[4]/div[2]/div/div[3]/button[1]', //Memo detail page case save pop up option YES
        memoSavePopUpNoOption                           :'/html/body/div[4]/div[2]/div/div[3]/button[2]', //Memo detail page case save pop up option NO
        memoCCUsersDiv                                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div', //Memo detail page CC users whole div
        memoMessagesSubTabLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]', //Memo detail page messages subtab
        memoAttachmentsSubTabLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]', //Memo detail page attachments subtab
        memoMessagesTable                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[2]/table', //Memo detail page messages table
        memoMessageCustomListDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list in memo detail page messages sub tab
        memoMessageSettingsIcon                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in memo detail page messages sub tab
        memoMessageCustomListEditLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in memo detail page messages sub tab
        memoMessageCustomListCloneLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in memo detail page messages sub tab
        memoMessageNewCustomListLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in memo detail page messages sub tab
        memoMessageOrderCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in memo detail page messages sub tab
        memoMessageCloneCustomListInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in memo detail page messages sub tab
        memoMessageCloneCustomListCreateBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in memo detail page messages sub tab
        memoMessageCloneCustomListCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone custom list close button in memo detail page messages sub tab
        memoMessageNewCustomListInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in memo detail page messages sub tab
        memoMessageCustomListAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in memo detail page messages sub tab
        memoMessageCustomListSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list Selected columns in memo detail page messages sub tab
        memoMessageCustomListAddColumnBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in memo detail page messages sub tab
        memoMessageCustomListRemoveColumnBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in memo detail page messages sub tab
        memoMessageCustomListSaveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in memo detail page messages sub tab
        memoMessageCustomListCancelBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //Custom list cancel button in memo detail page messages sub tab
        memoMessageOrderCustomListDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in memo detail page messages sub tab
        memoMessageOrderCustomListSaveBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]', //Order custom list save button in memo detail page messages sub tab
        memoMessageOrderCustomListCancelBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[2]', //Order custom list  cancel button in memo detail page messages sub tab
        memoMessageOrderCustomListCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom  list close button in memo detail page messages sub tab
        memoMessageCustomListDeleteLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Custom list delete link in memo detail page messages sub tab
        memoMessageQuickFilterLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in memo detail page messages sub tab
        memoMessageFilterAvailColumns                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in memo detail page messages sub tab
        memoMessageFilterSelectedColumns                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in memo detail page messages sub tab
        memoMessageFilterAddBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in memo detail page messages sub tab
        memoMessageFilterRemoveBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in memo detail page messages sub tab
        memoMessageFilterApplyBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in memo detail page messages sub tab
        memoMessageFilterSaveBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button in memo detail page messages sub tab
        memoMessageFilterResetBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button in memo detail page messages sub tab
        memoMessageFilterCloseBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button in memo detail page messages sub tab
        memoMessagePaginationDiv                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[3]/div/div', //Pagination div in memo detail page messages subtab
        memoAttachmentsCustomListDropdown               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list in memo detail page attachments subtab
        memoAttachmentsSettingsIcon                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in memo detail page attachments subtab
        memoAttachmentsCustomListEditLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in memo detail page attachments subtab
        memoAttachmentsDeleteCustomListLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in attachments subtab memo detail page
        memoAttachmentsCustomListCloneLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in memo detail page attachments subtab
        memoAttachmentsNewCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in memo detail page attachments subtab
        memoAttachmentsOrderCustomListLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in memo detail page attachments subtab
        memoAttachmentsCloneCustomListInput             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in memo detail page attachments subtab
        memoAttachmentsCloneCustomListCreateBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in memo detail page attachments subtab
        memoAttachmentsNewCustomListInput               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in memo detail page attachments subtab
        memoAttachmentsNewCustomListSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in memo detail page attachments subtab
        memoAttachmentsNewCustomListCancelBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //New custom list cancel button in memo detail page attachments subtab
        memoAttachmentsCustomListAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in memo detail page attachments subtab
        memoAttachmentsCustomListSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in memo detail page attachments subtab
        memoAttachmentsCustomListAddColumnBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in memo detail page attachments subtab
        memoAttachmentsCustomListRemoveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in memo detail page attachments subtab
        memoAttachmentsOrderCustomListDiv               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in memo detail page attachments subtab
        memoAttachmentsOrderCustomListSaveBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list save button in memo detail page attachments subtab
        memoAttachmentsOrderCustomListCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in memo detail page attachments subtab
        memoAttachmentsQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', //Quick filter link in memo detail page attachments subtab
        memoAttachmentsFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in memo detail page attachments subtab
        memoAttachmentsFilterSaveBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button in memo detail page attachments subtab
        memoAttachmentsFilterResetBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button in memo detail page attachments subtab
        memoAttachmentsFilterCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button in memo detail page attachments subtab
        memoAttachmentsFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in memo detail page attachments subtab
        memoAttachmentsFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in memo detail page attachments subtab
        memoAttachmentsFilterAddColumnBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in memo detail page attachments subtab
        memoAttachmentsFilterRemoveColumnBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in memo detail page attachments subtab
        memoAttachmentsUploadLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //Upload attachment link in memo detail page attachments subtab
        memoAttachmentUploadPopup                       :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div', //New upload pop up in memo detail page attachments subtab
        memoAttachmentUploadPopupCloseBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a', //Close button for attachment upload in attachments subtab memo detail page
        memoAttachmentNameRequiredLabel                 :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/label', //Required field label for name input in memo detail page attachments subtab
        memoAttachmentFileRequiredLabel                 :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/label', //Required field label for filename input in memo detail page attachments subtab
        memoAttachmentNameInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/input', //Name input in memo detail page attachments subtab
        memoAttachmentDescriptionInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[2]/div/input', //Description input in memo detail page attachments subtab
        memoAttachmentFileInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/input', //File upload input in memo detail page attachments subtab
        memoAttachmentPHICheckbox                       :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[4]/div/label/input', //PHI checkbox in memo detail page attachments subtab
        memoAttachmentUploadBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload button in memo detail page attachments subtab
        memoAttachmentTable                             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Attachments list table  in memo detail page attachments subtab
        memoAttachmentPagingDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[3]/div/div', //Paging div in attachments subtab memo detail page


        activityTabHeaderOnCloudSearch			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[5]', //Activity cloud search head tab
        activityTabHeaderOnCloudSearchDiv		: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[5]/div[1]', //Activity cloud search head tab div
        activityCaseSpan				: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span',//Activity page Case (Case number) span
        activityOrgName					: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[1]/span',//Activity page organization name (Case edit page) span
        categoryCaseInput				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select',//Activity page case category input field
        subCategoryCaseInput				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select',//Activity page case sub category input field
        categoryCaseDiv					: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div',//Activity page Case (Case category) span
        subCategoryCaseDiv				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div',//Activity page Task (Case Sub category) span
        caseCloudSearchDataLink				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[5]/div[1]/ul/li/div[1]/div[1]/div/span/span', //Activity cloud search searchable case link
        activityTaskSpan				: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span', //Activity page Task (Task number) span
        categoryTaskDiv					: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div',//Activity page Task (Task category) span
        subCategoryTaskDiv				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div',//Activity page Task (Task Sub category) span
        categoryTaskInput				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/select',//Activity page Task category input field
        subCategoryTaskInput				: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select',//Activity page Task sub category input field
        activityMemoSpan				: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span',//Activity memo span field on thee activity page


    };
module.exports.activityPageElements = activityPageElements;
//ACTIVITY PAGE ELEMENTS - END



//COLLABORATION PAGE ELEMENTS - START
var collaborationPageElements =
    {
        collaborationTabLink                            :'//*[@id="comm-help"]/a', //Collaboration center tab on home page
        collaborationPageHeader                         :'/html/body/div[1]/main/div[3]/div/div[1]/div/h1/span', //To check name of Collaboration center present on Collaboration centre page
        collaborationTabCount                           :'/html/body/div[1]/header/div[2]/div/nav/ul/li[1]/a/span[1]', //Collaboration center notifications count
        notificationSubTab                              :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/button[1]', //Notification sub tab present on Collaboration centre page
        notificationTabCount                            :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/button[1]/span', //notifications tab count
        messageSubTab                                   :'//*[@id="message_list_subtab"]',  //Message sub tab present on Collaboration centre page
        sentMessageSubTab                               :'//*[@id="message_sent_list_subtab"]', //Sent message sub tab present on Collaboration centre page
        calendarSubTab                                  :'//*[@id="calendar_subtab"]', //Calendar sub tab present on Collaboration centre page
        newMessageLink                                  :'//*[@id="new_message"]', //New message link on message page
        newMessageToInput                               :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[1]/div/div/div[1]/div[2]/div/div/div/div/div[1]/input',    //"to message text field"  on Message pop up window
        newMessageCCInput                               :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[2]/div/div/div[1]/div[2]/div/div/div/div/div[1]/input',    //"cc message text field " on Message pop up window
        newMessageSubjectInput                          :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[4]/div/ul/li[1]/div/input', //"subject text field"  on Message pop up window
        newMessageMessageInput                          :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[4]/div/ul/li[2]/div/textarea', //" message  text field " on Message pop up window
        newMessageRelatedItemInput                      :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[2]/div/div/div[1]/div[2]/div/div/div[1]/input',     //"Related items text field"  on Message pop up window
        newMessageSendBtn                               :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/button[1]', //Send button on Message pop up window
        newMessageCancelBtn                             :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/button[2]', //Cancel button on Message pop up window
        newMessageReplyBtn                              :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/button[1]', //Reply button on Message pop up window
        newMessageReplyAllBtn                           :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[1]/div/div[2]/div[2]/button[2]', //Reply all button on Message pop up window
        messagesRecordsTable                            :'//*[@id="message_list"]/div[1]/div[2]/div/div/div[2]/table', //Message record table on Message tab page in collaboration center
        newMessageCloseBtn                              :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[1]/a', //Close button on Message pop up window
        messageTabCount                                 :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/button[2]/span', //Messages count span in message tab
        newMessagePopup                                 :'/html/body/div[1]/main/div[3]/div/div[4]',
        messageRecipientWrapper                         :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[1]/div/div/div[2]',
        messageCCRecipientWrapper                      :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[2]/div/div/div[2]',
        messageRelatedItemsTable                        :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[2]/div/div/div[2]/div/table',
        messageToRecipientDiv                           :'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[1]',
        relatedItemsSearchResultsDiv                    :'html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[2]/div/div/div[1]/div[2]/div/div/div[3]/ul/div/div[1]', //Related items search results div

        notificationCustomListDropdown                  :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[1]/select',   //Custom list dropdown
        notificationSettingsIcon                        :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        notificationEditCustomListLink                  :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        notificationCloneCustomListLink                 :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        notificationNewCustomListLink                   :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link
        notificationOrderCustomListLink                 :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        notificationDeleteCustomListLink                :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link
        notificationFilterByDropdown                    :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Filter by dropdown
        notificationFilterOperatorByDropdown            :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Operator by dropdown
        notificationFilterAvailColumns                  :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        notificationFilterSelectedColumn                :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns
        notificationCloneCustomListInput                :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input
        notificationCloneCreateBtn                      :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button
        notificationNewCustomListInput                  :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        notificationNewCustomListSaveBtn                :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //NEw custom list save button
        notificationCustomListAvailColumns              :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns
        notificationCustomListSelectedColumns           :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns
        notificationAddColumnBtn                        :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button
        notificationRemoveColBtn                        :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button
        notificationOrderList                           :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order list
        notificationOrderListCloseBtn                   :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order list close button
        notificationQuickFilterLink                     :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        notificationFilterApplyBtn                      :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //filter apply button
        notificationFilterSaveBtn                       :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Filter save button
        notificationFilterResetBtn                      :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Filter reset button
        notificationFilterCloseBtn                      :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Filter close button
        notificationTable                               :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[2]/table', //Notification custom list table
        notificationOrderUpBtn                          :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[1]', //Filter order list up button
        notificationOrderDownBtn                        :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[2]', //Filter order list down button
        notificationPagingTotalRecords                  :'html/body/div[1]/main/div[3]/div/div[3]/div[1]/div[1]/div[2]/div/div/div[3]/div/div/ul/li[6]/span/span', //Total records in paging
    };
module.exports.collaborationPageElements = collaborationPageElements;
//COLLABORATION PAGE ELEMENTS - END



//DASHBOARD PAGE ELEMENTS - START
var dashboardPageElements =
    {
        dashboardTabLink                                :'//*[@id="site_nav"]/ul/li[2]/a/span', //Dashboard tab on home page upper panel
        dashboardPageHeader                             :'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //view Dashboard header on dashboard page
    };
module.exports.dashboardPageElements = dashboardPageElements;
//DASHBOARD PAGE ELEMENTS - END



//CONTACTS PAGE ELEMENTS - START
var contactsPageElements =
    {
        contactCreateModalDiv                           :'/html/body/div[4]/div[2]/div', //Create contact modal
        contactsPageHeader                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span', //Contact tab on home page upper panel
        contactsPageSaveAndBackBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //contact save and back button on edited contact page
        contactsPageSaveBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //contact save button on edited contact page
        contactsTabLink                                 :'//*[@id="site_nav"]/ul/li[4]/a/span', //Contact button on  contact page
        contactPageTitle                                :'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //to check contact name on contact page
        createContactLink                               :'//*[@id="contact_create"]', //Create button on contact page
        contactTypeInput                                :'//*[@id="Contact"]', //Contact type input on Create new contact pop window
        providerTypeInput                               :'//*[@id="Provider"]', //Provider type input on Create new contact pop window
        firstNameInput                                  :'//*[@id="contact_firstname"]', //Contact firstName input on Create new contact pop window
        middleNameInput                                 :'//*[@id="contact_middlename"]',//'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[6]/div/input', //Contact middleName input on Create new contact pop window
        lastNameInput                                   :'//*[@id="contact_lastname"]', //Contact lastName input on Create new contact pop window
        suffixInput                                     :'//*[@id="contact_suffix"]',//'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[8]/div/input', //Contact suffix input on Create new contact pop window
        emailInput                                      :'//*[@id="contact_email"]',//'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[9]/div/input', //Contact email input on Create new contact pop window
        phoneInput                                      :'//*[@id="contact_phone"]',//'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[10]/div/input', //Contact phone input on Create new contact pop window
        mobileInput                                     :'//*[@id="contact_mobile"]',//'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[11]/div/input', //Contact mobile input on Create new contact pop window
        organizationInput                               :'//*[@id="contact_organization"]', //Contact organization input on Create new contact pop window
        orgSearchPopUp                                  :'/html/body/div[4]/div[2]/div/div[3]/div[2]/div/div[3]/div/div[1]/ul/div/div[1]/li[1]', //Org search pop up window is open on create new contact page
        createLink                                      :'//*[@id="create_contact"]', //Create btn on create new contact page
        quickCreateLink                                 :'//*[@id="quick_create_contact"]', //Quick Create btn on create new contact page
        contactsTable                                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/table', //Contacts/providers table list
        contactEditBtn                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Edit button on contacts page
        contactGeneralTab                               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[1]', //Contacts page general tab
        contactAddressTab                               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[2]', //Contacts page address subtab
        contactCommPrefTab                              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[3]', //Contacts page communication preferences subtab
        contactOrgSubtab                                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[4]', //Contacts page organization subtab
        contactActivitiesSubTab                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]', //Contacts page activities subtab
        contactAttachmentsSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]', //Contacts page attachments subtab
        contactMessagesSubTab                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]', //Contacts page messages subtab
        contactCampaignsSubTab                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[4]', //Contacts page campaigns subtab
        contactPageIcon                                 :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/i', //Contact icon on contact detail page
        contactJobTitleDiv                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div/div', //Job title div in contact detail page without edit mode
        contactJobTitleInput                            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div/input', //Contact job title input in contact detail page
        contactDepartmentInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[4]/div/li/div/div/input', //Department input
        contactCredentialsInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[5]/div/li/div/div/input', //Credentials input in contact detail page
        contactSpecialtiesDiv                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[3]/div/li/div', //Specialties div in case of provider and non edit mode
        contactSettingsPopupCloseBtn                    :'/html/body/div[4]/div/div/div[1]/a', //Contact settings popup close button
        contactPageFnameInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/input', //First name input in contact detail page
        contactPageMNameInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/input', //Middle name input in contact detail page
        contactPageLNameInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/input', //Last name input in contact detail page
        contactPageSuffixInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/div/input', //Suffix input in contact detail page
        contactPageEmailInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/input', //Email input in contact detail page
        contactPageSalutationDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/select', //Salutation dropdown in contact detail page
        contactPageOtherPhoneInput                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[10]/div/li/div/div/input', //Other phone input in contact detail page
        contactPageFaxInput                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[11]/div/li/div/div/input', //Fax input in contact detail page
        contactPagePhoneInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[8]/div/li/div/div/input', //Phone input in contact detail page
        contactPageMobileInput                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div/input', //Mobile input in contact detail page
        contactFooterInfo                               :'/html/body/div[1]/main/div[3]/div[1]/div[5]/div/span', //Created/modified info div in footer with user, date and time
        contactPageNewCaseLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //New case link in contacts page
        contactPageNewTaskLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a', //New task link in contacts page
        contactPageNewMemoLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[4]/a', //New memo link in contacts page
        contactPageNewOpporLink                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[5]/a', //New opportunity in contacts page
        contactPageTypeDropdown                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/select', //Contact type dropdown in contact detail page
        providerNPIInput                                :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[2]/div/input', //NPI input for contact type provider
        providerDetailPageSpecialtyDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[3]/div/li/div/div[1]/span[1]/input[2]', //Specialty dropdown for provider contact type detail page

        contactDoNotCallCheckbox                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/div/input', //Do not call check box in contact detail page
        contactEmailOptOutCheckbox                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/div/input', //Email opt out checkbox in contact detail page
        contactTextOptOutCheckbox                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/div/input', //Text opt out checkbox in contact detail page
        contactPreferEmailsCheckbox                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div/div/input', //Prefer emails checkbox in contact detail page
        contactPreferTextsCheckbox                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div/div/div/input', //Prefer texts checbox in contact detail page
        contactLeadCheckbox                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[3]/div/li/div/div/div/input', //Lead checkbox in contact detail page
        contactCustomerCheckbox                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[4]/div/li/div/div/div/input', //Customer checkbox in contact detail page

        contactCreateModalContactOption                 :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[1]/div/label[1]/input', //Contact radio button in create contact modal
        contactCreateModalProviderOption                :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[1]/div/label[2]/input', //Contact radio button in create provider modal
        contactCreateModalTypeDropdown                  :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[3]/div/div/select', //Contact type dropdown in create contact modal
        contactCreateModalSalutationDropdown            :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[4]/div/div/select', //Salutation dropdown in create contact modal
        contactExistingSearchResultsDiv                 :'/html/body/div[5]/div[2]/div/div[3]/div[1]/div/div[3]/div', //Existing contacts search results
        contactCreateOrgBtnInOrgSearch                  :'/html/body/div[4]/div[2]/div/div[3]/div[2]/div/div[3]/div/div[2]/button', //Create organization button in search organization popup
        contactSaveOrgBtnInOrgSearch                    :'/html/body/div[4]/div[2]/div/div[3]/form/div[2]/button', //Create and save button in organization search pop up
        contactCopyOrgAddressCheckbox                   :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[13]/div/label/input', //Copy organization address to contact checkbox
        contactMakePrimaryCheckBox                      :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[14]/div/label/input', //Make primary checkbox in create modal
        contactFNameErrorLabel                          :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[5]/div/label', //Error label for firstname input
        contactLNameErrorLabel                          :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[7]/div/label', //Error label for lastname input
        contactOrgErrorLabel                            :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[12]/div/label', //Error label for organization input
        contactOrgInputDiv                              :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[12]', //Organization input  div in create modal

        contactPicUploadBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[1]/li[2]/span/input', //Upload profile pic button
        contactPicEditBtn                               :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[1]/li[1]', //Edit profile pic button
        contactPicDeleteBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[1]/li[3]', //Delete profile pic button
        contactPicSaveBtn                               :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[1]/li[4]', //Save profile pic button
        contactPicCancelBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[1]/li[5]', //Cancel save profile pic button
        contactPicResizeControl                         :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div[2]/div/ul[2]/li', //Resize profile pic button

        contactSettingsIcon                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/i', //Settings Icon on contact detail page
        contactHostCodeEditBtn                          :'/html/body/div[4]/div[2]/div/div[5]/button', //Edit button to update host code
        contactVCardContactTypeDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[1]', //Contact type div in contact VCard
        contactVCardOrgDetailsDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[2]', //Organization details in contact VCard
        contactVCardOrgDetailNameDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[2]/span/span', //Organization name in contact VCard
        contactVCardHostCodeDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[3]', //Host code div in contact VCard

        contactAttachmentUploadLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[2]/ul[2]/li[2]/a', //Upload attachment link in attachments tab contact detail page
        contactAttachmentPopupUploadBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload attachment in upload attachment pop up
        contactAttachmentPopupCloseBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a/i', //Upload popup close button

        contactMailingAddressStreet1Div                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[1]/div', //Street1 mailing address div
        contactMailingAddressStreet1Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[1]/div/input', //Street1 mailing address input
        contactMailingAddressStreet2Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[2]/div/input', //Street2 mailing address input
        contactMailingAddressCityInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[3]/div/input', //City mailing address input
        contactMailingAddressStateInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[4]/div/input', //State mailing address input
        contactMailingAddressPostalInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[5]/div/input', //Postal code mailing address input
        contactMailingAddressCountryInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[6]/div/input', //Country mailing address input

        contactBillingAddressStreet1Div                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[1]/div/div', //Street1 div in billing address
        contactBillingAddressStreet2Div                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[2]/div/div', //Street2 div in billing address
        contactBillingAddressCityDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[3]/div/div', //City div in billing address
        contactBillingAddressStateDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[4]/div/div', //State div in billing address
        contactBillingAddressPostalDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[5]/div/div', //Postal div in billing address
        contactBillingAddressCountryDiv                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[6]/div/div', //Country div in billing address
        contactBillingAddressStreet1Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[1]/div/input', //Street1 billing address input
        contactBillingAddressStreet2Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[2]/div/input', //Street2 billing address input
        contactBillingAddressCityInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[3]/div/input', //City billing address input
        contactBillingAddressStateInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[4]/div/input', //State billing address input
        contactBillingAddressPostalInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[5]/div/input', //Postal code billing address input
        contactBillingAddressCountryInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[6]/div/input', //Country billing address input
        contactSameAsMailingCheckbox                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/h3/span/small/span/input', //Same as mailing address checkbox

        contactSavePopUpYesOption                       :'/html/body/div[4]/div[2]/div/div[3]/button[1]', //'Yes' option in save contact pop up when navigated to other tab without saving contact
        contactSavePopUpNoOption                        :'/html/body/div[4]/div[2]/div/div[3]/button[2]', //'No' option in save contact pop up when navigated to other tab without saving contact
        contactSavePopup                                :'/html/body/div[4]/div[2]/div', //Save contact pop up when navigated to other tab without saving contact

        contactOrgSearchBoxInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[1]/input', //Organization search box
        contactOrgContactAssociateBtn                   :'/html/body/div[4]/div[2]/div/div[5]/button', //Create association button in associate organization and contact pop up
        contactOrgCreateOrgLink                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[4]/a', //Create organization link in organization subtab
        contactOrgCreatePopupOrgNameInput               :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li/div/input', //Organization name input in create org pop up from org subtab
        contactOrgCreatePopupNextStepBtn                :'/html/body/div[4]/div[2]/div/div[5]/button', //Next step button  in create org pop up from org subtab


        contactOrgQuickFilterLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in organization subtab
        contactOrgFilterValueInput                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/input', //Value input in quick filter organization subtab
        contactOrgFilterByDropdown                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Filter by dropdown in organization subtab
        contactOrgFilterOperatorDropdown                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Operator dropdown in organization subtab
        contactOrgFilterAvailColumns                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Available columns in organization subtab
        contactOrgFilterSelectedColumns                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Selected columns in organization subtab
        contactOrgFilterAddColBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button in  organization subtab
        contactOrgFilterRemoveColBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button in  organization subtab
        contactOrgFilterApplyBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button in organization subtab
        contactOrgFilterSaveBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Save button in organization subtab
        contactOrgFilterResetBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Reset button in organization subtab
        contactOrgFilterCloseBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Close button in organization subtab
        contactOrgTable                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Custom list organization table in organization subtab

        contactOrgSettingsIcon                          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in organization subtab
        contactOrgCustomListEditLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in organization subtab
        contactOrgCustomListCloneLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in organization subtab
        contactOrgCustomListNewLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in organization subtab
        contactOrgCustomListOrderLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in organization subtab
        contactOrgCustomListCloneInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone name input in organization subtab
        contactOrgCustomListCloneCreateBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in organization subtab
        contactOrgCustomListCloneCloseBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone pop up close button in organization subtab
        contactOrgCustomListNewNameInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input in organization subtab
        contactOrgCustomListAvailColumns                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns in organization subtab
        contactOrgCustomListSelectedColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns in organization subtab
        contactOrgCustomListAddColBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in organization subtab
        contactOrgCustomListRemoveColBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in organization subtab
        contactOrgCustomListNewListSaveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Save new custom list button in organization subtab
        contactOrgCustomListOrderDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in organization subtab
        contactOrgCustomListOrderCloseBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list pop up close button in organization subtab
        contactOrgCustomListDeleteLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link

        contactActivityTable                            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Activity table in activities subtab
        contactActivitySettingsIcon                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in activities subtab
        contactActivityCustomListEditLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in activities subtab
        contactActivityCustomListCloneLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in activities subtab
        contactActivityCustomListNewLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in activities subtab
        contactActivityCustomListOrderLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in activities subtab
        contactActivityCustomListCloneInput             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone input in activities subtab
        contactActivityCustomListCloneCreateBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in activities subtab
        contactActivityCustomListCloneCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone pop up close button in activities subtab
        contactActivityCustomListNewInput               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in activities subtab
        contactActivityCustomListAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns in activities subtab
        contactActivityCustomListSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns in activities subtab
        contactActivityCustomListAddColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in activities subtab
        contactActivityCustomListRemoveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in activities subtab
        contactActivityCustomListNewSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in activities subtab
        contactActivityCustomListOrderDiv               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in activities subtab
        contactActivityCustomListOrderCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in activities subtab
        contactActivityCustomListDeleteLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in activities subtab
        contactActivityQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        contactActivityFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Available columns
        contactActivityFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Selected columns
        contactActivityFilterAddColBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button
        contactActivityFilterRemoveColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button
        contactActivityFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button

        contactAttachmentUploadLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[2]/ul[2]/li[2]/a', //Upload attachment link
        contactAttachmentUploadPopupNameInput           :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/input', //Upload pop up name input
        contactAttachmentUploadPopupDescInput           :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[2]/div/input', //Upload pop up description input
        contactAttachmentUploadPopupFilenameInput       :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/input', //Filename input in upload attachment pop up
        contactAttachmentUploadPhiCheckbox              :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[4]/div/label/input', //PHI checkbox in upload attachment pop up
        contactAttachmentUploadBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload button in upload attachment pop up
        contactAttachmentUploadPopupCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a', //Upload attachment pop up close button
        contactAttachmentsTable                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[2]/table', //Attachments table custom list
        contactAttachmentsNameErrorLabel                :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/label', //Name error label in upload attachment popup
        contactAttachmentsFilenameErrorLabel            :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[3]/div/div/label', //Filename error label in upload attachment popup

        contactAttachmentSettingsIcon                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in attachments subtab
        contactAttachmentCustomListEditLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        contactAttachmentCustomListCloneLink            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        contactAttachmentCustomListNewLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link
        contactAttachmentCustomListOrderLink            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        contactAttachmentCustomListCloneInput           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone name input
        contactAttachmentCustomListCloneCreateBtn       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button
        contactAttachmentCustomListCloneCloseBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone close button
        contactAttachmentCustomListNewInput             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        contactAttachmentCustomListAvailColumns         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns
        contactAttachmentCustomListSelectedColumns      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns
        contactAttachmentCustomListAddColBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button
        contactAttachmentCustomListRemoveColBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button
        contactAttachmentCustomListNewSaveBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button
        contactAttachmentCustomListOrderDiv             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div
        contactAttachmentCustomListOrderCloseBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button
        contactAttachmentCustomListDeleteLink           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete link
        contactAttachmentQuickFilterLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        contactAttachmentFilterAvailColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        contactAttachmentFilterSelectedColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns
        contactAttachmentFilterAddColBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button
        contactAttachmentFilterRemoveColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button
        contactAttachmentFilterApplyBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button
        contactAttachmentPagingDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[3]/div/div', //Paging div in attachments subtab

        contactMessageSettingsIcon                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in messages subtab
        contactMessageCustomListEditLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in messages subtab
        contactMessageCustomListCloneLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //CLone custom list link in messages subtab
        contactMessageCustomListNewLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in messages subtab
        contactMessageCustomListOrderLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in messages subtab
        contactMessageCustomListCloneInput              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in messages subtab
        contactMessageCustomListCloneCreateBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in messages subtab
        contactMessageCustomListNewInput                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in messages subtab
        contactMessageCustomListAvailColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns in messages subtab
        contactMessageCustomListSelectedColumns         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns in messages subtab
        contactMessageCustomListNewSaveBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in messages subtab
        contactMessageCustomListOrderDiv                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in messages subtab
        contactMessageCustomListOrderCloseBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in messages subtab
        contactMessageCustomListDeleteLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in messages subtab
        contactMessageTable                             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table', //Messages custom list table
        contactMessagePagingDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[3]/div/div', //Paging div in messages subtab
        contactMessageCustomListAddColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button
        contactMessageCustomListRemoveColBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Remove column button

        contactMessageQuickFilterLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[2]/a', //Quick filter link in messages subtab
        contactMessageFilterAvailColumns                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in messages subtab
        contactMessageFilterSelectedColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in messages subtab
        contactMessageFilterAddColBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button in messages subtab
        contactMessageFilterRemoveColBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button in messages subtab
        contactMessageFilterApplyBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button in messages subtab

        contactCampaignsTable                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[2]/table', //Campaigns custom list in campaigns subtab
        contactCampaignsSettingsIcon                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in campaigns subtab
        contactCampaignsCustomListEditLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in campaigns subtab
        contactCampaignsCustomListCloneLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in campaigns subtab
        contactCampaignsCustomListNewLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in campaigns subtab
        contactCampaignsCustomListOrderLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in campaigns subtab
        contactCampaignsCustomListCloneNameInput        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone name input in campaigns subtab
        contactCampaignsCustomListCloneCreateBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in campaigns subtab
        contactCampaignsCustomListNewNameInput          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in campaigns subtab
        contactCampaignsCustomListAvailColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns in campaigns subtab
        contactCampaignsCustomListSelectedColumns       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns in campaigns subtab
        contactCampaignsCustomListAddColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in campaigns subtab
        contactCampaignsCustomListRemoveColBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in campaigns subtab
        contactCampaignsCustomListSaveBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Save custom list button in campaigns subtab
        contactCampaignsCustomListOrderDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in campaigns subtab
        contactCampaignsCustomListOrderCloseBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in campaigns subtab
        contactCampaignsCustomListDeleteLink            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete link in campaigns subtab
        contactCampaignsQuickFilterLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[2]/a', //Quick filter link in campaigns subtab
        contactCampaignsFilterAvailaColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in campaigns subtab
        contactCampaignsFilterSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in campaigns subtab
        contactCampaignsFilterAddColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in campaigns subtab
        contactCampaignsFilterRemoveColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in campaigns subtab
        contactCampaignsFilterApplyBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in campaigns subtab
        contactCampaignsPagingDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[3]/div/div', //Paging div in campaigns subtab

        //---------------------------------------- CLOUD SEARCH START -------------------------------------------------------------------

        cloudSearchField					: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[1]/input', //Cloud search field on the desktop
        collaborationHomeTabLink			: '/html/body/div[1]/header/div[2]/div/nav/ul/li[1]/a/span[2]', //Collaboration link on desktop
        saveChangesYesIcon					: '/html/body/div[4]/div[2]/div/div[3]/button[1]', //Save pop window (when page is redirected to other page without save it)
        cloudSearchContactTabLink			: '/html/body/div[1]/header/div[2]/div/nav/ul/li[1]/a/span[2]',//Contact tab after cloud search is applied
        contactCloudSearchDataLink			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li[1]',  //For select searchable data on cloud search
        editBtnOnHostCodeCreationPage		: '/html/body/div[4]/div[2]/div/div[5]/button',// Edit button on Contact page host code code creation page
        hostCodeInput						: '/html/body/div[4]/div/div/div[2]/div/div/table/tbody/tr[1]/td[2]/div/div/div[1]/input',//Host code input field on contact page
        saveBtnOnHostCodePage				: '/html/body/div[4]/div/div/div[5]/button',//Save button on the host code pop window page
        closeIconBtnOnHostCodePage			: '/html/body/div[4]/div/div/div[1]/a/i',//Close button on the host code pop window page
        hostCodeSpanElement					: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[3]/span',//Host code span on the contact save page
        mobileNumberDiv						: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div/div/a', //Mobile number div on contact edit pagee
        phoneNumberDiv						: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[8]/div/li/div/div/div/a',//Phone number div on contact edit pagee

        cloudSearchCaseIcon					: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li/div[1]/div[2]/div/ul/li[1]/span/i',//Case  icon when search Task or Case activity searchable on cloud search
        cloudSearchOpportunityIcon			: '//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[4]/span/i', //Opportunity  icon when search Task or Case activity searchable on cloud search

        cloudSearchTaskIcon						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li/div[1]/div[2]/div/ul/li[2]/span/i',//Task icon when search Memo or Case activity searchable on cloud search
        cloudSearchCaseIcon						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li/div[1]/div[2]/div/ul/li[1]/span/i', //Case icon when search Memo or Task activity searchable on cloud search
        cloudSearchMemoIcon						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li/div[1]/div[2]/div/ul/li[3]/span/i',//Memo icon when search Task or Case activity searchable on cloud search
        cloudSearchOpportunityIcon			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul/li/div[1]/div[2]/div/ul/li[4]/span/i', //Opportunity  icon when search Task or Case activity searchable on cloud search

        // Provider
        providerCloudSearchDataLink			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[1]/div/span',//Provider data link after cloud search is applied
        providerCloudSearchCaseIcon			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[2]/div/ul/li[1]/span/i',//Case icon on Provider after cloud search is applied
        providerCloudSearchTaskIcon			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[2]/div/ul/li[2]/span/i',//Task icon on Provider after cloud search is applied
        providerCloudSearchMemoIcon			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[2]/div/ul/li[3]/span/i',//Memo icon on Provider after cloud search is applied
        providerCloudSearchOpportunityIcon	: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[2]/div/ul/li[4]/span/i',//Opportunity icon on Provider after cloud search is applied

    };
module.exports.contactsPageElements = contactsPageElements;
//CONTACTS PAGE ELEMENTS - END



//CAMPAIGN PAGE ELEMENTS - START
var campaignPageElements =
    {
        campaignTabLink                                 :'/html/body/div[1]/header/div[2]/div/nav/ul/li[5]/a', //campaign tab on home page
        campaignListPageHeader                          :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[1]/h1', //Campaigns list page header
        campaignsTable                                  :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/table', //Campaigns custom list
        pageTitle                                       :'//*[@id="campaign_list"]/div[1]/div[1]/div[1]/h1', //campaign list on campaign page
        campaignCreateLink                              :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/ul/li[2]/a', //Create campaign link
        campaignEditBtn                                 :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Edit button
        campaignPopupNameInput                          :'/html/body/div[4]/div[2]/div/div[2]/div[1]/form/ul/li[2]/div/input', //Campaign name input in popup
        campaignPopupTargetTypeDropdown                 :'/html/body/div[4]/div[2]/div/div[2]/div[1]/form/ul/li[3]/div/select', //Target type dropdown in popup
        campaignPopupCreateBtn                          :'/html/body/div[4]/div[2]/div/div[3]/button', //Create button in pop up
        campaignPopupCampaignTypeDropdown               :'/html/body/div[8]/div[2]/div/div[2]/div[1]/form/ul/li[1]/div/select', //Campaign type dropdown in create campaign pop up
        campaignSaveBtn                                 :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Save campaign button
        campaignSaveAndBackBtn                          :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Save and back button
        campaignGeneralInfoSubTab                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[1]', //General information subtab
        campaignAudienceSubTab                          :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[2]', //Audience subtab
        campaignMessageTemplatesSubTab                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[3]', //Message templates subtab
        campaignActivitySubTab                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]/button', //Activity subtab
        campaignMessageSubTab                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Messages subtab
        campaignSearchContactInput                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[1]/input', //Search and add contact input box in audience subtab
        campaignDetailPageNameInput                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div/div/ul/div[1]/div/li/div/div/input', //Name input in contact detail page
        campaignDetailPageTypeDropdown                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div/div/ul/div[2]/div/li/div/div/select', //Campaign type dropdown in detail page
        campaignDetailPageDeleteBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/a[2]', //Delete button in detail page
        campaignTargetContactType                       :'//option[contains(@value,"Contact")]', //Contact target type
        campaignTargetPatientType                       :'//option[contains(@value,"Patient")]', //Patient target type
        campaignNumberDiv                               :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div/span[1]/span', //Campaign number display
        campaignIcon                                    :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/div/i', //Campaign icon
        campaignNameHeading                             :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span', //Campaign name header
        campaignCancelChangesLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/a[1]', //Campaign cancel changes link
        campaignCreateCaseLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a', //Create case link in campaign detail page
        campaignCreateTaskLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a', //Create task link in campaign detail page
        campaignCreateMemoLink                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[4]/a', //Create memo link in campaign detail page

        pagingNumberOfRecordsDropdown                   :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select', //Paging number of records drop down
        pagingTotalRecords                              :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[6]/span/span', //Paging total records
        pagingRecordNumbers                             :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[3]/span', //Paging record numbers
        pagingNextButton                                :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[4]/a', //Paging next button
        pagingLastButton                                :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Paging last button
        pagingPreviousButton                            :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[2]/a', //Paging previous button
        pagingFirstButton                               :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[1]/a', //Paging first button
        pagingCurrentPageInput                          :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/input', //Paging current page
        pagingGoBtn                                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/button', //Go button in paging division

        campaignCreatePopup                             :'/html/body/div[7]/div[2]/div', //Create campaign popup
        campaignNameErrorLabel                          :'/html/body/div[4]/div[2]/div/div[2]/div[1]/form/ul/li[2]/div/label', //Campaign name required error message
        campaignTargetTypeErrorLabel                    :'/html/body/div[4]/div[2]/div/div[2]/div[1]/form/ul/li[3]/div/label', //Target type required error label

        campaignSettingsIcon                            :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        campaignCustomListEditLink                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        campaignCustomListCloneLink                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        campaignCustomListNewLink                       :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link
        campaignCustomListOrderLink                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        campaignCustomListCloneInput                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input
        campaignCustomListCloneCreateBtn                :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button
        campaignCustomListNewInput                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        campaignCustomListAvailColumns                  :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Available columns
        campaignCustomListSelectedColumns               :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Selected columns
        campaignCustomListAddColBtn                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button
        campaignCustomListRemoveColBtn                  :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button
        campaignCustomListNewSaveBtn                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button
        campaignCustomListOrderDiv                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div
        campaignCustomListOrderCloseBtn                 :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button
        campaignCustomListDeleteLink                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link
        campaignQuickFilterLink                         :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        campaignFilterAvailColumns                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        campaignFilterSelectedColumns                   :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns
        campaignFilterAddColBtn                         :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button
        campaignFilterRemoveColBtn                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button
        campaignFilterApplyBtn                          :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button

        campaignActivityTable                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Campaigns activity table in activities subtab
        campaignActivitySettingsIcon                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in activities subtab
        campaignActivityCustomListEditLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in activities subtab
        campaignActivityCustomListCloneLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in activities subtab
        campaignActivityCustomListNewLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in activities subtab
        campaignActivityCustomListOrderLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in activities subtab
        campaignActivityCustomListCloneNameInput        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in activities subtab
        campaignActivityCustomListCloneCreateBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in activities subtab
        campaignActivityCustomListNewNameInput          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input in activities subtab
        campaignActivityCustomListAvailColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in activities subtab
        campaignActivityCustomListSelectedColumns       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in activities subtab
        campaignActivityCustomListAddColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in activities subtab
        campaignActivityCustomListRemoveColBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in activities subtab
        campaignActivityCustomListNewSaveBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in activities subtab
        campaignActivityCustomListOrderDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in activities subtab
        campaignActivityCustomListOrderCloseBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in activities subtab
        campaignActivityCustomListDeleteLink            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in activities subtab
        campaignActivityQuickFilterLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', //Quick filter link in activities subtab
        campaignActivityFilterAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in activities subtab
        campaignActivityFilterSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in activities subtab
        campaignActivityFilterAddColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in activities subtab
        campaignActivityFilterRemoveColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in activities subtab
        campaignActivityFilterApplyBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in activities subtab

        campaignMessageSettingsIcon                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in messages subtab
        campaignMessageCustomListEditLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in messages subtab
        campaignMessageCustomListCloneLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in messages subtab
        campaignMessageCustomListNewLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in messages subtab
        campaignMessageCustomListOrderLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in messages subtab
        campaignMessageCustomListCloneNameInput         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list name input in messages subtab
        campaignMessageCustomListCloneCreateBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in messages subtab
        campaignMessageCustomListNewNameInput           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input in messages subtab
        campaignMessageCustomListAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in messages subtab
        campaignMessageCustomListSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in messages subtab
        campaignMessageCustomListAddColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in messages subtab
        campaignMessageCustomListRemoveColBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in messages subtab
        campaignMessageCustomListNewSaveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in messages subtab
        campaignMessageCustomListDeleteLink             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in messages subtab
        campaignMessageCustomListOrderDiv               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in messages subtab
        campaignMessageCustomListOrderCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in messages subtab
        campaignMessageQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in messages subtab
        campaignMessageFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in messages subtab
        campaignMessageFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in messages subtab
        campaignMessageFilterAddColBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in messages subtab
        campaignMessageFilterRemoveColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in messages subtab
        campaignMessageFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in messages subtab
        campaignMessageTable                            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div/div/div[2]/table', //Campaigns custom list table

        campaignAudienceSettingsIcon                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in audience subtab
        campaignAudienceCustomListEditLink              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in audience subtab
        campaignAudienceCustomListCloneLink             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in audience subtab
        campaignAudienceCustomListNewLink               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in audience subtab
        campaignAudienceCustomListOrderLink             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in audience subtab
        campaignAudienceCustomListCloneNameInput        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list name input in audience subtab
        campaignAudienceCustomListCloneCreateBtn        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button in audience subtab
        campaignAudienceCustomListNewNameInput          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input in audience subtab
        campaignAudienceCustomListAvailColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in audience subtab
        campaignAudienceCustomListSelectedColumns       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in audience subtab
        campaignAudienceCustomListAddColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in audience subtab
        campaignAudienceCustomListRemoveColBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in audience subtab
        campaignAudienceCustomListNewSaveBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in audience subtab
        campaignAudienceCustomListDeleteLink            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in audience subtab
        campaignAudienceCustomListOrderDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in audience subtab
        campaignAudienceCustomListOrderCloseBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in audience subtab
        campaignAudienceQuickFilterLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in audience subtab
        campaignAudienceFilterAvailColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in audience subtab
        campaignAudienceFilterSelectedColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in audience subtab
        campaignAudienceFilterAddColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in audience subtab
        campaignAudienceFilterRemoveColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in audience subtab
        campaignAudienceFilterApplyBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in audience subtab
        campaignAudienceTable                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Audience subtab table
        campaignCreateContactLink                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[4]/a', //Create contact link
        campaignAudienceContactFNameInput               :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[6]/div/input', //First name input in create contact pop up in audience subtab
        campaignAudienceContactLNameInput               :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[8]/div/input', //Last name input in create contact pop up in audience subtab
        campaignAudienceCreateCaseLinkInContact         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[7]/span[1]/a', //Create case link in contact record
        campaignAudienceCreateTaskLinkInContact         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[7]/span[2]/a', //Create task link in contact record
        campaignAudienceCreateMemoLinkInContact         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[7]/span[3]/a', //Create memo link in contact record
        campaignAudienceCreateOpportunityLinkInContact  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/tbody/tr/td[7]/span[4]/a', //Create opportunity link in contact record
        campaignAudienceSendMessageLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[5]/a', //Send a message to this audience link
        campaignAudienceSendMessagePopup                :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div', //Send a message to this audience pop up
        campaignAudiencePopupSendToDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[1]', //Send to div in send message to audience pop up
        campaignAudiencePopupSelectATemplateDiv         :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[2]', //Select a template div in send message to audience pop up
        campaignAudiencePopupFromDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[3]', //From div in send message to audience pop up
        campaignAudiencePopupSubjectDiv                 :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[4]', //Subject div in send message to audience pop up
        campaignAudiencePopupSendToDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[1]/div/select', //Send to dropdown in send message to pop up
        campaignAudiencePopupFromInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[3]/div/div/div/div[1]/input', //From input in send message to pop up
        campaignAudiencePopupSubjectInput               :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[4]/div/input', //Subject input  in send message to pop up
        campaignAudiencePopupMessageInput               :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[5]/div/textarea', //Message input in send message to pop up
        campaignAudiencePopupUploadAttachmentLink       :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[2]/div/div/ul/div[2]/div/ul/li[2]/a', //Upload attachment link in send message to pop up
        campaignAudiencePopupAttachmentsTable           :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div/div/table', //Attachments table in send message to pop up
        campaignAudiencePopupUploadAttachmentButton     :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[2]/div/div/ul/div[2]/div/div[1]/div/div[2]/div/div/div/form/div/div[2]/button', //Upload attachment button in upload attachment pop up
        campaignAudiencePopupUploadFileInput            :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[2]/div/div/ul/div[2]/div/div[1]/div/div[2]/div/div/div/form/div/ul/li/div/div/input',//filename input in upload attachment pop up
        campaignAudiencePopupCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[1]/a', //Send message pop up close button
        campaignAudienceMsgPopupSendBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[4]/button', //Send button in send message pop up
        campaignAudienceMsgPopupFromErrorLabel          :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[3]/div/div/div/div[1]/label', //From error label in send message pop up in audience
        campaignAudienceMsgSubjectDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[4]', //Subject div in send a message pop up in audience
        campaignAudienceMsgFromDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[3]', //From div in send a message pop up in audience
        campaignAudienceMsgSendToDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[1]', //Send to div in send a message pop up in audience
        campaignAudienceMsgSelectATemplateDiv           :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[2]', //Select a template div in send a message pop up in audience

        campaignAudiencePatientCreateLink               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[4]/a', //Create patient link in audience sub tab
        campaignAudiencePatientFNameInput               :'//*[@id="patient_firstname"]', //First name input in create patient pop up
        campaignAudiencePatientLNameInput               :'//*[@id="patient_lastname"]', //Last name input in create patient pop up
        campaignAudiencePatientCreateBtn                :'//*[@id="create_patient"]', //Create button in create patient pop up
        campaignAudiencePatientSearchInput              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[1]/input', //Patient search input box in audience subtab


        campaignMsgTemplateCreatePopupCloseBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[1]/a', //Create message template pop up close button
        campaignMsgTemplateCreateLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/a', //Create a message template link
        campaignMsgTemplateTable                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Message templates custom list
        campaignMsgTemplateNameInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[1]/div/div/ul/li[1]/div/input', //Name input in create message template pop up
        campaignMsgTemplateSubjectInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[1]/div/div/ul/li[2]/div/input', //Subject input in create message template pop up
        campaignMsgTemplateCopyFromDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[1]/div/div/ul/li[3]/div/div/select' ,//Copy from template dropdown in create message template pop up
        campaignMsgTemplateSaveBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[2]/button', //Save button in create message template pop up
        campaignMsgTemplateDetailSaveBtn                :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[1]/div/button[2]', //Message template detail page save button
        campaignMsgTemplateDetailEditBtn                :'//*[@id="actionbuttons"]/button', //Message template detail page edit button
        campaignMsgTemplateDetailNameInput              :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div/input', //Message template detail page name input
        campaignMsgTemplateDetailSubjectInput           :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[2]/div/input', //Message template detail page subject input
        campaignMsgTemplateDetailCopyFromDropdown       :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[3]/div/div/select', //Message template detail page copy from template dropdown
        campaignMsgTemplateDetailDeleteLink             :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[1]/div/button[4]', //Delete record link in message template detail page

        campaignMsgTemplateCreatePopup                  :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div', //Create message template pop up
        campaignMsgTemplateNameErrorLabel               :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[1]/div/div/ul/li[1]/div/label', //Name required field error label
        campaignMsgTemplateSubjectErrorLabel            :'/html/body/div[1]/main/div[3]/div[1]/div[8]/div[2]/div/div/div[2]/div[1]/div/div/ul/li[2]/div/label', //Subject required field  error label
        campaignMsgTemplateEmailTextInput               :'/html/body', //Email text input box in message template detail page
        campaignMsgTemplateSMSSubTab                    :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[2]/div/div/ul/div[1]/button[2]', //SMS sub tab in message template detail page
        campaignMsgTemplateAttachmentsSubTab            :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[2]/div/div/ul/div[1]/button[3]', //Attachments sub tab in message template detail page
        campaignMsgTemplateSMSTextInput                 :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[2]/div/div/div/div[2]/textarea', //SMS text input in message template detail page
        campaignMsgTemplateAttachmentLink               :'/html/body/div[1]/main/div[3]/div/div/div[1]/div[2]/div[2]/div/div/ul/div[2]/div/ul/li[2]/a', //Upload attachment link in message template detail page
        campaignMsgTemplateSettingsIcon                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in message template subtab
        campaignMsgTemplateCustomListEditLink           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in message template subtab
        campaignMsgTemplateCustomListCloneLink          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in message template subtab
        campaignMsgTemplateCustomListNewLink            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in message template subtab
        campaignMsgTemplateCustomListOrderLink          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in message template subtab
        campaignMsgTemplateCustomListCloneNameInput     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone name input in message template subtab
        campaignMsgTemplateCustomListCloneCreateBtn     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in message template subtab
        campaignMsgTemplateCustomListNewNameInput       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in message template subtab
        campaignMsgTemplateCustomListAvailColumns       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in message template subtab
        campaignMsgTemplateCustomListSelectedColumns    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in message template subtab
        campaignMsgTemplateCustomListAddColBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in message template subtab
        campaignMsgTemplateCustomListRemoveColBtn       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in message template subtab
        campaignMsgTemplateCustomListNewSaveBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in message template subtab
        campaignMsgTemplateCustomListOrderDiv           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in message template subtab
        campaignMsgTemplateCustomListOrderCloseBtn      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in message template subtab
        campaignMsgTemplateCustomListDeleteLink         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in message template subtab
        campaignMsgTemplateQuickFilterLink              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in message template subtab
        campaignMsgTemplateFilterAvailColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in message template subtab
        campaignMsgTemplateFilterSelectedColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in message template subtab
        campaignMsgTemplateFilterAddColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in message template subtab
        campaignMsgTemplateFilterRemoveColBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in message template subtab
        campaignMsgTemplateFilterApplyBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in message template subtab
        campaignMsgTemplatePagingDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div', //Pagination div in message template subtab
        campaignMsgTemplateMsgTemplateDropdown          :'/html/body/div[1]/main/div[3]/div[1]/div[10]/div[2]/div/div/div[2]/div/div/div[1]/ul/li[2]/div/select', //Message template dropdown on send message to audience pop up
        campaignTabHeaderOnCloudSearchDiv					: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[7]', // Campaign page -> Campaign header on cloud search
        campaignTabHeaderOnCloudSearch						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[7]/div[1]/ul/li[1]/div[1]/div[1]/div/span', // Campaign search on Campaign page cloud search
        campaignNumberCloudSearch							: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div/span[1]/span', // Campaign page having a campaign number span
        campaignName										: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span', // Campaign page having campaign name on top of page
        campaignSearchField									: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[1]/input', // Campaign page having a campaign cloud search field
        campaignCaseIconOnCloudSearch						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[7]/div[1]/ul/li/div[1]/div[2]/div/ul/li[1]/span/i',// Campaign cloud search link have contain the Case icon
        campaignTaskIconOnCloudSearch						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[7]/div[1]/ul/li/div[1]/div[2]/div/ul/li[2]/span/i',// Campaign cloud search link have contain the Task icon
        campaignMemoIconOnCloudSearch						: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[7]/div[1]/ul/li/div[1]/div[2]/div/ul/li[3]/span/i',// Campaign cloud search link have contain the Memo icon

    };
module.exports.campaignPageElements = campaignPageElements;
//CAMPAIGN PAGE ELEMENTS - END



//REPORTS PAGE ELEMENTS - START
var reportsPageElements =
    {
        reportsTabLink                                  :'//*[@id="site_nav"]/ul/li[8]/a/span', //Reports tab on home page
        reportsPageHeader                               :'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //Reports header tab on home page
    };
module.exports.reportsPageElements = reportsPageElements;
//REPORTS PAGE ELEMENTS - END



//ORDERS AND PATIENTS PAGE ELEMENTS - START
var ordersAndPatientsPageElements =
    {
        ordersAndPatientsTabLink                        :'/html/body/div[1]/header/div[2]/div/nav/ul/li[9]/a',  //Orders and Patients tab on dashboard
        ordersPageHeader                                :'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1', //Orders header tab on dashboard
        detailPageRecordHeader                          :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span', //Record header in orders and patients page
        newPatientCreateBtn                             : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[2]/ul/li/a', //Create patient link in orders and patients page
        patientTabLink                                  :'/html/body/div[1]/main/div[3]/div/div[1]/div[1]', //Patient tab link in orders and patients page
        createPatientLink                               :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[2]/ul/li/a', //Create patient link in orders and patients page
        patientSalutationDropdown                       :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[1]/div/div/select', //Salutation dropdown in create patient pop up
        patientFirstNameInput                           :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[2]/div/input', //First name input in patient create pop up
        patientMiddleNameInput                          :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[3]/div/input', //Middle name input in patient create pop up
        patientLastNameInput                            :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[4]/div/input', //Last name input in patient create pop up
        patientSuffixInput                              :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[5]/div/input', //Suffix input in patient create pop up
        patientEmailInput                               :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[6]/div/input', //Email input in  patient create pop up
        patientMobileInput                              :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[7]/div/input', //Mobile input in patient create pop up
        patientOrgInput                                 :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[8]/div/input', //Organization input in patient create pop up
        patientHostCodeInput                            :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[9]/table/tbody/tr[2]/td[2]/div/div/div[1]/input', //Host code input for hc1test interface in create patient pop up
        patientCreateBtn                                :'/html/body/div[4]/div[2]/div/div[5]/button', //Create button in patient create pop up
        patientSaveBtn                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Save button in patient detail page
        patientCancelBtn                                :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Cancel button in patient detail page
        patientSearchFNameInput                         :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/input', //Firstname input box in patient search
        patientSearchBtn                                :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[2]/button[1]', //Search button in orders and patients page
        patientsTable                                   :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[1]/table', //Patients table list
        orderSearchTableListSSNInput                    : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[3]/div/input', //Order search table list ssn input fields
        orderSearchTableListMRNInput                    : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[4]/div/input', //Order search table list MRN input fields
        orderSearchTableListDOBInput                    : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[5]/div/input', //Order search table list DATE OF BIRTH input fields
        orderSearchTableListOrderNumberInput            : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[6]/div/input', //Order search table list ORDER NUMBER input fields
        orderSearchTableListSpecimenIdInput             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[7]/div/input', //Order search table list SPECIMEN ID input fields
        orderSearchTableListOrderingProviderInput       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[8]/div/div/div/div[1]/input', //Order search table list ORDERING PROVIDER input fields
        orderSearchTableListOrderingLocationInput       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[9]/div/div/div/div[1]/input', //Order search table list ORDERING LOCATION input fields

        orderSearchTableListPanelNameInput              : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/input', //Order search table list -> Additional information PANEL NAME input fields
        orderSearchTableListOrderDateInput              : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[2]/div/div[1]/div/input', //Order search table list -> Additional information ORDER DATE input fields
        orderSearchTableListResultDateInput             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[3]/div/div[1]/div/input', //Order search table list -> Additional information RESULT DATE input fields
        orderSearchTableListAccessionedDateInput        : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[4]/div/div[1]/div/input', //Order search table list -> Additional information ACCESSIONED DATE input fields
        orderSearchTableListLastNameInput               :'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[2]/div/input', //Order search table list last name input
        orderSearchBtn                                  : '/html/body/div[1]/main/div[3]/div/div[1]/div[2]', // Order Search tab on order and patient page
        orderSearchTableListFirstName                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[1]/label', //Order search table list first name
        orderSearchTableSearchBtn                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[2]/button[1]',     //Order and patients search search button
        orderSearchTableListLastName                    : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[2]/label', //Order search table list LAST NAME
        orderSearchTableListSSN                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[3]/label', //Order search table list ssn
        orderSearchTableListMRN                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[4]/label', //Order search table list MRN
        orderSearchTableListDOB                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[5]/label', //Order search table list DATE OF BIRTH
        orderSearchTableListOrderNumber                 : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[6]/label', //Order search table list ORDER NUMBER
        orderSearchTableListSpecimenId                  : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[7]/label', //Order search table list SPECIMEN ID
        orderSearchTableListOrderingProvider            : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[8]/label', //Order search table list ORDERING PROVIDER
        orderSearchTableListOrderingLocation            : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[9]/label', //Order search table list ORDERING LOCATION

        orderSearchTableListPanelName                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[1]/label', //Order search table list -> Additional information PANEL NAME
        orderSearchTableListOrderDate                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[2]/label', //Order search table list -> Additional information ORDER DATE
        orderSearchTableListResultDate                  : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[3]/label', //Order search table list -> Additional information RESULT DATE
        orderSearchTableListAccessionedDate             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[4]/label', //Order search table list -> Additional information ACCESSIONED DATE

        orderSearchDobDiv                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[5]/div/div', //Order and patients page (ORDER SEARCH) search div
        orderSearchOrderDateDiv                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[2]/div/div[1]/div/div', //Order and patients page (ORDER DATE) search div
        orderSearchResultDateDiv                        : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[3]/div/div[1]/div/div', //Order and patients page (RESULT DATE) search div
        orderSearchAccessionedDateDiv                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[4]/div/div[1]/div/div',//Order and patients page (ACCESSIONED DATE) search div
        orderAndPatientsOrderingProviderSearchIcon      : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[8]/div/div/div/div[1]/input',//Order and patients page (ORDERING PROVIDER) search div
        orderAndPatientsOrderingLocationSearchIcon      : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[9]/div/div/div/div[1]/input', //Order and patients page (ORDERING LOCATION) search div
        orderAndPatientsPanelNameSearchIcon             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/input',//Order and patients page (PANEL NAME) search div
        orderAndPatientTableListOfColumn                : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/thead',//Table list of Order and Patients column
        caseSaveAndBackBtn                              : '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and back button on Case edited page
        orderCaseIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[2]/a/i', //Case icon on Order table list
        orderTaskIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[3]/a/i',//Task icon on Order table list
        orderMemoIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[4]/a/i',//Memo icon on Order table list
        orderPageResetBtn                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[2]/button[2]', //Reset button on Order and patient page
        ordersPagePagingDiv                             : '//*[@id="search_paging"]/ul/li[5]/a/i', //Order and patients detail page orders sub tab paging div
        orderErrorMsgOnFirstName                        : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[1]/div/label', //Order and patient page message print on first name label

        patientSearchTab                                : '/html/body/div[1]/main/div[3]/div/div[1]/div[1]', //Patients search tab on Order and patients
        patientSearchFNameInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/input', //First name input on Patients search tab
        patientSearchLNameInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input', //Last name input on Patients search tab
        patientSearchSSNInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[3]/div/input', //SSN input on Patients search tab
        patientSearchMRNInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[4]/div/input',//MRN input on Patients search tab
        patientSearchDOBInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[5]/div/div',//DOB input on Patients search tab
        patientBtn                                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[2]/ul/li/a', //Patients button on patients search page
        patientsPopWindowCloseBtn                       : '/html/body/div[4]/div[2]/div/div[1]/a/i',
        patientsSearchBtn                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[2]/button[1]',
        patientErrorMsgOnFirstName                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/label', //Order and patient page message print on first name label


        orderPageResetBtn                     		    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[2]/button[2]', //Reset button on Order and patient page
        patientsSearchTableList                 	    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[1]/table',
        patientNextArrow                        	    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[2]/ul/li[4]/a/i',//next page icon on patients page
        patientsOrganizationInput                       : '/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[8]/div/input', //Organization field in new patients creation pop page
        newPatientsCreateBtn                            : '/html/body/div[5]/div[2]/div/div[5]/button', // Create button on new patients creation pop page
        patientEditBtn                                  :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Patient detail page edit button
        patientFNameErrorMsg                            : '//*[@id="create_patient_form"]/ul/li[2]/div/label', //First name error message on patients creation page
        patientLNameErrorMsg                            : '//*[@id="create_patient_form"]/ul/li[4]/div/label',//'/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[4]/div/label',//Last Name error message on patients creation page
        patientOrgErrorMsg                              : '//*[@id="create_patient_form"]/ul/li[8]/div/label',//'/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[8]/div/label',//Organization error message on patients creation page
        patientSalutationDrpDownMenu                    : '//*[@id="patient_salutation"]',// Salutation drop down menu on creating new patients page
        patientsCreateNewOrgBtn                         : '//*[@id="footer_button"]', //Create new org button on new patients creation
        patientsCreateOrgBtn                            : '//*[@id="create_org"]',//Create org button -> organization creation on patients page
        patientsSalutationMrOption                      : "//option[contains(@value,'Mr.')]", //Option menu  (Mr Salutation drop down list) on Create new Patient page
        patientsSalutationMrsOption                     : "//option[contains(@value,'Mrs.')]", //Option menu (Mrs Salutation drop down list) on Create new Patient page
        patientsSalutationMsOption                      : "//option[contains(@value,'Ms.')]", //Option menu (Ms Salutation drop down list) on Create new Patient page
        patientsSalutationDrOption                      : "//option[contains(@value,'Dr.')]", //Option menu (Dr Salutation drop down list) on Create new Patient page
        patientAddressSubTab                            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[2]', //Address subTab in patient detail page
        patientAddressMailingStreet1Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[1]/div/input', //Mailing street1 input box in address subtab patient detail page
        patientAddressMailingStreet2Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[2]/div/input', //Mailing street2 input box in address subtab patient detail page
        patientAddressMailingCityInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[3]/div/input', //Mailing address city input in address subtab patient detail page
        patientAddressMailingStateInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[4]/div/input', //Mailing address state input in address subtab patient detail page
        patientAddressMailingPostalInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[5]/div/input', //Mailing address postal input in address subtab patient detail page
        patientAddressMailingCountryInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div/div/ul/li[6]/div/input', //Mailing address country input in address subtab patient detail page
        patientAddressBillingStreetInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[1]/div/input', //Billing street1 input in address subtab patient detail page
        patientAddressBillingStreet2Input               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[2]/div/input', //Billing street2 input in address subtab patient detail page
        patientAddressBillingCityInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[3]/div/input', //Billing city input in address subtab patient detail page
        patientAddressBillingStateInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[4]/div/input', //Billing state input in address subtab patient detail page
        patientAddressBillingPostalInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[4]/div/input', //Billing postal input in address subtab patient detail page
        patientAddressBillingCountryInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/ul/li[6]/div/input', //Billing country input in address subtab patient detail page

        patientOrgTabNumberOfRecordsDropdown            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select', //Number of records dropdown in patient organization tab
        patientOrgTabTotalRecords                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[6]/span/span', //Total records
        patientOrgTabPagingRecordNumbers                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[3]/span', //Paging record numbers
        patientOrgPagingNextBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[4]/a', //Next button
        patientOrgPagingLastBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Last button
        patientOrgPagingPrevBtn                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[2]/a', //Previous button
        patientOrgPagingFirstBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[1]/a', //First button
        patientOrgPagingCurrentPageInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[7]/span/input', //Page input

        patientOrgCreatePopupOrgNameInput               :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li/div/input', //Organization name input in create org pop up from org subtab
        patientOrgTabSettingsIcon                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in patient detail page organization subtab
        patientOrgTabEditCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in patient detail page organization subtab
        patientOrgTabCloneCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in patient detail page organization subtab
        patientOrgTabNewCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in patient detail page organization subtab
        patientOrgTabOrderCustomListLink                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in patient detail page organization subtab
        patientOrgTabCloneCustomListInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input in patient detail page organization subtab
        patientOrgTabCloneCreateBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in patient detail page organization subtab
        patientOrgTabNewCustomListInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in patient detail page organization subtab
        patientOrgTabCustomListAvailColumns             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in patient detail page organization subtab
        patientOrgTabCustomListSelectedColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in patient detail page organization subtab
        patientOrgTabCustomListAddColBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Add column button in patient detail page organization subtab
        patientOrgTabCustomListRemoveColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Remove column button in patient detail page organization subtab
        patientOrgTabNewCustomListSaveBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in patient detail page organization subtab
        patientOrgTabOrderCustomLists                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom lists in patient detail page organization subtab
        patientOrgTabOrderCustomListCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in patient detail page organization subtab
        patientOrgTabCustomListDeleteLink               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link in patient detail page organization subtab
        patientOrgTabTable                              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table', //Organization custom list table  in patient detail page organization subtab
        patientOrgCustomListAddSortConditionBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/a[2]', //Add sort condition button in custom list
        patientOrgCustomListSortByField                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[2]/tbody/tr/td[1]/div/select', //Sort by field drop down in custom list
        patientOrgCustomListSortByOrder                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[2]/tbody/tr/td[2]/div/select', //Sort by order drop down in custom list
        patientOrgCustomListSortByOrganizationOption    :"//option[contains(.,'Organization')]", //Sort by organization option in sort by field dropdown in custom list
        patientOrgTableOrgColumnHeader                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table/thead/tr/th[3]', //Organization column header
        patientOrgCustomListOrderBtnDown                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[2]', //Custom list order button down
        patientOrgTabSearchOrgInput                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[2]/div/div/div[1]/input', //Organization search box
        patientOrgTabCreateOrgLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[1]/div/div/ul/li[4]/a', //Create new org link
        patientOrgTabCreateOrgPopup                     :'/html/body/div[4]/div[2]/div/div[2]', //Create new org pop up

        patientOrgTabQuickFilterLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link  in patient detail page organization subtab
        patientOrgTabFilterAvailColumns                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns  in patient detail page organization subtab
        patientOrgTabFilterSelectedColumns              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns  in patient detail page organization subtab
        patientOrgTabFilterAddColBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button  in patient detail page organization subtab
        patientOrgTabFilterRemoveColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button  in patient detail page organization subtab
        patientOrgTabFilterApplyBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button  in patient detail page organization subtab
        patientOrgTabFilterByDropdown                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Filter by dropdown in filter
        patientOrgTabFilterOperatorDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Operator dropdown in filter
        patientOrgTabFilterValueInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/input', //Value input box in filter

        patientOrgTab                                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[4]', //Patient organization tab


        orderSearchBtn                                  : '/html/body/div[1]/main/div[3]/div/div[1]/div[2]', // Order Search tab on order and patient page
        orderSearchTableListFirstName                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[1]/label', //Order search table list FIRST NAME
        orderSearchTableListFirstNameInput              : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[1]/div/input',//Order search table list FIRST NAME input fields
        orderSearchTableSearchBtn                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[2]/button[1]',     //Order and patients search search button
        orderSearchTableListLastNameInput                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[2]/div/input', //Order search table list LAST NAME input fields
        orderSearchTableListSSNInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[3]/div/input', //Order search table list ssn input fields
        orderSearchTableListMRNInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[4]/div/input', //Order search table list MRN input fields
        orderSearchTableListDOBInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[5]/div/input', //Order search table list DATE OF BIRTH input fields
        orderSearchTableListOrderNumberInput                 : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[6]/div/input', //Order search table list ORDER NUMBER input fields
        orderSearchTableListSpecimenIdInput                  : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[7]/div/input', //Order search table list SPECIMEN ID input fields
        orderSearchTableListOrderingProviderInput            : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[8]/div/div/div/div[1]/input', //Order search table list ORDERING PROVIDER input fields
        orderSearchTableListOrderingLocationInput            : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[9]/div/div/div/div[1]/input', //Order search table list ORDERING LOCATION input fields

        orderSearchTableListPanelNameInput                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/input', //Order search table list -> Additional information PANEL NAME input fields
        orderSearchTableListOrderDateInput                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[2]/div/div[1]/div/input', //Order search table list -> Additional information ORDER DATE input fields
        orderSearchTableListResultDateInput                  : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[3]/div/div[1]/div/input', //Order search table list -> Additional information RESULT DATE input fields
        orderSearchTableListAccessionedDateInput             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[4]/div/div[1]/div/input', //Order search table list -> Additional information ACCESSIONED DATE input fields

        orderSearchDobDiv                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[5]/div/div', //Order and patients page (ORDER SEARCH) search div
        orderSearchOrderDateDiv                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[2]/div/div[1]/div/div', //Order and patients page (ORDER DATE) search div
        orderSearchResultDateDiv                        : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[3]/div/div[1]/div/div', //Order and patients page (RESULT DATE) search div
        orderSearchAccessionedDateDiv                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[4]/div/div[1]/div/div',//Order and patients page (ACCESSIONED DATE) search div
        orderAndPatientsOrderingProviderSearchIcon      : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[8]/div/div/div/div[1]/input',//Order and patients page (ORDERING PROVIDER) search div
        orderAndPatientsOrderingLocationSearchIcon      : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[9]/div/div/div/div[1]/input', //Order and patients page (ORDERING LOCATION) search div
        orderAndPatientsPanelNameSearchIcon             : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/input',//Order and patients page (PANEL NAME) search div
        orderAndPatientTableListOfColumn                : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/thead',//Table list of Order and Patients column
        caseSaveAndBackBtn                              : '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and back button on Case edited page
        orderCaseIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[2]/a/i', //Case icon on Order table list
        orderTaskIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[3]/a/i',//Task icon on Order table list
        orderMemoIconOnOrderTable                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/div/div/div[1]/table/tbody/tr[1]/td[12]/span[4]/a/i',//Memo icon on Order table list
        orderPageResetBtn                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[2]/button[2]', //Reset button on Order and patient page
        ordersPagePagingDiv                             : '//*[@id="search_paging"]/ul/li[5]/a/i', //Order and patients detail page orders sub tab paging div
        orderErrorMsgOnFirstName                        : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/form/div[1]/div[1]/ul/li[1]/div/label', //Order and patient page message print on first name label

        patientSearchTab                                : '/html/body/div[1]/main/div[3]/div/div[1]/div[1]', //Patients search tab on Order and patients
        patientSearchFNameInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/input', //First name input on Patients search tab
        patientSearchLNameInput                         : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input', //Last name input on Patients search tab
        patientSearchSSNInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[3]/div/input', //SSN input on Patients search tab
        patientSearchMRNInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[4]/div/input',//MRN input on Patients search tab
        patientSearchDOBInput                           : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[5]/div/div',//DOB input on Patients search tab
        patientBtn                                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[2]/ul/li/a', //Patients button on patients search page
        patientsSearchBtn                               : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[2]/button[1]',
        patientErrorMsgOnFirstName                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/label', //Order and patient page message print on first name label

        orderPageResetBtn                     		    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[2]/button[2]', //Reset button on Order and patient page
        patientNextArrow                        	    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[2]/ul/li[4]/a/i',//next page icon on patients page
        patientsOrganizationInput                        : '/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[8]/div/input', //Organization field in new patients creation pop page
        newPatientsCreateBtn                            : '/html/body/div[5]/div[2]/div/div[5]/button', // Create button on new patients creation pop page
        patientFNameErrorMsg                            : '//*[@id="create_patient_form"]/ul/li[2]/div/label', //First name error message on patients creation page
        patientLNameErrorMsg                            : '//*[@id="create_patient_form"]/ul/li[4]/div/label',//'/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[4]/div/label',//Last Name error message on patients creation page
        patientOrgErrorMsg                              : '//*[@id="create_patient_form"]/ul/li[8]/div/label',//'/html/body/div[5]/div[2]/div/div[2]/div/form/ul/li[8]/div/label',//Organization error message on patients creation page
        patientSalutationDrpDownMenu                    : '//*[@id="patient_salutation"]',// Salutation drop down menu on creating new patients page
        patientsCreateNewOrgBtn                         : '//*[@id="footer_button"]', //Create new org button on new patients creation
        patientsCreateOrgBtn                            : '//*[@id="create_org"]',//Create org button -> organization creation on patients page
        patientsSaveAndBackBtn                          : '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Save and back button on the creation of new patients page
        patientsSalutationMrOption                      : "//option[contains(@value,'Mr.')]", //Option menu  (Mr Salutation drop down list) on Create new Patient page
        patientsSalutationMrsOption                     : "//option[contains(@value,'Mrs.')]", //Option menu (Mrs Salutation drop down list) on Create new Patient page
        patientsSalutationMsOption                      : "//option[contains(@value,'Ms.')]", //Option menu (Ms Salutation drop down list) on Create new Patient page
        patientsSalutationDrOption                      : "//option[contains(@value,'Dr.')]", //Option menu (Dr Salutation drop down list) on Create new Patient page


        orderListTab									:'/html/body/div[1]/main/div[3]/div/div[1]/div[3]', //Order list tab on order and patients home page
        patientsListDateRangeMenu						:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div[1]/div[1]/div/select', //List Date Range drop down menu on Order list page
        patientsLDROrderDateOption						:"//option[contains(@value,'orderDate')]", //LDR = List Date Range     //Option menu (Order date drop down list) on Order list page
        patientsLDRAccessionedDateOption				:"//option[contains(@value,'accessionedDate')]", //LDR = List Date Range     //Option menu (Order date drop down list) on order list page
        patientsLDRResultDateOption						:"//option[contains(@value,'resultDate')]", //LDR = List Date Range     //Option menu (Order date drop down list) on Order list page
        patientsLDRCollectionDateOption					:"//option[contains(@value,'collectionDate')]", //LDR = List Date Range     //Option menu (Order date drop down list) on order list page
        orderStartDateDiv								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div[1]/div[2]/div/div',//Order date date selector drop down menu on order list
        orderRangeSlideInput							:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div[1]/div[3]/div/input',//Range slide input in order list page
        orderSettingsIconBtn							:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/a/i',
        cloneCustomListBtn								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Order and patients clone custom list button on order list page
        newCustomListBtn								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //Order and patients new custom list button on order list page
        orderCustomListBtn								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order and patients order custom list button on order list page
        orderQuickFilterBtn								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[2]/a', //Order quick filter tab
        orderAndPatientsTable							: '/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[2]/table',   //Table of order and patients list on order and patients page
        orderFiltersByColumnList						:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select',// Filters by column on quick filter table on order list page
        valueColumnListOption							: '/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[1]/select', // Value column on quick filter table on order list page
        operatorColumnList								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select',//Operator column list on Quick filter page
        patientsEqualsOperatorOption					: "//option[contains(@value,'EQ')]", //Equal operator drop down list on quick filter (order list page)
        patientsNEQOperatorOption						: "//option[contains(@value,'NEQ')]", //Does Not Equal operator drop down list on quick filter (order list page)
        patientsGTOperatorOption						: "//option[contains(@value,'GT')]",//Greater Than operator drop down list on quick filter (order list page)
        patientsGTEOperatorOption						: "//option[contains(@value,'GTE')]",//Greater Than or Equal operator drop down list on quick filter (order list page)
        patientsLTOperatorOption						: "//option[contains(@value,'LT')]",//Less Than operator drop down list on quick filter (order list page)
        patientsISNULLOperatorOption					: "//option[contains(@value,'ISNULL')]",//IsNull operator drop down list on quick filter (order list page)
        patientsISNOTNULLOperatorOption					: "//option[contains(@value,'ISNOTNULL')]",//IsNotNull operator drop down list on quick filter (order list page)
        staticValueDropDownOption						:"//option[contains(@value,'static')]", //Static element option in value drop down list on quick filter page
        relativeValueDropDownOption						:"//option[contains(@value,'static')]", //Relative element option in value drop down list on quick filter page

        scheduleCollectionFilterByElementOption			:"//option[contains(@value,'scheduledCollectionDate')]", //Schedule collection date drop down option menu in filter by column
        scheduleCollectionFilterByElementOption			:"//option[contains(@value,'scheduledCollectionDate')]", //Schedule collection date drop down option menu in filter by column
        dateSelectorOnValueField						:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[2]/div/div',
        applyBtnOrderQuickFilterPage					:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[3]/a[1]',//Apply button on the quick filter page
        resetBtnOnQuickFilterPage						:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Reset button on the quick filter page
        orderFilterAvailColumns							:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Available Columns list on quick filter page(order page)
        orderFilterSelectedColumns						:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter by drop down option on order page
        orderFilterAddBtn								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Add button on quick filter page(order list)
        orderFilterRemoveBtn							:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove button on quick filter page(order list)
        orderDateOptionInLDR							:"//option[contains(@value,'orderDate')]",  //Order list date range drop down menu on order list page
        startDateInputField								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div[1]/div[2]/div/input',   //Start date input field on the order list page
        orderRecordTable								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[2]/table', //Order record table on order list page
        rangeMoveOrderList								:'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div[1]/div[3]/div/input', //Order page range arrow movement


        generalInfoSubTab                               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[1]', //General information table on order and patients page
        generalInfoSubTabChrome                         :'//*[@id="generalTab"]',//General information Sub tab on order and patients page
        customlistTabTableOnGeneralInfoPage             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]', //Custom list Tab on general inforamtion page(Order list)
        editBtnOrderAndPatientsPage                     :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Edit sbutton on the order and patients page
        firstNameInput                                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input', //First name  on Edit page (Order and patients page)
        middleNameInput                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/input', //Middle name  on Edit page (Order and patients page)
        phoneNumberInput                                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/input', //Phone number on Edit page (Order and patients page)
        orderAndPatientsSalutationDrpDownMenu           : "//select[contains(@id,'salutation')]",// Salutation drop down menu on edited order and patients page
        editedSalutationMrOption                        :"//option[contains(@value,'Mr')]", //Option menu  (Mr Salutation drop down list) on edited order and patients page
        editedSalutationMrsOption                       :"//option[contains(@value,'Mrs')]",//Option menu  (Mrs Salutation drop down list) on edited order and patients page
        editedSalutationMsOption                        :"//option[contains(@value,'Ms')]", //Option menu  (Ms Salutation drop down list) on edited order and patients page
        editedSalutationDrOption                        :"//option[contains(@value,'Dr')]", //Option menu  (Dr Salutation drop down list) on edited order and patients page
        communicationPreferencesBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[3]', // Communication Preferences button on the order and patients edited page
        doNotCallToggle                                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/label', //Do not call toggle button on the order and patients (General information page)
        emailOptOutToggle                               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/label', //Email opt out toggle button on the order and patients (General information page)
        textOptOutToggle                                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/label', //Text Opt Out toggle button on the order and patients (General information page)
        campaignEmailOptOutToggle                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/label', //Campaign Email Opt Out toggle button on the order and patients (General information page)
        campaignTextOptOutToggle                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/label', //Campaign Text Opt Out toggle button on the order and patients (General information page)
        saveBtnOrderAndPatients                         :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', // Order and patients page save button (General information)
        patientFNameErrorLabel                          :'html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/label', //Patient first name error label

        closeIcon                                       :'/html/body/div[1]/div/div[1]/div[2]/div/div[1]/a/i', //Close icon on the Order and Patients page
        settingsIconTopOfPage                           :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/i',//Settings icon on top of page of Order and patients(General information)
        hostCodeInput                                   :'/html/body/div[4]/div[2]/div/div[2]/div/div/table/tbody/tr[1]/td[2]/div/div/div[1]/input',// Host code input on the order and patients(General information)
        hc1TestCodeInput                                :'/html/body/div[4]/div/div/div[2]/div/div/table/tbody/tr[2]/td[2]/div/div/div[1]/input',// Hc1 test code input on the order and patients(General information)
        saveBtnOnSettingsIconPage                       :'/html/body/div[4]/div/div/div[5]/button',// Save button on setting icon pop page -> order and patients(General information)
        closeIconBtn                                    : '/html/body/div[4]/div/div/div[1]/a/i',//Close button on the host code enter page(General information page)
        hostCodeSpan                                    :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[2]/span',
        editBtn                                         :'/html/body/div[4]/div[2]/div/div[5]/button',// Edit button on the host code creation page(General information page)
        closeBtnOnSaveChangesPopWindow                  :'/html/body/div[4]/div[2]/div/div[1]/button,',// Close button on the pop window (When no page is not save)
        orgLinkOnPatientsPage                           :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[1]/span/span/a', //Order detail page having organization link


        orderAndPatientsActivitySubTab                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]/button',//Activities button on order and patients(General info)
        orderAndPatientsOrderSubTab                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Orders button on order and patients(General info)
        orderAndPatientsMessagesSubTab                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]/button',//Message button on order and patients(General info)
        orderAndPatientsCampaignsSubTab                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[4]/button',//Campaigns button on order and patients(General info)

        orderSettingIcon                                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a/i', //Order and Patients page setting icon
        orderPageCustomListNewLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Order and Patients detail page messages sub tab new custom list link
        orderPageCustomListOrderLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order and patients detail page messages sub tab new custom list link
        orderPageNewListInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order and Patients detail page messages sub tab new custom list input
        orderPageSaveColBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]',////Order and Patients Custom list Save button
        orderCustomListOrderCloseBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order and Patients page custom list order close button
        orderPageOrderListDiv                           :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order and Patients page order list div
        orderPageOrderAddColumnBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]',//Order and Patients custom list Add button on "Create new customlist pop window"
        orderPageOrderRemoveColBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',//Order and Patients custom list Remove button on "Create new customlist pop window"
        orderPageOrderAvailableColumns                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Order and Patients page available columns in custom list
        orderPageOrderSelectedColumns                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul',//Order and Patients page available columns in custom list
        orderPageTableList                              :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div/div/div/div[1]/table',//Order and Patients page  table list

        orderPageCloneListInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input',//Order detail page order sub tab clone custom list link
        contactPageCustomListCloneLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Order detail page order sub tab clone custom list link

        orderCustomListCloneCreateBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]',//Order detail page order sub tab clone custom list link having create button
        contactCustomListOrderCloseBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order detail page order sub tab clone custom list link having close button
        orderPageCustomListEditLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]',//Order detail page order sub tab edit custom list link
        orderPageNewListInput                           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order detail page order sub tab edit custom list link
        orderOrgTable                                   :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[2]/table',//Custom list organization table in organization subtab
        orderOrgCustomListNewLink                       :'//*[@id="new_list_btn"]',       //New lsit link on general info sub tab in order page
        orderOrgCustomListAddColBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]',//Custom list add column button in organization subtab
        orderOrgCustomListRemoveColBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',//Custom list remove column button in organization subtab
        orderQuickFilterLink                            :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[2]/a',  //QuickFilter link on genral info sub tab in order page
        filtersByColumnList                             : '/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select',// Filters by column on quick filter table on order list page
        orderListTableList                              : '/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[2]/table',//Order detail page order table list
        orderQuickFilterAvailableColumn                 :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul',//Order detail page order sub tab custom list available columns
        orderQuickFilterSelectedColumn                  :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul',//Order detail page order sub tab custom list selected columns
        orderQuickFilterAddColBtn                       :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Order detail page order sub tab add button
        orderQuickFilterRemoveColBtn                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]',//Order detail page order sub tab remove button
        orderQuickFilterApplyBtn                        :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Order detail page order sub tab apply button

        orderListSettingsIcon                           :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/a/i',//Order detail page order list settings icon
        orderListCustomListNewLink                      :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Order detail page order list new custom list link
        orderListCustomListOrderLink                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page order list order list link
        orderListNewListInput                           :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order detail page order list -> new list input
        orderListCustomListSaveColBtn                   :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]',//Order detail page order list save button on new list pop window page
        orderListCustomListOrderListDiv                 :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div',//Order detail page order list custom list div
        orderListCustomListOrderCloseBtn                :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[4]/div[2]/div/div/a/i',//Order detail page order list close button on new list pop window page
        orderListCustomListOrderAddBtn                  :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]',//Order detail page order list add button on new list pop window page
        orderListCustomListOrderRemoveBtn               :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',//Order detail page order list remove button on new list pop window page
        orderListCustomListAvailableColumns             :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul',//Order detail page order list availlable column on new list crete pop window page
        orderListCustomListSelectedColumns              :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul',//Order detail page order list selected column on new list crete pop window page


        orderListCustomListCloneInput                   :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input',//Order detail page order list custom list clone input
        orderListCustomListCloneLink                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]',//Order detail page order list having custom list clone link
        orderListCustomListCloneCreateBtn               :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]',//Order detail page order list having create button on the clone cretae pop window page
        orderListCustomListOrderLink                    :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page order list custom list order link
        orderListCustomListOrderDiv                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div',//Order detail page order list custom list dive
        orderListCustomListOrderCloseBtn                :'/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[2]/div/div[2]/div/div/div[4]/div[2]/div/div/a/i',//Order detail page order list having close button on the order create page

        orderCloneCustomListInput                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input on pop window -> clone link (Order page)
        orderCloneCustomListLink                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Custom list link on pop window -> clone link (Order page)
        orderCloneCustomListCreateBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Custom list create button on pop window clone link (Order page)
        orderOrderCustomListLink                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Custom list link on pop window (Order page)
        orderOrderCustomListDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Custom list Div on pop window (Order page)
        orderOrderCustomListCloseBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/div/a/i',  // Close icon on pop window of order page
        quickFilterLinkOnGeneralInfoSubTab              : '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', //QuickFilter link on genral info sub tab in order page
        operatorDropDownList                            : '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Operator drop down list on quick filte page
        patientActivityFiltersByDropdown                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select',// Filters by column on quick filter table on order list page

        activityNumberFilterByElementOption             : "//option[contains(@value,'activityNumber')]",//Activity number Filters by column on quick filter table on order list page

        orderActivityFilterAvailColumns                   		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Available columns
        orderActivityTable                                		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Activity table in activities sub tab
        orderActivityFilterSelectedColumns                		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Selected columns
        orderActivityFilterAddColBtn                      		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button
        orderActivityFilterRemoveColBtn                   		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button
        orderActivityFilterApplyBtn                       		: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button
        orderSaveBtnActivityOrderSubTab                   		:'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]',      //Order list save button

        caseLinkOrderPage                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a',//Case object link (Order and patients page)
        taskLinkOrderPage                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a',//Task object link (Order and patients page)
        memoLinkOrderPage                               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[4]/a',//Memo object  link (Order and patients page)

        caseSubjectInputField                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',//case object subject input field in (Order and patients page)
        taskSubjectInputField                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',//Task object  Subject input field in (Order and patients page)
        memoSubjectInputField                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/input',//Memo object  Subject input field in (Order and patients page)
        memoSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and back button on Memo edited page
        taskSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]',//Save and back button on Task edited page

        caseSaveAndBackBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]',//Save and back button on Case edited page

        orderActivityPagingDiv                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div',//Paging div in attachments subTab
        rowsPerPageCount                                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[3]/div/div/ul/li[8]/span/div/select/option[4]',//Rows per page count on order page

    orderActivityFilterAvailColumns			: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Available columns
    orderActivityTable					: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Activity table in activities sub tab
    orderActivityFilterSelectedColumns			: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Selected columns
    orderActivityFilterAddColBtn			: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button
    orderActivityFilterRemoveColBtn			: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button
    orderActivityFilterApplyBtn				: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button
    orderSaveBtnActivityOrderSubTab			: '/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]',
    //--------------------------------------Order Sub Tab --------------------------------------------
        orderSubTabOnOrderPage                          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Order sub tab on order and patients page
        orderSubTabCustomListNewLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Order detail page order sub tab new custom list link
        orderSubTabCustomListOrderLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page order sub tab order custom list link
        orderSubTabNewListInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order detail page order sub tab new custom list input
        orderSubTabCustomListSaveColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Order detail page order sub tab custom list save button
        orderSubTabCustomListOrderListDiv               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order detail page order sub tab page order list
        orderSubTabCustomListOrderCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order page sub tab order custom list close button
        orderSubTabCustomListOrderAddBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]',//Order detail page order  sub tab custom list add button
        orderSubTabCustomListOrderRemoveBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',//Order detail page order sub tab custom list remove button
        orderSubTabCustomListAvailableColumns           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul',//Order detail page order sub tab custom list available columns
        orderSubTabCustomListSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul',//Order detail page order  sub tab custom list selected columns
        orderSubTabCustomListTable                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[2]/table',//Order detail page order  sub tab records table
        orderSubTabSettingsIcon                         :'//*[@id="customlist_views"]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a',// Order detail page sub tab settings icon
        orderSubTabCloneLink                            :"/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]", //Clone link
        orderSubTabCloneInput                           :"/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input", //Clone name input
        orderSubTabCloneCreateBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //clone create btn
        quickFilterLinkOnOrderSubTab                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[2]/a',//Order detail page Quick filter link on order subTab

        orderOrgCustomListCloneInput                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input',//Order detail page order sub tab clone custom list input
        orderOrgCustomListCloneLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]',//Order detail page order sub tab clone custom list link
        orderOrgCustomListCloneCreateBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]',  //Order detail page order  sub tab clone custom list create button
        orderOrgCustomListOrderLink                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page order sub tab order custom list link
        orderOrgCustomListOrderDiv                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div',//Order detail page order  messages sub tab page order list
        orderOrgCustomListOrderCloseBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a/i',//Order detail page order sub tab order custom list close button

        orderSubTabQuickFilterAvailableColumn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul',//Availlable column on the quick filter link(Order page)
        orderSubTabQuickFilterSelectedColumn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul',//Selected column on the quick filter link(Order page)
        orderSubTabQuickFilterAddColBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Quick filter add button on the pop window (Order page)
        orderSubTabQuickFilterRemoveColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]',//Quick filter remove button on the pop window (Order page)
        orderSubTabQuickFilterApplyBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]',//Quick filter apply button on the pop window (Order page)

        orderSubTabActivityPagingDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[2]/div[2]/div[2]/div/div/div[3]/div/div',//order pagination div on order page

        messageSubTabOnOrderPage                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]/button', //Order and detail page message sub tab on General info page
        quickFilterLinkOnMessageOrderPage               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[2]/a', // Quick filter link on order subTab
        messageSubTabSettingsIcon                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/a/i',//Order detail page messages sub tab settings icon
        messageSubTabCustomListNewLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Order detail page messages sub tab new custom list link
        messageSubTabCustomListOrderLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page messages sub tab order custom list link
        messageSubTabNewListInput                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order detail page messages sub tab new custom list input
        messageSubTabCustomListSaveColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]',//Order detail page messages sub tab custom list save button
        messageSubTabCustomListAddColBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]',//Order detail page messages sub tab custom list add button
        messageSubTabCustomListRemoveColBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]',//Order detail page messages sub tab custom list remove button
        messageSubTabCustomListAvailableColumns         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul',//Order detail page messages sub tab custom list available columns
        messageSubTabCustomListSelectedColumns          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul',//Order detail page messages sub tab custom list selected columns
        messageSubTabCustomListOrderListDiv             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order detail page messages sub tab page order list
        messageSubTabCustomListOrderCloseBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/div/a/i',//Order detail page messages sub tab order custom list close button
        messageSubTabCustomListTable                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table',//Order detail table messages sub tab page order list

        messageOrgCustomListCloneInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input',//Order detail page messages sub tab clone custom list input
        messageOrgCustomListCloneCreateBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]',//Order detail page messages sub tab clone custom list create button
        messageOrgCustomListOrderLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Orrder detail page messages sub tab order custom list link
        messageOrgCustomListOrderDiv                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div', //Order detail page messages sub tab order div
        messageOrgCustomListOrderCloseBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[4]/div/div/div/a/i',//Order detail page messages sub tab order custom list close button
        messageOrgCustomListCloneLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Order detail page messages sub tab clone custom list link

        messageSubTabQuickFilterAvailableColumn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul',//Order detail page messages sub tab custom list available columns
        messageSubTabQuickFilterSelectedColumn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul',//Order detail page messages sub tab custom list selected columns
        messageSubTabQuickFilterAddColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Order detail page messages sub tab quick filter add column button
        messageSubTabQuickFilterRemoveColBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]',//Order detail page messages sub tab quick filter remove column button
        messageSubTabQuickFilterApplyBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]',//Order detail page messages sub tab quick filter apply column button
        messageSubTabActivityPagingDiv                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[3]/div/div',//Order detail page messages sub tab table div



        campaignSubTabOnOrderPage                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[4]/button',//Order detail page campaign sub tab
        campaignSubTabSettingsIcon                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[4]/button',//Order detail page campaign sub tab having setting icon
        campaignSubTabCustomListNewLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]',//Order detail page campaign sub tab new custom list link
        campaignSubTabCustomListOrderLink               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page campaign sub tab order custom list link
        campaignSubTabNewListInput                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input',//Order detail page campaign sub tab new custom list input
        campaignSubTabCustomListSaveColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Order detail page campaign sub tab custom list save button
        campaignSubTabCustomListOrderAddBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Order detail page campaign sub tab custom list add button
        campaignSubTabCustomListOrderRemoveBtn          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Order detail page campaign sub tab custom list add button
        campaignSubTabCustomListAvailableColumns        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul',//Order detail page campaign sub tab custom list available columns
        campaignSubTabCustomListSelectedColumns         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul/li[1]',//Order detail page campaign sub tab custom list selected columns
        campaignSubTabCustomListOrderListDiv            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[2]',//Order detail page campaign sub tab order list div
        campaignSubTabCustomListOrderCloseBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/div/a/i',//Order detail page campaign sub tab order custom list close button
        campaignSubTabCustomListTable                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[2]/table', //Order detail page campaign sub tab records table

        campaignOrgCustomListCloneLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]',//Order detail page campaign sub tab clone custom list link
        campaignOrgCustomListCloneInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Order detail page campaign sub tab clone custom list input
        campaignOrgCustomListCloneCreateBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]',//Order detail page campaign sub tab clone custom list create button
        campaignOrgCustomListOrderLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]',//Order detail page campaign sub tab order custom list link
        campaignOrgCustomListOrderDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div',
        campaignOrgCustomListOrderCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div/div/div/a/i',//Order detail page campaign sub tab order custom list close button
        patientDetailPageCampaignOrderListDiv           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul',

        patientDetailPageCampaignSettingsIcon           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon on campaigns tab patient detail page
        quickFilterLinkOnCampaignOrderPage              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[1]/div[2]/a',//Order detail page campaign sub tab quick filter link
        campaignSubTabQuickFilterAvailableColumn        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul',//Order detail page campaign sub tab custom list available columns
        campaignSubTabQuickFilterSelectedColumn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul',//Order detail page campaign sub tab custom list selected columns
        campaignSubTabQuickFilterAddColBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]',//Order detail page campaign sub tab custom list add button
        campaignSubTabQuickFilterRemoveColBtn           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]',//Order detail page campaign sub tab custom list add button
        campaignSubTabQuickFilterApplyBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]',//Order detail page campaign sub tab custom list add button

        campaignSubTabActivityPagingDiv                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[3]/div/div',//Order detail page campaign sub tab table div drop down list icon
        descendingArrowOnOrderList                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[2]',//Order detail page campaign sub tab order list drop down list icon
        saveBtnOnOrderListPopWindow                     :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[4]/div[2]/div/div/div[4]/div[2]/div/form/div[2]/a[1]'//Order detail page campaign sub tab custom list add button

    };
module.exports.ordersandPatientsPageElements = ordersAndPatientsPageElements;
//Orders and Patients Page Elements - End



//ADMIN PAGE ELEMENTS - START
var adminPageElements =
    {
        userAdministrationLink                       	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[2]/a', //user administration link in security drop down list on Admin tenant Settings
        usernameLink                                 	:'//*[@id="27433894261325170288406987244"]/td[2]/span[2]/a', //user name link on user administration page
        adminRightsCheckBox                          	:'//*[@id="user_role_27433412174115552735464495796"]', //Admin rights check box on user administration page
        hrmAccessControlLink                         	:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[1]/ul/li[10]/a',//hrm access control link on administration drop down sub tab
        createControlTreeLink                        	:'//*[@id="tree_record"]', //Tree record link in hrm access control tree page
        adminControlTreeLink                            :'//a[contains(@title,"Admin Tree")]', //Admin control tree link
        controlAdminTreeOrgTab                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[2]/button[2]', //Control tree node organization subtab
        controlTreeOrgTab                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[3]/button[2]', //Control tree node organization subtab
        controlAdminTreeOrgTable                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[3]/div[2]/table', //Control tree organization custom list in node organization tab
        controlTreeDuplicateNodeError                   :'/html/body/div[1]/div/div[1]/div[2]/div/div[2]/p', //Duplicate child node name error label
        controlTreeNodeErrorCloseBtn                    :'/html/body/div[1]/div/div[1]/div[2]/div/div[1]/a', //Duplicate node error div close button
        controlTreeAddOrgInput                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[4]/div[2]/div[2]/ul/li/div/div/div/div[1]/input', //Add organization input box
        controlTreeTable                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Control tree custom list table
        controlTreeCloneNameInput                       :'/html/body/div[4]/div[2]/div/div[2]/div/form/ul/li[1]/div/input', //Clone control tree name input
        controlTreeCloneBtn                             :'/html/body/div[4]/div[2]/div/div[5]/button', //Clone control tree create button
        controlTreeOrgTable                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div/div[3]/div/div[4]/div[2]/table', //Control tree organization custom list
        mergeRecordsTab                                 :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[2]/ul/li[2]/a', //Information merge records tab

        adminLink                                    	:'//*[@id="global_nav"]/ul/li[1]/ul/li[2]', //Admin link is present on header menu drop down list on home page
        interfaceLink                                	:'//*[@id="global_nav"]/ul/li[1]/ul/li[3]/a', //Interface link is present on header menu drop down list on home page
        bulkImportLink                               	:'//*[@id="global_nav"]/ul/li[1]/ul/li[4]/a', //Bulk import link is present on header menu drop down list on home page
        informationLink                              	:'//*[@id="admin-nav"]/ul/li[2]/a', //Information link in admin header menu
        relationshipLink                             	:'//*[@id="admin-nav"]/ul/li[3]/a', //Relationship  link is present on user administration page
        panelsLink                                   	:'//*[@id="admin-nav"]/ul/li[4]/a', //Panels link is present on user administration page
        customizationsLink                           	:'//*[@id="admin-nav"]/ul/li[5]/a', //Customization link is present on user administration page
        userAdminLinkTemp                               :'//*[@id="admin-nav"]/ul/li[1]/ul/li[2]', //User administration li present on administration page
        userAdminLink                                	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[2]/a', //User administration link present on user administration page
        userProfilesLink                             	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[3]/a', //User profile link is present on user administration page (security sub tab)
        rolesLinkLi                                     :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[1]/ul/li[4]', //Roles link li
        rolesLink                                    	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[4]/a', //Roles link is present on user administration page (security sub tab)
        authProviderLink                             	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[5]/a', //Auth Provider link is present on user administration page (security sub tab)
        loginConfigLink                              	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[6]/a', //Login Config link is present on user administration page (security sub tab)
        distributionLink                             	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[7]/a', //Distribution link is present on user administration page (security sub tab)
        calendarDispLink                             	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[8]/a', //Calendar Display link is present on user administration page (security sub tab)
        notificationLink                             	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[9]/a', //Notification link is present on user administration page (security sub tab)
        hrmAccessLink                                	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[10]/a', //Hrm Accesslink is present on user administration page (security sub tab)
        serverCredentialsLink                        	:'//*[@id="admin-nav"]/ul/li[1]/ul/li[11]/a', //server Credentials link is present on user administration page (security sub tab)
        activityCategoryLink                         	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[1]/a', //Activity Category link is present on user administration page (Relationship management sub tab)
        activityStatusLink                           	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[2]/a', //Activity Status link is present on user administration page (Relationship management sub tab)
        activityPriorityLink                         	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[3]/a', //Activity priority link is present on user administration page (Relationship management sub tab)
        campaignTypeLink                             	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[4]/a', //Campaign Type link is present on user administration page (Relationship management sub tab)
        competitiveLabLink                           	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[5]/a', //Competitive Lab link is present on user administration page (Relationship management sub tab)
        contactTypeLink                              	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[6]/a', //Contact Type link is present on user administration page (Relationship management sub tab)
        correctiveActionLink                         	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[7]/a', //Corrective Actione link is present on user administration page (Relationship management sub tab)
        emrLink                                      	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[8]/a', //EMR link is present on user administration page (Relationship management sub tab)
        opportunityStageLink                         	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[9]/a', //Opportunity stage link is present on user administration page (Relationship management sub tab)
        opportunityTypeLink                          	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[10]/a', //Opportunity Type link is present on user administration page (Relationship management sub tab)
        organizationTypeLink                         	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[11]/a', //Organization Type link is present on user administration page (Relationship management sub tab)
        rootCauseLink                                	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[12]/a', //Root Cause link is present on user administration page (Relationship management sub tab)
        specialityLink                               	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[13]/a', //Specialty link is present on user administration page (Relationship management sub tab)

        customListLink                               	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[1]/a', //Custom List is present on user administration page (Customizations sub tab)
        emailMetaDataLink                            	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[2]/a', //Email MetaData is present on user administration page (Customizations sub tab)
        customProfilesLink                           	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[3]/a', //Customization Profiles is present on user administration page (Customizations sub tab)
        lensProfilesLink                             	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[4]/a', //Lens Profiles is present on user administration page (Customizations sub tab)
        tabProfilesLink                              	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[5]/a', //Tab Profiles is present on user administration page (Customizations sub tab)
        customTabsLink                               	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[6]/a', //Custom Tabs is present on user administration page (Customizations sub tab)
        userDefinedLayoutsLink                       	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[7]/a', //User Defined Layouts is present on user administration page (Customizations sub tab)
        quickCreateCompLink                          	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[8]/a', //Quick Create Components is present on user administration page (Customizations sub tab)
        userDefinedFieldsLink                        	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[9]/a', //User Defined Fields is present on user administration page (Customizations sub tab)
        workflowTemplatesLink                         	:'//*[@id="admin-nav"]/ul/li[5]/ul/li[10]/a', //Workflow Templates is present on user administration page (Customizations sub tab)

        actCategoryAddLink                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/a', //Activity category page add link
        actCategorySaveBtn                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[1]', //Activity category page save button
        actCategoryCancelBtn                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[2]', //Activity category page cancel button
        actCategoryTable                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table', //Activity category page category list table

        actSubCategoryTab                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[2]', //Activity sub category tab
        actSubCategoryAddLink                           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/div/span', //Activity sub category page add link
        actSubCategorySaveBtn                           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/button[1]', //Activity sub category page save button
        actSubCategoryCancelBtn                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/button[2]', //Activity sub category page cancel button
        actSubCategoryTable                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table', //Activity sub category page sub categories table
        actSubCategoryInput                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr[12]/td[1]/div/input',    // Activity subcategory name on create subcategory page
        actSubCategoryDesc                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr[12]/td[2]/textarea',  // Activity subcategory description text on create subcategory page

        opporStageCreateLink                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/div/span', //Create new stage link in opportunity stage page
        opporStageTable                                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table', //Opportunity stage table
        opporStageSaveBtn                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]', //Save button in opportunity stage page
        opporStageCancelBtn                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[2]', //Cancel button in opportunity stage page

        specialtyCreateLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/div/span', //Create link in specialty page
        specialtyTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table', //Specialty table
        specialtySaveBtn                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]', //Save button in specialty page
        addSpecialityLink				                : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/div/span',
        specialtyCreateLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/div/span', //Create link in specialty page
        specialtyTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table', //Specialty table
        specialtyInactiveLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',
        specialtyInactiveTable				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',
        specialtySaveBtn                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]', //Save button in specialty page

        emrAddLink                                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a', //Create new EMR link
        emrSaveBtn                                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]', //Save EMR button
        emrTable                                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table', //EMR table

        campaignTypeTable                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table', //CAmpaigns type table
        campaignTypeAddLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/a', //Create campaign type link
        campaignTypeSaveBtn                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[1]', //Campaign save button
        campaignTypeInactiveListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/a', //Inactive campaigns type list link
        campaignTypeInactiveListTable                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table', //Inactive campaign types table
        campaignStageTab                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[2]', //Campaign stage tab
        campaignStageCreateLink                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/a', //Create campaign stage link
        campaignStageTable                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table', //Campaigns table
        campaignStageSaveBtn                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/button[1]', //Campaign stage save button
        campaignStageInactiveLink                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/a', //Inactive campaign stage link
        campaignStageInactiveTable                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table', //Inactive campaign stage table
        campaignDependencyMappingSubtab                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[3]', //Dependency mapping subtab in campaign type
        campaignDependencyMappingSaveBtn                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[3]/div[1]/div/div/div/button[1]', //Dependency mapping save button
        campaignDependencyStageTable                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[3]/div[2]/div[2]/ol', //Campaign stage table in dependency mapping table

        notificationDefaultSettingLink                  :"//a[contains(@title,'Default Setting')]",

        emailMetadataTable                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/table', //Email metadata table list

        userAdminPageDiv                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div', //User detail page div
        userAdminCreateUserLink                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[1]/ul/li[2]/a', //Create user link in user administration page
        userAdminEmailAllUsersLink                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[1]/ul/li[3]/a', //Email all users not logged in link in user administration page
        userAdminEditUserRoles                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[2]/ul[1]', //Roles div in user edit page
        userAdminEditUserInsightRole                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[2]/ul[2]/li/div/div/select', //Insight role div in user edit page
        userAdminEditUserEmailInput                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[1]/div/input', //Email input in user edit page
        userAdminEmailErrorLabel                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[1]/div/label', //Email error label
        userAdminEditUserAliasNameInput                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[2]/div/input', //Alias name input in user edit page
        userAdminAliasErrorLabel                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[2]/div/label', //Alias name error label
        userAdminEditUserFullNameInput                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[3]/div/input', //Full name input in user edit page
        userAdminFullNameErrorLabel                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[3]/div/label', //Full name error label
        userAdminEditUserSMSNumberInput                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[4]/div/div/input', //SMS Number in user edit page
        userAdminEditUserUserTypeDropdown               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[5]/div/div/select', //USer type in user edit page
        userAdminEditUserSettingProfileDropdown         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[1]/li[9]/div/div/select', //User setting profile in user edit page
        userAdminEditUserDisabledCheckbox               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[2]/li[1]/label/input', //Disabled check box in user edit page
        userAdminEditUserLockedCheckbox                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[2]/li[2]/label/input', //Locked check box in user edit page
        userAdminEditUserSalesRepCheckbox               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[2]/li[3]/label/input', //Sales rep check box in user edit page
        userAdminEditUserReassignSalesRepBtn            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/div[1]/button', //Reassign sales rep account button in user edit page
        userAdminEditUserAccessControlNodeInput         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/div[2]/div/div/div/div[1]/input', //Add access control node search box in user edit page
        userAdminEditUserAccessLogLink                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/small[1]/a', //Audit/Access Log that User Accessed/Modified link in user edit page
        userAdminEditUserAuditLogLink                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/small[2]/a', //Audit Log of User Modifications in user edit page
        userAdminEditUserTwoFactorAuthDropdown          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[2]/div[1]/ul[3]/li/div/div/select', //Two factor authentication dropdown in user edit page
        userAdminEditUserSaveBtn                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[1]/div[2]/button[1]', //Save button in user edit page
        userAdminEditUserSendEmailBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[1]/div[2]/button[2]', //Send email button in user edit page
        userAdminCreateSuccessHeader                    :'/html/body/div[1]/div/div[2]/div[2]/div/div[1]/h4', //Create user success pop up
        userAdminCreateSuccessHeaderCloseBtn            :'/html/body/div[1]/div/div[2]/div[2]/div/div[1]/a', //Create user success pop up close button

        userAdminTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[2]/table', //User admin custom list table
        userAdminSettingsIcon                           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        userAdminEditCustomListLink                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        userAdminCloneCustomListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        userAdminNewCustomListLink                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link
        userAdminOrderCustomListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        userAdminCloneCustomListInput                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input
        userAdminCloneCustomListCreateBtn               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button
        userAdminNewCustomListInput                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        userAdminCustomListAvailColumns                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns
        userAdminCustomListSelectedColumns              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns
        userAdminCustomListAddColBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button
        userAdminCustomListRemoveColBtn                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button
        userAdminNewCustomListSaveBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button
        userAdminDeleteCustomListBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list button
        userAdminOrderCustomListDiv                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div
        userAdminOrderCustomListCloseBtn                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button
        userAdminPagingDiv                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div/div/div[3]/div/div', //User admin page pagination div
        userAdminDeleteLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/form/div[1]/div[2]/a[2]', //Delete user record link

        userAdminQuickFilterLink                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        userAdminFilterAvailColumns                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        userAdminFilterSelectedColumns                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns
        userAdminFilterAddColBtn                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button
        userAdminFilterRemoveColBtn                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button
        userAdminFilterApplyBtn                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button
        userAdminNameHeader                             :"//th[contains(@fieldid,'name')]", //Name header from user administration table
        userAdminSearchUser                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[1]/ul/li[1]/div/div[1]/input', //Search user

        userProfileTable                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //User profiles table
        userProfileNewProfileLink                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a', //New user profile link
        userProfileDescInput                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[2]/div/input', //Description input
        userProfileNameInput                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[1]/div/input', //Name input
        userProfileUserTypeDropdown                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[3]/div/div/select', //User type dropdown
        userProfileCategoryDropdown                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[4]/div/div/select', //Default activity category dropdown
        userProfileCustomizationProfileDropdown         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[5]/div/div/select', //Customization profile dropdown
        userProfileAuthenticationCheckbox               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/ul/li/div[2]/label/input', //2 factor authentication checkbox
        userProfileUsersSubTab                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[1]', //Users subtab
        userProfileUsersSubTabAddUserInput              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[2]/ul/li/div/div[1]/input', //Add user input
        userProfileUsersSubTabUsersTable                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[1]/div[2]/div/table', //Users custom list table
        userProfileDistributionGroupSubTab              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[2]', //Distribution groups subtab
        userProfileAddDistributionGroupInput            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[2]/div[1]/div/div[2]/ul/li/div/div[1]/input', //Add distribution group input
        userProfileDistributionGroupTable               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[2]/div[2]/div/table', //Distribution group table
        userProfileVisibleDistributionGroupSubTab       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[3]', //Visible distribution groups subtab
        userProfileAddVisibleDistributionGrpInput       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[3]/div[1]/div/div[2]/ul/li/div/div[1]/input', //Add visible distribution group input
        userProfileVisibleDistributionGroupTable        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[3]/div[2]/div/table', //Visible distribution group table
        userProfileNotificationSettingsSubTab           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[4]', //Notification settings subtab
        userProfileAddNotificationSetting               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[4]/div[1]/div/div/div/select', //Add notification setting dropdown
        userProfileAccessControlSubTab                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[5]', //Access control nodes subtab
        userProfileRolesDiv                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[2]/ul[1]', //Roles div in user profile page
        userProfileSaveBtn                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button', //Create user profile save button


        userProfileSettingsIcon							:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in user profile page
        userProfileEditCustomListLink					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list in user profile page
        userProfileCloneCustomListLink					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in user profile page
        userProfileNewCustomListLink					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in user profile page
        userProfileOrderCustomListLink					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in user profile page
        userProfileCloneInput							:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone input in user profile page
        userProfileCloneCreateBtn						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in user profile page
        userProfileNewCustomListInput					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in user profile page
        userProfileCustomListAvailColumns				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in user profile page
        userProfileCustomListSelectedColumns 			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in user profile page
        userProfileCustomListAddColBtn					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in user profile page
        userProfileCustomListRemoveBtn					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in user profile page
        userProfileCustomListSaveBtn					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in user profile page
        userProfileCustomListDeleteBtn					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list button in user profile page
        userProfileOrderCustomListDiv 					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in user profile page
        userProfileOrderCloseBtn						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in user profile page
        userProfileQuickFilterLink						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/a', //quick filter link in user profile page
        userProfileFilterAvailColumns					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //filter available columns in user profile page
        userProfileFilterSelectedColumns				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in user profile page
        userProfileFilterApplyBtn						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in user profile page
        userProfileFilterAddColBtn                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Add column button in filter
        userProfileFilterRemoveColBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Remove column button in filter
        userProfileNameHeader                           :"//th[contains(@fieldid,'name')]", //Name header from user profile table

        userRolesCreateNewRoleLink                    	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a', //Create new role link
        userRolesRoleNameInput                        	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[1]/div/input', //New role input
        userRolesRoleDescInput                        	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[2]/div/textarea', //New role description input
        userRolesUserTypeDropdown                     	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[3]/div/div/select', ///New role user type dropdown
        userRolesAdminPermissionsDiv                  	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div[1]/ul', //Admin Permissions div
        userRolesHRMPermissionsDiv                    	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div[2]/ul', //HRM permissions div
        userRolesUserAccessControlPermissionsDiv      	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div[3]/ul', //User access control permissions div
        userRolesIntegrationPermissonsDiv             	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div[5]/ul', //Integration permission div
        userRolesHealthCareInsightPermissionsDiv      	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/div[4]/ul', //Health care insight permissions
        userRolesRoleSaveBtn                          	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button', //New role save button
        userRoleCancelBtn                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/a[1]', //Cancel button on user role page
        userRolesTable                                	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //User roles table
        userRolesSettingsIcon                         	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        userRolesEditCustomListLink                   	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        userRolesCloneCustomListLink                  	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        userRolesNewCustomListLink                    	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link
        userRolesOrderCustomListLink                  	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        userRolesCloneInput                           	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input
        userRolesCloneCreateBtn                       	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button
        userRolesNewListInput                         	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        userRolesCustomListAvailColumns               	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns
        userRolesCustomListSelectedColumns            	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns
        userRolesCustomListAddColBtn                  	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button
        userRolesCustomListRemoveColBtn               	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button
        userRolesCustomListSaveBtn                    	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button
        userRolesCustomListDeleteBtn                  	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Custom list delete button
        userRolesOrderList                            	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div
        userRolesOrderListCloseBtn                    	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button
        userRolesQuickFilterLink                      	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        userRolesFilterAvailColumns                   	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        userRolesFilterSelectedColumns                	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns
        userRolesFilterAddColBtn                      	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button
        userRolesFilterRemoveColBtn                   	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button
        userRolesFilterApplyBtn                       	:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button
        userRolesPagingDiv                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div', //User roles paging div


        distributionGrpCreateLink                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a', //create group link in distribution group page
        distributionGrpNameInput                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[1]/div/input', //Name input in distribution group page
        distributionGrpEmailInput                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[2]/div/input', //Email input in distribution group page
        distributionGrpDescInput                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[3]/div/input', //Description input in distribution group page
        distributionGrpSaveBtn                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button', //Save button in distribution group page
        distributionGroupAddUserInput                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/ul/li[2]/div/div/div[1]/input', //Add user input in distribution group page
        distributionGrpSettingsIcon                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in distribution group page
        distributionGrpEditListLink                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in distribution group page
        distributionGrpCloneListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in distribution group page
        distributionGrpNewListLink                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in distribution group page
        distributionGrpOrderListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in distribution group page
        distributionGrpCloneInput                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone input in distribution group page
        distributionGrpCloneCreateBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in distribution group page
        distributionGrpNewInput                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in distribution group page
        distributionGrpListAvailColumns                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in distribution group page
        distributionGrpListSelectedColumns              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in distribution group page
        distributionGrpListAddColBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in distribution group page
        distributionGrpListRemoveColBtn                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in distribution group page
        distributionGrpListSaveBtn                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button in distribution group page
        distributionGrpListDeleteBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list button in distribution group page
        distributionGrpOrderListDiv                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in distribution group page
        distributionGrpOrderListCloseBtn                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button in distribution group page
        distributionGrpQuickFilterLink                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in distribution group page
        distributionGrpFilterAvailColumns               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in distribution group page
        distributionGrpFilterSelectedColumns            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in distribution group page
        distributionGrpFilterAddColBtn                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in distribution group page
        distributionGrpFilterRemoveColBtn               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in distribution group page
        distributionGrpFilterApplyBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button in distribution group page
        distributionGrpTable                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Distribution group table

        notifySettingCreateLink           				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a', //Create notification setting link in notification settings page
        notifySettingNameInput            				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[1]/div/input', //Notification setting name input in notification settings page
        notifySettingSaveBtn              				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button', //Notification setting save button in notification settings page
        notifySettingRecordTypes          				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[1]/tbody/tr/td[3]/div/div/div[1]/select', //Record types dropdown in notification settings page
        notifySettingUpdateType           				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[2]/tbody/tr/td[3]/div/div/div[1]/select', //Condition update type dropdown in notification settings page
        notifySettingRelationshipType     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[3]/tbody/tr/td[3]/div/div/div[1]/select', //Condition relationship types dropdown in notification settings page
        notifySettingPriorityType         				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[4]/tbody/tr/td[3]/div/div/div[1]/select', //Condition priority types dropdown in notification settings page
        notifySettingDeliveryType         				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[5]/tbody/tr/td[3]/div/div/div[1]/select', //Delivery types dropdown (then send) in notification settings page
        notifySettingSettingsIcon         				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in notification settings page
        notifySettingEditLink             				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in notification settings page
        notifySettingCloneLink            				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in notification settings page
        notifySettingNewLink           	  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in notification settings page
        notifySettingOrderLink            				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in notification settings page
        notifySettingCloneInput           				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone input in notification settings page
        notifySettingCloneCreateBtn       				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in notification settings page
        notifySettingNewListInput         				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New list input in notification settings page
        notifySettingListAvailColumns     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in notification settings page
        notifySettingListSelectedColumns  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in notification settings page
        notifySettingListAddColBtn        				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in notification settings page
        notifySettingListRemoveColBtn     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in notification settings page
        notifySettingCustomListSaveBtn    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in notification settings page
        notifySettingCustomListDeleteBtn  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Custom list delete button in notification settings page
        notifySettingOrderList            				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order list div in notification settings page
        notifySettingOrderListCloseBtn    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order list close button in notification settings page
        notifySettingQuickFilterLink      				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in notification settings page
        notifySettingFilterAvailColumns   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in notification settings page
        notifySettingFilterSelectedColumns				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in notification settings page
        notifySettingFilterAddColBtn      				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in notification settings page
        notifySettingFilterRemoveColBtn   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in notification settings page
        notifySettingFilterApplyBtn       				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button
        notifySettingsRecordTypeTable                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[1]/tbody/tr/td[3]/div/div/div[2]/table', //Record type table
        notifySettingsUpdateTypeTable                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[2]/tbody/tr/td[3]/div/div/div[2]/table', //Condition Update type table
        notifySettingsRelationshipTypeTable             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[3]/tbody/tr/td[3]/div/div/div[2]/table', //Condition relationship type table
        notifySettingsPriorityTypeTable                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[4]/tbody/tr/td[3]/div/div/div[2]/table', //Condition priority type table
        notifySettingsDeliveryTypeTable                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/table[5]/tbody/tr/td[3]/div/div/div[2]/table', //Condition delivery type table
        notifySettingsTable                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Notifications settings table

        controlTreeSettingsIcon     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in Access control tree page
        controlTreeEditListLink     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in Access control tree page
        controlTreeCloneListLink     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //clone custom list link in Access control tree page
        controlTreeNewListLink     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //new custom list link in Access control tree page
        controlTreeOrderListLink     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //order custom list link in Access control tree page
        controlTreeCloneInput     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone input in Access control tree page
        controlTreeCloneCreateBtn    					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone create button in Access control tree page
        controlTreeNewInput     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New list input in Access control tree page
        controlTreeListAvailColumns     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns in Access control tree page
        controlTreeListSelectedColumns  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns in Access control tree page
        controlTreeListAddColBtn     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button in Access control tree page
        controlTreeListRemoveColBtn     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button in Access control tree page
        controlTreeListSaveBtn     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in Access control tree page
        controlTreeListDeleteBtn     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Custom list delete button in Access control tree page
        controlTreeOrderList     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div in Access control tree page
        controlTreeOrderListCloseBtn    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order list close button in Access control tree page
        controlTreeQuickFilterLink     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link in Access control tree page
        controlTreeFilterAvailColumns   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns in Access control tree page
        controlTreeFilterSelectedColumns				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Filter selected columns in Access control tree page
        controlTreeFilterAddColBtn     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button in Access control tree page
        controlTreeFilterRemoveColBtn   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button in Access control tree page
        controlTreeFilterApplyBtn     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Filter apply button  in Access control tree page

        serverCredentialsCreateLink						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a', //Create server credentials link in Server credentials page
        serverCredentialsNameInput						:'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Name input in Server credentials page
        serverCredentialsUsernameInput					:'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Username input in Server credentials page
        serverCredentialsPwdInput						:'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[3]/div/input', //Password input in Server credentials page
        serverCredentialsRepeatPwdInput					:'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[4]/div/input', //Repeat password input in Server credentials page
        serverCredentialsDescInput						:'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[5]/div/input', //Description input in Server credentials page
        serverCredentialsCreateBtn						:'/html/body/div[4]/div[2]/div/form/div[2]/button[2]', //Create button in Server credentials page
        serverCredentialsPopupCloseBtn					:'/html/body/div[4]/div[2]/div/div/a', //Popup close button in Server credentials page
        serverCredentialsTable							:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Server credentials list table in Server credentials page

        mergeRecordContactSubTab                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]', //Contact subtab
        mergeRecordOrgSearchNameInput                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input', //Name search input for org in merge records page
        mergeRecordsOrgSearchBtn                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[1]/form/div[2]/button[1]', //Search button in Org search merge records page
        mergeRecordsOrgSearchClearBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[1]/form/div[2]/button[2]', //Clear button in Org search merge records page
        mergeRecordsOrgSearchCompareBtn                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[1]/form/div[2]/button[3]', //Compare button in Org search merge records page
        mergeRecordsOrgSearchResultTable                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[2]/table', //Organization search table results
        mergeRecordsMergeBtn                            :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[1]/div[3]/form/div/button[1]', //Merge button
        mergeRecordContactSearchFNameInput              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input', //First name input contact search
        mergeRecordContactSearchBtn                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/form/div[2]/button[1]', //Contact search button
        mergeRecordContactCompareBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[2]/div[1]/form/div[2]/button[3]', //Compare search button
        mergeRecordContactMergeBtn                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[2]/div[3]/form/div/button[1]', //Contact merge button
        mergeRecordContactSearchTable                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[2]/div[2]/table', //Contact search results table
        mergeRecordProviderSubTab                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[3]', //Provider subtab
        mergeRecordProviderFNameInput                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[3]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input', //Provider first name input
        mergeRecordProviderSearchBtn                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[3]/div[1]/form/div[2]/button[1]', //Provider search button
        mergeRecordProviderCompareBtn                   :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[3]/div[1]/form/div[2]/button[3]', //Provider compare button
        mergeRecordProviderMergeBtn                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[3]/div[3]/form/div/button[1]', //Provider merge button
        mergeRecordsProviderSearchTable                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[2]/div[3]/div[2]/table', //Provider search result table

        diagnosisTab                                    :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[2]/a',
        diagnosisCreateLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Create link in diagnosis page
        diagnosisCodeInput                              :'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Code input in diagnosis page
        diagnosisDescInput                              :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Description input in diagnosis page
        diagnosisCreateBtn                              :'/html/body/div[4]/div[2]/div/form/div[2]/button[2]', //Create button in diagnosis page
        diagnosisTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Custom list table in diagnosis page
        diagnosisSettingsIcon          					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in diagnosis list page
        diagnosisEditCustomListLink     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in diagnosis list page
        diagnosisCloneCustomListLink    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in diagnosis list page
        diagnosisNewCustomListLink      				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in diagnosis list page
        diagnosisOrderCustomListLink    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in diagnosis list page
        diagnosisNewListInput          					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in diagnosis list page
        diagnosisListOperatorDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Custom list operator dropdown in diagnosis list page
        diagnosisListFilterByTrueOption 				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //Custom list filter by true option in diagnosis list page
        diagnosisListFilterByFalseOption				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //Custom list filter by false option in diagnosis list page
        diagnosisListSaveBtn          					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in diagnosis list page
        diagnosisPagingLastBtn                          :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Paging last button

        panelsTabLink                                   :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[3]/a', //Panels tab
        panelTable                                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table',
        panelSettingsIcon           					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        panelEditCustomListLink     					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link
        panelCloneCustomListLink    					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link
        panelNewCustomListLink      					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //NEw custom list link
        panelOrderCustomListLink    					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link
        panelNewListInput           					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input
        panelListOperatorDropdown   					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //CUstom list operator dropdown
        panelListFilterByTrueOption 					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //Custom list filter by true option
        panelListFilterByFalseOption					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //Custom list filter by false option
        panelListSaveBtn            					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button
        panelPagingLastBtn                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a/i', //Pagin last button


        panelTypeTab                    				:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[4]/a', //Panel type tab in panels type list page
        panelTypeSettingsIcon           				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in panels type list page
        panelTypeEditCustomListLink     				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in panels type list page
        panelTypeCloneCustomListLink    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in panels type list page
        panelTypeNewCustomListLink      				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in panels type list page
        panelTypeOrderCustomListLink    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in panels type list page
        panelTypeNewListInput           				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in panels type list page
        panelTypeListOperatorDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Custom list operator dropdown in panels type list page
        panelTypeListFilterByTrueOption 				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //List filter by true option in panels type list page
        panelTypeListFilterByFalseOption				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //List filter by false option in panels type list page
        panelTypeListSaveBtn            				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in panels type list page
        panelTypePagingLastBtn          				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a/i', //Paging last button in panels type list page

        panelTypeCreateLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Panel type create link
        panelTypeNameInput                              :'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Panel type name input
        panelTypeDescInput                              :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Panel type description input
        panelTypeCreateBtn                              :'/html/body/div[4]/div[2]/div/form/div[2]/button[2]', //Panel type create button
        panelTypeTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Panel type list table


        procedureCreateLink                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Procedure Create link
        procedureCodeInput                              :'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Procedure Code input
        procedureNameInput                              :'/html/body/div[4]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Procedure Name input
        procedureDescInput                              :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[3]/div/input', //Procedure Description input
        procedureCreateBtn                              :'/html/body/div[4]/div[2]/div/form/div[2]/button[2]', //Procedure Create button
        procedureTable                                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Procedure Custom list table
        proceduresTab    								:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[5]/a', //Procedures tab in procedures list page
        proceduresSettingsIcon     						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in procedures list page
        proceduresNewListLink      						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in procedures list page
        proceduresNewListInput      					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //NEw custom list input in procedures list page
        proceduresListOperatorDropdown  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Custom list operator dropdown in procedures list page
        proceduresListFilterByTrueOpt   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //Custom list filter by true option in procedures list page
        proceduresListFilterByFalseOpt  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //Custom list filter by false option in procedures list page
        proceduresListSaveBtn      						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in procedures list page
        proceduresPagingLastBtn    						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a/i', //Paging last button in procedures list page



        testsTabLink                                    :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[6]/a',// tests tab link
        testsCreateLink                                 :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Tests create link
        testsNameInput                                  :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Tests name input
        testsAbbreviationInput                          :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Tests abbreviation input
        testsDescInput                                  :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[3]/div/input', //Tests description input
        testsUnitsInput                                 :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[4]/div/input', //Tests units input
        testsRefLowInput                                :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[5]/div/input', //Tests ref range low input
        testsRefHighInput                               :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[6]/div/input', //Tests ref range high input
        testsConclusionTypeDropdown                     :'/html/body/div[5]/div[2]/div/form/div[1]/ul/li[7]/div/div/select', //Tests conclusion type dropdown
        testsCreateBtn                                  :'/html/body/div[5]/div[2]/div/form/div[2]/button[2]', //Tests create button
        testsSettingsIcon								:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in tests list page
        testsNewCustomListLink							:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in tests list page
        testsNewListInput								:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in tests list page
        testsCustomListOperatorDropdown					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Custom list operator dropdown in tests list page
        testsListFilterByTrueOpt						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //Custom list filter by true option in tests list page
        testsListFilterByFalseOpt						:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //Custom list filter by false option in tests list page
        testsListSaveBtn								:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Custom list save button in tests list page
        testsPagingLastBtn								:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a/i', //Paging last button in tests list page
        testsTable                                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //tests table

        customizationsListTypeDropdown                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/ul/li/div/div/select', //List type dropdown
        customizationsListViewDropdown                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/ul/li/div/div/select', //List view dropdown
        customizationsListNameInput                     :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/form/div[1]/div/ul/li/div/input', //List name input
        customizationsListSaveBtn                       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/form/div[4]/div/a[1]', //List save button
        customizationsNewCustomListBtn                  :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/button[1]', //New custom list button
        customizationsDeleteBtn                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/form/div[4]/div/a[2]',
        customizationsCustomListLink                    :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[5]/ul/li[1]/a', //Custom list link
        customizationListViewNameDiv                    :'html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/form/div[1]/div/ul/li/div/div', //Selected list view name div

        modelProfileNewProfileLink   					:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Create new custom profile link in customization profile page
        modelProfileProfileNameInput   		 			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[1]/li[1]/div/input', //Name input in customization profile page
        modelProfileTabProfileDropdown  				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[1]/li[2]/div/div/select', //Tab profile dropdown in customization profile page
        modelProfileLensProfileDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[1]/li[3]/div/div/select', //Lens profile dropdown in customization profile page
        modelProfileCampaignLayoutDropdown   			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[2]/li/div/div/select', //Campaign layout dropdown in customization profile page
        modelProfileCaseLayoutDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[3]/li/div/div/select', //Case layout dropdown in customization profile page
        modelProfileContactLayoutDropdown  	 			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[4]/li/div/div/select', //Contact layout dropdown in customization profile page
        modelProfileMemoLayoutDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[5]/li/div/div/select', //memo layout dropdown in customization profile page
        modelProfileOpportunityLayoutDropdown			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[6]/li/div/div/select', //opportunity layout dropdown in customization profile page
        modelProfileOrgLayoutDropdown      				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[7]/li/div/div/select', //organization layout dropdown in customization profile page
        modelProfilePanelLayoutDropdown    				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[8]/li/div/div/select', //panel layout dropdown in customization profile page
        modelProfilePatientLayoutDropdown  	 			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[9]/li/div/div/select', //patient layout dropdown in customization profile page
        modelProfileTaskLayoutDropdown   				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[10]/li/div/div/select', //task layout dropdown in customization profile page
        modelProfileProfileSaveBtn    		 			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/button', //profile save button	in customization profile page

        actCategorySaveButton				            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[1]',//Activity category page category save button
        actCategoryInactiveLink				            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Activity category page category inactive link
        actCategoryInactiveTable			            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Activity category page category inactive table list
        actCategoryPlusIcon				                : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Activity category page category plus icon on inactive table list
        actSubCategoryPlusIcon				            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Activity sub category page category plus icon on inactive table list
        actSubCategoryTab                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[2]', //Activity sub category tab
        actSubCategoryAddLink                           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/div/span', //Activity sub category page add link
        actSubCategorySaveBtn                           :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/button[1]', //Activity sub category page save button
        actSubCategoryCancelBtn                         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[1]/div/div/button[2]', //Activity sub category page cancel button
        actSubCategoryTable                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table', //Activity sub category page sub categories table
        actSubCategoryInput                             :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr[12]/td[1]/div/input',    // Activity subcategory name on create subcategory page
        actSubCategoryDesc                              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[1]/form/table/tbody/tr[12]/td[2]/textarea',  // Activity subcategory description text on create subcategory page

        activityCategoryInactiveLink			        : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Activity Category inactive link
        activityCategoryTableList			            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Activity Category table list
        activitySubCategoryInactiveLink			        : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Activity SubCategory inactive link
        activitySubCategoryTableList			        : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Activity SubCategory table list
        activitySubCategoryInactiveTableList		    : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',
        activitySubCategoryInput			            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr/td[1]/input',//Activity SubCategory input field
        opportunityStageBtn					            : '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[9]/a', //Opportunity stage button on the relation ship management page
        opportunityStageTableList			            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr[1]/td[1]/div/div', //Opportunity stage table list on the relation ship management page
        contactTypeLink                              	:'//*[@id="admin-nav"]/ul/li[3]/ul/li[6]/a', //Contact Type link is present on user administration page (Relationship management sub tab)
        contactTypebAddLink                             : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',//Contact type add link
    };
module.exports.adminPageElements = adminPageElements;
//ADMIN PAGE ELEMENTS - END



//HOME PAGE ELEMENTS - START
var homePageElements =
    {
        cloudSearchInputField					: '//*[@id="search_field"]', //Search Field on Cloud right side of search upper tab

        cloudSearchContactResultHeader				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[4]', //Header of searchable object
        cloudSearchProviderResultHeader				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[3]', // Provider search head on cloud search
        cloudSearchContactResultData				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[4]/div[1]/ul', // Result data of contact on header page
        cloudSearchProviderResultData				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[3]/div[1]/ul/li/div[1]/div[1]/div/span/span[1]/abbr', //Contact searchable result data after searching data
        cloudSearchAllTabLink                          	:'//*[@id="global_search"]/div/div[3]/ul/li[1]/small',  // custom list all text field on home page
        cloudSearchOrgTabLink                          	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[2]', //org search field on home page
        cloudSearchResultBox                           	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]', //Showing list of searchable text in pop window
        cloudSearchOrgResultHeader                     	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/h3', //Header of searchable org in pop window
        cloudSearchOrgOptionLink                       	:'/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]', //Option of searchable org in pop window
        cloudSearchOrgResultData                       	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul', //Searchable result data
        cloudSearchCaseIcon                            	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[1]/span/i', //Case icon when search Memo or Task activity searchable on cloud search
        cloudSearchTaskIcon                            	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[2]/span/i', //Task icon when search Memo or Case activity searchable on cloud search
        cloudSearchMemoIcon                            	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[3]/span/i', //Memo icon when search Task or Case activity searchable on cloud search
        cloudSearchOpportunityIcon                     	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[4]/span/i', //Opportunity  icon when search Task or Case activity searchable on cloud search
        cloudSearchMetaTagIcon                         	:'//html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[2]/div[1]/ul/li[1]/div[1]/div[2]/div/ul/li[5]/span/i', //Meta tag icon when search Task,Memo or Case activity searchable on cloud search
        cloudSearchMetaTagClipboard                    	:'//html/body/div[1]/div/div[9]/div[2]/div/div[1]/h4', //Open pop window and show clip board when click on meta tag icon
        cloudSearchClipBoardCopyOption                 	:'//html/body/div[1]/div/div[9]/div[2]/div/div[2]/p', //Open pop window and show option of  clip board when click on meta tag icon
    };
module.exports.homePageElements = homePageElements;
//HOME PAGE ELEMENTS - END


//OPPORTUNITY PAGE ELEMENTS - START
var opportunityPageElements =
    {
        opportunityPageHeader                        	:'//*[@id="main_content_view"]/div[1]/div[1]/div[1]/h1',  //Opportunity page header on desktop upper panel on Opportunity pop window
        opportunityCreateLink                           :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div[2]/div/ul/li[2]/a', //Opportunity page create link
        opportunityEditBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Opportunity page edit button
        opportunityOrgInput                             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/input', //Opportunity page organization input
        opportunityPopupSalesTerritoryDropdown          :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[2]/ul/li[2]/div/div/select', //Opportunity page sales territory dropdown
        opportunityPopupSalesRepDropdown                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[2]/ul/li[3]/div/div/select', //Opportunity page sales rep dropdown
        opportunityPopupApplyBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[3]/button[2]', //Apply button in create opportunity pop up
        opportunityPopupCreateOrgBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[3]/button[3]', //Create org button in create opportunity pop up
        opportunityPopupUpdateOrgBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[3]/button[4]', //Update org button in create opportunity pop up
        opportunityStageDropdown                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/select', //Stage dropdown in opportunity page
        opportunityCloseDateInput                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/div/input', //Close date input in opportunity page
        opportunityProjectedStartDateInput: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[8]/div/li/div/div/div/input', //Projected Start date input in opportunity page
        opportunityCloseDateDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/div/div[2]', //Close date div to select date from date picker
        opportunityNameInputField                    	:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/input',   //Creating opportunity NameInputField
        opportunitySalesTerritorySelect              	:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li[2]/div/div/select', //Enter SalesTerritorySelect on Opportunity creating page
        opportunitySalesRepSelect                    	:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li[3]/div/div/select', //Enter page SalesRepSelect on Opportunity creating page
        opportunityCloseDateOption                   	:'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/div/div[2]', //Enter close date option on Opportunity creating page
        opportunityCloseDateCalendar                 	:'/html/body/div[4]',   //Enter close date calendar on Opportunity creating page
        opportunityCloseDateSelection                	:'/html/body/div[4]/div[1]/table/tbody/tr[5]/td[5]', //Enter close date selection on opportunity creating page
        opportunitySaveButton                        	:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]',      //Save button on Opportunity creating page
        opportunityViewPageHeader                    	:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[2]/div/h1/span',
        opportunitiesTabLink                         	:'/html/body/div[1]/header/div[2]/div/nav/ul/li[7]/a', //Opportunity tab on desktop
        opportunitySaveBtn                              :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Save button in opportunity creating page
        opportunitySaveAndBackBtn                    	:'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Save&Back button in opportunity creating page
        opportunitiesTable                              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/table', //Opportunities table list
        opportunityChangeOrgPopup                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]', //Create opportunity pop up for modifying organization
        opportunityChangeOrgPopupCloseBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[1]/a', //Create opportunity pop up close button for modifying organization
        opportunityTypeDropdown                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/select', //Opportunity type dropdown
        opportunityOrgEditBtn                           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li[1]/div/div/span/i', //Organization edit button in opportunity page
        opportunityOrgSearchResultContainer             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[2]/ul/li[1]/div/div/div/div[3]', //Organization search results div
        opportunityOrgRequiredErrorLabel                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/div/div[2]/div/div[1]/div[2]/ul/li[1]/div/div/div/div[1]/label', //Organization required field error
        opportunityCloseDatePicker                      :'/html/body/div[4]', //Date picker for close date input
        opportunitySpecialtyDropdown                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select', //Specialty dropdown
        opportunityNameErrorLabel                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/label', //Name error label
        opportunityOrgErrorLabel                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li[1]/div/div/label', //Org name error label
        opportunityStageErrorLabel                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[5]/div/li/div/div/label', //Stage error label
        opportunityProbabilityErrorLabel                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/label', //Probability error label
        opportunityCloseDateErrorLabel                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[7]/div/li/div/div/div/label', //Close date error label
        opportunityCCUsersInput                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div[1]/div/div/div[1]/input', //CC Users input in opportunity page
        opportunityCCUsersSearchResults                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[9]/div/li/div/div[1]/div/div/div[3]', //Search results for CC users in opportunity page
        opportunityProbabilityInput                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[6]/div/li/div/input', //Probability input
        opportunityActivityFilterValueDropdown          :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/select', //Value dropdown in activity quick filter
        opportunityRecentItemsInput                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[2]/div/li/div[1]/div/div/div/div/div[1]/input', //Recent items input box


        opportunityOrdersSubTab                         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button[1]', //Orders sub tab
        opportunityRevenueSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button[2]', //Revenue sub tab
        opportunityPanelMixSubTab                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[1]/div[2]/button[3]', //PanelMix sub tab

        opportunityOrderLifeCycleDropdown               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[1]/div/div/div[1]/div/select', //Order lifecycle dropdown
        opportunityOrderLifeCycleInputBox               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[1]/div/div/div[2]/input', //Orders lifecycle input box
        opportunityOrdersEstimatedOrderDropdown         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[2]/div/div/div[1]/div/select', //Orders estimated orders dropdown
        opportunityOrdersEstimatedOrderInput            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[2]/div/div/div[2]/input', //Orders estimated orders input box
        opportunityOrderPerOrderCalculation             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[3]/div/input', //Average revenue per order calculation input box
        opportunityOrderDaysInWork                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[4]/div/div/div[1]/div/select', //Days in work dropdown
        opportunityOrderDaysInWorkInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/ul/li[4]/div/div/div[2]/input', //Days in work input box
        opportunityOrderApplyBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/p/button[1]', //order apply button
        opportunityOrderCancelBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[1]/p/button[2]', //Orders cancel button
        opportunityEstimatedMonthlyRevenueDiv           :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[3]/table/tbody/tr[1]/td[2]/span', //Estimated monthly revenue div
        opportunityTotalEstimatedRevenueDiv             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[3]/table/tbody/tr[2]/td[2]/span', //Total estimated revenue div
        opportunityTotalWeightedRevenueDiv              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[3]/table/tbody/tr[3]/td[2]/span', //Total weighted revenue div


        opportunityRevenueLifecycleDropdown             :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[2]/ul/li[1]/div/div/div[1]/div/select', //Total lifecycle dropdown
        opportunityRevenueLifecycleInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[2]/ul/li[1]/div/div/div[2]/input', //Total lifecycle input box
        opportunityRevenueEstimatedInput                :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[2]/ul/li[2]/div/input', //Total estimated revenue input
        opportunityRevenueApplyBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[2]/p/button[1]', //Revenue page apply button
        opportunityRevenueCancelBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[2]/p/button[2]', //Revenue page cancel button

        opportunityPanelMixLifeCycleDropdown            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/div[1]/ul/li[1]/div/div/div[1]/div/select', //Panel mix total lifecycle dropdown
        opportunityPanelMixLifeCycleInput               :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/div[1]/ul/li[1]/div/div/div[2]/input', //Panel mix total lifecycle input box
        opportunityPanelMixVolumePeriodDropdown         :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/div[1]/ul/li[2]/div/div/div/div/select', //Volume period input
        opportunityPanelMixFeeScheduleSearch            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/div[2]/div[2]/div/div/div/div[1]/input', //Panel mix fee schedule search input box
        opportunityPanelMixApplyBtn                     :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/p/button[1]', //Panel Mix apply button
        opportunityPanelMixCancelBtn                    :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/p/button[2]',//Panel Mix cancel button
        opportunityPanelMixPanelsTable                  :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/table', //Added panels table list

        opportunityGeneralSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[1]', //Details subtab
        opportunityDetailsSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div/div/button[2]', //Details subtab
        opportunityDetailsEMRDiv                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div', //EMR div in details tab
        opportunityDetailsEMRInput                      :'//*[@id="emr_text"]',//'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[1]/div/li/div/div/span[1]/input[2]', //EMR input box
        opportunityDetailsOrderInterfaceCheckbox        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div/div/input', //Order check box
        opportunityDetailsResultInterfaceCheckbox       :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[3]/div/li/div/div/div/input', //Result interface checkbox
        opportunityDetailsCurrentLabDropdown            :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[4]/div/li/div/div/select', //Current lab dropdown
        opportunityDetailsDescriptionInput              :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div/div/li/div/div/textarea', //Description input
        opportunityDetailsSaveBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Save button in details page
        opportunityDetailsSaveAndBackBtn                :'/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[2]', //Save and back button in details page
        opportunityAddPanelInput                        :'/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[2]/div/div/ul/div[1]/div/li/div/div[2]/div[3]/table/tfoot/tr/th/div/div/div[1]/input', //Add a panel input in panel mix tab


        opportunityCustomList                           :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[1]/select', //Custom list dropdown
        opportunitySettingsIcon                         :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon
        opportunityEditCustomListLink                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list
        opportunityCloneCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list
        opportunityNewCustomListLink                    :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list
        opportunityOrderCustomListLink                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list
        opportunityNewCustomListFilterByDropdown        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[1]/div/select', //Filter by dropdown in new custom list pop up
        opportunityNewCustomListOperatorDropdown        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Operator dropdown in new custom list pop up
        opportunityNewCustomListFilterByValueInput      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/div[1]/div/div[1]/input', //Filter by value input box in new custom list pop up
        opportunityCustomListFilterByContactOption      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[1]/div/select/option[4]', //Contact option in filter by dropdown

        opportunityNewCustomListInput                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list name input
        opportunityNewCustomListSaveBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //New custom list save button
        opportunityNewCustomListCancelBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[2]', //New custom list cancel button
        opportunityCustomListAvailColumns               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[1]/ul', //Custom list available columns
        opportunityCustomListSelectedColumns            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[3]/ul', //Custom list selected columns
        opportunityCustomListAddColBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[1]', //Custom list add column button
        opportunityCustomListRemoveColBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/div/div[2]/button[2]', //Custom list remove column button
        opportunityDeleteCustomListLink                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[3]', //Delete custom list link
        opportunityCloneCustomListInput                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[1]/ul/li/div/input', //Clone custom list input
        opportunityCloneCustomListCreateBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/form/div[2]/button[2]', //Clone custom list create button
        opportunityCloneCustomListCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[7]/div[2]/div/div/a', //Clone custom list close button
        opportunityOrderCustomListDiv                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/ul', //Order custom list div
        opportunityOrderCustomListCloseBtn              :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/div/a', //Order custom list close button
        opportunityOrderCustomListUpBtn                 :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[1]', //Order custom list up button
        opportunityOrderCustomListDownBtn               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[2]/div/div/div[4]/div[2]/div/form/div[1]/div/div/a[2]', //Order custom list down button
        opportunitySavePopUpYesOption                    :'/html/body/div[4]/div[2]/div/div[3]/button[1]', //Opportunity page save pop up option YES
        opportunitySavePopUpNoOption                    :'/html/body/div[4]/div[2]/div/div[3]/button[2]', //Opportunity page save pop up option NO


        opportunityQuickFilterLink                      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[1]/div[2]/a', //Quick filter link
        opportunityFilterByDropdown                     :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Quick filter - filter by dropdown
        opportunityFilterOperatorDropdown               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[2]/div/div/select', //Quick filter operator dropdown
        opportunityFilterApplyBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Quick filter apply button
        opportunityFilterSaveBtn                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[2]', //Quick filter save button
        opportunityFilterResetBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[3]', //Quick filter reset button
        opportunityFilterCloseBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[3]/a[4]', //Quick filter close button
        opportunityFilterAvailColumns                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/ul', //Filter available columns
        opportunityFilterSelectedColumns                :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[3]/ul', //Quick filter selected columns
        opportunityFilterAddColBtn                      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[1]', //Filter add column button
        opportunityFilterRemoveColBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[2]/div/div/div[2]/button[2]', //Filter remove column button
        opportunityValueDropdown                        :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[1]/select', //Value dropdown for begin date column selection
        opportunityValueDateInput                       :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[2]/div/input', //Date input for begin date filter option
        opportunityValueRelativeDatePlusBtn             :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[3]/div[1]/button[1]', //'+' button in value relative date input
        opportunityValueRelativeDateMinusBtn            :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[3]/div[1]/button[2]', //'-' button in value relative date input
        opportunityValueRelativeDaysInput               :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[3]/input', //No of days input in value relative date input
        opportunityValueRelativeDatePeriodDropdown      :'/html/body/div[1]/main/div[3]/div[1]/div[2]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[3]/div/div/div/div/div[3]/div[2]/select', //Period (day, weeks..etc) dropdown in value relative date input

        opportunityActivitySubTab                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[1]/button',   // Activity button on opportunity tab
        opportunityTaskActivityTab                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[2]/a/i', // Task Activity on Opportunity page
        opportunityMemoActivityTab                      :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[1]/ul[2]/li[3]/a', // Memo Activity on Opportunity page
        opportunityActivityQuickFilterLink              :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[2]/a', // Quick filter on opportunity page
        opportunityActivityFilterOption                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[1]/div/form/span/div[1]/div/div/select', //Filter option in quick filter pop page
        opportunityActivityFilterApplyBtn                       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[2]/div/div[1]/div[3]/a[1]', //Apply button on quick filter pop page
        opportunityAttachmentsSubTab                    :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[2]/button', //Attachment button on Opportunity tab
        opportunityAttachmentsUploadLink                :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[2]/div[2]/ul[2]/li[2]/a', //Attachments sub button on opportunity button
        opportunityAttachmentNameInput                  :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/ul/li[1]/div/input', //Attachments name input on Upload a File as Attachment
        opportunityAttachmentUploadBtn                  :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/form/div/div[2]/button', //Upload button on  Upload a File as Attachment
        opportunityAttachmentCloseBtn                   :'/html/body/div[1]/main/div[3]/div[1]/div[6]/div/div[2]/div/div/a/i', // Close icon on  Upload a File as Attachment
        opportunityMessageSubTab                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[2]/div[1]/div/ul/li[3]/button', //Attachment button on Opportunity tab
        opportunityActivityNewCustomListInput           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', // List name in custom list pop up window
        opportunityActivityNewCustomListLink            :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New CustomList button on Opportunity page
        opportunityActivityFilterOptionCustomList       :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[1]/div/select',
        opportunityActivityNewCustomListSaveBtn         :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]',
        opportunityActivityCustomListDropdown           :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[1]', //CustomList table on Opportunity page
        opportunityActivityTable                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[2]/div/div/div[2]/table', //Activities table in opportunity page
        opportunityMessagesTable                        :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[3]/div[2]/div/div/div[2]/table', //Message table list on opportunity page
        opportunityActivityFilterByStatusOption         :'//option[contains(@value,"status")]', //'Status' option in filter by dropdown in activity quick filter
        opportunityActivityFilterValueOpenOption        :'//option[contains(@value,"Open")]', //'Open' option in value dropdown in activity quick filter
        opportunityActivitySettingsIcon                 :'/html/body/div[1]/main/div[3]/div[1]/div[4]/div/div[3]/div[1]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Settings icon in activity tab opportunity
        opportunityTabHeaderOnCloudSearchDiv		: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[6]/div[1]',// Tab header div when searched the opportunity
        opportunityTabHeaderOnCloudSearch		: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/ul/li[6]/span',// Opportunity Tab header when searched the opportunity
        opportunityCloudSearchDataLink			: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[6]/div[1]/ul/li/div[1]/div[1]/div/span',// Opportunity data link when searched the opportunity on cloud search
        opportunityOrgName				: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[1]/span[1]', //Organization name on the opportunity name
        opportunityType					: '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div[1]/div/div/ul/div[2]/div/li/div/div', //Organization type on the opportunity name

        saveAndBackBtn					: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[2]', //Save and Back buttton on activity page
        saveBtnOnActivityPage				: '/html/body/div[1]/main/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/div/div[2]/button[1]',//Activity page contain save button on edited page
        opportunitySearchField				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[1]/input', //Cloud search field on the desktop
        taskIconOnCloudSearch				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[6]/div[1]/ul/li/div[1]/div[2]/div/ul/li[1]/span/i',// Opportunity cloud search link have contain the task icon
        memoIconOnCloudSearch				: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[3]/div/div/div[1]/div[6]/div[1]/ul/li/div[1]/div[2]/div/ul/li[2]/span/i',// Opportunity cloud search link have contain the memo icon
    };
module.exports.opportunityPageElements = opportunityPageElements;
//OPPORTUNITY PAGE ELEMENTS - END



//Dashboard Page Elements - Start
var dashboardPageElements =
    {
        dashboardTabLink                                :'//*[@id="site_nav"]/ul/li[2]/a/span', //dashboard navigation link
    };
module.exports.dashboardPageElements = dashboardPageElements;
//Dashboard Page Elements - End



//Audit log Elements - Start
var auditLogElements =
    {
        datePickerDiv                                   :'/html/body/div[4]', //Date picker div in audit log pop up
        auditLogLink                                    :'/html/body/div[1]/main/div[3]/div[1]/div[5]/div/small/a', //Audit link after select organization name on organization page
        auditHistoryTab                                 :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[1]/button[1]', //Audit history tab on Audit/Access history pop up window
        accessHistoryTab                                :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[1]/button[2]', //Access history tab on Audit/Access history pop up window
        allHistoryTab                                   :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[1]/button[3]', //All history tab on Audit/Access history pop up window
        startDateDiv                                    :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[1]/div/div[1]/div/div', //Start date drop down list on Audit/Access history pop up window
        endDateDiv                                      :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[1]/div/div[2]/div/div', //End date drop down list on Audit/Access history pop up window
        auditLogsTable                                  :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div/table', //Audit logs table in audit log pop up
        exportLink                                      :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[2]/div/ul/li[2]/a', //Export link for "audit history" on Audit/Access history pop up window
        accessHistoryStartDateDiv                       :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div[1]/div/div', //Start date drop down of "access history" tab list on Audit/Access history pop up window
        accessHistoryEndDateDiv                         :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/div[2]/div/div', //End date drop down of "access history" tab list on Audit/Access history pop up window
        accessHistoryRecordsTable                       :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[2]/div/table', //Access history records table
        accessHistoryExportLink                         :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[2]/div/ul/li[2]/a', //Export link for "access history" on Audit/Access history pop up window
        allHistoryStartDateDiv                          :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[3]/div[1]/div/div[1]/div/div', //Start date drop down of "all history" tab list on Audit/Access history pop up window
        allHistoryEndDateDiv                            :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[3]/div[1]/div/div[2]/div/div', //End date drop down of "all history "tab list on Audit/Access history pop up window
        allHistoryRecordsTable                          :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/div/div[3]/div[2]/div/table', //All history tab table in audit log pop up
        allHistoryExportLink                            :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[2]/div/div/div/ul/div[2]/div/ul/li[2]/a', //Export link for "access history" on Audit/Access history pop up window
        auditLogDivCloseBtn                             :'/html/body/div[1]/main/div[3]/div[1]/div[7]/div[2]/div/div/div[1]/a', //close button on Audit/Access history pop up window
    };
module.exports.auditLogElements = auditLogElements;
//Audit log Elements - End


//Panel Page Elements - Start
var panelsAdminSectionElements =
    {
        feeScheduleAdmin                                : '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[1]/a', //Fee schedule on panels and Fees page
        feeScheduleLinkSchedule                         : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //For creating fee schedule on fees schedule page
        nameInputFeeSchedule                            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div[2]/div/form/div[1]/ul/li[1]/div/input', //Fee Schedule create name input
        descriptionInputFeeSchedule                     : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Fee schedule description on fees schedule page
        createBtnFeeSchedule                            : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div[2]/div/form/div[2]/button[2]', //Create button on fee schedule page
        feesScheduleDeskBoard                           : '/html/body/div[1]/main/div[3]/div/div/div[2]',  //Custom list on desktop Fee schedule page
        feeScheduleTable                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table', //Fee Scheudle custom list table
        tBodyFeeScheduleTab                             : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table/tbody', //Table body in fee schedul page
        panelSelectField                                : '/html/body/div[1]/main/div[3]/div[1]/div[3]/div/div/div/div[2]/div/div[2]/div/div/div[1]/input', //To select panel from customlist on fee schedule page
        addPanel                                        : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div/div[2]/div/div/ul/li[2]/a', //Panel add button on fee schedule page
        panelNameInput                                  : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/form/div[1]/ul/li[1]/div/div/div/div[1]/input', //Panel name field input field
        panelPriceInput                                 : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/form/div[1]/ul/li[2]/div/input', //Panel price field input field
        createBtnPanel                                  : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/form/div[2]/button[2]', //Create button on panels page

        feeScheduleSettingsIcon             			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/a', //Setting icon in fee schedule list page
        feeScheduleEditCustomListLink           		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[1]', //Edit custom list link in fee schedule list page
        feeScheduleCloneCustomListLink          		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[2]', //Clone custom list link in fee schedule list page
        feeScheduleNewCustomListLink            		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[3]', //New custom list link in fee schedule list page
        feeScheduleOrderCustomListLink          		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div[1]/div[2]/div/div[1]/a[4]', //Order custom list link in fee schedule list page
        feeScheduleNewCustomListInput           		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/ul/li/div/input', //New custom list input in fee schedule list page
        feeScheduleCustomListOperatorDropdown   		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[2]/div/select', //Custom list Operator dropdown in fee schedule list page
        feeScheduleListFilterByTrueOption       		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[1]/input', //Custom list filter by true option in fee schedule list page
        feeScheduleListFilterByFalseOption      		:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[1]/table[1]/tbody/tr/td[3]/div/div/label[2]/input', //Custom list filter by false option in fee schedule list page
        feeScheduleListSaveBtn             				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[5]/div[2]/div/form[2]/div[2]/button[1]', //Save custom list button in fee schedule list page
        feeSchedulePagingLastBtn                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[3]/div/div/ul/li[5]/a', //Last button of paging in fee schedule list page

        panelsTab                                       :'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[4]/ul/li[3]/a', //Panels button on panels page
        panelOpportunitySearchableCheckbox              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[3]/div/div/div/div/div/ul/div[12]/div/li/div/div/div/input', //Searchable for opportunity checkbox
        createPanels                                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div/div/ul/li[2]/a', //Create button on panels page
        panelsNameInput                                 :'//*[@id="panel_name"]', //Name of panels on Panelpop window
        panelsDescriptionInput                          :'//*[@id="panel_description"]', //Description Field on Panelpop window
        newPanelForm                                    :'/html/body/div[4]/div[2]/div/form', //New panel form
        createBtnPanels                                 :'/html/body/div[4]/div[2]/div/form/div[2]/button[2]', //Create button on panels page
        saveBtnPanelsPage                               :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[2]/div[1]/div/div[1]/button[1]', //Save Button on panels page
        saveAndBackPanelsPage                           :'//*[@id="action_buttons"]/div/div[1]/button[2]', ////Save&Back Button on panels page
        toCheckNumberOfPanelList                        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div', // Customlist for checking list of panels
        panelPageEditBtn                                :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div[2]/div[1]/div/div/button[2]', //Panel detail page edit button
        saveBtnPanelsCreatedPage                        : '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button',    //Save Button on creating panels page
        feeScheduleDefaultCheckbox                      :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[3]/div[1]/label/input', //Default checkbox in feeschedule
        feeScheduleOpportunitySearchableCheckbox        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[3]/div[2]/label/input', //Opportunity searchable checkbox

    };
module.exports.panelsAdminSectionElements = panelsAdminSectionElements;
//Panel Page Elements - End

var otherSheet =

    {
        userNameInput                       : '/html/body/div[2]/div/form/div/div[1]/input', //User name input on login page
        passwordInput                       : '/html/body/div[2]/div/form/div/div[2]/input', //Password input on login page
        okayBtnOnLoginFailedPage                  : '/html/body/div[1]/div/div[2]/button[2]',// Okay button on login failed page
        submitBtnOnLoginPage                  : '/html/body/div[2]/div/form/div/div[3]/button[1]', // Submit button on login page
        headerUsernameDropdown                   : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[1]/a',  //Admin menu username link (firefox xpath)
        changePasswordLink                   : '/html/body/div[1]/main/div[3]/div/div[2]/div[2]/a', // Password change link on the my account page
        newPasswordInput                     : '/html/body/div[2]/div/form/div[4]/input', //New Password Input field  my account page
        confirmPasswordInput                  : 'html/body/div[2]/div/form/div[5]/input', //Confirm Password Input field  my account page
        headerHelpMenuDropdown                : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[2]/a', // Help menu Drop down list on header panel
        videoDropDownList                    : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[2]/ul/li[2]/a',
        onlineHelpDropDownList                : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[2]/ul/li[1]/a',
        hc1AcademyDropDownList                : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[2]/ul/li[3]/a',
        aboutThisApplicationDropDownList         : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[2]/ul/li[5]/a',
        aboutThisAppPopUp               :'/html/body/div[4]', //About this application pop up
        aboutThisPopupContentDiv                :'/html/body/div[4]/div[2]/div/div[2]/div/div', //About this pop content
        spaceDirectoryList                   : "/html/body/div[2]/header/nav/div/div[1]/ul/li[1]/a", //Space directory menu on video tutorial new tab (video tutorial i help menu)
        aboutThisCloseBtn                       :'html/body/div[4]/div[2]/div/div[1]/a', //Close button in about this application pop up

        termsOfUseLink                      : '//*[@id="site_footer"]/div/div/ul/li[3]/a',//Terms of use link on bottom of page
        termsOfUseDiv                       : '/html/body/section[1]/div/div[3]/div', //Terms of use div on redirect on Terms of use page
        termsOfUsePageDiv                 :'//*[@id="banner"]/div[3]', //Banner in privacy policy page
        privacyPolicyLink                    : '/html/body/div[1]/footer/div/div/ul/li[4]/a',//Privacy policy link on bottom of page
        privacyPolicyPageBanner                 :'//*[@id="banner"]/div[3]', //Banner in privacy policy page


        recentItemsDropDownList                   : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[3]/a',// Recent menu Drop down list on header panel

        //---------------------------------------My account -------------------------------
        myAccountDropDownList                 : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[1]/ul/li[1]/a',// My account drop down list on upper panel
        OutOfOfficeDiv                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/ul[1]/li[5]/div/div[1]/div/div',//Out of office div on My account page
        accountDate                            : '/html/body/div[1]/main/div[3]/div/div[1]/div/p/small',//Account date on the
        userAdminDiv                    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div', //User admin div in administration page


        firstNameInput                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/ul[1]/li[2]/div/input',//First name input on my account page
        lastNameInput                       : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/ul[1]/li[3]/div/input',//Last name input on my account page
        saveButtonOntAccountPage               : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div/button',//Save button on the account page
        smsNumberInput                      : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/ul[1]/li[4]/div/div/input', //Sms number input on accoun page
        accountEmailInput                    : '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/ul[1]/li[1]/div/input',//Account email on My account page

        // -----------------------------------------Recent items ---------------------------------
        recentItemsDropUlList              : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[3]/ul', // Drop down list od Recent items om upper panel
        newPasswordLabelMsg                     :'/html/body/div[2]/div/form/div[4]/label', //Message label on password change page
        oldPasswordInput                        :'/html/body/div[2]/div/form/div[3]/input', //Old Password Input field  my account page
        submitBtnChangePwdPage                  :'/html/body/div[2]/div/form/div[6]/button[1]',//Submit button on password change page
    }

module.exports.otherSheet = otherSheet;

var footerSection =

    {
        termsOfUseLink                   : '//*[@id="site_footer"]/div/div/ul/li[3]/a',//Terms of use link on bottom of page
        termsOfUseDiv                    : '/html/body/section[1]/div/div[3]/div', //Terms of use div on redirect on Terms of use page
        privacyPolicyLink                 : '/html/body/div[1]/footer/div/div/ul/li[4]/a',//Privacy policy link on bottom of page
        recentItemsDropDownList                : '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[3]/a',
    }

module.exports.footerSection = footerSection;


var adminRelationshipManagementPageElements =
    {

        //------------------------------------- Relationship mangement ------------------------------------------
        relationshipManagementTAb			: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/a',//Relationship Management link on admin section
        activityCategoryTab					: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[1]/a', //Activity category sub tab on Relationship management column
        activityCategoryBtn					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[1]', //Activity category button on Relationship management column
        addActivityCategoryLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/a',//For add activity category link on Activity category page
        addActivityCategoryInput			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr/td[1]/div/input',//For add activity category input on Activity category page
        saveBtnOnRelationshipMgm			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[1]',//Save button on the add activity category page
        campaignTypeTab						: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[4]/a',//Campaign Type tab on Relationship management
        addCampaignTypeLink					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/a',//Add campaign type link on Relationship management
        campaignTypeInput					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table/tbody/tr[1]/td[1]/div/input',//Input campaign type link on Relationship management
        campaignStageButton					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/button[2]',//Campaign Stage button on the campaign type page
        campaignTypeInactiveLink			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Inactive campaign type link on Relationship management
        campaignTypeITableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Campaign type table on Relationship management
        campaignStageInactiveLink			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Inactive campaign stage link on Relationship management
        campaignStageTableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Campaign stage table on Relationship management
        emrButton							: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[8]/a',//Emr button on relation ship management
        emrPageSaveBtn						: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Emr page save button on relation ship management
        emrPlusIcon							: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Emr page plus icon on inactive span on relation ship management
        emrInactiveLink						: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',//EMR inactive link
        emrInactiveTableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//EMR inactive table list
        specialtyBtn						: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[13]/a', //Specialty button on relation ship management
        rootCauseBtn						: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[12]/a',//Root cause button on relation ship management
        rootCauseTableList					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',//Root cause Table list
        rootCauseAddLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',//Root cause add link on relation ship management
        rootCauseSaveBtn				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Root cause save button on relation ship management
        rootCausePlusIcon				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr[1]/td[2]/div/span',
        competitiveLabBtn					: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[5]/a',//Competitive lab button on relation ship management
        competitiveLabTableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',//Competitive lab table on Competitive page
        competitiveLabAddLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',//Competitive lab having add link
        competitiveLabSaveBtn				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Competitive lab having save button
        competitiveLabInactivePlusIcon		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Plus icon on competitive lab page
        competitiveLabInactiveLink			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',//Inactive link on competitive lab
        contactTypeBtn						: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[6]/a',//Contact type button on relation ship management
        contactTypeTableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',//Contact type table list on relation ship management
        contactLabAddLink					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',//Contact type having add link
        contactLabSaveBtn					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Contact type having save button
        contactTypeInactivePlusIcon			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Contact type plus icon on contatct type inactive table list
        contactTypeSaveBtn					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Contact type having save button
        contactTypeInactiveLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',//Contact type inactive link
        contactTypeInactiveTableList		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Contact type having inactive table list
        correctionActionBtn					: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[7]/a',//Correction action button on relation ship management
        correctionActionTableList			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',//Correction action table on relation ship management
        correctionActionAddLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',//Correction action add link on relation ship management
        correctionActionSaveBtn				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',//Correction action having save button
        correctionActionInactiveLink		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',//Correction action button having inactive link
        correctionActionInactivePlusIcon	: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Correction action having save icon
        correctionActionInactiveTableList	: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Correction action table listrelation ship management
        activityStatusBtn					: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[2]/a',//Activity status button on the relation ship management page
        activityStatusTableList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/table',//Activity status table list on the relation ship management page
        activityStatusAddLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[1]/form/div/span',//Activity status having inactive link
        activityStatusSaveBtn				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/button[1]',//Activity status having save button
        activityStatusInactiveLink			: '//*[@id="inactive-nav"]/ul/li/a',//Activity status having inactive link
        activityStatusInactiveTable			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Activity status inactive table list
        activityStatusPlusIcon				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',//Activity status having save button


        //Search Data
        // inactiveActivityStatus			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',
        // plusIcon							: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',
        // elementXpath						: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[" + i + "]/td[1]/div/input',

        activityCategoryInactiveLink		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Activity Category inactive link
        activityCategoryTableList			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Activity Category table list
        activityCategoryInactiveTable       :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Inactive list table
        activitySubCategoryInactiveLink		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/a',//Activity SubCategory inactive link
        activitySubCategoryTableList		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr',//Activity SubCategory table list
        activitySubCategoryInput			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr/td[1]/input',//Activity SubCategory input field
        opportunityStageBtn					: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[9]/a', //Opportunity stage button on the relation ship management page
        opportunityStageTableList			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr[1]/td[1]/div/div', //Opportunity stage table list

        opportunityTypeBtn					:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[10]/a',
        opportunityTypeTableList			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',
        opportunityTypeAddLink				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',
        // opportunityTypeTableList			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table/tbody/tr[1]/td[1]/div/div', //
        opportunityTypeSaveBtn              :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',
        opportunityTypeInactiveLink         :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',
        opportunityTypePlusIcon				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',

        organizationTypeBtn					:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[3]/ul/li[11]/a',
        organizationTypeTableList			:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/table',// /tbody/tr[1]/td[1]/div/div',
        organizationTypeAddLink				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/a',
        organizationTypeSaveBtn:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',
        organizationTypeInactiveLink        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',
        organizationTypePlusIcon				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',
        organizationTypeInactiveTable			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',//Organization type inactive table on the relation ship management page

        opportunityStageAddLink				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[1]/form/div/span', //Opportunity stage having add link
        opportunityStageSaveBtn				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]', //Opportunity stage having save button
        activityStatusPlusIcon				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table/tbody/tr[1]/td[2]/span',
        opportunityStageInactiveLink        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a', //opportunity stage having inactive link
        opportunityStageInactiveTable	    :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table', //opportunity stage Inactive campaign stage table
        opportunityStageSaveBtn				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/button[1]',

        rootCauseInactiveTable:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/ul/table',///tbody/tr[1]/td[1]/input
        rootCauseInactiveLink        :'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li/a',

    }
module.exports.adminRelationshipManagementPageElements = adminRelationshipManagementPageElements;

var bulkImportElements = {
        importTaskTab                           :'/html/body/div[1]/main/div[3]/div/div[1]/div[1]', //import tasks tab
        viewMappingsTab                         :'/html/body/div[1]/main/div[3]/div/div[1]/div[2]', //View mappings tab
        bulkImportJobsTable                     :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/table', //jobs table
        startNewImportBtn                       :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[1]/div[2]/div/button', //Start new import button
        objectTypeLi                            :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/ul/li[1]', //Object type li
        uploadFileLi                            :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/ul/li[2]', //Upload file li
        selectDelimetersLi                      :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/ul/li[3]', //Select delimeters li
        selectMappingLi                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/ul/li[4]', //Select mapping li
        objectTypesDropdown                     :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/ul/li/div/div/select', //Object types dropdown in import pop up
        importNextBtn                           :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[1]/button[2]', //Next button in import pop up
        importPrevBtn                           :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[1]/button[1]', //Prev button in import pop up
        fileNameInput                           :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/form/ul/li/div/input', //File name input
        fileUploadInput                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/form/div/p[1]/input', //Upload file input
        uploadBtn                               :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/form/div/p[2]/button', //Upload button
        recentFilesTable                        :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[2]/div/div[2]/table', //Recent files table
        advancedOptions                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/span', //Advanced options
        columnSeparatorInput                    :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[1]/ul/li[1]/div/input', //Column separator input
        subColSeparatorInput                    :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[1]/ul/li[2]/div/input', //Sub column separator input
        quoteCharacterInput                     :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[1]/ul/li[3]/div/input', //Quote character input
        escapeCharacter                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[1]/ul/li[4]/div/input', //Escape character input
        modalHeader                             :'/html/body/div[4]/div[2]/div[2]/div[1]/h4', //modal header
        encodingDropdown                        :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[2]/ul/li[1]/div/div/select', //Encoding dropdown
        nullStringInput                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[1]/div/div[2]/ul/li[2]/div/input', //Null String input
        refreshPreviewBtn                       :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/div[1]/button', //Refresh preview button
        previewTable                            :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/div[2]/div[2]/table', //Preview table
        mappingDropdown                         :'/html/body/div[4]/div[2]/div[2]/div[2]/div/div/div[2]/div[4]/ul/li[1]/div/div/select', //Mapping dropdown
        mappingNameInput                        :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/ul/li[2]/div/input', //Mapping name input
        saveModeDropdown                        :'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/ul/li[3]/div/div/select', //Save mode dropdown
        sourceUl                                :'/html/body/div[1]/main/div[3]/div/div[3]/div[1]/ul', //Source ul
        addMappingPlusBtn                       :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/span', //add mapping plus button
        functionDropdown                        :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/div/div/ul/li[2]/div/div/select', //Function dropdown
        sourceDropdownExpandBtn                 :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/div/div/ul/li[3]/div/div[1]/div/button', //Select source dropdown expand button
        sourceInputBox                          :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/div/div/ul/li[3]/div/div[1]/input', //Source input box
        targetDropdown                          :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/div/div/ul/li[4]/div/div/select', //Target dropdown
        saveMappingBtn                          :'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/button[1]', //Save mapping button
        createJobBtn                            :'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/button[2]', //Create job button
        cancelBtn                               :'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div/a', //Cancel button
        mappingPageHeader                       :'/html/body/div[1]/main/div[3]/div/div[1]/div/h1', //Bulk mapping page header
        mapsDiv                                 :'/html/body/div[1]/main/div[3]/div/div[3]/div[2]/div', //Maps div



}
module.exports.bulkImportElements = bulkImportElements;

var adminCustomizationPageElements = {
    udfDropDownList				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/select',//User defined fields drop down list
    orgUDFList					: "//option[contains(@value,'Organization')]",//To select organization from User defined fields drop down list
    campaignUDFList				: "//option[contains(@value,'Campaign')]",//To select Campaign from User defined fields drop down list
    caseUDFList					: "//option[contains(@value,'Case')]",//To select Case from User defined fields drop down list
    contactUDFList				: "//option[contains(@value,'Contact')]",//To select Contact from User defined fields drop down list
    memoUDFList					: "//option[contains(@value,'Memo')]",//To select Memo from User defined fields drop down list
    opportunityUDFList				: "//option[contains(@value,'Opportunity')]",//To select Opportunity from User defined fields drop down list
    panelUDFList				: "//option[contains(@value,'Panel')]",//To select Panel from User defined fields drop down list
    patientUDFList				: "//option[contains(@value,'Patient')]",//To select Patient from User defined fields drop down list
    taskUDFList					: "//option[contains(@value,'Task')]",//To select Task from User defined fields drop down list
    udfDropDownList				: '/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[5]/ul/li[7]/a',//User defined fields drop down list
    udfCreateNewField				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div/ul/li[2]/a',//On Customization page User Defined fields having create new field link
    udlCreateNewField				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div/ul/li[2]/a',//On Customization page User Defined Layout having create layout link
    udfInsightNameInput				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[1]/li[2]/div/input',//On Customization page User Defined fields Insight name
    udfDisplayNameInput				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[1]/li[3]/div/input',//On Customization page User Defined fields display name
    udfSaveBtn					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/div[2]/button',//On Customization page User Defined fields having save button
    udfInsightNameTable				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/div/table',//insiht name Data table list on user defined fields
    componentsDropDownList			: '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/div[3]/select',//components Drop Down List on the layout designe r page
    firstElementDropDownList			: '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/ul[3]/li[10]/div/div/table/tbody/tr/td[2]/div/div',//first Element DropDown List in components Drop Down List
    saveBtnLayoutDesignerPage			:'/html/body/div[1]/main/div[3]/div/div[1]/div/button',//Save button on the layout designer page
    layoutNameInput				:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/ul/li[2]/div/div/input', //Layout name input on layout designer page
    layoutNameTableList				:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/div/table',//Table list on user defined layout page
    dragElementXpath				: '/html/body/div[1]/main/div[3]/div/div[2]/div[1]/ul[3]',//user defined layout page Drag element
    objectDropList				: '/html/body/div[1]/main/div[3]/div/div[2]/div[3]/div[1]/div/div/div[1]/ul[2]',//Drop section on user defined fields
    dataTypeDropDownList			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/ul[2]/li[1]/div/div/select',//Data type drop down list on user defined fields

    //Work flow template
    createLinkWFT				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[2]/div/ul/li[2]/a',//Work flow template link on customization page
    workFlowTemplateDropDownList		: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div/div[1]/div[1]/div/select',//Work flow template drop downlist on work flow template page
    caseOptionWFT				: "//option[contains(.,'Case')]",// Case option on work flow template choose option
    taskOptionWFT				: "//option[contains(.,'Task')]",// Task option on work flow template choose option
    memoOptionWFT				: "//option[contains(.,'Memo')]",// Memo option on work flow template choose option
    recordTypeDiv				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div/ul/li[1]/div/div',//Record type div on Work flow template
    workFlowTemplateNameInput			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div/ul/li[2]/div/input',//Work flow name input on WOrkflow template page
    workFlowTemplateDescInput			: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[3]/div/ul/li[3]/div/textarea',//Work flow name input on WOrkflow template page
    dragElementWFT				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[4]/div[1]/ul/li[2]',//Drag element on user defined layout edit page
    dropElementDivWFT				: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[4]/div[2]/div[2]/div[1]',//Drop element on user defined layout edit page
    saveBtnWTF					: '/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div/button',//Save button on user defined layout edit page

}
exports.adminCustomizationPageElements = adminCustomizationPageElements;

var webservicePageElements = {
    interfaceOption			: '/html/body/div[1]/header/div[1]/div/div/div/nav/ul/li[1]/ul/li[3]/a',//Interface option on header menu
    interfaceDataTransactionsTab	:'/html/body/div[1]/main/div[3]/div/div[1]/div[1]',//Data Transactions tab on interface page
    interfaceDataSettingsTab		:'/html/body/div[1]/main/div[3]/div/div[1]/div[2]',//Settings tab on interface page
    interfaceSystemInput		:'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[1]/div/div/select',//System input field in standard fields on interface page
    interfaceMessageInput		:'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[1]/div[1]/ul/li[2]/div/input',// Message input field in standard fields on interface page
    interfaceResetBtn			:'/html/body/div[1]/main/div[3]/div/div[2]/div[1]/form/div[3]/div/button[2]',//Reset button in standard fields on interface page

    interfaceSettingTable		:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div[2]/table',//Interface settings in standard fields on interface page
    interfaceCreateLink			:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/ul/li[2]/a',//Interface create link on standard fields on interface page
    interfaceNameInput			:'/html/body/div[1]/main/div[3]/div/div/form/ul/li[1]/div/input',//Interface nameon new creating interface page
    interfaceConnectionIdInput		:'/html/body/div[1]/main/div[3]/div/div/form/ul/li[2]/div/input',//connection id on new creating interface page
    interfaceSaveBtn			:'/html/body/div[1]/main/div[3]/div/div/div/div/button[1]', //Save button on new creating interface page

    interfaceNameMessageLabel		:'/html/body/div[1]/main/div[3]/div/div/form/ul/li[1]/div/label',//Interface Name message label on new creating interface page
    interfaceConnectionIdMessageLabel	:'/html/body/div[1]/main/div[3]/div/div/form/ul/li[2]/div/label',//Interface c message label on new creating interface page
    interfaceTabel			:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div[2]/table',//Interface table on new creating interface page
    interfaceActiveIconTable		:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div[2]/table',//Interface Active table icon on new creating interface page
    interfaceDeactiveIcon		:'/html/body/div[1]/main/div[3]/div/div[2]/div[2]/div[2]/table/tbody/tr[4]/td[6]/i',//Interface de active  icon on new creating interface page


    //Distribution group

    addNewDistributionGroupLink:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a',//Create distribution group link on Distribution group page
    distributionGroupNameInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[1]/div/input',//Name distribution group input field on Distribution group page
    distributionGroupEmailInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[2]/div/input',//Email distribution group input field on Distribution group page
    distributionGroupDescInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div/ul/li[3]/div/input',//Discription distribution group input field on Distribution group page
    saveBtnDistributionGroup:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button',//Save button distribution group input field on Distribution group page
    distributionGroupTable:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table',//Table distribution group input field on Distribution group page
    cloudSearchField: '/html/body/div[1]/header/div[1]/div/div/div/nav/div/div/div/div[1]/input', //Cloud search field on the desktop
    addUserInputField:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[3]/div/ul/li[2]/div/div/div[1]/input',//Add user input field
    sentMsgTable:'/html/body/div[1]/main/div[3]/div/div[3]/div[3]/div[1]/div[2]/div/div/div[2]/table',//Sent message table on the collaboration centre page
    sendMessageToSearchInput:'/html/body/div[1]/main/div[3]/div/div[4]/div[2]/div/div/div[2]/div/div[3]/div[1]/ul/li[1]/div/div/div[1]/div[2]/div/div/div/div/div[1]/input',//To search input on send message pop window

    //User Profile

    userProfileTab:'/html/body/div[1]/main/div[3]/div/div/div[1]/ul/li[1]/ul/li[3]/a',//User Profile tab on Administration menu
    createNewUserProfile:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[1]/ul/li/a',//Create new user profile link
    userProfileNameInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[1]/div/input',//User profile new name input
    userProfileDescInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[2]/div/input',//User profile description input
    userProfileReadOnlyCheckBox:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/ul/li[4]/div/span/input',//Read only check box input on user profile page
    userProfileVisibleDistributionGroupTab:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[1]/div/button[3]',//Visible Distribution Group Tab on user profile
    userProfileVisibleDistributionGroupInput:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[2]/div[1]/div[2]/div[3]/div[1]/div/div[2]/ul/li/div/div[1]/input',//Visible Distribution Group input on user profile
    userProfileSaveBtn:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/form/div[1]/div[2]/button',//Save button on the on user profile
    userProfileTable:'/html/body/div[1]/main/div[3]/div/div/div[2]/div/div/div[2]/div[2]/div/div/div[2]/table',//User profile table on the on user profile
    userProfilePopWindowYesBtn:'/html/body/div[4]/div[2]/div/div[3]/button[1]',//User profile pop window yes button on user profile
}
exports.webservicePageElements = webservicePageElements;